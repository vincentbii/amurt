/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
//
// Note that these are all defined as panel configs, rather than instantiated
// as panel objects.  You could just as easily do this instead:
//
// var absolute = new Ext.Panel({ ... });
//
// However, by passing configs into the main container instead of objects, we can defer
// layout AND object instantiation until absolutely needed.  Since most of these panels
// won't be shown by default until requested, this will save us some processing
// time up front when initially rendering the page.
//
// Since all of these configs are being added into a layout container, they are
// automatically assumed to be panel configs, and so the xtype of 'panel' is
// implicit.  To define a config of some other type of component to be added into
// the layout, simply provide the appropriate xtype config explicitly.
//
/*
 * ================  Start page config  =======================
 */
// The default start page, also a simple example of a FitLayout.
var start = {
    id: 'start-panel',
    title: 'Start Page',
    layout: '',
    bodyStyle: 'padding:25px',
    contentEl: 'start-div'  // pull existing content from the page
};

/*
 * ================  Adult Pharmacy Form config  =======================
 */
var adult = {
    Xtype: 'panel',
    id: 'adult-panel',
    title: 'Adult Pharmacy Form',
    layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'adult'
};

/*
 * ================  ARV Follow-up config  =======================
 */
var followup ={
    Xtype: 'panel',    
    id: 'followup-panel',
    title: 'ARV Follow-up',
    layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'followup'
};

/*
 * ================  Contact & Care Tracking config  =======================
 */
var tracking = {
    Xtype: 'panel',    
    id: 'tracking-panel',
    title: 'Contact & Care Tracking',
    layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'tracking'
};

/*
 * ================  Family Information config  =======================
 */
var family = {
    Xtype: 'panel',
    id: 'family-panel',
    title: 'Family Information',
    layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'family'
};

/*
 * ================  HIV Care Enrollment config  =======================
 */

var enrollment = {
    Xtype: 'panel',    
    id: 'enrollment-panel',
    title: 'HIV Care Enrollment',
   layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'enrollment'
};
/*
     * ================  HIV Care Patient Profile config  =======================
     */
var profile = {
    Xtype: 'panel',
    id: 'profile-panel',
    title: 'HIV Care Patient Profile',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'profile'
};

/*
     * ================  Initial Evaluation config  =======================
     */
var initialData = {
    Xtype: 'panel',
    id: 'initialData-panel',
    title: '',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    autoHeight: true,
    contentEl: 'initial'

};

/*
     * ================  Laboratory Order/ results config  =======================
     */
var laboratory = {
    Xtype: 'panel',
    id: 'laboratory-panel',
    title: 'Laboratory Order/ results',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'laboratory'
};

/*
     * ================  Pediatric Pharmacy Order config  =======================
     */

var pediatric = {
    Xtype: 'panel',
    id: 'pediatric-panel',
    title: 'Pediatric Pharmacy Order',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'pediatric'
};

/*
     *================================  ART Reports ============================
     */
    
/*
     * ================  No of Patients Enrolled in ART Report  =======================
     */
var patients = {
    Xtype: 'panel',
    id: 'patients-panel',
    title: 'Report: Patients Enrolled In ART',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'patients'
};

/*
     * ================  No of Patients in Care  =======================
     */
var pcare = {
    Xtype: 'panel',
    id: 'pcare-panel',
    title: 'Report: No of Patients in Care',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'pcare'
};

/*
     * ================  No of Patients Who Died, Transferred out, Missed Appointments, or Stopped ART  =======================
     */
var ptracking = {
    Xtype: 'panel',
    id: 'ptracking-panel',
    title: 'Report: No of Patients Who Died, Transferred, Missed Appointments Or Stopped ART ',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'ptracking'
};

/*
     * ================  No of Children in ART  =======================
     */
var children = {
    Xtype: 'panel',
    id: 'children-panel',
    title: 'Report: No of Children in ART ',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'children'
};

/*
     * ================  No of Children in ART  =======================
     */
var ccare = {
    Xtype: 'panel',
    id: 'ccare-panel',
    title: 'Report: No of Children in Care ',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'ccare'
};

/*
     * ================  No of Patients Enrolled Per Month Or a Predifined Period  =======================
     */
var monthly = {
    Xtype: 'panel',
    id: 'monthly-panel',
    title: 'Report: Patients Enrolled Monthly ',
    //        layout: 'ux.center',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'monthly'
};

   