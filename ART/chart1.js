
Ext.chart.Chart.CHART_URL = '../include/Extjs/resources/charts.swf';

Ext.onReady(function(){
    var store = new Ext.data.JsonStore({
        url: 'chart1.php',
        fields: ['pharmacy','count'],
        root: 'chart_list'
    });
    store.load();

    new Ext.Panel({
        width: 250,
        height: 250,
        title: 'Percent Adult and Children in Pharmacy',
        draggable: true,
        animate : true,
        renderTo: 'container',        
        items: {
            store: store,
            xtype: 'piechart',
            dataField: 'count',
            categoryField: 'pharmacy',
            //extra styles get applied to the chart defaults
            extraStyle:
            {
                legend:
                {
                    display: 'bottom',
                    padding: 5,
                    font:
                    {
                        family: 'Tahoma',
                        size: 13
                    }
                }
            }
        }
    });
});

