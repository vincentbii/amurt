Ext.onReady(function(){
    var stores = new Ext.data.JsonStore({

        url: 'chart2.php',
        root: 'charts_list',
        method: 'POST',
        fields: [{
            name: 'testMonth',
            type:'string'
        }, {
            name: 'labtest',
            type: 'string'
        },{
            name: 'testCount',
            type: 'int'
        }]
        
    });
    stores.load();

    //    function refreshData() {
    //        store.loadData('chart_list');
    //    }
    
    new Ext.Panel({
        width: 600,
        height: 300,
        draggable: true,
        bodyStyle:'overflow-y:auto; background:transparent;',
        renderTo: 'container2',
        title: 'Column Chart showing no of Lab Test Done Per Month',
        //        tbar: [{
        //            text: 'Load new data set',
        //            handler: function(){
        ////                store.loadData(generateData());
        //            }
        //        }],
        items: {
            xtype: 'columnchart',
            store: stores,
            yField: 'testMonth',
            url: '../include/Extjs/resources/charts.swf',
            xField: 'labtest',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Lab Tests'
            }),
            yAxis: new Ext.chart.NumericAxis({
                title: 'Months',
                labelRenderer: Ext.util.Format.numberRenderer('0,0')
            }),
            series: [{
                type: 'column',
                displayName: 'Months',
//                labelRotation: -90,
                yField: 'testMonth',
                style: {
                    image:'bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'No. of Tests',
                yField: 'testCount',
                style: {
                    color: 0x15428B
                }
            }],
            extraStyle: {
                xAxis: {
                    labelRotation: -90
                }
            }
        }
    });

//    Ext.TaskMgr.start({
//        run: refreshData,
//        interval: 5000
//    });

});
