/*!
 * Ext JS Library 3.3.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.onReady(function() {
    Ext.QuickTips.init();

    var fm = Ext.form;
    var primaryKey='ID'; //primary key is used several times throughout

    var store = new Ext.data.JsonStore({

        url: 'getTests.php',
        root: 'tests',
         id: 'ID',//
        baseParams:{
            task: "readTests"
        },//This
        fields: ['ID','pid', 'enroll_no','test_Date','weight','doses_missed',
        'diag_treat','OI-PX','CD4','lab','ART_change_stop','reason']
    });



    var testGrid = new Ext.grid.EditorGridPanel({
//        title: 'Profile Tests',
        store: store,
        frame:true,
        width:795,
        height:200,
        collapsible:true,
        renderTo:'docbody2',
        columns: [
        {
            header: "ID",
            width: 35,
            dataIndex: 'ID',
            sortable: true,
            hidden:false
        },
        {
            header: "PID",
            width: 80,
            dataIndex: 'pid',
            sortable: true,
            hidden:true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "enroll_no",
            width: 70,
            dataIndex: 'enroll_no',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },

        {
            header: "Test Date",
            width: 80,
            dataIndex: 'test_Date',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {

            header: "Weight",
            width: 50,
            dataIndex: 'weight',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "Doses Missed",
            width: 80,
            dataIndex: 'doses_missed',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "Diagnosis/Treatment",
            width: 120,
            dataIndex: 'diag_treat',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "OI-PX",
            width: 50,
            dataIndex: 'OI-PX',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "CD4",
            width: 40,
            dataIndex: 'CD4',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "LABS",
            width: 80,
            dataIndex: 'lab',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "ART Change Stop",
            width: 100,
            dataIndex: 'ART_change_stop',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        {
            header: "Reason",
            width: 80,
            dataIndex: 'reason',
            sortable: true,
            editor: new fm.TextField({
                allowBlank: false
            })
        },
        ],
        clicksToEdit: 2,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect:false
        }),
        stripeRows: true,
        tbar: [{
            text: 'Add Tests',
            iconCls:'add',
            handler : function(){
                // access the Record constructor through the grid's store
                var ptype =testGrid.getStore().recordType;
                var p = new ptype({
                     ID:0,
                    test_Date:'2010-10-01',
                    weight:'',
                    doses_missed:'0',
                    diag_treat:'',
                    CD4:'',
                    lab:'',
                    ART_change_stop:'',
                    reason:''
                });
                testGrid.stopEditing();
                store.insert(0, p);
                testGrid.startEditing(0, 1);
            }

        },'-',{
            text: 'Delete Tests',
            iconCls:'remove'//,
        //                handler :handleDelete
        }, '->', // next fields will be aligned to the right
        {
            text: 'Refresh',
            tooltip: 'Click to Refresh the table',
            handler: refreshGrid,
            iconCls:'refresh'
        }]


    });
    store.load();
    testGrid.addListener('afteredit', handleEdit);

    function refreshGrid() {
        store.reload();//
    } // end refresh

    function handleEdit(editEvent) {
        //determine what column is being edited
        var gridField = editEvent.field;

        //start the process to update the db with cell contents
        updateDB(editEvent);

    }

    function updateDB(oGrid_Event) {

        if (oGrid_Event.value instanceof Date)
        {   //format the value for easy insertion into MySQL
            var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
        } else
{
            var fieldValue = oGrid_Event.value;
        }

        //submit to server
        Ext.Ajax.request(
        {
            waitMsg: 'Saving changes...',
            url: 'getTests.php',
            params: {
                task: "update",
                key: primaryKey,
                keyID: oGrid_Event.record.data.ID,

                field: oGrid_Event.field,//the column name
                value: fieldValue,//the updated value

                originalValue: oGrid_Event.record.modified
            },//end params
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            },//end failure block

            success:function(response,options){
                if(oGrid_Event.record.data.ID == 0){
                    var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server

                    var newID = responseData.newID;
                    oGrid_Event.record.set('newRecord','no');
                    oGrid_Event.record.set('ID',newID);
                    store.commitChanges();
                } else {
                    store.commitChanges();
                }
            }//end success block
        }//end request config
        ); //end request
    }; //end updateDB


//    var buildUI = testGrid.addButton({
//        text: 'Delete',
//        iconCls: 'icon-delete',
//        id:'delete',
//        handler : function() {
//            Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(btn, text){
//                if (text == 'Yes'){
//                    var index = testGrid.getSelectionModel().getSelectedCell();
//                    if (!index) {
//                        return false;
//                    }
//                    var rec = testGrid.store.getAt(index[0]);
//                    testGrid.store.remove(rec);
//                        url:'updateDepts.php'
//                }else{
//                    return false;
//                }
//            });
//        }
//    });


});


