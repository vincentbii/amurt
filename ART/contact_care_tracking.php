<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

//check if any previous entry's have been made and how many
$newreg = 'select cctauto from art_contact_care_tracking';
$result = $db->Execute($newreg);
$recordCount = $result->RecordCount($newreg);

if ($recordCount == '0') {
    $mid = 1;
}
else {
    //if records exist loop through them to the end and register last record
    while ($marow = $result->FetchRow($newreg)) {
        $mid = $marow[0];
    }
    //assign new number to next new record
    $mid = $mid+1;
}

///////////////////////// Contact & Care Tracking Dates ////////////////////
//
$missed_appointment_date = $_POST['cct_appointmentyear']."-". $_POST['cct_appointmentmonth']."-".$_POST['cct_appointmentday'];
$missed_appointment_date=  mysql_real_escape_string($missed_appointment_date);


$last_contact_date = $_POST['cct_contactyear']."-". $_POST['cct_contactmonth']."-".$_POST['cct_contactday'];
$last_contact_date=  mysql_real_escape_string($last_contact_date);


$first_attempt_date = $_POST['cct_firstyear']."-". $_POST['cct_firstmonth']."-".$_POST['cct_firstday'];
$first_attempt_date=  mysql_real_escape_string($first_attempt_date);


$second_attempt_date = $_POST['cct_secondyear']."-". $_POST['cct_secondmonth']."-".$_POST['cct_secondday'];
$second_attempt_date=  mysql_real_escape_string($second_attempt_date);


$third_attempt_date = $_POST['cct_thirdyear']."-". $_POST['cct_thirdmonth']."-".$_POST['cct_thirdday'];
$third_attempt_date=  mysql_real_escape_string($third_attempt_date);


$fourth_attempt_date = $_POST['cct_fourthyear']."-". $_POST['cct_fourthmonth']."-".$_POST['cct_fourthday'];
$fourth_attempt_date=  mysql_real_escape_string($fourth_attempt_date);


$last_attempt_date = $_POST['cct_lastyear']."-". $_POST['cct_lastmonth']."-".$_POST['cct_lastday'];
$last_attempt_date=  mysql_real_escape_string($last_attempt_date);

$arv_end_date = $_POST['cct_arvyear']."-". $_POST['cct_arvmonth']."-".$_POST['cct_arvday'];
$arv_end_date=  mysql_real_escape_string($arv_end_date);

$death_date = $_POST['cct_deathyear']."-". $_POST['cct_deathmonth']."-".$_POST['cct_deathday'];
$death_date=  mysql_real_escape_string($death_date);


$care_end_date = $_POST['cct_careyear']."-". $_POST['cct_caremonth']."-".$_POST['cct_careday'];
$care_end_date=  mysql_real_escape_string($care_end_date);
///////////////////////////////////////////////////////////////////////////
$patient_name =$_POST['patient_name'];
$hosp_no =$_POST['hosp_no'];
$satellite_no =$_POST['satellite_no'] ;
$patient_enrollment_no =$_POST['patient_enrollment_no'];
$patient_contact_info =$_POST['patient_contact_info'];
$guardian_name =$_POST['guardian_name'];
$guardian_contact_info =$_POST['guardian_contact_info'];
$first_attempt_contact =$_POST['first_attempt_contact'];
$first_attempt_mode =$_POST['first_attempt_mode'];
$first_attempt_guardian =$_POST['first_attempt_guardian'];
$second_attempt_contact =$_POST['second_attempt_contact'];
$second_attempt_mode =$_POST['second_attempt_mode'];
$second_attempt_guardian =$_POST['second_attempt_guardian'];
$third_attempt_contact =$_POST['third_attempt_contact'];
$third_attempt_mode =$_POST['third_attempt_mode'];
$third_attempt_guardian =$_POST['third_attempt_guardian'];
$fourth_attempt_contact =$_POST['fourth_attempt_contact'];
$fourth_attempt_mode =$_POST['fourth_attempt_mode'];
$fourth_attempt_guardian =$_POST['fourth_attempt_guardian'];
$last_attempt_contact =$_POST['last_attempt_contact'];
$last_attempt_mode =$_POST['last_attempt_mode'];
$last_attempt_guardian =$_POST['last_attempt_guardian'];
$patient_pallative_care = $_POST['patient_pallative_care'];
$patient_pallative_reason =$_POST['patient_pallative_reason'];
$patient_care_ended =$_POST['patient_care_ended'];
$exit_reason =$_POST['exit_reason'];
$exit_reason_extra =$_POST['exit_reason_extra'];
$exit_reason_comments =$_POST['exit_reason_comments'];



$reg_result = "$mid, $patient_name, $hosp_no, $satellite_no, $patient_enrollment_no, $patient_contact_info, $guardian_name,
$guardian_contact_info, $missed_appointment_date, $last_contact_date, $first_attempt_date, $first_attempt_contact,
$first_attempt_mode, $first_attempt_guardian, $second_attempt_date, $second_attempt_contact, $second_attempt_mode,
$second_attempt_guardian, $third_attempt_date, $third_attempt_contact, $third_attempt_mode, $third_attempt_guardian,
$fourth_attempt_date, $fourth_attempt_contact, $fourth_attempt_mode, $fourth_attempt_guardian, $last_attempt_date,
$last_attempt_contact, $last_attempt_mode, $last_attempt_guardian, $patient_pallative_care, $patient_pallative_reason,
$arv_end_date,$patient_care_ended, $exit_reason, $exit_reason_extra, $exit_reason_comments, $death_date, $care_end_date";


if ($reg_result == true)
{
$query = "insert into art_contact_care_tracking
(cctauto, patient_name, hosp_no, satellite_no, patient_enrollment_no, patient_contact_info, guardian_name, guardian_conatct_info,
missed_appointment_date, last_contact_date, first_attempt_date, first_attempt_contact, first_attempt_mode, first_attempt_guardian,
second_attempt_date, second_attempt_contact, second_attempt_mode, second_attempt_guardian, third_attempt_date,  third_attempt_contact,
third_attempt_mode, third_attempt_guardian, fourth_attempt_date, fourth_attempt_contact, fourth_attempt_mode, fourth_attempt_guardian,
last_attempt_date, last_attempt_contact, last_attempt_mode, last_attempt_guardian, patient_pallative_care, patient_pallative_reason,
arv_end_date, patient_care_ended, exit_reason, exit_reason_extra, exit_reason_comments, death_date, care_end_date) values

('$mid', '$patient_name', '$hosp_no', '$satellite_no', '$patient_enrollment_no', '$patient_contact_info', '$guardian_name',
'$guardian_contact_info', '$missed_appointment_date', '$last_contact_date', '$first_attempt_date', '$first_attempt_contact',
'$first_attempt_mode', '$first_attempt_guardian', '$second_attempt_date', '$second_attempt_contact', '$second_attempt_mode',
'$second_attempt_guardian', '$third_attempt_date', '$third_attempt_contact', '$third_attempt_mode', '$third_attempt_guardian',
'$fourth_attempt_date', '$fourth_attempt_contact', '$fourth_attempt_mode', '$fourth_attempt_guardian', '$last_attempt_date',
'$last_attempt_contact', '$last_attempt_mode', '$last_attempt_guardian', '$patient_pallative_care', '$patient_pallative_reason',
'$arv_end_date','$patient_care_ended', '$exit_reason', '$exit_reason_extra', '$exit_reason_comments', '$death_date', '$care_end_date')";

$results= $db->Execute($query) or
        die (mysql_error());


//provide link to members pguardian_name
echo "<script languguardian_name='JavaScript'>";
echo "document.location.href='index.php#'";
echo "</script>";
}
else
{
//provide a link back, tell them to try again
 echo $reg_result;
exit;
}
//end pguardian_name
?>