<?php
class DateDDLGenerator{

    var $intYear;
    var $intMonth;
    var $intDay;
    var $bolSetToCurrentDay;

    function DateDDLGenerator(){
        $this->bolSetToday = false;
        $this->intYear  = date("Y");
        $this->intMonth = date("m");
        $this->intDay   = date("d");
    }

    function setToCurrentDay(){
        $this->bolSetToCurrentDay = false;
    }

    #Generate Year range
    function genYearDDL($selName = 'Year', $yearCount = 50, $year = ''){
        /*
            Check if the year passed in is the same as current year.
            If the year got is not given or same as current year, the list
            will select the current year by default.  Otherwise, $yearSelect
            will be set to what user entered.
        */
        $yearSelect = $year == '' ? date("Y") : $year;

        /*
            $yearCount: it is the length of your drop down list, i.e. how many
            years do you want to show.  It is 50 by default, which shows 50 years
            from now.
        */

        $str = "<select id='$selName' name='$selName'>\n";
        $str .= "<option value=''>   </option>\n";
        for($i = $yearSelect; $i >= ($yearSelect - $yearCount); $i--){
            if($this->bolSetToCurrentDay == true){
                $selected = $this->intYear == $i ? 'selected="selected"' : '';
            }
            $str .= "\t<option value='$i' $selected>$i</option>\n";
        }
        $str .= "</select>\n";
        print $str;
    }

    #Generate month range from 1 to 12
    function genMonthDDL($selName = 'Month', $date_format = 'short'){
        $shortM = array(1 => "01", "02", "03",
                             "04", "05", "06",
                             "07", "08", "09",
                             "10", "11", "12");

        $longM  = array(1 => "January", "February", "March",
                             "April"  , "May"       , "June" ,
                             "July"      , "Aug"       , "September",
                             "October", "November", "December");

        $str = "<select id='$selName' name='$selName'>\n";
        $str .= "<option value=''>   </option>\n";
        if($date_format == 'short'){
            for($i = 1; $i <= 12; $i++){
                if($this->bolSetToCurrentDay == true){
                    $selected = $this->intMonth == $i ? 'selected="selected"' : '';
                }
                $str .= "\t<option value='$i' $selected>".$shortM[$i]."</option>\n";
            }
        }elseif($date_format == 'long'){
            for($i = 1; $i <= 12; $i++){
                if($this->bolSetToCurrentDay == true){
                    $selected = $this->intMonth == $i ? 'selected="selected"' : '';
                }
                $str .= "\t<option value='$i' $selected>".$longM[$i]."</option>\n";
            }
        }
        $str .= "</select>\n";

        print $str;
    }

    #Generate day range from 1 to max days of relevant month
    function genDayDDL($selName = 'Day'){
        $str = "<select id='$selName' name='$selName'>\n";
        $str .= "<option value=''>   </option>\n";

        //Thanks to Peter K on this improvement and now this method support leap year
        if ($this->intMonth == 2) {                                            // February ?
            $leap_day = 0;

            if ($this->intYear >= 4 && $this->intYear % 4 == 0) {            // Leap year ?
                if ($this->intYear >= 1800 && $this->intYear % 100 == 0) {    // No accurate leap centuries before that
                    if (($this->intYear / 100) % 4 == 0)
                        $leap_day = 1;
                } else
                    $leap_day = 1;
            }

            $max_days = 28 + $leap_day;
        } else if ($this->intMonth == 4 || $this->intMonth == 6 ||
                   $this->intMonth == 9 || $this->intMonth == 11)
            $max_days = 30;
        else
            $max_days = 31;

        for($i = 1; $i <= $max_days; $i++){
            if($this->bolSetToCurrentDay == true){
                $selected = $this->intDay == $i ? 'selected="selected"' : '';
            }
            $str .= "\t<option value='$i' $selected>$i</option>\n";
        }
        $str .= "</select>\n";
        print $str;
    }
}
?>