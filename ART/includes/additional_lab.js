function openLab(){

//var fm = Ext.form;

var add_LabReader = new Ext.data.JsonReader(
{
        url: 'add_lab.php',
        root: 'addlab_list'
//        fields: ['ID', 'testID','labTest']
    },
[
{
    name: 'ID',
    type:'numeric',
    width: 25
},
{
    name: 'labTest',
    type: 'string',
    width: 150
}
]);


//    var filters = new Ext.ux.grid.GridFilters({
//        autoReload: false, //don&#39;t reload automatically
//        local: true, //only filter locally
//        filters: [{
//            type: 'numeric',
//			width: 25,
//            dataIndex: 'ID'
//        }, {
//            type: 'string',
//			width: 150,
//            dataIndex: 'labTest'
//        }]
//    });


    var createColModel = function (finish, start) {

        var columns = [{
            dataIndex: 'ID',
            width: 25,
            header: 'ID'
//            editor: new fm.TextField({
//                allowBlank: false
//            }),
//            filterable: true,
//           filter: {type: 'numeric'}
        }, {
            dataIndex: 'labTest',
            width: 175,
            header: 'Test Name'            
//            editor: new fm.TextField({
//                allowBlank: false
//            }),
//            filter: {
//                type: 'string'
//            }
        }];

        return new Ext.grid.ColumnModel({
            columns: columns.slice(start || 0, finish),
            defaults: {
                sortable: false
            }
        });
    };

    var add_labStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        //	        	url: 'sheldon.xml', //where to retrieve data
        url: 'add_lab.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
//        task: "readRates"
    },//This
    reader:add_LabReader,
    sortInfo: {
        field:'ID',
        direction:'ASC'
    }
//    groupField:'ID'
});

    var add_labsGrid = new Ext.grid.GridPanel({
        //border: false,
//        title: 'Lab Test',
//        region: 'center',
        store: add_labStore,
        colModel: createColModel(2),
        loadMask: true,
        height: 400,
        width: 150,
        view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
//        plugins: [filters],

        listeners: {
            render: {
                fn: function(){
                    add_labStore.load({
                        params: {
                            start: 0,
                            limit: 20
                        }
                    });
                //g.getsSelectionModel().selectRow(0)
                //delay: 10
                }
            }
        },

        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function(sm, row, rec) {
//                    Ext.getCmp("").getForm().loadRecord(rec);
                }
            }
        }),

//         tbar: [{
//        text: 'Export To Excel',
//        tooltip: 'Click to Export as Excel Format',
//        handler: function () {
//                    document.location.href='reports/exportiodrugs.php';
//                },
//        iconCls:'application_go'
//    }],

        bbar: [{

                
        }]
    });

    var add_labGrid = new Ext.Window ({
//    Xtype: 'panel',
    id: 'addlabCon-panel',
//    autoHeight: true,
//    autoWidth: true,
    title: 'Lab Test',
    items:[add_labsGrid]
});

add_labGrid.show();
}