<script type="text/javascript" src="composite-field.js"> </script>

<div id="adult" class="main_container">
    <div align="center">
        <div  class="main_container_form">
            <form id="" class="" method="post" action="adult_pharmacy.php" onSubmit="" name="adult_pharmacy" >
                <div class="section adult_section_1">
                    <div class="adult_section_1_left">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold" style="padding-right: 6px;"> Patient Name: </label>
                            <input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" readonly="true"/>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold" style="padding-right: 15px;"> ID: </label>
                            <input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" style="padding-right: 8px;" readonly="true"/>
                            &#8211;
                            <input name="enrollment_no" id="enrollment_no" type="text" size="7" maxlength="7" style="padding-left: 8px;" readonly="true"/>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class=" font_bold" style="padding-left: 27px;"> Satellite#</label>
                            <label class="font_bold" style="padding-left: 3px;">Patient Enrollment#</label>
                        </div>
                    </div>
                    <div class="adult_section_1_right">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold" style="padding-right: 15px;"> Existing Clinic/ Hosp#: </label>
                            <input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" readonly="true"/>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold" style="padding-right: 10px;"> Weight: </label>
                            <input name="weight" id="weight" type="text" size="3" maxlength="3" style="padding-right: 5px;" />
                            <label class="font_small" style="padding-right: 20px;" > Kg </label>
                            <label class="font_bold" style="padding-right: 10px;"> Age: </label>
                            <input name="age" id="age" type="text" size="3" maxlength="3" readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section adult_section_2">
                    <div class="adult_section_2_left">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold" style="padding-right: 22px;"> Specify Treatment Type: </label>
                            <input name="arv_type" id="arv_type" type="radio" value="art" /> ART
                            <label style="padding-right: 48px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="pmtct" /> PMTCT
                            <label style="padding-right: 22px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="pep" /> PEP
                            <label style="padding-right: 22px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="non_art" /> Non-ART
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold" style="padding-right: 36px;"> Specify ARV Provider: </label>
                            <input name="arv_provider" id="arv_provider" type="radio" value="governement" /> Government
                            <label style="padding-right: 12px;"></label>
                            <input name="arv_provider" id="arv_provider" type="radio" value="usg" /> USG
                            <label style="padding-right: 36px;"></label>
                            <input name="arv_provider" id="arv_provider" type="radio" value="other" /> Other
                        </div>
                    </div>
                    <div class="adult_section_2_right">
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section adult_section_3">
                    <div class="adult_section_3_left">
                        <div class="spacer"></div>
                        <div class=" section_row">                            
                            <table width="98%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="227" height="23"><label class="font_bold" > 1. ARV Medication </label></td>
                                    <td colspan="2"></td>
                                    <td width="93"></td>
                                    <td width="59"></td>

                                <td width="71"></td>

                                <td width="70"></td>
                                <td width="39"></td>
                                </tr>
                                <tr>
                                    <td width="227" height="23"><label class="font_bold" >NRTIs </label></td>
                                    <td colspan="2"><label class="font_bold" ></label></td>
                                    <td width="93"></td>
                                    <td width="59"></td>
                                
                                <td width="71"><label class="font_bold">Quantity
                                    </label></td>

                                <td width="70"><label class="font_bold">Quantity
                                    </label></td>
                                <td width="39"></td>
                            </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><label class="font_bold">Dose</label></td>
                                    <td><label class="font_bold">Frequency</label></td>
                                    <td><label class="font_bold">Duration</label></td>
                                    <td><label class="font_bold">Prescribed</label></td>
                                    <td><label class="font_bold">Dispensed</label></td>
                                    <td><label class="font_bold">Price</label></td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1274_meds" type="radio" value="ART1274"  />
                                        lamivudine (3TC)  </td>
                                    <td colspan="2">
                                        <input name="ART1274_dose" id="ART1274_dose" type="radio" value="300 mg" />
                                        300 mg</td>
                                    <td><input name="ART1274_frequency" id="ART1274_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1274_duration" id="ART1274_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1274_prescribed" id="ART1274_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1274_dispensed" id="ART1274_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1274_price" id="ART1274_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td><label class="font_small" >(adjust for CRCL) </label></td>
                                    <td colspan="2"><input name="ART1274_dose" id="ART1274_dose" type="radio" value="150 mg" />
                                        150 mg</td>
                                    <td><input name="ART1274_frequency" id="ART1274_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1407_meds" type="radio" value="ART1407" />
                                        zidovudine (AZT)</td>
                                    <td colspan="2"><div align="left">
                                            <input name="ART1407_dose" id="ART1407_dose" type="radio" value="300 mg" />
                                            300 mg</div></td>
                                    <td><input name="ART1407_frequency" id="ART1407_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td><input name="ART1407_duration" id="ART1407_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1407_prescribed" id="ART1407_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1407_dispensed" id="ART1407_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1407_price" id="ART1407_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td>(adjust for CRCL)</td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1374_meds" type="radio" value="ART1374" />
                                        Stavudine (D4T, Zerit)</td>
                                    <td colspan="2"><div align="left">
                                            <input name="nrti_dose" id="ART1374_dose" type="radio" value="300 mg" />
                                            300 mg</div></td>
                                    <td><input name="ART1374_frequency" id="ART1374_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td><input name="ART1374_duration" id="ART1374_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1374_prescribed" id="ART1374_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1374_dispensed" id="ART1374_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1374_price" id="ART1374_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td>(adjust for weight & CRCL)</td>
                                    <td colspan="2">
                                        <input name="ART1374_dose" id="ART1374_dose" type="radio" value="20 mg" />
                                        20 mg</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1381_meds" type="radio" value="ART1381" />
                                        Tenofovir (TDF, Viread)</td>
                                    <td colspan="2"><input name="ART1381_dose" id="ART1381_dose" type="radio" value="300 mg" />
                                        300 mg</td>
                                    <td><input name="ART1381_frequency" id="ART1381_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1381_duration" id="ART1381_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1381_prescribed" id="ART1381_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1381_dispensed" id="ART1381_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1381_price" id="ART1381_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td><label class="font_small">(adjust for CRCL) </label></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1101_meds" type="radio" value="ART1101" />
                                        Abacavir (ABC, Ziagen)  </td>
                                    <td colspan="2"><input name="ART1101_dose" id="ART1101_dose" type="radio" value="300 mg" />
                                        300 mg</td>
                                    <td><input name="ART1101_frequency" id="ART1101_frequency" type="radio" value="BD" />
                                        BD </td>
                                    <td><input name="ART1101_duration" id="ART1101_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1101_prescribed" id="ART1101_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1101_dispensed" id="ART1101_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1101_price" id="ART1101_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">Fixed Dose Combinations</label></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART1408_meds" type="radio" value="ART1408" />
                                        AZT/ 3TC</td>
                                    <td colspan="2"><input name="ART1408_dose" id="ART1408_dose" type="radio" value="300mg" />
                                        300mg</td>
                                    <td><input name="ART1408_frequency" id="ART1408_frequency" type="radio" value="1BD" />
                                        1BD</td>
                                    <td><input name="ART1408_duration" id="ART1408_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1408_prescribed" id="ART1408_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1408_dispensed" id="ART1408_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1408_price" id="ART1408_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><input name="ART1408_dose" id="ART1408_dose" type="radio" value="150mg" />
                                        150mg</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART1415_meds" type="radio" value="ART1415" />
                                        Tenofovir/ FTC (Truvada)</td>
                                    <td colspan="2"><input name="ART1415_dose" id="ART1415_dose" type="radio" value="300mg" />
                                        300mg</td>
                                    <td><input name="ART1415_frequency" id="ART1415_frequency" type="radio" value="1OD" />
                                        1OD</td>
                                    <td><input name="ART1415_duration" id="ART1415_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1415_prescribed" id="ART1415_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1415_dispensed" id="ART1415_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1415_price" id="ART1415_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><input name="ART1415_dose" id="ART1415_dose" type="radio" value="200mg" />
                                        200mg </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART1417_meds" type="radio" value="ART1417" />
                                        D4T 30/3TC</td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1417_frequency" id="ART1417_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1417_duration" id="ART1417_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1417_prescribed" id="ART1417_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1417_dispensed" id="ART1417_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1417_price" id="ART1417_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1417_frequency" id="ART1417_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART1418_meds" type="radio" value="ART1418" />
                                        D4T 30/3TC/NVP</td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1418_frequency" id="ART1418_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1418_duration" id="ART1418_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1418_prescribed" id="ART1418_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1418_dispensed" id="ART1418_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1418_price" id="ART1418_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1418_frequency" id="ART1418_frequency" type="radio" value="BD" />
                                        BD </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">NNRTIs</label></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART03_meds" id="ART1203_meds" type="radio" value="ART1203" />
                                        Efavirenz (Stocrin)</td>
                                    <td colspan="2"><input name="ART1203_dose" id="ART1203_dose" type="radio" value="600 mg" />
                                        600 mg</td>
                                    <td><input name="ART1203_frequency" id="ART1203_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1203_duration" id="ART1203_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1203_prescribed" id="ART1203_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1203_dispensed" id="ART1203_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1203_price" id="ART1203_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART03_meds" id="ART1310_meds" type="radio" value="ART1310" />
                                        Nevirapine</td>
                                    <td colspan="2"><input name="ART1310_dose" id="ART1310_dose" type="radio" value="200 mg" />
                                        200 mg</td>
                                    <td><input name="ART1310_frequency" id="ART1310_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1310_duration" id="ART1310_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1310_prescribed" id="ART1310_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1310_dispensed" id="ART1310_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1310_price" id="nnrti_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1310_frequency" id="ART1310_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold"> PIs </label></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART04_meds" id="ART1281_meds" type="radio" value="ART1281" />
                                        Lopinavir/r (Aluvia)</td>
                                    <td colspan="2"><input name="ART1281_dose" id="ART1281_dose" type="radio" value="200mg" />
                                        200mg</td>
                                    <td><input name="ART1281_frequency" id="ART1281_frequency" type="radio" value="2 Tabs BD" />
                                        2 Tabs BD</td>
                                    <td><input name="ART1281_duration" id="ART1281_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1281_prescribed" id="ART1281_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1281_dispensed" id="ART1281_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1281_price" id="ART1281_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><input  name="ART1281_dose" id="ART1281_dose" type="radio" value="50mg" />
                                        50mg</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td colspan="4"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td colspan="4">&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <label class="font_bold">Other ARV (specify): </label>
                                        <input name="other" id="other" type="text" size="45" maxlength="45" /></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold" >2. OI Prophylaxis and Treatment </label>                                        </td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART05_meds" id="ART1416_meds" type="radio" value="ART1416" />
                                        TMP/SMX</td>
                                    <td width="57"><input name="ART1416_dose" id="ART1416_dose" type="radio" value="DS" />
                                        DS</td>
                                    <td width="55"># of pills</td>
                                    <td><input name="ART1416_frequency" id="ART1416_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1416_duration" id="ART1416_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1416_prescribed" id="ART1416_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1416_dispensed" id="ART1416_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1416_price" id="ART1416_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input name="ART1416_dose" id="ART1416_dose" type="radio" value="SS" />
                                        SS</td>
                                    <td><div align="center">
                                            <input name="ART1416_pills" id="ART1416_pills" type="text" size="4" maxlength="4"  />
                                        </div></td>
                                    <td><input name="ART1416_frequency" id="ART1416_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1416_frequency" id="ART1416_frequency" type="radio" value="TDS" />
                                        TDS</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2">&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART05_meds" id="ART1223_meds" type="radio" value="ART1223" />
                                        Fluconazole</td>
                                    <td colspan="2"><input name="ART1223_dose" id="ART1223_dose" type="radio" value="200 mg" />
                                        200 mg</td>
                                    <td><input name="ART1223_frequency" id="ART1223_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1223_duration" id="ART1223_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1223_prescribed" id="ART1223_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1223_dispensed" id="oi_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1223_price" id="oi_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><input name="ART1223_dose" id="ART1223_dose" type="radio" value="400 mg" />
                                        400 mg</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td colspan="4"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td colspan="4">&nbsp;</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <input name="ART05_meds" id="ART_meds" type="radio" value="ART" />
                                        Other
                                        <input name="ART_describe" id="ART_describe" type="text" size="45" maxlength="45"  /></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="2"></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td colspan="2"></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART06_meds" id="ART1301_meds" type="radio" value="ART1301" />
                                        Multivitamin</td>
                                    <td colspan="2"></td>
                                    <td><input name="ART1301_frequency" id="ART1301_frequency" type="radio" value="1OD" />
                                        1 OD</td>
                                    <td><input name="ART1301_duration" id="ART1301_duration" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1301_prescribed" id="ART1301_prescribed" type="text" size="4" maxlength="4"  /></td>
                                    <td><input name="ART1301_dispensed" id="ART1301_dispensed" type="text" size="4" maxlength="4" readonly="true" /></td>
                                    <td><input name="ART1301_price" id="ART1301_price" type="text" size="4" maxlength="4" readonly="true" /></td>
                                </tr>
                            </table>
                      </div>
                        

                    </div>
                    <div class="adult_section_3_right">
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section adult_section_4" >
                    <div id="docbody"> </div>
                </div>
                <div class="spacer"></div>
                <div class="section adult_section_5">
                    <div class="spacer"></div>
                    <div class="adult_section_5_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />

                            <input id="Reset" type="reset" value="Reset" />
                        </div>
                    </div>
                    <div class="adult_section_5_right">

                    </div>
                </div>
            </form>
            <div id="docbody"> </div>
        </div>
    </div>
</div>


