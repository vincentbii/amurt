var ccareStore = new Ext.data.JsonStore({
    url: 'ccare.php',
    root: 'ccare_list',
    //        totalProperty: 'total',
    fields: ['patient_enrollment_no', 'patient_name', 'weight', 'age', 'arv_type', 'arv_provider']
});


var filters = new Ext.ux.grid.GridFilters({
    autoReload: false, //don&#39;t reload automatically
    local: true, //only filter locally
    filters: [{
        type: 'numeric',
        width: 100,
        dataIndex: 'patient_enrollment_no'
    }, {
        type: 'string',
        width: 200,
        dataIndex: 'patient_name'
    //disabled: true
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'weight'
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'age'
    }, {
        type: 'string',
        width: 100,
        dataIndex: 'arv_type'
    }, {
        type: 'string',
        width: 100,
        dataIndex: 'arv_provider'
    }]
});


var createColModel = function (finish, start) {

    var columns = [{
        dataIndex: 'patient_enrollment_no',
        width: 100,
        header: 'Patient Number',
        // instead of specifying filter config just specify filterable=true
        // to use store's field's type property (if type property not
        // explicitly specified in store config it will be 'auto' which
        // GridFilters will assume to be 'StringFilter'
        filterable: true,
        filter: {
            type: 'numeric'
        }
    }, {
        dataIndex: 'patient_name',
        width: 200,
        header: 'Patient Name',
        //id: 'company',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'weight',
        width: 80,
        header: 'Weight',
        filter: {
            type: 'numeric'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'age',
        width: 80,
        header: 'Age',
        filter: {
            type: 'numeric'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'arv_type',
        width: 100,
        header: 'ART Type',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'arv_provider',
        width: 100,
        header: 'ART Provider',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }];

    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
};

var careGrids = new Ext.grid.GridPanel({
    //border: false,
    title: 'No of Children Patients in Care',
    //        region: 'center',
    store: ccareStore,
    colModel: createColModel(6),
    loadMask: true,
    autoHeight: true,
    width: 670,
    plugins: [filters],

    listeners: {
        render: {
            fn: function(){
                ccareStore.load({
                    params: {
                        start: 0,
                        limit: 20
                    }
                });
            //g.getsSelectionModel().selectRow(0)
            //delay: 10
            }
        }
    },

    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("ccare_info").getForm().loadRecord(rec);
            }
        }
    }),

     tbar: [{
        text: 'Export To Excel',
        tooltip: 'Click to Export as Excel Format',
        handler: function () {
                    document.location.href='reports/exportChildrenPatients.php';
                },
        iconCls:'refresh'
    }],

    bbar: new Ext.PagingToolbar({
        store: ccareStore,
        pageSize: 20,
        plugins: [filters]
    })
});

var ccareGrid = {
    Xtype: 'panel',
    id: 'ccare-panel',
    //    title: 'Report: Patients Enrolled In ART',
    items:[careGrids]
};