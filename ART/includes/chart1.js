
Ext.chart.Chart.CHART_URL = '../include/Extjs/resources/charts.swf';

    var pieStore = new Ext.data.JsonStore({
        url: 'chart1.php',
        fields: ['pharmacy','count'],
        root: 'chart_list'
    });
    
    var pieChart = new Ext.Panel({
        width: 250,
        height: 250,
        items: {
            store: pieStore,
            xtype: 'piechart',
            dataField: 'count',
            categoryField: 'pharmacy',
            extraStyle:
            {
                legend:
                {
                    display: 'bottom',
                    padding: 5,
                    font:
                    {
                        family: 'Tahoma',
                        size: 13
                    }
                }
            }
        }
});

pieStore.load();