
    var colStore = new Ext.data.JsonStore({

        url: 'chart2.php',
        root: 'charts_list',
        method: 'POST',
        fields: [{
            name: 'testMonth',
            type:'string'
        }, {
            name: 'labtest',
            type: 'string'
        },{
            name: 'testCount',
            type: 'int'
        }]
        
    });
 
    var colChart = new Ext.Panel({
        width: 600,
        height: 300,
        items: {
            xtype: 'columnchart',
            store: colStore,
            yField: 'testMonth',
            url: '../include/Extjs/resources/charts.swf',
            xField: 'labtest',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Lab Tests'
            }),
            yAxis: new Ext.chart.NumericAxis({
                title: 'Months',
                labelRenderer: Ext.util.Format.numberRenderer('0,0')
            }),
            series: [{
                type: 'column',
                displayName: 'Months',
                yField: 'testMonth',
                style: {
                    image:'bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'No. of Tests',
                yField: 'testCount',
                style: {
                    color: 0x15428B
                }
            }],
            extraStyle: {
                xAxis: {
                    labelRotation: -90
                }
            }
        }
    });
colStore.load();

    