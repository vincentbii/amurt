// JavaScript Document

//============================== Date Fields in Contact $ Care Tracking  Form ==============================
Ext.onReady(function(){
    var appointmentPanel = new Ext.Panel({
//        applyTo:'cct_appointment',
        width:100,
        border:false,
        items:[{
            xtype:'datefield',
            id:'missed_appointment_date',
            name:'missed_appointment_date',
            width:100,
            allowBlank:false
        }]
    });
    appointmentPanel.render('cct_appointment');
//


    var contactPanel = new Ext.Panel({
//        applyTo:'cct_contact',
        width:100,
        border:false,
        items:[{
            xtype:'datefield',
            id:'last_contact_date',
            name:'last_contact_date',
            width:100,
            allowBlank:false
        }]
    });
    contactPanel.render('cct_contact');



    var ccfirstPanel = new Ext.Panel({
//        applyTo:'cct_first',
        width:100,
        border:false,
        items:[{
            xtype:'datefield',
            id:'first_attempt_date',
            name:'first_attempt_date',
            width:100,
            allowBlank:false
        }]
    });
ccfirstPanel.render('cct_first');


    var cctSecondPanel = new Ext.Panel({
//        applyTo:'cct_second',
        width:100,
        border:false,
        items:[{
            xtype:'datefield',
            id:'second_attempt_date',
            name:'second_attempt_date',
            width:100,
            allowBlank:false
        }]
    });
    cctSecondPanel.render('cct_second');
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cct_third',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'third_attempt_date',
//            name:'third_attempt_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cct_fourth',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'fourth_attempt_date',
//            name:'fourth_attempt_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cct_last',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'last_attempt_date',
//            name:'last_attempt_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cct_arv',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'arv_end_date',
//            name:'arv_end_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cct_death',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'death_date',
//            name:'death_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cct_care',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'care_end_date',
//            name:'care_end_date',
//            width:100,
//            allowBlank:false
//        }]
//    });


//========================================== Date fields in laboratory Order results Form ============================================
//
//    Ext.onReady(function(){
var labpanel =new Ext.Panel({
//        applyTo:'lor_preclinic',
        id:'lo_preclinicdt',
        width:100,
        boarder:false,
        items:[{
            xtype:'datefield',
            id:'lab_date',
            name:'lab_date',
            format: 'Y/m/d',
            minValue: '01/01/06',
            disabledDays: [0, 6],
            width:100,
            allowBlank:false
        }]
    });
    labpanel.render('lor_preclinic');
//    });

//========================================== Date fields in Pediatric Pharmacy Order Form ============================================


//    var ppoPanel = new Ext.Panel({
////        applyTo:'ppo_dob',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'dob',
//            name:'dob',
//            width:100,
//            allowBlank:false
//        }]
//    });
//     ppoPanel.render('ppo_dob');
//
//
////================================= Date Fields in ART Follow-up Form =============================================
//
//    var formPanel = new Ext.Panel({
//        applyTo:'arv_vdate',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'art_vdate',
//            name:'art_vdate',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'arv_sdate',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'tb_therapy_start_date',
//            name:'tb_therapy_start_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'arv_edate',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'tb_therapy_end_date',
//            name:'tb_therapy_end_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'arv_adate',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'appointment_date',
//            name:'appointment_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'arv_rx_date',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'adherence_rx_date',
//            name:'adherence_rx_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
////================================= Date Fields in HIV Care Enrollment Form =============================================
//
//    var formPanel = new Ext.Panel({
//        applyTo:'hce_vdate',
//        width: 85,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'visit_date',
//            name:'visit_date',
//            width:85,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'hce_bdate',
//        width: 90,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'art_dob',
//            name:'art_dob',
//            width: 90,
//            allowBlank:false
//        }]
//    });
//
////============================== Date Fields in Family Information  Form ==============================
//
//    var formPanel = new Ext.Panel({
//        applyTo:'family_vdate',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'visit_date',
//            name:'visit_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'family_bdate1',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'relative_1_dob',
//            name:'relative_1_dob',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'family_bdate2',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'relative_2_dob',
//            name:'relative_2_dob',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'family_bdate3',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'relative_3_dob',
//            name:'relative_3_dob',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'family_bdate4',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'relative_4_dob',
//            name:'relative_4_dob',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'family_bdate5',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'relative_5_dob',
//            name:'relative_5_dob',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
////========================================== Date fields in Adult Pharmacy Order Form ============================================
//
//    var formPanel = new Ext.Panel({
//        applyTo:'adult_dob',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'age',
//            name:'age',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
////============================================ Date Fields in Initial Evaluation Form ==========================================
//
    var formPanel = new Ext.Panel({
        applyTo:'initial_vdate',
        width:100,
        boarder:false,
        items:[{
            xtype:'datefield',
            id:'visit_date',
            name:'visit_date',
            width:100,
            allowBlank:false
        }]
    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_ddate',
//        width:100,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'hiv_diagnosis_date',
//            name:'hiv_diagnosis_date',
//            width:100,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_ltdate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'current_longterm_date',
//            name:'current_longterm_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_ldate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'medical_liver_date',
//            name:'medical_liver_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_kdate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'medical_kidney_date',
//            name:'medical_kidney_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_adate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'medical_anaemia_date',
//            name:'medical_anaemia_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_drdate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'medical_drug_date',
//            name:'medical_drug_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_modate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'medical_other_date',
//            name:'medical_other_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_moodate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'medical_other2_date',
//            name:'medical_other2_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'initial_tbdate',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'tb_start_date',
//            name:'tb_start_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//
////============================== Date Fields in HIV Care Patient Profile  Form ==============================
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cpp_diagnosis',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'hiv_diagnosis_date',
//            name:'hiv_diagnosis_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cpp_enrollment',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'enrollment_date',
//            name:'enrollment_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cpp_care',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'care_end_date',
//            name:'care_end_date',
//            width:95,
//            allowBlank:false
//        }]
//    });
//
//    var formPanel = new Ext.Panel({
//        applyTo:'cpp_dob',
//        width:95,
//        boarder:false,
//        items:[{
//            xtype:'datefield',
//            id:'dob',
//            name:'dob',
//            width:95,
//            allowBlank:false
//        }]
//    });
});