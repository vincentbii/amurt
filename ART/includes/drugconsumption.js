var fm = Ext.form;

var drugReader = new Ext.data.JsonReader(
{
    url: 'drugList.php',
    root: 'drug_list'
//        fields: ['enrollment_no', 'drug','qty_prescribed','qty_dispensed','unit_price','total_price']
},
[{
    name: 'enrollment_no',
    type:'numeric'
},
{
    name: 'drug',
    type: 'string'
},
{
    name: 'qty_prescribed',
    type: 'string'
},
{
    name: 'qty_dispensed',
    type: 'string'
},
{
    name: 'unit_price',
    type: 'numeric'
},
{
    name: 'total_price',
    type: 'numeric'
}
]);


var filters = new Ext.ux.grid.GridFilters({
    autoReload: false, //don&#39;t reload automatically
    local: true, //only filter locally
    filters: [{
        type: 'numeric',
        width: 80,
        dataIndex: 'enrollment_no'
    }, {
        type: 'string',
        width: 150,
        dataIndex: 'drug'
    //disabled: true
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'qty_prescribed'
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'qty_dispensed'
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'unit_price'
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'total_price'
    }]
});


var createColModel = function (finish, start) {

    var columns = [{
            dataIndex: 'enrollment_no',
            header: 'Patient Number',
            filterable: true,
            editor: new fm.TextField({
                allowBlank: false
            }),
           filter: {type: 'numeric'}
        }, {
            dataIndex: 'drug',
            header: 'Drug Name',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
        }, {
            dataIndex: 'qty_prescribed',
            header: 'Amount Prescribed',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
        }, {
            dataIndex: 'qty_dispensed',
            header: 'Amount Dispensed',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
            }, {
            dataIndex: 'unit_price',
            header: 'Cost per Unit',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
            }, {
            dataIndex: 'total_price',
            header: 'Total Charges',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'numeric'
            }
        }];

    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
};

 var drugStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'drugList.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
//        task: "readRates"
    },//This
    reader:drugReader,
    sortInfo: {
        field:'enrollment_no',
        direction:'ASC'
    },
    groupField:'enrollment_no'
});

var drugsGrid = new Ext.grid.GridPanel({
    //border: false,
    title: 'ARV Drugs Consumption',
    //        region: 'center',
    store: drugStore,
    colModel: createColModel(6),
    loadMask: true,
    autoHeight: true,
    width: 700,
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    plugins: [filters],

    listeners: {
        render: {
            fn: function(){
                drugStore.load({
                    params: {
                        start: 0,
                        limit: 20
                    }
                });
            //g.getsSelectionModel().selectRow(0)
            //delay: 10
            }
        }
    },

    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("drugCon_info").getForm().loadRecord(rec);
            }
        }
    }),
    
    tbar: [{
        text: 'Export To Excel',
        tooltip: 'Click to Export as Excel Format',
        handler: function () {
                    document.location.href='reports/exportARVdrugs.php';
                },
        iconCls:'refresh'
    }],

    bbar: new Ext.PagingToolbar({
        store: drugStore,
        pageSize: 20,
        plugins: [filters]
    })
});

var drugGrid = {
    Xtype: 'panel',
    id: 'drugCon-panel',
    //    title: 'Report: Patients Enrolled In ART',
    items:[drugsGrid]
};