<div id="enrollment" class="main_container">
    <div align="center">
        <div class="main_container_form">
            <form class="" name="hiv_care" action="hiv_care.php" method="post" onSubmit="">

                <div class="section enrollment_section_1">
                    <div class="spacer"></div>
                    <div class="enrollment_section_1_left">  
                      <div class="section_row">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="9"> <label class="font_bold">1. Patient name:</label>
      <input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" readonly="true" />    </td>
    </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
    <td width="12%">&nbsp;</td>
  </tr>
  <tr>
    <td> <label class="font_bold">3. ID:</label></td>
    <td width="11%"><input name="country_no" id="country_no" type="text" size="3" maxlength="3" readonly="true" /></td>
    <td width="1%">&nbsp;</td>
    <td width="9%"><input name="lptf_no" id="lptf_no" type="text" size="3" maxlength="3" readonly="true" /></td>
    <td width="2%">&nbsp;</td>
    <td width="11%"><input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" readonly="true" /></td>
    <td width="1%">&nbsp;</td>
    <td colspan="2"><input name="patient_enrollment_no" id="patient_enrollment_no" type="text" size="7" maxlength="7" readonly="true" /></td>
    </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="13%">&nbsp;</td>
    <td>Country</td>
    <td>&nbsp;</td>
    <td>LPTF#</td>
    <td>&nbsp;</td>
    <td><label>Satellite#</label>
      <label class="font_bold" style="padding-right: 4px;"></label></td>
    <td>&nbsp;</td>
    <td colspan="2"><label>Patient Enrollment#</label></td>
    </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"> <label class="font_bold">6. Age:</label>
      <input name="age_is" id="extact" type="text" size="4" maxlength="4" readonly="true" /></td>
    <td colspan="7"><label class="font_bold">7. Date of birth:</label>      <span class="font_small">(MM/DD/YY)
      <input name="art_dob" id="art_dob" type="text" size="13" maxlength="10" readonly="true" />
      </span></td>
    </tr>
  <tr>
    <td colspan="8">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

                         
                      </div>

                    </div>
                    <div class="enrollment_section_1_right"> 
                        <div class="spacer"></div>
                        <div class="section_row">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3"><label class="font_bold">2. Visit Date</label><span class="font_small"> (DD/MM/YY)</span>:
      <?php
                            $fullDate->genDayDDL("hce_vdateday");
                            $fullDate->genMonthDDL("hce_vdatemonth");
                            $fullDate->genYearDDL("hce_vdateyear");
                            ?>
    </td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td width="16%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><label class="font_bold">4. Existing Hosp/Clinic #:</label>
      <input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" readonly="true" />
    </td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="17%"><label class="font_bold">5. Sex:</label></td>
    <td colspan="2"><input name="sex" id="male" type="radio" value="male" />
Male
&nbsp;&nbsp;&nbsp;
  <input name="sex" id="female" type="radio" value="female" />
Female</td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>          
</div>  
                    </div>
                </div> 
                <div class="spacer"></div>
                <div class="section enrollment_section_2">
                    <div class="spacer"></div>
                    <div class="enrollment_section_2_left">
                        <div class=" section_row">
                            <label for="contact_info" class="font_bold">8. Contact Information:  </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="chief_council"> Chief / Location Council:</label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="contact_location" id="chief_council" type="text" size="45" maxlength="45" />
                        </div>
                        <div class="spacer"> </div>
                        <div class="section_row">
                            <label for="town_name"> Village / Town / City Name:</label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="contact_village" id="town_name" type="text" size="42" maxlength="42" />
                        </div>
                        <div class="spacer"> </div>
                        <div class="section_row">
                            <label for="district"> District:</label>
                            <label class="font_bold" style="padding-right: 97px;"></label>
                            <input name="contact_district" id="district" type="text" size="42" maxlength="42" />
                        </div>
                        <div class="spacer"> </div>
                        <div class="section_row">
                            <label for="province"> Province:</label>
                            <label class="font_bold" style="padding-right: 57px;"></label>
                            <input name="contact_province" id="province" type="text" size="50" maxlength="50" />
                        </div>
                        <div class="spacer"> </div>
                        <div class="section_row">
                            <label for="address"> Residence / Address / P.O. Box:</label>
                            <label style="padding-right: 2px;"></label>
                            <input name="contact_address" id="address" type="text" size="37" maxlength="37" />
                        </div>
                        <div class="spacer"> </div>
                        <div class="section_row">
                            <label for="phone_number"> Phone Number: </label>
                            <label style="padding-right: 90px;"></label>
                            <input name="contact_phone_no" id="phone_number" type="text" size="30" maxlength="30" />
                        </div>
                        <div class="spacer"> </div>
                        <div class="section_row">
                            <label for="emergency_name" class="font_bold"> 9. Emergency Contact: </label>
                            <label  style="padding-right: 80px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="emergency_contact_name"> Name: </label>
                            <label  style="padding-right: 110px;"></label>
                            <input name="emergency_contact_name" id="emergency_name" type="text" size="40" maxlength="40" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="relationship"> Relationship:</label>
                            <label class="" style="padding-right: 80px;"></label>
                            <input name="emergency_contact_rship" id="relationship" type="text" size="40" maxlength="35" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="phone_number"> Phone Number:</label>
                            <label class="" style="padding-right: 67px;"></label>
                            <input name="emergency_contact_phone_no" id="phone_number" type="text" size="40" maxlength="14" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="emergency_address"> Address:</label>
                            <label class="" style="padding-right: 1px;"></label>
                            <input name="emergency_contact_address" id="emergency_address" type="text" size="65" maxlength="65" />
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="emergency_address"> 10. Does contact person know your HIV status? </label>
                            <label class="font_bold" style="padding-right: 12px;"></label>
                            <input name="known_hiv_status" id="yes" type="radio" value="yes" /> Yes
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="known_hiv_status" id="no" type="radio" value="no" /> No
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="emergency_address"> 11. If No, can we discuss your status with this contact person? </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="discuss_status" id="" type="radio" value="yes" /> Yes
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="discuss_status" id="" type="radio" value="no" /> No
                        </div>

                    </div>
                    <div class="enrollment_section_2_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 12. Marital Status:</label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="marital_status" id="single" type="radio" value="single" /> Single
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="marital_status" id="married" type="radio" value="married" /> Married
                            <label class="font_bold" style="padding-right: 15px;"></label>
                            <input name="marital_status" id="widowed" type="radio" value="widowed" /> Widowed
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="marital_status" id="divorced" type="radio" value="divorced" /> Divorced
                            <label class="font_bold" style="padding-right: 10px;"></label>
                            <input name="marital_status" id="not_applicable" type="radio" value="not_applicable" /> Not Applicable
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 13. Educational Level:</label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="educational_level" id="none" type="radio" value="none" /> None
                            <label class="font_bold" style="padding-right: 28px;"></label>
                            <input name="educational_level" id="secondary" type="radio" value="secondary" /> Secondary
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="educational_level" id="primary" type="radio" value="primary" /> Primary
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="educational_level" id="post_secondary" type="radio" value="post_secondary" />  Post-Secondary
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="educational_level" id="other" type="radio" value="other" /> Other
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 14. Literacy: </label>
                            <label class="font_bold" style="padding-right: 80px;"></label>
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="literacy" id="literate" type="radio" value="literate" /> Literate
                            <label class="font_bold" style="padding-right: 25px;"></label>
                            <input name="literacy" id="illiterate" type="radio" value="illiterate" /> Illiterate
                        </div>

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section enrollment_section_3">
                    <div class="enrollment_section_3">
                        <div class="enrollment_section_3_left">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label> 15. Have you ever recievied care for HIV/AIDS? </label>
                                <label class="font_bold" style="padding-right: 3px;"></label>
                                <input name="hiv_care" id="yes" type="radio" value="yes" /> Yes
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="hiv_care" id="no" type="radio" value="no" /> No
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <label> If yes, specify type:</label>
                                <label class="font_bold" style="padding-right: 8px;"></label>
                                <input name="hiv_care_type" id="home_based" type="radio" value="home_based"
                                       /> Home Based Care
                                <label class="font_bold" style="padding-right: 7px;"></label>
                                <input name="hiv_care_type" id="vct" type="radio" value="vct" > VCT
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="hiv_care_type" id="tb_ti_clinic" type="radio" value="tb_ti_clinic" > TB/TI Clinic
                                <label class="font_bold" style="padding-right: 29px;"></label>
                                <input name="hiv_care_type" id="pmtct" type="radio" value="pmtct" /> PMTCT
                                <label class="font_bold" style="padding-right: 59px;"></label>
                                <input name="hiv_care_type" id="inpatient" type="radio" value="inpatient" /> In-Patient Care

                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="hiv_care_type" id="other" type="radio" value="Other" /> Other:
                                <label class="font_bold" style="padding-right: 12px;"></label>
                                <input name="other_hiv_care" id="other" size="35" type="text" maxlength="35" />

                            </div>


                        </div>
                        <div class="enrollment_section_3_right"> 
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label> 16. Have you ever been on ARTs? </label>
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="arv" id="yes" type="radio" value="yes" /> Yes
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="arv" id="no" type="radio" value="no" /> No
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <label>If yes, specify sponsor: </label>
                                <label class="font_bold" style="padding-right: 12px;"></label>
                                <input name="arv_sponsor" id="self_pay" type="radio" value="self_pay"  /> Self-pay

                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_sponsor" id="gov_sponsered" type="radio" value="gov_sponsored" /> Govt. Sponsored
                                <label class="font_bold" style="padding-right: 23px;"></label>
                                <input name="arv_sponsor" id="usg" type="radio" value="usg" /> USG Sponsered Program
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_sponsor" id="mission_based" type="radio" value="mission_based" /> Mission/Faith Based
                                <label class="font_bold" style="padding-right: 8px;"></label>
                                <input name="arv_sponsor" id="this_facility" type="radio" value="this_facility" /> This Facility
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_sponsor" id="other" type="radio" value="Other" /> Other:
                                <label class="font_bold" style="padding-right: 12px;"></label>
                                <input name="other_arv_sponsor" id="other" size="35" type="text" maxlength="35" />
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <label> 16a. If yes, can we have a copy of your medical records today? </label>
                            </div>

                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="medical_records" id="yes" type="radio" value="yes" /> Yes
                                <label class="font_bold" style="padding-right: 12px;"></label>
                                <input name="medical_records" id="no" type="radio" value="no" /> No
                            </div>


                        </div>

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section enrollment_section_4">
                    <div class="enrollment_section_4_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 17. Employment Status: </label>
                            <label class="font_bold" style="padding-right: 12px;"></label>
                            <input name="employment_status" id="unemployeed" type="radio" value="unemployeed" /> Unemployeed
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="employment_status" id="employeed" type="radio" value="employeed" /> Employeed
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="" style="padding-right: 140px;"></label>
                            <input name="employment_status" id="retired" type="radio" value="retired" /> Retired
                            <label class="font_bold" style="padding-right: 38px;"></label>
                            <input name="employment_status" id="not_applicable" type="radio" value="not_applicable" /> Not Applicable
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> Occupation </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="occupation" id="occupation" size="40" type="text" maxlength="40" />
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="">18. Monthly Income: </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="monthly_income" id="income" size="10" type="text" maxlength="10" />
                            <label class="font_bold" style="padding-right: 80px;"></label>
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>19. How many children do you have? </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="no_of_children" id="children" size="2" type="text" maxlength="2" />
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>20. How many people are in your Household?</label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="pple_in_house" id="household" size="2" type="text" maxlength="2" />
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>21. Distance Travelled from residence to point of service </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="distance_travelled" id="kilometers" size="4" type="text" maxlength="4" />
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <label> Kilometers</label>
                        </div>

                    </div>
                    <div class="enrollment_section_4_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>22. How Long did it take you to get here today? </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                        </div>

                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="time_taken" id="min" size="15" type="text" maxlength="15" />
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <label> Minutes/Hours/Days </label>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section enrollment_section_5">
                    <div class="enrollment_section_5_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> 24.  HIV Status:   </label>
                            <label class="font_bold" style="padding-right: 12px;"></label>
                            <input name="hiv_status" id="positive" type="radio" value="positive" /> Positive
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_status" id="negative" type="radio" value="negative" /> Negative
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_status" id="unknown" type="radio" value="unknown" /> Unknown
                            <label class="font_bold" style="padding-right: 150px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> 24b.  Childern born to HIV positive Mother </label>
                            <label class="font_bold" style="padding-right: 12px;"></label>
                            <input name="child_positive_mother" id="born" type="text" maxlength="5" size="5">
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> 25.  HIV disclosure Status: </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_disclosure_status" id="none" type="radio" value="none" > None
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_disclosure_status" id="spouse" type="radio" value="spouse" > Spouse
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_disclosure_status" id="family" type="radio" value="family"> Family
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_disclosure_status" id="friend" type="radio" value="friend" > Friend (s)
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_disclosure_status" id="spiritual_leader" type="radio" value="spiritual_leader"> Spiritual Leader
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_disclosure_status" id="other" type="radio" value="other" > Other
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>26.  How many Member(s) of your household do you know have done an HIV test? </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="house_hiv_test" id="household" size="4" type="text" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>26b.  How many Member(s) of your household do you know have tested Positive for HIV test? </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="house_test_positive" id="household" size="4" type="text" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>27. How many Member(s) of your household do you know are deceased from HIV? </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="house_hiv_deceased" id="household" size="4" type="text" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>28.  Membership in HIV Support group: </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_support_membership" id="yes" type="radio" value="yes" >Yes
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="hiv_support_membership" id="no" type="radio" value="no" >No
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <label>If yes, Specify </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <input name="membership_specify" id="support" size="50" type="text" maxlength="50" />
                        </div>
                    </div>
                    <div class="enrollment_section_5_right">
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section enrollment_section_6">
                    <div class="enrollment_section_6_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Assessment/Notes:</label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section enrollment_section_6 enrollment_section_6_row">
                            <div class="enrollment_section_6_boarded">
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> 29. Patient Referred From: </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="referred_from" id="vct" type="radio" value="vct" > VCT
                                    <label class="font_bold" style="padding-right: 6px;"></label>
                                    <input name="referred_from" id="outpatient" type="radio" value="outpatient"> Outpatient
                                    <label class="font_bold" style="padding-right: 6px;"></label>
                                    <input name="referred_from" id="other_source" type="radio" value="other_source" > Other Source
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="referred_from" id="pmtct" type="radio" value="pmtct" > PMTCT
                                    <label class="font_bold" style="padding-right: 6px;"></label>
                                    <input name="referred_from" id="tb_outpatient" type="radio" value="tb_outpatient"> TB Outpatient
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="referred_from" id="inpatient_ward" type="radio" value="inpatient_ward" > Inpatient Ward
                                    <label class="font_bold" style="padding-right: 6px;"></label>
                                    <input name="referred_from" id="other_facility" type="radio" value="other_facility"> Other Facility
                                </div>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Patient Barriers to Successful HIV Care:</label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="barriers" id="financial" type="radio" value="financial"  > Financial
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="barriers" id="fear" type="radio" value="fear_of_disclosure"> Fear of Disclosure
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="barriers" id="transport" type="radio" value="transport" > Transport
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="barriers" id="social_support" type="radio" value="lack_of_social_support"> Lack of Social Support
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="barriers" id="stigma" type="radio" value="stigma_discemination" > Stigma/Discremination
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="barriers" id="none" type="radio" value="none"> None
                            </div>
                        </div>
                    </div>
                    <div class="enrollment_section_6_right">
                        <div class=" enrollment_section_6_spacer"></div>
                        <div class=" enrollment_section_6_position">
                            <div class=" enrollment_section_6_spacer2"></div>
                            <div class="section_row">
                                <label> Satellite </label>
                                <label class="font_bold" style="padding-right: 6px;"></label>
                                <input name="satellite_no2" id="satellite_no2" size="35" type="text" maxlength="35" readonly="true" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="spacer"></div>
                <div class="section enrollment_section_7">
                    <div class="enrollment_section_7_left">
                        <div class="spacer"></div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />

                            <input id="Reset" type="reset" value="Reset" />
                        </div>
                    </div>
                    <div class="enrollment_section_7_right">
                        <div class="spacer"></div>


                    </div>


                </div>

            </form>
        </div>
    </div>
</div>
