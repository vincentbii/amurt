<div id="family" class="main_container"> <!-- Begining of Main Container -->
    <div align="center">
        <div class="main_container_form"><!-- Begining of Main Container Form -->
            <form class="" method="post" action="family_info.php" onSubmit="" name="family_info">
                <div class="section family_section_1"><!-- Begining of Section One -->
                    <div class="spacer"></div>
                    <div class="family_section_1_left">  <!--Begining of Section one left -->
                        <div class="section_row">
                            <label  class="font_bold"> 1. Patient name:</label>&nbsp;
                            <input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" readonly="true" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="id_1" class="font_bold"> 3. ID: </label>&nbsp;

                            <input name="country_no" id="country_no" type="text" size="3" maxlength="3" readonly="true"/>&nbsp;&nbsp;&nbsp;&#8211;

                            <input name="lptf_no" id="lptf_no" type="text" size="3" maxlength="3" readonly="true" />&nbsp;&nbsp;&nbsp;&#8211;

                            <input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" readonly="true"/>&nbsp;&nbsp;&nbsp;&#8211;

                            <input name="patient_enrollment_no" id="patient_enrollment_no" type="text" size="7" maxlength="7" readonly="true"/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Country</label>
                            &nbsp;&nbsp;<label> LPTF#</label>
                            &nbsp;&nbsp;&nbsp;<label> Satellite#</label>
                            &nbsp;&nbsp;<label>Patient Enrollment#</label>
                        </div>
                    </div> <!-- End of Section One Left -->
                    <div class="family_section_1_right"> <!-- Begining of Section one Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 2. Visit Date<span class="font_small"> (MM/DD/YY)</span></label>
                            &nbsp;

                            <?php
                            $fullDate->genDayDDL("vdateday");
                            $fullDate->genMonthDDL("vdatemonth");
                            $fullDate->genYearDDL("vdateyear");
                            ?>

                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label  class="font_bold"> 4. Existing Hosp/Clinic #:</label>&nbsp;
                            <input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" readonly="true"/>         	</div>

                    </div><!-- End of Section one Right -->
                </div><!-- End of Section One -->
                <div class="spacer"></div>
                <div class="section family_section_2"><!-- Begining of Section Two -->
                    <div class="spacer"></div>
                    <div class="family_section_2_left"><!-- Begining of Section Two Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 1. Relative's Name </label>
                            <label class="font_small"> First Name/ Last Name </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Relative's Age </label>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_1_names" id="relative_1_names" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            <?php
                            $fullDate->genDayDDL('rageday');
                            $fullDate->genMonthDDL("ragemonth");
                            $fullDate->genYearDDL("rageyear");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relationship to Client </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_1_rship" id="relative_1_rship" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <div class="section_row" style="border:1px solid #000000; min-height:100%; width:210px; float: right;">
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Relative's HIV Status </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="relative_1_hiv_status" id="relative_1_hiv_status" type="radio" value="positive" > Positive
                                    <input name="relative_1_hiv_status" id="relative_1_hiv_status" type="radio" value="negative"> Negative
                                    <input name="relative_1_hiv_status" id="relative_1_hiv_status" type="radio" value="unknown" > Unknown
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Is Relative Registered at this Clinic </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_1_registered" id="relative_1_registered" type="radio" value="yes"> Yes
                            <input name="relative_1_registered" id="relative_1_registered" type="radio" value="no" > No
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> File Number </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_1_file_no" id="relative_1_file_no" type="text" size="4" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>
                    </div><!-- End of Section Two Left -->
                    <div class=" family_section_2_right"><!-- Begining of Section Two Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relative's Gender </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_1_gender" id="relative_1_gender" type="radio" value="male" > Male
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="relative_1_gender" id="relative_1_gender" type="radio" value="female" > Female
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row" style=" border:1px solid #000000; min-height:100%; width:170px;">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HIV Care Status </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_1_hiv_care" id="relative_1_hiv_care" type="radio" value="on_art" > On ART
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_1_hiv_care" id="relative_1_hiv_care" type="radio" value="died" > Died
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_1_hiv_care" id="relative_1_hiv_care" type="radio" value="in_hiv_care"> In HIV Care
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_1_hiv_care" id="relative_1_hiv_care" type="radio" value="unknown" > Unknown
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_1_hiv_care" id="relative_1_hiv_care" type="radio" value="not_in_hiv_care"> Not in HIV Care
                            </div>
                        </div>

                    </div><!-- End of Section Two Right -->
                </div><!-- End of Section Two -->
                <div class="spacer"></div>
                <div class="section family_section_3"><!-- Begining of Section Three -->
                    <div class="spacer"></div>
                    <div class="family_section_3_left"><!-- Begining of Section Three Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 2. Relative's Name </label>
                            <label class="font_small"> First Name/ Last Name </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Relative's Age </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_2_names" id="relative_2_names" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <?php
                            $fullDate->genDayDDL("rage1day");
                            $fullDate->genMonthDDL("rage1month");
                            $fullDate->genYearDDL("rage1year");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relationship to Client </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_2_rship" id="relative_2_rship" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <div class="section_row" style="border:1px solid #000000; min-height:30px; width:210px; float: right;">
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Relative's HIV Status </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="relative_2_hiv_status" id="relative_2_hiv_status" type="radio" value="positive" > Positive
                                    <input name="relative_2_hiv_status" id="relative_2_hiv_status" type="radio" value="negative"> Negative
                                    <input name="relative_2_hiv_status" id="relative_2_hiv_status" type="radio" value="unknown"> Unknown
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Is Relative Registered at this Clinic </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_2_registered" id="relative_2_registered" type="radio" value="yes" > Yes
                            <input name="relative_2_registered" id="relative_2_registered" type="radio" value="no" > No
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> File Number </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_2_file_no" id="relative_2_file_no" type="text" size="4" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>
                    </div><!-- End of Section Three Left -->
                    <div class=" family_section_3_right"><!-- Begining of Section Three Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relative's Gender </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_2_gender" id="relative_2_gender" type="radio" value="male" > Male
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="relative_2_gender" id="relative_2_gender" type="radio" value="female" > Female
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row" style=" border:1px solid #000000; min-height:40px; width:170px;">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HIV Care Status </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_2_hiv_care" id="relative_2_hiv_care" type="radio" value="on_art" > On ART
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_2_hiv_care" id="relative_2_hiv_care" type="radio" value="died" > Died
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_2_hiv_care" id="relative_2_hiv_care" type="radio" value="in_hiv_care"> In HIV Care
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_2_hiv_care" id="relative_2_hiv_care" type="radio" value="unknown" > Unknown
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_2_hiv_care" id="relative_2_hiv_care" type="radio" value="not_in_hiv_care"> Not in HIV Care
                            </div>
                        </div>

                    </div><!-- End of Section Three Right -->
                </div><!-- End of Section Three -->
                <div class="spacer"></div>
                <div class="section family_section_4"><!-- Begining of Section Four -->
                    <div class="spacer"></div>
                    <div class="family_section_4_left"><!-- Begining of Section Four Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 3. Relative's Name </label>
                            <label class="font_small"> First Name/ Last Name </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Relative's Age </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_3_names" id="relative_3_names" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <?php
                            $fullDate->genDayDDL("rage2day");
                            $fullDate->genMonthDDL("rage2month");
                            $fullDate->genYearDDL("rage2year");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relationship to Client </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_3_rship" id="relative_3_rship" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <div class="section_row" style="border:1px solid #000000; min-height:30px; width:210px; float: right;">
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Relative's HIV Status </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="relative_3_hiv_status" id="relative_3_hiv_status" type="radio" value="positive" > Positive
                                    <input name="relative_3_hiv_status" id="relative_3_hiv_status" type="radio" value="negative"> Negative
                                    <input name="relative_3_hiv_status" id="relative_3_hiv_status" type="radio" value="unknown" > Unknown
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Is Relative Registered at this Clinic </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_3_registered" id="relative_3_registered" type="radio" value="yes" > Yes
                            <input name="relative_3_registered" id="relative_3_registered" type="radio" value="no" > No
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> File Number </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_3_file_no" id="relative_3_file_no" type="text" size="4" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>
                    </div><!-- End of Section Four Left -->
                    <div class=" family_section_4_right"><!-- Begining of Section Four Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relative's Gender </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_3_gender" id="relative_3_gender" type="radio" value="male" > Male
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="relative_3_gender" id="relative_3_gender" type="radio" value="female" > Female
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row" style=" border:1px solid #000000; min-height:40px; width:170px;">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HIV Care Status </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_3_hiv_care" id="relative_3_hiv_care" type="radio" value="on_art" > On ART
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_3_hiv_care" id="relative_3_hiv_care" type="radio" value="died" > Died
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_3_hiv_care" id="relative_3_hiv_care" type="radio" value="in_hiv_care"> In HIV Care
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_3_hiv_care" id="relative_3_hiv_care" type="radio" value="unknown" > Unknown
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_3_hiv_care" id="relative_3_hiv_care" type="radio" value="not_in_hiv_care"> Not in HIV Care
                            </div>
                        </div>

                    </div><!-- End of Section Four Right -->
                </div><!-- End of Section Four -->
                <div class="spacer"></div>
                <div class="section family_section_5"><!-- Begining of Section Five -->
                    <div class="spacer"></div>
                    <div class="family_section_5_left"><!-- Begining of Section Five Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 4. Relative's Name </label>
                            <label class="font_small"> First Name/ Last Name </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Relative's Age </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_4_names" id="relative_4_names" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <?php
                            $fullDate->genDayDDL("rage3day");
                            $fullDate->genMonthDDL("rage3month");
                            $fullDate->genYearDDL("rage3year");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relationship to Client </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_4_rship" id="relative_4_rship" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <div class="section_row" style="border:1px solid #000000; min-height:30px; width:210px; float: right;">
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Relative's HIV Status </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="relative_4_hiv_status" id="relative_4_hiv_status" type="radio" value="positive"> Positive
                                    <input name="relative_4_hiv_status" id="relative_4_hiv_status" type="radio" value="negative"> Negative
                                    <input name="relative_4_hiv_status" id="relative_4_hiv_status" type="radio" value="unknown" > Unknown
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Is Relative Registered at this Clinic </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_4_registered" id="relative_4_registered" type="radio" value="yes"> Yes
                            <input name="relative_4_registered" id="relative_4_registered" type="radio" value="no" > No
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> File Number </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_4_file_no" id="relative_4_file_no" type="text" size="4" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>
                    </div><!-- End of Section Five Left -->
                    <div class=" family_section_5_right"><!-- Begining of Section Five Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relative's Gender </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_4_gender" id="relative_4_gender" type="radio" value="male" > Male
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="relative_4_gender" id="relative_4_gender" type="radio" value="female" > Female
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row" style=" border:1px solid #000000; min-height:40px; width:170px;">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HIV Care Status </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_4_hiv_care" id="relative_4_hiv_care" type="radio" value="on_art" > On ART
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_4_hiv_care" id="relative_4_hiv_care" type="radio" value="died" > Died
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_4_hiv_care" id="relative_4_hiv_care" type="radio" value="in_hiv_care"> In HIV Care
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_4_hiv_care" id="relative_4_hiv_care" type="radio" value="unknown" > Unknown
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_4_hiv_care" id="relative_4_hiv_care" type="radio" value="not_in_hiv_care"> Not in HIV Care
                            </div>
                        </div>

                    </div><!-- End of Section Five Right -->
                </div><!-- End of Section Five -->
                <div class="spacer"></div>
                <div class="section family_section_6"><!-- Begining of Section Six -->
                    <div class="spacer"></div>
                    <div class="family_section_6_left"><!-- Begining of Section Six Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 5. Relative's Name </label>
                            <label class="font_small"> First Name/ Last Name </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Relative's Age </label>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_5_names" id="relative_5_names" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <?php
                            $fullDate->genDayDDL("rage4day");
                            $fullDate->genMonthDDL("rage4month");
                            $fullDate->genYearDDL("rage4year");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relationship to Client </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_5_rship" id="relative_5_rship" type="text" size="35" maxlength="35" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <div class="section_row" style="border:1px solid #000000; min-height:30px; width:210px; float: right;">
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Relative's HIV Status </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="relative_5_hiv_status" id="relative_5_hiv_status" type="radio" value="positive" > Positive
                                    <input name="relative_5_hiv_status" id="relative_5_hiv_status" type="radio" value="negative"> Negative
                                    <input name="relative_5_hiv_status" id="relative_5_hiv_status" type="radio" value="unknown"> Unknown
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Is Relative Registered at this Clinic </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_5_registered" id="relative_5_registered" type="radio" value="yes" > Yes
                            <input name="relative_5_registered" id="relative_5_registered" type="radio" value="no"> No
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> File Number </label>&nbsp;&nbsp;&nbsp;
                            <input name="relative_5_file_no" id="relative_5_file_no" type="text" size="4" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>
                    </div><!-- End of Section Six Left -->
                    <div class=" family_section_6_right"><!-- Begining of Section Six Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Relative's Gender </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="relative_5_gender" id="relative_5_gender" type="radio" value="male" > Male
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="relative_5_gender" id="relative_5_gender" type="radio" value="female" > Female
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row" style=" border:1px solid #000000; min-height:40px; width:170px;">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HIV Care Status </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_5_hiv_care" id="relative_5_hiv_care" type="radio" value="on_art" > On ART
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_5_hiv_care" id="relative_5_hiv_care" type="radio" value="died" > Died
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_5_hiv_care" id="relative_5_hiv_care" type="radio" value="in_hiv_care"> In HIV Care
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="relative_5_hiv_care" id="relative_5_hiv_care" type="radio" value="unknown" > Unknown
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="relative_5_hiv_care" id="relative_5_hiv_care" type="radio" value="not_in_hiv_care"> Not in HIV Care
                            </div>
                        </div>

                    </div><!-- End of Section Six Right -->
                </div><!-- End of Section Six -->
                <div class="spacer"></div>
                <div class="section family_section_7"><!-- Begining of Section Seven -->
                    <div class="spacer"></div>
                    <div class="section_row">
                        <input id="Submit" type="submit" value="Submit" />&nbsp;&nbsp;

                        <input id="Reset" type="reset" value="Reset" />
                    </div>
                </div><!-- End of Section Seven -->

            </form>
        </div><!-- End of Main Container Form -->
    </div>
</div><!-- End of Main Container -->

