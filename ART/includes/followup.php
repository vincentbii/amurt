<div id="followup" class ="main_container">
    <div align="center">
        <div class="profile_main_container_form">
            <form class="" action="arv_followup.php" method="post" onSubmit="" name="arv_followup">

                <div class="section followup_section_1">
                    <div class="spacer"></div>
                    <div class="followup_section_1_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="459"><label class="font_bold">1. Patient Name:</label></td>
                                    <td colspan="3"><input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">3. ID: </label>
                                        &nbsp;</td>
                                    <td width="55"><input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" readonly="true"/></td>
                                    <td width="16" align="center">&nbsp;</td>
                                    <td width="612"><input name="patient_enrollment_no" id="patient_enrollment_no" type="text" size="7" maxlength="7" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class=" font_bold">Satellite#</label></td>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold">Patient Enrollment#</label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="followup_section_1_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><label class="font_bold">2. Visited Date <span class="font_small">(YY/MM/DD)</span>: </label></td>
                                    <td>&nbsp;</td>
                                    <td><?php
                                        $fullDate->genDayDDL("visitday");
                                        $fullDate->genMonthDDL("visitmonth");
                                        $fullDate->genYearDDL("visityear");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">4. Existing Hosp/Clinic #:</label></td>
                                    <td>&nbsp;</td>
                                    <td><input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" readonly="true"/></td>
                                </tr>
                            </table></div>
                    </div>

                </div>
                <div class="spacer"></div>
                <div class="section followup_section_2">
                    <div class="spacer"></div>
                    <div class="followup_section_2_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0">
                                <tr>
                                    <td width="488"><label class="font_bold">5. Last CD4 Count:</label></td>
                                    <td width="130"><input name="last_cd4_count" id="cd4_count" type="text" size="4" maxlength="4" />
                                        <label class="font_small"> c/mm<sup>3</sup> </label></td>
                                    <td width="57"><label class="font_bold">Date: </label></td>
                                    <td width="459"><?php
                                        $fullDate->genDayDDL("lastCD4day");
                                        $fullDate->genMonthDDL("lastCD4month");
                                        $fullDate->genYearDDL("lastCD4year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">5a. CD4 Prior to Starting ARV:</label></td>
                                    <td><input name="cd4_prior_arv" id="prior_cd4" type="text" size="4" maxlength="4" />
                                        <label class="font_small"> c/mm<sup>3</sup> </label></td>
                                    <td><label class="font_bold">Date: </label></td>
                                    <td><?php
                                        $fullDate->genDayDDL("priorCD4day");
                                        $fullDate->genMonthDDL("prioCD4month");
                                        $fullDate->genYearDDL("prioCD4year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">6. Last Viral Load:</label></td>
                                    <td><input name="last_viral_load" id="viral_load" type="text" size="6" maxlength="6" /></td>
                                    <td><label class="font_bold">Date:</label></td>
                                    <td><?php
                                        $fullDate->genDayDDL("viralLoadday");
                                        $fullDate->genMonthDDL("viralLoadmonth");
                                        $fullDate->genYearDDL("viralLoadyear");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="followup_section_2_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3"><label class="font_bold">7a. Current regimen Begin:</label> </td>
                                    <td><input name="current_regimen_begin" id="regimen_begin" type="text" size="15" maxlength="15" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                    <td width="44%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><label class="font_bold">7b. Regimen:</label></td>
                                    <td><input name="regimen" id="regimen" type="text" size="25" maxlength="25" /></td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="28%"><label class="font_bold">8. Pregnant?:</label></td>
                                    <td width="28%"><input name="pregnant" id="yes" type="radio" value="yes" />
                                        Yes
                                        <input name="pregnant" id="no" type="radio" value="no" />
                                        No</td>
                                    <td colspan="2"><label class="font_bold"></label>
                                        <label class="font_bold">LMP </label>
                                        &nbsp;&nbsp;
                                        <input name="lmp" id="lmp" type="text" size="4" maxlength="4" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="spacer"></div>
                <div class="section followup_section_3">
                    <div class="spacer"></div>
                    <div class="followup_section_3_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="520" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="5"><label class="font_bold">9. Adherence: </label>
                                        &nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="right"><label class="font_bold">Number of doses missed last week:</label>
                                        <input name="last_week" id="last_week" type="text" size="5" maxlength="5" />
                                        <input name="doses_missed_lweek" id="none2" type="radio" value="none" />
                                        None</td>
                                    <td width="19%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="right"><label class="font_bold">Last Month:</label>
                                        <input name="last_month" id="last_month" type="text" size="5" maxlength="5" />
                                        <input name="doses_missed_lmonth" id="none3" type="radio" value="none" />
                                        None</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="13%"><input name="adherence" id="adherence" type="radio" value="none" />
                                        Dot<label class="font_small"></label>
                                        <label class="font_small"></label>                                  <label class="font_small"></label></td>
                                    <td width="34%"><input name="adherence_times2" id="adherence_times2" type="text" size="2" maxlength="2" />
                                        <label class="font_small">Times/Week</label></td>
                                    <td width="13%"><label class="font_small">
                                            <input name="adherence" id="home_visit" type="radio" value="none" />
                                            Home Visit</label></td>
                                    <td width="21%"><input name="adherence_times" id="adherence_times" type="text" size="2" maxlength="2" />
                                        <label class="font_small"> Times/Week</label></td>
                                    <td><input name="adherence" id="support_group" type="radio" value="none" />
                                        Support Group </td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="adherence_rx" id="adherence_rx2" type="radio" value="none" />
                                        Rx was interrupted(unintentional)</td>
                                    <td colspan="2"><label class="font_bold">Date:</label>
                                        <?php
                                        $fullDate->genDayDDL("unintday");
                                        $fullDate->genMonthDDL("unintmonth");
                                        $fullDate->genYearDDL("unintyear");
                                        ?></td>
                                    <td># of Days:
                                        <input name="adherence_rx_no_days2" id="adherence_rx_no_days2" type="text" size="3" maxlength="3" /></td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="adherence_rx" id="adherence_rx3" type="radio" value="none" />
                                        Rx was stopped(intentional)
                                        <label class="font_bold"></label></td>
                                    <td colspan="2"><label class="font_bold">Date: </label>
                                        <?php
                                        $fullDate->genDayDDL("intday");
                                        $fullDate->genMonthDDL("intmonth");
                                        $fullDate->genYearDDL("intyear");
                                        ?></td>
                                    <td><label class="font_small"># of Days:</label>
                                        <input name="adherence_rx_no_days" id="adherence_rx_no_days" type="text" size="3" maxlength="3" /></td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5"><input name="adherence_rx" id="adherence_rx" type="radio" value="none" />
                                        Patient Reports taking Herbal Medicine</td>
                                </tr>
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="followup_section_3_right">
                        <div class=" followup_section_3_section">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold_small"> Adherence Codes </label>
                                <label class="font_small"> (Check Those that Apply) </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="adherence_codes[0]"  type="checkbox" value="forgot" /> 1. Forgot                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="adherence_codes[6]" type="checkbox" value="program_stopped" /> 7. Program Stopped
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="adherence_codes[1]" type="checkbox" value="side_effects" /> 2. Side Effects &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="adherence_codes[7]" type="checkbox" value="delivery_travel_problems" /> 8. Delivery/Travel Problems
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="adherence_codes[2]"  type="checkbox" value="feel_sick" /> 3. Feel Sick
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="adherence_codes[8]" id="out_of_stock" type="checkbox" value="dispensary_out_of_stock" /> 9. Dispensary Out of Stock
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="adherence_codes[3]" type="checkbox" value="illness_in_family" /> 4.Illness in the Family &nbsp;
                                <input name="adherence_codes[9]" type="checkbox" value="unable_to_pay" /> 10.Unable to Pay for Meds
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="adherence_codes[4]" type="checkbox" value="work_conflicts" /> 5. Work Conflicts                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="adherence_codes[10]" type="checkbox" value="perceived_lack_need" /> 11. Perceived Lack of Need
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="adherence_codes[5]" type="checkbox" value="share_medications" /> 6. Sharing Medications
                                <input name="adherence_codes[11]" type="checkbox" value="others" /> 12. Others
                            </div>
                        </div>
                    </div>


                </div>
                <div class="spacer"></div>
                <div class="section followup_section_4">
                    <div class="spacer"></div>
                    <div class="followup_section_4_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="25%"><label class="font_bold"> 10. Presenting Complain(s): </label></td>
                                    <td width="13%"><input name="presenting_complains" id="none4" type="radio" value="none" />
                                        None</td>
                                    <td width="25%"> <input name="presenting_complains" id="recent_weight_loss" type="checkbox" value="recent weight loss" /> Recent Weight Loss</td>
                                    <td width="20%"> <input name="presenting_complains" id="fever" type="checkbox" value="fever" /> Fever</td>
                                    <td width="17%"><input name="presenting_complains" id="night_sweats" type="checkbox" value="night sweats" /> Night Sweats</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="cough" id="cough" type="checkbox" value="cough" />
                                        Cough
                                        &nbsp;&nbsp;</td>
                                    <td><input name="headache" id="headache2" type="checkbox" value="headache" />
                                        Headache</td>
                                    <td><input name="nausea_vomiting" id="nausea_vomiting" type="checkbox" value="nausea/vomiting" />
                                        Nausea/Vomiting </td>
                                    <td><input name="numbness_tingling" id="numbness_tingling" type="checkbox" value="numbness/tingling" />
                                        Numbness/Tingling </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="shortness_of_breath" id="shortness_of_breath" type="checkbox" value="shortness of breath" />
                                        Shrotness of Breath </td>
                                    <td><input name="pain_swallowing" id="pain_swallowing" type="checkbox" value="pain nwhen swallowing" />
                                        Pain When Swallowing</td>
                                    <td><input name="chronic_diarrhea" id="chronic_diarrhea" type="checkbox" value="chronic diarrhea" />
                                        Chronic Diarrhea
                                        &nbsp;</td>
                                    <td><input name="new_visual_problem" id="new_visual_problem" type="checkbox" value="new visual problem" />
                                        New Visual Problem</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5"><label class="font_bold">11. Presenting Complain(s) - Description or Additional Comments: </label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5"><textarea name="presenting_complains_comments" id="complains"  cols="95" rows="2"  > </textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="followup_section_4_right">

                    </div>
                </div>
                <div class="spacer"></div>
                <div class=" section followup_section_5">
                    <div class="spacer"></div>
                    <div class="followup_section_5_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><label class="font_bold"> 12. Physical Exam: </label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">Temp: </label>
                                        <input name="temperature" id="tempurature" type="text" size="4" maxlength="4" />
                                        C</td>
                                    <td><label class="font_bold"> RR: </label>
                                        <input name="rr" id="rr" type="text" size="4" maxlength="4" />
                                        <label class="font_small"> bpm</label></td>
                                    <td><label class="font_bold">HR: </label>
                                        <input name="hr" id="hr" type="text" size="4" maxlength="4" />
                                        <label class="font_small"> bpm </label></td>
                                    <td><label class="font_bold">Bp: </label>
                                        <input name="bp" id="bp" type="text" size="7" maxlength="7" />
                                        <label class="font_small"> mm/Hg</label></td>
                                    <td><label class="font_bold"> Height: </label>
                                        <input name="height" id="height" type="text" size="3" maxlength="3" />
                                        &nbsp;</td>
                                    <td><label class="font_bold">Wt: </label>
                                        <input name="weight" id="weight" type="text" size="3" maxlength="3" />
                                        <label class=" font_small"> KG</label></td>
                                    <td><label class="font_bold">Pain: </label>
                                        <input name="pain" id="pain" type="text" size="4" maxlength="4" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="7"><textarea name="physical_exam_comments" id="physical_exam"  cols="95" rows="2"  > </textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="followup_section_5_right">


                    </div>

                </div>
                <div class="spacer"></div>
                <div class="section followup_section_6">
                    <div class="spacer"></div>
                    <div class="section_row">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><label class="font_bold"> 13. ARV Side Effects: </label></td>
                                <td><input name="arv_side_effects" id="none5" type="radio" value="none" />
                                    None</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="headache2" id="headache" type="checkbox" value="headache" />
                                    Headache</td>
                                <td><input name="hyperlipidemia" id="hyperlipidemia" type="checkbox" value="hyperlipidemia" />
                                    Hyperlipidemia</td>
                                <td><input name="hepatic_toxicity" id="hepatic_toxicity" type="checkbox" value="hepatic_toxicity" />
                                    Hepatic Toxicity
                                    <label class="font_smaller"> (LFT > 5X Normal)</label></td>
                                <td><input name="pancreatitis" id="pancreatitis" type="checkbox" value="pancreatitis" />
                                    Pancreatitis </td>
                                <td><input name="suicide_attempt" id="suicide_attempt" type="checkbox" value="suicide_attempt" />
                                    Suicide Attempt </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="anemia" id="anemia" type="checkbox" value="anemia" />
                                    Anemia</td>
                                <td><input name="hyperglycemia" id="hyperglycemia" type="checkbox" value="hyperglycemia" />
                                    Hyperglycemia</td>
                                <td><input name="liver_failure" id="liver_failure" type="checkbox" value="liver_failure" />
                                    Liver Failure</td>
                                <td><input name="lactic_acidosis" id="lactic_acidosis" type="checkbox" value="lactic_acidosis" />
                                    Lactic Acidosis </td>
                                <td><input name="renal_insufficiency" id="renal_insufficiency" type="checkbox" value="renal_insufficiency" />
                                    Renal Insufficiency</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="severe_nv" id="severe_nv" type="checkbox" value="severe_nv" />
                                    Severe N&V</td>
                                <td><input name="rash" id="rash" type="checkbox" value="rash" />
                                    Rash </td>
                                <td><input name="peripheral_neuropathy" id="peripheral_neuropathy" type="checkbox" value="peripheral_neuropathy" />
                                    Peripheral Neuropathy </td>
                                <td><input name="insomnia" id="insomnia" type="checkbox" value="insomnia" />
                                    Insomnia</td>
                                <td><input name="renal_failure" id="renal_failure" type="checkbox" value="renal_failure" />
                                    Renal Failure </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="diarrhoea" id="diarrhoea" type="checkbox" value="diarrhoea" />
                                    Diarrhoea</td>
                                <td><input name="stevens_johnson_syndrome" id="stevens_johnson_syndrome" type="checkbox" value="stevens_johnson_syndrome" />
                                    Stevens Johnson Syndrome </td>
                                <td><input name="lipoatrophy" id="lipoatrophy" type="checkbox" value="lipoatrophy" />
                                    Lipoatrophy</td>
                                <td><input name="psychosis" id="psychosis" type="checkbox" value="psychosis" />
                                    Psychosis</td>
                                <td><input name="iris" id="iris" type="checkbox" value="iris" />
                                    IRIS </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><label class="font_bold"> Comments: </label></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="5"><textarea name="arv_comments" id="arv_comments"  cols="95" rows="2"  > </textarea></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><label class="font_bold"> 14. OIs or AIDS Defining Illnessses </label></td>
                                <td><input name="ols_defining_illnesses" id="none" type="radio" value="none" />
                                    None</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="pulmonary_tb" id="pulmonary_tb" type="checkbox" value="pulmonary_tb" />
                                    Pulmonary TB</td>
                                <td><input name="mycobacteria_other" id="mycobacteria_other" type="checkbox" value="mycobacteria_other" />
                                    Mycobacteria Other</td>
                                <td><input name="herpes_zoster" id="herpes_zoster" type="checkbox" value="herpes_zoster" />
                                    Herpes Zoster</td>
                                <td><input name="encephalopathy_dementia" id="encephalopathy_dementia" type="checkbox" value="encephalopathy_dementia" />
                                    Encephalopathy/Dementia</td>
                                <td><input name="septicemia" id="septicemia" type="checkbox" value="septicemia" />
                                    Septicemia</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="urethritis_cervicitis" id="urethritis_cervicitis" type="checkbox" value="urethritis_cervicitis" />
                                    Urethritis/Cervicitis</td>
                                <td><input name="extrapulm_tb" id="extrapulm_tb2" type="checkbox" value="extrapulm_tb" />
                                    Extrapulm TB</td>
                                <td><input name="candidiasis_oral" id="candidiasis_oral" type="checkbox" value="candidiasis_oral" />
                                    Candidiasis Oral </td>
                                <td><input name="herpes_simplex" id="herpes_simplex" type="checkbox" value="herpes_simplex" />
                                    Herpes Simplex</td>
                                <td><input name="neuro" id="neuro" type="checkbox" value="neuro" />
                                    Neuro (Toxo, PML...)</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="lympoma" id="lympoma" type="checkbox" value="lympoma" />
                                    Lympoma </td>
                                <td><input name="diarrhoea_wasting" id="diarrhoea_wasting" type="checkbox" value="diarroea_wasting" />
                                    Diarrhoea/Wasting</td>
                                <td><input name="malaria" id="malaria" type="checkbox" value="malaria" />
                                    Malaria</td>
                                <td><input name="pid" id="pid" type="checkbox" value="pid" />
                                    PID</td>
                                <td><input name="ks_cutaneous" id="ks_cutaneous" type="checkbox" value="ks_cutaneous" />
                                    KS Cutaneous</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Oesophageal </td>
                                <td><input name="candidiasis_oesophageal" id="candidiasis_oesophageal" type="checkbox" value="candidiasis_oesophageal" />
                                    Candidiasis</td>
                                <td><input name="pcp" id="pcp" type="checkbox" value="pcp" />
                                    PCP </td>
                                <td><input name="salmonellosis" id="salmonellosis" type="checkbox" value="salmonellosis" />
                                    Salmonellosis </td>
                                <td><input name="ks_visceral" id="ks_visceral" type="checkbox" value="ks_visceral" />
                                    KS-Visceral </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input name="cr_meningitis" id="cr_meningitis" type="checkbox" value="cr_meningitis" />
                                    Cr Meningitis</td>
                                <td><input name="pneumonia" id="pneumonia" type="checkbox" value="pneumonia" />
                                    Pneumonia</td>
                                <td><input name="cmv_retinitis" id="cmv_retinitis" type="checkbox" value="cmv_retinitis" />
                                    CMV Retinitis </td>
                                <td><input name="genital_ulcerative" id="genital_ulcerative" type="checkbox" value="genital ulcerative" />
                                    Genital Ulcerative Disease </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Comments</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="5"><textarea name="ols_defining_comments" id="comments"  cols="95" rows="2"  > </textarea></td>
                            </tr>
                        </table></div>
                </div>
                <div class="spacer"></div>
                <div class="section followup_section_7">
                    <div class="spacer"></div>
                    <div class=" followup_section_7_1">
                        <div class="followup_section_7_top">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2"><label class="font_bold"> 15. Assessment: </label></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><input name="assessment " id="assessment2" type="radio" value="improving_stable" />
                                            Improving/Stable</td>
                                        <td><input name="assessment" id="assessment3" type="radio" value="active_ol" />
                                            ActiveOl </td>
                                        <td><input name="assessment" id="assessment4" type="radio" value="drug_toxicity" />
                                            Drug Toxicity</td>
                                        <td><input name="assessment" id="assessment5" type="radio" value="non_adherence" />
                                            Non-adherence </td>
                                        <td><label class="font_bold_small">WHO Stage </label>
                                            <input name="assessment_who" id="who_stage" type="text" size="3" maxlength="3" />
                                            &nbsp;</td>
                                        <td><label class="font_bold_small">WAB </label>
                                            <input name="assessment_wab" id="wab" type="text" size="3" maxlength="3" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"><textarea name="assessment_comments" id="assessment"  cols="95" rows="2"  > </textarea></td>
                                    </tr>
                                </table></div>
                        </div>
                    </div>
                    <div class="followup_section_7_2">
                        <div class="followup_section_7_left">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2"><label class="font_bold"> 16. Plain - ARV Therapy </label></td>
                                    </tr>
                                    <tr>
                                        <td width="54%">&nbsp;</td>
                                        <td width="46%">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><input name="arv_therapy" id="continue_current_treatment" type="radio" value="continue_current_treatment" />
                                            Continue Current Treatment</td>
                                        <td><input name="arv_therapy" id="change_regimen" type="radio" value="change_regimen" />
                                            Change Regimen (Indicate Code) </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><input name="arv_therapy" id="restart_treatment" type="radio" value="restart_treatment" />
                                            Restart Treatment </td>
                                        <td><input name="arv_therapy" id="stop_treatment" type="radio" value="stop_treatment" />
                                            Stop Treatment (Indicate Code) </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><input name="arv_therapy" id="treatment_not_indicated" type="radio" value="treatment_not_indicated" />
                                            Treatment Not Indicated Now </td>
                                        <td><input name="arv_therapy" id="start_new_treatment" type="radio" value="start_new_treatment" />
                                            Start New Treatment (Naive Patient) </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Current Regimen </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><input name="arv_current_regimen" id="current_regimen" type="text" size="75" maxlength="75" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="followup_section_7_right">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Therapy Change Codes </label>
                                <label class="font_bold_small"> (Check One) </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes[0]" type="checkbox" value="toxicity" /> 1. Toxicity &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes[6]" type="checkbox" value="drugs_not_available" /> 7. Drugs Not Available

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label> 2. Treatment Failure </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes[7]" type="checkbox" value="migratory_patient" /> 8. Migratory Patient
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="arv_therapy_codes[1]" type="checkbox" value="virologic" /> 2a. Virologic &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small"> Follow-up Difficult </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="arv_therapy_codes[2]" type="checkbox" value="immunologic" /> 2b. Immunologic &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes[8]" type="checkbox" value="transfer" /> 9. Transfer To Other
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="arv_therapy_codes[3]" type="checkbox" value="clinical" /> 2c. Clinical &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small"> Program </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes[4]" type="checkbox" value="non_adherence" /> 3. Non Adherence
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes[9]" type="checkbox" value="inability_to_pay" /> 10. Inability To Pay
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes[5]" type="checkbox" value="drug_interaction" /> 4. Drug Interaction
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes[10]" type="checkbox" value="other" /> 11. Other
                            </div>
                        </div>
                    </div>
                    <div class="followup_section_7_3">
                        <div class="followup_section_7_bottom">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 17. Tuberculosis </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Diagnosed with TB since last Visit? </label>
                                <input name="tb_diagonised" id="" type="radio" value="yes" /> Yes
                                <input name="tb_diagonised" id="" type="radio" value="no" /> No
                                &nbsp;&nbsp;&nbsp;
                                <label class="font_bold"> Type of TB: </label>&nbsp;&nbsp;&nbsp;
                                <input name="tb_type" id="ptb_sputum_plus" type="radio" value="ptb_sputum_plus" /> PTB Sputum +
                                <input name="tb_type" id="ptb_sputum_neg" type="radio" value="ptb_sputum_neg" /> PTB Sputum -
                                <input name="tb_type" id="extrapulm_tb" type="radio" value="extrapulm_tb" /> Extrapulm TB
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> TB Therapy Start Date: </label>&nbsp;
                                <label class="font_bold"> Date: </label>

                                <?php
                                $fullDate->genDayDDL("tbStartday");
                                $fullDate->genMonthDDL("tbStartmonth");
                                $fullDate->genYearDDL("tbStartyear");
                                ?>

                            </div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_bold"> TB Therapy End Date: </label>&nbsp;
                                <label class="font_bold"> Date: </label>

                                <?php
                                $fullDate->genDayDDL("tbEndday");
                                $fullDate->genMonthDDL("tbEndmonth");
                                $fullDate->genYearDDL("tbEndyear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Regimen: </label>
                                <input name="tb_regimen" id="2rhze_6eh" type="radio" value="2rhze_6eh" /> 2RHZE/ 6EH
                                <input name="tb_regimen" id="2rhze_4rh" type="radio" value="2rhze_4rh" /> 2RHZE/ 4RH
                                <input name="tb_regimen" id="2rhz_6eh" type="radio" value="2rhz_6eh" /> 2RHZ/ 6EH
                                <input name="tb_regimen" id="2rhz_4rh" type="radio" value="2rhz_4rh" /> 2RHZ/ 4RH
                                <input name="tb_regimen" id="2shze_1rhze_5rhe" type="radio" value="2shze_1rhze_5rhe" /> 2SHZE/ 1RHZE/ 5RHE
                                <input name="tb_regimen" id="2shze_4rhze" type="radio" value="2shze_4rhze" /> 2SHZE/ 4RHZE
                                <input name="tb_regimen" id="other" type="radio" value="other" /> Other
                            </div>
                        </div>
                    </div>
                    <div class="followup_section_7_4">
                        <div class="followup_section_7_leftB">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 18. When is the Patient's Next Appointment </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="next_appointment" id="1_week" type="radio" value="1_week" /> 1 Week
                                <input name="next_appointment" id="2_months" type="radio" value="2_months" /> 2 Months &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_bold"> Date: </label>

                                <?php
                                $fullDate->genDayDDL("nextAppday");
                                $fullDate->genMonthDDL("nextAppmonth");
                                $fullDate->genYearDDL("nextAppyear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="next_appointment" id="2_weeks" type="radio" value="2_weeks" /> 2 Weeks
                                <input name="next_appointment" id="3_months" type="radio" value="3_months" /> 3 Months &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_bold_small"> Reason: </label>
                                <input name="appointment_reason" id="reason" size="35" type="text" maxlength="35" />
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="next_appointment" id="4_weeks" type="radio" value="4 weeks" /> 4 Weeks
                                <input name="next_appointment" id="6_months" type="radio" value="6 months" /> 6 Months
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class=" followup_section_7_rightB">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold_small"> 19. Print Name: </label>
                                <input name="name" id="print_name" size="32" type="text" maxlength="32" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section followup_section_8">
                    <div class="spacer"></div>
                    <div class="followup_section_8_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />&nbsp;&nbsp;

                            <input id="Reset" type="reset" value="Reset" />
                        </div>
                    </div>
                    <div class="spacer"></div>
                    <div class="followup_section_8_right">
                    </div>


                </div>


            </form>
        </div>
    </div>
</div>

