<!--javascript:get(document.getElementById('initial_evaluation'));-->
<div id="initialData" class="main_container">
    <div align="center">
        <div class="main_container_form">
            <form action="initial_evaluation.php" method="post" name="initial_evaluation" id="initial_evaluation">
                <div class="section initial_section_1">
                    <div class="initial_section_1_left">  
                        <div class="spacer"></div>
                        <div class="section_row">                        
                            <label for="patient_name" class="font_bold"> 1. Patient name:</label>&nbsp;
                            <input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 3. ID: </label>&nbsp;

                            <input name="country_no" id="country_no" value="" type="text" size="3" maxlength="3" />
                            <input name="enrollment_no" id="enrollment_no" value="" type="text" size="7" maxlength="7" />
                            <input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class=" font_bold_small">Country</label>
                            &nbsp;&nbsp;<label class="font_small"> Patient Enrollment#</label>
                            &nbsp;&nbsp;&nbsp;<label class="font_small"> Existing Hosp/Clinic#</label>
                        </div>
                        <div class="section_row">
                            <label class="font_bold"> 4a. Date Of HIV Diagnosis: </label>
                            &nbsp;&nbsp;&nbsp;

                            <?php
                            $fullDate->genDayDDL("init_diagnosisday");
                            $fullDate->genMonthDDL("init_diagnosismonth");
                            $fullDate->genYearDDL("init_diagnosisyear");
                            ?>

                        </div>
                        <div class="section_row">
                            <label class="font_bold"> 4b. Diagnosis Verified: </label>
                            <input name="diagnosis_verified" id="yes" type="radio" value="yes"  />Yes
                            <input name="diagnosis_verified" id="no" type="radio" value="no" >No
                        </div>
                    </div>
                    <div class="spacer"></div>
                    <div class="initial_section_1_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="visit_date" class="font_bold"> 2. Visit Date<span class="font_small"> (MM/DD/YY)</span></label>
                            &nbsp;&nbsp;&nbsp;

                            <?php
                            $fullDate->genDayDDL("init_vdateday");
                            $fullDate->genMonthDDL("init_vdatemonth");
                            $fullDate->genYearDDL("init_vdateyear");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 5. Disclosed: </label>
                            <input name="hiv_disclosed" id="yes" type="radio" value="Yes"  />Yes
                            <input name="hiv_disclosed" id="no" type="radio" value="No" >No
                            &nbsp;
                            <label class="font_bold"> Who To: </label>&nbsp;
                            <input name="lmp" id="lmp" type="text" size="15" maxlength="15" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 6. Pregnant?: </label>
                            <input name="pregnant" id="yes" type="radio" value="Yes" >Yes
                            <input name="pregnant" id="no" type="radio" value="No" >No
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> LMP: </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lmp" id="lmp" type="text" size="4" maxlength="4" />
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section initial_section_2">
                    <div class="spacer"></div>
                    <div class="initial_section_2_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 7. Presenting Complain(s): </label>
                            <input name="complains" id="none" type="radio" value="none" > None
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="recent_weight_loss" type="radio" value="Recent Weight Loss" > Recent Weight Loss
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="fever" type="radio" value="fever"> Fever  
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <input name="complains" id="night_sweats" type="radio" value=" Night Sweats" > Night Sweats
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="complains" id="cough" type="radio" value="cough"> Cough  
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="headache" type="radio" value="headache" > Headache  
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="nausea_vomiting" type="radio" value="Nausea/Vomiting"> Nausea/Vomiting
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="numbness_tingling" type="radio" value="Numbness/Tingling" > Numbness/Tingling
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="complains" id="shortness_of_breath" type="radio" value="Shrotness of Breath"> Shrotness of Breath
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="pain_swallowing" type="radio" value="Pain When Swallowing" > Pain When Swallowing
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="chronic_diarrhea" type="radio" value="Chronic Diarrhea"> Chronic Diarrhea
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="complains" id="new_visual_problem" type="radio" value="New Visual Problem" > New Visual Problem
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 8. Presenting Complain(s) - Description or Additional Comments: </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <textarea name="complains_comments" id="complains_extra"  cols="74" rows="3" > </textarea>

                        </div>
                    </div>
                    <div class="initial_section_2_right">

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section initial_section_3">
                    <div class="initial_section_3_1">
                        <div class="spacer"></div>
                        <div class="initial_section_3_left">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="631"><label class="font_bold">Medical History </label></td>
                                  <td><input name="hist001" id="none2" type="radio" value="none" />
                                            None</td>
                                        <td width="220">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="631"><label class="font_bold">9. Year </label></td>
                                      <td width="291"><label class="font_bold">Disease</label></td>
                                      <td><label class="font_bold">Specify</label></td>
                                    </tr>
                                    <tr>
                                        <td><?php
                                            $fullDate->genMonthDDL("hist002_month");
                                            $fullDate->genYearDDL("hist002_year");
                                            ?></td>
                                        <td><input name="hist002" id="hist002" type="checkbox" value="k76.9" />
                                            Liver Disease</td>
                                        <td><input name="hist002_comments" id="hist002_comments" type="text" size="20" maxlength="20" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><?php
                                            $fullDate->genMonthDDL("hist003_month");
                                            $fullDate->genYearDDL("hist003_year");
                                            ?></td>
                                        <td><input name="hist003" id="hist003" type="checkbox" value="M93.1" />
                                            Kidney Disease</td>
                                        <td><input name="hist003_comments" id="hist003_comments" type="text" size="20" maxlength="20" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><?php
                                            $fullDate->genMonthDDL("hist004_month");
                                            $fullDate->genYearDDL("hist004_year");
                                            ?></td>
                                        <td><input name="hist004" id="hist004" type="checkbox" value="D64.9" />
                                            Anaemia</td>
                                        <td><input name="hist004_comments" id="hist004_comments" type="text" size="20" maxlength="20" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><?php
                                            $fullDate->genMonthDDL("hist005_month");
                                            $fullDate->genYearDDL("hist005_year");
                                            ?></td>
                                        <td><input name="hist005" id="hist005" type="checkbox" value="Z71.5" />
                                            Drug Abuse</td>
                                        <td><input name="hist005_comments" id="hist005_comments" type="text" size="20" maxlength="20" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><?php
                                            $fullDate->genMonthDDL("hist006_month");
                                            $fullDate->genYearDDL("hist006_year");
                                            ?></td>
                                        <td><input name="hist006" id="hist006" type="checkbox" value="Z72.1" />
                                            Alcohol Abuse</td>
                                        <td><input name="hist006_comments" id="hist006_comments" type="text" size="20" maxlength="20" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><?php
                                            $fullDate->genMonthDDL("hist007_month");
                                            $fullDate->genYearDDL("hist007_year");
                                            ?></td>
                                        <td><input name="input" id="input" type="checkbox" value="" />
                                            <?php
                                            $medical ->getMedical("hist007");
                                            ?></td>
                                        <td><input name="hist007_comments" id="hist007_comments" type="text" size="20" maxlength="20" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="initial_section_3_right">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 10. Drug Allergies / Toxicities </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="drug_allergies" id="none" type="radio" value="None" > None
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="drug_allergies" id="sulfa_drugs" type="radio" value="Sulfa Drugs" > Sulfa Drugs
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="drug_allergies" id="other" type="radio" value="other" > Other (Specify)
                            </div>
                            <input name="drug_allergies_comments" id="other" type="text" size="35" maxlength="35" />
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 11. Current Long Term Medications </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="current_medications" id="none" type="radio" value="None" > None
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="current_medications" id="sulfa_tmp" type="radio" value="Sulfa / TMP" > Sulfa / TMP
                                <input name="current_medications_comments[]" id="sulfa_tmp" type="text" size="35" maxlength="35" />

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="current_medications" id="tb_rx" type="radio" value="TB Rx" > TB Rx (Specify)
                                <input name="current_medications_comments[]" id="tb_rx" type="text" size="13" maxlength="15" />
                                &nbsp;&nbsp;&nbsp;
                                <!--div id="initial_tbdate" align="justify" style="float:right;" ></div-->

                                <?php
                                $fullDate->genMonthDDL("tbmonth");
                                $fullDate->genYearDDL("tbyear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small"> Start: </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small">    MM / YY </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="current_medications" id="other" type="radio" value="Other" /> Other (Specify)
                                &nbsp;&nbsp;&nbsp;
                                <input name="current_medications_comments[]" id="other" type="text" size="25" maxlength="25" />
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="current_medications" id="other" type="radio" value="Other" /> Other (Specify)
                                &nbsp;&nbsp;&nbsp;
                                <input name="current_medications_comments[]" id="other" type="text" size="25" maxlength="25" />
                            </div>
                        </div>
                    </div>
                    <div class="initial_section_3_2">
                        <div class="spacer"></div>
                        <div class="initial_section_3_left">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> HIV-Related History </label>
                                <label class="font_bold" style="padding-right: 20px;"></label>
                                <input name="hiv_related_history" id="none" type="radio" value="None" > None
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 12. Lowest CD4 </label>
                                <label class="font_bold" style="padding-right: 28px;"></label>
                                <input name="lowest_cd4_count" id="lowest_cd4_count" type="text" size="4" maxlength="4" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_small"> c/mm<sup>3</sup> </label>
                                <label class="font_bold" style="padding-right: 8px;"></label>
                                <label class="font_bold"> Date: </label>
                                <label class="font_bold" style="padding-right: 4px;"></label>
                                <?php
                                $fullDate->genMonthDDL("lowest_cd4month");
                                $fullDate->genYearDDL("lowest_cd4year");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold" style="padding-right: 120px;"></label>
                                <input name="lowest_cd4_percent" id="lowest_cd4_percent" type="text" size="2" maxlength="2" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_bold"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 12b. CD4 Prior To </label>
                                <label class="font_bold" style="padding-right: 17px;"></label>
                                <input name="cd4_prior_count" id="cd4_prior_count" type="text" size="4" maxlength="4" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_small"> c/mm<sup>3</sup> </label>
                                <label class="font_bold" style="padding-right: 8px;"></label>
                                <label class="font_bold"> Date: </label>
                                <label class="font_bold" style="padding-right: 4px;"></label>
                                <?php
                                $fullDate->genMonthDDL("cd4_priormonth");
                                $fullDate->genYearDDL("cd4_prioryear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_bold"> Starting ARVs </label>
                                <label class="font_bold" style="padding-right: 40px;"></label>
                                <input name="cd4_prior_percent" id="cd4_prior_percent" type="text" size="2" maxlength="2" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_bold"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 13. Most Recent CD4 </label>
                                <input name="resent_cd4_count" id="resent_cd4_count" type="text" size="4" maxlength="4" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_small"> c/mm<sup>3</sup> </label>
                                <label class="font_bold" style="padding-right: 8px;"></label>
                                <label class="font_bold"> Date: </label>
                                <label class="font_bold" style="padding-right: 4px;"></label>
                                <?php
                                $fullDate->genMonthDDL("resent_cd4month");
                                $fullDate->genYearDDL("resent_cd4year");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold" style="padding-right: 120px;"></label>
                                <input name="resent_cd4_percent" id="resent_cd4_percent" type="text" size="2" maxlength="2" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_bold"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 14. Pre-Therapy </label>
                                <label class="font_bold" style="padding-right: 20px;"></label>
                                <input name="viral_load" id="viral_load" type="text" size="6" maxlength="6" />
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_small"> c/ml </label>
                                <label class="font_bold" style="padding-right: 8px;"></label>
                                <label class="font_bold"> Date: </label>
                                <label class="font_bold" style="padding-right: 4px;"></label>
                                <?php
                                $fullDate->genMonthDDL("viral_loadmonth");
                                $fullDate->genYearDDL("viral_loadyear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold" style="padding-right: 2px;"></label>
                                <label class="font_bold">  Viral Load  </label>
                            </div>
                        </div>
                        <div class="initial_section_3_right">
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 15. ARV Exposure </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_exposure" id="arv_exposure" type="radio" value="ARV Exposure None" /> None
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Current ART </label>
                                <input name="current_art" id="current_art" type="text" size="20" maxlength="20" />

                                <?php
                                $fullDate->genMonthDDL("current_artmonth");
                                $fullDate->genYearDDL("current_artyear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small"> Start Date: </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small">   MM / YY </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Previous ARV Exposure </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="previous_regimen_name" id="previous_regimen_name" type="radio" value="Single Dose NVP" > Single Dose NVP
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small"> Date: </label>

                                <?php
                                $fullDate->genMonthDDL("single_dosemonth");
                                $fullDate->genYearDDL("single_doseyear");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_small"> Date: </label>

                                <?php
                                $fullDate->genMonthDDL("single_dosemonth1");
                                $fullDate->genYearDDL("single_doseyear1");
                                ?>

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="previous_regimen_name" id="previous_regimens" type="radio" value="Previous Regimens" > Previous Regimens:
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label> Months on Rx<sup>*</sup> </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_small"> Regimen </label>
                                &nbsp;&nbsp;&nbsp;

                                <?php
                                $regimen ->getRegimens("regs1 ");
                                ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="regs1_months" id="regs1_months" type="text" size="4" maxlength="4" />

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_small"> Regimen </label>
                                &nbsp;&nbsp;&nbsp;

                                <?php
                                $regimen ->getRegimens("regs2");
                                ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="regs2_months" id="regs2_months" type="text" size="4" maxlength="4" />

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_small"> Regimen </label>
                                &nbsp;&nbsp;&nbsp;

                                <?php
                                $regimen -> getRegimens("regs3");
                                ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="regs3_months" id="regs3_months" type="text" size="4" maxlength="4" />

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_small"> Regimen </label>
                                &nbsp;&nbsp;&nbsp;

                                <?php
                                $regimen -> getRegimens("regs4");
                                ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="regs4_months" id="regs4_months" type="text" size="4" maxlength="4" />

                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section initial_section_4">
                    <div class="spacer"></div>
                    <div class="initial_section_4_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">16. HIV - Associated Conditions</td>
                                    <td colspan="2"><input name="hiv_associated_conditions" id="hiv_associated_conditions" type="radio" value="None" />
                                        None</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td width="7%">&nbsp;</td>
                                    <td width="34%">MM / YY</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc1" id="assoc1" type="checkbox" value="Assoc001" />
                                        Pulmonary TB</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc1_month");
                                        $fullDate->genYearDDL("assoc1_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr><td width="11%">&nbsp;</td>
                                    <td width="48%"><input name="assoc2" id="smear_plus" type="radio" value="Assoc023"/>
                                        Smear +
                                        <input name="assoc2" id="smear_neg" type="radio" value="Assoc024" />
                                        Smear -</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc2_month");
                                        $fullDate->genYearDDL("assoc2_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc3" id="assoc3" type="checkbox" value="Assoc002" />
                                        Extrapulmonary TB</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc3_month");
                                        $fullDate->genYearDDL("assoc3_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc4" id="assoc4" type="checkbox" value="Assoc004" />
                                        PCP</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc4_month");
                                        $fullDate->genYearDDL("assoc4_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc5" id="assoc5" type="checkbox" value="Assoc003" />
                                        Pneumonia</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc5_month");
                                        $fullDate->genYearDDL("assoc5_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc6" id="assoc6" type="checkbox" value="Assoc012" />
                                        Mycobacteria Other</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc6_month");
                                        $fullDate->genYearDDL("assoc6_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc7" id="assoc7" type="checkbox" value="Assoc005" />
                                        Candidiasis - Oral</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc7_month");
                                        $fullDate->genYearDDL("assoc7_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc8" id="assoc8" type="checkbox" value="Assoc006" />
                                        Candidiasis - Oesophageal</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc8_month");
                                        $fullDate->genYearDDL("assoc8_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc9" id="assoc9" type="checkbox" value="Assoc016" />
                                        CMV Retinitis</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc9_month");
                                        $fullDate->genYearDDL("assoc9_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc10" id="assoc10" type="checkbox" value="Assoc009" />
                                        Herpes Zoster</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc10_month");
                                        $fullDate->genYearDDL("assoc10_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc11" id="assoc11" type="checkbox" value="Assoc010" />
                                        Herpes Simplex</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc11_month");
                                        $fullDate->genYearDDL("assoc11_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input name="assoc12" id="assoc12" type="checkbox" value="Assoc022" />
                                        Lympoma</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc12_month");
                                        $fullDate->genYearDDL("assoc12_year");
                                        ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="initial_section_4_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="61%">&nbsp;</td>
                                    <td width="5%">&nbsp;</td>
                                    <td width="34%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>MM / YY</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc13" id="assoc13" type="checkbox" value="Assoc007" />
                                        Cryptococcal Meningitis</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc13_month");
                                        $fullDate->genYearDDL("assoc13_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc14" id="assoc14" type="checkbox" value="Assoc008" />
                                        Encephalopathy/Dementia</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc14_month");
                                        $fullDate->genYearDDL("assoc14_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc15" id="assoc15" type="checkbox" value="Assoc011" />
                                        Neuro (Toxo, PML, Lymphoma)</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc15_month");
                                        $fullDate->genYearDDL("assoc15_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc16" id="assoc16" type="checkbox" value="Assoc014" />
                                        Chronic Diarrhoea/Wasting</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc16_month");
                                        $fullDate->genYearDDL("assoc16_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc17" id="assoc17" type="checkbox" value="Assoc013" />
                                        Salmonellosis</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc17_month");
                                        $fullDate->genYearDDL("assoc17_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc19" id="assoc19" type="checkbox" value="Assoc015" />
                                        Septicemia</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc19_month");
                                        $fullDate->genYearDDL("assoc19_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc20" id="assoc20" type="checkbox" value="Assoc018" />
                                        KS - Cutaneous</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc20_month");
                                        $fullDate->genYearDDL("assoc20_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc21" id="assoc21" type="checkbox" value="Assoc019" />
                                        KS - Visceral</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc21_month");
                                        $fullDate->genYearDDL("assoc21_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc22" id="assoc22" type="checkbox" value="Assoc017" />
                                        Genital Ulcerative Disease</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc22_month");
                                        $fullDate->genYearDDL("assoc22_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc23" id="assoc23" type="checkbox" value="Assoc020" />
                                        Urethritis/Cervicitis</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc23_month");
                                        $fullDate->genYearDDL("assoc23_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc24" id="assoc24" type="checkbox" value="Assoc021" />
                                        PID</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc24_month");
                                        $fullDate->genYearDDL("assoc24_year");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc25" id="assoc25" type="checkbox" value="Assoc0" />
                                        Other</td>
                                    <td colspan="2"><?php
                                        $fullDate->genMonthDDL("assoc25_month");
                                        $fullDate->genYearDDL("assoc25_year");
                                        ?></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section initial_section_5">
                    <div class="spacer"></div>
                    <div class="section_row">

                        <input type="submit" name="button" value="Next >>" onclick="javascript:get(this.parentNode)">
                        <input type="submit" name="button" value="Normal Submit Button">
                        <input id="task" name="task" type="hidden" value="init1" />
                    </div>
                    <span id="myspan"></span>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    var http_request = false;
    function makeRequest(url, parameters) {
        http_request = false;
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                // set type accordingly to anticipated content type
                //http_request.overrideMimeType('text/xml');
                http_request.overrideMimeType('text/html');
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }
        if (!http_request) {
            alert('Cannot create XMLHTTP instance');
            return false;
        }
        http_request.onreadystatechange = alertContents;
        http_request.open('POST', url + parameters, true);
        http_request.send(null);
    }

    function alertContents() {
        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
                //alert(http_request.responseText);
                result = http_request.responseText;
                document.getElementById('myspan').innerHTML = result;
                if(result=='false1'){
                    alert('The enrollment already exists');
                }else if(result=='false2'){
                    alert('There was a problem with the Query.');
                }else{
                    document.initial_evaluation2.enroll_no.value=document.getElementById('hosp_no').value;
                    Ext.getCmp('content-panel').layout.setActiveItem('initialData2-panel');
                }

            }


        }
    }

    function get(obj) {
        var getstr = "?";
        for (var i=0; i<document.initial_evaluation.elements.length; i++) {
            if (document.initial_evaluation.elements[i].type=="text") {

                getstr += document.initial_evaluation.elements[i].name + "=" + document.initial_evaluation.elements[i].value + "&";
            }
            if (document.initial_evaluation.elements[i].type== "checkbox") {
                if (document.initial_evaluation.elements[i].checked) {
                    getstr += document.initial_evaluation.elements[i].name + "=" + document.initial_evaluation.elements[i].value + "&";
                } else {
                    getstr += document.initial_evaluation.elements[i].name + "=&";
                }
            }
            if (document.initial_evaluation.elements[i].type == "radio") {
                if (document.initial_evaluation.elements[i].checked) {
                    getstr += document.initial_evaluation.elements[i].name + "=" + document.initial_evaluation.elements[i].value + "&";
                }
            }

            if (document.initial_evaluation.elements[i].tagName == "SELECT") {
                var sel = document.initial_evaluation.elements[i];
                getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
            }

            if (document.initial_evaluation.elements[i].type=="hidden") {

                getstr += document.initial_evaluation.elements[i].name + "=" + document.initial_evaluation.elements[i].value + "&";
            }

            if (document.initial_evaluation.elements[i].tagName=="textarea") {
                getstr += document.initial_evaluation.elements[i].name + "=" + document.initial_evaluation.elements[i].value + "&";
            }

        }
        makeRequest('initial_evaluation.php', getstr);
    }
</script>