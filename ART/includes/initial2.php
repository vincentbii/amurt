
<div id="initialData2" class="main_container"><!-- Begining of Main Container -->
    <div align="center">
        <div id="initial2" class="main_container_form"><!-- Begining of Main Container Form -->
            <form action="javascript:get2(document.getElementById('initial_evaluation2'));" method="post" name="initial_evaluation2"  id="initial_evaluation2">
                <div class="section initial_section_6"><!-- Begining of Section Six -->
                    <div class="spacer"></div>
                    <div class="initial_section_6_1"><!-- Begining of Section Six Top -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 17. Physical Exam: </label>
                            <label class="font_bold" style="padding-right: 15px;"></label>
                            <label> Check and/or Write Additional Findings in the Spaces Below the Itemized Signs</label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Temp: </label>
                            <input name="temp" id="tempurature" type="text" size="4" maxlength="4" />
                            <label class="font_small"> C </label>
                           <label class="font_bold" style="padding-right: 2px;"></label>
                            <label class="font_bold"> RR: </label>
                            <input name="rr" id="rr" type="text" size="4" maxlength="4" />
                            <label class="font_small"> bpm </label>
                            <label class="font_bold" style="padding-right: 2px;"></label>
                            <label class="font_bold"> HR: </label>
                            <input name="hr" id="hr" type="text" size="4" maxlength="4" />
                            <label class="font_small"> bpm </label>
                            <label class="font_bold" style="padding-right: 2px;"></label>
                            <label class="font_bold"> Bp: </label>
                            <input name="bp" id="bp" type="text" size="7" maxlength="7" />
                            <label class="font_small"> mm/Hg </label>
                            <label class="font_bold" style="padding-right: 2px;"></label>
                            <label class="font_bold"> Ht: </label>
                            <input name="height" id="height" type="text" size="3" maxlength="3" />
                            <label class="font_small"> cm </label>
                            <label class="font_bold" style="padding-right: 2px;"></label>
                            <label class="font_bold"> Wt: </label>
                            <input name="weight" id="weight" type="text" size="3" maxlength="3" />
                            <label class=" font_small"> KG </label>
                            <label class="font_bold" style="padding-right: 2px;"></label>
                            <label class="font_bold"> Pain: </label>
                            <input name="pain" id="pain" type="text" size="4" maxlength="4" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <textarea name="physical_comments" id="physical_comments"  cols="74" rows="3"  > </textarea>
                        </div>
                    </div><!-- End of Section Six Top -->
                    <div class="initial_section_6_2"><!-- Begining of Section Six Bottom  -->
                        <div class="initial_section_6_left"><!-- Begining of Section Six Left -->
                            <div class="spacer"></div>
                            <div class="initial_section_6_3"><!-- Begining of Section Six Left Top -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> General </label>
                                    <label class="font_bold" style="padding-right: 40px;"></label>
                                    <input name="general" id="normal" type="radio" value="Normal" > Normal
                                   <label class="font_bold" style="padding-right: 7px;"></label>
                                    <input name="general" id="wasted" type="radio" value="Wasted" > Wasted
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="general" id="normal" type="radio" value="Acutely III" > Acutely III
                                     <label class="font_bold" style="padding-right: 20px;"></label>
                                    <input name="general" id="normal" type="radio" value="Pallor" > Pallor
                                     <label class="font_bold" style="padding-right: 12px;"></label>
                                    <input name="general" id="normal" type="radio" value="Icterus" > Icterus
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="general" id="normal" type="radio" value="Thrush" > Thrush
                                     <label class="font_bold" style="padding-right: 33px;"></label>
                                    <input name="general" id="normal" type="radio" value="Oedema" > Oedema
                                     <label class="font_bold" style="padding-right: 0px;"></label>
                                    <input name="general" id="normal" type="radio" value="Axillary" > Axillary
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="general" id="normal" type="radio" value="Lymph Nodes" > Lymph Nodes
                                     <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="general" id="normal" type="radio" value="Cervical" > Cervical
                                     <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="general" id="normal" type="radio" value="Inguinal" > Inguinal
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="general_comments" id="general"  cols="22" rows="5"  > </textarea>
                                </div>
                            </div><!-- Emd of Section Six Left Top -->
                            <div class="initial_section_6_4"><!-- Begining of Section Six Left Bottom -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> Chest / Lungs: </label>
                                    <label class="font_bold" style="padding-right: 4px;"></label>
                                    <input name="chest" id="chest" type="radio" value="normal" > Normal
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="chest" id="normal" type="radio" value="Resp.Distress" > Resp.Distress
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="chest" id="normal" type="radio" value="Wheezes" > Wheezes
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="chest" id="normal" type="radio" value="Crackles" > Crackles
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="chest" id="normal" type="radio" value="Breath Sounds" > Breath Sounds
                                    <label class="font_bold" style="padding-right: 0px;"></label>
                                    <input name="chest" id="normal" type="radio" value="Dullness" > Dullness
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="chest" id="normal" type="radio" value="Bronchial BS" > Bronchial BS
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="chest_comments" id="chest_comments"  cols="22" rows="5"  > </textarea>
                                </div>
                            </div><!-- End of Section Six Left Bottom -->
                        </div><!-- End of Section Four Left -->
                        <div class="initial_section_6_right"><!-- Begining of Section Six Right -->
                            <div class="spacer"></div>
                            <div class="initial_section_6_5"><!-- Begining of Section Six Right Top -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> CNS: </label>
                                    <label class="font_bold" style="padding-right: 32px;"></label>
                                    <input name="cns" id="normal" type="radio" value="Normal" > Normal
                                    <label class="font_bold" style="padding-right: 8px;"></label>
                                    <input name="cns" id="normal" type="radio" value="Neck Stiffness" > Neck Stiffness
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="cns" id="normal" type="radio" value="Paralysis" > Paralysis
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="cns" id="normal" type="radio" value="Brudzinski" > Brudzinski
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="cns" id="normal" type="radio" value="Confused" > Confused
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="cns_comments" id="cns_comments"  cols="22" rows="4"  > </textarea>
                                </div>
                            </div><!-- End of Section Six Right Top -->
                            <div class="initial_section_6_6"><!-- Bengining of Section Six Right Middle -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> Genitourinary: </label>
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="gen" id="gen" type="radio" value="normal" > Normal
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="gen" id="normal" type="radio" value="Bleeding" > Bleeding
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="gen" id="normal" type="radio" value="Discharge" > Discharge
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="gen" id="normal" type="radio" value="Ulceration" > Ulceration
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="gen_comments" id="gen_comments"  cols="22" rows="4"  > </textarea>
                                </div>
                            </div><!-- End of Section Six Right Middle -->
                            <div class="initial_section_6_7"><!-- Begining of Section Six Right Bottom -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> Wab Stage (or Karnofsky Score): </label>
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold" style="padding-right: 15px;"></label>
                                    <label> Working (70 - 100%) </label>
                                    <label class="font_bold" style="padding-right: 24px;"></label>
                                    <input name="wab_working" id="working" type="text" size="3" maxlength="3" />
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold" style="padding-right: 15px;"></label>
                                    <label> Ambulatory (50 - 70%) </label>
                                    <label class="font_bold" style="padding-right: 15px;"></label>
                                    <input name="wab_ambulatory" id="ambulatory" type="text" size="3" maxlength="2" />
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold" style="padding-right: 15px;"></label>
                                    <label> Bedridden (< 50%) </label>
                                    <label class="font_bold" style="padding-right: 30px;"></label>
                                    <input name="wab_bedridden" id="bedridden" type="text" size="3" maxlength="2" />
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> WHO Stage: </label>
                                    <label class="font_bold" style="padding-right: 15px;"></label>
                                    <input name="who" id="who_stage" type="text" size="3" maxlength="3" />
                                </div>
                            </div><!-- End of Section Six Right Bottom -->
                        </div><!-- End of Section Six Right -->
                        <div class="initial_section_6_centre"><!-- Begining of Section Six Centre -->
                            <div class="spacer"></div>
                            <div class="initial_section_6_5"><!-- Begining of Section Six Centre Top -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> Cardiovascular: </label>
                                    <label class="font_bold" style="padding-right: 14px;"></label>
                                    <input name="cardiovascular" id="normal" type="radio" value="normal" > Normal
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="cardiovascular" id="normal" type="radio" value="Murmur" > Murmur
                                    <label class="font_bold" style="padding-right: 5px;"></label>
                                    <input name="cardiovascular" id="normal" type="radio" value="Rub" > Rub
                                    <label class="font_bold" style="padding-right: 5px;"></label>
                                    <input name="cardiovascular" id="normal" type="radio" value="Irregular Rhythm" > Irregular Rhythm
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="cardiovascular_comments" id="cardiovascular_comments"  cols="22" rows="4"  > </textarea>
                                </div>
                            </div><!-- End of Section Six Centre Top -->
                            <div class="initial_section_6_6"><!-- Begining of Section Six Centre Middle -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> Abdomen: </label>
                                    <input name="abdomen" id="abdomen" type="radio" value="normal" > Normal
                                    <input name="abdomen" id="abdomen" type="radio" value="Tenderness" > Tenderness
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="abdomen" id="normal" type="radio" value="Hepatomegaly" > Hepatomegaly
                                    <label class="font_bold" style="padding-right: 2px;"></label>
                                    <input name="abdomen" id="normal" type="radio" value="Splenomegaly" > Splenomegaly
                                </div>
                                 <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="abdomen" id="normal" type="radio" value="Mass" > Mass
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="abdomen_comments" id="abdomen_comments"  cols="22" rows="3"  > </textarea>
                                </div>
                            </div><!-- End of Section Six Centre Middle -->
                            <div class="initial_section_6_7"><!-- Begining of Section Six Centre Bottom -->
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <label class="font_bold"> Skin: </label>
                                    <label class="font_bold" style="padding-right: 43px;"></label>
                                    <input name="skin" id="skin" type="radio" value="normal" > Normal
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <input name="skin" id="normal" type="radio" value="Rash" > Rash
                                     <label class="font_bold" style="padding-right: 30px;"></label>
                                    <input name="skin" id="normal" type="radio" value="Ulcers" > Ulcers
                                     <label class="font_bold" style="padding-right: 30px;"></label>
                                    <input name="skin" id="normal" type="radio" value="KS" > KS
                                </div>
                                <div class="spacer"></div>
                                <div class="section_row">
                                    <textarea name="skin_comments" id="skin_comments"  cols="22" rows="4"  > </textarea>
                                </div>
                            </div><!-- End of Section Six Centre Bottom -->
                        </div><!-- End of Section Six Centre -->
                    </div><!-- End of Section Six Bottom -->
                </div><!-- End of Section Six -->
                <div class="spacer"></div>
                <div class="section initial_section_7"><!-- Begining of Section Seven -->
                    <div class="spacer"></div>
                    <div class="initial_section_7_1"><!-- Begining of top Section -->
                        <div class="initial_section_7_top"><!-- Begining of Section Seven Top -->
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 18. Assessment: </label>
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="assessment " id="no_illness_found" type="radio" value="No Illness Found" /> No Illness Found
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="assessment" id="hiv_related_illness_ol" type="radio" value="HIV-Related Illness/OI" /> HIV-Related Illness/OI
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="assessment" id="non_hiv_related_illness" type="radio" value="Non-HIV-Related Illness" /> Non-HIV-Related Illness
                                <label class="font_bold" style="padding-right: 15px;"></label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <textarea name="assessment_comments" id="assessment_comments"  cols="74" rows="3"  > </textarea>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 19. Plan: </label>
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="plan " id="plan" type="radio" value="Lab Evaluation / TB Screen" > Lab Evaluation / TB Screen
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="plan" id="ol_prophylaxis_treatment" type="radio" value="OI Prophylaxis / Treatment" > OI Prophylaxis / Treatment
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="plan" id="treatment_preparation" type="radio" value="Treatment Preparation" > Treatment Preparation
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="plan" id="other" type="radio" value="other" > Other
                                <label class="font_bold" style="padding-right: 15px;"></label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <textarea name="plan_comments" id="plan_comments"  cols="74" rows="3"  > </textarea>
                            </div>
                        </div><!-- End of Section Seven Top -->
                    </div><!-- End of Top Section -->
                    <div class="initial_section_7_2"><!-- Begining of Lower Section -->
                        <div class="initial_section_7_left"><!-- Begining of Section Seven Left -->
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> 20. ARV Therapy </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy" id="continue_current_treatment" type="radio" value="continue_current_treatment" > Continue Current Treatment
                                 <label class="font_bold" style="padding-right: 20px;"></label>
                                <input name="arv_therapy" id="change_regimen" type="radio" value="change_regimen" > Change Regimen (Indicate Code)
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy" id="treatment_not_indicated" type="radio" value="treatment_not_indicated" > Treatment Not Indicated Now
                                <label class="font_bold" style="padding-right: 5px;"></label>
                                <input name="arv_therapy" id="stop_treatment" type="radio" value="stop_treatment" > Stop Treatment (Indicate Code)
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy" id="restart_treatment" type="radio" value="restart_treatment" > Restart Treatment
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="arv_therapy" id="start_new_treatment" type="radio" value="start_new_treatment" > Start New Treatment (Naive Patient)
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                
                                
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold"> Current Regimen </label>
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                 <input name="current_regimen" id="current_regimen" type="text" size="35" maxlength="35" />
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                               
                            </div>
                        </div><!-- End of Section Seven Left -->
                        <div class="initial_section_7_right"><!-- Begining of Section Seven Right -->
                            <div class="spacer"></div>
                            <div class="section_row">
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <label class="font_bold"> Therapy Change Codes </label>
                                <label class="font_bold_small"> (Check One) </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes" id="toxicity" type="checkbox" value="toxicity" > 1. Toxicity
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="arv_therapy_codes" id="drugs_not_available" type="checkbox" value="drugs_not_available" > 7. Drugs Not Available

                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes" id="treatment_failure" type="checkbox" value="treatment_failure" > 2. Treatment Failure
                                &nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes" id="migratory_patient" type="checkbox" value="migratory_patient" > 8. Migratory Patient;
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes" id="non_adherence" type="checkbox" value="non_adherence" > 3. Non Adherence 
                               <label class="font_bold" style="padding-right: 15px;"></label>
                                <label class="font_small"> Follow-up Difficult </label>
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes" id="drug_interaction" type="checkbox" value="drug_interaction" > 4. Drug Interaction
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="arv_therapy_codes" id="transfer" type="checkbox" value="transfer" > 9. Transfer To Other
                            </div>
                            <!--div class="spacer"></div-->
                            <div class="section_row">
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <label class="font_small"> Program </label>
                            </div>
                            <!--div class="spacer"></div-->
                            <div class="section_row">
                                <input name="arv_therapy_codes" id="pregnancy" type="checkbox" value="pregnancy" > 5. Pregnancy
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="arv_therapy_codes" id="inability_to_pay" type="checkbox" value="inability_to_pay" > 10. Inability To Pay
                            </div>
                            <div class="spacer"></div>
                            <div class="section_row">
                                <input name="arv_therapy_codes" id="non_adherence" type="checkbox" value="non_adherence" > 6. Patient Preference
                                <label class="font_bold" style="padding-right: 15px;"></label>
                                <input name="arv_therapy_codes" id="other" type="checkbox" value="other" > 11. Other
                            </div>
                        </div><!-- End of Section Seven Right -->
                    </div><!-- End of Lower Section -->

                </div><!-- End of Section Seven -->
                <div class="spacer"></div>
                <div class="section initial_section_8"><!-- Begining of Section Eight -->
                    <div class="initial_section_8_left"><!-- Begining of Section Eight Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 21. When is the Patient's Next Appointment? </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="next_appointment" id="1_week" type="radio" value="1_week" > 1 Week
                            <input name="next_appointment" id="2_months" type="radio" value="2_months" > 2 Months
                           <label class="font_bold" style="padding-right: 15px;"></label>
                            <label class="font_bold_small"> Date: </label>
                           <label class="font_bold" style="padding-right: 15px;"></label>
                            <select name="appointment_date" id="appointment_date"  >
                                <option value=""> Day </option>
                                <option value="1"> 1 </option>
                                <option value="2"> 2 </option>
                            </select>
                            <label> / </label>
                            <select name="appointment_date" id="appointment_date"  >
                                <option value=""> Month </option>
                                <option value="1"> 01 </option>
                                <option value="2"> 02 </option>
                            </select>
                            <label> /20 </label>
                            <select name="appointment_date" id="appointment_date"  >
                                <option value=""> Year </option>
                                <option value="01"> 01 </option>
                                <option value="02"> 02 </option>
                            </select>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="next_appointment" id="2_weeks" type="radio" value="2_weeks" > 2 Weeks
                            <input name="next_appointment" id="3_months" type="radio" value="3_months" > 3 Months 
                           <label class="font_bold" style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="next_appointment" id="4_weeks" type="radio" value="4 weeks"> 4 Weeks
                            <input name="next_appointment" id="6_months" type="radio" value="6 months" > 6 Months
                            <label class="font_bold" style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 22.Appointment Reason </label>
                            <label class="font_bold" style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="appointment_reason" id="followup" type="radio" value="followup" > Follow Up
                            <label class="font_bold" style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="appointment_reason" id="lab_tests" type="radio" value="lab_tests" > Lab Tests
                            <label class="font_bold" style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input name="appointment_reason" id="treatment_preparation" type="radio" value="treatment_preparation" > Treatment Preparation
                            <label class="font_bold" style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> 23. Print Name: </label>
                            <input name="print_name" id="print_name" size="35" type="text" maxlength="35" />
                        </div>
                    </div><!-- End of Section Eight Left -->
                    <div class="spacer"></div>
                    <div class="initial_section_8_right"><!-- Begining of Section Eight Right -->

                    </div><!-- End of Section Eight Right -->
                </div><!-- End of Section Eight -->
                <div class="spacer"></div>
                <div class="section initial_section_9"><!-- Begining of Section Nine -->
                    <div class="spacer"></div>
                    <div class="section_row">

                        <input type="button" name="button" value="Submit" onclick="javascript:get2(this.parentNode);">
                        <!--input type="submit" name="button" value="Normal Submit Button"-->
                        <input id="task" name="task" type="hidden" value="init2" />
                        <input id="enroll_no" name="enroll_no" type="text" value=""  style="display:none" />
                        <span id="myspan2"></span>
                    </div>
                </div><!-- End of Section Nine -->
            </form>
        </div><!-- End of Main Container Form -->
    </div>
</div><!--End Main Container-->


<script type="text/javascript" language="javascript">
    var http_request = false;
    function makeRequest2(url, parameters) {
        http_request = false;
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            http_request = new XMLHttpRequest();
            if (http_request.overrideMimeType) {
                // set type accordingly to anticipated content type
                //http_request.overrideMimeType('text/xml');
                http_request.overrideMimeType('text/html');
            }
        } else if (window.ActiveXObject) { // IE
            try {
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    http_request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {}
            }
        }
        if (!http_request) {
            alert('Cannot create XMLHTTP instance');
            return false;
        }
        http_request.onreadystatechange = alertContents2;
        http_request.open('POST', url + parameters, true);
        http_request.send(null);
    }

    function alertContents2() {
        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
                //alert(http_request.responseText);
                result = http_request.responseText;
                document.getElementById('myspan2').innerHTML = result;
            } else {
                alert('There was a problem with the request.');
            }
        }
    }

    function get2(obj) {
        var getstr = "?";
        for (var i=0; i<document.initial_evaluation2.elements.length; i++) {
            if (document.initial_evaluation2.elements[i].type=="text") {

                getstr += document.initial_evaluation2.elements[i].name + "=" + document.initial_evaluation2.elements[i].value + "&";
            }
            if (document.initial_evaluation2.elements[i].type== "checkbox") {
                if (document.initial_evaluation2.elements[i].checked) {
                    getstr += document.initial_evaluation2.elements[i].name + "=" + document.initial_evaluation2.elements[i].value + "&";
                } else {
                    getstr += document.initial_evaluation2.elements[i].name + "=&";
                }
            }
            if (document.initial_evaluation2.elements[i].type == "radio") {
                if (document.initial_evaluation2.elements[i].checked) {
                    getstr += document.initial_evaluation2.elements[i].name + "=" + document.initial_evaluation2.elements[i].value + "&";
                }
            }

            if (document.initial_evaluation2.elements[i].type == "SELECT") {
                var sel = document.initial_evaluation2.elements[i];
                getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
            }

            if (document.initial_evaluation2.elements[i].type=="hidden") {
                getstr += document.initial_evaluation2.elements[i].name + "=" + document.initial_evaluation2.elements[i].value + "&";
            }
            if (document.initial_evaluation.elements[i].tagName=="textarea") {
                getstr += document.initial_evaluation2.elements[i].name + "=" + document.initial_evaluation2.elements[i].value + "&";
            }
        }
        makeRequest2('initial_evaluation2.php', getstr);
    }
</script>