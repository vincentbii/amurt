
<div id="laboratory" class="main_container"><!-- Beginning of Main Container -->
    <div align="center">
        <div class="main_container_form" id="labform"><!-- Beginning of Main Container Form -->
            <form class="" action="lab_order.php" method="post" name="lab_order_form" id="lab_order_form">
                <div class="section lab_section_1"><!-- Beginning of Section One -->
                    <div class="lab_section_1_left"><!-- Beginning of Section One Left -->
                        <div class="spacer"></div>
                        <div class="section_row">

                            <label for="patient_name" class="font_bold"> Patient name: </label>&nbsp;&nbsp;&nbsp;
                            <input name="pnames" id="pnames" type="text" size="35" maxlength="45" value=""/>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label for="id_1" class="font_bold" style="padding-right: 10px"> ID: </label>
                            <input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" style="margin-right: 10px"/>

                            -<input name="enroll_no" id="enroll_no" type="text" size="4" maxlength="4" style="margin-left: 10px; margin-right:56px "/>

                            <label for="id_1" class="font_bold"> Age: </label>
                            <input name="age" id="age" type="text" size="3" maxlength="3"  style="margin-left: 10px"/>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                            <label style="margin-right: 10px; margin-left: 5px"> Satellite#</label>
                            <label>Patient Enrollment#</label>

                        </div>
                    </div><!-- End of Section One Left -->
                    <div class="lab_section_1_right"><!-- Beginning of Section One Right -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class=" font_bold"> Existing Hosp/ Clinic No: </label>

                            <input name="hosp_no" id="hosp_no" type="text" size="35" maxlength="45"  style="margin-left: 10px"/>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row" >
                            <label class="font_bold"  style="margin-right: 5px;margin-left: 19px"> 1. </label>
                            <input name="preclinic_labs" id="preclinic_labs" type="checkbox" value="preclinic_labs">
                            &nbsp;
                            <label class="font_bold"> Preclinic Labs </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row" style="margin-left: 19px" id="george">
                            <label class="font_bold" style="margin-right: 5px"> 2. Lab to be Done on: </label>

                            <div id="lor_preclinic" align="justify" style="float:right;" ></div>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">

                        </div>
                    </div><!-- End of Section One Right -->
                </div><!-- End of Section One -->
                <div class="spacer"></div>
                <div class="section lab_section_2"><!-- Beginning of Section Two -->
                    <div class="lab_section_2_left"><!-- Beginning of Section Two Left -->
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> 3. ARV related Labs (AIDSRelief to Provide) </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Immunology </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row" style=" border: 1px solid black; min-height: 120px; width: 95%;">
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_bold"> Orders </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="font_bold"> Results </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab01"  id="lab01" type="checkbox" value="lab01" /> HIV Serology
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab01_true_results" id="lab01_results" type="radio" value="true" /> Pos
                                <input name="lab01_false_results" id="lab01_results" type="radio" value="false" /> Neg
                                <input name="lab01_confirm_results" id="lab01_results" type="radio" value="confirmatory" /> Confirmatory
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab02" id="lab02" type="checkbox" value="lab02" /> CD4
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab02_results" id="lab02_results" type="text" size="4" maxlength="4" />
                                <label class="font_small"> c/mm<sup>3</sup> </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab02_extra_results" id="lab02_extra_results" type="text" size="2" maxlength="2" />

                                <label class="font_small"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab03" id="lab03" type="checkbox" value="lab03" /> Viral Load
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab03_results" id="lab03_results" type="text" size="6" maxlength="6" />
                                <label class="font_small"> copies/ ml </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab04" id="lab04" type="checkbox" value="lab04" /> Store Plasma
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab04_results" id="lab04_results" type="radio" value="true" /> Stored
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Hematology </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row" style=" border: 1px solid black; min-height: 160px; width: 95%;">
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab07" id="lab07" type="checkbox" value="lab07" /> HCT
                                &nbsp;&nbsp;&nbsp;
                                <input name="lab07_results" id="lab07_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> % </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab09" type="checkbox" value="lab09" /> HB
                                &nbsp;&nbsp;&nbsp;
                                <input name="lab09_results" id="lab09_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> g/ dl </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab08" id="lab08" type="checkbox" value="lab08" > WBC
                                &nbsp;&nbsp;&nbsp;
                                <input name="lab08_results" id="lab08_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> x 10<sup>9</sup> c/L </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                <input name="lab10" id="lab10" type="checkbox" value="lab10"/> Diff Neut:
                                &nbsp;&nbsp;
                                <input name="lab10_results" id="lab10_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> 10<sup>3</sup> c/mcl </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab10_extra_results" id="lab10_extra_results" type="text" size="2" maxlength="2" />
                                <label class="font_small"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                <input name="lab34" id="lab34" type="checkbox" value="lab34" />  Lymp:
                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                <input name="lab34_results" id="lab34_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> 10<sup>3</sup> c/mcl </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab34_extra_results" id="lab34_extra_results" type="text" size="2" maxlength="2" />
                                <label class="font_small"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">

                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                <input name="lab11" id="lab11" type="checkbox" value="lab11" />  Mono:
                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                <input name="lab11_results" id="lab11_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> 10<sup>3</sup> c/mcl </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab11_extra_results" id="lab11_extra_results" type="text" size="2" maxlength="2" />
                                <label class="font_small"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                <input name="lab11 id="lab35" type="checkbox" value="lab35" />  Eosino:
                                       &nbsp;&nbsp;&nbsp; &nbsp;
                                       <input name="lab11_results" id="lab11_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> 10<sup>3</sup> c/mcl </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab11_extra_results" id="lab11_extra_results" type="text" size="2" maxlength="2" />
                                <label class="font_small"> % </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab12" id="lab12" type="checkbox" value="lab12" /> Platelets
                                &nbsp;&nbsp;&nbsp;
                                <input name="lab12_results" id="lab12_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> x 10<sup>9</sup> c/L </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">

                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Chemistry </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row" style=" border: 1px solid black; min-height: 130px; width: 95%;">
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab13" id="lab13" type="checkbox" value="lab13" /> AST/ SGOT
                                &nbsp;&nbsp;&nbsp;
                                <input name="lab13_results" id="lab13_results" type="text" size="4" maxlength="4" />
                                <label class="font_small"> U/L </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab14" id="lab14" type="checkbox" value="lab14" /> ALT/ SGPT
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab14_results" id="lab14_results" type="text" size="4" maxlength="4" />
                                <label class="font_small"> U/L </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab15" id="lab15" type="checkbox" value="lab15" > Creatinine
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab15_results" id="lab15_results" type="text" size="3" maxlength="3" />
                                <label class="font_small"> mg/dL or mmole/dL </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab16" id="lab16" type="checkbox" value="lab16" > Serum VDRL

                                <input name="lab16_results" id="lab16_results" type="radio" value="serum_vdrl_positive" /> Pos
                                <input name="lab16_results" id="lab16_results" type="radio" value="serum_vdrl_negative" /> Neg
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab17" id="lab17" type="checkbox" value="lab17" /> Pregnancy
                                &nbsp;&nbsp;
                                <input name="lab17_true_results" id="lab17_results" type="radio" value="true" /> Pos
                                <input name="lab17_false_results" id="lab17_results" type="radio" value="false" /> Neg
                            </div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab18" id="lab18" type="checkbox" value="lab18" /> Stool O/P
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab18_true_results" id="lab18_results" type="radio" value="true" /> Pos
                                <input name="lab18_false_results" id="lab18_results" type="radio" value="false" /> Neg
                            </div>
                        </div>
                    </div><!-- End of Section Two Left -->
                    <div class="lab_section_2_right"><!-- Begining of Section Two Right -->
                        <div class="spacer"></div>
                        <div class=" section_row">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Microbiology </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row" style=" border: 1px solid black; min-height: 220px; width: 95%;">
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab05" id="lab05" type="checkbox" value="lab05" /> Malaria Parasite
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab05_true_results" id="lab05_results" type="radio" value="true" /> Pos
                                <input name="lab05_false_results" id="lab05_results" type="radio" value="false" /> Neg
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab06" id="lab06" type="checkbox" value="lab06" /> Serum Crypto. Ag
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input name="lab06_true_results" id="lab06_results" type="radio" value="true" /> Pos
                                <input name="lab06_false_results" id="lab06_results" type="radio" value="false" /> Neg      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                <label class="font_bold"> Sputum </label>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab21" id="lab21" type="checkbox" value="lab21" /> AFB
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class=""> #1 </label>
                                <input name="lab21_results" id="lab21_results" type="text" size="5" maxlength="5" />
                                &nbsp;&nbsp;&nbsp;
                                <label class=""> #2 </label>
                                <input name="lab21_2results" id="lab21_2results" type="text" size="5" maxlength="5" />
                                &nbsp;&nbsp;&nbsp;
                                <label class=""> #3 </label>
                                <input name="lab21_3results" id="lab21_3results" type="text" size="5" maxlength="5" />
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab22" id="lab22"  type="checkbox" value="lab22" /> Gram Stain
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;&nbsp;&nbsp;
                                <textarea name="lab22_results" id="lab22_results"  cols="22" rows="6"> </textarea>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Urine </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row" style=" border: 1px solid black; min-height: 235px; width: 95%;">
                            <div class="spacer"></div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab19" id="lab19" type="checkbox" value="lab19" /> Urinalysis
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <textarea name="lab19_results" id="lab19_results"  cols="22" rows="5" " > </textarea>
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <input name="lab20" id="lab20" type="checkbox" value="lab20" /> Culture/ Sensitivity
                            </div>
                            <div class="spacer"></div>
                            <div class=" section_row">
                                &nbsp;
                                <textarea name="lab20_results" id="lab20_results"  cols="19" rows="5" " > </textarea>
                            </div>
                        </div>
                    </div><!-- End of Section Two Right -->
                </div><!-- End of Section Two -->
                <div class="spacer"></div>
                <div class="section lab_section_3"><!-- Begining of Section Three -->
                    <div class="lab_section_3_left"><!-- Begining of Section Three Left -->
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class=" font_bold"> CSF </label>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class=" font_bold"> Results </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab23" id="lab23" type="checkbox" value="lab23" > Cryptococcal Ag
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab23_true_results" id="lab23_results" type="radio" value="cryptococcal_ag_positive" /> Pos
                            <input name="lab23_false_results" id="lab23_results" type="radio" value="cryptococcal_ag_negative" /> Neg
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name=" lab24" id=" lab24" type="checkbox" value="lab24" /> CSF India Ink
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab24_true_results" id="lab24_results" type="radio" value="csf_india_ink_positive" /> Pos
                            <input name="lab24_false_results" id="lab24_results" type="radio" value="csf_india_ink_negative" /> Neg
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                            <input name="lab25" id="lab25" type="checkbox" value="lab25" /> CSF Gram Stain
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <textarea name="lab25_resuts" id="lab25_resuts"  cols="20" rows="2" " > </textarea>

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                            <input name="lab26" id="lab26" type="checkbox" value="lab26" /> Culture
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab26_results" id="lab26_results"  cols="20" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab27" id="lab27" type="checkbox" value="lab27" /> CSF VDRL
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab27_true_results" id="lab27_results" type="radio" value="csf_vdrl_positive" /> Pos
                            <input name="lab27_false_results" id="lab27_results" type="radio" value="csf_vdrl_negative" /> Neg
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <!--input name="lab28" id="lab28" type="checkbox" value="lab28" /--> Biochemistry
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <input name="lab28" id="lab28" type="checkbox" value="lab28" />  Glucose:
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <input name="lab28_results" id="lab28_results" type="text" size="4" maxlength="4" />
                            <label class="font_small"> mg/dL or mmole/dL </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <input name="lab32" id="lab32" type="checkbox" value="lab32" />  Protein:
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab32_results" id="lab32_results" type="text" size="4" maxlength="4" />
                            <label class="font_small"> mg/dL or mmole/dL </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                    </div><!-- End of Section Three Left -->
                    <div class="lab_section_3_right"><!-- Beginning of Section Three Right -->
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class=" font_bold"> CSF </label>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class=" font_bold"> Results </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
<!--                            <input name="csf_test[]" type="checkbox" value="csf_cell_count" >--> Cell Count
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab29" id="lab29" type="checkbox" value="lab29"> RBCs
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab29_results" id="lab29_results"  cols="20" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab30" id="lab30" type="checkbox" value="lab30"> WBCs
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab30_results" id="lab30_results"  cols="20" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
<!--                            <input name="csf_test[]" type="checkbox" value="31" >--> Culture
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="lab31" id="lab31" type="checkbox" value="lab31"> Neutrophils
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab31_results" id="lab31_results"  cols="20" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">

                        </div>
                    </div><!-- End of Section Three Right -->
                </div><!-- End of Section Three -->
                <div class="spacer"></div>
                <div class="section lab_section_4"><!-- Begining of Section Four -->
                    <div class="lab_section_4_left"><!-- Begining of Section Four Left -->
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class=" font_bold" > Chest X-Ray </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> LMP </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;
                            <textarea name="xray_lmp_results" id="xray_lmp_results"  cols="23" rows="2" " > </textarea>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Reason for Request </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;
                            <textarea name="xray_reason_results" id="xray_reason_results"  cols="23" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Report </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="xray_report1_results" id="xray_report_results" type="radio" value="chest_xray_normal" > Normal
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input name="xray_report2_results" id="xray_report_results" type="radio" value="chest_xray_abnormal" > Abnormal
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Additional Labs </label>
                            <label class="font_small"> (Check if AIDSRelief is to provide for test) </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="font_bold"> Results </label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab33" id="lab33" type="checkbox" value="lab33" onclick="openLab()" >
                            &nbsp;&nbsp;&nbsp;
                            <input name="lab33_results" id="lab33_results" type="text" size="60" maxlength="60" value=""/>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab33_extra_results" id="lab33_extra_results"  cols="30" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab36" id="lab36" type="checkbox" value="lab36" >
                            &nbsp;&nbsp;&nbsp;
                            <input name="lab36_results" id="lab36_results" type="text" size="60" maxlength="60" />
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab36_extra_results" id="lab36_extra_results"  cols="30" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab37" id="lab37" type="checkbox" value="lab37" >
                            &nbsp;&nbsp;&nbsp;
                            <input name="lab37_results" id="lab37_results" type="text" size="60" maxlength="60" />
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab37_extra_results" id="lab37_extra_results"  cols="30" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab38" id="lab38" type="checkbox" value="lab38" >
                            &nbsp;&nbsp;&nbsp;
                            <input name="lab38_results" id="lab38_results" type="text" size="60" maxlength="60" />
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab38_extra_results" id="lab38_extra_results"  cols="30" rows="2" " > </textarea>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <input name="lab39" id="lab39" type="checkbox" value="lab39" >
                            &nbsp;&nbsp;&nbsp;
                            <input name="lab39_results" id="lab39_results" type="text" size="60" maxlength="60" />
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <textarea name="lab39_extra_results" id="lab39_extra_results"  cols="30" rows="2" " > </textarea>
                        </div>
                    </div><!-- End of Section Four Left -->
                    <div class="lab_section_4_right"><!-- Begining of Section Four Right -->


                    </div><!-- End of Section Four Right -->
                </div><!-- End of Section Four -->
                <div class="spacer"></div>
                <div class="section lab_section_5"><!-- Begining of Section Five -->
                    <div class="spacer"></div>
                    <div class="lab_section_5_left"><!-- Begining of Section Five Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />&nbsp;&nbsp;

                            <input id="Reset" type="reset" value="Reset" />
                            <input type="text" size="20" id="queryType" name="queryType">
                        </div>
                    </div><!-- End of SSetion Five Left -->
                    <div class="lab_section_5_right"><!-- Begining of Section Five Right -->

                    </div><!-- End of Section Five Right -->
                </div><!-- End of Section Five -->

            </form>
        </div><!-- End of Main Container Form -->
    </div>
</div><!-- End of Main Container -->


<script type="text/javascript">


    function getLabtests(str)
    {
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="getLabTests.php?enrollno="+str;
        url=url+"&sid="+Math.random();
        url=url+"&task=filtered";
        xmlhttp.onreadystatechange=stateChanged;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);

    }

    function stateChanged()
    {
        //get payment description
        if (xmlhttp.readyState==4)
        {
            var data = eval('(' + xmlhttp.responseText + ')');
            for (var i=0;i<data.labTest.length;i++)
            {

                var addrField = document.getElementById('pnames');
                addrField.value = data.labTest[0].test.pname;
                document.lab_order_form.age.value=data.labTest[0].test.age;
                document.lab_order_form.lab_date.value=data.labTest[0].test.testDate;
                document.lab_order_form.lab_date.value=data.labTest[0].test.pid;
                for(var j=0; j<document.lab_order_form.elements.length; j++)
                {
                    if(document.lab_order_form.elements[j].type=="checkbox")
                    {
                        if(document.lab_order_form.elements[j].id==data.labTest[i].test.testID){
                            document.lab_order_form.elements[j].checked=true;
                            document.lab_order_form.elements[j].disabled=true;

                        }
                    }

                }
            }
        }
    }

    function getLabResults(str)
    {
        xmlhttp2=GetXmlHttpObject();
        if (xmlhttp2==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="getLabTests.php?enrollno="+str;
        url=url+"&sid="+Math.random();
        url=url+"&task=results";
        xmlhttp2.onreadystatechange=stateChanged2;
        xmlhttp2.open("POST",url,true);
        xmlhttp2.send(null);

    }

    function stateChanged2()
    {
        //get payment description
        if (xmlhttp2.readyState==4)
        {
            var data2 = eval('(' + xmlhttp2.responseText + ')');
            for (var s=0;s<data2.labResults.length;s++)
            {

                var addrField = document.getElementById('pnames');
             addrField.value = data2.labResults[0].results.pname;
               document.lab_order_form.age.value=data2.labResults[0].results.age;
               document.lab_order_form.lab_date.value=data2.labResults[0].results.testDate;
                for(var k=0; k<document.lab_order_form.elements.length; k++)
                {
                    if(document.lab_order_form.elements[k].type=="text")
                    {
                        var str2=document.lab_order_form.elements[k].id;
                        var myStrID=str2.substr(0,5);

                        if(myStrID==data2.labResults[s].results.testID){
                            document.lab_order_form.elements[k].value=data2.labResults[s].results.result;
                        }
                    }
                    if(document.lab_order_form.elements[k].type=="radio")
                    {
                        var str3=document.lab_order_form.elements[k].name;
                        var myStrID=str3.substr(0,5);

//                        for (var l = 0; l < lab_order_form.str3.length; l++) {
                            if (lab_order_form.elements[k].value==data2.labResults[s].results.result && myStrID==data2.labResults[s].results.testID) {
                                lab_order_form.elements[k].checked=true;
                            }
//                        }

                    }

                }
            }
        }
    }

    function GetXmlHttpObject()
    {
        if (window.XMLHttpRequest)
        {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject)
        {
            // code for IE6, IE5
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }


</script>