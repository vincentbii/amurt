
Ext.BLANK_IMAGE_URL="../include/Extjs/resources/images/default/s.gif";
Ext.onReady(function(){
	
    Ext.QuickTips.init();

    var tools = [{
        id:'gear',
        handler: function(){
            Ext.Msg.alert('Message', 'The Settings tool was clicked.');
        }
    },{
        id:'close',
        handler: function(e, target, panel){
            panel.ownerCt.remove(panel, true);
        }
    }];


    var facility = [
    '<table width="100%" border="0" cellspacing="0" cellpadding="0">',
    '<tr>',
    ' <td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    ' </tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Ever Enrolled Patients:</td>',
    ' <td> </td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    ' <td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Females Enrolled:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    ' </tr>',
    ' <tr>',
    '<td>Males Enrolled:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Total Number of Active Patients:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Active Non-ART Patients:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>ART Patients:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>ART Mortality:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Number Lost to Followup:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>ART Unknown:</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '</table>'
    ];

    var nonart = [
    '<table width="100%" border="0" cellspacing="0" cellpadding="0">',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td colspan="2" align="center" style="font-weight:bold">Males</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>0 - 1 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>2 - 4 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>5 - 14 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>15+ Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td colspan="2" align="center" style="font-weight:bold">Females</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>0 - 1 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>2 - 4 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>5 - 14 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>15+ Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '</table>'
    ];
       
    var art =[
    '<table width="100%" border="0" cellspacing="0" cellpadding="0">',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td colspan="2" align="center" style="font-weight:bold">Males</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>0 - 1 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>2 - 4 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>5 - 14 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>15+ Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td colspan="2" align="center" style="font-weight:bold">Females</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>0 - 1 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>2 - 4 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>5 - 14 Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>15+ Years</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '</table>'
    ];
        
    var contentPanel = new Ext.Panel({
        id: 'content-panel',
        region: 'center', 
        layout: 'card',
        margins: '2 5 5 0',
        activeItem: 0,
        border: false,
        items: [

        {
            xtype:'portal',
            margins:'35 5 5 0',
            height: '100%',
            title:'ARV Portal',
            items:[{
                columnWidth:.33,
                style:'padding:10px 0 10px 10px',
                items:[{
                    title: 'Facility Statictics',
                    autoScroll: true,
                    tools: tools,
                    html: facility.join('')

                },{
                    title: 'Percent Adult and Children in Pharmacy',
                    width: 250,
                    height: 300,
                    tools: tools,
                    items:[pieChart]
                }]
            },{
                columnWidth:.33,
                style:'padding:10px 0 10px 10px',
                items:[{
                    title: 'Non-ART Patients breakdown by Age and Sex',
                    tools: tools,
                    html: nonart.join('')
                },{
                    title: 'Another Panel 2',
                    autoScroll: true,
                    tools: tools,
                    items:[sChart]
                //                                html: Ext.example.shortBogusMarkup
                }]
            },{
                columnWidth:.33,
                style:'padding:10px 0 10px 10px',
                items:[{
                    title: 'ART Patient breakdown by Age and Sex',
                    tools: tools,
                    html: art.join('')
                },{
                    title: 'No of Lab Test Done Per Month',
                    autoScroll: true,
                    tools: tools,
                    items:[colChart]
                }]
            }]
        },
        start,adult, followup, tracking, family, enrollment, profile, initialData, initialData2, laboratory,  pediatric,
                
        drugGrid, oi_drugGrid, pcareGrid, ccareGrid,pharmGrid, winLab
        
        ]
    });
    
    
    var treePanel = new Ext.tree.TreePanel({
        id: 'tree-panel',
        title: 'ARV Forms',
        region:'north',
        split: true,
        height: 200,
        minSize: 150,
        autoScroll: true,
        
        
        rootVisible: true,
        lines: true,
        singleExpand: true,
        useArrows: true,
        
        loader: new Ext.tree.TreeLoader({
            dataUrl:'tree-data.json'
        }),
        
        root: new Ext.tree.AsyncTreeNode({

            text: 'ART Menus',
            id:'start',
            expanded: false
        })
    });
    
    
    treePanel.on('click', function(n){
        var sn = this.selModel.selNode || {}; 
        if(n.leaf && n.id != sn.id){ 
            if(n.id == 'laboratory'){
                Ext.getCmp('content-panel').layout.setActiveItem(winLab);
            }else {
                Ext.getCmp('content-panel').layout.setActiveItem(n.id + '-panel');
            }
        }
    });

    var detailsStore = new Ext.data.JsonStore({
        url: 'patdet.php',
        root: 'details',
        totalProperty: 'total',
        fields: ['pid', 'name_first','name_2','name_last','selian_pid','age','current_dept_nr','date_birth','sex','civil_status',
        'addr_zip', 'cellphone_1_nr', 'citizenship', 'region', 'district', 'contact_person', 'contact_relation','country_no','lptf_no','satellite_no']
    });
	
	
    var filters = new Ext.ux.grid.GridFilters({
        autoReload: false, 
        local: true, 
        filters: [{
            type: 'numeric',
            width: 30,
            dataIndex: 'selian_pid'
        }, {
            type: 'string',
            width: 80,
            dataIndex: 'name_first'
        }, {
            type: 'string',
            width: 80,
            dataIndex: 'name_2'
        }, {
            type: 'string',
            width: 80,
            dataIndex: 'name_last'
        }
        ]
    });    


    var createColModel = function (finish, start) {

        var columns = [{
            dataIndex: 'selian_pid',
            header: 'Patient ID',
            width: 60,
            filterable: true,
            filter: {
                type: 'numeric'
            }
        }, {
            dataIndex: 'name_first',
            header: 'First Name',
            width: 80,
            filter: {
                type: 'string'
            }
        }, {
            dataIndex: 'name_2',
            header: 'Middle Name',
            width: 80,
            filter: {
                type: 'string'
            }
        }, {
            dataIndex: 'name_last',
            header: 'Last Name',
            width: 80,
            filter: {
                type: 'string'
            }
        }];

        return new Ext.grid.ColumnModel({
            columns: columns.slice(start || 0, finish),
            defaults: {
                sortable: true
            }
        });
    };
    
    var detailsGrid = new Ext.grid.GridPanel({
        id: 'grid-details' ,
        title: 'Patient Details',
        region: 'center',
        store: detailsStore,
        colModel: createColModel(4),
        loadMask: true,
        plugins: [filters],
        
        listeners: {
            render: {
                fn: function(){
                    detailsStore.load({
                        params: {
                            start: 0,
                            limit: 20
                        }
                    });
                }
            }
        },
		
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function(sm, row, rec) {
                    var gridrecord = detailsGrid.getSelectionModel().getSelected();
                    if (gridrecord == undefined){
                        alert('no good');
                    }
                    else{
                        document.family_info.patient_name.value=gridrecord.get('name_first') + " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.family_info.patient_enrollment_no.value = gridrecord.get('selian_pid');
                        document.family_info.hosp_no.value = gridrecord.get('pid');
                        document.family_info.country_no.value = gridrecord.get('country_no');
                        document.family_info.lptf_no.value = gridrecord.get('lptf_no');
                        document.family_info.satellite_no.value = gridrecord.get('satellite_no');
                        
                        document.hiv_care.patient_name.value=gridrecord.get('name_first')+ " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.hiv_care.patient_enrollment_no.value = gridrecord.get('selian_pid');
                        document.hiv_care.hosp_no.value = gridrecord.get('pid');
                        document.hiv_care.age_is.value = gridrecord.get('age');
                        document.hiv_care.art_dob.value = gridrecord.get('date_birth');
                        document.hiv_care.contact_address.value = gridrecord.get('addr_zip');
                        document.hiv_care.contact_phone_no.value = gridrecord.get('cellphone_1_nr');
                        document.hiv_care.contact_village.value = gridrecord.get('citizenship');
                        document.hiv_care.contact_province.value = gridrecord.get('region');
                        document.hiv_care.contact_district.value = gridrecord.get('district');
                        document.hiv_care.emergency_contact_name.value = gridrecord.get('contact_person');
                        document.hiv_care.emergency_contact_rship.value = gridrecord.get('contact_relation');
                        document.hiv_care.country_no.value = gridrecord.get('country_no');
                        document.hiv_care.lptf_no.value = gridrecord.get('lptf_no');
                        document.hiv_care.satellite_no.value = gridrecord.get('satellite_no');
                        document.hiv_care.satellite_no2.value = gridrecord.get('satellite_no');
                        
                        if (gridrecord.get('sex') == 'm'){

                            document.hiv_care.male.checked = true;
                        }else{
                            document.hiv_care.female.checked = true;
                        }


                        if(gridrecord.get('civil_status') == 'married'){

                            document.hiv_care.married.checked = true;

                        }else if(gridrecord.get('civil_status') == 'single'){

                            document.hiv_care.single.checked = true;
                        } else if(gridrecord.get('civil_status') == 'widowed'){

                            document.hiv_care.widowed.checked = true;

                        }else if(gridrecord.get('civil_status') == 'divorced'){

                            document.hiv_care.divorced.checked = true;

                        } else if(gridrecord.get('civil_status') == ' '){

                            document.hiv_care.not_applicable.checked = true;

                        }
                        
                        document.initial_evaluation.patient_name.value = gridrecord.get('name_first')+ " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.initial_evaluation.enrollment_no.value = gridrecord.get('selian_pid');
                        document.initial_evaluation.hosp_no.value = gridrecord.get('pid');
                        document.initial_evaluation.country_no.value = gridrecord.get('country_no');
                        
                        document.contact_care_tracking.patient_name.value = gridrecord.get('name_first') + " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.contact_care_tracking.patient_enrollment_no.value = gridrecord.get('selian_pid');
                        document.contact_care_tracking.hosp_no.value = gridrecord.get('pid');
                        document.contact_care_tracking.satellite_no.value = gridrecord.get('satellite_no');
                       
                        document.arv_followup.patient_name.value = gridrecord.get('name_first') + " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.arv_followup.patient_enrollment_no.value = gridrecord.get('selian_pid');
                        document.arv_followup.hosp_no.value = gridrecord.get('pid');
                        document.arv_followup.satellite_no.value = gridrecord.get('satellite_no');
                        
                        document.adult_pharmacy.patient_name.value = gridrecord.get('name_first')+ " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.adult_pharmacy.enrollment_no.value = gridrecord.get('selian_pid');
                        document.adult_pharmacy.hosp_no.value = gridrecord.get('pid');
                        document.adult_pharmacy.age.value = gridrecord.get('age');
                        document.adult_pharmacy.satellite_no.value = gridrecord.get('satellite_no');
                        
                        document.pediatric_pharmacy.patient_name.value = gridrecord.get('name_first') + " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.pediatric_pharmacy.patient_enrollment_no.value = gridrecord.get('selian_pid');
                        document.pediatric_pharmacy.hosp_no.value = gridrecord.get('pid');
                        //                        document.pediatric_pharmacy.dob.value = gridrecord.get('date_birth');
                       
                        document.care_profile.patient_name.value = gridrecord.get('name_first') + " " + gridrecord.get('name_2')  + " " + gridrecord.get('name_last');
                        document.care_profile.patient_enrollment_no.value = gridrecord.get('selian_pid');
                        document.care_profile.hosp_no.value = gridrecord.get('pid');
                        document.care_profile.dob.value = gridrecord.get('date_birth');
                        document.care_profile.satellite_no.value = gridrecord.get('satellite_no');
                        
                        document.lab_order_form.pnames.value=gridrecord.get('name_first')+" "+gridrecord.get('name_2') +" "+gridrecord.get('name_last');
                        document.lab_order_form.age.value=gridrecord.get('age');
                        document.lab_order_form.hosp_no.value=gridrecord.get('pid');
                        document.lab_order_form.enroll_no.value=gridrecord.get('selian_pid');
                        document.lab_order_form.satellite_no.value=gridrecord.get('satellite_no');

                    }
                }
            }
        }),

        tbar: [{
            text: 'New Patients ',
            tooltip: 'Click view New Patients Who do not have an Initial Evaluation Form',
            handler: function ()
            {
                //            Ext.getCmp('content-panel').layout.setActiveItem('adult-panel');
                //                Ext.getCmp('details-details').detailsGrid.hide();
                Ext.MessageBox.alert('Success','New Patients List...');

            }

        },{
            text: 'View Patients ',
            tooltip: 'Click to View Patients with an Initial Evaluation Form',
            iconCls:'refresh',
            handler: function ()
            {
                //                Ext.getCmp('detailsGrid').layout.setActiveItem('pediatric-panel');
                Ext.MessageBox.alert('Success','View Patient List...');
            }

        }],


        bbar: new Ext.PagingToolbar({
            store: detailsStore,
            pageSize: 20,
            plugins: [filters]
        })
    });
	
    var viewPort=new Ext.Viewport({
        layout: 'border',
        items: [{
            xtype: 'box',
            region: 'north',
            applyTo: 'header',
            height: 2
        },{
            layout: 'border',
            id: 'layout-browser',
            region:'west',
            border: false,
            split:true,
            margins: '2 0 5 5',
            width: 275,
            minSize: 100,
            maxSize: 500,
            items: [treePanel, detailsGrid]
        },
        contentPanel
        ]
    });
    viewPort.render(document.body);
});
    
