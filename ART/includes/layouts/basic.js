
var start = {
    id: 'Facility Statistics',
    title: 'Start Page',
    //    bodyStyle: 'padding:25px',
    //    contentEl: 'start-div'
    items: {
        xtype:'portal',
        margins:'35 5 5 0',
        height: '100%',
        items:[{
            columnWidth:.50,
            style:'padding:10px 0 10px 10px',
            items:[{
                title: 'Cost Centers',
                //                                        layout:'fit',
//                tools: tools,
                items: []
            },{
                title: 'No of Employee in each Departments',
//                tools: tools,
                items:[]
            }]
        },{
            columnWidth:.50,
            style:'padding:10px 0 10px 10px',
            items:[{
                title: 'Employee Salary by Month',
//                tools: tools,
                items:[]
            },{
                title: 'Another Panel 2',
//                tools: tools,
                html: Ext.example.shortBogusMarkup
            }]
        }]
    }
};

/*
 * ================  Adult Pharmacy Form config  =======================
 */
var adult = {
    Xtype: 'panel',
    id: 'adult-panel',
    title: 'Adult Pharmacy Form',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'adult'
};

/*
 * ================  ARV Follow-up config  =======================
 */
var followup ={
    Xtype: 'panel',
    id: 'followup-panel',
    title: 'ARV Follow-up',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'followup'
};

/*
 * ================  Contact & Care Tracking config  =======================
 */
var tracking = {
    Xtype: 'panel',
    id: 'tracking-panel',
    title: 'Contact & Care Tracking',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'tracking'
};

/*
 * ================  Family Information config  =======================
 */
var family = {
    Xtype: 'panel',
    id: 'family-panel',
    title: 'Family Information',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'family'
};

/*
 * ================  HIV Care Enrollment config  =======================
 */

var enrollment = {
    Xtype: 'panel',
    id: 'enrollment-panel',
    title: 'HIV Care Enrollment',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'enrollment'
};
/*
     * ================  HIV Care Patient Profile config  =======================
     */
var profile = {
    Xtype: 'panel',
    id: 'profile-panel',
    title: 'HIV Care Patient Profile',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'profile'
};

/*
     * ================  Initial Evaluation config  =======================
     */
var initialData = {
    Xtype: 'panel',
    id: 'initialData-panel',
    title: 'Initial Evaluation',
    autoScroll: true,
    autoWidth: true,
    items:[{
        contentEl:'initialData'
    }]
};


var initialData2 = {
    Xtype: 'panel',
    id: 'initialData2-panel',
    title: 'Initial Evaluation',
    autoScroll: true,
    autoWidth: true,
    items:[{
        contentEl: 'initialData2'
    }]
};
/*
     * ================  Laboratory Order/ results config  =======================
     */
var laboratory = {
    Xtype: 'panel',
    id: 'laboratory-panel',
    title: 'Laboratory Order/ results',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'laboratory'
};

/*
     * ================  Pediatric Pharmacy Order config  =======================
     */

var pediatric = {
    Xtype: 'panel',
    id: 'pediatric-panel',
    title: 'Pediatric Pharmacy Order',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'pediatric'
};

/*
     *================================  ART Reports ============================
     */

/*
     * ================  No of Patients Enrolled in ART Report  =======================
     */
//var patients = {
//    Xtype: 'panel',
//    id: 'patients-panel',
//    title: 'Report: Patients Enrolled In ART',
//    //        layout: 'ux.center',
//    autoScroll: true,
//    autoWidth: true,
//    contentEl: 'patients'
//};

//var addlabCon = {
//    Xtype: 'panel',
//    id: 'addlabCon-panel',
//    title: 'Report: Additional',
//    autoScroll: true,
//    autoWidth: true,
//    contentEl: 'addlabCon'
//};
/*
     * ================  No of Patients in Care  =======================
     */
var pcare = {
    Xtype: 'panel',
    id: 'pcare-panel',
    title: 'Report: No of Patients in Care',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'pcare'
};

/*
     * ================  No of Patients Who Died, Transferred out, Missed Appointments, or Stopped ART  =======================
     */
var ptracking = {
    Xtype: 'panel',
    id: 'ptracking-panel',
    title: 'Report: No of Patients Who Died, Transferred, Missed Appointments Or Stopped ART ',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'ptracking'
};

/*
     * ================  No of Children in ART  =======================
     */
var children = {
    Xtype: 'panel',
    id: 'children-panel',
    title: 'Report: No of Children in ART ',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'children'
};

/*
     * ================  No of Children in ART  =======================
     */
var ccare = {
    Xtype: 'panel',
    id: 'ccare-panel',
    title: 'Report: No of Children in Care ',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'ccare'
};

/*
 * ================  No of Patients Enrolled Per Month Or a Predifined Period  =======================
 */
var monthly = {
    Xtype: 'panel',
    id: 'monthly-panel',
    title: 'Report: Patients Enrolled Monthly ',
    autoScroll: true,
    autoWidth: true,
    contentEl: 'monthly'
};

var fm = Ext.form;
var primaryKey='ID'; 

function refreshGrid() {
    labTestStore.reload();
}

var peddingLabTests = new Ext.grid.ColumnModel(
    [
    {
        id: 'ID',
        header: 'ID',
        dataIndex: 'ID',
        width: 100
    },{
        header: 'Patient',
        dataIndex: 'pname',
        width: 300

    },
    {
        header: 'Enrollment No',
        dataIndex: 'enrollNo',
        width: 200
    },
    {
        header: 'Test Date',
        dataIndex: 'testDate',
        width: 200
    },{
        header: 'Lab Test',
        dataIndex: 'labTest',
        width: 200
    },{
        header: 'Results',
        dataIndex: 'results',
        width: 400,
        editor: new fm.TextField({
            allowBlank: false
        })
    }
    ]);

peddingLabTests.defaultSortable = true;

var labTestReader= new Ext.data.JsonReader(
{
    root: 'labTest',
    id: 'ID'
},
[
{
    name: 'ID',
    type:'numeric'
},
{
    name: 'pname',
    type: 'string'
},
{
    name: 'enrollNo',
    type: 'string'
},
{
    name: 'testDate',
    type: 'string'
},
{
    name: 'labTest',
    type: 'string'
},
{
    name: 'Results',
    type: 'string'
}
]
)

var labTestStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'getLabTests.php', 
        method: 'POST'
    }),
    baseParams:{
        task: "readTests"
    },
    reader:labTestReader,
    sortInfo: {
        field:'ID',
        direction:'ASC'
    },
    groupField:'pname'
});

var editor = new Ext.ux.grid.RowEditor({
    saveText: 'Update'
});


var peddingTestsGrid = new Ext.grid.EditorGridPanel({
    store: labTestStore,
    cm: peddingLabTests,
    url:'getLabTests',
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    width: 700,
    height: 500,
    title: 'Pending Lab Tests',
    frame: true,
    clicksToEdit: 2,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:true
    }),
    stripeRows: true,
    tbar: [{
        text: 'Add Tests',
        iconCls:'add',
        handler : function(){

        }

    },
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid,
        iconCls:'refresh'
    }],
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                var gridrecord = peddingTestsGrid.getSelectionModel().getSelected();
                if (gridrecord == undefined){
                    alert('no good');
                }
                else{
                    document.lab_order_form.enroll_no.value=gridrecord.get('enrollNo')

                }
            }
        }
    })
});
labTestStore.load();

peddingTestsGrid.on('rowdblclick', function() {
    var gridrecord = peddingTestsGrid.getSelectionModel().getSelected();
    if (gridrecord == undefined){
        alert('no good');
    }
    else{
        document.lab_order_form.enroll_no.value=gridrecord.get('enrollNo');
        document.lab_order_form.queryType.value="update";
        getLabtests(gridrecord.get('enrollNo'));
        getLabResults(gridrecord.get('enrollNo'));

        Ext.getCmp('content-panel').layout.setActiveItem('laboratory-panel');
    }
});

var openTest = peddingTestsGrid.addButton({
    text: 'Create New Test',
    disabled:false,
    handler: function(){
        document.lab_order_form.reset();
        document.lab_order_form.queryType.value="insert";
        Ext.getCmp('content-panel').layout.setActiveItem('laboratory-panel');

    }
});

winLab = new Ext.Panel({
    title:'Laboratory Order/ Results',
    width: 700,
    autoHeight: true,
    id: 'peddingTestForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    items: [peddingTestsGrid]

});



var http_request = false;
function makePOSTRequest(url, parameters) {
    http_request = false;
    if (window.XMLHttpRequest) {
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/html');
        }
    } else if (window.ActiveXObject) {
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }
    if (!http_request) {
        alert('Cannot create XMLHTTP instance');
        return false;
    }
    http_request.onreadystatechange = alertContents;
    http_request.open('POST', url + parameters, true);
    http_request.send(null);
}

function alertContents() {
    if (http_request.readyState == 4) {
        //            if (http_request.status == 200) {
        //alert(http_request.responseText);
        result = http_request.responseText;
        document.getElementById('mySpan').innerHTML = result;
    //            } else {
    //                alert('There was a problem with the request.');
    //            }
    }
}

//    function get(obj) {
//        var getstr = "?";
//        for (i=0; i<obj.childNodes.length; i++) {
//            if (obj.childNodes[i].tagName == "input") {
//                if (obj.childNodes[i].type == "text") {
//                    getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
//                }
//                if (obj.childNodes[i].type == "checkbox") {
//                    if (obj.childNodes[i].checked) {
//                        getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
//                    } else {
//                        getstr += obj.childNodes[i].name + "=&";
//                    }
//                }
//                if (obj.childNodes[i].type == "radio") {
//                    if (obj.childNodes[i].checked) {
//                        getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
//                    }
//                }
//            }
//            if (obj.childNodes[i].tagName == "SELECT") {
//                var sel = obj.childNodes[i];
//                getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
//            }
//
//        }
//        makeRequest('initial_evaluation.php', getstr);
//    }
function get(obj) {
    var getstr = "?";
    for (i=0; i<obj.getElementsByTagName("input").length; i++) {
        if (obj.getElementsByTagName("input")[i].type == "text") {
            getstr += obj.getElementsByTagName("input")[i].name + "=" +
            obj.getElementsByTagName("input")[i].value + "&";
        }
        if (obj.getElementsByTagName("input")[i].type == "checkbox") {
            if (obj.getElementsByTagName("input")[i].checked) {
                getstr += obj.getElementsByTagName("input")[i].name + "=" +
                obj.getElementsByTagName("input")[i].value + "&";
            } else {
                getstr += obj.getElementsByTagName("input")[i].name + "=&";
            }
        }
        if (obj.getElementsByTagName("input")[i].type == "radio") {
            if (obj.getElementsByTagName("input")[i].checked) {
                getstr += obj.getElementsByTagName("input")[i].name + "=" +
                obj.getElementsByTagName("input")[i].value + "&";
            }
        }
        if (obj.getElementsByTagName("input")[i].tagName == "SELECT") {
            var sel = obj.getElementsByTagName("input")[i];
            getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
        }

    }
    makePOSTRequest('initial_evaluation.php', getstr);
}
