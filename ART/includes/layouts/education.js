/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
// some data used in the examples
Ext.namespace('Ext.exampledata');

Ext.exampledata.education = [
    ['None'],
    ['Primary'],
    ['Secondary'],
    ['Post-Secondary']
    ];

//var combos = {
//    init: function(){
        //    }};

//        var store = new Ext.data.SimpleStore({
//            fields: ['level'],
//            data : Ext.exampledata.education
//        });
//
//        var cmbeducation = new Ext.form.ComboBox({
//            store: store,
//            displayField:'level',
//            typeAhead: true,
//            mode: 'local',
//            forceSelection: true,
//            triggerAction: 'all',
//            emptyText:'Select a Level...'
//        //        selectOnFocus:true
//        //        applyTo: 'level'
//        });
//
//        cmbeducation.applyTo('level');

//    }
//};
//Ext.onReady(combos.init, combos);
