
// vim: sw=4:ts=4:nu:nospell:fdc=4
/*global Ext, Example */
/**
* Displaying 1:n Data Example
*
* @author Ing. Jozef Sakáloš
* @copyright (c) 2008, by Ing. Jozef Sakáloš
* @date 11. May 2008
* @version $Id: one2many.js 137 2009-03-13 23:00:01Z jozo $
*
* @license one2many.js is licensed under the terms of the Open Source
* LGPL 3.0 license. Commercial use is permitted to the extent that the
* code/component(s) do NOT become part of another Open Source or Commercially
* licensed development library or toolkit without explicit permission.
*
* License details: http://www.gnu.org/licenses/lgpl.html
*/

Ext.ns('Example');

Ext.BLANK_IMAGE_URL = 'ext/resources/images/default/s.gif';

  /**
 * @class Example.Grid
  * @extends Ext.grid.GridPanel
 */
 Example.Grid = Ext.extend(Ext.grid.GridPanel, {

  // configurables
  border:false

  // {{{
   ,initComponent:function() {

  // hard coded - cannot be changed from outside
  var config = {
  // store
 store:new Ext.data.JsonStore({
  id:'persID'
   ,root:'rows'
  ,totalProperty:'totalCount'
   ,url:'process-request.php'
  ,baseParams:{cmd:'getData', objName:'person'}
   ,fields:[
  {name:'persID', type:'int'}
  ,{name:'persFirstName', type:'string'}
   ,{name:'persMidName', type:'string'}
   ,{name:'persLastName', type:'string'}
   ,{name:'persNote', type:'string'}
  ,{name:'phones', type:'string'}
   ]
  })

   // column model
  ,columns:[{
   dataIndex:'persFirstName'
   ,header:'First'
   ,width:50
   },{
   dataIndex:'persMidName'
  ,header:'Middle'
   ,width:40
   },{
  dataIndex:'persLastName'
   ,header:'Last'
   ,width:80
   ,renderer:this.renderLastName.createDelegate(this)
   },{
   dataIndex:'persNote'
   ,header:'Note'
   ,width:200
   }]

   // force fit
   ,viewConfig:{forceFit:true, scrollOffset:0}

  // tooltip template
   ,qtipTpl:new Ext.XTemplate(
   '<h3>Phones:</h3>'
   ,'<tpl for=".">'
   ,'<div><i>{phoneType}:</i> {phoneNumber}</div>'
   ,'</tpl>'
   )
   }; // eo config object

   // apply config
   Ext.apply(this, Ext.apply(this.initialConfig, config));

   // call parent
  Example.Grid.superclass.initComponent.apply(this, arguments);

  } // eo function initComponent
   // }}}
   // {{{
   ,onRender:function() {

   // call parent
   Example.Grid.superclass.onRender.apply(this, arguments);

  // load store
  this.store.load();

 } // eo function onRender
  // }}}
  // {{{
  /**
  * Last Name rederer including tooltip with phones
  * @param {Mixed} val Value to render
 * @param {Object} cell
  * @param {Ext.data.Record} record
 */
  ,renderLastName:function(val, cell, record) {

  // get data
 var data = record.data;

 // convert phones to array (only once)
  data.phones = Ext.isArray(data.phones) ? data.phones : this.getPhones(data.phones);

 // create tooltip
  var qtip = this.qtipTpl.apply(data.phones);

  // return markup
 return '<div qtip="' + qtip +'">' + val + '</div>';
 } // eo function renderLastName
 // }}}
  // {{{
  /**
 * Converts string phones to array of objects
  * @param {String} phones
 * @return {Array} Array of phone objects
  */
 ,getPhones:function(phones) {

  // empty array if nothing to do
  if(!phones) {
  return [];
  }

  // init return value
  var retval = [];

  // split string to phones
  var aps = phones.split('|');

  // iterate through phones to extract phoneType and phoneNumber
  Ext.each(aps, function(phone) {
  var a = phone.split('~');
  retval.push({phoneType:a[0], phoneNumber:a[1]});
  });

  return retval;
 } // eo function getPhones
  // }}}

  }); // eo extend

  // register xtype
  Ext.reg('examplegrid', Example.Grid);


  // application main entry point
  Ext.onReady(function() {

 // initialize
 Ext.QuickTips.init();

  // create and show window
  var win = new Ext.Window({
  width:500
  ,height:300
  ,id:'one2many-win'
 ,layout:'fit'
 ,autoScroll:true
  ,title:Ext.getDom('page-title').innerHTML
  ,items:[{xtype:'examplegrid', id:'one2many-grid'}]
  });
  win.show();

  }); // eo function onReady

  // eof

