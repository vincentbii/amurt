<?
// vim: ts=4:sw=4:fdc=4:nu:nospell
/**
 4. * Lightweight SQL class suitable for public use and sqlite access
 5. *
 6. * @author Ing. Jozef Sakáloš
 7. * @copyright (c) 2008, by Ing. Jozef Sakáloš
 8. * @date 31. March 2008
 9. * @version $Id: csql.php 820 2010-02-14 18:30:45Z jozo $
 10. */

// {{{
/**
 14. * regexp: callback for sqlite REGEXP operator
 15. *
 16. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
 17. * @date 01. April 2008
 18. * @return boolean true if value matches regular expression
 19. * @param string $search string to find
 20. * @param string $value string to search in
 21. */
function regexp($search, $value) {
    // case insensitive hard coded
    return @preg_match("/$search/i", $value);
}
// }}}

/**
 29. * concat_ws: sqlite custom function
 30. *
 31. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
 32. * @date 11. May 2008
 33. * @return string
 34. * @param mixed
 35. */
function concat_ws() {
    $args = func_get_args();
    $sep = array_shift($args);
    return implode($sep, $args);
} // eo function concat_ws
// {{{
/**
 43. * Quote array callback function
 44. *
 45. * This is walk array callback function that
 46. * surrounds array element with quotes.
 47. *
 48. * @date 08. September 2003
 49. * @access public
 50. * @return void
 51. * @param string &$val Array element to be quouted
 52. * @param mixed $key Dummy. Not used in the function.
 53. * @param string $quot Quoute to use. Double quote by default
 54. */
function quote_array(&$val, $key, $quot = '"') {
    $quot_right = array_key_exists(1, (array) $quot) ? $quot[1] : $quot[0];
    $val = is_null($val) ? "null" : $quot[0] . preg_replace("/'/", "''", $val) . $quot_right;
}
// }}}

/**
 62. * csql class
 63. *
 64. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
 65. * @copyright (c) 2008 by Ing. Jozef Sakáloš
 66. * @date 31. March 2008
 67. */
class csql {

    // protected functions
    // {{{
    /**
     73. * getOdb: Creates PDO object
     74. *
     75. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     76. * @date 31. March 2008
     77. * @access protected
     78. * @return PDO
     79. * @param string $engine
     80. * @param string $file
     81. */
    protected function getOdb($engine, $file) {
        switch($engine) {
            case "sqlite":
                if("/" !== $file[0]) {
                    $file = realpath(".") . "/$file";
                }
                $odb = new PDO("sqlite:$file");
                $odb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $odb->sqliteCreateFunction("regexp", "regexp", 2);
                $odb->sqliteCreateFunction("concat_ws", "concat_ws");
                break;
        }

        return $odb;
    } // eo function getOdb
    // }}}
    // {{{
    /**
     100. * getWhere: return where clause
     101. *
     102. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     103. * @date 01. April 2008
     104. * @access protected
     105. * @return string where clause including where keyword
     106. * @param array $params
     107. */
    protected function getWhere($params, $ignoreFilter = "") {
        extract($params);

        $where = isset($where) ? "where $where" : "";

        if($filters) {
            $a = array();
            foreach($filters as $f=>$value) {
                if($f === $ignoreFilter) {
                    continue;
                }
                if(is_array($value)) {
                    if(sizeof($value)) {
                        array_walk($value, "quote_array", "'");
                        $a[] = "$f in (" . implode(",", $value) . ")";
                    }
                }
                else {
                    $value = trim($value);
                    $ab = preg_split("/(>)|(<)|(=)|(!)/", $value, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                    if(preg_match("/\|/", $value)) {
                        $value = preg_replace("/\s*\|\s*/", "|", $value);
                        $aa = explode("|", $value);
                        for($i = 0; $i < sizeof($aa); $i++) {
                            $aa[$i] = "'" . $aa[$i] . "'";
                        }
                        $a[] = "$f in (" . implode(",", $aa) . ")";
                    }
                    else if(1 < sizeof($ab)) {
                        $value = array_pop($ab);
                        $operator = "";
                        for($i = 0; $i < sizeof($ab); $i++) {
                            // $operator .= ($ab[$i] === "!" ? "not " : $ab[$i]);
                            $operator .= $ab[$i];
                        }
                        $a[] = "$f $operator '$value'";
                    }
                    else {
                        $a[] = "$f regexp '$value'";
                    }
                }
            }
            if(sizeof($a)) {
                $where .= $where ? " and(" : "where (";
                $where .= implode(" and ", $a) . ")";
            }
        }

        if($query && is_array($search) && sizeof($search)) {
            $a = array();
            foreach($search as $f) {
                $a[] = "$f regexp '$query'";
            }
            $where .= $where ? " and(" : "where (";
            $where .= implode(" or ", $a) . ")";
        }

        error_log("Where = " . $where);
        return $where;

    } // eo function getWhere
    // }}}

    // public functions
    // {{{
    /**
     174. * __construct: Constructs the csql instance
     175. *
     176. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     177. * @date 31. March 2008
     178. * @access public
     179. * @return void
     180. * @param string $engine Engine to use
     181. */
    public function __construct($engine = "sqlite", $file = "db.sqlite") {
        $this->odb = $this->getOdb($engine, $file);
    } // eo constructor
    // }}}
    // {{{
    /**
     188. * getCount: Returns count of records in a table
     189. *
     190. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     191. * @date 31. March 2008
     192. * @access public
     193. * @return integer number of records
     194. * @param array $params
     195. */
    public function getCount($params) {
        $count = null;
        $countArg = $params["distinct"] ? "distinct " . $params["distinct"] : "*";
        $ostmt = $this->odb->prepare("select count($countArg) from {$params['table']} " . $this->getWhere($params));
        $ostmt->bindColumn(1, $count);
        $ostmt->execute();
        $ostmt->fetch();
        return (int) $count;
    } // eo function getCount
    // }}}
    // {{{
    /**
     208. * getData: Retrieves data and returns array of objects
     209. *
     210. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     211. * @date 31. March 2008
     212. * @access public
     213. * @return array
     214. * @param array $params Associative array with:
     215. * - string table (Mandatory)
     216. * - array field (Mandatory)
     217. * - integer start (Optional)
     218. * - integer limit (Optional)
     219. * - string sort (Optional)
     220. * - array search (Optional) fields to search in
     221. * - string where (Optional)
     222. */
    public function getData($params) {
        // params to variables
        extract($params);

        $sql = "select " . ($distinct ? "distinct " : "");
        $sql .= implode(",", $fields);
        $sql .= " from $table " . $this->getWhere($params);
        $sql .= isset($groupBy) && $groupBy ? " group by $groupBy" : "";

        if(!is_null($sort)) {
            $sql .= " order by $sort";
            $sql .= is_null($dir) ? "" : " $dir";
        }
        if(!is_null($start) && !is_null($limit)) {
            $sql .= " limit $start,$limit";
        }

        $ostmt = $this->odb->query($sql);
        return $ostmt->fetchAll(PDO::FETCH_OBJ);

    } // eo function getData
    // }}}

    /**
     247. * getFilterRows: short description of
     248. *
     249. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     250. * @date 05. February 2010
     251. * @access public
     252. * @return mixed Description
     253. * @param
     254. */
    public function getFilterRows($name, $params, $ignoreMyself = false) {
        extract($params);
        $ignoreFilter = $ignoreMyself ? $name : "";
        $sql = "select distinct $name from $table " . $this->getWhere($params, $ignoreFilter) . " order by $name";
        error_log($sql);
        $ostmt = $this->odb->query($sql);
        $rows = $ostmt->fetchAll(PDO::FETCH_NUM);
        $a = array();
        foreach($rows as $row) {
            $a[] = $row[0];
        }
        return $a;
        // return $rows;
    } // eo function getFilterRows

    // {{{
    /**
     272. * insertRecord: inserts record to table
     273. *
     274. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     275. * @date 01. April 2008
     276. * @access public
     277. * @return integer id of the inserted record
     278. * @param array $params
     279. */
    public function insertRecord($params) {
        // params to vars
        extract($params);

        $o = new stdClass();
        $o->success = false;

        $a = "object" === gettype($data) ? get_object_vars($data) : $data;
        unset($a["newRecord"]);
        if($idName) {
            unset($a[$idName]);
        }
        $fields = array_keys($a);
        $values = array_values($a);
        array_walk($values, "quote_array", "'");

        $sql = "insert into $table (" . implode(",", $fields) . ") values (" . implode(",", $values) . ")";

        try {
            $this->odb->exec($sql);
            $o->success = true;
            $o->insertId = $this->odb->lastInsertId();
        }
        catch(PDOException $e) {
            $o->error = "$e";
        }

        return $o;

    } // eo function insertRecord
    // }}}
    // {{{
    /**
     313. * saveData: saves data (updates or inserts)
     314. *
     315. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     316. * @date 01. April 2008
     317. * @access public
     318. * @return object either {success:true} or {success:false,error:message}
     319. * @param array $params
     320. */
    public function saveData($params) {
        // params to vars

// return object
        $o = new stdClass;
        $o->success = false;

        if(!$table || !is_array($data) || !$idName) {
            $o->error = "Table, data or idName is missing";
            return $o;
        }

        // record loop
        $p = array(
                "table"=>$table
                ,"idName"=>$idName
        );
        $this->odb->exec("begin transaction");

        foreach($data as $orec) {
            $p["data"] = $orec;

            // insert/update switch
            if(isset($orec->newRecord) && $orec->newRecord) {
                $result = $this->insertRecord($p);
            }
            else {
                $result = $this->updateRecord($p);
            }

            // handle error
            if(true !== $result->success) {
                $o->success = false;
                $o->error = $result->error;
                $this->odb->exec("rollback");
                return $o;
            }
            else {
                $o->success = true;
            }

            // handle insertId if any
            if(isset($result->insertId)) {
                $o->insertIds[] = $result->insertId;
            }
        } // eo record loop

        $this->odb->exec("commit");
        return $o;
    } // eo function saveData
    // }}}
    // {{{
    /**
     375. * updateRecord: updtates one record in table
     376. *
     377. * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     378. * @date 31. March 2008
     379. * @access public
     380. * @return object either {success:true} or {success:false,error:message}
     381. * @param array $params with:
     382. * - string table (Mandatory)
     383. * - string idName (Mandatory) name of id field
     384. * - object data (Mandatory) Name/value pairs object or associative array
     385. */
    public function updateRecord($params) {
        // array to vars
        extract($params);

        $o = new stdClass();
        $o->success = false;

        if(!isset($table) || !isset($idName) || !isset($data)) {
            $o->error = "Table, idName or data not set.";
            return $o;
        }
        $asets = array();
        $where = "";

        foreach($data as $field => $value) {
            if($idName === $field) {
                $where = " where $field='$value'";
                continue;
            }
            array_push($asets, "$field=" . (is_null($value) ? "null" : "'$value'"));
        }
        if(!$where) {
            $o->error = "idName not found in data";
            return $o;
        }

        $sql = "update $table set " . implode(",", $asets) . $where;

        try {
            $this->odb->exec($sql);
            $o->success = true;
        }
        catch(PDOException $e) {
            $o->error = "$e";
        }

        return $o;

    } // eo function updateRecord
    // }}}
    // {{{
    /**
     * output: Encodes json object and sends it to client
     *
     * @author Ing. Jozef Sakáloš <jsakalos@aariadne.com>
     * @date 31. March 2008
     * @access protected
     * @return void
     * @param object/array $o
     * @param string $contentType
     */
    public function output($o = null, $contentType = "application/json; charset=utf-8") {
        $o = $o ? $o : $this->o;
        $buff = json_encode($o);
        header("Content-Type: {$contentType}");
        header("Content-Size: " . strlen($buff));
        echo $buff;
    } // eo function output
    // }}}

} // eo class csql

// eof
