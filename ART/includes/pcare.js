var pcareStore = new Ext.data.JsonStore({
    url: 'pcare.php',
    root: 'care_list',
    //        totalProperty: 'total',
    fields: ['enrollment_no', 'patient_name', 'weight', 'age', 'arv_type', 'arv_provider']
});


var filters = new Ext.ux.grid.GridFilters({
    autoReload: false, //don&#39;t reload automatically
    local: true, //only filter locally
    filters: [{
        type: 'numeric',
        width: 100,
        dataIndex: 'enrollment_no'
    }, {
        type: 'string',
        width: 200,
        dataIndex: 'patient_name'
    //disabled: true
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'weight'
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'age'
    }, {
        type: 'string',
        width: 100,
        dataIndex: 'arv_type'
    }, {
        type: 'string',
        width: 100,
        dataIndex: 'arv_provider'
    }]
});


var createColModel = function (finish, start) {

    var columns = [{
        dataIndex: 'enrollment_no',
        width: 100,
        header: 'Patient Number',
        // instead of specifying filter config just specify filterable=true
        // to use store's field's type property (if type property not
        // explicitly specified in store config it will be 'auto' which
        // GridFilters will assume to be 'StringFilter'
        filterable: true,
        filter: {
            type: 'numeric'
        }
    }, {
        dataIndex: 'patient_name',
        width: 200,
        header: 'Patient Name',
        //id: 'company',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'weight',
        width: 80,
        header: 'Weight',
        filter: {
            type: 'numeric'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'age',
        width: 80,
        header: 'Age',
        filter: {
            type: 'numeric'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'arv_type',
        width: 100,
        header: 'ART Type',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }, {
        dataIndex: 'arv_provider',
        width: 100,
        header: 'ART Provider',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }];

    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
};

var pcareGrids = new Ext.grid.GridPanel({
    //border: false,
    title: 'No of Adult Patients in Care',
    //        region: 'center',
    store: pcareStore,
    colModel: createColModel(6),
    loadMask: true,
    autoHeight: true,
    width: 670,
    plugins: [filters],

    listeners: {
        render: {
            fn: function(){
                pcareStore.load({
                    params: {
                        start: 0,
                        limit: 20
                    }
                });
            //g.getsSelectionModel().selectRow(0)
            //delay: 10
            }
        }
    },

    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("pcare_info").getForm().loadRecord(rec);
            }
        }
    }),

     tbar: [{
        text: 'Export To Excel',
        tooltip: 'Click to Export as Excel Format',
        handler: function () {
                    document.location.href='reports/exportAdultPatients.php';
                },
        iconCls:'refresh'
    }],

    bbar: new Ext.PagingToolbar({
        store: pcareStore,
        pageSize: 20,
        plugins: [filters]
    })
});

var pcareGrid = {
    Xtype: 'panel',
    id: 'pcare-panel',
    //    title: 'Report: Patients Enrolled In ART',
    items:[pcareGrids]
};