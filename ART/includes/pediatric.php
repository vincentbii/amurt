<script type="text/javascript" src="pediatric_composite.js"></script>

<div id="pediatric" class="main_container">
    <div align="center">
        <div class="profile_main_container_form">
            <form class="" method="post" action="pediatric_pharmacy.php"  name="pediatric_pharmacy">
                <div class="section pediatric_section_1">
                    <div class="pediatric_section_1_left">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Patient Name: </label>
                            <label style="padding-right: 5px;"></label>
                            <input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" readonly="true" />
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> ID: </label>
                            <label style="padding-right: 22px;"></label>
                            <input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" readonly="true" />
                            <label style="padding-right: 4px;"></label>
                            &#8211;
                            <label style="padding-right: 4px;"></label>
                            <input name="patient_enrollment_no" id="patient_enrollment_no" type="text" size="7" maxlength="7" readonly="true" />
                            <label style="padding-right: 22px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label style="padding-right: 28px;"></label>
                            <label class=" font_bold"> Satellite#</label>
                            <label style="padding-right: 7px;"></label>
                            <label class="font_bold">Patient Enrollment#</label>
                        </div>
                    </div>
                    <div class="pediatric_section_1_right">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Existing Clinic/ Hosp#: </label>
                            <label style="padding-right: 12px;"></label>
                            <input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" readonly="true" />
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Weight: </label>
                            <label style="padding-right: 10px;"></label>
                            <input name="weight" id="weight" type="text" size="3" maxlength="3" />
                            <label class="font_small"> Kg </label>
                            <label style="padding-right: 22px;"></label>
                            <label class="font_bold"> Height: </label>
                            <label style="padding-right: 10px;"></label>
                            <input name="height" id="height" type="text" size="3" maxlength="3" />
                            <label class="font_small"> cm </label>
                            <label style="padding-right: 15px;"></label>
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Date of Birth:  </label>
                            <label style="padding-right: 2px;"></label>
                            <input name="dob" id="dob" type="text" size="9" maxlength="9" readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section pediatric_section_2">
                    <div class="pediatric_section_2_left">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Specify Treatment Type: </label>
                            <label style="padding-right: 22px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="art" /> ART
                            <label style="padding-right: 46px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="pmtct" /> PMTCT
                            <label style="padding-right: 22px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="pep" /> PEP
                            <label style="padding-right: 22px;"></label>
                            <input name="arv_type" id="arv_type" type="radio" value="non_art" /> Non-ART
                        </div>
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <label class="font_bold"> Specify ARV Provider: </label>
                            <label style="padding-right: 40px;"></label>
                            <input name="arv_provider" id="arv_provider" type="radio" value="governement" /> Government
                            <label style="padding-right: 10px;"></label>
                            <input name="arv_provider" id="arv_provider" type="radio" value="usg" /> USG
                            <label style="padding-right: 34px;"></label>
                            <input name="arv_provider" id="arv_provider" type="radio" value="other" /> Other
                        </div>
                    </div>
                    <div class="pediatric_section_2_right">

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section pediatric_section_3">
                    <div class="pediatric_section_3_left">
                        <div class="spacer"></div>
                        <div class=" section_row">
                            <table width="98%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2"><label class="font_bold">1. ARV Medication </label></td>
                                    <td width="11%"></td>
                                    <td width="9%"></td>
                                    <td width="8%"></td>
                                    <td width="6%"></td>
                                    <td width="7%"></td>
                                    <td width="9%"></td>
                                    <td width="4%"></td>
                                </tr>
                                <tr>
                                    <td colspan="5"><label class="font_small">(NB: Liquid formulations for children &lt; 15kg or &lt; 3 years and pills for other children are suggested.) </label></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="24%"></td>
                                    <td width="22%"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" rowspan="2" align="center"><label class="font_bold">
                                            Quantity </label>
                                        <label class="font_bold">(mL (bottle) or tab/cap) </label> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td height="70"><label class="font_bold"></label></td>
                                    <td></td>
                                    <td></td>
                                    <td><label class="font_bold"></label><br/>
                                        <label class="font_bold"></label></td>
                                    <td rowspan="2"><label class="font_bold">Total Daily</label><br/>
                                        <label class="font_bold">Dose</label></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">NRTIs</label></td>
                                    <td><label class="font_bold">Formulation </label></td>
                                    <td><label class="font_bold"> Dose </label></td>
                                    <td><label class="font_bold">Frequency</label></td>
                                    <td align="center"><label class="font_bold">
                                            Duration
                                        </label></td>
                                    <td align="center"><label class="font_bold">
                                            Prescribed
                                        </label></td>
                                    <td align="center"><label class="font_bold">
                                            Dispensed
                                        </label></td>
                                    <td align="center"><label class="font_bold">Price 
                                        </label></td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1274_meds" type="radio" value="ART1274"/>Lamivudine (3TC,Epivir)</td>
                                    <td><input name="ART1274_formulation" id="ART1274_formulation" type="radio" value="10mg/ml_syrup"/>10 mg/ml syrup</td>
                                    <td>AM <input name="ART1274_sdose" id="ART1274_sdose" type="text" size="4" maxlength="4"/></td>
                                    <td><input name="ART1274_frequency" id="ART1274_frequency" type="radio" value="BD"/>BD</td>
                                    <td><input name="ART1274_dose" id="ART1274_dose"  type="text" size="4" maxlength="4"/></td>
                                    <td><input name="ART1274_duration" id="ART1274_duration" type="text" size="4" maxlength="4"/></td>
                                    <td><input name="ART1274_prescribed" id="ART1274_prescribed"  type="text" size="4" maxlength="4"/></td>
                                    <td><input name="ART1274_dispensed" id="ART1274_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1274_price" id="ART1274_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>(adjust for CRCL)</td>
                                    <td><input name="ART1274_formulation" id="ART1274_formulation" type="radio" value="150 mg tab"/>
                                        150 mg tab</td>
                                    <td>PM <input name="ART1274_sdose" id="ART1274_sdose" type="text" size="4" maxlength="4"/></td>
                                    <td></td>
                                    <td colspan="2">(Bottle = 240 ml)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center">Max - 150 mg bd</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1407_meds" type="radio" value="ART1407" /> Zidovudine (AZT,Retrovir)</td>
                                    <td><input name="ART1407_formulation" id="ART1407_formulation" type="radio" value="10mg/ml" />
                                        10 mg/ml solution</td>
                                    <td>AM <input name="ART1407_sdose" id="ART1407_sdose" type="text" size="4" maxlength="4"/></td>
                                    <td><input name="ART1407_frequency" id="ART1407_frequency" type="radio" value="BD" /> BD</td>
                                    <td><input name="ART1407_dose" id="ART1407_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1407_duration" id="ART1407_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1407_prescribed" id="ART1407_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1407_dispensed" id="ART1407_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1407_price" id="ART1407_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>(adjust for CRCL)</td>
                                    <td><input name="ART1407_formulation" id="ART1407_formulation" type="radio" value="100 mg Capsule" />
                                        100 mg Capsule</td>
                                    <td> PM <input name="ART1407_sdose" id="ART1407_sdose" type="text" size="4" maxlength="4" /></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" align="center">(Bottle = 200 ml)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center">Max - 150 mg BD</td>
                                    <td><input name="ART1407_formulation" id="ART1407_formulation" type="radio" value="300 mg tab" />
                                        300 mg tab</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1374_meds" type="radio" value="ART1374" />
                                        Stavudine (AZT,Retrovir)</td>
                                    <td><input name="ART1374_formulation" id="ART1374_formulation" type="radio" value="15mg/15 mg/ml Capsule" />
                                        15 mg/ml Capsule</td>
                                    <td align="center"><input name="ART1374_sdose" id="ART1374_sdose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1374_frequency" id="ART1374_frequency" type="radio" value="BD" /> BD</td>
                                    <td><input name="ART1374_dose" id="ART1374_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1374_duration" id="ART1374_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1374_prescribed" id="ART1374_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1374_dispensed" id="ART1374_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1374_price" id="ART1374_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>(adjust for CRCL)</td>
                                    <td><input name="ART1374_formulation" id="ART1374_formulation" type="radio" value="20 mg Capsule" />
                                        20 mg Capsule</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center">Max - 30 mg BD</td>
                                    <td><input name="ART1374_formulation" id="ART1374_formulation" type="radio" value="30 mg Capsule" />
                                        30 mg Capsule</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART01_meds" id="ART1381_meds" type="radio" value="ART1381" />
                                        Abacavir (ABC,Ziagen)</td>
                                    <td><input name="ART1381_formulation" id="ART1381_formulation" type="radio" value="20mg/20 mg/ml Suspension" />
                                        20 mg/ml Suspension</td>
                                    <td align="center"><input name="ART1381_sdose" id="ART1381_sdose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1381_frequency" id="ART1381_frequency" type="radio" value="BD" />BD</td>
                                    <td><input name="ART1381_dose" id="ART1381_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1381_duration" id="ART1381_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1381_prescribed" id="ART1381_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1381_dispensed" id="ART1381_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1381_price" id="ART1381_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input name="ART1381_formulation" id="ART1381_formulation" type="radio" value="300 mg Tab" />
                                        300 mg Tab</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" align="center">(Bottle = 240 ml)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Age 3 months - 16 years 8 mg/Kg BID; Maximum 300 mg BID</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">Pls</label></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART04_meds" id="ART1281_meds" type="radio" value="ART1281" />
                                        Lopinavir/r</td>
                                    <td><input name="ART1281_formulation" id="ART1281_formulation" type="radio" value="80 mg/ml LPV/20 mg/ml RTV" />
                                        80 mg/ml LPV/20 mg/ml RTV</td>
                                    <td align="center"><input name="ART1281_sdose" id="ART1281_sdose" type="text" size="4" maxlength="4" />                                      </td>
                                    <td><input name="ART1281_frequency" id="ART1281_frequency" type="radio" value="BD" />BD</td>
                                    <td><input name="ART1281_dose" id="ART1281_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1281_duration" id="ART1281_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1281_prescribed" id="ART1281_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1281_dispensed" id="ART1281_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1281_price" id="ART1281_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>Max - 400mg/ 100mg BD</td>
                                    <td><input name="ART1281_formulation" id="ART1281_formulation" type="radio" value="100 mg LPV/25 mg RTV tab" />100 mg LPV/25 mg RTV tab</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" align="center">(Bottle = 60 ml) </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input name="ART1281_formulation" id="ART1281_formulation" type="radio" value="200 mg LPV/50 mg RTV tab" />
                                        200 mg LPV/50 mg RTV tab</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">NNRTIs</label></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART03_meds" id="ART1310_meds" type="radio" value="ART1310" />
                                        Nevirapine (Viramune)</td>
                                    <td><input name="ART1310_formulation" id="ART1310_formulation" type="radio" value="10 mg/ml suspension" />
                                        10 mg/ml suspension</td>
                                    <td><label class="font_bold">AM </label>
                                        <input name="ART1310_sdose" id="ART1310_sdose" type="text" size="4" maxlength="4" />
                                        <label style="padding-right: 4px;"></label></td>
                                    <td><input name="ART1310_frequency" id="ART1310_frequency" type="radio" value="OD" />
                                        OD </td>
                                    <td><input name="ART1310_dose" id="ART1310_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1310_duration" id="ART1310_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1310_prescribed" id="ART1310_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1310_dispensed" id="ART1310_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1310_price" id="ART1310_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>Max - 200mg BD</td>
                                    <td><input name="ART1310_formulation" id="ART1310_formulation" type="radio" value="200 mg scored tab" />
                                        200 mg scored tabs</td>
                                    <td><label class="font_bold">PM </label>
                                        <input name="ART1310_sdose" id="ART1310_sdose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1310_frequency" id="ART1310_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2" align="center">(Bottle = 240 ml)</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART03_meds" id="ART1203_meds" type="radio" value="ART1203" />
                                        Efavirenz (Stocrin)</td>
                                    <td><input name="ART1203_formulation" id="ART1203_formulation" type="radio" value="50mg_capsules" />
                                        50 mg Capsule</td>
                                    <td align="center"><input name="ART1203_sdose" id="ART1203_sdose" type="text" size="4" maxlength="4" />                                    </td>
                                    <td><input name="ART1203_frequency" id="ART1203_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1203_dose" id="ART1203_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1203_duration" id="ART1203_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1203_prescribed" id="ART1203_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1203_dispensed" id="ART1203_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1203_price" id="ART1203_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>Not recommended in</td>
                                    <td><input name="ART1203_formulation" id="ART1203_formulation" type="radio" value="200 mg capsule" />
                                        200 mg capsule</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Children < 3 years</td>
                                    <td><input name="ART1203_formulation" id="ART1203_formulation" type="radio" value=" 600mg tabs" />
                                        600 mg tabs</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center">Max - 600mg OD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label class="font_bold">PEDIATRIC FIXED DOSE COMBINATION </label> </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="fixed_meds" type="radio" value="ART1419" />
                                        D4T/3TC/NVP</td>
                                    <td><input name="ART1419_formulation" id="ART1419_formulation" type="text" size="15" maxlength="15" /></td>
                                    <td align="center"><input name="ART1419_sdose" id="ART1419_sdose" type="text" size="4" maxlength="4" />                                  </td>
                                    <td><input name="ART1419_frequency" id="ART1419_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1419_dose" id="ART1419_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1419_duration" id="ART1419_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1419_prescribed" id="ART1419_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1419_dispensed" id="ART1419_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1419_price" id="ART1419_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><input name="ART1419_frequency" id="ART1419_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART1420_meds" type="radio" value="ART1420" />
                                        D4T/3TC</td>
                                    <td><input name="ART1420_formulation" id="ART1420_formulation" type="text" size="15" maxlength="15" /></td>
                                    <td align="center"><input name="ART1420_sdose" id="ART1420_sdose" type="text" size="4" maxlength="4" />                                 </td>
                                    <td><input name="ART1420_frequency" id="ART1420_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1420_dose" id="ART1420_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1420_duration" id="ART1420_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1420_prescribed" id="ART1420_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1420_dispensed" id="ART1420_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1420_price" id="ART1420_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><input name="ART1420_frequency" id="ART1420_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label class="font_bold">OTHER ARVs (Specify Name & Formulation)</label> </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART0_meds" type="checkbox" value="ART0" />
                                        <label style="padding-right: 10px;"></label>
                                        <input name="ART0_formulation" id="ART0_formulation" type="text" size="20" maxlength="20" /></td>
                                    <td></td>
                                    <td align="center"><input name="ART0_sdose" id="ART0_sdose" type="text" size="4" maxlength="4" />                                 </td>
                                    <td><input name="ART0_frequenc" id="ART0_frequenc" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_dose" id="ART0_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_duration" id="ART0_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_prescribed" id="ART0_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_dispensed" id="ART0_dispensed" type="text" size="4" maxlength="4"readonly="true" /></td>
                                    <td><input name="ART0_price" id="ART0_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART0_meds" type="checkbox" value="ART0" />
                                        <label style="padding-right: 10px;"></label>
                                        <input name="ART0_formulation" id="ART0_formulation" type="text" size="20" maxlength="20" /></td>
                                    <td></td>
                                    <td align="center"><input name="ART0_sdose" id="ART0_sdose" type="text" size="4" maxlength="4" />                                  </td>
                                    <td><input name="ART0_frequency" id="ART0_frequency" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_dose" id="ART0_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_duration" id="ART0_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_prescribed" id="ART0_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_dispensed" id="ART0_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART0_price" id="ART0_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART02_meds" id="ART0_meds" type="checkbox" value="ART0" />
                                        <label style="padding-right: 10px;"></label>
                                        <input name="ART0_formulation" id="ART0_formulation" type="text" size="20" maxlength="20" /></td>
                                    <td></td>
                                    <td align="center"> <input name="ART0_sdose" id="ART0_sdose" type="text" size="4" maxlength="4" />                                 </td>
                                    <td><input name="ART0_frequency" id="ART0_frequency" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_dose" id="ART0_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_duration" id="ART0_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_prescribed" id="ART0_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART0_dispensed" id="ART0_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART0_price" id="ART0_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">2. Ol Prophylaxis </label> </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><input name="ART05_meds" id="ART1416_meds" type="radio" value="ART1416" />
                                        TMP/SMX (Septrin)</td>
                                    <td><input name="ART1416_formulation" id="ART1416_formulation" type="radio" value="40mg TMP/ 200mg SMX/ 5ml" />
                                        40mg TMP/ 200mg SMX/ 5ml</td>
                                    <td align="center"><input name="ART1416_sdose" id="ART1416_sdose" type="text" size="4" maxlength="4" />
                                    </td>
                                    <td><input name="ART1416_frequency" id="ART1416_frequency" type="radio" value="OD" />
                                        OD</td>
                                    <td><input name="ART1416_dose" id="ART1416_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1416_duration" id="ART1416_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1416_prescribed" id="ART1416_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1416_dispensed" id="ART1416_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1416_price" id="ART1416_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input name="ART1416_formulation" id="ART1416_formulation" type="radio" value="80 mg TMP/400 mg SMX(SS)" />
                                        80 mg TMP/400 mg SMX(SS)</td>
                                    <td></td>
                                    <td><input name="ART1416_frequency" id="ART1416_frequency" type="radio" value="BD" />
                                        BD</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input name="ART1416_formulation" id="ART1416_formulation" type="radio" value="160 mg TMP/800 mg SMX(DS)" />
                                        160 mg TMP/800 mg SMX(DS)</td>
                                    <td></td>
                                    <td><input name="ART1416_frequency" id="ART1416_frequency" type="radio" value="TDS" />
                                        TDS</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td align="center">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="ART06_meds" id="ART1301_meds" type="radio" value="ART1301" />Multivitamin Syr/Tabs</td>
                                    <td></td>
                                    <td align="center"><input name="ART1301_sdose" id="ART1301_sdose" type="text" size="4" maxlength="4" />                                  </td>
                                    <td><input name="ART1301_frequency" id="ART1301_frequency" type="radio" value="OD" /> OD </td>
                                    <td><input name="ART1301_dose" id="ART1301_dose" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1301_duration" id="ART1301_duration" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1301_prescribed" id="ART1301_prescribed" type="text" size="4" maxlength="4" /></td>
                                    <td><input name="ART1301_dispensed" id="ART1301_dispensed" type="text" size="4" maxlength="4" readonly="true"/></td>
                                    <td><input name="ART1301_price" id="ART1301_price" type="text" size="6" maxlength="6" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>

                    </div><!-- End of Section Three Left -->
                    <div class="pediatric_section_3_right">

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section pediatric_section_4"><!-- Begining of Section Four -->
                    <div class="pediatric_section_4_left" id="pedsbody ">

                    </div><!-- End of Section Four Left -->
                    <div class="pediatric_section_4_right"><!-- Begining of Section Four Right -->

                    </div><!-- End of Section Four Right -->
                </div><!-- End of Section Four -->
                <div class="spacer"></div>
                <div class="section pediatric_section_5"><!-- Begining of Section Five -->
                    <div class="spacer"></div>
                    <div class="pediatric_section_5_left"><!-- Begining of Section Five Left -->
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />
                            <label style="padding-right: 22px;"></label>
                            <input id="Reset" type="reset" value="Reset" />
                        </div>
                    </div><!-- End of SSetion Five Left -->
                    <div class="pediatric_section_5_right"><!-- Begining of Section Five Right -->

                    </div><!-- End of Section Five Right -->
                </div><!-- End of Section Five -->
            </form>
        </div><!-- End of Main Container Form -->
    </div>
</div>
