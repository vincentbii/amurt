var fm = Ext.form;

var primaryKey='pharmauto';

var selectedKeys;

var fieldValue;

var filters = new Ext.ux.grid.GridFilters({
    autoReload: false, 
    local: true, 
    filters: [{
        type: 'numeric',
        width: 80,
        dataIndex: 'enrollment_no'
    }, {
        type: 'string',
        width: 150,
        dataIndex: 'drug'
    },
//    {
//        type: 'string',
//        width: 80,
//        dataIndex: 'drug_dose'
//    },
    {
        type: 'string',
        width: 80,
        dataIndex: 'qty_prescribed'
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'qty_dispensed'
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'unit_price'
    }, {
        type: 'numeric',
        width: 80,
        dataIndex: 'total_price'
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'pharmacy'
    }]
});


var createColModel = function (finish, start) {

    var columns = [{
        dataIndex: 'enrollment_no',
        header: 'Patient Number',
        width: 15,
        filter: {
            type: 'numeric'
        }
    }, {
        dataIndex: 'drug',
        header: 'Drug Name',
        width: 80
    }, {
        dataIndex: 'drug_formulation',
        header: 'Formulation',
        width: 80
    }, {
        dataIndex: 'drug_sdose',
        header: 'Single Dose',
        width:20
    }, {
        dataIndex: 'drug_frequency',
        header: 'Frequency',
        width: 20
    }, {
        dataIndex: 'drug_dose',
        header: 'Dose',
        width: 20
    }, {
        dataIndex: 'drug_duration',
        header: 'Duration',
        width: 20
    }, {
        dataIndex: 'qty_prescribed',
        header: 'Amount Prescribed',
        width: 20
    }, {
        dataIndex: 'qty_dispensed',
        header: 'Amount Dispensed',
        width: 20,
        editor: new fm.TextField({
            allowBlank: false
        })
    }, {
        dataIndex: 'unit_price',
        header: 'Cost per Unit',
        width: 40
    }, {
        dataIndex: 'total_price',
        header: 'Total Charges',
        width: 45,
        format: 'Ksh.0,0.00',
        renderer: function(v, params, record){
            return Ext.util.Format.usMoney(record.data.qty_dispensed * record.data.unit_price);
        },
        filter: {
            type: 'numeric'
        }
    } , {
        dataIndex: 'pharmacy',
        header: 'Pharmacy',
        hidden: true
    }];

    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
};


var pharmReader = new Ext.data.JsonReader(
{
root: 'pharm_list',
    id: 'pharmauto'
},
[{
    name: 'pharmauto',
    type:'numeric'
},{
    name: 'enrollment_no',
    type:'numeric',
    width: 15
},
{
    name: 'drug',
    type: 'string',
    width: 80
},
{
    name: 'drug_formulation',
    type: 'string',
    autoWidth: 80
},
{
    name: 'drug_sdose',
    type: 'string',
    autoWidth: 20
},
{
    name: 'drug_frequency',
    type: 'string',
    autoWidth: 20
},
{
    name: 'drug_dose',
    type: 'string',
    autoWidth: 20
},
{
    name: 'drug_frequency',
    type: 'string',
    autoWidth: 20
},
{
    name: 'drug_duration',
    type: 'numeric',
    width: 20
},
{
    name: 'qty_prescribed',
    type: 'numeric',
    width: 20
},
{
    name: 'qty_dispensed',
    type: 'numeric',
    width: 20
},
{
    name: 'unit_price',
    type: 'numeric',
    width: 40
},
{
    name: 'total_price',
    type: 'numeric',
    width: 45
},
{
    name: 'pharmacy',
    type: 'string'
}
]);


var pharmEditor = new Ext.ux.grid.RowEditor({
    saveText: 'Update'
});


var pharmStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'pharmacy.php', 
        method: 'POST'
    }),
    reader:pharmReader,
    sortInfo: {
        field:'pharmauto',
        direction:'ASC'
    },
    groupField:'pharmacy'
});
pharmStore.load();


var pharmsGrid = new Ext.grid.EditorGridPanel({
    id: 'pharm-panel',
    title: 'Pending Presciptions',
    draggable: true,
    store: pharmStore,
    colModel: createColModel(12),
    loadMask: true,
    autoHeight: true,
    width: 900,
    clicksToEdit: 2,
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),

    stripeRows: true,
    
    plugins: [filters],

    listeners: {
        render: {
            fn: function(){
                pharmStore.load({
                    params: {
                        start: 0,
                        limit: 20
                    }
                });
            }
        }
    },

    tbar: [{
        text: 'New Adult Prescription ',
        tooltip: 'Click to Open Adult Pharmacy Form',
        handler: function ()
        {
            Ext.getCmp('content-panel').layout.setActiveItem('adult-panel');
            getioform(); 

        }

    },{
        text: 'New Pediatric Prescription ',
        tooltip: 'Click to Open Pediatric Pharmacy Form',
        iconCls:'refresh',
        handler: function ()
        {
            Ext.getCmp('content-panel').layout.setActiveItem('pediatric-panel');
          
        }

    }, '->',
    {
        text: 'Update ',
        tooltip: 'Click to Update Changes Made',
        handler: handleEdit,
        iconCls:'refresh'
    }],

    bbar: new Ext.PagingToolbar({
        store: pharmStore,
        pageSize: 20,
        plugins: [filters]
    })
});
pharmsGrid.addListener('afteredit', handleEdit);

function refreshGrid() {
    pharmStore.reload();//
} // end refresh


function handleEdit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;

    //start the process to update the db with cell contents
    updateDB(editEvent);

}

//function handleDelete() {
//
//    //returns array of selected rows ids only
//    var selectedKeys = pharmsGrid.selModel.selections.keys;
//    if(selectedKeys.length > 0)
//    {
//        Ext.MessageBox.confirm('Message','Do you really want to delete selection?', deleteRecord);
//    }
//    else
//    {
//        Ext.MessageBox.alert('Message','Please select at least one item to delete');
//    }//end if/else block
//}; // end handleDelete

function updateDB(oGrid_Event) {
//
    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
    } else
{
        var fieldValue = oGrid_Event.value;
    }

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {   //Specify options (note success/failure below that
        //receives these same options)
        waitMsg: 'Saving changes...',
        //url where to send request (url to server side script)
        url: 'updatePharmacy.php',

        //If specify params default is 'POST' instead of 'GET'
//        method: 'POST',

        //params will be available server side via $_POST or $_REQUEST:
        params: {
            task: "update", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used

            //For existing records this is the unique id (we need
            //this one to relate to the db). We'll check this
            //server side to see if it is a new record
            keyID: oGrid_Event.record.data.pharmauto,

            //For new records Ext creates a number here unrelated
            //to the database
            //            bogusID: oGrid_Event.record.id,

            field: oGrid_Event.field,//the column name
            value: fieldValue,//the updated value

            //The original value (oGrid_Event.orginalValue does
            //not work for some reason) this might(?) be a way
            //to 'undo' changes other than by cookie? When the
            //response comes back from the server can we make an
            //undo array?
            originalValue: oGrid_Event.record.modified

        },//end params

        //the function to be called upon failure of the request
        //(404 error etc, ***NOT*** success=false)
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Unable to Update...');
        //ds.rejectChanges();//undo any changes
        },//end failure block

        //The function to be called upon success of the request
        success:function(response,options){
//            Ext.MessageBox.alert('Success','Record Updated...');


            //if this is a new record need special handling
            if(oGrid_Event.record.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server

                //Extract the ID provided by the server
                var newID = responseData.newID;
                //oGrid_Event.record.id = newID;

                //Reset the indicator since update succeeded
                oGrid_Event.record.set('newRecord','no');

                //Assign the id to the record
                oGrid_Event.record.set('pharmauto',newID);
//                Note the set() calls do not trigger everything
//                since you may need to update multiple fields for
//                example. So you still need to call commitChanges()
//                to start the event flow to fire things like
//                refreshRow()

                //commit changes (removes the red triangle which
//                //indicates a 'dirty' field)
                pharmStore.commitChanges();
//
//            //var whatIsTheID = oGrid_Event.record.modified;
//
//            //not a new record so just commit changes
            } else {
//                //commit changes (removes the red triangle
//                //which indicates a 'dirty' field)
                pharmStore.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
} //end updateDB

//function deleteRecord(btn) {
//    if(btn=='yes')
//    {
//
//        //returns record objects for selected rows (all info for row)
//        var selectedRows = pharmsGrid.selModel.selections.items;
//        //returns array of selected rows ids only
//        var selectedKeys = pharmsGrid.selModel.selections.keys;
//        //encode array into json
//         Ext.MessageBox.alert('OK',selectedKeys)
//        var encoded_keys = Ext.encode(selectedKeys);
//        //submit to server
//        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
//        {   //specify options (note success/failure below that receives these same options)
//            waitMsg: 'Saving changes...',
//            //url where to send request (url to server side script)
//            url: 'getRates.php',
//            //params will be available via $_POST or $_REQUEST:
//            params: {
//                task: "delete", //pass task to do to the server script
//                ID: encoded_keys,//the unique id(s)
//                key: primaryKey//pass to server same 'id' that the reader used
//            },
//
//            callback: function (options, success, response) {
//                if (success) { //success will be true if the request succeeded
//                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
//                    var json = Ext.util.JSON.decode(response.responseText);
//
//                    Ext.MessageBox.alert('OK',json.del_count + ' record(s) deleted.');
//
//                } else{
//                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
//                }
//            },
//
//            //the function to be called upon failure of the request (server script, 404, or 403 errors)
//            failure:function(response,options){
//                Ext.MessageBox.alert('Warning','Oops...');
//            //ds.rejectChanges();//undo any changes
//            },
//            success:function(response,options){
//                //Ext.MessageBox.alert('Success','Yeah...');
//                //commit changes and remove the red triangle which
//                //indicates a 'dirty' field
//                pharmStore.reload();
//            }
//        } //end Ajax request config
//        );// end Ajax request initialization
//    };//end if click 'yes' on button
//} // end deleteRecord



var pharmGrid = new Ext.Window(
{
    autoWidth: true,
    autoHeight: true,
    autoScroll:true,
    id: 'pharm-panel',
    items:[pharmsGrid]
});
