//
//Ext.QuickTips.init();
//
//var fm = Ext.form;
//
//var pharmReader = new Ext.data.JsonReader(
//{
//    url: 'pharmacy.php',
//    root: 'pharm_list'
//    //        fields: ['enrollment_no', 'drug','qty_prescribed','qty_dispensed','unit_price','total_price']
//},
//[{
//        name: 'pharmauto',
//        type:'numeric'
//    },
//    {
//        name: 'enrollment_no',
//        type:'numeric'
//    },
//    {
//        name: 'drug',
//        type: 'string'
//    },
//    {
//        name: 'qty_prescribed',
//        type: 'numeric'
//    },
//    {
//        name: 'qty_dispensed',
//        type: 'numeric'
//    },
//    {
//        name: 'unit_price',
//        type: 'numeric'
//    },
//    {
//        name: 'total_price',
//        type: 'numeric'
//    },
//    {
//        name: 'pharmacy',
//        type: 'string'
//    }
//]);
//
//
//var pharmStore = new Ext.data.GroupingStore({
//    proxy: new Ext.data.HttpProxy({
//        url: 'pharmacy.php', //url to data object (server side script)
//        method: 'POST'
//    }),
//    reader: pharmReader,
//    sortInfo: {
//        field:'pharmauto',
//        direction:'ASC'
//    },
//    groupField:'pharmacy'
//});
//pharmStore.load();
////pharmStore.refreshData();
////store.on('add', pharmStore.refreshData, pharmStore);
////store.on('remove', pharmStore.refreshData, pharmStore);
////store.on('update', pharmStore.refreshData, pharmStore);
//
//var pharmEditor = new Ext.ux.grid.RowEditor({
//    saveText: 'Update'
//});
//
//
//var filters = new Ext.ux.grid.GridFilters({
//    autoReload: false, //don&#39;t reload automatically
//    local: true, //only filter locally
//    filters: [
//        {
//            type: 'numeric',
//            width: 80,
//            dataIndex: 'enrollment_no'
//        }, {
//            type: 'string',
//            width: 150,
//            dataIndex: 'drug'
//            //disabled: true
//        }, {
//            type: 'string',
//            width: 80,
//            dataIndex: 'qty_prescribed'
//        }, {
//            type: 'numeric',
//            width: 80,
//            dataIndex: 'qty_dispensed'
//        }, {
//            type: 'numeric',
//            width: 80,
//            dataIndex: 'unit_price'
//        }, {
//            type: 'numeric',
//            width: 80,
//            dataIndex: 'total_price'
//        }, {
//            type: 'string',
//            width: 80,
//            dataIndex: 'pharmacy'
//        }]
//});
//
//var createColModel = function (finish, start) {
//
//    var columns = [
//        new Ext.grid.RowNumberer(),
//        {
//            dataIndex: 'pharmauto',
//            header: 'ID',
//            width: 40
//            //        editor: new fm.TextField({
//            //            allowBlank: false
//            //        }),
//            //        filterable: true,
//            //        filter: { type: 'string'  }
//        },{
//            dataIndex: 'enrollment_no',
//            header: 'Patient Number',
//            filterable: true,
//            //            editor: new fm.TextField({
//            //                allowBlank: false
//            //            }),
//            filter: {
//                type: 'numeric'
//            }
//        }, {
//            dataIndex: 'drug',
//            header: 'Drug Name',
//            //            editor: new fm.TextField({
//            //                allowBlank: false
//            //            }),
//            filter: {
//                type: 'string'
//            }
//        }, {
//            dataIndex: 'qty_prescribed',
//            header: 'Amount Prescribed',
//            //            editor: new fm.TextField({
//            //                allowBlank: false
//            //            }),
//            filter: {
//                type: 'numeric'
//            }
//        }, {
//            dataIndex: 'qty_dispensed',
//            header: 'Amount Dispensed',
//            editor: new fm.TextField({
//                allowBlank: false
//            }),
//            filter: {
//                type: 'numeric'
//            }
//        }, {
//            dataIndex: 'unit_price',
//            header: 'Cost per Unit',
//            //            editor: new fm.TextField({
//            //                allowBlank: false
//            //            }),
//            filter: {
//                type: 'numeric'
//            }
//        }, {
//            dataIndex: 'total_price',
//            header: 'Total Charges',
//            format: 'Ksh.0,0.00',
//            renderer: function(v, params, record){
//                return Ext.util.Format.usMoney(record.data.qty_dispensed * record.data.unit_price);
//            },
//
//            //        editor: new fm.TextField({
//            //            allowBlank: false
//            //        }),
//            filter: {
//                type: 'numeric'
//            }
//        }, {
//            dataIndex: 'pharmacy',
//            header: 'Pharmacy Type',
//            hidden: true,
//            //            editor: new fm.TextField({
//            //                allowBlank: false
//            //            }),
//            filter: {
//                type: 'string'
//            }
//        }];
//
//    return new Ext.grid.ColumnModel({
//        columns: columns.slice(start || 0, finish),
//        defaults: {
//            sortable: true
//        }
//    });
//};
//
//
//    var pharmsGrid = new Ext.grid.GridPanel({
//    store: pharmStore,
//    width: 600,
//    autoHeight: true,
//    //    region:'center',
//    margins: '0 5 5 5',
//    autoExpandColumn: 'drug',
//    plugins: [pharmEditor, filters],
//    view: new Ext.grid.GroupingView({
//        markDirty: false,
//        forceFit:true,
//        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
//    }),
//    tbar: [{
//            iconCls: 'icon-user-add',
//            text: 'New Adult Prescription',
//            handler: function(){
//                Ext.getCmp('content-panel').layout.setActiveItem('adult-panel');
//            }
//        },{
//            iconCls: 'icon-user-delete',
//            text: 'New Pediatric Prescription',
//            handler: function(){
//                Ext.getCmp('content-panel').layout.setActiveItem('pediatric-panel');
//            }
//        }],
//
//    bbar: new Ext.PagingToolbar({
//        store: pharmStore,
//        pageSize: 20,
//        plugins: [filters]
//    })
//
//    });
//
//    var pharmGrid = new Ext.Window({
//        id: 'pharm-panel',
//        title: 'Pharmacy Order Form',
//        //    layout: 'border',
//        layoutConfig: {
//            columns: 8
//        },
//        autoWidth:true,
//        autoHeight: true,
//        items: [pharmsGrid]
//    });
//    //pharmGrid.render(Ext.getBody());
//
////    pharmsGrid.getSelectionModel().on('selectionchange', function(sm){
////        pharmsGrid.removeBtn.setDisabled(sm.getCount() < 1);
////});
////});
