var fm = Ext.form;

var prescReader = new Ext.data.JsonReader(
{
    url: 'prescription.php',
    root: 'prescribed_list'
//        fields: ['enrollment_no', 'drug','qty_prescribed','qty_dispensed','unit_price','total_price']
},
[{
    name: 'article',
    type:'string'
},
{
    name: 'partcode',
    type: 'numeric'
},
{
    name: 'drug_class',
    type: 'string'
},
{
    name: 'prescribe_date',
    type: 'string'
},
{
    name: 'prescriber',
    type: 'string'
}
]);


var filters = new Ext.ux.grid.GridFilters({
    autoReload: false, //don&#39;t reload automatically
    local: true, //only filter locally
    filters: [{
        type: 'string',
        width: 80,
        dataIndex: 'article'
    }, {
        type: 'numeric',
        width: 150,
        dataIndex: 'partcode'
    //disabled: true
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'drug_class'
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'prescribe_date'
    }, {
        type: 'string',
        width: 80,
        dataIndex: 'prescriber'
    }]
});


var createColModel = function (finish, start) {

    var columns = [{
            dataIndex: 'article',
            header: 'Drug Name',
            filterable: true,
            editor: new fm.TextField({
                allowBlank: false
            }),
           filter: {type: 'string'}
        }, {
            dataIndex: 'partcode',
            header: 'Partcode',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
        }, {
            dataIndex: 'drug_class',
            header: 'Drug Class',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
        }, {
            dataIndex: 'prescribe_date',
            header: 'Prescription Date',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
            }, {
            dataIndex: 'prescriber',
            header: 'Consultant Name',
            editor: new fm.TextField({
                allowBlank: false
            }),
            filter: {
                type: 'string'
            }
            }];

    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
};

 var prescStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'prescription.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
//        task: "readRates"
    },//This
    reader:prescReader,
    sortInfo: {
        field:'article',
        direction:'ASC'
    },
    groupField:'article'
});

var drugprescGrid = new Ext.grid.GridPanel({
    //border: false,
    title: 'Drug Consumption',
    //        region: 'center',
    store: prescStore,
    colModel: createColModel(6),
    loadMask: true,
    autoHeight: true,
    width: 700,
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    plugins: [filters],

    listeners: {
        render: {
            fn: function(){
                prescStore.load({
                    params: {
                        start: 0,
                        limit: 20
                    }
                });
            //g.getsSelectionModel().selectRow(0)
            //delay: 10
            }
        }
    },

    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("drugCon_info").getForm().loadRecord(rec);
            }
        }
    }),

    tbar: [{
        text: 'Export To Excel',
        tooltip: 'Click to Export as Excel Format',
        handler: function () {
                    document.location.href='reports/';
                },
        iconCls:'refresh'
    }],

    bbar: new Ext.PagingToolbar({
        store: prescStore,
        pageSize: 20,
        plugins: [filters]
    })
});

var drugGrid = {
    Xtype: 'panel',
    id: 'prescCon-panel',
    //    title: 'Report: Patients Enrolled In ART',
    items:[drugprescGrid]
};