<script type="text/javascript" src="composite-tests.js"></script>

<div id="profile" class="main_container">
    <div align="center">
        <div class="profile_main_container_form">
            <form class="" method="post" action="care_person.php" onSubmit="" name="care_profile">
                <div class="section profile_section_1">
                    <div class="profile_section_1_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold">Patient Name:</label>
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="patient_name" id="patient_name" type="text" size="31" maxlength="31" readonly="true" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> ID: </label>
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="satellite_no" id="satellite_no" type="text" size="2" maxlength="2" readonly="true" />
                            <label class="font_bold" style="padding-right: 5px;"></label>
                            &#8211;
                            <label class="font_bold" style="padding-right: 5px;"></label>
                            <input name="patient_enrollment_no" id="patient_enrollment_no" type="text" size="9" maxlength="9" readonly="true" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold" style="padding-right: 35px;"></label>
                            <label class="font_bold">Satellite#</label>
                            <label class="font_bold" style="padding-right: 10px;"></label>
                            <label class="font_bold">Patient Enrollment#</label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Primary Provider: </label>
                            <label class="font_bold" style="padding-right: 2px;"></label>
                            <input name="primary_provider" id="primary_provider" type="text" size="15" maxlength="15" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Date Of Birth: </label>
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="dob" id="dob" type="text" size="15" maxlength="20" readonly="true" />
                        </div>
                    </div>
                    <div class="profile_section_1_right">
                        <div class=" spacer"></div>
                        <div class="section_row">
                            <label class="font_bold">Phone Number: </label>
                            <label class="font_bold" style="padding-right: 10px;"></label>
                            <input name="phone_no" id="phone_no" type="text" size="12" maxlength="10" />
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <label class="font_bold"> Date of HIV Diagnosis: </label>
                            <label class="font_bold" style="padding-right: 10px;"></label>

                            <?php
                            $fullDate->genDayDDL("diagnosisday");
                            $fullDate->genMonthDDL("diagnosismonth");
                            $fullDate->genYearDDL("diagnosisyear");
                            ?>

                        </div>
                        <div class=" spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Hosp/Clinic#: </label>
                            <label class="font_bold" style="padding-right: 23px;"></label>
                            <input name="hosp_no" id="hosp_no" type="text" size="8" maxlength="8" readonly="true"/>
                            <label class="font_bold" style="padding-right: 28px;"></label>
                            <label class="font_bold"> Date of Enrollment: </label>
                            <label class="font_bold" style="padding-right: 26px;"></label>

                            <?php
                            $fullDate->genDayDDL("enrollmentday");
                            $fullDate->genMonthDDL("enrollmentmonth");
                            $fullDate->genYearDDL("enrollmentyear");
                            ?>

                        </div>
                        <div class=" spacer"></div>
                        <div class="section_row">
                            <label class="font_bold"> Date Care Ended: </label>
                            <label class="font_bold" style="padding-right: 4px;"></label>

                            <?php
                            $fullDate->genDayDDL("careEndday");
                            $fullDate->genMonthDDL("careEndmonth");
                            $fullDate->genYearDDL("careEndyear");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class = "section_row">
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <label class=" font_bold"> Reason(s) </label>
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <textarea name="care_end_reason" id="care_end_reason"  cols="19" rows="3"> </textarea>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section profile_section_2">
                    <div class="profile_section_2_left">
                        <label class="font_bold" style="float:left;"> ARV Treatment History </label>
                        <label class="font_bold" style="padding-right: 20px;"></label>
                        <label class="font_bold"> Date </label>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> Lowest CD4: </label>
                            <label class="font_bold" style="padding-right: 40px;"></label>
                            <input name="lowest_cd4" id="lowest_cd4" type="text" size="8" maxlength="8" />
                            <label class="font_small"> % </label>
                            <label class="font_bold" style="padding-right: 40px;"></label>

                            <?php
                            $fullDate->genMonthDDL("lowestCD4month");
                            $fullDate->genYearDDL("lowestCD4year");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> Viral Load prior to ARV: </label>
                            <label class="font_bold" style="padding-right: 7px;"></label>
                            <input name="viral_load_prior_arv" id="viral_load_prior_arv" type="text" size="4" maxlength="4" />
                            <label class="font_bold" style="padding-right: 55px;"></label>

                            <?php
                            $fullDate->genMonthDDL("priormonth");
                            $fullDate->genYearDDL("prioryear");
                            ?>

                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> ARV Start Date: </label>
                            <label class="font_bold" style="padding-right: 20px;"></label>

                            <?php
                            $fullDate->genDayDDL("artStartday");
                            $fullDate->genMonthDDL("artStartmonth");
                            $fullDate->genYearDDL("artStartyear");
                            ?>

                        </div>
                    </div>
                    <div class="profile_section_2_right">
                        <label class="font_bold" style=" float:left;"> ARV Regimens </label>
                        <label class="font_bold" style="padding-right: 70px;"></label>
                        <label class=" font_bold"> Date </label>
                        <label class="font_bold" style="padding-right: 60px;"></label>
                        <label class="font_bold"> Months </label>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> Current Regimen: </label>
                            <label class="font_bold" style="padding-right: 6px;"></label>
                            <?php
                            $regimen->getRegimens("reg1");
                            ?>

                            <label class="font_bold" style="padding-right: 30px;"></label>

                            <?php
                            $fullDate->genMonthDDL("reg1_month");
                            $fullDate->genYearDDL("reg1_year");
                            ?>

                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="reg1_months" id="reg1_months" type="text" size="3" maxlength="3" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> Previous Regimen: </label>

                            <?php
                            $regimen ->getRegimens("reg2");
                            ?>

                            <label class="font_bold" style="padding-right: 30px;"></label>

                            <?php
                            $fullDate->genMonthDDL("reg2_month");
                            $fullDate->genYearDDL("reg2_year");
                            ?>

                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="reg2_months" id="reg2_months" type="text" size="3" maxlength="3" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label> Previous Regimen: </label>

                            <?php
                            $regimen ->getRegimens("reg3");
                            ?>

                            <label class="font_bold" style="padding-right: 30px;"></label>

                            <?php
                            $fullDate->genMonthDDL("reg3_month");
                            $fullDate->genYearDDL("reg3_year");
                            ?>

                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="reg3_months" id="reg3_months" type="text" size="3" maxlength="3" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label>Previous Regimen:</label>

                            <?php
                            $regimen ->getRegimens("reg4");
                            ?>

                            <label class="font_bold" style="padding-right: 30px;"></label>

                            <?php
                            $fullDate->genMonthDDL("reg4_month");
                            $fullDate->genYearDDL("reg4_year");
                            ?>

                            <label class="font_bold" style="padding-right: 20px;"></label>
                            <input name="reg4_months" id="reg4_months" type="text" size="3" maxlength="3" />
                        </div>
                        <div class="spacer"></div>
                        <div class="section_row">
                            <label class="font_bold" style="padding-right: 60px;"></label>
                            <label> Single Dose NVP </label>
                            <label class="font_bold" style="padding-right: 20px;"></label>

                            <?php
                            $fullDate->genMonthDDL("singleDosemonth");
                            $fullDate->genYearDDL("singleDoseyear");
                            ?>

                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section profile_section_3">
                    <div class="profile_section_3_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="120%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                                    <td colspan="2"><label class="font_bold">HIV Associated Conditions</label></td>
                                    <td width="29%"></td>
                              </tr>
                                <tr>
                                    <td width="42%"></td>
                                  <td width="29%"><label class="font_bold">Initial Dx.</label></td>
                                  <td><label class="font_bold">Recurrence</label></td>
                                </tr>
                                <tr>
                                    <td><input name="assoc1" id="assoc1" type="checkbox" value="Assoc001" />
                                        Pulmonary TB</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc1_monthinit");
                                        $fullDate->genYearDDL("assoc1_yearinit");

//                            $associate__pulmon = concat_ws('-', $_POST['tbinityear'],$_POST['tbinitmonth']);
//                            $associate__pulmon =  mysql_real_escape_string($associate__pulmon);

                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc1_monthrec");
                                        $fullDate->genYearDDL("assoc1_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc2" id="assoc2" type="checkbox" value="Assoc003" />
                                        Pneumonia</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc2_monthinit");
                                        $fullDate->genYearDDL("assoc2_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc2_monthrec");
                                        $fullDate->genYearDDL("assoc2_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc3" id="assoc3" type="checkbox" value="Assoc005" />
                                        Candidiasis - Oral</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc3_monthinit");
                                        $fullDate->genYearDDL("assoc3_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc3_monthrec");
                                        $fullDate->genYearDDL("assoc3_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc4" id="assoc4" type="checkbox" value="Assoc007" />
                                        Cryptococcal Meningitis</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc4_monthinit");
                                        $fullDate->genYearDDL("assoc4_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc4_monthrec");
                                        $fullDate->genYearDDL("assoc4_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc5" id="assoc5" type="checkbox" value="Assoc008" />
                                        Herpes Zoster</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc5_monthinit");
                                        $fullDate->genYearDDL("assoc5_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc5_monthrec");
                                        $fullDate->genYearDDL("assoc5_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc6" id="assoc6" type="checkbox" value="Assoc010" />
                                        Herpes Simplex</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc6_monthinit");
                                        $fullDate->genYearDDL("assoc6_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc6_monthrec");
                                        $fullDate->genYearDDL("assoc6_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc7" id="assoc7" type="checkbox" value="Assoc013" />
                                        Salmonellosis</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc7_monthinit");
                                        $fullDate->genYearDDL("assoc7_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc7_monthrec");
                                        $fullDate->genYearDDL("assoc7_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc8" id="assoc8" type="checkbox" value="Assoc015" />
                                        Septicemia</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc8_monthinit");
                                        $fullDate->genYearDDL("assoc8_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc8_monthrec");
                                        $fullDate->genYearDDL("assoc8_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc9" id="assoc9" type="checkbox" value="Assoc017" />
                                        Genital Ulcerative Disease</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc9_monthinit");
                                        $fullDate->genYearDDL("assoc9_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc9_monthrec");
                                        $fullDate->genYearDDL("assoc9_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc10" id="assoc10" type="checkbox" value="Assoc020" />
                                        Urethritis/ Cervicitis</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc10_monthinit");
                                        $fullDate->genYearDDL("assoc10_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc10_monthrec");
                                        $fullDate->genYearDDL("assoc10_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc11" id="assoc11" type="checkbox" value="Assoc021" />
                                        PID</td>
                                    <td><?php

                                        $fullDate->genMonthDDL("assoc11_monthinit");
                                        $fullDate->genYearDDL("assoc11_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc11_monthrec");
                                        $fullDate->genYearDDL("assoc11_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc12" id="assoc12" type="checkbox" value="Assoc0" />
                                        Other</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc12_monthinit");
                                        $fullDate->genYearDDL("assoc12_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc12_monthrec");
                                        $fullDate->genYearDDL("assoc12_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc13" id="assoc13" type="checkbox" value="Assoc0" />
                                        Other</td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc13_monthinit");
                                        $fullDate->genYearDDL("assoc13_yearinit");
                                        ?></td>
                                    <td><?php
                                        $fullDate->genMonthDDL("assoc13_monthrec");
                                        $fullDate->genYearDDL("assoc13_yearrec");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                      </div>
                    </div>
                    <div class="profile_section_3_right">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="110%" border="0" cellpadding="0" cellspacing="0">
<tr>
                                    <td></td>
                    <td ></td>
                  <td ></td>
                              </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><label class="font_bold">Initial Dx</label></td>
                                    <td><label class="font_bold">Recurrence </label></td>
                                </tr>
                                <tr>
                                    <td><input name="assoc14" id="assoc14" type="checkbox" value="Assoc002" /> Extrapulmonary TB</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc14_monthinit");
                                            $fullDate->genYearDDL("assoc14_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc14_monthrec");
                                            $fullDate->genYearDDL("assoc14_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc15" id="assoc15" type="checkbox" value="Assoc004" /> PCP</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc15_monthinit");
                                            $fullDate->genYearDDL("assoc15_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc15_monthrec");
                                            $fullDate->genYearDDL("assoc15_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc16" id="assoc16" type="checkbox" value="Assoc006" />
                                            Candidiasis - Oesophageal</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc16_monthinit");
                                            $fullDate->genYearDDL("assoc16_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc16_monthrec");
                                            $fullDate->genYearDDL("assoc16_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc17" id="assoc17" type="checkbox" value="Assoc008" />
                                            Encephalopathy/ Dementia</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc17_monthinit");
                                            $fullDate->genYearDDL("assoc17_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc17_monthrec");
                                            $fullDate->genYearDDL("assoc17_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc18" id="assoc18" type="checkbox" value="Assoc011" />
                                            Neuro(Toxo,PML,Lymphoma)</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc18_monthinit");
                                            $fullDate->genYearDDL("assoc18_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc18_monthrec");
                                            $fullDate->genYearDDL("assoc18_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc19" id="assoc19" type="checkbox" value="Assoc012" />
                                            Mycobacteria Other</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc19_monthinit");
                                            $fullDate->genYearDDL("assoc19_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc19_monthrec");
                                            $fullDate->genYearDDL("assoc19_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc20" id="assoc20" type="checkbox" value="Assoc014" />
                                            Chronic Diarrhoea/ Wasting</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc20_monthinit");
                                            $fullDate->genYearDDL("assoc20_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc20_monthrec");
                                            $fullDate->genYearDDL("assoc20_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc21" id="assoc21" type="checkbox" value="Assoc016" />
                                            CMV Retinitis</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc21_monthinit");
                                            $fullDate->genYearDDL("assoc21_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc21_monthrec");
                                            $fullDate->genYearDDL("assoc21_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc22" id="assoc22" type="checkbox" value="Assoc018" />
                                            KS - Cutaneous</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc22_monthinit");
                                            $fullDate->genYearDDL("assoc22_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc22_monthrec");
                                            $fullDate->genYearDDL("assoc22_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc23" id="assoc23" type="checkbox" value="Assoc019" />
                                            KS - Visceral</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc23_monthinit");
                                            $fullDate->genYearDDL("assoc23_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc23_monthrec");
                                            $fullDate->genYearDDL("assoc23_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc24" id="assoc24" type="checkbox" value="Assoc022" />
                                            Lymphoma</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc24_monthinit");
                                            $fullDate->genYearDDL("assoc24_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc24_monthrec");
                                            $fullDate->genYearDDL("assoc24_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc25" id="assoc25" type="checkbox" value="Assoc0" />Other</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc25_monthinit");
                                            $fullDate->genYearDDL("assoc25_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc25_monthrec");
                                            $fullDate->genYearDDL("assoc25_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input name="assoc26" id="assoc26" type="checkbox" value="Assoc0" />Other</td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc26_monthinit");
                                            $fullDate->genYearDDL("assoc26_yearinit");
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                            $fullDate->genMonthDDL("assoc26_monthrec");
                                            $fullDate->genYearDDL("assoc26_yearrec");
                                            ?>
                                        </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                      </div>
                    </div>

                </div>
                <div class="spacer"></div>
                <div class="section profile_section_4" id="docbody2">
                </div>
                <div class="spacer"></div>
                <div class="section profile_section_5">
                    <div class="spacer"></div>
                    <div class="profile_section_5_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />

                            <input id="Reset" type="reset" value="Reset" />
                        </div>
                    </div>
                    <div class="profile_section_5_right">

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>