<div id="tracking" class="main_container">
    <div align="center">
        <div class="main_container_form">
            <form class="" method="post" action="contact_care_tracking.php" name="contact_care_tracking" >
                <div class="section tracking_section_1">
                    <div class="tracking_section_1_left">
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                <tr>
                                    <td width="55%"><label class="font_bold">Patient name:</label></td>
                                    <td colspan="3"><input name="patient_name" id="patient_name" type="text" size="35" maxlength="35" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">ID:</label></td>
                                    <td width="6%"><input name="satellite_no" id="satellite_no" type="text" size="3" maxlength="3" readonly="true"/></td>
                                    <td width="3%" >&nbsp;</td>
                                    <td width="36%"><input name="patient_enrollment_no" id="patient_enrollment_no" type="text" size="7" maxlength="7" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label>Satellite#</label></td>
                                    <td>&nbsp;</td>
                                    <td><label>Patient Enrollment#</label></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">Patient Contact Information:</label></td>
                                    <td colspan="3"><input name="patient_contact_info" id="patient_contact_info" type="text" size="45" maxlength="45" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">Guardian Name:</label></td>
                                    <td colspan="3"><input name="guardian_name" id="guardian_name" type="text" size="45" maxlength="45" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">Guardian Contact Information:</label></td>
                                    <td colspan="3"><input name="guardian_contact_info" id="guardian_contact_info" type="text" size="45" maxlength="45" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tracking_section_1_right">
                        <div class="spacer"></div>

                        <div class="section_row">
                            <label class=" font_bold"> Existing Hosp/ Clinic No: </label>
                            <label class="font_bold" style="padding-right: 4px;"></label>
                            <input name="hosp_no" id="hosp_no" type="text" size="9" maxlength="9" readonly="true"/>

                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section tracking_section_2">
                    <div class="tracking_section_2_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                <tr>
                                    <td width="1%">1.</td>
                                    <td colspan="3"><label class="font_bold">Date of Missed Scheduled Appointment:</label></td>
                                    <td colspan="3"><?php
                                        $fullDate->genDayDDL("cct_appointmentday");
                                        $fullDate->genMonthDDL("cct_appointmentmonth");
                                        $fullDate->genYearDDL("cct_appointmentyear");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><label class="font_bold">Date of Last Actual Contact:</label></td>
                                    <td colspan="3"><?php
                                        $fullDate->genDayDDL("cct_contactday");
                                        $fullDate->genMonthDDL("cct_contactmonth");
                                        $fullDate->genYearDDL("cct_contactyear");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="23%">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td width="21%"><label class="font_bold">Who</label></td>
                                    <td width="16%"><label class="font_bold">Mode of</label></td>
                                    <td width="15%"><label class="font_bold">Guardian/ CHV/</label></td>
                                </tr>

                                <tr>
                                    <td>2.</td>
                                    <td><label class="font_bold">Attempted to Contact</label></td>
                                    <td colspan="2"><label class="font_bold">Date</label></td>
                                    <td><label class="font_bold">Attempted Contact?</label></td>
                                    <td><label class="font_bold">Communictaion</label></td>
                                    <td><label class="font_bold">CHW Contacted? </label></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold">1st Attempted Contact </label>
                                        <label class="font_bold" style="padding-right: 30px;"></label></td>
                                    <td colspan="2"><?php
                                        $fullDate->genDayDDL("cct_firstday");
                                        $fullDate->genMonthDDL("cct_firstmonth");
                                        $fullDate->genYearDDL("cct_firstyear");
                                        ?></td>
                                    <td><input name="first_attempt_contact" id="first_attempt_contact" type="text" size="25" maxlength="25" /></td>
                                    <td><input name="first_attempt_mode" id="first_attempt_mode2" type="radio" value="telephone"/>
                                        Telephone</td>
                                    <td><input name="first_attempt_guardian" id="first_attempt_guardian2" type="radio" value="yes"/>
                                        Yes </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input name="first_attempt_mode" id="first_attempt_mode" type="radio" value="home_visit"/>
                                        Home Visit</td>
                                    <td><input name="first_attempt_guardian" id="first_attempt_guardian" type="radio" value="no"/>
                                        No</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold">Other Attempted Contact</label></td>
                                    <td colspan="2"><?php
                                        $fullDate->genDayDDL("cct_secondday");
                                        $fullDate->genMonthDDL("cct_secondmonth");
                                        $fullDate->genYearDDL("cct_secondyear");
                                        ?></td>
                                    <td><input name="second_attempt_contact" id="second_attempt_contact" type="text" size="25" maxlength="25" /></td>
                                    <td><input name="second_attempt_mode" id="second_attempt_mode2" type="radio" value="telephone"/>
                                        Telephone</td>
                                    <td><input name="second_attempt_guardian" id="second_attempt_guardian2" type="radio" value="yes"/>
                                        Yes</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input name="second_attempt_mode" id="second_attempt_mode" type="radio" value="home_visit"/>
                                        Home Visit</td>
                                    <td><input name="second_attempt_guardian" id="second_attempt_guardian" type="radio" value="no"/>
                                        No</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold">Other Attempted Contact</label></td>
                                    <td colspan="2"><?php
                                        $fullDate->genDayDDL("cct_thirdday");
                                        $fullDate->genMonthDDL("cct_thirdmonth");
                                        $fullDate->genYearDDL("cct_thirdyear");
                                        ?></td>
                                    <td><input name="third_attempt_contact" id="third_attempt_contact" type="text" size="25" maxlength="25" /></td>
                                    <td><input name="third_attempt_mode" id="third_attempt_mode2" type="radio" value="telephone"/>
                                        Telephone</td>
                                    <td><input name="third_attempt_guardian" id="third_attempt_guardian2" type="radio" value="yes"/>
                                        Yes</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input name="third_attempt_mode" id="third_attempt_mode" type="radio" value="home_visit"/>
                                        Home Visit</td>
                                    <td><input name="third_attempt_guardian" id="third_attempt_guardian" type="radio" value="no"/>
                                        No </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold">Other Attempted Contact</label></td>
                                    <td colspan="2"><?php
                                        $fullDate->genDayDDL("cct_fourthday");
                                        $fullDate->genMonthDDL("cct_fourthmonth");
                                        $fullDate->genYearDDL("cct_fourthyear");
                                        ?></td>
                                    <td><input name="fourth_attempt_contact" id="fourth_attempt_contact" type="text" size="25" maxlength="25" /></td>
                                    <td><input name="fourth_attempt_mode" id="fourth_attempt_mode2" type="radio" value="telephone"/>
                                        Telephone</td>
                                    <td><input name="fourth_attempt_guardian" id="fourth_attempt_guardian2" type="radio" value="yes"/>
                                        Yes</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input name="fourth_attempt_mode" id="fourth_attempt_mode" type="radio" value="home_visit"/>
                                        Home Visit</td>
                                    <td><input name="fourth_attempt_guardian" id="fourth_attempt_guardian" type="radio" value="no"/>
                                        No</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold">Last Attempted Contact</label></td>
                                    <td colspan="2"><?php
                                        $fullDate->genDayDDL("cct_lastday");
                                        $fullDate->genMonthDDL("cct_lastmonth");
                                        $fullDate->genYearDDL("cct_lastyear");
                                        ?></td>
                                    <td><input name="last_attempt_contact" id="last_attempt_contact" type="text" size="25" maxlength="25" /></td>
                                    <td><input name="last_attempt_mode" id="last_attempt_mode2" type="radio" value="telephone"/>
                                        Telephone</td>
                                    <td><input name="last_attempt_guardian" id="last_attempt_guardian2" type="radio" value="yes"/>
                                        Yes</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>(after 3 months)</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="font_bold"></label></td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input name="last_attempt_mode" id="last_attempt_mode" type="radio" value="home_visit"/>
                                        Home Visit</td>
                                    <td><input name="last_attempt_guardian" id="last_attempt_guardian" type="radio" value="no"/>
                                        No</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tracking_section_2_right">

                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section tracking_section_3">
                    <div class="tracking_section_3_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="4"><label class="font_bold">3. Patient ARV ended but Still in Palliative care?</label></td>
                                    <td width="7%"><input name="patient_pallative_care" id="patient_pallative_care" type="radio" value="yes" />
                                        Yes  </td>
                                    <td width="7%"><input name="patient_pallative_care" id="patient_pallative_care2" type="radio" value="no"/>
                                        No</td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="15%">&nbsp;</td>
                                    <td colspan="5">&nbsp;</td>
                                    <td width="18%">&nbsp;</td>
                                    <td width="24%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><label class="font_bold">3a. Reasons:</label></td>
                                    <td colspan="5"><input name="patient_pallative_reason" id="patient_pallative_reason" type="radio" value="self_discontinuation"/>
                                        Self Discontinuation
                                        <label class="font_bold" style="padding-right: 15px;"></label>
                                        <input name="patient_pallative_reason" id="patient_pallative_reason" type="radio" value="physician_stopped"/>
                                        Physician Stopped</td>
                                    <td><label class="font_bold">Date ARV Ended:</label></td>
                                    <td><?php
                                        $fullDate->genDayDDL("cct_arvday");
                                        $fullDate->genMonthDDL("cct_arvmonth");
                                        $fullDate->genYearDDL("cct_arvyear");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label class="font_bold">4. Patient Care Ended?</label></td>
                                    <td width="9%"><input name="patient_care_ended" id="patient_care_ended2" type="radio" value="yes"  />
                                        Yes

                                        <label class="font_bold" style="padding-right: 15px;"></label></td>
                                    <td width="9%"><input name="patient_care_ended" id="patient_care_ended" type="radio" value="no" />
                                        No</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>

                        </div>
                    </div>
                    <div class="tracking_section_3_right">
                        <div class="spacer"></div>
                        <div class="section_row">


                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section tracking_section_4">
                    <div class="tracking_section_4_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="6"><label class="font_bold">5. Reason for Exit From Program: </label></td>
                                    <td colspan="2">&nbsp;</td>
                                    <td width="19%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="9"><input name="exit_reason" id="exit_reason2" type="radio" value="lost" />
                                        Lost To Follow Up
                                        <div class="section_row" style=" border: 1px solid black; float:right; width: 450px; min-height: 20px; text-align: left;">
                                            <label class="font_small"> A Patient is Lost to Follow up if they have not returned for their follow up appointment within
                                                three months of the last scheduled appointment. The facility should make best efforts to contact the patient. </label>
                                        </div></td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><input name="exit_reason" id="exit_reason3" type="radio" value="transfer" />
                                        Transfer to Another LPTF</td>
                                    <td colspan="6"><label class="font_bold">Transferred To:</label>
                                        <input name="exit_reason_comment2" id="exit_reason_comment2" type="text" size="45" maxlength="45" /></td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="13%">&nbsp;</td>
                                    <td colspan="4"><input name="exit_reason" id="exit_reason" type="radio" value="dropped" />
                                        Dropped Out of Care</td>
                                    <td width="7%">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4"><input name="exit_reason_extra" id="exit_reason_extra2" type="radio" value="self_discontinuation" />
                                        Self Discontinuation</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4"><input name="exit_reason_extra" id="exit_reason_extra3" type="radio" value="physician_stopped" />
                                        Physician Stopped</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="7"><input name="exit_reason_extra" id="exit_reason_extra4" type="radio" value="other" />
                                        Other (Specify)
                                        <input name="exit_reason_comment3" id="exit_reason_comment3" type="text" size="45" maxlength="45" /></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><input name="exit_reason_extra" id="exit_reason_extra5" type="radio" value="hiv_negative" />
                                        HIV Negative</td>
                                    <td colspan="5"><input name="exit_reason_extra" id="exit_reason_extra6" type="radio" value="death" />
                                        Death
                                        <?php
                                        $fullDate->genDayDDL("cct_deathday");
                                        $fullDate->genMonthDDL("cct_deathmonth");
                                        $fullDate->genYearDDL("cct_deathyear");
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4"><input name="exit_reason_extra" id="exit_reason_extra7" type="radio" value="arv_seffects" />
                                        Suspected ARV Side Effects (specify)</td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4"><input name="exit_reason_comments" id="exit_reason_comments" type="text" size="45" maxlength="45" /></td>
                                    <td>&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5"><input name="exit_reason_extra" id="exit_reason_extra8" type="radio" value="opp_infections" />
                                        Suspected opportunistic Infection (specify)</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5"><input name="exit_reason_comment4" id="exit_reason_comment4" type="text" size="45" maxlength="45" /></td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5"><input name="exit_reason_extra" id="exit_reason_extra9" type="radio" value="other" />
                                        Other (specify)</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5"><input name="exit_reason_comment" id="exit_reason_comment" type="text" size="45" maxlength="45" /></td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5"><input name="exit_reason_extra" id="exit_reason_extra" type="radio" value="unknown" />
                                        Unknown</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6"> <label class="font_bold">Date Care Ended:</label>
                                        <?php
                                        $fullDate->genDayDDL("cct_careday");
                                        $fullDate->genMonthDDL("cct_caremonth");
                                        $fullDate->genYearDDL("cct_careyear");
                                        ?></td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                    <td colspan="2">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>

                        </div>
                    </div>
                    <div class="tracking_section_4_right">
                        <div class="spacer"></div>
                        <div class="section_row">


                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="section tracking_section_5">
                    <div class="spacer"></div>
                    <div class="tracking_section_5_left">
                        <div class="spacer"></div>
                        <div class="section_row">
                            <input id="Submit" type="submit" value="Submit" />
                            <label class="font_bold" style="padding-right: 4px;"></label>

                            <input id="Reset" type="reset" value="Reset" />
                        </div>
                    </div>
                    <div class="tracking_section_5_right">

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>