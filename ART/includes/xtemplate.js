

var getStatistics = new Ext.data.JsonStore({
    url: 'xtemplate.php', 
    method: 'POST',
    baseParams:{
        
    },
    root: '',
    fields:[
    {
        name: '',
        type: 'string'
    }
    ]
});


var tpl2 = new Ext.XTemplate(
    '<table border=0 width="100%">',
    '<tpl for=".">',
    '<tpl><tr class="statistics">',
    '<tr>',
    ' <td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    ' </tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Ever Enrolled Patients:</td>',
    ' <td>{Ever Enrolled Patients} </td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    ' <td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Females Enrolled:</td>',
    '<td>{Females Enrolled}</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    ' </tr>',
    ' <tr>',
    '<td>Males Enrolled:</td>',
    '<td>{Males Enrolled}</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Total Number of Active Patients:</td>',
    '<td>{Total Number of Active Patients}</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Active Non-ART Patients:</td>',
    '<td>{Active Non-ART Patients}</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>ART Patients:</td>',
    '<td>{ART Patients}</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>ART Mortality:</td>',
    '<td>{ART Mortality}</td>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Total ART Ever Enrolled:</td>',
    '<td>{Total ART Ever Enrolled}</td>',
    '</tr>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>Number Lost to Followup:</td>',
    '<td>{Number Lost to Followup}</td>',
    '</tr>',
    '<tr>',
    '<td>&nbsp;</td>',
    '<td>&nbsp;</td>',
    '</tr>',
    '<tr>',
    '<td>ART Unknown:</td>',
    '<td>{ART Unknown}</td>',
    '</tr>',
    '</tpl>',
    '</tpl>',
    '</table>',
    '<div class="x-clear"></div>'
    );


var statPanel = new Ext.Panel({
    width: 465,
    region:'center',
    margins: '0 5 5 5',
    items: new Ext.DataView({
        store: getStatistics,
        tpl: tpl2,
        autoHeight:true,
        multiSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.statistics'
    })
});


var sChart = new Ext.Panel({

    layout: 'border',
//    layoutConfig: {
//        columns: 1
//    },
    autoWidth: true,
    autoHeight: true,
    autoScroll:true,
    items: [statPanel]
});


getStatistics.load();






