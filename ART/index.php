<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>ARV Clinic Module</title>

        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css"/>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/gridfilters/css/GridFilters.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/gridfilters/css/RangeMenu.css" />
        <link rel="stylesheet" type="text/css" href="css/arv.css" />
        <link rel="stylesheet" type="text/css" href="css/layout-browser.css" />
        <link rel="stylesheet" type="text/css" href="css/CenterLayout.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/css/Portal.css" />

         <script type="text/javascript" src="../include/Extjs/yui/ext-yui-adapter.js"></script>
        <script type="text/javascript" src="../include/Extjs/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="../include/Extjs/ext-all-debug.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/CenterLayout.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/RowLayout.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/XmlTreeLoader.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/Portal.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/PortalColumn.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/Portlet.js"></script>
       
        
        <script type="text/javascript" src="../include/Extjs/ux/CheckColumn.js"></script>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/css/RowEditor.css" />
        <script type="text/javascript" src="../include/Extjs/ux/RowEditor.js"></script>

        <link rel="stylesheet" type="text/css" href="../include/Extjs/shared/examples.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/shared/icons/silk.css" />
        <script type="text/javascript" src="../include/Extjs/shared/examples.js"></script>
        <!-- Extensions - Paging Toolbar -->
        <script type="text/javascript" src="../include/Extjs/ux/paging/pPageSize.js"></script>

        <!-- Extensions - Filtering -->
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/menu/EditableItem.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/menu/RangeMenu.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/menu/ListMenu.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/GridFilters.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/filter/Filter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/filter/StringFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/filter/DateFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/filter/ListFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/filter/NumericFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/filter/BooleanFilter.js"></script>

        <!-- page specific -->
        <!--script type="text/javascript" src="includes/layouts/education.js"></script-->

        <!--script type="text/javascript" src="includes/composite-field.js"></script-->
        <script type="text/javascript" src="includes/chart1.js"></script>
        <script type="text/javascript" src="includes/xtemplate.js"></script>
        <script type="text/javascript" src="includes/chart2.js"></script>
        <script type="text/javascript" src="includes/layouts/basic.js"></script>
        <script type="text/javascript" src="includes/drugconsumption.js"></script>        
        <script type="text/javascript" src="includes/oi_drugconsumption.js"></script>
        <script type="text/javascript" src="includes/additional_lab.js"></script>
        <script type="text/javascript" src="includes/pcare.js"></script>
        <script type="text/javascript" src="includes/ccare.js"></script>
        <script type="text/javascript" src="includes/pharmacy.js"></script>
         <script type="text/javascript" src="includes/layout-browser.js"></script>
        <script type="text/javascript" src="statistics.js"></script>   
        <!--script type="text/javascript" src="includes/date-picker.js"></script-->


    </head>
    <body>

        <?php
        require('includes/DateDDLGenerator.php');
        require('includes/GenRegimen.php');
        require('includes/GenMedical.php');

        $fullDate = new DateDDLGenerator;
        $fullDate -> setToCurrentDay();

        $regimen = new GenRegimen;

        $medical = new GenMedical;
        ?>

        <div id="labwin">

        </div>
        <div id="header">

        </div>
        <div style="display:none;">
            <div id="start-div">
                <div style="float:left;" > </div>
                <div style="margin-left:100px;">
                    <h2>ARV Home</h2>
                </div>

                <!--div id="container3" style="padding-top: 50px; float: none;"></div-->

                <div id="container" style=" padding-top: 450px; float: left;"></div>

                <div id="container2" style=" padding-top: 450px; float: right;"></div>
            </div>
            <div id="adult-details">
                /*************************************************************************************************************/

                <!--?php require('includes/adult.php'); ?-->

                /*************************************************************************************************************/

            </div>
            <div id="pharm-details">
                /*************************************************************************************************************/

                <?php require('includes/adult.php'); ?>

                /*************************************************************************************************************/

            </div>
            <div id="followup-details">
                /***************************************************************************************************************/


                <?php require('includes/followup.php'); ?>


                /***************************************************************************************************************/

            </div>
            <div id="tracking-details">
                /***************************************************************************************************************/

                <?php require('includes/tracking.php'); ?>

                /***************************************************************************************************************/

            </div>
            <div id="family-details">
                <h2> Additional Family Information Details </h2>
                /***************************************************************************************************************/

                <?php require('includes/family.php'); ?>

                /***************************************************************************************************************/
            </div>
            <div id="enrollment-details">
                /***************************************************************************************************************/

                <?php require('includes/enrollment.php'); ?>

                /***************************************************************************************************************/
            </div>
            <div id="profile-details">
                /***************************************************************************************************************/

                <?php require('includes/profile.php'); ?>

                /***************************************************************************************************************/
            </div>
            <div id="initialData-details">
                /***************************************************************************************************************/

                <?php require('includes/initial.php'); ?>

                /***************************************************************************************************************/
            </div>
            <div id="initialData2-details">
                /***************************************************************************************************************/

                <?php require('includes/initial2.php'); ?>

                /***************************************************************************************************************/
            </div>
            <div id="laboratory-details">
                /***************************************************************************************************************/

                <?php require('includes/laboratory.php'); ?>

                /***************************************************************************************************************/
            </div>
            <div id="pediatric-details">
                /***************************************************************************************************************/

                <?php require('includes/pediatric.php'); ?>

                /***************************************************************************************************************/
            </div>
            /*
            *========================================= ART Reports =====================================
            */
            <div id="patients-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="pcare-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="ptracking-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="children-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="ccare-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="monthly-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="drugCon-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>
            <div id="oidrugCon-details">
                /***************************************************************************************************************/



                /***************************************************************************************************************/
            </div>

        </div>

    </body>
</html>

