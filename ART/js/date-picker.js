// JavaScript Document

//============================== Date Fields in Contact $ Care Tracking  Form ==============================
Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_appointment',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'missed_appointment_date',
      name:'missed_appointment_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});


Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_contact',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'last_contact_date',
      name:'last_contact_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_first',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'first_attempt_date',
      name:'first_attempt_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_second',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'second_attempt_date',
      name:'second_attempt_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_third',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'third_attempt_date',
      name:'third_attempt_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_fourth',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'fourth_attempt_date',
      name:'fourth_attempt_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_last',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'last_attempt_date',
      name:'last_attempt_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_arv',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'arv_end_date',
      name:'arv_end_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_death',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'death_date',
      name:'death_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'cct_care',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'care_end_date',
      name:'care_end_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//========================================== Date fields in laboratory Order results Form ============================================

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'lor_preclinic',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'lab_date',
      name:'lab_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//========================================== Date fields in Pediatric Pharmacy Order Form ============================================

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'ppo_dob',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'dob',
      name:'dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//================================= Date Fields in ART Follow-up Form =============================================
Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'arv_vdate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'visit_date',
      name:'visit_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});


Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'arv_sdate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'tb_therapy_start_date',
      name:'tb_therapy_start_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'arv_edate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'tb_therapy_end_date',
      name:'tb_therapy_end_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'arv_adate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'appointment_date',
      name:'appointment_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'arv_rx_date',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'adherence_rx_date',
      name:'adherence_rx_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//================================= Date Fields in HIV Care Enrollment Form =============================================
Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'hce_vdate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'visit_date',
      name:'visit_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});


Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'hce_bdate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'dob',
      name:'dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//============================== Date Fields in Family Information  Form ==============================
Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'family_vdate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'visit_date',
      name:'visit_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});


Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'family_bdate1',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'relative_1_dob',
      name:'relative_1_dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'family_bdate2',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'relative_2_dob',
      name:'relative_2_dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'family_bdate3',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'relative_3_dob',
      name:'relative_3_dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'family_bdate4',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'relative_4_dob',
      name:'relative_4_dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'family_bdate5',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'relative_5_dob',
      name:'relative_5_dob',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//========================================== Date fields in Adult Pharmacy Order Form ============================================

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'adult_dob',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'age',
      name:'age',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

//============================================ Date Fields in Initial Evaluation Form ==========================================
Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_vdate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'visit_date',
      name:'visit_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_ddate',
    //layout:'form',
    //labelAlign:'right',
    width:100,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'hiv_diagnosis_date',
      name:'hiv_diagnosis_date',
      width:100,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_ltdate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'current_longterm_date',
      name:'current_longterm_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_ldate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'medical_liver_date',
      name:'medical_liver_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_kdate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'medical_kidney_date',
      name:'medical_kidney_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_adate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'medical_anaemia_date',
      name:'medical_anaemia_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_drdate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'medical_drug_date',
      name:'medical_drug_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_modate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'medical_other_date',
      name:'medical_other_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_moodate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'medical_other2_date',
      name:'medical_other2_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});

Ext.onReady(function(){
  var formPanel = new Ext.Panel({
    //title:'Date of HIV Diagnosis',
    applyTo:'initial_tbdate',
    //layout:'form',
    //labelAlign:'right',
    width:95,
    //autoHeight:true,
	boarder:false,
    //frame:true,
    items:[{
      xtype:'datefield',
      //fieldLabel:'Date of HIV Diagnosis',
	  id:'tb_start_date',
      name:'tb_start_date',
      width:95,
	  //height: 40,
      allowBlank:false
    }]
  });
});