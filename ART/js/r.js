/*
 * Playpen Examples
 * Copyright(c) 2009, Mark Lancaster.
 *
 * This code is licensed under BSD license. Use it as you wish,
 * but keep this copyright intact.
 */

// Blank image (using Oracle Apex image)
Ext.BLANK_IMAGE_URL = '/i/1px_trans.gif';

// Create namespace if doesn't already exist
Ext.namespace('Ext.ux');

// Define an iframe component, better for garbage collection
Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
    url : null,
    onRender : function(ct, position){
        var url = this.url;
        this.el = ct.createChild({
            tag: 'iframe',
            id: 'iframe-'+ this.id,
            frameBorder: 0,
            src: url
        });
    }
});
Ext.reg('iframe', Ext.ux.IFrameComponent);


/**
 * Create a custom component.
 * This allows us to pre-configure some settings and not allow over-rides.
 * Also
 */
Ext.ux.PopupWindow = Ext.extend(Ext.Window, {
    url: '',
    title: document.title,
    width: 700,
    height: 600,
    initComponent: function(){

        this.isProcessed = false;

        var config = {
            border: false,
            closable: true,
            closeAction: 'close',
            height: this.height,
            items: [ new Ext.ux.IFrameComponent({
                id: id,
                url: this.url
            }) ],
            layout: 'fit',
            maximizable: true,
            modal: true,
            plain: false,
            title: this.title,
            width: this.width
        };

        // apply config
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        Ext.ux.PopupWindow.superclass.initComponent.call(this);

        // define custom events
        this.addEvents('success');

    },
    processSuccessful: function(){
        this.isProcessed = true;
        this.fireEvent("success", this);
    },
    hasChanged: function() {
        return this.isProcessed;
    },
    show: function(){
        this.isProcessed = false;
        Ext.ux.PopupWindow.superclass.show.call(this);
    },

    // private
    initTools : function(){
        // over-ride Ext.Window functions adding qtips to original code
        if(this.minimizable){
            this.addTool({
                id: 'minimize',
                handler: this.minimize.createDelegate(this, []),
                qtip: 'Minimize'
            });
        }
        if(this.maximizable){
            this.addTool({
                id: 'maximize',
                handler: this.maximize.createDelegate(this, []),
                qtip: 'Maximize'
            });
            this.addTool({
                id: 'restore',
                handler: this.restore.createDelegate(this, []),
                hidden:true,
                qtip: 'Restore size'
            });
            this.header.on('dblclick', this.toggleMaximize, this);
        }
        if(this.closable){
            this.addTool({
                id: 'close',
                handler: this[this.closeAction].createDelegate(this, []),
                qtip: 'Close'
            });
        }
    }

});

Ext.reg('PopupWindow', Ext.ux.PopupWindow);



///////////////////////////////////////////////////////

/*
 * Playpen Examples
 * Copyright(c) 2009, Mark Lancaster.
 *
 * This code is licensed under BSD license. Use it as you wish,
 * but keep this copyright intact.
 */

// Blank image (using Oracle Apex image)
Ext.BLANK_IMAGE_URL = '/i/1px_trans.gif';

// Create namespace if doesn't already exist
Ext.namespace('Ext.ux');

// Define an iframe component, better for garbage collection
Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
    url : null,
    onRender : function(ct, position){
        var url = this.url;
        this.el = ct.createChild({tag: 'iframe', id: 'iframe-'+ this.id, frameBorder: 0, src: url});
    }
});
Ext.reg('iframe', Ext.ux.IFrameComponent);


/**
 * Create a custom component.
 * This allows us to pre-configure some settings and not allow over-rides.
 * Also
 */
Ext.ux.PopupWindow = Ext.extend(Ext.Window, {
    url: '',
    title: document.title,
    width: 700,
    height: 600,
    initComponent: function(){

        this.isProcessed = false;

        var config = {
            border: false,
            closable: true,
            closeAction: 'close',
            height: this.height,
            items: [ new Ext.ux.IFrameComponent({ id: id, url: this.url }) ],
            layout: 'fit',
            maximizable: true,
            modal: true,
            plain: false,
            title: this.title,
            width: this.width
        };

        // apply config
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        Ext.ux.PopupWindow.superclass.initComponent.call(this);

        // define custom events
        this.addEvents('success');

    },
    processSuccessful: function(){
        this.isProcessed = true;
        this.fireEvent("success", this);
    },
    hasChanged: function() {
        return this.isProcessed;
    },
    show: function(){
        this.isProcessed = false;
        Ext.ux.PopupWindow.superclass.show.call(this);
    },

    // private
    initTools : function(){
        // over-ride Ext.Window functions adding qtips to original code
        if(this.minimizable){
            this.addTool({
                id: 'minimize',
                handler: this.minimize.createDelegate(this, []),
                qtip: 'Minimize'
            });
        }
        if(this.maximizable){
            this.addTool({
                id: 'maximize',
                handler: this.maximize.createDelegate(this, []),
                qtip: 'Maximize'
            });
            this.addTool({
                id: 'restore',
                handler: this.restore.createDelegate(this, []),
                hidden:true,
                qtip: 'Restore size'
            });
            this.header.on('dblclick', this.toggleMaximize, this);
        }
        if(this.closable){
            this.addTool({
                id: 'close',
                handler: this[this.closeAction].createDelegate(this, []),
                qtip: 'Close'
            });
        }
    }

});

Ext.reg('PopupWindow', Ext.ux.PopupWindow);
