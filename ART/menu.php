<?php

echo "[{
    text:'Main Menu',
    expanded: true,
    children:[{
        text:'Employees',
        id:'employees',
        leaf:true
    },{
        text:'Payments',
        id:'payments',
        leaf:true
    },{
        text:'Process payroll',
        id:'process',
        leaf:true
    },{
        text:'start-panel',
        id:'start-panel',
        leaf:true
    }]
},{
    text:'Admin',
    children:[{
        text:'Company',
        id:'company',
        leaf:true
    },{
        text:'Pay Types',
        id:'paytypes',
        leaf:true
    },{
        text:'Departments',
        id:'departments',
        leaf:true
    },{
        text:'Rates',
        id:'rates',
        leaf:true
    },{
        text:'Loans',
        id:'loans',
        leaf:true
    },{
        text:'Leaves',
        id:'leaves',
        leaf:true
    },{
        text:'Over Time',
        id:'overtime',
        leaf:true
    }]
},{
    text:'Reports',
    children:[{
        text:'Payslips',
        id:'payslips',
        leaf:true
    },{
        text:'P9 Report',
        id:'p9report',
        leaf:true
    },{
        text:'P9A Report',
        id:'p9Areport',
        leaf:true
    },{
        text:'NSSF Return',
        id:'nssfReturn',
        leaf:true
    },{
        text:'PAYE Return',
        id:'payeReturn',
        leaf:true
    }]
}]"
?>