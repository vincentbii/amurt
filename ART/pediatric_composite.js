/*!
 * Ext JS Library 3.3.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
var getioform=function() {
    Ext.QuickTips.init();

    var form = new Ext.Panel({
        renderTo: 'pedsbody',
        //        title   : 'Composite Fields',
        autoHeight: true,
        autoWidth  : true,
        bodyStyle: 'padding: 2px; background-color:#99BBE8',

        defaults: {
            anchor: '-20'
        },
        items:[
        {
            xtype: 'fieldset',
            title: 'OI Treatment and Other Medications',
            collapsible: true,
            margins:'0 5 0 0',
            bodyStyle: 'padding: 2px; background-color:#99BBE8',
            items: [
            {
                xtype: 'compositefield',
                hideLabel: true,
                combineErrors: false,
                items: [
                {
                    xtype: 'displayfield',
                    value: 'medication',
                    margins:'0 80 0 40'
                },
                {
                    xtype: 'displayfield',
                    value: 'Dose',
                    margins:'0 30 0 60'
                },
                {
                    xtype: 'displayfield',
                    value: 'Frequency',
                    margins:'0 10 0 0'
                },

                {
                    xtype: 'displayfield',
                    value: 'Duration',
                    margins:'0 10 0 0'
                },

                {
                    xtype: 'displayfield',
                    value: 'Prescribed',
                    margins:'0 25 0 0'
                },
                {
                    xtype: 'displayfield',
                    value: 'Dispensed'
                },
                ]
            },
            {
                xtype: 'compositefield',
                  fieldLabel: '2',
                   hideLabel: true,
                combineErrors: false,
                items: [
                {
                    xtype: 'checkbox',
                    name:'oi1',
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'dose-1',
                    width: 200,
                    allowBlank: false,
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'freq-2',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'dura-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'presc-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false
                }
                ]
            },
            {
                xtype: 'compositefield',
                  fieldLabel: '3',
                   hideLabel: true,
                combineErrors: false,
                items: [
                 {
                    xtype: 'checkbox',
                    name:'oi1',
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'dose-1',
                    width: 200,
                    allowBlank: false,
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'freq-2',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'dura-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'presc-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false
                }
                ]
            },
            {
                xtype: 'compositefield',
                  fieldLabel: '3',
                   hideLabel: true,
                combineErrors: false,
                items: [
                 {
                    xtype: 'checkbox',
                    name:'oi1',
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'dose-1',
                    width: 200,
                    allowBlank: false,
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'freq-2',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'dura-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'presc-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false
                }
                ]
            },
            {
                xtype: 'compositefield',
                  fieldLabel: '3',
                   hideLabel: true,
                combineErrors: false,
                items: [
                 {
                    xtype: 'checkbox',
                    name:'oi1',
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'dose-1',
                    width: 200,
                    allowBlank: false,
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'freq-2',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'dura-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'presc-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false
                }
                ]
            },
            {
                xtype: 'compositefield',
                  fieldLabel: '3',
                   hideLabel: true,
                combineErrors: false,
                items: [
                 {
                    xtype: 'checkbox',
                    name:'oi1',
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'dose-1',
                    width: 200,
                    allowBlank: false,
                    margins: '0 5 0 0'

                },

                {
                    xtype: 'textfield',
                    name: 'freq-2',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'dura-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'presc-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false,
                    margins: '0 10 0 0'
                },

                {
                    xtype: 'textfield',
                    name: 'disp-3',
                    width: 60,
                    allowBlank: false
                }
                ]
            }
            ]
        }
        ],
        buttons: [
        {
            text   : 'Load test data',
            handler: function() {
                var Record = Ext.data.Record.create([
                {
                    name: 'email',
                    type: 'string'
                },

                {
                    name: 'title',
                    type: 'string'
                },

                {
                    name: 'firstName',
                    type: 'string'
                },

                {
                    name: 'lastName',
                    type: 'string'
                },

                {
                    name: 'phone-1',
                    type: 'string'
                },

                {
                    name: 'phone-2',
                    type: 'string'
                },

                {
                    name: 'phone-3',
                    type: 'string'
                },

                {
                    name: 'hours',
                    type: 'number'
                },

                {
                    name: 'minutes',
                    type: 'number'
                },

                {
                    name: 'startDate',
                    type: 'date'
                },

                {
                    name: 'endDate',
                    type: 'date'
                }
                ]);

                form.form.loadRecord(new Record({
                    'email'    : 'ed@extjs.com',
                    'title'    : 'mr',
                    'firstName': 'Abraham',
                    'lastName' : 'Elias',
                    'startDate': '01/10/2003',
                    'endDate'  : '12/11/2009',
                    'phone-1'  : '555',
                    'phone-2'  : '123',
                    'phone-3'  : '4567',
                    'hours'    : 7,
                    'minutes'  : 15
                }));
            }
        },
        {
            text   : 'Save',
            handler: function() {
                if (form.form.isValid()) {
                    var s = '';

                    Ext.iterate(form.form.getValues(), function(key, value) {
                        s += String.format("{0} = {1}<br />", key, value);
                    }, this);

                    Ext.example.msg('Form Values', s);
                }
            }
        },

        {
            text   : 'Reset',
            handler: function() {
                form.form.reset();
            }
        }
        ]
    });

    var ioform = new Ext.Panel({
        title:'MyPanel',
        autoWidth: true,
        autoHeight: true,
        items:[form]
    })
};



//var displayIo=function(){
//    form.renderTo('docbody');
//}