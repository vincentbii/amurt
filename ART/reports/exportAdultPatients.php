<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
header("Pragma: public");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: pre-check=0, post-check=0, max-age=0");
header("Pragma: no-cache");
header("Expires: 0");
header("Content-Transfer-Encoding: none");
header("Content-Type: application/vnd.ms-excel;");
header("Content-type: application/x-msexcel");
header("Content-Disposition: attachment; filename=Invoices".date('Y-m-d h:m:s').".xls");
require('roots.php');
require($root_path.'include/inc_environment_global.php');
?>
<html>
<body>
<table border="0">
<tr>
    <th>Patient Name</th>
    <th>Drug Name</th>
    <th>Amount Prescribed</th>
    <th>Amount Dispensed</th>
    <th>Cost Per Unit</th>
    <th>Total Charges</th>
</tr>
<?php
$sql = 'SELECT enrollment_no, qty_dispensed, qty_prescribed, drug, unit_price  FROM art_adult_pharmacyrpt where drug_type not like "arv medication%" ';
$result=$db->Execute($sql);


    while($row = $result->FetchRow())
{
?>
<tr>
    <td><?php echo $row[0] ?></td>
    <td><?php echo $row[3] ?></td>
    <td><?php echo $row[2] ?></td>
    <td><?php echo $row[1] ?></td>
    <td><?php echo $row[4] ?></td>
    <td><?php echo $row[1] * $row[4] ?></td>
</tr>
<?php
}
?>
</table>
</body>
</html>