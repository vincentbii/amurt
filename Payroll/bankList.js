/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var fm=Ext.form;

var bankStore = new Ext.data.JsonStore({
    proxy: new Ext.data.HttpProxy({
        //where to retrieve data
        url: 'getBanklist.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
//        task:dt.format(Date.patterns.monthName)
    },
    //    url: 'getPayments.php',p.`Pid`, p.`emp_names`,p.`pay_type`, p.`amount`,e.bank_name,e.account_no
    root: 'Accounts',
    fields: ['pid', 'emp_names','pay_type','amount','bank_name','account_no','payMonth']
});


var filters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'PID'
    }, {
        type: 'string',
        dataIndex: 'emp_names',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'pay_type'
    }, {
        type: 'string',
        dataIndex: 'amount'
    }, {
        type: 'string',
        dataIndex: 'bank_name'
    }, {
        type: 'string',
        dataIndex: 'account_no'
    }, {
        type: 'string',
        dataIndex: 'payMonth'
    }]
});


var bankColModel=function(finish,start){
    var columns = [
    {
        id:'pid',
        header: "PID",
        width: 55,
        sortable: true,
        dataIndex: 'pid',
        filterable: true
    },
    {
        header: "Names",
        width: 200,
        sortable: true,
        dataIndex: 'emp_names',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Pay Type",
        width: 80,
        sortable: true,
        dataIndex: 'pay_type',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Amount",
        width: 80,
        sortable: true,
        dataIndex: 'amount'
    },
    {
        header: "bank_name",
        width: 200,
        sortable: true,
        dataIndex: 'bank_name',
        filter: {
            type: 'string'
        }
    },
    {
        header: "account_no",
        width: 80,
        sortable: true,
        dataIndex: 'account_no'
    },
    {
        header: "payMonth",
        width: 80,
        sortable: true,
        dataIndex: 'payMonth'
    },
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}




var bankPanel=new Ext.Panel({
    title:'Employee Accounts',
    id:'bankAccounts',
    height:600,
    width:500,
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            xtype: 'grid',
            id:'grid',
            store: bankStore,
            colModel: bankColModel(7),
            tbar: [{
                text: 'Update',
                iconCls:'remove'
            },{
                text: 'Export to Excel',
                iconCls:'Save',
                handler: function () {
                    document.location.href='reports/exportBanklist.php';
                }
            }, '->', // next fields will be aligned to the right
            {
                text: 'Refresh',
                tooltip: 'Click to Refresh the table',
                handler: refreshGrid,
                iconCls:'refresh'
            },{
                text: 'Clear Filter Data',
                handler: function () {
                    grid.filters.clearFilters();
                }
            },{
                text: 'Reconfigure Grid',
                handler: function () {
                    grid.reconfigure(empreader, createColModel(6));
                }
            }],
            height: 600,
            title:'Employees Data',
            border: true,
            listeners: {
                render: {
                    fn: function(g){
                        empreader.load({
                            params: {
                                start: 0,
                                limit: 10
                            }
                        });
                        g.getSelectionModel().selectRow(0);
                            delay: 10
                    }

                }
            // Allow rows to be rendered.
            },

            plugins: [filters],
            bbar: new Ext.PagingToolbar({
                store: bankStore,
                pageSize: 10,
                plugins: [filters]

            })

        }
    }]
});





bankStore.load();
