/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var banksForm=new Ext.form.FormPanel({
    id: 'banksForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:540,
    waitMsgTarget: true,
    items: [
    {
        xtype: 'textfield',
        id: 'BranchID',
        name:'BranchID',
        fieldLabel: 'BranchID',
        allowBlank: false,
        msgTarget:'side'
    },{
        xtype: 'textfield',
        id: 'bankCode',
        name:'bankCode',
        fieldLabel: 'Bank Code',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'bankName',
        name:'bankName',
        fieldLabel: 'Bank Name',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'branch',
        name:'branch',
        fieldLabel: 'Branch',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
});

var banksStore = new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getAllBanks',
    id: 'ID',//
    baseParams:{
        task: "getAllBanks"
    },
    fields: ['BranchID','bankCode', 'bankName','branch']
});

banksStore.load();

var banksGrid = new Ext.grid.EditorGridPanel({
    // title: 'List of Departments',
    store: banksStore,
    frame:true,
    columns: [
    {
        header: "BranchID",
        width: 80,
        dataIndex: 'BranchID',
        sortable: true,
        hidden:false
    },{
        header: "Bank Code",
        width: 80,
        dataIndex: 'bankCode',
        sortable: true,
        hidden:false
    },
    {
        header: "Bank Name",
        width: 150,
        dataIndex: 'bankName',
        sortable: true
    },
    {
        header: "Branch",
        width: 300,
        dataIndex: 'branch',
        sortable: true
    }
    ],

    width:540,
    height:400,
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("banksForm").getForm().loadRecord(rec);
            }
        }
    })
 
});


var buildUI = banksGrid.addButton({
   text: 'Delete',
    disabled:false,
    handler: function(){
        banksForm.form.submit({
            url:'comboFields.php?task=deleteBank', //php

            waitMsg:'Deleting Data...',

            success: function (form, action) {
                Ext.MessageBox.alert('Message', 'Banks Delete Successfully');
                refreshGrid();
            },
            failure:function(form, action) {
                Ext.MessageBox.alert('Message', 'Delete unsuccessful, Check Data');
            }
        });
    }
});



var submit = banksForm.addButton({
    text: 'Save',
    disabled:false,
    handler: function(){
        banksForm.form.submit({
            url:'comboFields.php?task=updateBanks', //php

            waitMsg:'Saving Data...',

            success: function (form, action) {
                Ext.MessageBox.alert('Message', 'Banks Updated Successfully');
                refreshGrid();
            },
            failure:function(form, action) {
                Ext.MessageBox.alert('Message', 'Update unsuccessful, Check Data');
            }
        });
    }
            
});

var Refresh = banksForm.addButton({
    text: 'Refresh',
    disabled:false,
    handler: function(){
    
        refreshGrid();
    }
});

function refreshGrid() {
    banksStore.reload();//
} // end refresh

var cancel = banksForm.addButton({
    text: 'Cancel',
    disabled:false,
    handler: function(){
        banksForm.close();
    }
});


var myForm=new Ext.Panel({
    title:'Banks',
    id:'banks',
    width:600,
    minHeight:500,
    items:[banksForm,banksGrid]
});


