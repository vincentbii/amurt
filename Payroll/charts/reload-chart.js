/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

Ext.onReady(function(){
    var salStore = new Ext.data.JsonStore({
        url: 'getSalary.php',
        root: 'salary',
        fields: ['pid', 'basicpay']
    });

    
    new Ext.Panel({
        width: 700,
        height: 700,
        renderTo: document.body,
        title: 'Column Chart with Reload - Hits per Month',
        tbar: [{
            text: 'Load new data set'//,
        //            handler: function(){
        //                store.loadData(generateData());
        //            }
        }],
        items: {
            xtype: 'columnchart',
            store: salStore,
            yField: 'basicpay',
            url: '../resources/charts.swf',
            xField: 'pid',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Staff Salary'
            }),
            yAxis: new Ext.chart.NumericAxis({
                displayName:'basicpay',
                labelRenderer:Ext.util.Format.numberRenderer('0,0')
            }),
            extraStyle: {
                xAxis: {
                    labelRotation: -90
                }
            },
            series: [{
                type: 'column',
                displayName: 'basicpay',
                yField: 'basicpay',
                style: {
                    image:'../examples/charts/bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'pid',
                yField: 'pid',
                style: {
                    color: 0x15428B
                }
            }]
        }
    });
    salStore.load();
});
