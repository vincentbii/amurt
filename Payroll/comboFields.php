
<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : ($_REQUEST['task']);
$pid = ($_POST['pid']) ? ($_POST['pid']) : 'P001';
$bankID = ($_POST['bankID']) ? ($_POST['bankID']) : '1';

switch ($task) {
    case "getEmpType":
        getEmpType();
        break;
    case "getEmpDept":
        getEmpDept();
        break;
    case "getJobGroup":
        getJobGroup();
        break;
    case "getEmpTitle":
        getEmpTitle();
        break;
    case "getAllBanks":
        getAllBanks();
        break;
    case "updateBanks":
        updateBanks();
        break;
    case "deleteBank":
          deleteBank();
        break;
    case "getBanks":
        getBanks();
        break;
    case "getBranch":
        getBranch($bankID);
        break;
    case "getPaye":
        getPaye($pid);
        break;
    case "getEmpBranch":
        getEmpBranch();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function getPaye($pid) {
    global $db;
    $sql = 'SELECT `ID`, `EmpType`, `Duration` FROM `proll_emptypes`';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getEmpTypes":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","EmpType":"' . $row[1] . '","Duration":"' . $row[2] . '"}';
        if ($counter < $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmpType($pid) {
    global $db;
    $sql = 'SELECT `ID`, `EmpType`, `Duration` FROM `proll_emptypes`';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getEmpTypes":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","EmpType":"' . $row[1] . '","Duration":"' . $row[2] . '"}';
        if ($counter < $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmpDept() {
    global $db;
    $sql = 'SELECT ID,`Name` FROM proll_departments';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getEmpDept":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","DeptName":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmpTitle() {
    global $db;
    $sql = 'SELECT p.`ID`, p.`JobTitle` FROM proll_jobtitles p';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getEmpTitle":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","JobTitle":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getJobGroup() {
    global $db;
    $sql = 'SELECT p.`code`, p.`jname` FROM proll_jobgroups p';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getJobGroup":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"jcode":"' . $row[0] . '","jname":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getBanks() {
    global $db;
    $sql = 'SELECT p.bankcode,p.`BankName` FROM proll_banks p';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getBanks":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"bankID":"' . $row[0] . '","BankName":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getAllBanks() {
    global $db;
    $sql = 'SELECT b.BranchID,b.bankID,b.BankBranch,c.BankName FROM proll_bankbranches b LEFT JOIN proll_banks c 
ON b.BankID=c.BankCode';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getAllBanks":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        $bankCode = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]);
        $bankName = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[3]);
        $branch = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]);

        echo '{"BranchID":"' . $row[0] . '","bankCode":"' . $bankCode . '","bankName":"' . $bankName . '","branch":"' . $branch . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function deleteBank() {
    global $db;
    $sql = 'DELETE FROM proll_bankbranches where BranchID="' . $_POST['BranchID'] . '"';
    if($result = $db->Execute($sql)){
       echo "{success: true}";
    }else{
        echo $sql;
        echo "{success: false}"; 
    }
    
    
}

function updateBanks() {
    global $db;
    if (trim($_POST['bankCode'] == "")) {
        $Err = 'bankCode:"Your must bankCode",';
    }

    if (trim($_POST['bankName'] == "")) {
        $Err .= 'bankName:"You must supply a bankName"';
    }

    if (trim($_POST['branch'] == "")) {
        $Err .= 'branch Type:"You must supply a branch"';
    }


    if ($Err == "") {


        $sql1 = "select bankcode from proll_banks where bankcode='" . trim($_POST['bankCode']) . "'";
        $result1 = $db->Execute($sql1);
//        echo $sql1;
        $numRows = $result1->RecordCount();
        if ($numRows < 1) {
            $sqli = 'insert into proll_banks(bankCode,BankName) value("' . $_POST['bankCode'] . '","' . $_POST['bankName'] . '")';
            $resulti = $db->Execute($sqli);
        } elseif ($numRows > 0) {


            $sql1 = "select BranchID from proll_bankbranches where BranchID='" . trim($_POST['BranchID']) . "'";
            $result1 = $db->Execute($sql1);
//                 echo $sql;
            $numRows2 = $result1->RecordCount();
//            echo $numRows2;
            if ($numRows2==0) {
                $sql3 = 'insert into proll_bankbranches(BranchID,bankID,BankBranch) 
                    values("' . $_POST['BranchID'] . '","' . $_POST['bankCode'] . '","' . $_POST['branch'].'")';
                $resultl = $db->Execute($sql3);
                $results = "{success: true}";
                     echo $sql3;
            } else {
                $sql2 = 'update proll_bankbranches set BankBranch="' . $_POST['branch'] . '" 
                where bankID="' . $_POST['bankCode'] . '" and BranchID="' . $_POST['BranchID'] . '"';
                echo $sql2;
                if($result = $db->Execute($sql2)){
                       $results = "{success: true}";
                }else{
                    $results = "{success: false}";
                }
            }
        } else {
            
        }
    } else {
        // Errors. Set the json to return the fieldnames and the associated error messages
        $results = '{success: false, errors:{' . $Err . '} }'; // Return the error message(s)
    }

    echo $results;
}

function getBranch($bankid) {
    global $db;
    $sql1 = 'select bankcode from proll_banks where bankcode="' . $bankid . '"';
    $result = $db->Execute($sql1);
    $Row = $result->FetchRow();
    $bid = $Row[0];

    $sql = 'SELECT p.`BranchID`, p.`BankID`, p.`BankBranch`, p.`BankBranchCode` FROM proll_bankbranches p where p.`bankid`="' . $bid . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
//    echo $sql;
    echo '{
    "getBranch":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"BranchID":"' . $row[0] . '","BankBranch":"' . $row[2] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmpBranch() {
    global $db;
  
    $sql = 'SELECT distinct Branch FROM `proll_empregister`';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
//    echo $sql;
    echo '{
    "getEmpBranch":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"empBranch":"' . $row[0] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}
?>

