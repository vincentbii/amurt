/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var costStore = new Ext.data.JsonStore({
        url: 'getCostCenter.php',
        root: 'costC',
        fields: ['department', 'amount']
    });
    
  costStore.load();

   var costChart= new Ext.Panel({
        height: 300,
        renderTo: document.body,
        minHeight:100,
        maxHeight:300,

        items: {
            xtype: 'columnchart',
            store: costStore,
            yField: 'amount',
            url: '../include/Extjs/resources/charts.swf',
            xField: 'department',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Department Salaries'
            }),
            yAxis: new Ext.chart.NumericAxis({
                displayName:'Cost',
                labelRenderer:Ext.util.Format.numberRenderer('0,0')
            }),
            extraStyle: {
                xAxis: {
                    labelRotation: -90
                }
            },
            series: [{
                type: 'column',
                displayName: 'Cost',
                yField: 'amount',
                style: {
                    image:'../include/Extjs/chart/bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'department',
                yField: 'department',
                style: {
                    color: 0x15428B
                }
            }]
        }
    });
  