/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var deptStore = new Ext.data.JsonStore({
    url: 'getDept.php',
    root: 'Departments',
     baseParams:{
        task: "departments"
    },
    fields: ['deptID', 'deptName','gl_acc']
});

//Get the combo with the bank Code
var glStore=new Ext.data.JsonStore({
    url: 'getDept.php',
    root: 'glAccounts',
    id: 'ID',//
    baseParams:{
        task: "glAccounts"
    },//This
    fields:['accountcode','accountname']
});
glStore.load()

var glCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'gl_acc',
    name:"gl_acc",
    triggerAction: 'all',
    lazyRender:true,
    width:200,
    mode: 'local2',
    fieldLabel:'GL Accounts',
    store: glStore,
    valueField: 'accountcode',
    displayField: 'accountname'
});


var grid = new Ext.grid.EditorGridPanel({
    // title: 'List of Departments',
    store: deptStore,
    frame:true,
    columns: [
    {
        header: "ID",
        width: 80,
        dataIndex: 'deptID',
        sortable: true,
        hidden:false
    },
    {
        id: 'deptName',
        header: "Departments",
        width: 300,
        dataIndex: 'deptName',
        sortable: true,
        autoExpandColumn: 'title-col'
    },
    {
        id: 'gl_acc',
        header: "GL Account",
        width: 300,
        dataIndex: 'gl_acc',
        sortable: true,
        autoExpandColumn: 'title-col'
    }
    ],

    width:540,
    height:300,
    sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        Ext.getCmp("deptForm").getForm().loadRecord(rec);
                    }
                }
            })

 
});
deptStore.load();

var buildUI = grid.addButton({
    text: 'Delete',
    iconCls: 'icon-delete',
    id:'delete',
    handler : function() {
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?', function(btn, text){
            if (text == 'Yes'){
                var index = grid.getSelectionModel().getSelectedCell();
                if (!index) {
                    return false;
                }
                var rec = grid.store.getAt(index[0]);
                grid.store.remove(rec);
                    url:'updateDepts.php'
            }else{
                return false;
            }
        });
    }
});



var deptForm=new Ext.form.FormPanel({
    id: 'deptForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:540,
    waitMsgTarget: true,
    items: [
    {
        xtype: 'textfield',
        id: 'deptID',
        name:'deptID',
        fieldLabel: 'Department ID',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'deptName',
        name:'deptName',
        fieldLabel: 'Departments',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },glCombo
    ]
});

var submit = deptForm.addButton({
    text: 'Save',
    disabled:false,
    handler: function(){
        deptForm.form.submit({
            url:'saveDept.php', // php

            waitMsg:'Saving Data...'
               
        });
        refreshDepts();
//        store.reload();
    }
            
});

var Refresh = deptForm.addButton({
    text: 'Refresh',
    disabled:false,
    handler: function(){
    
        refreshDepts();

    }

});

function refreshDepts() {
    deptStore.reload();//
} // end refresh

var cancel = deptForm.addButton({
    text: 'Cancel',
    disabled:false,
    handler: function(){
        myForm.close();
    }
});

var companyForm=new Ext.form.FormPanel({
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:540,
    waitMsgTarget: true,
    items: [
    {
        xtype: 'textfield',
        id: 'company_name',
        name:'company_name',
        fieldLabel: 'Company Name',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'cophone',
        name:'cophone',
        fieldLabel: 'Phone',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'coPhysical',
        name:'coPhysical',
        fieldLabel: 'Physical Address',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'cobox',
        name:'cobox',
        fieldLabel: 'Box',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'copostal',
        name:'copostal',
        fieldLabel: 'Postal Code',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'cotown',
        name:'cotown',
        fieldLabel: 'Town',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'cocountry',
        name:'cocountry',
        fieldLabel: 'Country',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'coemail',
        name:'coemail',
        fieldLabel: 'Email',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'cowebsite',
        name:'cowebsite',
        fieldLabel: 'Website',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
});

var coNumbers=new Ext.form.FormPanel({
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:540,
    height:500,
    waitMsgTarget: true,
    items: [
    {
        xtype: 'textfield',
        id: 'Pin_no',
        name:'Pin_no',
        fieldLabel: 'Pin_no Name',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'VAT',
        name:'VAT',
        fieldLabel: 'VAT No',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Inc No',
        name:'Inc No',
        fieldLabel: 'Incorporation No',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Bank',
        name:'Bank',
        fieldLabel: 'Bank Account Number',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
});



var myForm=new Ext.Panel({
    title:'Departments',
    id:'departments',
    width:600,
    minHeight:500,
    items:[deptForm,grid]
});


var company=new Ext.Panel({
    title:'Company details',
    id:'company',
    minWidth:300,
    minHeight:400,
    items:{
        xtype:'tabpanel',
        activeTab:0,
        items: [{
            title: 'Addresses',
            items:[companyForm]
        },{
            title: 'Numbers',
            items:[coNumbers]
        }]
    }
    
});

var start = new Ext.Panel({
    id: 'start-panel',
    title: 'Home',
    layout: 'fit',
    bodyStyle: 'padding:25px',
    contentEl: 'start-div'  // pull existing content from the page
});

var paytypes=new Ext.Panel({
    title:'Payment Types',
    id:'paytypes',
    minWidth:400,
    minHeight:400,
    items:[ptypegrid]
});


