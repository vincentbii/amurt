<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css">
        <link rel="stylesheet" type="text/css" href="prollcss.css">
        <link rel="stylesheet" type="text/css" href="xml-tree-loader.css" />
        <script type="text/javascript" src="../include/Extjs/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="../include/Extjs/ext-all-debug.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/XmlTreeLoader.js"></script>
        <script type="text/javascript" src="register.js"></script>

        <link rel="stylesheet" type="text/css" href="../include/Extjs/shared/examples.css" />
        <script type="text/javascript" src="../include/Extjs/shared/examples.js"></script>
        <script type="text/javascript" >

            Ext.BLANK_IMAGE_URL="../include/Extjs/resources/images/default/s.gif";
            Ext.onReady(function(){

                var detailsText = '<i>Select a book to see more information...</i>';

                var tpl = new Ext.Template(
                '<h2 class="title">{title}</h2>',
                '<p><b>Published</b>: {published}</p>',
                '<p><b>Synopsis</b>: {innerText}</p>',
                '<p><a href="{url}" target="_blank">Purchase from Amazon</a></p>'
            );
                tpl.compile();

                Ext.app.BookLoader = Ext.extend(Ext.ux.tree.XmlTreeLoader, {
                    processAttributes : function(attr){
                        if(attr.first){ // is it an author node?

                            // Set the node text that will show in the tree since our raw data does not include a text attribute:
                            attr.text = attr.first;

                            // Author icon, using the gender flag to choose a specific icon:
                            attr.iconCls = 'mainmenu';

                            // Override these values for our folder nodes because we are loading all data at once.  If we were
                            // loading each node asynchronously (the default) we would not want to do this:
                            attr.loaded = true;
                            attr.expanded = true;
                        }
                        else if(attr.title){ // is it a book node?

                            // Set the node text that will show in the tree since our raw data does not include a text attribute:
                            attr.text = attr.title;

                            // Book icon:
                            attr.iconCls = 'admin';

                            // Tell the tree this is a leaf node.  This could also be passed as an attribute in the original XML,
                            // but this example demonstrates that you can control this even when you cannot dictate the format of
                            // the incoming source XML:
                            attr.leaf = true;
                        }
                        else if(attr.slip){ // is it a book node?

                            // Set the node text that will show in the tree since our raw data does not include a text attribute:
                            attr.text = attr.slip;

                            // Book icon:
                            attr.iconCls = 'reports';

                            // Tell the tree this is a leaf node.  This could also be passed as an attribute in the original XML,
                            // but this example demonstrates that you can control this even when you cannot dictate the format of
                            // the incoming source XML:
                            attr.leaf = true;

                        }}
                });



                var border = new Ext.Panel({
                    layout:'border',
                    items: [{
                            title: '',
                            region: 'south',
                            height: 50,
                            minSize: 75,
                            maxSize: 250,
                            margins: '0 5 5 5'
                        },{
                            title: 'Menus',
                            region:'west',
                            margins: '5 0 0 5',
                            cmargins: '5 5 0 5',
                            width: 200,
                            minSize: 100,
                            maxSize: 300,
                            items: [{
                                    xtype: 'treepanel',
                                    id: 'menus',
                                    width: 200,
                                    height:400,
                                    iconCls: 'report',
                                    useArrows: true,
                                    autoScroll: true,
                                    animate: true,
                                    enableDD: true,
                                    collapsed:false,
                                    containerScroll: true,
                                    border: false,
                                    root: new Ext.tree.AsyncTreeNode(),
                                    // Our custom TreeLoader:
                                    loader: new Ext.app.BookLoader({
                                        dataUrl:'menus.xml'
                                    })
                                }]
                        }
                        ,{
                            title: 'Care2x Payroll',
                            region:'center',
                            margins: '5 5 0 0',
                            width:600,
                            minSize: 400,
                            maxSize:600,
                            items:[contactForm]
                        }]
                });



                var viewPort=new Ext.Viewport({
                    //position items within this container using CSS-Style style absolute positioning
                    layout:'fit',
                    items:[border]
                });

                viewPort.render(document.body);
            });
        </script>
    </head>
    <body>
        <div id="menus"></div>
    </body>
</html>
