/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



Ext.QuickTips.init();

// turn on validation errors beside the field globally
Ext.form.Field.prototype.msgTarget = 'side';

var fm2 = Ext.form;
var fm =Ext.form;
var primaryKey='ID'; //primary key is used several timpostes throughout
var url = {
    local2:  'getEmpPayments.php',  // static data file
    remote2: 'getEmpPayments.php'
};
var encode2 = false;

var local2 = true;

var empayStore=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'empPayments',

    id: 'ID',//
    baseParams:{
        task: "readPayments"
    },//This
    fields:['ID','PID', 'Pay_Type','Pay_Name','Amount','Balance' ]
});
/*    Here is where we create the Form
 */

function refreshGrid() {
    empayStore.reload();//
} // end refresh
function refreshGrid2() {
    empayStore.reload();//
}

/************************************************************
 * Create a new dialog - reuse by create and edit partpay 
 ************************************************************/
var payTypeStore=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'getPayTypes',
    id: 'ID',//
    baseParams:{
        task: "getPayTypes"
    },//This
    fields:['payCode','payType']
});

payTypeStore.load();

var payCodeStore=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'postPaybyCode',
    id: 'ID',//
    baseParams:{
        task: "postPaybyCode"
    },//This
    fields:['ID','PID','empNames','Amount','Balance']
});

var typeStore=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'getTypes',

    id: 'ID',//
    baseParams:{
        task: "getType"
    },//This
    fields:['ID','CatName']
});
typeStore.load();

var ptypeStore2=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'getTypes2',
    id: 'ID',//
    baseParams:{
        task: "getType2"
    },//This
    fields:['ID','paytype']
});
ptypeStore2.load();

var amountStore=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'getAmount',

    id: 'ID',//
    baseParams:{
        task: "getAmount"
    },//This
    fields:['amount']
});
amountStore.load()

function calcAmount(param){
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'getEmpPayments.php',
        root:'getAmount',
        params: {
            task: "getAmount",
            pid:param
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myTax = responseData.amount;
            addEmpPayments.getForm().findField("Amount").setValue(myTax);
        }
    }
    );

}

function calcNHIF(param){
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'getEmpPayments.php',
        root:'getNHIF',
        params: {
            task: "getNHIF",
            pid:param
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myTax = responseData.amount;
            addEmpPayments.getForm().findField("Amount").setValue(myTax);
        }
    }
    );

}

function calcNSSF(param){
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'getEmpPayments.php',
        root:'getNSSF',
        params: {
            task: "getNSSF",
            pid:param
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myTax = responseData.amount;
            addEmpPayments.getForm().findField("Amount").setValue(myTax);
        }
    }
    );

}

function calcGratuity(param){
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'getEmpPayments.php',
        root:'getGratuity',
        params: {
            task: "getGratuity",
            pid:param
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myTax = responseData.amount;
            addEmpPayments.getForm().findField("Amount").setValue(myTax);
        }
    }
    );

}

function calcOverTime(param,param2,overTime){
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'getEmpPayments.php',
        root:'getOvertime',
        params: {
            task: "getOvertime",
            pid:param,
            pType:param2,
            overTime:overTime
            
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myAmount = responseData.amount;
            addEmpPayments.getForm().findField("Amount").setValue(myAmount);
             addEmpPayments.getForm().findField("Balance").setValue(0);
            refreshGrid();
        }
    }
    );

}

var typeCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'catID',
    name:"catID",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Payment Type',
    store: typeStore,
    valueField: 'ID',
    displayField: 'CatName',
    listeners: {
        select: function(f,r,i){
//            Ext.MessageBox.alert('Message', 'You selected '+addEmpPayments.getForm().findField('catID').value);
            ptypeStore2.load({
                params:{
                    catID: i+1
                }
            });
        }
    }
});

var ptypeCombo2 = new Ext.form.ComboBox({
    store: ptypeStore2,
    typeAhead: true,
    id:'payID',
    name:"payID",
    mode: 'local2',
    forceSelection: true,
    triggerAction: 'all',
    lazyRender:true,
    selectOnFocus:true,
    fieldLabel:'Payment',
    valueField: 'ID',
    displayField: 'paytype',
    listeners: {
        select: function(f,r,i){
          
          var pid=addEmpPayments.getForm().findField('pid4').value;
          var comb1=addEmpPayments.getForm().findField('catID').value;
          var comb2=addEmpPayments.getForm().findField('payID').value;
//          var overTime=addEmpPayments.getForm().findField('overTime').value;
           var overTime=document.getElementById('overTime').value;
           
//           Ext.MessageBox.alert('Message', 'You selected '+overTime);
           
            if(addEmpPayments.getForm().findField('payID').value==15){
               //calcAmount(addEmpPayments.getForm().findField('pid').value);
                Ext.MessageBox.alert('You are not allowed to add this HERE, its calculated automatically');
            }else if(comb1==3 && comb2=='019'){
                calcNHIF(addEmpPayments.getForm().findField('pid4').value);
            }else if(comb1==3 && comb2=='016'){
                calcNSSF(addEmpPayments.getForm().findField('pid4').value);
//            }else if(addEmpPayments.getForm().findField('payID').value==8){
//                calcGratuity(addEmpPayments.getForm().findField('pid4').value);
            }else if(comb1==2){
                if(comb2=='003'||comb2=='004'){
                    calcOverTime(pid,comb2,overTime);
                }else {
                    
                }
            }
        }
    }


});





//var getPaye=function (i){
//    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
//    {   //Specify options (note success/failure below that
//        waitMsg: 'Saving changes...',
//        root:'getPaye',
//        url: 'comboFields.php',
//        params: {
//            task: "paye", //pass task to do to the server script
//            keyID: i
//        },//end params
//        failure:function(response,options){
//            Ext.MessageBox.alert('Warning','Oops...');
//        },//end failure block
//        success:function(response,options){
//
//                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
//                var newID = responseData.newID;
//                document.getElementById('Amount').value='1400';
//
//        }//end success block
//    }//end request config
//    ); //end request
//}

var pcodeDetails = new Ext.grid.EditorGridPanel({
    title: 'Payment Codes',
    id:'pcodeDetails',
    store: payTypeStore,
    frame:true,
    minWidth:400,
    height:470,
    collapsible:true,
    clicksToEdit:2,
    columns: [
    {
        header: "Pay Code",
        width: 40,
        dataIndex: 'payCode',
        sortable: true,
        hidden:false
    },
    {
        header: "Pay Type",
        width: 200,
        dataIndex: 'payType',
        sortable: true
    }
    ],
    stripeRows: true,
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("postByCodeForm").getForm().loadRecord(rec);
                var gridrecord1 = pcodeDetails.getSelectionModel().getSelected();
                if (gridrecord1 == undefined){
                    alert('no good');
                }else{
                    //sends details to family information form
                    strpayType=gridrecord1.get('payCode');
                    payCodeStore.load({
                        params:{
                            payType:strpayType
                        }
                    });
                }
            }
        }
    })
});

var payByCodeDetails = new Ext.grid.EditorGridPanel({
    title: 'Post By Code',
    id:'payByCodeDetails',
    store: payCodeStore,
    frame:true,
    minWidth:400,
    height:400,
    collapsible:false,
    columns: [
        {
        header: "ID",
        width: 50,
        dataIndex: 'ID',
        sortable: true,
        hidden:false,
        editor: new fm2.TextField({
            allowBlank: false
        })
    },
    {
        header: "PID",
        width: 60,
        dataIndex: 'PID',
        sortable: true,
        hidden:false,
        editor: new fm2.TextField({
            allowBlank: false
        })
    },
    {
        header: "Names",
        width: 170,
        dataIndex: 'empNames',
        sortable: true,
        hidden:false,
        editor: new fm2.TextField({
            allowBlank: false
        })
    },
    {

        header: "Amount",
        width: 80,
        dataIndex: 'Amount',
        sortable: true,
        editor: new fm2.TextField({
            allowBlank: false
        })
    },
    {
        header: "Bal/Total",
        width: 80,
        dataIndex: 'Balance',
        sortable: true,
        editor: new fm2.TextField({
            allowBlank: false
        })
    }
    ],
    stripeRows: true,
    tbar: [{
        text: 'Add New',
        handler : addNewRow
    }],
    buttons: [{
        text: 'Save',
        listeners:{
            afteredit:{
                fn:handlePcodeEdit,
                buffer:200
            }
        },
    handler:refreshPCodeGrid
    },
    {
        text: 'Close',
        handler: function() {
            postByCode.hide();
        //            refreshGrid2();
        }
    }]
});
payCodeStore.load();
payByCodeDetails.addListener('afteredit', handlePcodeEdit);
payByCodeDetails.on('specialkey', function(field, e) {
    if (e.getKey() == e.TAB) {
        addNewRow();
        var nextRow = payByCodeDetails.lastEdit.row + 1;
        if (nextRow < payByCodeDetails.view.getRows().length) {
            payByCodeDetails.stopEditing();
            payByCodeDetails.startEditing(nextRow, payByCodeDetails.lastEdit.col);
            payByCodeDetails.selModel.select(nextRow, payByCodeDetails.lastEdit.col);
        }
    }
});



  function addNewRow(){  
            // access the Record constructor through the grid's store
            var pbyCode=payByCodeDetails.getStore().recordType;
            var payCode=postByCodeForm.getForm().findField('payCode').getValue();
//            var payType=postByCodeForm.getForm().findField('payType').getValue()
            
            if(payCode!='' ){
                var p = new pbyCode({
                    ID:0,
                    PID:0,
                    Amount:0,
                    Balance:0
                });
                payByCodeDetails.stopEditing();
                payCodeStore.insert(0, p);
                payByCodeDetails.startEditing(0, 1);
            }else{
                Ext.MessageBox.alert('Message',"Please select a Payment Code");
            }
        }
        
function refreshPCodeGrid() {
    payCodeStore.reload();//
} // end refresh

var postByCodeForm = new Ext.FormPanel({
    id: 'postByCodeForm',
    frame: true,
    labelAlign: 'left',
    //    title: 'Employees List',
    bodyStyle:'padding:5px',
    url:'getEmpPayments.php',
    width: 700,
    height:600,
    record: null,
    layout: 'column',    // Specifies that the items will now be arranged in columns
    items: [{
        columnWidth: 0.65,
        xtype: 'fieldset',
        title:'Post by',
        autoHeight: true,
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: false,
        style: {
            "margin-left": "10px", // when you add custom margin in IE 6...
            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Pay Code',
            name: 'payCode',
            id: 'payCode',
            disabled:false
        },{
            xtype: 'textfield',
            fieldLabel: 'payType',
            name: 'payType',
            id: 'payType',
            disabled:false
        },{
            xtype: 'panel',
            activeTab:0,
            items: [{
                items:[payByCodeDetails]
            }]
        }]
    },{
        columnWidth: 0.35,
        layout: 'fit',
        items: {
            items:[pcodeDetails]
        }
    }]

});


var addEmpPayments = new Ext.FormPanel({
    id: 'addEmpPayments',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:350,
    url: 'getEmpPayments.php',
    items: [{
        xtype: 'textfield',
        id: 'pid4',
        fieldLabel: 'PID',
        allowBlank: false,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vpid = this.el.getValue();
            if(vpid === this.emptyText || vpid === undefined){
                vpid = '';
            }
            return vpid;
        }

    },{
        xtype: 'textfield',
        id: 'overTime',
        fieldLabel: 'Total Time',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },typeCombo,ptypeCombo2,{
        xtype: 'textfield',
        id: 'Amount',
        fieldLabel: 'Amount',
        allowBlank: false,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vamount = this.el.getValue();
            if(vamount === this.emptyText || vamount === undefined){
                vamount = '';
            }
            return vamount;
        }
    },{
        xtype: 'textfield',
        id: 'Balance',
        fieldLabel: 'Balance',
        allowBlank: false,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vbalance = this.el.getValue();
            if(vbalance === this.emptyText || vbalance === undefined){
                vbalance = '';
            }
            return vbalance;
        }
    }],
    buttons: [{
        text: 'Save',
        handler: function() {
            var pid = addEmpPayments.getForm().findField("pid4").getValue();
            var catId=addEmpPayments.getForm().findField('catID').getValue();
            var PayID=addEmpPayments.getForm().findField('payID').getValue();
            var amount=addEmpPayments.getForm().findField('Amount').getValue();
            var balance=addEmpPayments.getForm().findField('Balance').getValue();
            Ext.Ajax.request({
                url: 'getEmpPayments.php',
                method: 'POST',
                params: {
                    pid:pid,
                    catId:catId,
                    PayID:PayID,
                    amount:amount,
                    balance:balance,
                    task:'addEmpPays'
                },
                waitMsg:'Saving Data...',
                success: function (form, action) {
                    //                    Ext.MessageBox.alert('Message', 'Saved OK');
                    //                    refreshGrid2();
                    createPayments.hide();
                      
                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                }
            });

        }
    },
    {
        text: 'Calc NHIFs',
        handler: function() {
            var pid = addEmpPayments.getForm().findField("pid2").getValue();
            var catId=addEmpPayments.getForm().findField('catID').getValue();
            var PayID=addEmpPayments.getForm().findField('payID').getValue();
            var amount=addEmpPayments.getForm().findField('Amount').getValue();
            var balance=addEmpPayments.getForm().findField('Balance').getValue();
            Ext.Ajax.request({
                url: 'getEmpPayments.php',
                method: 'POST',
                params: {
                    pid:pid,
                    catId:catId,
                    PayID:PayID,
                    amount:amount,
                    balance:balance,
                    task:'calcNHIF'
                },
                waitMsg:'Saving Data...',
                success: function (form, action) {
                    //                    Ext.MessageBox.alert('Message', 'Saved OK');
                    //                    refreshGrid2();
                    createPayments.hide();

                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                }
            });

        }
    },
    {
        text: 'Calc NSSFs',
        handler: function() {
            var pid = addEmpPayments.getForm().findField("pid2").getValue();
            var catId=addEmpPayments.getForm().findField('catID').getValue();
            var PayID=addEmpPayments.getForm().findField('payID').getValue();
            var amount=addEmpPayments.getForm().findField('Amount').getValue();
            var balance=addEmpPayments.getForm().findField('Balance').getValue();
            Ext.Ajax.request({
                url: 'getEmpPayments.php',
                method: 'POST',
                params: {
                    pid:pid,
                    catId:catId,
                    PayID:PayID,
                    amount:amount,
                    balance:balance,
                    task:'calcNSSF'
                },
                waitMsg:'Saving Data...',
                success: function (form, action) {
                    //                    Ext.MessageBox.alert('Message', 'Saved OK');
                    refreshGrid2();
                    createPayments.hide();

                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                }
            });

        }
    },
    {
        text: 'Close',
        handler: function() {
            createPayments.hide();
            refreshGrid2();
        }
    }]
})



createPayments = new Ext.Window({
    title:'Add A payment',
    //                    applyTo:'payslipWin',
    layout:'fit',
    id: 'addEmpPays',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:400,
    height:250,
    closable:false,
    items: [addEmpPayments]

});

postByCode = new Ext.Window({
    title:'Post by Code',
    //                    applyTo:'payslipWin',
    layout:'fit',
    id: 'postByCode',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:1000,
    height:600,
    closable:false,
    items: [postByCodeForm]

});

var empayGrid = new Ext.grid.EditorGridPanel({
    title: 'Employee Payments and Deductions',
    store: empayStore,
    frame:true,
    minWidth:400,
    height:350,
    collapsible:true,
    columns: [
    {
        header: "ID",
        width: 50,
        dataIndex: 'ID',
        sortable: true,
        hidden:true
    },
    {
        header: "PID",
        width: 80,
        dataIndex: 'PID',
        sortable: true,
        hidden:false
    },
    {
        header: "Pay_Type",
        width: 150,
        dataIndex: 'Pay_Type',
        sortable: true//,
//        editor: new fm2.TextField({
//            allowBlank: false
//        })
    },

    {
        header: "Pay_Name",
        width: 200,
        dataIndex: 'Pay_Name',
        sortable: true//,
//        editor: new fm2.TextField({
//            allowBlank: false
//        })
    },
    {

        header: "Amount",
        width: 80,
        dataIndex: 'Amount',
        sortable: true,
        editor: new fm2.TextField({
            allowBlank: false
        })
    },
    {

        header: "Balance",
        width: 80,
        dataIndex: 'Balance',
        sortable: true,
        editor: new fm2.TextField({
            allowBlank: false
        })
    }
    ],
    clicksToEdit: 2,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    stripeRows: true,
    tbar: new Ext.Toolbar({
        items: [{
            text: 'Post By Pid',
            handler :function(){
                createPayments.show();
                addEmpPayments.getForm().reset();
                var gridrecord = detailsPanel.getSelectionModel().getSelected();
                strPid=gridrecord.get('PID');
                addEmpPayments.getForm().findField("pid4").setValue(strPid);
            }
        },'-',{
            text: 'Post By Code',
            handler :function(){
                postByCode.show();
            }
        },'-','-',{
            text: 'Delete Payments',
            iconCls:'remove',
            handler :handleEmpPayDelete
        }, '->', // next fields will be aligned to the right
        {
            text: 'Refresh',
            tooltip: 'Click to Refresh the table',
            handler: refreshGrid2,
            iconCls:'refresh'
        },{
        text: 'Add a pay Type',
        handler : function(){
            // access the Record constructor through the grid's store
            var petype =ptypegrid.getStore().recordType;
            var p = new petype({
                ID:0,
                catID: 1,
                type: 'Loan',
                Fixed: 0,
                notes: '',
                Contribution: false
            });
            empayGrid.stopEditing();
            empayStore.insert(0, p);
            empayGrid.startEditing(0, 1);
        }
    }]
    })

});

empayStore.load();
empayGrid.addListener('afteredit', handletEmPayEdit);



function handletEmPayEdit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;

    //start the process to update the db with cell contents
    updateEmpPayment(editEvent);

}

function updateEmpPayment(oGrid_Event) {

    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
    } else
{
        var fieldValue = oGrid_Event.value;
    }

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {  
        waitMsg: 'Saving changes...',
        url: 'getEmpPayments.php',
        params: {
            task: "saveempPayment2", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used
            keyID: oGrid_Event.record.data.ID,
            PID: oGrid_Event.record.data.PID,
            payType: oGrid_Event.record.data.Pay_Type,
            payName: oGrid_Event.record.data.Pay_Name,
            field: oGrid_Event.field,//the column name
            value: fieldValue,//the updated value
            originalValue: oGrid_Event.record.modified
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            if(oGrid_Event.record.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
                var newID = responseData.newID;
                oGrid_Event.record.set('newRecord','no');
                oGrid_Event.record.set('ID',newID);
                empayStore.commitChanges();
            } else {
                empayStore.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
} //end updateDB

function handleEmpPayDelete() {

    //returns array of selected rows ids only
    var selectedKeys = empayGrid.selModel.selections.keys;
    if(selectedKeys.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to delete selection?'+empayGrid.selModel.selections.keys, deleteEmpPay);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
}; // end handleDelete

function deleteEmpPay(btn) {
    if(btn=='yes')
    {

        //returns record objects for selected rows (all info for row)
        var selectedRows = empayGrid.selModel.selections.items;
        //returns array of selected rows ids only
        var selectedKeys = empayGrid.selModel.selections.keys;
        //encode array into json
        Ext.MessageBox.alert('OK',selectedKeys)
        var encoded_keys = Ext.encode(selectedKeys);
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getEmpPayments.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "delete", //pass task to do to the server script
                ID: encoded_keys,//the unique id(s)
                key: primaryKey//pass to server same 'id' that the reader used
            },

            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    //                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);

                    Ext.MessageBox.alert('OK',json.del_count + ' record(s) deleted.');

                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                empayStore.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    }//end if click 'yes' on button
} // end deleteRecord

var empPayreader=new Ext.data.JsonStore({
    url: 'getEmpPayments.php',
    root: 'dispEmps',

    id: 'ID',//
    baseParams:{
        task: "getEmp"
    },//This
    fields:['PID','FirstName','LastName','Surname','department','JobTitle']
});

var filters12 = new Ext.ux.grid.GridFilters({
    // encode2 and local configuration options defined previously for easier reuse
    encode: encode2, // json encode2 the filter query
    local: local2,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'PID'
    }, {
        type: 'string',
        dataIndex: 'FirstName',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'LastName'
    }, {
        type: 'string',
        dataIndex: 'Surname'
    }, {
        type: 'string',
        dataIndex: 'department'
    }, {
        type: 'string',
        dataIndex: 'JobTitle'
    }]
});

var createColModels1=function(finish,start){
    var columns = [
    {
        id:'PID2',
        header: "PID",
        width: 55,
        sortable: true,
        dataIndex: 'PID',
        filterable: true
    },
    {
        header: "First Name",
        width: 80,
        sortable: true,
        dataIndex: 'FirstName',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "Last name",
        width: 80,
        sortable: true,
        dataIndex: 'LastName',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "SurName",
        width: 80,
        sortable: true,
        dataIndex: 'Surname',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Department",
        width: 80,
        sortable: true,
        dataIndex: 'department',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "Role",
        width: 80,
        sortable: true,
        dataIndex: 'JobTitle',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    }
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var detailsPanel=new Ext.grid.GridPanel({
    id:'grid2',
    store: empPayreader,
    colModel: createColModels1(4),
    tbar: [{
        tag: 'input',
        type: 'text',
        size: '30',
        value: '',
        style: 'background: #F0F0F9;'
    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid,
        iconCls:'refresh'
    },{
        text: 'Clear Filter',
        handler: function () {
            grid.filters.clearFilters();
        }
    }],
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("payments").getForm().loadRecord(rec);
                var gridrecord = detailsPanel.getSelectionModel().getSelected();
                if (gridrecord == undefined){
                    alert('no good');
                }else{
                    //sends details to family information form
                    strPid=gridrecord.get('PID');
                    empayStore.load({
                        params:{
                            pid:strPid
                        }
                    });
                }
            }
        }
    }),
    //                autoExpandColumn: 'FirstName',
    height: 550,
    title:'Employees Data',
    border: true,
    listeners: {
        render: {
            fn: function(g){
                empPayreader.load({
                    params: {
                        start: 0,
                        limit: 10
                    }
                });
                g.getSelectionModel().selectRow(0);
                    delay: 10
            }

        }
    // Allow rows to be rendered.
    },

    plugins: [filters12],
    bbar: new Ext.PagingToolbar({
        store: empPayreader,
        pageSize: 10,
        plugins: [filters12]

    })
})

var empgridForm = new Ext.FormPanel({
    id: 'payments',
    frame: true,
    labelAlign: 'left',
    title: 'Employees List',
    bodyStyle:'padding:5px',
    url:'getEmpPayments.php',
    width: 750,
    height:600,
    record: null,
    layout: 'column',    // Specifies that the items will now be arranged in columns
    items: [{
        columnWidth: 0.30,
        layout: 'fit',
        items: {
            items:[detailsPanel]
        }
    },{
        columnWidth: 0.70,
        xtype: 'fieldset',
        title:'Employee details',
        autoHeight: true,
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: false,
        style: {
            "margin-left": "10px", // when you add custom margin in IE 6...
            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'PID',
            name: 'PID',
            disabled:true
        },{
            xtype: 'textfield',
            fieldLabel: 'First Name',
            name: 'FirstName',
            disabled:true
        },{
            xtype: 'textfield',
            fieldLabel: 'Last Name',
            name: 'LastName',
            disabled:true
        },{
            xtype: 'textfield',
            fieldLabel: 'Surname',
            name: 'Surname',
            disabled:true
        },{
            xtype: 'textfield',
            fieldLabel: 'Department',
            name: 'department',
            disabled:true
        }, {
            xtype: 'textfield',
            fieldLabel: 'JobTitle',
            name: 'JobTitle',
            disabled:true

        },{
            xtype: 'panel',
            activeTab:0,
            items: [{
                items:[empayGrid]
            }]
        }],
        buttons: [{
            text: 'Save',
            iconCls:'icon-save',
            handler: function(){
                empgridForm.form.submit({
                    url:'saveEmployee.php', //php
                    baseParams:{
                        task: "create"
                    },
                    waitMsg:'Saving Data...',
                    success: function (form, action) {
                        Ext.MessageBox.alert('Message', 'Saved OK');

                    },
                    failure:function(form, action) {
                        Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                    }
                })
            //                refreshGrid();
            }
        },{
            text: 'Cancel'
        },{
            text: 'Create',
            iconCls:'silk-user-add',
            handler:function(){
                empgridForm.getForm().reset();
            }
        },{
            text: 'Reset',
            iconCls:'silk-user-add',
            handler:function(){
                empgridForm.getForm().reset();
            }
        }]
    }]

});

empPayreader.load();


function handleTab(editEvent){
var colName= payByCodeDetails.getColumnModel().getDataIndex(4); 
//                    //    alert(gridField+','+colName)
    if(colName=='balance'){
        addNewRow();
    }else{
        updatePayCode(editEvent);
    }
}

function handlePcodeEdit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;
//    var colName= payByCodeDetails.getColumnModel().getDataIndex(4); 
//    alert(gridField+','+colName)
//    if(gridField=='balance'){
//        addNewRow();
//    }else{
         updatePayCode(editEvent);
//    }
//    start the process to update the db with cell contents

}

function handlePTypeDelete() {

    //returns array of selected rows ids only
    var selectedKeys = ptypegrid.selModel.selections.keys;
    if(selectedKeys.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to delete selection?'+ptypegrid.selModel.selections.keys, deletePtype);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
}; // end handleDelete

function updatePayCode(oGrid_Event) {

    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
    } else
{
        var fieldValue = oGrid_Event.value;
    }
    var payCode=postByCodeForm.getForm().findField('payCode').getValue();
    var payType=postByCodeForm.getForm().findField('payType').getValue();

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {  
        waitMsg: 'Saving changes...',
        url: 'getEmpPayments.php',
        params: {
            task: "saveempPayment", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used
            keyID: oGrid_Event.record.data.ID,
            pid:oGrid_Event.record.data.PID,
            paycode:payCode,
            paytype:payType,
            amount:oGrid_Event.record.data.Amount,
            balance:oGrid_Event.record.data.Balance,
            field: oGrid_Event.field,//the column name
            value: fieldValue,//the updated value
            
            originalValue: oGrid_Event.record.modified
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            if(oGrid_Event.record.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
                var newID = responseData.newID;
                var empNames = responseData.empNames;
                var Amount=responseData.Amount;
                var Balance=responseData.Balance;
                oGrid_Event.record.set('newRecord','no');
                oGrid_Event.record.set('ID',newID);
                oGrid_Event.record.set('empNames',empNames);
//                 payCodeStore.commitChanges();
                oGrid_Event.record.set('Amount',Amount);
                oGrid_Event.record.set('Balance',Balance);
                payCodeStore.commitChanges();
             
            
            } else {
                payCodeStore.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
} //end updateDB

function deletePtype(btn) {
    if(btn=='yes')
    {

        //returns record objects for selected rows (all info for row)
        var selectedRows = ptypegrid.selModel.selections.items;
        //returns array of selected rows ids only
        var selectedKeys = ptypegrid.selModel.selections.keys;
        //encode array into json
        Ext.MessageBox.alert('OK',selectedKeys)
        var encoded_keys = Ext.encode(selectedKeys);
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getPayTypes.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "delete", //pass task to do to the server script
                ID: encoded_keys,//the unique id(s)
                key: primaryKey//pass to server same 'id' that the reader used
            },

            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);

                    Ext.MessageBox.alert('OK',json.del_count + ' record(s) deleted.');

                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                ptypestore.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    }//end if click 'yes' on button
} // end deleteRecord