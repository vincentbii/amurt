/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

Ext.QuickTips.init();

var empDCm = new Ext.grid.ColumnModel(
    [
    {
        id: 'ID',
        header: 'ID',
        dataIndex: 'ID',
        width: 100
    },{
        header: 'First Name',
        dataIndex: 'FirstName',
        width: 100
    },
    {
        header: 'Surname',
        dataIndex: 'Surname',
        width: 200
    },
    {
        header: 'Last Name',
        dataIndex: 'LastName',
        width: 200
    },{
        header: 'Start Date',
        dataIndex: 'AppointDate',
        width: 130
    },{
        header: 'Basic Pay',
        dataIndex: 'BasicPay',
        width: 130
    }
    ]);

// by default columns are sortable
empDCm.defaultSortable = true;
// specify a jsonReader (coincides with the XML format of the returned data)
var empDreader= new Ext.data.JsonReader(
{
    root: 'dispEmps',
    id: 'ID'//
},
[
{
    name: 'ID',
    type:'numeric'
},
{
    name: 'FirstName',
    type: 'string'
},
{
    name: 'Surname',
    type: 'string'
},
{
    name: 'LastName',
    type: 'string'
},
{
    name: 'AppointDate',
    type: 'string'
},
{
    name: 'BasicPay',
    type: 'string'
}
]
)
// create the Data Store
var empDstore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        //	        	url: 'sheldon.xml', //where to retrieve data
        url: 'getEmps.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        task: "readEmps"
    },//This
    reader:empDreader,
    sortInfo: {
        field:'FirstName',
        direction:'ASC'
    },
    groupField:'FirstName'
});

//var editor = new Ext.ux.grid.RowEditor({
//    saveText: 'Update'
//});

var empDgrid = new Ext.grid.GridPanel({
    store: empDstore,
    width: 600,
    cm: empDCm,
    region:'center',
    margins: '0 5 5 5',
    autoExpandColumn: 'ID',
    //        plugins: [editor],
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    frame: true
        
});





 var salStore = new Ext.data.JsonStore({
        url: 'getSalary.php',
        root: 'salary',
        fields: ['pid', 'salary','payMonth']
    });


   var chart= new Ext.Panel({
        height: 300,
        renderTo: document.body,
        region:'north',
        layout:'fit',
        margins:'5 5 0',
        split:true,
        minHeight:100,
        maxHeight:300,

        items: {
            xtype: 'columnchart',
            store: salStore,
            yField: 'salary',
            url: '../include/Extjs/resources/charts.swf',
            xField: 'payMonth',
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Staff Salaries'
            }),
            yAxis: new Ext.chart.NumericAxis({
                displayName:'salary',
                labelRenderer:Ext.util.Format.numberRenderer('0,0')
            }),
            extraStyle: {
                xAxis: {
                    labelRotation: -90
                }
            },
            series: [{
                type: 'column',
                displayName: 'Salary',
                yField: 'salary',
                style: {
                    image:'../include/Extjs/chart/bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'payMonth',
                yField: 'payMonth',
                style: {
                    color: 0x15428B
                }
            }]
        }
    });

var emps = new Ext.Panel({

    layout: 'border',
    layoutConfig: {
        columns: 1
    },
    width:600,
    height: 600,
    items: [chart,empDgrid]
});



empDstore.load();
salStore.load();


empDgrid.getSelectionModel().on('selectionchange', function(sm){
    empDgrid.removeBtn.setDisabled(sm.getCount() < 1);
});

