<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../Classes/PHPExcel.php';

// Create new PHPExcel object
//echo date('H:i:s') . " Create new PHPExcel object\n";
$objPHPExcel = new PHPExcel();

// Set properties
//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Create a first sheet
// 
$sql = 'SHOW COLUMNS FROM proll_listings';

    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    
    $x=0;
    while ($x<$numRows) {
//        if ($x<3){
            $colname=$result->FetchRow();
            $col[$colname[0]]=$colname[0];
//        }
      $x++; 
        
    }
//echo date('H:i:s') . " Add data\n";
    $objPHPExcel->setActiveSheetIndex(0);
   for ($i = 2; $i <= $numRows; $i++) {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,'$i' );
        }


// Rows to repeat at top
//echo date('H:i:s') . " Rows to repeat at top\n";
$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);


//// Add data
//for ($i = 2; $i <= 5000; $i++) {
//	$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, "FName $i");
//	$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, "LName $i");
//	$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, "PhoneNo $i");
//	$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, "FaxNo $i");
//	$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, true);
//}


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
