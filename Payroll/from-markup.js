/*!
 * Ext JS Library 3.3.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.onReady(function(){
    var btn = Ext.get("create-grid");
    btn.on("click", function(){
        btn.dom.disabled = true;
        
        // create the grid
        var grid = new Ext.ux.grid.TableGrid("the-table", {
            stripeRows: true, // stripe alternate rows
        tbar: [{xtype: 'exportbutton',
                store: itemsReader
            }]
//        })
        });
        grid.render();
    }, false, {
        single: true
    }); // run once
    
//    var linkButton = new Ext.LinkButton({
//        id: 'grid-excel-button',
//        text: 'Export to Excel'
//    });
    //create the Grid
 
//    linkButton.getEl().child('a', true).href = 'data:application/vnd.ms-excel;base64,' +
//    Base64.encode(grid.getExcelXml());
});
