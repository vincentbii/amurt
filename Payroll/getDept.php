
<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task=$_POST['task'];

if($task=='glAccounts'){
    getGLAccounts2();
}else if($task=='departments'){
    getDepartments();
}else{
    echo '{"Failure":"True"}';
}

function getGLAccounts2(){
     global $db;
    $sql = "SELECT accountcode,accountname FROM chartmaster where accountname like '%staff%'";
    
    if($strName<>''){
       $sql =$sql." where accountname like '$strName%'";
    }
    
    $request = $db->Execute($sql);
//    echo $sql;
    
    echo '{
        "glAccounts":[';
    while ($row = $request->FetchRow()) {
        $accDesc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[accountname]);

        echo '{"accountcode":"' . $row[0] . '","accountname":"' . $row[1] .'"},';
    }
    echo ']}';
}

function getdepartments(){
    global $db;
    $sql = 'SELECT `ID`, `Name`,gl_acc FROM proll_departments';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
        "Departments":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"deptID":"' . $row[0] . '","deptName":"' . $row[1] . '","gl_acc":"' . $row[2] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}
?>