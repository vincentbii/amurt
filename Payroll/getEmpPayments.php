<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : ($_REQUEST['task']);
$pid = ($_REQUEST['pid']) ? ($_REQUEST['pid']) : ($_POST['pid']);
$catID == ($_POST['catID']) ? ($_POST['catID']) : '1';
$payMonth == ($_POST['payMonth']) ? ($_POST['payMonth']) : ($_REQUEST['task']);
$PayID == ($_POST['PayID']) ? ($_POST['PayID']) : '1';
$strpayType = $_REQUEST['payType'];
$ptype=($_REQUEST['pType']) ? ($_REQUEST['pType']) : ($_POST['pType']);
$overTime=($_REQUEST['overTime']) ? ($_REQUEST['overTime']) : ($_POST['overTime']);
//$task="getAmount";
switch ($task) {
    case "update":
        saveData();
        break;
    case "readPayments":
        showData($pid);
        break;
    case "getEmp":
        getEmp();
        break;
    case "getType":
        getTypes();
        break;
    case "getType2":
        getTypes2($catID);
        break;
    case "addEmpPays":
        addEmpPays();
        break;
    case "calcNSSF":
        calcAllNSSF();
        break;
    case "calcNHIF":
        calcAllNHIF();
        break;
    case "getAmount":
        getAmount($pid);
        break;
    case "getNSSF":
        getNSSF($pid);
        break;
    case "getOvertime":
        getOverTime($pid,$ptype,$overTime);
        break;
    case "getNHIF":
        getNHIF($pid);
        break;
    case "getGratuity":
        getGratuity($pid);
        break;
    case "delete":
        deleteEmpPay($pid);
        break;
    case "updateAllTax":
        if ($payMonth == "") {
            echo "{failure:true}";
        } else {
            updateAllTax($payMonth);
        }
        break;
    case "getPayDepts":
        getPayDepts();
        break;
    case "getPayTypes":
        getPayTypes();
        break;
    case "postPaybyCode":
        postPaybyCode($strpayType);
        break;
    case "deleteEmps":
        deleteEmps();
        break;
    case "saveempPayment":
        saveempPayment();
        break;
    case "saveempPayment2":
        saveempPayment2();
        break;
    case "savePayment":
        savePayment();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function getAmount($pid) {
    global $db;

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name like "income%"';
    $result2 = $db->Execute($sql2);
    while ($row = $result2->FetchRow()) {
        $data[1][] = $row[0];
        $data[2][] = $row[1];
        $data[3][] = $row[2];
        $data[4][] = $row[3];
    }

    $sql3 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name like "Personal R%"';
    $result3 = $db->Execute($sql3);
    $row3 = $result3->FetchRow();
    $relief = $row3[0];
    
//    echo 'relief='.$relief;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name="016" and pid="' . $pid . '"';
//    echo $sql;
    $result1 = $db->Execute($sql);
    $nsRow = $result1->FetchRow();
    $nssf = $nsRow[1];
    
//    echo '<br>nssf='.$nssf;
    
    $sql = 'Select pid,amount from proll_emp_payments where pay_name="014" and pid="' . $pid . '"';
    $result1 = $db->Execute($sql);
    $nsRow = $result1->FetchRow();
    $pension = $nsRow[1];

//        echo '<br>pension='.$pension;
    
    $sql = 'Select pid,sum(amount) from proll_emp_payments where pay_type in (1,2) and pay_name<>"014" and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    while ($row = $result->FetchRow()) {
//        for($i=0;$i<5;$i++){
        $pay = $row[1] - $nssf;
        $pay = $pay - $pension;
      

        if ($pay < $data[2][0]) {
            $tax = 0;
        } else if ($pay == $data[2][0]) {
            $tax = $data[3][0] / 100 * $data[4][0];
//            $tax =($tax-$relief);
        } else if ($pay > $data[1][1] && $pay <= $data[2][1]) {
            $tax = intval($data[3][0] / 100 * $data[4][0]);
            $balTax = intval($pay - $data[4][0]);
            $k = intval($data[3][1] / 100 * $balTax);
            $tax = $tax + $k ;
//            $tax =($tax-$relief);
        } else if ($pay > $data[1][2] && $pay <= $data[2][2]) {
            $tax = intval($data[3][0] / 100 * $data[4][0]);
            $tax2 = intval($data[3][1] / 100 * $data[4][1]);
            $balTax = intval($pay - ($data[4][0] + $data[4][1]));
            $k = intval($data[3][2] / 100 * $balTax);
            $tax = $tax + $k + $tax2 ;
//             $tax =($tax-$relief);
        } else if ($pay > $data[1][3] && $pay <= $data[2][3]) {
            $tax = intval($data[3][0] / 100 * $data[4][0]);
            $tax2 = intval($data[3][1] / 100 * $data[4][1]);
            $tax3 = intval($data[3][2] / 100 * $data[4][2]);
            $balTax = intval($pay - ($data[4][0] + $data[4][1] + $data[4][2]));
            $k = intval($data[3][3] / 100 * $balTax);
            $tax = $tax + $k + $tax2 + $tax3 ;
//            $tax =($tax-$relief);
        } else if ($pay > $data[1][4]) {
            $tax = intval($data[3][0] / 100 * $data[4][0]);
            $tax2 = intval($data[3][1] / 100 * $data[4][1]);
            $tax3 = intval($data[3][2] / 100 * $data[4][2]);
            $tax4 = intval($data[3][3] / 100 * $data[4][3]);
            $balTax = intval($pay - ($data[4][0] + $data[4][1] + $data[4][2] + $data[4][3]));
            $k = $data[3][4] / 100 * $balTax;
            $tax = $tax + $k + $tax2 + $tax3 + $tax4 ;
//            $tax =($tax-$relief);
        } else {
            $tax = 0;
        }
//        }
    }
    return $tax;
//    echo '{"amount":"' . $tax . '"}';
//    echo ']}';
}

function updateAllTax($payMonth) {
    global $db;
    $payMonth = $_POST['payMonth'];
    $sql2 = 'Select a.pid,concat(b.firstname," ",b.lastname," ",b.surname) as empnames, a.amount from proll_emp_payments a
inner join proll_empregister b on a.pid=b.pid where a.pay_name=1';
    $result2 = $db->Execute($sql2);
    while ($row = $result2->FetchRow()) {
        $amount = getAmount($row[0]);
        $sql3 = "select pid,pay_type from proll_payments where pid='" . $row[0] . "' and pay_type IN('PAYE','Personal Relief') 
            and payMonth='" . $payMonth . "'";
        $result = $db->Execute($sql3);
        $numRows = $result->RecordCount();
        if ($numRows < 1) {
            $sql = 'insert into proll_payments(pid,emp_names,catid,pay_type,amount,payDate,payMonth,status)
                    values("' . $row[0] . '","' . $row[1] . '","TAX","PAYE","' . $amount . '","' . date("Y-m-d") . '","' . $payMonth . '","Open")';
            $db->Execute($sql);
            echo $sql;
            
            $sql2 = 'insert into proll_payments(pid,emp_names,catid,pay_type,amount,payDate,payMonth,status)
                    values("' . $row[0] . '","' . $row[1] . '","Relief","Personal Relief","1162","' . date("Y-m-d") . '","' . $payMonth . '","Open")';
            $db->Execute($sql2);
//            echo $sql2;
            echo '{success:true}';
        }else{
            echo '{failure:true}';
        }
    }
    
    //  echo '{"amount":"' . $sql . '"}';
//    echo ']}';
}

function getNHIF($pid) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=1 and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $pay = $row[1];

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="NHIF"';
    $result2 = $db->Execute($sql2);

    while ($row2 = $result2->FetchRow()) {
//        echo $row2[1];

        if ($pay >= $row2[0] && $pay <= $row2[1]) {
            $nhif = $row2[2];
        }
    }

    echo '{"amount":"' . $nhif . '"}';
//    echo ']}';
}

function getNSSF($pid) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=1 and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $pay = $row[1];

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="NSSF"';
    $result2 = $db->Execute($sql2);
    $row = $result2->FetchRow();
    $nssf = $row[2];

    echo '{"amount":"' . $nssf . '"}';
//    echo ']}';
}

function getOverTime($pid,$ptype,$overTime) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name="001" and pid="' . $pid . '"';
//    echo $sql;
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $basicPay = $row[1];
    if($ptype=='003'){
        $overType=1.5;
    }elseif($ptype=='004'){
        $overType=2;
    }
    
    //17000/30 * 3/8 *1.5
    
    $overAmount=($basicPay/30*$overTime/8*$overType);
 

    echo '{"amount":"' . number_format($overAmount,2) . '"}';
//    echo ']}';
}

function getGratuity($pid) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=1 and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $pay = $row[1];

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="Gratuity"';
    $result2 = $db->Execute($sql2);
    $row = $result2->FetchRow();
    $gratuity = $row[2] / 100 * $pay;



    echo '{"amount":"' . $gratuity . '"}';
//    echo ']}';
}

function showData($pid) {
    global $db;
    
    $debug=true;
    
    $sql = 'SELECT p.`ID`,p.`pid`, k.`catname`, c.`type`, p.`Amount`,p.Balance FROM proll_emp_payments p
                inner join proll_paycategory k on k.id=p.pay_type
                inner join proll_paytypes c on c.id=p.pay_name where pid="' . $pid . '"';
    if($debug) echo $sql;
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "empPayments":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        $paytype = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]);
        $PayName = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[3]);
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1] . '","Pay_Type":"' . $paytype . '",
            "Pay_Name":"' . $PayName . '","Amount":"' . $row[4]. '","Balance":"' . $row[5] . '"}';
        if ($counter < $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmp() {
    global $db;
    $sql = 'SELECT ID,PID,firstName, Surname,LastName,department,JobTitle FROM proll_empregister';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "dispEmps":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1] . '","FirstName":"' . $row[2] . '","Surname":"'
        . $row[3] . '","LastName":"' . $row[4] . '","department":"' . $row[5] . '","JobTitle":"' . $row[6] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getTypes() {
    global $db;
    $sql = 'SELECT ID,CatName FROM proll_Paycategory';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getTypes":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","CatName":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getTypes2($catID) {
    global $db;
    if (isset($catID) || !empty($catID)) {
        $sql = 'SELECT ID,`type` FROM proll_Paytypes where catid="' . $catID . '"';
    } else {
        $sql = 'SELECT ID,`type` FROM proll_Paytypes';
    }

    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getTypes2":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        $paytype = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]);
        $id = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[0]);
        echo '{"ID":"' . $id . '","paytype":"' . $paytype . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getPayDepts() {
    global $db;
    $sql = 'SELECT * FROM proll_departments p;';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getDepts":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","Name":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getPayTypes() {
    global $db;
    $sql = 'SELECT id,`type` FROM proll_paytypes p where posted=1;';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getPayTypes":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        $paytype = $desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]);
        echo '{"payCode":"' . $row[0] . '","payType":"' . $paytype . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function postPaybyCode($strpayType) {
    global $db;
    $mnth = date('m');
    $sql = 'SELECT p.ID,p.pid,p.emp_names,amount,p.Balance FROM proll_payments p 
        left join proll_paytypes t on p.pay_type=t.`type`
        where p.payMonth="' . date('F') . '" and t.ID="' . $strpayType . '"';
    $result = $db->Execute($sql);
//    echo $sql;
    $numRows = $result->RecordCount();
    echo '{
    "postPaybyCode":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1]. '","empNames":"' . $row[2] 
                . '","Amount":"' . $row[3]. '","Balance":"' . $row[4] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function addEmpPays() {
    global $db;
    $pid = $_POST[pid];
    $pay_type = $_POST[catId];
    $payname = $_POST[PayID];
    $amount = $_POST[amount];
    $balance = $_POST[balance];

    $sql = 'insert into proll_emp_payments(pid,pay_type,pay_name,amount,Balance)
        values("' . $pid . '","' . $pay_type . '","' . $payname . '","' . $amount . '","' . $balance  . '")';
    $result = $db->Execute($sql);
    echo $sql;
    echo '{success:true}';
}

function calcAllNHIF() {
    global $db;
    $pid = $_POST[pid];
    $pay_type = $_POST[catId];
    $payname = $_POST[PayID];
    $amount = $_POST[amount];

    $sql1 = 'Select pid,amount from proll_emp_payments where pay_name=1';
    $result1 = $db->Execute($sql1);

    while ($row1 = $result1->FetchRow()) {

        $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="NHIF"';
        $result2 = $db->Execute($sql2);
        while ($row = $result2->FetchRow()) {
            if ($row1[1] >= $row[0] && $row1[1] <= $row[1]) {
                $nhif = $row[2];
            }
        }

        $sql3 = 'Select pid,amount from proll_emp_payments where pay_name=2 and pid="' . $row1[0] . '"';
        $result3 = $db->Execute($sql3);
        $row3 = $result2->FetchRow();
        if ($row3[0] == "") {
            $sql = 'insert into proll_emp_payments(pid,pay_type,pay_name,amount)
                values("' . $row1[0] . '","' . $pay_type . '","' . $payname . '","' . $nhif . '")';
            $result3 = $db->Execute($sql);
        }
    }

//    echo '{success:true}';
}

function calcAllNSSF() {
    global $db;
    $pid = $_POST[pid];
    $pay_type = $_POST[catId];
    $payname = $_POST[PayID];
    $amount = $_POST[amount];

    $sql1 = 'select pid from proll_empregister';
    $result1 = $db->Execute($sql1);
    while ($row = $result1->FetchRow()) {

        $sql = 'insert into proll_emp_payments(pid,pay_type,pay_name,amount)
        values("' . $row[0] . '","' . $pay_type . '","' . $payname . '","' . $amount . '")';
        $result = $db->Execute($sql);
    }
//    echo '{success:true}';
}

function deleteEmpPay() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     */
    global $db;
    $key = $_POST['key'];
    $arr = $_POST['ID'];
    $count = 0;

    if (version_compare(PHP_VERSION, "5.2", "<")) {
        require_once("./JSON.php"); //if php<5.2 need JSON class
        $json = new Services_JSON(); //instantiate new json object
        $selectedRows = $json->decode(stripslashes($arr)); //decode the data from json format
    } else {
        $selectedRows = json_decode(stripslashes($arr)); //decode the data from json format
    }

    //should validate and clean data prior to posting to the database
    foreach ($selectedRows as $row_id) {
        $id = (integer) $row_id;
        $query = 'DELETE FROM proll_emp_payments WHERE `' . $key . '` = ' . $id;
        $result = $db->Execute($query); //returns number of rows deleted
        if ($result)
            $count++;
    }

    if ($count) { //only checks if the last record was deleted, others may have failed

        /* If using ScriptTagProxy:  In order for the browser to process the returned
          data, the server must wrap te data object with a call to a callback function,
          the name of which is passed as a parameter by the ScriptTagProxy. (default = "stcCallback1001")
          If using HttpProxy no callback reference is to be specified */
        $cb = isset($_GET['callback']) ? $_GET['callback'] : '';

        $response = array('success' => $count, 'del_count' => $count, 'sql:' => $query);


        if (version_compare(PHP_VERSION, "5.2", "<")) {
            $json_response = $json->encode($response);
        } else {
            $json_response = json_encode($response);
        }

        echo $cb . $json_response;
//        echo '{success: true, del_count: '.$count.'}';
    } else {
        echo '{failure: true}';
    }
}

function deleteEmps() {
    global $db;
    $key = $_POST['key'];
    $sql = "INSERT INTO proll_empregister_archive (select * from proll_empregister where pid='" . $key . "')";
    if ($results = $db->Execute($sql)) {
        $query = 'DELETE FROM proll_empregister WHERE PID = "' . $key . '"';
//        echo $query;
        if ($result = $db->Execute($query)) { //returns number of rows deleted
            echo '{success: true}';
        } else {
            echo '{failure: true}';
        }
    }
}

function savePayment() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     * $field: column or field name that is being updated (see data.Record mapping)
     * $value: the new value of $field
     */

    global $db;
    $key = $_POST['key'];
    $id = (integer) mysql_real_escape_string($_POST['keyID']);
    $field = $_POST['field'];
    $value = $_POST['value'];
    $paycode = $_POST['paycode'];
    $paytype = $_POST['paytype'];
    $newRecord = $id == 0 ? 'yes' : 'no';


    //should validate and clean data prior to posting to the database

    $sql1 = 'SELECT d.catName FROM proll_paytypes c INNER JOIN proll_paycategory d ON c.CatID=d.ID WHERE c.ID="' . $paycode . '"';
    $result1 = $db->Execute($sql1);
    $row1 = $result1->FetchRow();
    $catID = $row1[0];


//    $newID1 = $db->Insert_ID();

    if ($newRecord == 'yes') {
        //INSERT INTO `stock` (`company`) VALUES ('a new company');
        $query = 'INSERT INTO proll_payments(`' . $field . '`) VALUES (\'' . $value . '\')';
    } else {
        $query = 'UPDATE proll_payments SET `' . $field . '` = \'' . $value . '\',catID="' . $catID . '",emp_names="' . $names . '",payDate="' . date('Y-m-d') . '"
            ,payMonth="' . date('F') . '",Status="Open",pay_type="' . $paytype . '" WHERE `' . $key . '` = ' . $id;
    }

    //save data to database
    $result = $db->Execute($query);
    $rows = $db->Affected_Rows();

    $pids = $_POST['pid'];
    $sql2 = 'SELECT CONCAT(surname," ",firstname," ",lastname) from proll_empregister where pid="' . $pids . '"';
    $result2 = $db->Execute($sql2);
    $row2 = $result2->FetchRow();
    $names = $row2[0];

    $query2 = 'UPDATE proll_payments SET emp_names = "' . $names . '" where `' . $key . '` = ' . $id . ' and pid="' . $pids . '"';
    $result3 = $db->Execute($query2);

    if ($rows > 0) {
        if ($newRecord == 'yes') {
            $newID = $db->Insert_ID();
            echo "{success:true, newID:$newID}";
        } else {
            echo "{success:true}";
        }
    } else {
        echo "{success:false, error:$query}"; //if we want to trigger the false block we should redirect somewhere to get a 404 page
    }
}

function saveempPayment() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     * $field: column or field name that is being updated (see data.Record mapping)
     * $value: the new value of $field
     */
    global $db;
    $key = $_POST['key'];
    $id = (integer) mysql_real_escape_string($_POST['keyID']);
    $field = $_POST['field'];
    $value = $_POST['value'];
    $payName = $_POST['payName'];
    $paytype = $_POST['payType'];
    $pid = $_POST['PID'];
    $paycode=$_POST['paycode'];
    $sbalance=$_POST['balance'];
    $amount=$_POST['amount'];
    $newRecord = $id == 0 ? 'yes' : 'no';

    $sqli="SELECT concat(r.firstname,' ',r.lastname,' ',r.surname) as empnames,g.jname,g.house_all,g.leave_all
            FROM proll_empregister r
            left join proll_jobgroups g on r.job_group=g.jname
            where  pid='$pid'";
//    echo $sqli
    $resulti = $db->Execute($sqli);
    $rowi = $resulti->FetchRow();
    $empnames=$rowi[0];
    $houseAll=$rowi[2];
    $leaveAll=$rowi[3];
  
    $sqls="SELECT Amount FROM proll_emp_payments where pay_name='001' and pid='$pid'";
    $results = $db->Execute($sqls);
    $rows = $results->FetchRow();
    $pension=(7.5/100*$rows[0]);
    
    $sqlp="select max(balance) from proll_emp_payments where pid='$pid' and pay_name='014'";
//    echo $sqlp;
    $resultp = $db->Execute($sqlp);
    $rowp=$resultp->FetchRow();
    $pbalance=($rowp[0]+$pension);
     $pbalance=$pbalance;
    
    if($paycode=='005'){
        $amount=$houseAll;
    }elseif($paycode=='073'){
        $amount=$leaveAll;
    }elseif($sbalance){
          $balance=$sbalance;
    }elseif($paycode=='014'){
         $amount=$pension;   
         $balance=$pbalance;
    }else{
        $sql3="SELECT a.ID,a.catID,a.type,a.fixed,a.notes,a.Contribution,interest,interest_code,
        interest_name,gl_acc FROM proll_paytypes a where interest is not null";
        $result3 = $db->Execute($sql3);
        while($row3 = $result3->FetchRow()){
            if($paycode==$row3[0]){
                $SQL4="SELECT amount FROM proll_emp_payments where pay_name='$paycode'";
                $result4 = $db->Execute($sql4);
                $row4 = $result4->FetchRow();
                $amount=$row4[0];
            }
        }
    }
//    echo $sqli;
    //should validate and clean data prior to posting to the database

    $sql1 = 'SELECT c.ID,c.catID FROM proll_paytypes c WHERE c.ID ="' . $paycode . '"';
    $result1 = $db->Execute($sql1);
    $row1 = $result1->FetchRow();
    $catID = $row1[1];

//    $newID1 = $db->Insert_ID();

    if ($newRecord == 'yes') {
        //INSERT INTO `stock` (`company`) VALUES ('a new company');
        $query = 'INSERT INTO proll_emp_payments(`' . $field . '`) VALUES (\'' . $value . '\')';
//        echo $query;
    } else {
        $sql = 'select ID from proll_payCategory where catName="' . $paytype . '"';
        $result1 = $db->Execute($sql);
        $row2 = $result1->FetchRow();      
    }

    //save data to database
    $result = $db->Execute($query);
    $rows = $db->Affected_Rows();

//    if ($rows > 0) {
//        if ($newRecord == 'yes') {
            
            if($id==0){
                $newID = $db->Insert_ID();
            }else{
                $newID = $id;
            }
            
            $query = 'UPDATE proll_emp_payments SET '.$field.' = "' . $value 
                    . '",pay_type="'. $catID 
                    . '",pay_name="'. $paycode . '",amount="' . $amount
                    . '",balance="'. $balance. '" WHERE ID= ' . $newID;
        if($result = $db->Execute($query)){
//         echo $query;
            echo "{success:true, newID:$newID,empNames:'$empnames',Amount:'$amount',Balance:'$balance'}";
        } else {
            echo "{success:false,sql:$query}";
        }
//    } else {
//        echo "{success:false, error:$query}"; //if we want to trigger the false block we should redirect somewhere to get a 404 page
//    }
}

function saveempPayment2() {

    global $db;
    $key = $_POST['key'];
    $id = (integer) mysql_real_escape_string($_POST['keyID']);
    $field = $_POST['field'];
    $value = $_POST['value'];
    $payName = $_POST['payName'];
    $paytype = $_POST['payType'];
    $pid = $_POST['PID'];
    $paycode=$_POST['paycode'];
    $sbalance=$_POST['balance'];
    $amount=$_POST['amount'];
    $newRecord = $id == 0 ? 'yes' : 'no';

    $SQL1="Select count(id) as idcount from proll_emp_payments where id=$id";
    $result=$db->Execute($SQL1);
    $row=$result->FetchRow();
    
    if ($row[0]>0) {
        $query = "UPDATE proll_emp_payments SEt $field=$value WHERE ID=$id";
    } 

        if($result = $db->Execute($query)){
//         echo $query;
            echo "{success:true, newID:$id,empNames:'$empnames',Amount:'$amount',Balance:'$balance'}";
        } else {
            echo "{success:false,sql:$query}";
        }

}
?>

