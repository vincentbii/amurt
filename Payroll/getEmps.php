
<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : null;
//$task='readEmps';
switch($task) {

    case "readEmps":
        showData();
        break;
    case "readChart":
        getChartInfo();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function showData() {
    global $db;
    $sql = 'SELECT ID,PID,firstName, Surname,LastName,AppointDate,BasicPay FROM proll_empregister';
    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "dispEmps":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"'. $row[0].'","PID":"'. $row[1].'","FirstName":"'. $row[2].'","Surname":"'. $row[3].'","LastName":"'. $row[4].'","AppointDate":"'. $row[5].'","BasicPay":"'. $row[6].'"}';
        if ($counter<>$numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}


function getChartInfo() {
    global $db;
    $sql = 'SELECT ID,firstName, AppointDate,BasicPay FROM proll_empregister';
    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "dispEmps":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"'. $row[0].'","FirstName":"'. $row[2].'","AppointDate":"'. $row[5].'","BasicPay":"'. $row[6].'"}';
        if ($counter<>$numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}


?>