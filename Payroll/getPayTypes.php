<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : null;

switch($task) {
    case "create":
        addData();
        break;
    case "PayTypes":
        showData();
        break;
    case "update":
        saveData();
        break;
    case "delete":
        removeData();
        break;
    case "readCat":
        getCategory();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function showData() {
    global $db;
    $sql = 'SELECT a.ID,a.catID,a.type,a.fixed,a.notes,a.Contribution,interest,interest_code,
        interest_name,gl_acc,gl_desc FROM proll_paytypes a';
    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "PayTypes":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        $type= preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]);
        echo '{"ID":"'. $row[0].'","catID":"'. $row[1].'","type":"'. $type
                .'","fixed":"'. $row[3].'","notes":"'. $row[4].'","Contribution":"'. $row[5]
                .'","interest":"'. $row[6].'","interest_code":"'. $row[7].'","interest_name":"'. $row[8]
                .'","gl_acc":"'. $row[9].'","gl_desc":"'. $row[10].'"}';
        if ($counter<>$numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function saveData() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     * $field: column or field name that is being updated (see data.Record mapping)
     * $value: the new value of $field
    */

    global $db;
    $key = $_POST['key'];
    $id    = (integer) mysql_real_escape_string($_POST['keyID']);
    $field = $_POST['field'];
    $value = $_POST['value'];
    
    $sql="select ID from proll_paytypes where id=$id";
    $result=$db->Execute($sql);
    $currID=$result->FetchRow();
    
    $newRecord = empty($currID[0]) ? 'yes' : 'no';

    //should validate and clean data prior to posting to the database

    if ($newRecord == 'yes') {
        //INSERT INTO `stock` (`company`) VALUES ('a new company');
        $query = 'INSERT INTO proll_paytypes(`'.$field.'`) VALUES (\''.$value.'\')';
    } else {
        $query = 'UPDATE proll_paytypes SET `'.$field.'` = \''.$value.'\' WHERE `'.$key.'` = '.$id;
    }

    //save data to database
    $result=$db->Execute($query);
    $rows= $db->Affected_Rows();

    if ($rows > 0) {
        if($newRecord == 'yes') {
            $newID = $db->Insert_ID();
            echo "{success:true, newID:$newID}";
        } else {
            echo "{success:true}";
        }
    } else {
        echo "{success:false, error:$query}"; //if we want to trigger the false block we should redirect somewhere to get a 404 page
    }
}

function removeData() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
    */
    global $db;
    $key = $_POST['key'];
    $arr    = $_POST['ID'];
    $count = 0;

    if (version_compare(PHP_VERSION,"5.2","<")) {
        require_once("./JSON.php"); //if php<5.2 need JSON class
        $json = new Services_JSON();//instantiate new json object
        $selectedRows = $json->decode(stripslashes($arr));//decode the data from json format
    } else {
        $selectedRows = json_decode(stripslashes($arr));//decode the data from json format
    }

    //should validate and clean data prior to posting to the database
    foreach($selectedRows as $row_id) {
        $id = (integer) $row_id;
        $query = 'DELETE FROM proll_paytypes WHERE `'.$key.'` = '.$id;
        $result = $db->Execute($query); //returns number of rows deleted
        if ($result) $count++;
    }

    if ($count) { //only checks if the last record was deleted, others may have failed

        /* If using ScriptTagProxy:  In order for the browser to process the returned
           data, the server must wrap te data object with a call to a callback function,
           the name of which is passed as a parameter by the ScriptTagProxy. (default = "stcCallback1001")
           If using HttpProxy no callback reference is to be specified*/
        $cb = isset($_GET['callback']) ? $_GET['callback'] : '';

        $response = array('success'=>$count, 'del_count'=>$count,'sql:'=>$query);


        if (version_compare(PHP_VERSION,"5.2","<")) {
            $json_response = $json->encode($response);
        } else {
            $json_response = json_encode($response);
        }

        echo $cb . $json_response;
//        echo '{success: true, del_count: '.$count.'}';
    } else {
        echo '{failure: true}';
    }
}

function getCategory() {
    global $db;
    $sql = 'SELECT * FROM proll_paycategory';
    $result = $db->execute($sql);

    while($rec = $result->FetchRow()) {
        $arr[] = $rec;
    };

    if (version_compare(PHP_VERSION,"5.2","<")) {
        require_once("./JSON.php"); //if php<5.2 need JSON class
        $json = new Services_JSON();//instantiate new json object
        $data=$json->encode($arr);  //encode the data in json format
    } else {
        $data = json_encode($arr);  //encode the data in json format
    }

   
    $cb = isset($_GET['callback']) ? $_GET['callback'] : '';

    echo $cb . '({"results":' . $data . '})';

}//end getData

?>