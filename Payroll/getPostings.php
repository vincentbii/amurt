<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : null;
$pid = ($_REQUEST['pid']) ? ($_REQUEST['pid']) : 'P001';
$catID == ($_POST['catID']) ? ($_POST['catID']) : '1';
$pstMonth =$_POST['pstMonth'];
$pstCode=$_POST['pstCode'];
$pstCode2=$_POST['pstCode2'];
$pstBranch=$_POST['branch'];
$months=array('Jan','feb','mar','apr','may','june','july','aug','sept','oct','nov','dec');
switch ($task) {
    case "postingDetail":
        postingDetail($pstMonth,$pstCode,$pstCode1,$pstBranch);
        break;
    case "readPayments":
        showData($pid);
        break;

    default:
        echo "{failure:true}";
        break;
}//end switch


function postingDetail($pstMonth,$pstCode,$pstCode1,$pstBranch) {
    global $db;
    $sql = 'SELECT p.`ID`,p.`Pid`,p.`emp_names`,p.`pay_type`,p.`amount`,p.`payDate`,p.`payMonth` FROM `proll_payments` p 
        LEFT JOIN proll_paytypes k ON p.pay_type=k.PayType 
         left join proll_empregister r on p.pid=r.pid 
         where p.paymonth like "'.$pstMonth.'%"';
    
    if($pstCode<>''){
       $sql = $sql . " and k.ID like '$pstCode%'";
    }
    
     if($pstBranch<>''){
       $sql = $sql . " and r.branch like '$pstBranch%'";
    }
    $sql = $sql ." ORDER BY k.ID";
       // echo $sql;
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();


    echo '{
    "totalCount":"'.$numRows.'","pstDetail":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        $ptype= $row[3];
     $names= trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]));
     $pid=trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]));
        echo '{"ID":"' . $row[0] . '","Pid":"' . $pid . '","emp_names":"' . $names . '","pay_type":"' . $ptype . '",
            "amount":"' . $row[4] . '","payMonth":"' . $row[6] . '"}';
        if ($counter < $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function prollListing($pstMonth) {
    global $db;
    $sql = 'SELECT p.`ID`,p.`Pid`,p.`emp_names`,p.`pay_type`,p.`amount`,p.`payDate`,p.`payMonth` FROM `proll_payments` p 
        LEFT JOIN proll_paytypes k ON p.pay_type=k.Type where p.paymonth like"'.$pstMonth.'%" ORDER BY k.ID';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    

//    echo $sql;
    echo '{
    "totalCount":"'.$numRows.'","pstDetail":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        $ptype= $row[3];
     $names= trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]));
     $pid=trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]));
        echo '{"ID":"' . $row[0] . '","Pid":"' . $pid . '","emp_names":"' . $names . '","pay_type":"' . $ptype . '",
            "amount":"' . $row[4] . '","payMonth":"' . $row[6] . '"}';
        if ($counter < $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmp() {
    global $db;
    $sql = 'SELECT ID,PID,firstName, Surname,LastName,department,JobTitle FROM proll_empregister';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "dispEmps":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1] . '","FirstName":"' . $row[2] . '","Surname":"'
        . $row[3] . '","LastName":"' . $row[4] . '","department":"' . $row[5] . '","JobTitle":"' . $row[6] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}


function getPaytypes() {
    global $db;
    $sql = 'SELECT ID,PID,firstName, Surname,LastName,department,JobTitle FROM proll_empregister';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "dispEmps":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1] . '","FirstName":"' . $row[2] . '","Surname":"'
        . $row[3] . '","LastName":"' . $row[4] . '","department":"' . $row[5] . '","JobTitle":"' . $row[6] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

?>