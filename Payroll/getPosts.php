<?php
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : null;
$pid = ($_REQUEST['pid']) ? ($_REQUEST['pid']) : 'P001';
$catID == ($_POST['catID']) ? ($_POST['catID']) : '1';
$pstMonth = $_POST['pstMonth'];
$months = array('Jan', 'feb', 'mar', 'apr', 'may', 'june', 'july', 'aug', 'sept', 'oct', 'nov', 'dec');


function display_db_query($query_string, $connection, $header_bool, $table_params) {
    // perform the database query
    global $db;
    $result_id = $db->Execute($query_string);
    // find out the number of columns in result
    $column_count = $result_id->FieldCount();
    // Here the table attributes from the $table_params variable are added
    print("<TABLE $table_params >\n");
      $sql = 'SHOW COLUMNS FROM proll_listings';

    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    
    $x=0;
    while ($x<$numRows) {
//        if ($x<3){
            $colname=$result->FetchRow();
            $col[$colname[0]]=$colname[0];
//        }
      $x++; 
        
    }
    // optionally print a bold header at top of table
    if ($header_bool) {
        print("<thead><tr style='background:#eeeeee;'>");
        foreach ( $col as $key=>$value){
            print("<th style='width: 40px;'>".strtolower($key)."</th>");
        }
        print("</tr><thead>\n");
    }
    // print the body of the table
    while ($row = $result_id->FetchRow()) {
        print("<tbody><tr ALIGN=LEFT VALIGN=TOP>");
        for ($column_num = 0; $column_num < $column_count; $column_num++) {
            print("<TD>$row[$column_num]</TD>\n");
        }
        print("</tr></tbody>\n");
    }
    print("</table>\n");
}

function display_db_table($tablename, $connection, $header_bool, $table_params) {
    $query_string = "SELECT p.`PID`, p.`EMP_NAMES`, p.`BASIC PAY`, p.`OVERTIME HOURS @ 1.0`, p.`OVERTIME HOURS @ 1.5`, p.`OVERTIME HOURS @ 2.0`,
p.`HOUSE ALLOWANCE`, p.`ABSENTEEM (HOURS)`, p.`ABSENTEESM (DAYS)`, p.`LUMPSUM PAY1`, p.`LUMPSUM TAX`, p.`OVERTIME HOURS @ 1.14`,
p.`P.A.Y.E`, p.`PENSION`, p.`L.A.S.C`, p.`N.S.S.F`, p.`N.H.I.F`, p.`U N I O N`, p.`CHRISTMAS SAV.SCHEEM LOAN`, p.`CO-OP SAVINGS`,
p.`COTU`, p.`STAFF LOAN`, p.`SPECIAL ADVANCE`, p.`MOTOR VEHICLE BENEFIT`, p.`ACTING ALLOWANCE`, p.`LEAVE PAY`,
p.`KENINDIA ASSURANCE CO.LTD`, p.`RISK ALLOWANCE`, p.`TELEPHONE ALLOWANCE`, p.`ADMINISTRATIVE ALLOWANCE`, p.`SPECIAL DUTY ALLOWANCE`,
p.`WATER`, p.`ELECTRICITY`, p.`MEDICAL BILLS`, p.`ALLOWANCE ARREARS`, p.`CHRISTMAS SAVING REG`, p.`INVOICES REHAB`, p.`INVOICES`,
p.`INSURANCE-CFC`, p.`CALL ALLOWANCE`, p.`SECURITY CHARGES`, p.`P.C.E.A.CO-OP.SACCO SAVINGS`, p.`AFYA SACCO SAVINGS`,
p.`MID MONTH ADVANCE`, p.`UNIVERSITY LOAN`, p.`NON-PRACTICE ALLOWANCE`, p.`PENSION ARREARS`, p.`ALLOWANCES-CIRCUMCISION`,
p.`AFYA SACCO LOAN`, p.`AFYA SACCO INTEREST`, p.`AFYA SACCO REG`, p.`N.N.A.K.`, p.`P.C.E.A.CO-OP.SACCO REG`, p.`P.C.E.A.CO-OP.SACCO LOAN`,
p.`P.C.E.A.CO-OP.SACCO INTEREST`, p.`SALARY ARREARS`, p.`LONG SERVICE AWARDS`, p.`EXTRANEOUS ALLOWANCE`, p.`LEAVE ALLOWANCE`,
p.`MARATHON`, p.`BEREAVEMENT FUND`, p.`RECOVERY`, p.`CHRISTMAS S.S. INTEREST`, p.`KEWA REGISTRATION`, p.`KEWA SAVINGS`,
p.`KEWA LOAN`, p.`CAR LOAN`, p.`NIGHT & WEEKEND COVERAGE`, p.`LEAVE ALLOWANCE ARREARS`, p.`SECTIONAL HEAD ALLOWANCE`,
p.`OTHER ALLOWANCES`, p.`TRAINING`, p.`STAFF LUNCHES REHAB`, p.`EQUIPMENT RENTALS -COOKER`, p.`EQUIPMENT RENTALS-FRIDGE`,
p.`EYE UNIT INVOICES`, p.`NURSES HOSTEL RENT`, p.`REHAB STAFF PPF`, p.`REHAB STAFF MEDICAL`, p.`TOP UP ALLOWANCE`,
p.`INSURANCE-MADISON`, p.`INSURANCE-PANAFRICAN`, p.`INSURANCE-CANNON`, p.`SAFARI ALLOWANCE`, p.`SATURDAY ALLOWANCE`, p.`LOCUM`,
p.`KEWA LOAN INTEREST`, p.`INSURANCE-PIONEER`, p.`KCB LOAN`, p.`CAR LOAN INTEREST BENEFIT`, p.`STAFF LUNCHES EYE UNIT`,
p.`EQUITY LOAN`, p.`NURSES POOL-OVERTIME`, p.`HOUSE RENT`, p.`INSURANCE-BRITISH AMERI`, p.`SERVICE CHARGE`,
p.`INSURANCE COMPANY OF E.A`, p.`CHRISTMAS SAVINGS SCHEME`, p.`COOP SAVING`, p.`CIC INSURANCE`, p.`AFYA BENEVOLENT FUND`,
p.`BARCLAYS LOAN`, p.`CO-OP REG` FROM $tablename order by p.pid asc";

//    echo $query_string;
    
    display_db_query($query_string, $connection, $header_bool, 'id="the-table"');
}
?>
<HTML><HEAD><TITLE>Displaying a MySQL table</TITLE>
    <style type="text/css">
        #the-table { border:1px solid #bbb;border-collapse:collapse; }
        #the-table td,#the-table th { border:1px solid #ccc;border-collapse:collapse;padding:5px; }
        rotation {display: block;-webkit-transform: rotate(-90deg);-moz-transform: rotate(-90deg); }
         
        </style>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css" />

 	<script type="text/javascript" src="../include/Extjs/adapter/ext/ext-base.js"></script>


        <script type="text/javascript" src="../include/Extjs/ext-all.js"></script>
        
        <script type="text/javascript" src="../include/Extjs/ux/TableGrid.js"></script>


        <link rel="stylesheet" type="text/css" href="../include/Extjs/shared/examples.css" />
            <script type="text/javascript" src="../include/Extjs/export/Exporter-all.js"></script>
        <script type="text/javascript" src="from-markup.js"></script>
    </HEAD>
    <BODY>
        <script type="text/javascript" src="../include/Extjs/shared/examples.js"></script>
        <button id="create-grid" type="button">Create grid</button><br>
        <TABLE><TR><TD>
<?php
//In this example the table name to be displayed is  static, but it could be taken from a form
$table = "proll_listings p";

display_db_table($table, $global_dbh, TRUE, "border='2'");
?>
                </TD></TR></TABLE></BODY></HTML>




