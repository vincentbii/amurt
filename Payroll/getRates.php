
<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : null;

switch($task) {
    case "create":
        addData();
        break;
    case "readRates":
        showData();
        break;
    case "update":
        saveData();
        break;
    case "delete":
        removeData();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function showData() {
    global $db;
    $sql = 'SELECT ID,Rate_Name, Value,Upper_Limit,Lower_Limit FROM proll_rates';
    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "PayRates":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"'. $row[0].'","Rate_Name":"'. $row[1].'","Value":"'. $row[2].'","Upper_Limit":"'. $row[3].'","Lower_Limit":"'. $row[4].'"}';
        if ($counter<>$numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function addData() {

}

function saveData() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     * $field: column or field name that is being updated (see data.Record mapping)
     * $value: the new value of $field
    */

    global $db;
    $key = $_POST['key'];
    $id    = (integer) mysql_real_escape_string($_POST['keyID']);
    $field = $_POST['field'];
    $value = $_POST['value'];
    $newRecord = $id == 0 ? 'yes' : 'no';

    //should validate and clean data prior to posting to the database

    if ($newRecord == 'yes') {
        //INSERT INTO `stock` (`company`) VALUES ('a new company');
        $query = 'INSERT INTO proll_Rates(`'.$field.'`) VALUES (\''.$value.'\')';
    } else {
        $query = 'UPDATE proll_Rates SET `'.$field.'` = \''.$value.'\' WHERE `'.$key.'` = '.$id;
    }

    //save data to database
    $result=$db->Execute($query);
    $rows= $db->Affected_Rows();

    if ($rows > 0) {
        if($newRecord == 'yes') {
            $newID = $db->Insert_ID();
            echo "{success:true, newID:$newID}";
        } else {
            echo "{success:true}";
        }
    } else {
        echo "{success:false, error:$query}"; //if we want to trigger the false block we should redirect somewhere to get a 404 page
    }
}

function removeData() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
    */
    global $db;
    $key = $_POST['key'];
    $arr    = $_POST['ID'];
    $count = 0;

    if (version_compare(PHP_VERSION,"5.2","<")) {
        require_once("./JSON.php"); //if php<5.2 need JSON class
        $json = new Services_JSON();//instantiate new json object
        $selectedRows = $json->decode(stripslashes($arr));//decode the data from json format
    } else {
        $selectedRows = json_decode(stripslashes($arr));//decode the data from json format
    }

    //should validate and clean data prior to posting to the database
    foreach($selectedRows as $row_id) {
        $id = (integer) $row_id;
        $query = 'DELETE FROM proll_rates WHERE `'.$key.'` = '.$id;
        $result = $db->Execute($query); //returns number of rows deleted
        if ($result) $count++;
    }

    if ($count) { //only checks if the last record was deleted, others may have failed

        /* If using ScriptTagProxy:  In order for the browser to process the returned
           data, the server must wrap te data object with a call to a callback function,
           the name of which is passed as a parameter by the ScriptTagProxy. (default = "stcCallback1001")
           If using HttpProxy no callback reference is to be specified*/
        $cb = isset($_GET['callback']) ? $_GET['callback'] : '';

        $response = array('success'=>$count, 'del_count'=>$count,'sql:'=>$query);


        if (version_compare(PHP_VERSION,"5.2","<")) {
            $json_response = $json->encode($response);
        } else {
            $json_response = json_encode($response);
        }

        echo $cb . $json_response;
//        echo '{success: true, del_count: '.$count.'}';
    } else {
        echo '{failure: true}';
    }
}
?>