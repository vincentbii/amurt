<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$task = ($_POST['task']) ? ($_POST['task']) : null;
$pid = ($_REQUEST['pid']) ? ($_REQUEST['pid']) : 'P001';
$catID == ($_POST['catID']) ? ($_POST['catID']) : '1';
$payMonth == ($_POST['payMonth']) ? ($_POST['payMonth']) : 'January';

switch ($task) {
    case "update":
        saveData();
        break;
    case "readPayments":
        showData($pid);
        break;
    case "getEmp":
        getEmp();
        break;
    case "getType":
        getTypes();
        break;
    case "getType2":
        getTypes2($catID);
        break;
    case "addEmpPays":
        addEmpPays();
        break;
    case "getAmount":
        getAmount($pid);
        break;
    case "getNSSF":
        getNSSF($pid);
        break;
    case "getNHIF":
        getNHIF($pid);
        break;
    case "getGratuity":
        getGratuity($pid);
        break;
    case "delete":
        deleteEmpPay($pid);
        break;
    case "updateAllTax":
        updateAllTax($payMonth);
        break;
    case "getPayDepts":
        getPayDepts();
        break;
    case "deleteEmps":
        deleteEmps();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function getAmount($pid) {
    global $db;

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name like "income%"';
    $result2 = $db->Execute($sql2);
    while ($row = $result2->FetchRow()) {
        $data[1][] = $row[0];
        $data[2][] = $row[1];
        $data[3][] = $row[2];
        $data[4][] = $row[3];
    }

    $sql3 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name like "Personal R%"';
    $result3 = $db->Execute($sql3);
    $row3 = $result3->FetchRow();
    $relief = $row3[0];

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=3 and pid="' . $pid . '"';
    $result1 = $db->Execute($sql);
    $nsRow=$result1->FetchRow();
    $nssf=$nsRow[1];

    $sql = 'Select pid,sum(amount) from proll_emp_payments where pay_type in (1,2) and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    while ($row = $result->FetchRow()) {
//        for($i=0;$i<5;$i++){
        $pay = $row[1]-$nssf;

        if ($pay < $data[2][0]) {
            $tax = $data[3][0] / 100 * $data[4][0] - $relief;
        } else if ($pay > $data[1][1] && $pay <= $data[2][1]) {
            $tax = $data[3][0] / 100 * $data[4][0];
            $balTax = $pay - $data[4][0];
            $k = $data[3][1] / 100 * $balTax;
            $tax = $tax + $k - $relief;
        } else if ($pay > $data[1][2] && $pay <= $data[2][2]) {
            $tax = $data[3][0] / 100 * $data[4][0];
            $tax2 = $data[3][1] / 100 * $data[4][1];
            $balTax = $pay - ($data[4][0] + $data[4][1]);
            $k = $data[3][2] / 100 * $balTax;
            $tax = $tax + $k + $tax2 - $relief;
        } else if ($pay > $data[1][3] && $pay <= $data[2][3]) {
            $tax = $data[3][0] / 100 * $data[4][0];
            $tax2 = $data[3][1] / 100 * $data[4][1];
            $tax3 = $data[3][2] / 100 * $data[4][2];
            $balTax = $pay - ($data[4][0] + $data[4][1] + $data[4][2]);
            $k = $data[3][3] / 100 * $balTax;
            $tax = $tax + $k + $tax2 + $tax3 - $relief;
        } else if ($pay > $data[1][4]) {
            $tax = $data[3][0] / 100 * $data[4][0];
            $tax2 = $data[3][1] / 100 * $data[4][1];
            $tax3 = $data[3][2] / 100 * $data[4][2];
            $tax4 = $data[3][3] / 100 * $data[4][3];
            $balTax = $pay - ($data[4][0] + $data[4][1] + $data[4][2] + $data[4][3]);
            $k = $data[3][4] / 100 * $balTax;
            $tax = $tax + $k + $tax2 + $tax3 + $tax4 - $relief;
        } else {
            $tax = 0;
        }
//        }
    }
    return $tax;
//    echo '{"amount":"' . $tax . '"}';
//    echo ']}';
}

function updateAllTax($payMonth) {
    global $db;

    $sql2 = 'Select a.pid,concat(b.firstname," ",b.lastname," ",b.surname) as empnames, a.amount from proll_emp_payments a
inner join proll_empregister b on a.pid=b.pid where a.pay_name=1';
    $result2 = $db->Execute($sql2);
    while ($row = $result2->FetchRow()) {
        $amount = getAmount($row[0]);
        $sql = 'insert into proll_payments(pid,emp_names,catid,pay_type,amount,payDate,payMonth,status)
        values("' . $row[0] . '","' . $row[1] . '","Deduct","PAYE","' . $amount . '","' . date("Y-m-d") . '","' . $payMonth . '","Open")';
        $db->Execute($sql);
    }
    echo '{success:true}';
    //  echo '{"amount":"' . $sql . '"}';
//    echo ']}';
}

function getNHIF($pid) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=1 and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $pay = $row[1];

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="NHIF"';
    $result2 = $db->Execute($sql2);
    while ($row = $result2->FetchRow()) {
        if ($pay >= $row[0] && $pay <= $row[1]) {
            $nhif = $row[2];
        }
    }

    echo '{"amount":"' . $nhif . '"}';
//    echo ']}';
}

function getNSSF($pid) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=1 and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $pay = $row[1];

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="NSSF"';
    $result2 = $db->Execute($sql2);
    $row = $result2->FetchRow();
    $nssf = $row[2];

    echo '{"amount":"' . $nssf . '"}';
//    echo ']}';
}

function getGratuity($pid) {
    global $db;

    $sql = 'Select pid,amount from proll_emp_payments where pay_name=1 and pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    $pay = $row[1];

    $sql2 = 'select lower_limit,upper_limit,`value`,`rate` from proll_rates where rate_name ="Gratuity"';
    $result2 = $db->Execute($sql2);
    $row = $result2->FetchRow();
    $gratuity = $row[2] / 100 * $pay;



    echo '{"amount":"' . $gratuity . '"}';
//    echo ']}';
}

function showData($pid) {
    global $db;
    $sql = 'SELECT p.`ID`,p.`pid`, k.`catname`, c.`type`, p.`Amount` FROM proll_emp_payments p
inner join proll_paycategory k on k.id=p.pay_type
inner join proll_paytypes c on c.id=p.pay_name where pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "empPayments":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1] . '","Pay_Type":"' . $row[2] . '","Pay_Name":"' . $row[3] . '","Amount":"' . $row[4] . '"}';
        if ($counter < $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getEmp() {
    global $db;
    $sql = 'SELECT ID,PID,firstName, Surname,LastName,department,JobTitle FROM proll_empregister';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "dispEmps":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","PID":"' . $row[1] . '","FirstName":"' . $row[2] . '","Surname":"'
        . $row[3] . '","LastName":"' . $row[4] . '","department":"' . $row[5] . '","JobTitle":"' . $row[6] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getTypes() {
    global $db;
    $sql = 'SELECT ID,CatName FROM proll_Paycategory';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getTypes":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","CatName":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getTypes2($catID) {
    global $db;
    $sql = 'SELECT ID,`type` FROM proll_Paytypes where catid="' . $catID . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getTypes2":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","paytype":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getPayDepts() {
    global $db;
    $sql = 'SELECT * FROM proll_departments p;';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    echo '{
    "getDepts":[';
    $counter = 0;
    while ($row = $result->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","Name":"' . $row[1] . '"}';
        if ($counter <> $numRows) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function addEmpPays() {
    global $db;
    $pid = $_POST[pid];
    $pay_type = $_POST[catId];
    $payname = $_POST[PayID];
    $amount = $_POST[amount];

    $sql = 'insert into proll_emp_payments(pid,pay_type,pay_name,amount)
        values("' . $pid . '","' . $pay_type . '","' . $payname . '","' . $amount . '")';
    $result = $db->Execute($sql);

    echo '{success:true}';
}

function deleteEmpPay() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     */
    global $db;
    $key = $_POST['key'];
    $arr = $_POST['ID'];
    $count = 0;

    if (version_compare(PHP_VERSION, "5.2", "<")) {
        require_once("./JSON.php"); //if php<5.2 need JSON class
        $json = new Services_JSON(); //instantiate new json object
        $selectedRows = $json->decode(stripslashes($arr)); //decode the data from json format
    } else {
        $selectedRows = json_decode(stripslashes($arr)); //decode the data from json format
    }

    //should validate and clean data prior to posting to the database
    foreach ($selectedRows as $row_id) {
        $id = (integer) $row_id;
        $query = 'DELETE FROM proll_emp_payments WHERE `' . $key . '` = ' . $id;
        $result = $db->Execute($query); //returns number of rows deleted
        if ($result)
            $count++;
    }

    if ($count) { //only checks if the last record was deleted, others may have failed

        /* If using ScriptTagProxy:  In order for the browser to process the returned
          data, the server must wrap te data object with a call to a callback function,
          the name of which is passed as a parameter by the ScriptTagProxy. (default = "stcCallback1001")
          If using HttpProxy no callback reference is to be specified */
        $cb = isset($_GET['callback']) ? $_GET['callback'] : '';

        $response = array('success' => $count, 'del_count' => $count, 'sql:' => $query);


        if (version_compare(PHP_VERSION, "5.2", "<")) {
            $json_response = $json->encode($response);
        } else {
            $json_response = json_encode($response);
        }

        echo $cb . $json_response;
//        echo '{success: true, del_count: '.$count.'}';
    } else {
        echo '{failure: true}';
    }
}

function deleteEmps() {
    global $db;
    $key = $_POST['key'];
    $sql = "INSERT INTO proll_empregister_archive (select * from proll_empregister where pid='" . $key . "')";
    if ($results = $db->Execute($sql)) {
        $query = 'DELETE FROM proll_empregister WHERE PID = "' . $key . '"';
//        echo $query;
        if ($result = $db->Execute($query)) { //returns number of rows deleted
            echo '{success: true}';
        } else {
            echo '{failure: true}';
        }
    }
}
?>

