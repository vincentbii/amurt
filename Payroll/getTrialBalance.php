<?php

/* Care2x Payroll deployment 01-01-2010
 * GNU General Public License
 * Copyright 2010 George Maina
 * georgemainake@gmail.com
 *
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');


$sql1 = 'SELECT * FROM proll_company';
$result1 = $db->Execute($sql1);
$coRow = $result1->FetchRow();

$paymonth = $_POST['payMonth'];
$pstCode=$_POST['pstCode'];
$pstCode2=$_POST['pstCode2'];
$pstBranch=$_POST['branch'];
//$paymonth='August';

$coName = strtoupper($coRow['CompanyName']);
   
$sql = "SELECT t.id,p.pay_type,IF(p.Catid IN ('Pay','Benefit'),SUM(p.amount),'') AS debit,IF(p.Catid='Deduct',
SUM(p.amount),'') AS credit FROM proll_payments P 
LEFT JOIN proll_paytypes t ON p.pay_type=t.PayType
LEFT JOIN proll_empregister r on p.pid=r.pid 
WHERE p.payMonth='$paymonth' ";

     if($pstBranch<>''){
       $sql = $sql . " and r.branch like '$pstBranch%'";
    }
    $sql = $sql ." GROUP BY p.pay_type ORDER BY t.ID asc";
echo $sql;

    echo "<table border=0 cellpadding=2 width=60%>";
    echo "<tr><td colspan=4 align=center><b> $coName</b></td></tr>";
    echo "<tr><td colspan=4 align='center'><b>FINAL POSTINGS SUMMARY</b></td></tr>";
     echo "<tr><td colspan=4 align='center'><b>Branch:$pstBranch</b></td></tr>";
    echo "<tr><td colspan=4 align='center'><b>For the month of $paymonth</b></td></tr>";
    echo "<tr><td colspan=4 align='center'>------------------------------------------------------------------</td></tr>";

    echo "<tr><td><b>Code</td><td align='center'><b>Description</b></td><td><b>Debit</b></td><td><b>Credit</b></td></tr>";
$result2=$db->Execute($sql);
while ($row2 = $result2->FetchRow()) {
     if ($bg == "#ccdeeb")
            $bg = "white";
        else
            $bg = "#ccdeeb";
     echo "<tr bgcolor= '$bg'> 
                <td valign=top>$row2[0]</td>
                <td valign=top>$row2[1]</td>
                <td valign=top>".number_format($row2[2],2)."</td>
                <td valign=top>".number_format($row2[3],2)."</td>
           </tr>";
}

$sql2="SELECT SUM(p.amount) AS credit FROM proll_payments p 
left join proll_empregister r on p.pid=r.pid WHERE p.paymonth='$paymonth' AND 
p.catID IN ('Pay','Benefit')";
    
     if($pstBranch<>''){
       $sql2 = $sql2 . " and r.branch like '$pstBranch%'";
    }
    
//echo $sql2;
$result2=$db->Execute($sql2);
$row2=$result2->FetchRow();


$sql3="SELECT SUM(p.amount) AS credit FROM proll_payments P 
left join proll_empregister r on p.pid=r.pid where p.paymonth='$paymonth' and catID='Deduct'";
    
     if($pstBranch<>''){
       $sql3 = $sql3 . " and r.branch like '$pstBranch%'";
    }
    
$result3=$db->Execute($sql3);
$row3=$result3->FetchRow();
//echo $sql3;
$netpay=intval($row2[0]-$row3[0]);
echo "<tr><td></td><td>NET PAY</td><td><b></td><td><b>".number_format($netpay,2)."</b></td></tr>";
echo "<tr><td></td><td></td><td><b>".number_format($row2[0],2)."</td><td><b>".number_format(intval($row3[0]+$netpay),2)."</b></td></tr>";
echo '</table>';


?>
