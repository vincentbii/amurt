
Ext.QuickTips.init();

// turn on validation errors beside the field globally
Ext.form.Field.prototype.msgTarget = 'side';

var fm = Ext.form;


var loanReader=new Ext.data.JsonStore({
    url: 'getLoans.php',
    root: 'loans',
    fields:['pid','loanID','loanDate','StartDeduct','Amount', 'paidfrom','Notes','monthlyDeduct','Interest','paymentPeriod' ]
});
/*    Here is where we create the Form
 */

function refreshGrid() {
    loanReader.reload();//
}; // end refresh

var loanForm = new Ext.FormPanel({
    id: 'loans',
    frame: true,
    labelAlign: 'left',
    title: 'Loan data',
    bodyStyle:'padding:5px',
    url:'saveLoans.php',
    width: 500,
    height:600,
    record: null,
    layout: 'column',    // Specifies that the items will now be arranged in columns
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            xtype: 'grid',
            store: loanReader,
            columns:[
            {
                id:'pid',
                header: "PID",
                dataIndex: 'pid',
                width: 55,
                sortable: true
           
            },
            {
                header: "loanID",
                dataIndex: 'loanID',
                width: 80,
                sortable: true

            },

            {
                header: "Loan Date",
                dataIndex: 'loanDate',
                width: 80,
                sortable: true,
                editor: new Ext.form.DateField({ //DateField editor
                    allowBlank: false,

                    minValue: '10/15/07',
                    disabledDays: [0, 3, 6],
                    disabledDaysText: 'Closed on this day'
                })
               
            },

            {
                header: "Start Deduct Date",
                dataIndex: 'StartDeduct',
                width: 80,
                sortable: true
    
            },
            {
                header: "Amount",
                width: 80,
                sortable: true,
                dataIndex: 'Amount'
            },
            {
                header: "Paid From",
                dataIndex: 'paidfrom',
                width: 80,
                sortable: true

            },
            //'monthlyDeduct','Interest','paymentPeriod'
            {
                header: "Interest Rate",
                dataIndex: 'Interest',
                width: 80,
                sortable: true

            },
            {
                header: "Payment Period",
                width: 80,
                sortable: true,
                dataIndex: 'paymentPeriod'
            },
            {
                header: "Monthly Deduction",
                dataIndex: 'monthlyDeduct',
                width: 80,
                sortable: true

            },
            {
                header: "Notes",
                dataIndex: 'Notes',
                width: 80,
                sortable: true
              
            }
            ],
            tbar: [{
                text: 'Delete Rates',
                iconCls:'remove'
            }, '->', // next fields will be aligned to the right
            {
                text: 'Refresh',
                tooltip: 'Click to Refresh the table',
                handler: refreshGrid,
                iconCls:'refresh'
            }],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        Ext.getCmp("loans").getForm().loadRecord(rec);
                    }
                }
            }),
            //                autoExpandColumn: 'FirstName',
            height: 350,
            title:'Employees Loan Data',
            border: true,
            
            listeners: {
                render: function(g) {
                    g.getSelectionModel().selectRow(0);
                },
                delay: 10 // Allow rows to be rendered.
            }
        }
    },{
        columnWidth: 0.6,
        title:'Loan details',
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: false,
        style: {
            "margin-left": "10px", // when you add custom margin in IE 6...
            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        },
        items: [
        new Ext.form.FieldSet({
            defaultType: 'textfield',
            autoHeight: true,
            items: [
            {
                name: "pid",
                fieldLabel: 'PID'
            },
            {
                fieldLabel: 'loanID',
                name: 'loanID'
            },
            new Ext.form.DateField({
                fieldLabel: 'Load Date',
                name: 'loanDate',
                width:125,
                allowBlank:false
            }),
            new Ext.form.DateField({
                fieldLabel: 'Loan Start',
                name: 'StartDeduct',
                width:125,
                allowBlank:false
            }),
            {
                fieldLabel: 'Amount',
                name: 'Amount',
                id:'amount'
            }, {
                fieldLabel: "Interest Rate",
                name: 'Interest',
                id:'interest'
            },
            {
                fieldLabel: "Payment Period",
                name: 'paymentPeriod',
                id:'paymentPeriod',
                listeners: {
                    specialkey: function(f,e){
                        if (e.getKey() == Ext.EventObject.TAB) {
//                            this.value=this.
                            alert("about to submit");
                        }
                    }
                }
            },
            {
                fieldLabel: "Monthly Deduction",
                name: 'monthlyDeduct',
                id:'monthlyDeduct'
            },
            {
                fieldLabel: 'paidfrom',
                name: 'paidfrom'
            },
            {
                fieldLabel: 'Notes',
                name: 'Notes'
            }
            ]
        })],
        buttons: [{
            text: 'Save',
            iconCls:'icon-save',
            handler: function(){
                loanForm.form.submit({
                    url:'saveLoans.php', //php
                    waitMsg:'Saving Data...',
                    success: function (form, action) {
                        Ext.MessageBox.alert('Message', 'Saved OK');
                        //                        loanForm.getForm().reset();
                        refreshGrid();
                    },
                    failure:function(form, action) {
                        Ext.MessageBox.alert('Message', 'Save failed ');
                    }

                });

            }
        },{
            text: 'Cancel'
        },{
            text: 'Create',
            iconCls:'silk-user-add',
            handler:function(){
                loanForm.getForm().reset();
            }
        },{
            text: 'Reset',
            iconCls:'silk-user-add'
        }]
    }]

});

loanReader.load();
        //  Create Panel view code. Ignore.
        //    createCodePanel('form-grid.js', 'View code to create this Form');