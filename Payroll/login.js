
var loginHandler = function() {
    window.open('reports/payslips.php target="_blank"');
}

var failureHandler=function(){
    Ext.MessageBox.alert('Login Error', "Username or Password inc")
}

var loginForm = new Ext.form.FormPanel({
    id: 'login-form',
    title:'Login',
    bodyStyle: 'padding:15px;',
    border: true,
    width:350,
    height:200,
    url:'login.php',
    items: [
    {
        xtype: 'box',
        autoEl: {
            tag: 'div',
            html: '<div class="app-msg"><img src="../gui/img/logos/lopo/care_logo_mysql.gif" class="app-img" /> <b>Payroll Login</b></div>'
        }
    },
    {
        xtype: 'textfield',
        id: 'login-user',
        fieldLabel: 'Username',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'login-pwd',
        fieldLabel: 'Password',
        inputType: 'password',
        allowBlank: false,
        msgTarget:'side',
        minLength:6,
        minLengthText:'Password must be atleast six characters',
        maxLength:10,
        validationEvent:false
    }
    ],
    buttons: [{
        text: 'Login',
        handler: function() {
            Ext.Ajax.request({
                url: 'login.php',
                success:  loginHandler,
                failure:  failureHandler,
                method: 'POST',
                params: {
            //                                        username:login-user,
            //                                        password:login-pwd
            }
            });
        }
    },
    {
        text: 'Cancel',
        handler: function() {
            win.hide();
        }
    }]
});

//
var login=new Ext.Panel({
    title:'Login',
    id:'login',
    layout:'absolute',
    height:600,
        width:300,
    border:true,
    items:{
        x:400,
        y:200,
        items:[loginForm]
    }
});

//var login = new Ext.Panel({
//                    title: 'Login Form',
//                    layout:'border',
//                    items: [{
//                            region:'north',
//                            margins: '0 0 0 0',
//                            cmargins: '0 0 0 0',
//                            height:220,
//                            minSize: 100,
//                            maxSize: 300,
//                            border:false
//                        },{
//                            region:'west',
//                            margins: '0 0 0 0',
//                            cmargins: '0 0 0 0',
//                            width: 500,
//                            minSize: 100,
//                            maxSize: 300,
//                            border:false
//                        },{
//                            region:'center',
//                            margins: '0 0 0 0',
//                            top:100,
//                            border:false,
//                            items:[loginForm]
//                        }]
//                });
//
//
