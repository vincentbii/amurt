<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css">
        <link rel="stylesheet" type="text/css" href="prollcss.css">
<!--        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/xml-tree-loader.css" />-->
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/gridfilters/css/GridFilters.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/gridfilters/css/RangeMenu.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/css/Portal.css" />
        <link rel="stylesheet" type="text/css" href="../modules/accounting/accounting.css">
        <script type="text/javascript" src="../include/Extjs/adapter/yui/ext-yui-adapter.js"></script>
        <script type="text/javascript" src="../include/Extjs/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="../include/Extjs/ext-all-debug.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/XmlTreeLoader.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/CheckColumn.js"></script>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/css/RowEditor.css" />
        <script type="text/javascript" src="../include/Extjs/ux/RowEditor.js"></script>

        <link rel="stylesheet" type="text/css" href="../include/Extjs/shared/examples.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/shared/icons/silk.css" />
        <script type="text/javascript" src="../include/Extjs/shared/examples.js"></script>
        <!-- Extensions - Paging Toolbar -->
        <script type="text/javascript" src="../include/Extjs/ux/paging/pPageSize.js"></script>
        <!-- Extensions - Filtering -->
        <script type="text/javascript" src="../include/Extjs/ux/menu/EditableItem.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/menu/RangeMenu.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/gridfilters/menu/ListMenu.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/BufferView.js"></script>
        
        <script type="text/javascript" src="../include/Extjs/ux/grid/GridFilters.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/grid/filter/Filter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/grid/filter/StringFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/grid/filter/DateFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/grid/filter/ListFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/grid/filter/NumericFilter.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/grid/filter/BooleanFilter.js"></script>
        
        <!-- extensions -->
        <script type="text/javascript" src="../include/Extjs/ux/Portal.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/PortalColumn.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/Portlet.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/RowEditor.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/TableGrid.js"></script>

        
        <script type="text/javascript" src="../include/Extjs/export/Exporter-all.js"></script>
<!--        <script type="text/javascript" src="p9Reports.js"></script>-->
        <script type="text/javascript" src="empPayments.js"></script>
<!--        <script type="text/javascript" src="payroll_schedule.js"></script>-->
<!--        <script type="text/javascript" src="loans.js"></script>-->
        <script type="text/javascript" src="register.js"></script>
        <script type="text/javascript" src="payTypes.js"></script>
        <script type="text/javascript" src="payRates.js"></script>
        <script type="text/javascript" src="banks.js"></script>
        <script type="text/javascript" src="departments.js"></script>
        <script type="text/javascript" src="payments.js"></script>
        <script type="text/javascript" src="processProll.js"></script>
        <script type="text/javascript" src="payroll_Postings.js"></script>
        
       
        <script type="text/javascript" src="payeReturn.js"></script>
        <script type="text/javascript" src="nssfReturn.js"></script>
        <script type="text/javascript" src="nhifReport.js"></script>
        <script type="text/javascript" src="bankList.js"></script>
        <script type="text/javascript" src="basic.js"></script>
        <script type="text/javascript" src="paySlips.js"></script>
        <script type="text/javascript" src="employees.js"></script>
        <script type="text/javascript" src="costCenter.js"></script>
        <script type="text/javascript" src="pie-chart.js"></script>
         



        <script type="text/javascript" >
            Ext.BLANK_IMAGE_URL="../include/Extjs/resources/images/default/s.gif";
            Ext.onReady(function(){
                var win;
                var winSlip;
                var winSchedule;
                var winBankList;
                var winP9;
                var winPST;
                var winLST;
                var winTrial;
                
                var local3 = true;

                var payslipsMonths = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'payslipsMonths',
                    name:"payslipsMonths",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Month',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });

                    
                  var payslipsMonth = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'payslipsMonth',
                    name:"payslipsMonth",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Month',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });


                var dtcombo2 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'payslipMonth',
                    name:"payslipMonth",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Month',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });

                var postingType = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'postingType',
                    name:"postingType",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Posting',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'det','Detailed'], [2, 'sum', 'Summary']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });
                
                var lstCombo = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'listingType',
                    name:"listingType",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Type',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'det','Detailed'], [2, 'sum', 'Summary']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });
                
                
                var dtMonth1 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'payslipMonth1',
                    name:"payslipMonth1",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Month between ',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });

                var dtMonth2 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'payslipMonth2',
                    name:"payslipMonth2",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });

                                                var login=function() {
                                                    var pass=document.getElementById('login-pwd').value;
                                                    if(pass=='george') {
                                                      tabPanel.add(processProll);
                                                      tabPanel.doLayout();
                                                       tabPanel.setActiveTab(processProll);
                                                       win.hide();
                                                  }else{
                                                    Ext.Msg.alert('Invalid Password', 'Invalid Password, Please enter a valid Password to continue"');
                                                  }
                                            }

                var typeStore=new Ext.data.JsonStore({
                    url: 'getEmpPayments.php',
                    root: 'getDepts',

                    id: 'ID',//
                    baseParams:{
                        task: "getPayDepts"
                    },//This
                    fields:['ID','Name']
                });
                typeStore.load();

                var payDeptCombo = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'deptID',
                    name:"deptID",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: local3,
                    fieldLabel:'Department',
                    store: typeStore,
                    valueField: 'ID',
                    displayField: 'Name'
                });
                
                var typeStore2=new Ext.data.JsonStore({
                    url: 'getEmpPayments.php',
                    root: 'getDepts',

                    id: 'ID',//
                    baseParams:{
                        task: "getPayDepts"
                    },//This
                    fields:['ID','Name']
                });
                typeStore2.load();

                var payDeptCombo2 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'payDeptCombo2',
                    name:"payDeptCombo2",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: local3,
                    fieldLabel:'Department',
                    store: typeStore2,
                    valueField: 'ID',
                    displayField: 'Name'
                });
                
                 var empBranchStore2=new Ext.data.JsonStore({
                    url: 'comboFields.php',
                    root: 'getEmpBranch',
                    id: 'ID',//
                    baseParams:{
                        task: "getEmpBranch"
                    },//This
                    fields:['empBranch']
                });
                empBranchStore2.load()

                var empBranchCombo2 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'empBranchCombo2',
                    name:"empBranchCombo2",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: local3,
                    fieldLabel:'Branch',
                    store: empBranchStore2,
                    valueField: 'empBranch',
                    displayField: 'empBranch'
                });

//---------------------------------------------------
//payroll postings branches

            var empBranchStore3=new Ext.data.JsonStore({
                    url: 'comboFields.php',
                    root: 'getEmpBranch',
                    id: 'ID',//
                    baseParams:{
                        task: "getEmpBranch"
                    },//This
                    fields:['empBranch']
                });
                empBranchStore3.load()

                var empBranchCombo3 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'empBranchCombo3',
                    name:"empBranchCombo3",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: local3,
                    fieldLabel:'Branch',
                    store: empBranchStore3,
                    valueField: 'empBranch',
                    displayField: 'empBranch'
                });

//end of payroll postings
//=====================================================
                var printPayslipPDF = function() {

                    var pNo=document.getElementById('pNo').value;
                    var pNo2=document.getElementById('pNo2').value;
//                    var payDate1=document.getElementById('payDate1').value;
//                    var payDate2=document.getElementById('payDate2').value;
                    var slipMnth=document.getElementById('payslipsMonth').value;
                    var slipsNo=winPayslip.getForm().findField('cb').getGroupValue();
                    var payDept=document.getElementById('deptID').value;
                    var branch=document.getElementById('empBranchCombo2').value;
                    window.open('reports/payslips.php?pid='+pNo+'&pid2='+pNo2+'&slipMnth='+slipMnth+'&slipsNo='+slipsNo+'&deptID='+payDept+'&branch='+branch  
                    ,"Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");

                    //                    document.write(slipsNo);

                }

                var winPayslip = new Ext.form.FormPanel({
                    id: 'payslip',
                    name:'payslip',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    url: 'paySlips.php',
                    items: [payslipsMonth,{
                            xtype: 'box',
                            autoEl: { tag: 'div',
                                html: '<div class="app-msg"> <b>Payslips per page</b></div>'}
                        },{
                            xtype: 'radiogroup',
                            fieldLabel: 'No to Display ',
                            items: [
                                {boxLabel: '1', name: 'cb', id:'cb',inputValue: 1},
                                {boxLabel: '2', name: 'cb', id:'cb',inputValue: 2, checked: true},
                                {boxLabel: '3', name: 'cb', id:'cb',inputValue: 3},
                                {boxLabel: '4', name: 'cb', id:'cb',inputValue: 4}
                            ]
                        } ,{xtype: 'textfield',
                            id: 'pNo',
                            fieldLabel: 'Between: PNo',
                            allowBlank: false,
                            msgTarget:'side'
                        },{xtype: 'textfield',
                            id: 'pNo2',
                            fieldLabel:  'And    :PNo',
                            allowBlank: false,
                            msgTarget:'side'
                        },empBranchCombo2,payDeptCombo2],
                    buttons: [{
                            text: 'Open Paylips',
                            handler: function() {
                                var slipMnth=document.getElementById('payslipsMonth').value;
                               
                                var pNo=document.getElementById('pNo').value;
                                var pNo2=document.getElementById('pNo2').value;
//                                var payDate1=document.getElementById('payDate1').value;
//                                var payDate2=document.getElementById('payDate2').value;
                                var slipsNo=winPayslip.getForm().findField('cb').getGroupValue();
                                var dept=document.getElementById('payDeptCombo2').value;
                                var branch=document.getElementById('empBranchCombo2').value;

                                tabPanel.add('paySlips');
                                tabPanel.doLayout();
                                tabPanel.setActiveTab('paySlips');
                                paySlips.load({
                                    url:'paySlips.php',
                                    params:{
                                        param1:slipMnth,
                                        param2:slipsNo,
                                        param3:pNo,
                                        param4:pNo2,
                                        param6:dept,
                                        param7:branch
                                    }
                                })
                                winSlip.hide();
                                //                                }else{
                                //                                    Ext.Msg.alert('Invalid Date', 'Payslip for the month of '+slipMnth+' is not Available');
                                //                                }
                            }
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                winSlip.hide();
                            }
                        },{
                        text    : 'Print Payslip',
                        handler : printPayslipPDF
                    }
                        ]
                })

                var submitHandler = function() {

                    var pNo=document.getElementById('pNo').value;
                    var payDate1=document.getElementById('payDate1').value;
                    var payDate2=document.getElementById('payDate2').value;
                    var slipMnth=document.getElementById('payslipMonth').value;
                    var slipsNo=winPayslip.getForm().findField('cb').getGroupValue();
                    var payDept=document.getElementById('deptID').value;

                    window.open('reports/payslips.php?pid='+pNo+'&payDate1='+payDate1+'&payDate2='+payDate2+'&slipMnth='+slipMnth+'&slipsNo='+slipsNo+'&deptID='+payDept );

                    //                    document.write(slipsNo);

                }
                var buttons = [
                    {
                        text    : 'Print Payslip',
                        handler : submitHandler
                    }
                ];
                
                  var button5 = [
                    {
                        text    : 'Print Payroll',
                        handler : submitHandler5
                    }
                ];

                var paySlips=new Ext.Panel({
                    title:'PaySlips',
                    id:'paySlips',
                    height:600,
                    width:300,
                    autoScroll:true,
                    buttons: buttons
                });

                winSlip = new Ext.Window({
                    title:'Select Month',
                    applyTo:'payslipWin',
                    layout:'fit',
                    id: 'payslipform',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:400,
                    height:400,
                    closeAction:'hide',
                    items: [winPayslip]

                });

//                var postings=new Ext.Panel({
//                    title:'postings2',
//                    id:'postings2',
//                    height:600,
//                    width:300,
//                    autoScroll:true,
//                    buttons: buttons
//                });

                
                var winPostings = new Ext.form.FormPanel({
                    id: 'winPostings',
                    name:'winPostings',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    items: [postingType,dtcombo2,empBranchCombo3,
                        {xtype: 'textfield',
                            id: 'pstCode4',
                            fieldLabel: 'From Code',
                            allowBlank: false,
                            msgTarget:'side'
                        },{
                            xtype: 'textfield',
                            id: 'pstCode5',
                            fieldLabel: 'To Code',
                            allowBlank: false,
                            msgTarget:'side'
                        },{
                            xtype: 'textfield',
                            id: 'pid1',
                            fieldLabel: 'From PID',
                            allowBlank: false,
                            msgTarget:'side'
                        },{
                            xtype: 'textfield',
                            id: 'pid2',
                            fieldLabel: 'To PID',
                            allowBlank: false,
                            msgTarget:'side'
                        },{
                            xtype: 'radiogroup',
                            fieldLabel: 'Send to ',
                            items: [
                                {boxLabel: 'Screen', name: 'pstDisp1', id:'pstDisp1',inputValue: 1, checked: true},
                                {boxLabel: 'Printer', name: 'pstDisp2', id:'pstDisp2',inputValue: 2}
                            ]
                        }],
                    
                    buttons: [{
                            text: 'Open Report',
                            handler: function() {
                                var payslipMonth=document.getElementById('payslipMonth').value;
                                var pstCode=document.getElementById('pstCode4').value;
                                var pstCode1=document.getElementById('pstCode5').value;
                                var postingVer=document.getElementById('postingType').value;
                                var branch=document.getElementById('empBranchCombo3').value;
                               
                               if(postingVer=='Detailed'){
                                      pstStore.load({
                                        params:{
                                             pstMonth: payslipMonth,
                                             pstCode:pstCode,
                                             pstCode2:pstCode1,
                                             branch:branch
                                        }
                                });
                                     tabPanel.add(prollPostings);
                                     tabPanel.doLayout();
                                     tabPanel.setActiveTab(prollPostings);
                                     winPST.hide();
                                     
                               }else{
                                    tabPanel.add('tbalances');
                                    tabPanel.doLayout();
                                    tabPanel.setActiveTab('tbalances');
                                    tbalances.load({
                                    url:'getTrialBalance.php',
                                    params:{
                                        payMonth:payslipMonth,
                                        pstCode:pstCode,
                                        pstCode2:pstCode1,
                                        branch:branch
                          
                                    }
                                })
                                winTrial.hide();
                               }
                            }
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                winPST.hide();
                            }
                        }]
                })
                
                
                
                    
                  var listings=new Ext.Panel({
                    title:'listings',
                    id:'listings',
                    height:600,
                    width:300,
                    autoScroll:true,
                    buttons: button5
                });
                
                var winListings = new Ext.form.FormPanel({
                    id: 'winListings',
                    name:'winListings',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    items: [lstCombo],
                    buttons: [{
                            text: 'Open Report',
                            handler: function() {
                                var pstRprt=document.getElementById('payslipMonth').value;

                                tabPanel.add('listings');
                                tabPanel.doLayout();
                                tabPanel.setActiveTab('listings');
                                 listings.load({
                                    url:'getPosts.php',
                                    params:{
                                        list1:pstRprt
                          
                                    }
                                })
                                winLST.hide();
                            }
                        },                       
                      {
                            text: 'Cancel',
                            handler: function() {
                                winLST.hide();
                            }
                        }]
                })
//                var payslipsMonths = new Ext.form.ComboBox();
                
                  var tbalances=new Ext.Panel({
                    title:'Trial Balance',
                    id:'tbalances',
                    height:600,
                    width:300,
                    autoScroll:true,
                    buttons: button5
                });
                
                var winTrialbalance = new Ext.form.FormPanel({
                    id: 'winTrialbalance',
                    name:'winTrialbalance',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    items: [payslipsMonths,payDeptCombo],
                    buttons: [{
                            text: 'Open Trial Balance',
                            handler: function() {
                                var pstRprt=document.getElementById('payslipsMonths').value;
                                var dept=document.getElementById('deptID').value;

                                tabPanel.add('tbalances');
                                tabPanel.doLayout();
                                tabPanel.setActiveTab('tbalances');
                                 tbalances.load({
                                    url:'getTrialBalance.php',
                                    params:{
                                        payMonth:pstRprt,
                                        dept:dept 
                                    }
                                })
                                winTrial.hide();
                            }
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                winTrial.hide();
                            }
                        }]
                })
                
                winTrial = new Ext.Window({
                    title:'TRIAL BALANCE',
                    applyTo:'trialWin',
                    layout:'fit',
                    id: 'trialWin',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:400,
                    height:400,
                    items: [winTrialbalance]

                });
                
                 winPST = new Ext.Window({
                    title:'PAYROLL POSTING',
                    applyTo:'postingsWin',
                    layout:'fit',
                    id: 'postingWin',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:400,
                    height:400,
                    items: [winPostings]

                });
                
                winLST = new Ext.Window({
                    title:'PAYROLL LISTING',
                    applyTo:'listingsWin',
                    layout:'fit',
                    id: 'listingWin',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:400,
                    height:400,
                    items: [winListings]

                });
                
                win = new Ext.Window({
                    title:'Login',
                    applyTo:'hello-win',
                    layout:'fit',
                    id: 'login-form',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    items: [

                        {
                            xtype: 'textfield',
                            id: 'login-pwd',
                            fieldLabel: 'Password',
                            inputType: 'password',
                            allowBlank: false,
                            url: 'login.php',
                            msgTarget:'side',
                            minLength:6,
                            minLengthText:'Password must be atleast six characters',
                            maxLength:10,
                            validationEvent:false,
                            listeners: {
                                specialkey: function(f,e){
                                    if (e.getKey() == e.ENTER) {
                                        //                                        alert("about to submit");
                                        login();
                                    }
                                }

                            }
                        }
                    ],
                    buttons: [{
                            text: 'Login',
                            handler: login
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                win.hide();
                            }
                        }]


                });

                Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

                // create some portlet tools using built in Ext tool ids
                var tools = [{
                        id:'gear',
                        handler: function(){
                            Ext.Msg.alert('Message', 'The Settings tool was clicked.');
                        }
                    },{
                        id:'close',
                        handler: function(e, target, panel){
                            panel.ownerCt.remove(panel, true);
                        }
                    }];
                var tabPanel=new Ext.TabPanel({
                    id:'tabpanel',
                    name:'tabpanel',
                    autoScroll:true,
                    enableTabScroll : true,
                    resizeTabs: true,
                    closable:true,
                    activeTab: 0,
                    height:800,
                    
                    items: {xtype:'portal',
                        margins:'35 5 5 0',
                        title:'Payroll Portal',
                        closable:true,
                        items:[{
                                columnWidth:.50,
                                style:'padding:10px 0 10px 10px',
                                items:[{
                                        title: 'Cost Centers',
                                        layout:'fit',
                                        tools: tools,
                                        items: [costChart]
                                    },{
                                        title: 'No of Employee in each Departments',
                                        tools: tools,
                                        items:[charts1]
                                    }]
                            },{
                                columnWidth:.50,
                                style:'padding:10px 0 10px 10px',
                                items:[{
                                        title: 'Employee Salary by Month',
                                        tools: tools,
                                        items:[emps]
                                    },{
                                        title: 'Another Panel 2',
                                        tools: tools//,
                                        //html: Ext.example.shortBogusMarkup
                                    }]
                            }]
                    }
                });
                //            function showResultText(btn, text){
                //                //                    Ext.example.msg('Button Click', 'You clicked the {0} button and entered the text "{1}".', btn, text);
                //                if(text=='george') {
                //                    tabPanel.add(processProll);
                //                    tabPanel.doLayout();
                //                    tabPanel.setActiveTab(processProll);
                //                }else{
                //                    Ext.Msg.alert('Invalid Password', 'Invalid Password, Please enter a valid Password to continue"');
                //                }
                //            };


                //===========================================================================================================
                //Display the payroll schedule window
                //==========================================================================================================
                var local4 = true;

                var schMonthCombo = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'schMonth',
                    name:"schMonth",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Month',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });
                //
  var schMonthCombo2 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'schMonth2',
                    name:"schMonth2",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Month',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
                            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
                            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });

                //Schedule Departments
                var schStore=new Ext.data.JsonStore({
                    url: 'getSchedule.php',
                    root: 'getSchDepts',

                    id: 'ID',//
                    baseParams:{
                        task: "getSchDepts"
                    },//This
                    fields:['ID','Name']
                });
                schStore.load();
                //
                var schDeptCombo = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'deptSchID',
                    name:"deptSchID",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: local4,
                    fieldLabel:'Departments',
                    store: schStore,
                    valueField: 'ID',
                    displayField: 'Name'
                });


                //Schedule versions Combo


                var schVersions = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'schVersions',
                    name:"schVersions",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    fieldLabel:'Select Version',
                    store: new Ext.data.SimpleStore({
                        fields: [
                            'myID',
                            'dispValue',
                            'displayText'
                        ],
                        data: [[1, 'ver1','Version1'], [2, 'ver2', 'Version2']]
                    }),
                    valueField: 'dispValue',
                    displayField: 'displayText'

                });



                var schedule = new Ext.form.FormPanel({
                    id: 'schedules',
                    name:'schedules',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    url: 'getSchedule.php',
                    items: [schMonthCombo,{
                            xtype: 'box',
                            autoEl: {
                                tag: 'div',
                                html: '<div class="app-msg"> <b>schedules per page</b></div>'
                            }
                        },
                        new Ext.form.DateField({
                            fieldLabel: 'Dates Between',
                            name: 'schDate1',
                            id: 'schDate1',
                            width:130
                        }),
                        new Ext.form.DateField({
                            id: 'schDate2',
                            name: 'schDate2',
                            width:130
                        }),schDeptCombo,schVersions],
                    buttons: [{
                            text: 'Open Schedule',
                            handler: function() {
                                var schMnth=document.getElementById('schMonth').value;
                                var payDate1=document.getElementById('schDate1').value;
                                var payDate2=document.getElementById('schDate2').value;
                                var dept=document.getElementById('deptSchID').value;
                                var vers=document.getElementById('schVersions').value;

                                tabPanel.add('pSchedules');
                                tabPanel.doLayout();
                                tabPanel.setActiveTab('pSchedules');
                                if(vers=='Version1'){
                                    //                  Ext.Msg.alert('thanks', 'Version clicked '+vers);
                                    pSchedules.load({
                                        url:'prollSchedule.php',
                                        params:{
                                            schParam1:schMnth,
                                            schParam2:payDate1,
                                            schParam3:payDate2,
                                            schParam4:dept
                                        }
                                    })
                                }else if(vers=='Version2'){
                                    //             Ext.Msg.alert('Thanks', 'Version clicked '+vers);
                                    pSchedules.load({
                                        url:'prollSchedule2.php',
                                        params:{
                                            schParam1:schMnth,
                                            schParam2:payDate1,
                                            schParam3:payDate2,
                                            schParam4:dept
                                        }
                                    })
                                }else{
                                    Ext.Msg.alert('Please select a version', 'Version '+vers);
                                }
                                winSchedule.hide();
                                //                                }else{
                                //                                    Ext.Msg.alert('Invalid Date', 'schedule for the month of '+slipMnth+' is not Available');
                                //                                }
                            }
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                winSchedule.hide();
                            }
                        }]
                })

                var submitHandler2 = function() {
                    window.open('reports/prollSchedulepdf.php');
                }
//                
                var submitHandler5 = function() {
                    window.open('getPosts.php');
                }

//                var submitHandler2 = function() {
//                    window.open('reports/prollSchedulepdf2.php');
//                }
//                
                var buttons2 = [
                    {
                        text    : 'Print schedule',
                        handler : submitHandler2
                    }
                ];


                var buttons3 = [
                    {
                        text    : 'Print schedule2',
                        handler : submitHandler2
                    }
                ];

             

                var pSchedules=new Ext.Panel({
                    title:'Payroll Schedule',
                    id:'pSchedules',
                    height:600,
                    width:300,
                    autoScroll:true,
                    buttons: buttons2
                });
                
                //Get the combo with the bank Code
                var bankStore1=new Ext.data.JsonStore({
                    url: 'comboFields.php',
                    root: 'getBanks',

                    id: 'ID',//
                    baseParams:{
                        task: "getBanks"
                    },//This
                    fields:['bankID','BankName']
                });
                bankStore1.load()

                var bankCombo1 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'BankID1',
                    name:"BankID1",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local2',
                    fieldLabel:'Bank',
                    store: bankStore1,
                    valueField: 'bankID',
                    displayField: 'BankName',
                    listeners: {
                        select: function(f,r,i){
                            branchStore1.load({
                                params:{
                                    bankID: i+1
                                }
                            });
                        }
                    }
                });


                var branchStore1=new Ext.data.JsonStore({
                    url: 'comboFields.php',
                    root: 'getBranch',
                    id: 'branchID1',//
                    baseParams:{
                        task: "getBranch"
                        
                    },//This
                    fields:['BranchID','BankBranch']
                });
                branchStore1.load();

                var branchCombo1 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'BranchID1',
                    name:"BranchID1",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local2',
                    fieldLabel:'Bank Branch',
                    store: branchStore1,
                    valueField: 'BranchID',
                    displayField: 'BankBranch'
                });



                var pBankEmps=new Ext.Panel({
                    title:'Payroll Bank List',
                    id:'pBankEmps',
                    height:600,
                    width:300,
                    autoScroll:true,
                    buttons: buttons2
                });

                winSchedule = new Ext.Window({
                    applyTo:'scheduleWin',
                    layout:'fit',
                    title:'proll Schedule',
                    id:'proll_Schedule',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                     closeAction:'hide',
                    width:400,
                    height:400,
                    items: [schedule]

                });

                var banklist = new Ext.form.FormPanel({
                    id: 'banklist',
                    name:'banklist',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                    width:350,
                    url: 'getBankEmpList.php',
                    items: [schMonthCombo2,
                        bankCombo1,branchCombo1],
                    buttons: [{
                            text: 'Open Report',
                            handler: function() {
//                                var bProllMnth=document.getElementById('schMonth').value;
                                var bProllbank=document.getElementById('BankID1').value;
                                var schMonth2=document.getElementById('schMonth2').value;

                                tabPanel.add('pBankEmps');
                                tabPanel.doLayout();
                                tabPanel.setActiveTab('pBankEmps');
                                if(bProllbank!=''){
                                    window.open('reports/prollBanklistpdf.php?bankid='+bProllbank+'&payMonth='+schMonth2);
                                   
                                }else{
                                    Ext.Msg.alert('Please select a Bank', 'bank '+bProllbank);
                                }
                                winBankList.hide();
                                //                                }else{
                                //                                    Ext.Msg.alert('Invalid Date', 'schedule for the month of '+slipMnth+' is not Available');
                                //                                }
                            }
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                winBankList.hide();
                            }
                        }]
                })

                winBankList = new Ext.Window({
                    applyTo:'banklistWin',
                    layout:'fit',
                    title:'Proll Bank List',
                    id:'prollBanklist',
                    bodyStyle: 'padding:15px;background:transparent',
                    border: true,
                     closeAction:'hide',
                    width:400,
                    height:400,
                    items: [banklist]

                });

//=======================================================================================================================
//Begin P9 Report form
//----------------------------------------------------------------------------------------------------------------------

var local5 = true;


//Schedule Departments
var p9Store=new Ext.data.JsonStore({
    url: 'getSchedule.php',
    root: 'getSchDepts',

    id: 'ID',//
    baseParams:{
        task: "getSchDepts"
    },//This
    fields:['ID','Name']
});
p9Store.load();
//
var p9DeptCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'deptP9ID',
    name:"deptSP9ID",
    triggerAction: 'all',
    lazyRender:true,
    mode: local5,
    fieldLabel:'Department',
    store: p9Store,
    valueField: 'ID',
    displayField: 'Name'
});


//Schedule versions Combo


var p9Versions = new Ext.form.ComboBox({
    typeAhead: true,
    id:'p9Versions',
    name:"p9Versions",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Select Version',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'ver1','Version1'], [2, 'ver2', 'Version2']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText'

});



var p9Report = new Ext.form.FormPanel({
    id: 'p9Report',
    name:'p9Report',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:350,
    url: 'getP9.php',
    items: [{
        xtype: 'textfield',
        id: 'p9PNo',
        fieldLabel: 'Payroll No',
        allowBlank: false,
        msgTarget:'side'
    },
    p9DeptCombo,p9Versions],
    buttons: [{
        text: 'Open Form',
        handler: function() {
            var p9PNo=document.getElementById('p9PNo').value;
            var p9dept=document.getElementById('deptP9ID').value;
            var p9vers=document.getElementById('p9Versions').value;

            tabPanel.add('p9sreport');
            tabPanel.doLayout();
            tabPanel.setActiveTab('p9sreport');
            if(p9vers=='Version1'){
                //                  Ext.Msg.alert('thanks', 'Version clicked '+vers);
                p9sreport.load({
                    url:'reports/p9A.php',
                    params:{
                        schParam1:p9PNo,
                        schParam2:p9dept,
                        schParam3:p9vers
                    }
                })
            }else if(vers=='Version2'){
                //             Ext.Msg.alert('Thanks', 'Version clicked '+vers);
                p9sreport.load({
                    url:'reports/p9Ahosp.php',
                    params:{
                        schParam1:pid,
                        schParam2:p9dept,
                        schParam3:p9vers
                    }
                })
            }else{
                Ext.Msg.alert('Please select a version', 'Version '+vers);
            }
            winP9.hide();
        //                                }else{
        //                                    Ext.Msg.alert('Invalid Date', 'schedule for the month of '+slipMnth+' is not Available');
        //                                }
        }
    },
    {
        text: 'Cancel',
        handler: function() {
            winP9.hide();
        }
    }]
})

var submitHandler3 = function() {
    window.open('reports/p9A.php');
}

var submitHandler4 = function() {
    window.open('reports/p9Ahosp.php');
}

var buttons3 = [
{
    text    : 'Print Report',
    handler : submitHandler3
}
];


var buttons4 = [
{
    text    : 'Print Report2',
    handler : submitHandler4
}
];

var p9sreport=new Ext.Panel({
    title:'P9A REPORT',
    id:'p9sreport',
    height:600,
    width:300,
    autoScroll:true,
    buttons: buttons3
});


winP9 = new Ext.Window({
    applyTo:'p9Win',
    layout:'fit',
    title:'proll P9 Report',
    id:'proll_p9report',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
     closeAction:'hide',
    width:400,
    height:400,
    items: [p9Report]

});

//----------------------------------------------------------------------------------------------------------------------
//End of P9 Report code
//=======================================================================================================================


                var viewPort=new Ext.Viewport({
                    layout: 'border',
                    items: [{
                            region: 'north',
                            autoHeight: true,
                            border: false,
                            margins: '0 0 5 0'
                        }, {
                            region: 'west',
                            collapsible: true,
                            title: 'Navigation',
                            xtype: 'treepanel',
                            width: 200,
                            autoScroll: true,
                            split: true,
                            dataUrl: 'menus.php',
                            singleExpand:true,
                            root: {
                                nodeType: 'async',
                                text: 'Payroll Menus',
                                draggable: false,
                                id: 'src'
                            },
                            rootVisible: true,
                            listeners: {
                                click: function(n) {
                                    //Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                    if(n.attributes.id=='processProll'){
                                        //Ext.MessageBox.prompt('Process', 'Please enter Password:', showResultText);
                                        win.show();
                                    }else if(n.attributes.id=='paySlips'){
                                        //                                        Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                        winSlip.show();
                                    }else if(n.attributes.id=='proll_Schedule'){
                                        //                                        Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                        winSchedule.show();
                                    }
                                    else if(n.attributes.id=='prollBanklist'){
                                        //                                        Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                        winBankList.show();
                                    }else if(n.attributes.id=='pstReports'){
                                        //                                        Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                        winPST.show();
                                    }else if(n.attributes.id=='trialbalance'){
                                        //                                        Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                        winTrial.show();
                                    }else if(n.attributes.id=='lstReports'){
                                        //                                        Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
                                        winLST.show();
                                    }
                                    else if(n.attributes.id=='p9Report'){
                                        //                                      Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.id + '"');
//                                        getP9();
                                        winP9.show();
                                    }
                                    else{
                                        tabPanel.add(n.attributes.id);
                                        tabPanel.doLayout();
                                        tabPanel.setActiveTab(n.attributes.id);

//                                          tabPanel.add({
//                                            title:n.attributes.id,
//                                            active: true,
//                                            closable:true,
//                                            closeAction:'hide'
//                                        });
//                                    }
                                       
                                    }
                                }
                            }
                        }, {
                            region: 'center',
                            items: [tabPanel]
                        }]
                });
                viewPort.render(document.body);
            });
        </script>
    </head>
    <body>
        <div id="menus"></div>
        <div id="hello-win" class="x-hidden">

        </div>
        <div id="payslipWin" class="x-hidden">

        </div>
         <div id="postingsWin" class="x-hidden">

        </div>
         <div id="listingsWin" class="x-hidden">

        </div>
        <div id="scheduleWin" class="x-hidden">

        </div>
        <div id="banklistWin" class="x-hidden">

        </div>
         <div id="p9Win" class="x-hidden">
        </div>
         <div id="trialWin" class="x-hidden">
        </div>
    </body>
</html>
