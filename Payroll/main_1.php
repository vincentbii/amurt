<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css">
        <link rel="stylesheet" type="text/css" href="xml-tree-loader.css" />
        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css" />

        <!-- overrides to base library -->
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/css/CenterLayout.css" />

        <!-- page specific -->
        <link rel="stylesheet" type="text/css" href="layout-browser.css">

        <!-- ** Javascript ** -->
        <!-- ExtJS library: base/adapter -->
        <script type="text/javascript" src="../include/Extjs/adapter/ext/ext-base.js"></script>

        <!-- ExtJS library: all widgets -->
        <script type="text/javascript" src="../include/Extjs/ext-all.js"></script>

        <!-- overrides to base library -->

        <!-- extensions -->
        <script type="text/javascript" src="../include/Extjs/ux/CenterLayout.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/RowLayout.js"></script>

        <script type="text/javascript" src="../include/Extjs/ux/XmlTreeLoader.js"></script>
        <script type="text/javascript" src="departments.js"></script>
        <script type="text/javascript" src="basic.js"></script>
        <script type="text/javascript" src="register.js"></script>


        <script type="text/javascript" >

            Ext.BLANK_IMAGE_URL="../include/Extjs/resources/images/default/s.gif";
            Ext.onReady(function(){

                Ext.QuickTips.init();

                // This is an inner body element within the Details panel created to provide a "slide in" effect
                // on the panel body without affecting the body's box itself.  This element is created on
                // initial use and cached in this var for subsequent access.
                var detailEl;

                // This is the main content center region that will contain each example layout panel.
                // It will be implemented as a CardLayout since it will contain multiple panels with
                // only one being visible at any given time.
                var contentPanel = new Ext.Panel({
                    title:'display content',
                    id: 'content-panel',
                    region: 'center', // this is what makes this panel into a region within the containing layout
                    margins: '2 5 5 0'
//                    layout:'card'

                    //                    items: [
                    //                        // from basic.js:
                    //                        departments,start, contactForm,company
                    //                    ]
                });

                var Tree = Ext.tree;

                var tree = new Tree.TreePanel({
                    region:'north',
                    useArrows: true,
                    height: 300,
                    minSize: 150,
                    autoScroll: true,

                    animate: true,
                    enableDD: true,
                    containerScroll: true,
                    border: false,
                    // auto create TreeLoader
                    dataUrl: 'menus.php',

                    root: {
                        nodeType: 'async',
                        text: 'Ext JS',
                        draggable: false,
                        id: 'src'
                    },
                        listeners: {
                                click: function(n) {
                                    Ext.Message.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.text + '"');
                                }
                            }
                });

                tree.getRootNode().expand();
               
                var detailsPanel = {
                    id: 'details-panel',
                    title: 'Details',
                    region: 'center',
                    bodyStyle: 'padding-bottom:15px;background:#eee;',
                    autoScroll: true,
                    html: '<p class="details-info">When you select a layout from the tree, additional details will display here.</p>'
                };

                // Finally, build the main layout once all the pieces are ready.  This is also a good
                // example of putting together a full-screen BorderLayout within a Viewport.
                new Ext.Viewport({
                    layout: 'border',
                    title: 'Care2x Payroll',
                    items: [{
                            xtype:'box',
                            region: 'north',
                            applyTo: 'header'
                        },
                        {
                            layout:'border',
                            title:'Main Menu',
                            region:'west',
                            margins: '2 0 5 5',
                            width: 200,
                            minSize: 100,
                            maxSize: 500,
                            items: [tree,detailsPanel]
                        },{
                            layout:'border',
                            region:'center',
                            items:[contentPanel]

                        }],
                    renderTo: Ext.getBody()

                });
                // store.load();

            });
        </script>
    </head>
    <body>
        <div id="header"><h1>Care2x payroll</h1></div>
        <div style="display:none;">

            <!-- Start page content -->
            <div id="start-div">
                <div style="float:left;" ></div>
                <div style="margin-left:100px;">
                    <h2>Welcome!</h2>

                    <p>Care2x Paroll System.</p>
                </div>
            </div>
            <!-- Basic layouts -->
            <div id="absolute-details">
                <h2>Absolute Details</h2>

            </div>

            <div id="contactForm-details">
                <h2>Payroll Employees Register</h2>

            </div>

            <div id="departments-details">
                <h2>Departments</h2>

            </div>
            <div id="company-details">
                <h2>Departments</h2>
            </div>
            <div id="container">
            </div>
        </div>
    </body>
</html>
