<?php

echo "[{
    text:'Main Menu',
    expanded: true,
    children:[{
        text:'Employees',
        id:'employees',
        leaf:true
    },{
        text:'Process Payroll',
        id:'prollPayments',
        leaf:true
    },{
        text:'Post Payroll',
        id:'processProll',
        leaf:true
    },{
        text:'Employee Payments',
        id:'payments',
        leaf:true
    }]
},{
    text:'Admin',
    children:[{
        text:'Company',
        id:'company',
        leaf:true
    },{
        text:'Pay Types',
        id:'paytypes',
        leaf:true
    },{
        text:'Departments',
        id:'departments',
        leaf:true
    },{
        text:'Rates',
        id:'rates',
        leaf:true
    },{
        text:'Banks',
        id:'banks',
        leaf:true
    },{
        text:'Loans',
        id:'loans',
        leaf:true
    },{
        text:'Leaves',
        id:'leaves',
        leaf:true
    },{
        text:'Over Time',
        id:'overtime',
        leaf:true
    }]
},{
    text:'Reports',
    children:[{
        text:'Payslips',
        id:'paySlips',
        leaf:true
    },
   {
        text:'Payroll Posting',
        id:'pstReports',
        leaf:true
    },
    {
        text:'Bank Accounts',
        id:'bankAccounts',
        leaf:true
    },{
        text:'Payroll Listing',
        id:'lstReports',
        leaf:true
    },{
        text:'Trial Balance',
        id:'trialbalance',
        leaf:true
    },{
        text:'P9A Report',
        id:'p9Report',
        leaf:true
    },{
        text:'P9A(Hosp) Report',
        id:'p9AHospReport',
        leaf:true
    },{
        text:'NSSF Return',
        id:'nssfReturn',
        leaf:true
    },{
        text:'PAYE Return',
        id:'payeReturn',
        leaf:true
    },{
        text:'NHIF Return',
        id:'nhifReturn',
        leaf:true
    },{
        text:'Bank List',
        id:'prollBanklist',
        leaf:true
    },{
        text:'Monthly Earnings',
        id:'mEarnings',
        leaf:true
    },{
        text:'Monthly Deducations',
        id:'mDeductions',
        leaf:true
    },{
        text:'Monthly Department Totals',
        id:'mDetpTotals',
        leaf:true
    },{
        text:'Monthly Organisation Totals',
        id:'mOrgTotals',
        leaf:true
    },{
        text:'Employees List',
        id:'empList',
        leaf:true
    },{
        text:'Earnings Vs Deducations',
        id:'ernVSded',
        leaf:true
    },{
        text:'Cooperative Deducations',
        id:'mOrgTotals',
        leaf:true
    }]
}]"
?>