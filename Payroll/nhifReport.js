/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var fm=Ext.form;

var nhifStore = new Ext.data.JsonStore({
    proxy: new Ext.data.HttpProxy({
        //where to retrieve data
        url: 'getNhiflist.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
//        strMonth:dt.format(Date.patterns.monthName)
    },
    //    url: 'getPayments.php',p.`Pid`, p.`emp_names`,p.`pay_type`, p.`amount`,e.bank_name,e.account_no
    root: 'NhifList',
    fields: ['pid', 'emp_names','pay_type','amount','ID_no','nhif_no','payMonth']
});


var filters3 = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'PID'
    }, {
        type: 'string',
        dataIndex: 'emp_names',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'pay_type'
    }, {
        type: 'string',
        dataIndex: 'amount'
    }, {
        type: 'string',
        dataIndex: 'ID_no'
    }, {
        type: 'string',
        dataIndex: 'nhif_no'
    }, {
        type: 'string',
        dataIndex: 'payMonth'
    }]
});


var nhifColModel=function(finish,start){
    var columns = [
    {
        id:'pid',
        header: "PID",
        width: 55,
        sortable: true,
        dataIndex: 'pid',
        filterable: true
    },
    {
        header: "Names",
        width: 200,
        sortable: true,
        dataIndex: 'emp_names',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Pay Type",
        width: 80,
        sortable: true,
        dataIndex: 'pay_type',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Amount",
        width: 80,
        sortable: true,
        dataIndex: 'amount'
    },
    {
        header: "ID No",
        width: 200,
        sortable: true,
        dataIndex: 'ID_no',
        filter: {
            type: 'string'
        }
    },
    {
        header: "NHIF No",
        width: 80,
        sortable: true,
        dataIndex: 'nhif_no'
    },
    {
        header: "Month",
        width: 80,
        sortable: true,
        dataIndex: 'payMonth'
    },
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}



var nhifReturn=new Ext.Panel({
    title:'NHIF Return',
    id:'nhifReturn',
    height:600,
    width:500,
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            xtype: 'grid',
            id:'grid',
            store: nhifStore,
            colModel: nhifColModel(7),
            tbar: [{
                text: 'Update',
                iconCls:'remove'
            } ,{
                xtype: 'exportbutton',
                store: nhifStore,
                title:"NSSF Report"

            }, '->', // next fields will be aligned to the right

            {
                text: 'Refresh',
                tooltip: 'Click to Refresh the table',
                handler: refreshGrid,
                iconCls:'refresh'
            },{
                text: 'Clear Filter Data',
                handler: function () {
                    grid.filters.clearFilters();
                }
            },{
                text: 'Reconfigure Grid',
                handler: function () {
                    grid.reconfigure(nhifStore, nhifColModel(6));
                }
            }],
            height: 600,
            title:'NHIF List',
            border: true,
            listeners: {
                render: {
                    fn: function(g){
                        nhifStore.load({
                            params: {
                                start: 0,
                                limit: 10
                            }
                        });
                        g.getSelectionModel().selectRow(0);
                            delay: 10
                    }

                }
            // Allow rows to be rendered.
            },

            plugins: [filters3],
            bbar: new Ext.PagingToolbar({
                store: nhifStore,
                pageSize: 10,
                plugins: [filters3]
            }
            )

        }
    }]
});





nhifStore.load();


//var nhifRetun=new Ext.Panel({
//    title:'NHIF Returns',
//    id:'nhifReturn',
//    height:600,
//    width:300,
//    html:'test'
//});