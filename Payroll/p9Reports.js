/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//var p9Handler = function() {
//    window.open('reports/p9.php');
//
//}
//var buttons = [
//{
//    text    : 'Submit',
//    handler : p9Handler
//}
//];
//
//var p9Ahosp=new Ext.Panel({
//    title:'P9A (Hosp) Reports',
//    id:'p9AHospReport',
//    height:600,
//    width:300,
//    autoScroll:true,
//    autoLoad:{
//        url:'reports/p9Ahosp.php'
//    },
//    buttons: buttons
//});
//
//var p9=new Ext.Panel({
//    title:'P9A Reports',
//    id:'p9Report',
//    height:600,
//    width:300,
//    autoScroll:true,
//    autoLoad:{
//        url:'reports/p9A.php'
//    },
//    buttons: buttons
//});

 function getP9(){

var local4 = true;


//Schedule Departments
var p9Store=new Ext.data.JsonStore({
    url: 'getSchedule.php',
    root: 'getSchDepts',

    id: 'ID',//
    baseParams:{
        task: "getSchDepts"
    },//This
    fields:['ID','Name']
});
p9Store.load();
//
var p9DeptCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'deptP9ID',
    name:"deptSP9ID",
    triggerAction: 'all',
    lazyRender:true,
    mode: local4,
    fieldLabel:'Department',
    store: p9Store,
    valueField: 'ID',
    displayField: 'Name'
});


//Schedule versions Combo


var p9Versions = new Ext.form.ComboBox({
    typeAhead: true,
    id:'p9Versions',
    name:"p9Versions",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Select Version',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'ver1','Version1'], [2, 'ver2', 'Version2']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText'

});



var p9Report = new Ext.form.FormPanel({
    id: 'p9Report',
    name:'p9Report',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:350,
    url: 'getP9.php',
    items: [{
        xtype: 'textfield',
        id: 'pNo',
        fieldLabel: 'Payroll No',
        allowBlank: false,
        msgTarget:'side'
    },
    p9DeptCombo,p9Versions],
    buttons: [{
        text: 'Open Form',
        handler: function() {
            var schMnth=document.getElementById('pNo').value;
            var dept=document.getElementById('deptP9ID').value;
            var vers=document.getElementById('p9Versions').value;

            tabPanel.add('pSchedule');
            tabPanel.doLayout();
            tabPanel.setActiveTab('p9sreport');
            if(vers=='Version1'){
                //                  Ext.Msg.alert('thanks', 'Version clicked '+vers);
                p9sreport.load({
                    url:'reports/p9A.php',
                    params:{
                        schParam1:schMnth,
                        schParam2:dept,
                        schParam3:vers
                    }
                })
            }else if(vers=='Version2'){
                //             Ext.Msg.alert('Thanks', 'Version clicked '+vers);
                p9sreport.load({
                    url:'reports/p9Ahosp.php',
                    params:{
                        schParam1:schMnth,
                        schParam2:dept,
                        schParam3:vers
                    }
                })
            }else{
                Ext.Msg.alert('Please select a version', 'Version '+vers);
            }
            winP9.hide();
        //                                }else{
        //                                    Ext.Msg.alert('Invalid Date', 'schedule for the month of '+slipMnth+' is not Available');
        //                                }
        }
    },
    {
        text: 'Cancel',
        handler: function() {
            winP9.hide();
        }
    }]
})

var submitHandler3 = function() {
    window.open('reports/p9Ahosp.php');
}

var submitHandler4 = function() {
    window.open('reports/p9Ahosp.php');
}

var buttons3 = [
{
    text    : 'Print Report',
    handler : submitHandler3
}
];


var buttons4 = [
{
    text    : 'Print Report2',
    handler : submitHandler4
}
];

var p9sreport=new Ext.Panel({
    title:'Payroll Schedule',
    id:'p9sreport',
    height:600,
    width:300,
    autoScroll:true,
    buttons: buttons3
});


winP9 = new Ext.Window({
    applyTo:'p9Win',
    layout:'fit',
    title:'proll P9 Report',
    id:'proll_p9report',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:400,
    height:400,
    items: [p9Report]

});

    winP9.show();
  }



