/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// shorthand alias
var fm = Ext.form;
var primaryKey='ID'; //primary key is used several times throughout
// custom column plugin example
var selectedKeys;
var ratecm = new Ext.grid.ColumnModel(
    [
    {
        id: 'ID',
        header: 'ID',
        dataIndex: 'ID',
        width: 100
    },{
        header: 'rate name',
        dataIndex: 'Rate_Name',
        width: 100,
        editor: new fm.TextField({
            allowBlank: false
        })
    },
    {
        header: 'Lower_Limit',
        dataIndex: 'Lower_Limit',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },
    {
        header: 'Upper_Limit',
        dataIndex: 'Upper_Limit',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },{
        header: 'Value',
        dataIndex: 'Value',
        width: 130,
        editor: new fm.TextField({
            allowBlank: false
        })
    }
    ]);

// by default columns are sortable
ratecm.defaultSortable = true;
// specify a jsonReader (coincides with the XML format of the returned data)
var ratereader= new Ext.data.JsonReader(
{
    root: 'PayRates',
     id: 'ID'//
},
[
{
    name: 'ID',
    type:'numeric'
},
{
    name: 'Rate_Name',
    type: 'string'
},
{
    name: 'Lower_Limit',
    type: 'string'
},
{
    name: 'Upper_Limit',
    type: 'string'
},
{
    name: 'Value',
    type: 'string'
}
]
)
// create the Data Store
var ratestore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        //	        	url: 'sheldon.xml', //where to retrieve data
        url: 'getRates.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        task: "readRates"
    },//This
    reader:ratereader,
    sortInfo: {
        field:'Rate_Name',
        direction:'ASC'
    },
    groupField:'Rate_Name'
});

var editor = new Ext.ux.grid.RowEditor({
    saveText: 'Update'
});


var rategrid = new Ext.grid.EditorGridPanel({
    store: ratestore,
    cm: ratecm,
    url:'process_Rates_Requests',
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    width: 600,
    height: 500,
    //autoExpandColumn: 'type',
    title: 'Edit Rates?',
    frame: true,
    clicksToEdit: 2,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    stripeRows: true,
    tbar: [{
        text: 'Add Rates',
        iconCls:'add',
        handler : function(){
            // access the Record constructor through the grid's store
            var ptype =rategrid.getStore().recordType;
            var p = new ptype({
                ID:0,
                Rate_Name: 'rate',
                Lower_Limit: 0,
                Upper_Limit: 0,
                Value: 0
            });
            rategrid.stopEditing();
            ratestore.insert(0, p);
            rategrid.startEditing(0, 1);
        }
            
    },'-',{
        text: 'Delete Rates',
        iconCls:'remove',
        handler :handleDelete
    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid,
        iconCls:'refresh'
    }]
});
ratestore.load();
rategrid.addListener('afteredit', handleEdit);

function refreshGrid() {
    ratestore.reload();//
}; // end refresh


function handleEdit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;

    //start the process to update the db with cell contents
    updateDB(editEvent);

}

function handleDelete() {

    //returns array of selected rows ids only
    var selectedKeys = rategrid.selModel.selections.keys;
    if(selectedKeys.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to delete selection?', deleteRecord);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
}; // end handleDelete
        
function updateDB(oGrid_Event) {

    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
    } else
{
        var fieldValue = oGrid_Event.value;
    }

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {   //Specify options (note success/failure below that
        //receives these same options)
        waitMsg: 'Saving changes...',
        //url where to send request (url to server side script)
        url: 'getRates.php',

        //If specify params default is 'POST' instead of 'GET'
        //method: 'POST',

        //params will be available server side via $_POST or $_REQUEST:
        params: {
            task: "update", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used

            //For existing records this is the unique id (we need
            //this one to relate to the db). We'll check this
            //server side to see if it is a new record
            keyID: oGrid_Event.record.data.ID,

            //For new records Ext creates a number here unrelated
            //to the database
            //            bogusID: oGrid_Event.record.id,

            field: oGrid_Event.field,//the column name
            value: fieldValue,//the updated value

            //The original value (oGrid_Event.orginalValue does
            //not work for some reason) this might(?) be a way
            //to 'undo' changes other than by cookie? When the
            //response comes back from the server can we make an
            //undo array?
            originalValue: oGrid_Event.record.modified

        },//end params

        //the function to be called upon failure of the request
        //(404 error etc, ***NOT*** success=false)
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        //ds.rejectChanges();//undo any changes
        },//end failure block

        //The function to be called upon success of the request
        success:function(response,options){
            //Ext.MessageBox.alert('Success','Yeah...');


            //if this is a new record need special handling
            if(oGrid_Event.record.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server

                //Extract the ID provided by the server
                var newID = responseData.newID;
                //oGrid_Event.record.id = newID;

                //Reset the indicator since update succeeded
                oGrid_Event.record.set('newRecord','no');

                //Assign the id to the record
                oGrid_Event.record.set('ID',newID);
                //Note the set() calls do not trigger everything
                //since you may need to update multiple fields for
                //example. So you still need to call commitChanges()
                //to start the event flow to fire things like
                //refreshRow()

                //commit changes (removes the red triangle which
                //indicates a 'dirty' field)
                ratestore.commitChanges();

            //var whatIsTheID = oGrid_Event.record.modified;

            //not a new record so just commit changes
            } else {
                //commit changes (removes the red triangle
                //which indicates a 'dirty' field)
                ratestore.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
}; //end updateDB

function deleteRecord(btn) {
    if(btn=='yes')
    {

        //returns record objects for selected rows (all info for row)
        var selectedRows = rategrid.selModel.selections.items;
        //returns array of selected rows ids only
        var selectedKeys = rategrid.selModel.selections.keys;
        //encode array into json
         Ext.MessageBox.alert('OK',selectedKeys)
        var encoded_keys = Ext.encode(selectedKeys);
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getRates.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "delete", //pass task to do to the server script
                ID: encoded_keys,//the unique id(s)
                key: primaryKey//pass to server same 'id' that the reader used
            },

            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);

                    Ext.MessageBox.alert('OK',json.del_count + ' record(s) deleted.');

                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                ratestore.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    };//end if click 'yes' on button
}; // end deleteRecord

var payRates=new Ext.Panel({
    title:'Payment Rates',
    id:'rates',
    minWidth:400,
    minHeight:400,
    items:[rategrid]
});
