<?php

/* Care2x Payroll deployment 01-01-2010
 * GNU General Public License
 * Copyright 2010 George Maina
 * georgemainake@gmail.com
 *
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
$slipmont = $_POST[slipMonth];
$sql1 = 'SELECT * FROM proll_company';
$result1 = $db->Execute($sql1);
$coRow = $result1->FetchRow();

$paymonth = $_POST['param1'];
$spid = $_POST['param3'];
$spid2 = $_POST['param4'];
//$month1 = date_create($_POST['param4']);
//$month2 = date_create($_POST['param5']);
$maxSlips = 2;//$_POST['param2'];
$dept=$_POST['param6'];
$branch=$_POST['param7'];


$sql = "SELECT distinct p.Pid, CONCAT(p.surname ,' ', p.firstname ,' ', p.lastname) AS empnames,
p.EmpBranch,p.department,p.basicpay,c.payMonth,p.pin_no,p.id,c.payDate FROM proll_empRegister p
left join proll_payments c on p.PID=c.Pid where c.payMonth= '$paymonth'";

if ($spid <> ''  && $spid2 =='' ) {
    $sql.= " and p.pid='$spid'";
}

if ($spid <> '' && $spid2 <> '') {
    $sql.= " and p.pid between '$spid' and '$spid2'";
}

if ($dept <> '' ) {
    $sql.= " and p.department='$dept'";
}

if ($branch <> '' ) {
    $sql.= " and p.branch='$branch'";
}

$sql.=' order by p.Pid asc';
//echo $sql;
$result2 = $db->Execute($sql);
$numRows = $result2->RecordCount();

echo "<table border=0 width=100%>";

$max = $maxSlips;
echo "<tr>";
$position = 1;
while ($row2 = $result2->FetchRow()) {

    $coName = strtoupper($coRow['CompanyName']);
    $pid = $row2['Pid'];
    $empnames = $row2['empnames'];
    $payMonth = $row2['payMonth'];
    $dept = $row2['department'];
    $pn_no = $row2['pin_no'];
    $id = $row2['id'];
    $branch = $row2['branch'];
    $payDates = date_create($row2['payDate']);
    $payDate = date_format($payDates, "Y");
    if ($position == 1) {
        echo "<tr>";
    }

    echo '<td valign=top>';
    displaySlip($coName, $pid, $empnames, $pno, $branch,$dept, $pn_no, $payMonth, $id, $payDate);
    echo '</td>';

    if ($position == $max) {
        echo "</tr> ";
        $position = 1;
    } else {
        $position++;
    }
}
//$end = "";
//if ($position != 1) {
//    for ($z = (3 - $position); $z > 0; $z--) {
//        $end .= "<td></td>";
//    }
//    $end .= "</tr>";
//}

echo '</table>';

function displaySlip($coName, $pid, $empnames, $pno, $branch,$dept, $pn_no, $payMonth, $id, $payDate) {
    global $db;
    echo "<table border=0 cellpadding=2>";
    echo '<tr><td colspan=2><b>' . $coName . '</b></td></tr>';
    echo "<tr><td>Employee   :</td><td><b>" . $pid . ' : ' . $empnames . '</b></td></tr>';
    echo '<tr><td>Period    :</td><td><b>' . $payMonth . ' ' . $payDate . '</b></td></tr>';
    echo '<tr><td>Branch :</td><td><b>' . $branch . '</b></td></tr>';
    echo '<tr><td>Department :</td><td><b>' . $dept . '</b></td></tr>';
    echo '<tr><td>Pin_No     :</td><td><b>' . $pn_no . '</b></td></tr>';
    echo '<tr><td colspan=2>--------------------------------------</td></tr>';
    echo "</table>";

    echo '<table border=0>';
//Earnings
    echo '<tr><td cospan=2><b>Earnings</b><br></td><td></td><td align=center>Bal</td>';
    $sql = 'select a.Pid, a.emp_names,c.id,a.pay_type,a.amount,a.Notes FROM proll_payments a
            inner join proll_paytypes b on a.pay_type=b.PayType
            inner join proll_paycategory c on b.CatID=c.ID
            where a.catid in("pay","Relief","benefit") and amount>0 and pid="' . $pid . '" and a.paymonth="' . $payMonth . '"';
    //echo $sql;
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();

    while ($row = $result->FetchRow()) {
        echo '<tr><td align=left>' . $row[3] . '</td><td align=right>' . number_format($row[4],2) . '</td></tr>';
    }
 $sql = 'select pid,sum(amount) as grosspay from proll_payments where catID IN("pay","benefit") 
        and pid="' . $pid . '" and paymonth="' . $payMonth . '"  AND pay_type<>"PENSION" ';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    $sumRows = $result->FetchRow();
    $gloss= $sumRows[1];
    echo '<tr><td align=left>Gross Pay</td><td align=right>' . number_format($gloss,2) . '</td></tr>';
//Tax Calculation
    echo '<tr><td cospan=2><br><b>Tax Details</b><br></td>';
     $sql = 'select pid,sum(amount) as grosspay from proll_payments where catID IN("pay","benefit") and pay_type<>"PENSION" 
         and pid="' . $pid . '" and paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    $sumRows = $result->FetchRow();
    echo '<tr><td align=left>TAXABLE PAY</td><td align=right>' . number_format($sumRows[1],2) . '</td></tr>';
    
    $sql = 'SELECT a.Pid, a.emp_names,a.pay_type,a.amount,a.Notes FROM proll_payments a
WHERE  pid="' . $pid . '" AND a.paymonth="' . $payMonth . '" AND pay_type IN("N.S.S.F","Pension")';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    while($numRows = $result->FetchRow()){
        echo '<tr><td align=left>' . $numRows[2] . '</td><td align=right>' . number_format($numRows[3],2) . '</td><td></td></tr>';
    }
    
    $pyesql = 'select amount from proll_payments where pid="' . $pid . '" and pay_type="paye" and paymonth="' . $payMonth . '"';
    $pyeresult = $db->Execute($pyesql);
    $pyerow = $pyeresult->FetchRow();
    $txCharged=$pyerow[0] ;
    echo '<tr><td align=left>Tax Charged</td><td align=right>' . number_format($pyerow[0],2) . '</td><td></td></tr>';
    
     $pyesql = 'select amount from proll_payments where pid="' . $pid . '" and pay_type="Personal Relief" and paymonth="' . $payMonth . '"';
    $pyeresult = $db->Execute($pyesql);
    $pyerow = $pyeresult->FetchRow();
    $txRelief=$pyerow[0];
    echo '<tr><td align=left>Tax Relief</td><td align=right>' . number_format($pyerow[0],2) . '</td><td></td></tr>';
    echo '<tr><td align=left>Tax Deducted</td><td align=right>' . number_format(($txCharged-$txRelief),2) . '</td><td></td></tr>';
//Deductions

    echo '<tr><td cospan=3><br><b>Deductions</b><br></td>';
    $sql = ' select a.Pid, a.emp_names,c.id,a.pay_type,a.amount,a.Notes,a.balance FROM proll_payments a
inner join proll_paytypes b on a.pay_type=b.PayType
inner join proll_paycategory c on b.CatID=c.ID
where a.catid="deduct" and amount>0 and pid="' . $pid . '" and a.paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();

    while ($row = $result->FetchRow()) {
        $balance=$row[6];
        if(empty($balance)||$balance==0){
                $balance='';
            }else{
                $balance=number_format($balance,2);
            }
//           $balance=$row[6]?$row[6]:"";
           
        echo '<tr><td align=left>' . $row[3] . '</td><td align=right>' . number_format($row[4],2) . '</td><td align=right>&nbsp;&nbsp;&nbsp;' . $balance. '</td></tr>';
    }
    echo '<tr><td align=left>P.A.Y.E</td><td align=right>' .number_format(($txCharged-$txRelief),2) . '</td><td></td></tr>';
    //Summary
    echo '<tr><td cospan=2><br><b>Summary</b><br></td>';
   

    $sql = 'select pid,sum(amount) as deductions from proll_payments where catID IN("Deduct") and pid="' . $pid . '" and
        paymonth="' . $payMonth . '" AND pay_type NOT IN ("paye","Personal Relief")';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    $diffRows = $result->FetchRow();
    $tx=$txCharged-$txRelief;
    $deductions=$diffRows[1]+$tx;
    echo '<tr><td align=left>Less Deductions</td><td align=right>' . number_format($deductions,2) . '</td></tr>';

    echo '<tr><td align=left>Net pay</td><td align=right>' . number_format(($sumRows[1] - $deductions),2) . '</td></tr>';

    echo '<tr><td cospan=2><br><br></td>';
    $sql = 'select BankID,Account_no from proll_empregister where pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $AcRows = $result->FetchRow();
    echo '<tr><td align=left>PAYMENT BY:</td><td align=left>Bank</td></tr>';
    echo '<tr><td align=right>:</td><td align=left>' . $AcRows[0] . '</td></tr>';
    echo '<tr><td align=right>:</td><td align=left>' . $AcRows[1] . '</td></tr>';

    echo '</table><br><br>';
    echo '-------------------------------------------------------------------<br>';
    echo '-------------------------------------------------------------------<br><br>';
}

?>
