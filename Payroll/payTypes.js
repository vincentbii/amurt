/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// shorthand alias
var fm = Ext.form;
var primaryKey='catID'; //primary key is used several times throughout
// custom column plugin example
var selectedKeys;
// custom column plugin example
var createPaytypesWin;
var checkColumn = new Ext.grid.CheckColumn({
    header: 'Contribution?',
    dataIndex: 'Contribution',
    width: 80
});

var fixedColumn = new Ext.grid.CheckColumn({
    header: 'Fixed?',
    dataIndex: 'fixed',
    width: 55
});


var addEmpPayTypes = new Ext.FormPanel({
    id: 'addEmpPayTypes',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:450,
    url: 'getEmpPayments.php',
    items: [{
        xtype: 'textfield',
        id: 'pcode',
        fieldLabel: 'Code',
        allowBlank: false,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vpid = this.el.getValue();
            if(vpid === this.emptyText || vpid === undefined){
                vpid = '';
            }
            return vpid;
        }

    },{
        xtype: 'textfield',
        id: 'description',
        fieldLabel: 'Description',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },{
        xtype: 'textfield',
        id: 'catID',
        fieldLabel: 'Category',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },{
        xtype: 'textfield',
        id: 'interest',
        fieldLabel: 'Interest',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },{
        xtype: 'textfield',
        id: 'interestCode',
        fieldLabel: 'Interest Code',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },{
        xtype: 'textfield',
        id: 'interestDesc',
        fieldLabel: 'Interest Description',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },{
        xtype: 'textfield',
        id: 'glCode',
        fieldLabel: 'GL Account',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    },{
        xtype: 'textfield',
        id: 'glDesc',
        fieldLabel: 'Account Descripion',
        allowBlank: true,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vtime = this.el.getValue();
            if(vtime === this.emptyText || vtime === undefined){
                vtime = '';
            }
            return vtime;
        }
    }],
    buttons: [{
        text: 'Save',
        handler: function() {
            var pid = addEmpPayments.getForm().findField("pid4").getValue();
            var catId=addEmpPayments.getForm().findField('catID').getValue();
            var PayID=addEmpPayments.getForm().findField('payID').getValue();
            var amount=addEmpPayments.getForm().findField('Amount').getValue();
            var balance=addEmpPayments.getForm().findField('Balance').getValue();
            Ext.Ajax.request({
                url: 'getEmpPayments.php',
                method: 'POST',
                params: {
                    pid:pid,
                    catId:catId,
                    PayID:PayID,
                    amount:amount,
                    balance:balance,
                    task:'addEmpPays'
                },
                waitMsg:'Saving Data...',
                success: function (form, action) {
                    //                    Ext.MessageBox.alert('Message', 'Saved OK');
                    //                    refreshGrid2();
                    createPaytypesWin.hide();
                      
                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                }
            });

        }
    },
    {
        text: 'Close',
        handler: function() {
            createPaytypesWin.hide();
            refreshGrid2();
        }
    }]
})

createPaytypesWin = new Ext.Window({
    title:'Add A Pay Type',
    //                    applyTo:'payslipWin',
    layout:'fit',
    id: 'createPaytypesWin',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:450,
    height:350,
    closable:false,
    items: [addEmpPayTypes]

});



dsCategory = new Ext.data.Store({
    proxy: new Ext.data.HttpProxy({
        //where to retrieve data
        url: 'getPayTypes.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        task: "readCat"
    },//this parameter is passed for any HTTP request
    reader:  new Ext.data.JsonReader(
    {
        root: 'results',//name of the property that is container for an Array of row objects
        id: 'ID'//the property within each row object that provides an ID for the record (optional)
    },
    [
    {
        name: 'ID'
    },//name of the field in the payCategory table (not the PayTypes table)

    {
        name: 'catName'
    }
    ]
    ),
    sortInfo:{
        field: 'ID',
        direction: "ASC"
    }
}
);//End to Pay Category
dsCategory.load();

// the column model has information about grid columns
// dataIndex maps the column to the specific data field in
// the data store (created below)
var cm = new Ext.grid.ColumnModel(
    [
    {
        id: 'ID',
        header: 'ID',
        dataIndex: 'ID',
        width: 100,
        editor: new fm.TextField({
            allowBlank: false
        })
    },{
        dataIndex: 'catID',
        header: "Category",
        sortable: true,
        width: 150,

        //create a dropdown based on server side data (from db)
        editor: new Ext.form.ComboBox({
            //if we enable typeAhead it will be querying database
            //so we may not want typeahead consuming resources
            typeAhead: false,
            triggerAction: 'all',

            lazyRender: true,//should always be true for editor

            store: dsCategory,

            displayField: 'catName',

            valueField: 'ID'
        }),
        renderer:  //custom rendering specified inline
        function(data) {
            record = dsCategory.getById(data);
            if(record) {
                return record.data.catName;
            } else {
                //return data;
                return 'missing data';
            }
        }
    },{
        header: 'Type',
        dataIndex: 'type',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },
    fixedColumn,
    checkColumn,
    {
        header: 'Interest',
        dataIndex: 'interest',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },{
        header: 'Interest_code',
        dataIndex: 'interest_code',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },{
        header: 'interest_name',
        dataIndex: 'interest_name',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },{
        header: 'gl_acc',
        dataIndex: 'gl_acc',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },{
        header: 'GL Desc',
        dataIndex: 'gl_desc',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    },
    {
        header: 'Notes',
        dataIndex: 'notes',
        width: 200,
        editor: new fm.TextField({
            allowBlank: false
        })
    }
    ]);

// by default columns are sortable
cm.defaultSortable = true;
// specify a jsonReader (coincides with the XML format of the returned data)
var reader= new Ext.data.JsonReader(
{
    root: 'PayTypes',
    id:'ID'
},
[
{
    name: 'ID',
    type:'numeric'
},
{
    name: 'catID',
    type: 'numeric'
},
{
    name: 'type',
    type: 'string'
},
{
    name: 'fixed',
    type: 'bool'
},
{
    name: 'Contribution',
    type: 'bool'
},
{
    name: 'interest',
    type: 'string'
},
{
    name: 'interest_code',
    type: 'string'
},
{
    name: 'interest_name',
    type: 'string'
},
{
    name: 'gl_acc',
    type: 'string'
},
{
    name: 'notes',
    type: 'string'
}
]
)


// create the Data Store
var ptypestore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'getPayTypes.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        task: "PayTypes"
    },//This
    reader:reader,
    sortInfo: {
        field:'catID',
        direction:'ASC'
    },
    groupField:'catID'
});

var ptypegrid = new Ext.grid.EditorGridPanel({
    store: ptypestore,
    cm: cm,
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    width: 1000,
    height: 650,
    //autoExpandColumn: 'type',
    title: 'Edit Pay Types?',
    frame: true,
    plugins: [fixedColumn,checkColumn],
    clicksToEdit: 2,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    stripeRows: true,
    tbar: [{
        text: 'Add a pay Type',
        handler : function(){
            // access the Record constructor through the grid's store
            var ptype =ptypegrid.getStore().recordType;
            var p = new ptype({
                ID:0,
                catID: 1,
                type: 'Loan',
                Fixed: 0,
                Contribution: false,
                interest: '',
                interest_code: '',
                interest_name: '',
                gl_acc: '',
                notes: ''
            });
            ptypegrid.stopEditing();
            ptypestore.insert(0, p);
            ptypegrid.startEditing(0, 1);
        }
    },{
        text: 'Add Pay Types',
        handler : function(){
             createPaytypesWin.show();
             addEmpPayTypes.getForm().reset();
                var gridrecord = ptypegrid.getSelectionModel().getSelected();
                strPid=gridrecord.get('PID');
                addEmpPayTypes.getForm().findField("pid4").setValue(strPid);
        }
    },'-',{
        text: 'Delete Rates',
        iconCls:'remove',
        handler :handlePTypeDelete
    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshPtypeGrid,
        iconCls:'refresh'
    }, // next fields will be aligned to the right
    {
        text: 'Save',
        tooltip: 'Click to Save the Record',
        iconCls:'save',
        listeners:{
            afteredit:{
                fn:handletPtypeEdit,
                buffer:200
            }
        },
        handler:refreshPtypeGrid
    }]
});
ptypestore.load();
ptypegrid.addListener('afteredit', handletPtypeEdit);




function refreshPtypeGrid() {
    ptypestore.reload();//
} // end refresh


function handletPtypeEdit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;

    //start the process to update the db with cell contents
    updatePtype(editEvent);

}

function handlePTypeDelete() {

    //returns array of selected rows ids only
    var selectedKeys = ptypegrid.selModel.selections.keys;
    if(selectedKeys.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to delete selection?'+ptypegrid.selModel.selections.keys, deletePtype);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
} // end handleDelete

function updatePtype(oGrid_Event) {

    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
    } else
{
        var fieldValue = oGrid_Event.value;
    }

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {  
        waitMsg: 'Saving changes...',
        url: 'getPayTypes.php',
        params: {
            task: "update", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used
            keyID: oGrid_Event.record.data.ID,
            field: oGrid_Event.field,//the column name
            value: fieldValue,//the updated value
            originalValue: oGrid_Event.record.modified
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            if(oGrid_Event.record.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
                var newID = responseData.newID;
                oGrid_Event.record.set('newRecord','no');
                oGrid_Event.record.set('ID',newID);
                ptypestore.commitChanges();
            } else {
                ptypestore.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
} //end updateDB

function deletePtype(btn) {
    if(btn=='yes')
    {

        //returns record objects for selected rows (all info for row)
        var selectedRows = ptypegrid.selModel.selections.items;
        //returns array of selected rows ids only
        var selectedKeys = ptypegrid.selModel.selections.keys;
        //encode array into json
        Ext.MessageBox.alert('OK',selectedKeys)
        var encoded_keys = Ext.encode(selectedKeys);
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getPayTypes.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "delete", //pass task to do to the server script
                ID: encoded_keys,//the unique id(s)
                key: primaryKey//pass to server same 'id' that the reader used
            },

            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);

                    Ext.MessageBox.alert('OK',json.del_count + ' record(s) deleted.');

                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                ptypestore.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    }//end if click 'yes' on button
} // end deleteRecord



