/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var fm=Ext.form;

var payeStore = new Ext.data.JsonStore({
    proxy: new Ext.data.HttpProxy({
        //where to retrieve data
        url: 'getPayelist.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
//        strMonth:dt.format(Date.patterns.monthName)
    },
    //    url: 'getPayments.php',p.`Pid`, p.`emp_names`,p.`pay_type`, p.`amount`,e.bank_name,e.account_no
    root: 'PAYElist',
    fields: ['pid', 'emp_names','pay_type','amount','ID_no','pin_No']
});


var filters5 = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'PID'
    }, {
        type: 'string',
        dataIndex: 'emp_names',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'pay_type'
    }, {
        type: 'string',
        dataIndex: 'amount'
    }, {
        type: 'string',
        dataIndex: 'ID_no'
    }, {
        type: 'string',
        dataIndex: 'pin_No'
    }]
});


var payeColModel=function(finish,start){
    var columns = [
    {
        id:'pid',
        header: "PID",
        width: 55,
        sortable: true,
        dataIndex: 'pid',
        filterable: true
    },
    {
        header: "Names",
        width: 200,
        sortable: true,
        dataIndex: 'emp_names',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Pay Type",
        width: 80,
        sortable: true,
        dataIndex: 'pay_type',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Amount",
        width: 80,
        sortable: true,
        dataIndex: 'amount'
    },
    {
        header: "ID No",
        width: 200,
        sortable: true,
        dataIndex: 'ID_no',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Pin No",
        width: 80,
        sortable: true,
        dataIndex: 'pin_No'
    },
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}



var payeReturn=new Ext.Panel({
    title:'PAYE Return',
    id:'payeReturn',
    height:600,
    width:500,
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            xtype: 'grid',
            id:'grid',
            store: payeStore,
            colModel: payeColModel(6),
            tbar: [{
                text: 'Update',
                iconCls:'remove'
            } ,{
                xtype: 'exportbutton',
                store: payeStore,
                title:"NSSF Report"

            }, '->', // next fields will be aligned to the right

            {
                text: 'Refresh',
                tooltip: 'Click to Refresh the table',
                handler: refreshGrid,
                iconCls:'refresh'
            },{
                text: 'Clear Filter Data',
                handler: function () {
                    grid.filters.clearFilters();
                }
            },{
                text: 'Reconfigure Grid',
                handler: function () {
                    grid.reconfigure(payeStore, payeColModel(6));
                }
            }],
            height: 600,
            title:'PAYE List',
            border: true,
            listeners: {
                render: {
                    fn: function(g){
                        payeStore.load({
                            params: {
                                start: 0,
                                limit: 10
                            }
                        });
                        g.getSelectionModel().selectRow(0);
                            delay: 10
                    }

                }
            // Allow rows to be rendered.
            },

            plugins: [filters5],
            bbar: new Ext.PagingToolbar({
                store: payeStore,
                pageSize: 10,
                plugins: [filters5]
            }
            )

        }
    }]
});





payeStore.load();


//var nhifRetun=new Ext.Panel({
//    title:'NHIF Returns',
//    id:'nhifReturn',
//    height:600,
//    width:300,
//    html:'test'
//});