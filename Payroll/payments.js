/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var fm3=Ext.form;
var encode = false;
var local = true;
Date.patterns = {
    ISO8601Long:"Y-m-d H:i:s",
    ISO8601Short:"Y-m-d",
    ShortDate: "n/j/Y",
    LongDate: "l, F d, Y",
    FullDateTime: "l, F d, Y g:i:s A",
    MonthDay: "F d",
    ShortTime: "g:i A",
    LongTime: "g:i:s A",
    SortableDateTime: "Y-m-d\\TH:i:s",
    UniversalSortableDateTime: "Y-m-d H:i:sO",
    YearMonth: "F, Y",
    monthName:"F"
};
var dt = new Date();

var dtcombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'payMonth',
    name:"payMonth",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Select Payroll Month',
    store: new Ext.data.SimpleStore({
        fields: [
            'myID',
            'dispValue',
            'displayText'
        ],
        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
            [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
            [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText',
    listeners: {
        select: function(f,r,i){
            //               var strMonth=dtcombo.getValue();
            payStore.load({
                params:{
                    payMonth: i+1
                }
            });

        }
    }
});

var url = {
    local:  'getPayments.php',  // static data file
    remote: 'getPayments2.php'
};
//var strMonth=Ext.getDom('payMonth');
//var strMonth2=strMonth.value;

var payStore = new Ext.data.JsonStore({
    proxy: new Ext.data.HttpProxy({
        //where to retrieve data
        url: 'getPayments.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        payMonth:dtcombo.getValue(),
        start: 0,
        limit: 50
    },
    //    url: 'getPayments.php',
    root: 'Payments',
    totalProperty: 'total',
    fields: [{
            name:'pid',
            type: 'string'
        }, {
            name:'emp_names',
            type:'string'
        },{
            name:'pay_type',
            type:'string'
        },'amount3','balance2','Notes','payDate','payMonth','Status']
});

var filters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
            type: 'string',
            dataIndex: 'payMonth'
        },{
            type: 'string',
            dataIndex: 'pid'
        },{
            type: 'string',
            dataIndex: 'emp_names'
        },{
            type: 'string',
            dataIndex: 'pay_type'
        }
    ]
});

var filterPayments = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
            type: 'string',
            dataIndex: 'payMonth'
        },{
            type: 'string',
            dataIndex: 'pid'
        },{
            type: 'string',
            dataIndex: 'emp_names'
        },{
            type: 'string',
            dataIndex: 'pay_type'
        }
    ]
});

var payGrid = new Ext.grid.GridPanel({
    title: 'List',
    store: payStore,
    frame:true,

    columns: [
        {
            header: "PID",
            width: 80,
            dataIndex: 'pid',
            sortable: true,
            hidden:false,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            },
            editor: new fm3.TextField({
                allowBlank: false
            })
        },
        {
            id: 'Names',
            header: "Names",
            width: 140,
            dataIndex: 'emp_names',
            sortable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            },
            editor: new fm3.TextField({
                allowBlank: false
            })
        },
        {
            header: "Pay Type",
            width: 100,
            dataIndex: 'pay_type',
            sortable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            },
            editor: new fm3.TextField({
                allowBlank: false
            })
        },
        {
            header: "Amount",
            width: 80,
            dataIndex: 'amount3',
            sortable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            },
            editor: new fm3.TextField({
                allowBlank: false
            })
        },
        {
            header: "Balance",
            width: 80,
            dataIndex: 'balance2',
            sortable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            },
            editor: new fm3.TextField({
                allowBlank: true
            })
        },
        {
            header: "Notes",
            width: 80,
            dataIndex: 'Notes',
            sortable: true,
            editor: new fm3.TextField({
                allowBlank: false
            })
        },
        {
            header: "payMonth",
            width: 80,
            dataIndex: 'payMonth',
            sortable: true,
            hidden:false,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            },
            editor: new fm3.TextField({
                allowBlank: false
            })
        },
        {
            header: "Status",
            width: 80,
            dataIndex: 'Status',
            sortable: true,
            hidden:false,
            editor: new fm3.TextField({
                allowBlank: false
            })
        }
    ],plugins: [filters],
    bbar: new Ext.PagingToolbar({
        store: payStore,
        pageSize: 20,
        plugins: [filters]

    }),
    width:700,
    height:500,
    tbar: [{
            tag: 'input',
            type: 'text',
            size: '30',
            value: '',
            id:'searchParam',
            style: 'background: #F0F0F9;'
        },{
            text: 'Search',
            iconCls:'remove',
            handler: function () {
                var sParam=document.getElementById('searchParam').value;
                //                   Ext.MessageBox.alert('Message', sParam);
                payStore.load({
                    params:{
                        itemName:sParam,
                        start: 0,
                        limit: 50
                    }
                });
            }
        }, '->',{
            text: 'Delete Payment',
            iconCls:'remove'//,
            // handler :handleDelete
        }, '->', // next fields will be aligned to the right
        {
            text: 'Refresh',
            tooltip: 'Click to Refresh the table',
            handler: refreshGrid,
            iconCls:'refresh'
        }],
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("paymentForm").getForm().loadRecord(rec);
            }
        }
    }),
    listeners: {
        render: {
            fn: function(g){
                payStore.load({
                    params: {
                        start: 0,
                        limit: 50
                    }
                });
                g.getSelectionModel().selectRow(0);
                delay: 10
            }

        }
    }
});

var paymentForm=new Ext.form.FormPanel({
    id: 'paymentForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:350,
    minHeight:300,
    waitMsgTarget: true,
    items: [
        {
            xtype: 'textfield',
            id: 'pid',
            name:'pid',
            fieldLabel: 'PID',
            allowBlank: false,
            msgTarget:'side'
        },
        {
            xtype: 'textfield',
            id: 'emp_names',
            name:'emp_names',
            fieldLabel: 'Employee Names',
            allowBlank: false,
            msgTarget:'side',
            validationEvent:false
        },
        {
            xtype: 'textfield',
            id: 'pay_type',
            name:'pay_type',
            fieldLabel: 'Payment Type',
            allowBlank: false,
            msgTarget:'side',
            validationEvent:false
        },
        {
            xtype: 'textfield',
            id: 'amount3',
            name:'amount3',
            fieldLabel: 'Amount',
            allowBlank: false,
            msgTarget:'side',
            validationEvent:false
        },{
            xtype: 'textfield',
            id: 'balance2',
            name:'balance2',
            fieldLabel: 'Balance',
            allowBlank: true,
            msgTarget:'side',
            validationEvent:false
        },
        {
            xtype: 'textfield',
            id: 'notes',
            name:'notes',
            fieldLabel: 'Notes',
            allowBlank: true,
            msgTarget:'side',
            validationEvent:false
        }
    ]
});
// create the combo instance




var paymentServices=new Ext.form.FormPanel({
    id: 'paymentServices',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:350,
    waitMsgTarget: true,
    items: [ {
            xtype: 'textfield',
            id: 'Status',
            name:'Status',
            fieldLabel: 'Status',
            allowBlank: false,
            msgTarget:'side',
            disabled:true
        },dtcombo]
});

function clearGrid(){
    payStore.clear();
}
function refreshGrid() {
    payStore.reload();//
} // end refresh

var submit2 = paymentForm.addButton({
    text: 'Save',
    disabled:false,
    handler: function(){
        paymentForm.form.submit({
            url:'updatePayment.php', //php

            waitMsg:'Saving Data...',

            success: function (form, action) {
                Ext.MessageBox.alert('Message', 'Payment Updated Successfully');
                refreshGrid();
            },
            failure:function(form, action) {
                Ext.MessageBox.alert('Message', 'Update unsuccessful, Check Data');
            }
        });
    }
});

var importProll = paymentServices.addButton({
    text: 'Import default Values',
    disabled:false,
    handler: function(){
        paymentServices.form.submit({
            url:'importPrevious.php', //php
       
            method: 'POST',
            baseParams:{
                task: "readCat"
            },//this parameter is passed for any HTTP request
            waitMsg:'Saving Data...',

            success: function (form, action) {
                Ext.MessageBox.alert('Message', 'data import successful');
                 refreshGrid();
            },
            failure:function(form, action) {
                Ext.MessageBox.alert('Message', 'import failed, check data ');
            }
        });
        refreshGrid();
    }
});

var importDebits = paymentServices.addButton({
    text: 'Update Taxes',
    disabled:false,
    handler: function(){
        paymentServices.form.submit({
            url: 'getEmpPayments.php?task=updateAllTax',
             method: 'POST',
            baseParams: {
                task: "updateAllTax"
            },
            waitMsg: 'Updating Taxes ...',
            success: function (form, action) {
                Ext.MessageBox.alert('Message', 'Taxes import successful');
                 refreshGrid();
            },
            failure:function(form, action) {
                Ext.MessageBox.alert('Message', 'Tax Update failed, check data ');
            }
        })
            //        refreshGrid();
        }
    });

    var importLoans = paymentServices.addButton({
        text: 'Import Loans',
        disabled:false,
        handler: function(){
            paymentServices.form.submit({
                url:'importLoans.php', //php

                waitMsg:'Saving Data...',

                success: function (form, action) {
                    Ext.MessageBox.alert('Message', 'Saved OK');
                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed ');
                }
            });
        }
    });



    var cancel = paymentForm.addButton({
        text: 'Cancel',
        disabled:false,
        handler: function(){
            paysForm.hide();
        }
    });

    var paymentsForm=new Ext.Panel({
        title:'Payments',
        id:'prollPayments',
        height:600,
        width:400,
        layout:'table',
        layoutConfig: {
            columns: 2
        },
        defaults: {
            bodyStyle:'padding:15px 20px'
        },
        items: [{
                width:400,
                items:[paymentForm]
            },{
                width:400,
                cellId: 'basic-cell',
                cellCls: 'custom-cls',
                items:[paymentServices]
            },{
                colspan:2,
                items:[payGrid]
        
            }]
    });



    payStore.load();
