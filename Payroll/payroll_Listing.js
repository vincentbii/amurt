/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var selectedKeys;

//`Pid`,`emp_names`,`pay_type`,`amount`,`payDate`,`payMonth`
var encode = false;
var local = true;

var url = {
    local:  'getPostings.php',  // static data file
    remote: 'getPostings.php'
};
//

var pstCombo2= new Ext.form.ComboBox({
    
    typeAhead: true,
    id:'pstMonth',
    name:"pstMonth",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Select Payroll Month',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
        [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
        [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText',
    listeners: {
        select: function(f,r,i){
            //               var strMonth=dtcombo.getValue();
            pstStore.load({
                params:{
                    pstMonth: pstCombo2.getValue()
                }
            });

        }
    }
});


var pstFilters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'Pid'
    }, {
        type: 'string',
        dataIndex: 'emp_names',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'pay_type'
    }]
});

var pstCM = new Ext.grid.ColumnModel(
    [
    {
        id: 'ID',
        header: 'ID',
        dataIndex: 'ID',
        width: 100
    },{
        header: 'pay_type',
        dataIndex: 'pay_type',
        width: 200
    },{
        dataIndex: 'Pid',
        header: "Pid",
        sortable: true,
        width: 100
    },{
        header: 'emp_names',
        dataIndex: 'emp_names',
        width: 200
    },
  
    {
        header: 'amount',
        dataIndex: 'amount',
        width: 200
    }
    ]);

// by default columns are sortable
pstCM.defaultSortable = true;
// specify a jsonReader (coincides with the XML format of the returned data)

var pstReader= new Ext.data.JsonReader(
{
    root: 'pstDetail',
    id:'ID',
    totalProperty: 'totalCount'
},
[
{
    name: 'ID',
    type:'numeric'
},{
    name: 'pay_type',
    type: 'string'
},
{
    name: 'Pid',
    type:'string'
},
{
    name: 'emp_names',
    type: 'string'
},

{
    name: 'amount',
    type: 'string'
}
]
)


// create the Data Store
var pstStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'getPostings.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        task: "postingDetail",
        lightWeight:true,
        ext: 'js',
        pstMonth:pstCombo2.getValue()
    },//This
    autoLoad: {
        params:{
            start:0, 
            limit:500
        }
    },
reader:pstReader,
sortInfo: {
    field:'pay_type',
    direction:'ASC'
}//,
//groupField:'pay_type'
});
pstStore.load();

var pstGrid = new Ext.grid.EditorGridPanel({
    store: pstStore,
    cm: pstCM,
//    view: new Ext.grid.GroupingView({
//        forceFit:true,
//        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
//    }),
    width: 700,
    height: 700,
    //autoExpandColumn: 'type',
    title: 'PAYROLL POSTING DETAIL',
    frame: true,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    stripeRows: true,
    tbar: [{
                xtype: 'exportbutton',
                title:"POSTING DETAILS",
                store: pstStore

    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshPst,
        iconCls:'refresh'
    }],
    plugins: [paymentFilters],
    bbar: new Ext.PagingToolbar({
        store: pstStore,
        pageSize:500,
        displayInfo:true
    }),

    view: new Ext.ux.grid.BufferView({
        // custom row height
        rowHeight: 20,
        // render rows as they come into viewable area.
        scrollDelay: false
    })
});



var postingForm=new Ext.form.FormPanel({
    id: 'processForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:350,
    minHeight:300,
    waitMsgTarget: true,
    items: [pstCombo2]
});

var postings=new Ext.Panel({
    title:'PAYROLL POSTING DETAIL',
    id:'postings',
    height:700,
    width:700,
    layout:'table',
    layoutConfig: {
        columns: 1
    },
    items: [{
        width:700,
        items:[postingForm]
    },{
        items:[pstGrid]
    }]
        
})
function refreshPst() {
    pstStore.reload();//
} // end refresh

