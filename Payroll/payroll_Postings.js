/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//
var selectedKeys;

//`Pid`,`emp_names`,`pay_type`,`amount`,`payDate`,`payMonth`
var encode = false;
var local = true;
  var local3 = true;
var url = {
    local:  'getPostings.php',  // static data file
    remote: 'getPostings.php'
};


var postingsCombo2= new Ext.form.ComboBox({
    
    typeAhead: true,
    id:'postingMonth',
    name:"postingMonth",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Select Payroll Month',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
        [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
        [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText'
});


var pstFilters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'Pid'
    }, {
        type: 'string',
        dataIndex: 'emp_names',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'pay_type'
    }]
});

var pstCM = new Ext.grid.ColumnModel(
    [
    {
        id: 'ID',
        header: 'ID',
        dataIndex: 'ID',
        width: 100
    },{
        header: 'Pay Type',
        dataIndex: 'pay_type',
        width: 200
    },{
        dataIndex: 'Pid',
        header: "Pid",
        sortable: true,
        width: 100
    },{
        header: 'Emp Names',
        dataIndex: 'emp_names',
        width: 200
    },
    {
        header: 'Amount',
        dataIndex: 'amount',
        width: 200
    }
    ]);

// by default columns are sortable
pstCM.defaultSortable = true;
// specify a jsonReader (coincides with the XML format of the returned data)

var pstReader= new Ext.data.JsonReader(
{
    root: 'pstDetail',
    id:'ID',
    totalProperty: 'totalCount'
},
[
{
    name: 'ID',
    type:'numeric'
},{
    name: 'pay_type',
    type: 'string'
},
{
    name: 'Pid',
    type:'string'
},
{
    name: 'emp_names',
    type: 'string'
},

{
    name: 'amount',
    type: 'string'
}
]
)


// create the Data Store
var pstStore = new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'getPostings.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        task: "postingDetail",
        lightWeight:true,
        ext: 'js',
       pstMonth:postingsCombo2.getValue()
    },//This
    autoLoad: {
        params:{
            start:0, 
            limit:500
        }
    },
reader:pstReader,
sortInfo: {
    field:'pay_type',
    direction:'ASC'
},
groupField:'pay_type'
});
pstStore.load();

var pstGrid = new Ext.grid.EditorGridPanel({
    id:pstGrid,
    store: pstStore,
    cm: pstCM,
    width: 700,
    height: 500,
    //autoExpandColumn: 'type',
    title: 'PAYROLL POSTING DETAIL',
    frame: true,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    stripeRows: true,
    tbar: [ '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshPst,
        iconCls:'refresh'
    }],
    plugins: [pstFilters],
    bbar: new Ext.PagingToolbar({
        store: pstStore,
        pageSize:500,
        displayInfo:true
    }),

    view: new Ext.ux.grid.BufferView({
        // custom row height
        rowHeight: 20,
        // render rows as they come into viewable area.
        scrollDelay: false
    }),
     buttons: [
                        {
                            text: 'Print',
                            handler: function() {
                              var payslipMonth=document.getElementById('payslipMonth').value;
                                var pstCode=document.getElementById('pstCode4').value;
                                var pstCode1=document.getElementById('pstCode5').value;
                                var postingVer=document.getElementById('postingType').value;
                                var branch=document.getElementById('empBranchCombo3').value;
                                  window.open('reports/postings.php?pmonth='+payslipMonth+'&ptype='+postingVer 
                                  +'&pstCode='+pstCode+'&pstCode1='+pstCode1+'&branch='+branch 
                                ,"Earnings and Deductions","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,\n\
                                scrollbars=yes,status=yes//////");

                            }
                        },{
                            text: 'Export to Excel',
                            handler: function() {
                                var payslipMonth=document.getElementById('payslipMonth').value;
                                var pstCode=document.getElementById('pstCode4').value;
                                var pstCode1=document.getElementById('pstCode5').value;
                                var branch=document.getElementById('empBranchCombo3').value;
                                var postingVer=document.getElementById('postingType').value;
                                  window.open('reports/exportEarnings.php?pmonth='+payslipMonth+'&ptype='+postingVer 
                                      +'&pstCode='+pstCode +'&pstCode1='+pstCode1 +'&branch='+branch  
                                ,"Earnings and Deductions","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,\n\
                                scrollbars=yes,status=yes//////");
                            }
                        }]
});


//---------------------------------------------------
//payroll postings branches

            var empBranchStore4=new Ext.data.JsonStore({
                    url: 'comboFields.php',
                    root: 'getEmpBranch',
                    id: 'ID',//
                    baseParams:{
                        task: "getEmpBranch"
                    },//This
                    fields:['empBranch']
                });
                empBranchStore4.load()

                var empBranchCombo4 = new Ext.form.ComboBox({
                    typeAhead: true,
                    id:'empBranchCombo4',
                    name:"empBranchCombo4",
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: local3,
                    fieldLabel:'Branch',
                    store: empBranchStore4,
                    valueField: 'empBranch',
                    displayField: 'empBranch'
                });

//end of payroll postings
//=====================================================

var postingForm=new Ext.form.FormPanel({
    id: 'postingForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:350,
    minHeight:300,
    waitMsgTarget: true,
    items: [postingsCombo2],
            buttons: [{
                            text: 'Preview Report',
                            handler: function() {
                                var pstCode=document.getElementById('pstCode4').value;
                                var pstCode2=document.getElementById('pstCode5').value;
                                pstStore.load({
                                    params:{
//                                        pstMonth: postingsCombo2.getValue(),
                                        pstCode:pstCode,
                                        pstCode2:pstCode2
                                    }
                                });
                            }
                        },
                        {
                            text: 'Print',
                            handler: function() {
                                var pmonth=document.getElementById('pstMonth').value;
                                var ptype=document.getElementById('pstCode').value;
                                  window.open('reports/postings.php?pmonth='+pmonth+'&ptype='+ptype 
                                ,"Earnings and Deductions","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,\n\
                                scrollbars=yes,status=yes//////");

                            }
                        },{
                            text: 'Export to Excel',
                            handler: function() {
                                var pmonth=document.getElementById('pstMonth').value;
                                var ptype=document.getElementById('pstCode').value;
                                  window.open('reports/exportEarnings.php?pmonth='+pmonth+'&ptype='+ptype 
                                ,"Earnings and Deductions","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,\n\
                                scrollbars=yes,status=yes//////");
                            }
                        }]
});

var prollPostings=new Ext.Panel({
    title:'PAYROLL POSTING DETAIL',
    id:'prollPostings',
    height:700,
    width:700,
    layout:'table',
    layoutConfig: {
        columns: 1
    },
    items: [{
//        width:700,
//        items:[postingForm]
//    },{
        items:[pstGrid]
    }]
        
})

function refreshPst() {
    pstStore.reload();//
} // end refresh

