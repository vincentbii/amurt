/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.chart.Chart.CHART_URL = '../include/Extjs/resources/charts.swf';

var myStore = new Ext.data.JsonStore({
        url: 'getEmpDepts.php',
        root: 'data',
        fields: ['department', 'total']

    });
    
    var charts1=new Ext.Panel({
        width: 400,
        height: 400,
//        renderTo: 'container',
        items: {
            store: myStore,
            xtype: 'piechart',
            dataField: 'total',
            categoryField: 'department',
            //extra styles get applied to the chart defaults
            extraStyle:
            {
                legend:
                {
                    display: 'bottom',
                    padding: 5,
                    font:
                    {
                        family: 'Tahoma',
                        size: 13
                    }
                }
            }
        }
    });
myStore.load();