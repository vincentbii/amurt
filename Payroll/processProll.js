
var encode = false;
var local = true;

var url = {
    local:  'getProllProcess.php',  // static data file
    remote: 'getProllProcess.php'
};

var dtcombo2 = new Ext.form.ComboBox({
    typeAhead: true,
    id:'payMonth2',
    name:"payMonth2",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Select Payroll Month',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'jan','january'], [2, 'feb', 'February'], [3, 'mar', 'March'], [4, 'apr', 'April'],
        [5, 'may', 'May'], [6, 'jun', 'June'], [7, 'jul', 'July'], [8, 'aug', 'August'],
        [9, 'sep', 'September'], [10, 'oct', 'October'], [11, 'nov', 'November'], [12, 'dec', 'December']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText',
    listeners: {
        select: function(f,r,i){
            //               var strMonth=dtcombo.getValue();
            processStore.load({
                params:{
                    payMonth2: i+1
                }
            });

        }
    }
});

var processStore = new Ext.data.JsonStore({
    proxy: new Ext.data.HttpProxy({
        //where to retrieve data
        url: 'getProllProcess.php', //url to data object (server side script)
        method: 'POST'
    }),
    baseParams:{
        payMonth2:dtcombo2.getValue(),
        start: 0,
        limit: 50
    },
    //    url: 'getPayments.php',
    root: 'Payments',
    totalProperty: 'total',
    fields: [{
        name:'pid',
        type: 'string'
    }, {
        name:'emp_names',
        type:'string'
    },{
        name:'pay_type',
        type:'string'
    },'amount','Notes','payDate','payMonth','Status']
});


var processFilters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'pid'
    },{
        type: 'string',
        dataIndex: 'emp_names'
    },{
        type: 'string',
        dataIndex: 'pay_type'
    },{
        type: 'string',
        dataIndex: 'amount'
    }
    ]
});

var processGrid = new Ext.grid.GridPanel({
//    title: 'List',
    store: processStore,
    frame:true,
    columns: [
    {
        header: "PID",
        width: 80,
        dataIndex: 'pid',
        sortable: true,
        hidden:false
    },
    {
        id: 'title-col',
        header: "Names",
        width: 100,
        dataIndex: 'emp_names',
        sortable: true,
        autoExpandColumn: 'title-col'
    },
    {
        header: "Pay Type",
        width: 80,
        dataIndex: 'pay_type',
        sortable: true
    },
    {
        header: "Amount",
        width: 80,
        dataIndex: 'amount',
        sortable: true
    },
    {
        header: "Notes",
        width: 100,
        dataIndex: 'Notes',
        sortable: true
    },
    {
        header: "Month",
        width: 80,
        dataIndex: 'payMonth',
        sortable: true
    },
    {
        header: "Status",
        width: 80,
        dataIndex: 'Status',
        sortable: true
    },
    {
        header: "Notes",
        width: 300,
        dataIndex: 'Notes',
        sortable: true
    }
    ],

    width:700,
    height:500,
    tbar: [{
                tag: 'input',
                type: 'text',
                size: '30',
                value: '',
                id:'searchParam',
                style: 'background: #F0F0F9;'
            },{
                text: 'Search',
                iconCls:'remove',
                handler: function () {
                   var sParam=document.getElementById('searchParam').value;
//                   Ext.MessageBox.alert('Message', sParam);
                    payStore.load({
                        params:{
                            itemName:sParam,
                               start: 0,
                               limit: 50
                        }
                    });
                }
            },{
        text: 'Delete Payment',
        iconCls:'remove'//,
    // handler :handleDelete
    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid2,
        iconCls:'refresh'
    }]
,plugins: [processFilters]
,bbar: new Ext.PagingToolbar({
        store: processStore,
        pageSize: 50,
        plugins: [processFilters]

    }),
    listeners: {
        render: {
                    fn: function(g){
                        processStore.load({
                            params: {
                                start: 0,
                                limit: 50
                            }
                        });
                        g.getSelectionModel().selectRow(0);
                            delay: 10
                    }

                }
    }
});
processStore.load();
function clearGrid2(){
    processStore.clear();
}
function refreshGrid2() {
    processStore.reload();//
} // end refresh



var processForm=new Ext.form.FormPanel({
    id: 'processForm',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:350,
    minHeight:300,
    waitMsgTarget: true,
    items: [
    {
        xtype: 'textfield',
        id: 'prollStatus',
        name:'prollStatus',
        fieldLabel: 'Status',
        allowBlank: false,
        msgTarget:'side',
        value:'Closed'
    },dtcombo2
]
});


var process = processForm.addButton({
    text: 'Process',
    disabled:false,
    handler: function(){
        processForm.form.submit({
            url:'processProll.php', //php
            method: 'POST',
            baseParams:{
                task: "readCat"
            },//this
            waitMsg:'Saving Data...',

            success: function (form, action) {
                Ext.MessageBox.alert('Message', 'Saved OK');

            },
            failure:function(form, action) {
                Ext.MessageBox.alert('Message', 'Save failed ');
            }
        });
    }
});

var processProll=new Ext.Panel({
    title: 'Post Payroll',
    id:'processProll',
    height:500,
    width:500,
    layout:'table',
    layoutConfig: {
        columns: 1
    },
    items: [{
        width:500,
        items:[processForm]
    },{
//        colspan:2,
        items:[processGrid]

    }]
});


