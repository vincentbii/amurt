<?php

error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
require_once($root_path.'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path.'include/inc_init_xmlrpc.php');

$Err = "";
$payMonth = $_POST['payMonth2'];

if (trim($_POST['prollStatus'] == "")) {
	$Err = 'Status:"Payroll Status must not be empty",';
}

if ($Err == "") {

$sql="Update proll_payments set `status`='Closed' where payMonth='$payMonth'";
//echo $sql;
if($result=$db->Execute($sql)){
    closePayroll($db,$payMonth);
//    updatePayrollErp($db,$payMonth);
}

$results = "{success: true}";
      // echo $sql;

} else {
	// Errors. Set the json to return the fieldnames and the associated error messages
	$results = '{success: false, errors:{'.$Err.'} }'; // Return the error message(s)
}

echo $results;

function closePayroll($db,$payMonth){
    $debug=true;
    if ($debug) echo "<b>processPayroll.php:closePayroll()</b><br>";
    
    //DO A BACKUP OF THE PAYMENTS BEFORE CLOSING THE PAYROLL
    
    $sql="INSERT INTO `proll_payments_BKUP`
            (`ID`,`Pid`,`emp_names`,`catID`,`pay_type`,`amount`,`Balance`,`Total`,
            `Notes`,`payDate`, `payMonth`, `status`, `weberp_syncd`)
            SELECT `ID`,`Pid`, `emp_names`, `catID`, `pay_type`, `amount`,`Balance`,
            `Total`,`Notes`, `payDate`, `payMonth`,  `status`, `weberp_syncd`
            FROM `proll_payments` WHERE payMonth='$payMonth'";
    if($debug) echo $sql;
    $result=$db->Execute($sql);
    
    //End of payments backup procedure
    //===============================================================================
    
    //delete all the non recurrent postings WITH BALANCES as zero e.g mid month advance
    $sql="SELECT P.ID,P.PID,P.EMP_NAMES,T.ID,P.PAY_TYPE,P.AMOUNT,P.BALANCE FROM PROLL_PAYMENTS P 
            LEFT JOIN PROLL_PAYTYPES T ON P.PAY_TYPE=T.`TYPE` WHERE P.PAY_TYPE  
            IN(SELECT `TYPE` FROM proll_paytypes WHERE FIXED=0 and contribution=0) 
            AND paymonth='$payMonth' AND BALANCE=0";
    
    if($debug) echo $sql;
    $result=$db->Execute($sql);
    $processLog='';
    $processLog=$processLog.'Items removed for the Next Month Payroll<br>';
    while($row=$result->FetchRow()){
        //DELETE all the items fixed=0 and contribution=0 in proll_paytypes
        $sql="DELETE FROM PROLL_EMP_PAYMENTS WHERE PID='$row[1]' AND PAY_NAME='$row[3]'";
          if($debug) echo $sql;
        $db->Execute($sql);
      
        $processLog=$processLog.$row[1].'-'.$row[2].'-'.$row[3].'-'.$row[4].'-'.$row[5].'<br>';
    }
    //END OF DELETING NON RECURRENT POSTINGS
    //============================================================================================
    
    
    //--------------------------------------------------------------------------------------------
    //Calculate and update all the deductions balances e.g loans,advances,bills and interests
    //---> get all deductions with zeros and non zeros
    $sql2="SELECT P.ID,P.PID,P.EMP_NAMES,T.ID,P.PAY_TYPE,P.AMOUNT,P.BALANCE FROM PROLL_PAYMENTS P 
            LEFT JOIN PROLL_PAYTYPES T ON P.PAY_TYPE=T.`TYPE` WHERE P.PAY_TYPE  
            IN(SELECT `TYPE` FROM proll_paytypes WHERE FIXED=1 and contribution=2) 
            AND paymonth='$payMonth'";
    
      if($debug) echo $sql2;
    $result2=$db->Execute($sql2);
    
    $processLog=$processLog.'<BR>DELETED ALL DEDUCTIONS WITH ZERO BALANCES<BR>';
    
    while($row2=$result2->FetchRow()){
        //delete all the deductions with zero balances
        $sql2_1="DELETE FROM PROLL_EMP_PAYMENTS WHERE PID='$row2[1]' AND PAY_NAME='$row2[3]' and BALANCE=0";
          if($debug) echo $sql2_1;
        $db->Execute($sql2_1);
        $processLog=$processLog.$row2[1].'-'.$row2[2].'-'.$row2[3].'-'.$row2[4].'-'.$row2[5].'<br>';
    }
    
    //get all deductions with balances
    $sql3="SELECT P.ID,P.PID,P.EMP_NAMES,T.ID,P.PAY_TYPE,P.AMOUNT,P.BALANCE FROM PROLL_PAYMENTS P 
            LEFT JOIN PROLL_PAYTYPES T ON P.PAY_TYPE=T.`TYPE` WHERE P.PAY_TYPE  
            IN(SELECT `TYPE` FROM proll_paytypes WHERE FIXED=1 and contribution=2) 
            AND paymonth='$payMonth' and balance>0";
    
      if($debug) echo $sql3;
    $result3=$db->Execute($sql3);
    
    $processLog=$processLog.'<BR>UPDATED BALANCES FOR DEDUCTIONS <BR>';
    
     while($row3=$result3->FetchRow()){
        //delete all the deductions with zero balances
         $newBal=$row3[6]-$row3[5];
        $sql3_1="UPDATE PROLL_EMP_PAYMENTS SET BALANCE='$newBal' WHERE PID='$row3[1]' 
         AND PAY_NAME='$row3[3]'";
        
          if($debug) echo $sql3_1;
        $db->Execute($sql3_1);
        
        $processLog=$processLog.$row3[1].'-'.$row3[2].'-'.$row3[3].'-'.$row3[4].'-'.$row3[5].'<br>';
    }
    //
    //END OF deductions WITH balances
    //===========================================================================================
    
    
    
    //--------------------------------------------------------------------------------------------
    //calculate and update all the CONTRIBUTIONS e.g SAVINGS AND PENSION
        //get all CONTRIBUTIONS 
    $sql4="SELECT P.ID,P.PID,P.EMP_NAMES,T.ID,P.PAY_TYPE,P.AMOUNT,P.BALANCE FROM PROLL_PAYMENTS P 
            LEFT JOIN PROLL_PAYTYPES T ON P.PAY_TYPE=T.`TYPE` WHERE P.PAY_TYPE  
            IN(SELECT `TYPE` FROM proll_paytypes WHERE FIXED=1 and contribution=1)
            AND paymonth='$payMonth' and balance>0";
    
      if($debug) echo $sql4;
    $result4=$db->Execute($sql4);
    
    $processLog=$processLog.'<BR>UPDATED BALANCES FOR CONTRIBUTIONS  <BR>';
    
     while($row4=$result4->FetchRow()){
        //delete all the deductions with zero balances
         $newBal=$row4[6] + $row4[5];
        $sql4_1="UPDATE PROLL_EMP_PAYMENTS SET BALANCE='$newBal' WHERE PID='$row4[1]' 
         AND PAY_NAME='$row4[3]'";
        
          if($debug) 
              echo $sql4_1;
        $db->Execute($sql4_1);
        
        $processLog=$processLog.$row4[1].'-'.$row4[2].'-'.$row4[3].'-'.$row4[4].'-'.$row4[5].'<br>';
    }
    //delete all postings excluding default values like basic salary and postings with balances
   
    
    //CREATE THE lo file
    $File = "c:/Temp/payroll_$payMonth.txt";
    $Handle = fopen($File, 'w');
     fwrite($Handle, $processLog);
     fclose($Handle);
}

//function updatePayrollErp($db,$payMonth) {
//      //global $db, $root_path;
//      $debug=false;
//        if ($debug) echo "<b>class_tz_billing::updatePaymentErp()</b><br>";
//        if ($debug) echo "Voucher no: $Voucher_No <br>";
//        ($debug) ? $db->debug=TRUE : $db->debug=FALSE;
//        
//         $sql ="SELECT d.ID,e.Department,SUM(p.amount) AS grosspay,d.gl_acc FROM proll_payments p LEFT JOIN proll_empregister e 
//ON p.Pid=e.PID LEFT JOIN proll_departments d ON e.Department=d.Name
//WHERE p.catID IN('pay','benefit') 
//AND p.paymonth='$payMonth'  AND p.pay_type<>'PENSION'
//GROUP BY e.Department";
//         
//        if($debug) echo $sql;
//        $result=$db->Execute($sql);
//        if($weberp_obj = new_weberp()) {
//        //$arr=Array();
//
//            while($row=$result->FetchRow()) {
//                if(!$weberp_obj->transfer_Payroll_to_webERP_asCost($row)) {
//
//                     $sql2="Update proll_payments set
//                    weberp_syncd='1' where weberp_syncd='0' and pay_Month='$payMonth'";
//                     if($debug) echo $sql2;
//                    $db->Execute($sql2);
//
//                }
//                else {
//                    echo 'failed';
//                }
//                destroy_weberp($weberp_obj);
//                      
//            }
//        }else {
//            echo 'could not create object: debug level ';
//        }
//    }
?>