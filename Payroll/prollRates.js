
    // shorthand alias
    var fm = Ext.form;

// custom column plugin example
var checkColumn = new Ext.grid.CheckColumn({
       header: 'Contribution?',
       dataIndex: 'Contribution',
       width: 80
    });

var fixedColumn = new Ext.grid.CheckColumn({
       header: 'Fixed?',
       dataIndex: 'fixed',
       width: 55
    });
// the column model has information about grid columns
    // dataIndex maps the column to the specific data field in
    // the data store (created below)
    var cm = new Ext.grid.ColumnModel(
    [
        {
           id: 'CatID',
           header: 'Pay Category',
           dataIndex: 'PayCategory',
           width: 100,
           editor: new fm.TextField({
               allowBlank: false
           })
        },{
           header: 'Type',
           dataIndex: 'type',
           width: 130,
           editor: new fm.TextField({
               allowBlank: false
           })
        },
        fixedColumn,
        checkColumn,
        {
           header: 'Notes',
           dataIndex: 'notes',
           width: 200,
           editor: new fm.TextField({
               allowBlank: false
           })
        }
    ]);

    // by default columns are sortable
    cm.defaultSortable = true;
 // specify a jsonReader (coincides with the XML format of the returned data)
       var reader= new Ext.data.JsonReader(
            {
                root: 'Paytypes'
            },
            [

                {name: 'PayCategory', type: 'string'},
                {name: 'type', type: 'string'},
                {name: 'fixed', type: 'bool'},
                {name: 'Contribution', type: 'bool'},
                {name: 'notes', type: 'string'}
            ]
        )
 // create the Data Store
    var ptypestore = new Ext.data.GroupingStore({
       url: 'getPayTypes.php',
        reader:reader,
        sortInfo: {field:'PayCategory', direction:'ASC'},
         groupField:'PayCategory'
    });

 var ptypegrid = new Ext.grid.EditorGridPanel({
        store: ptypestore,
        cm: cm,
        view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
        }),
        width: 600,
        height: 500,
        //autoExpandColumn: 'type',
        title: 'Edit Pay Types?',
        frame: true,
        plugins: [fixedColumn,checkColumn],

        clicksToEdit: 1,
        tbar: [{
            text: 'Add a pay Type',
            handler : function(){
                // access the Record constructor through the grid's store
                var ptype =ptypegrid.getStore().recordType;
                var p = new ptype({
                    catID: '1',
                    type: 'Loan',
                    Fixed: 0,
                    notes: '',
                    Contribution: false
                });
                ptypegrid.stopEditing();
                ptypestore.insert(0, p);
                ptypegrid.startEditing(0, 0);
            }
        }]
    });
ptypestore.load();

