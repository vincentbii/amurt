<style type="text/css" media="Screen">
    table {
        border-collapse: collapse;
    }

    #t1 * .r1 td {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        border-right: 1px solid black;
        border-left: 1px solid black;
    }


</style>
<?php
/* Care2x Payroll deployment 01-01-2010
 * GNU General Public License
 * Copyright 2010 George Maina
 * georgemainake@gmail.com
 *
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
$slipmont = $_POST[slipMonth];

$paymonth = $_POST['scharam1'];
$month1 = date_create($_POST['scharam2']);
$month2 = date_create($_POST['scharam3']);
$dept = $_POST['schParam4'];

$sql = 'SELECT p.`Type` FROM proll_paytypes p';
$result = $db->Execute($sql);
$numRows = $result->RecordCount();

while ($row = $result->FetchRow()) {
    $payData[] = $row[0];
}

$rowsperpage = 10;

$sqlC = 'SELECT  count(distinct `Pid`) FROM proll_payments';
$resultC = $db->Execute($sqlC);
$rows = $resultC->FetchRow();
$numRows = $rows[0];
$maxPage = ceil($numRows / $rowsperpage);
$offset = 0;

$sqlD = 'select ID, CompanyName, Address, Postal, Phone, `Physical Address`, Town, country, email from care2x.proll_company ';
$resultD = $db->Execute($sqlD);
$rowD = $resultD->FetchRow();

echo "<table width=100%><tr><td  align=center><b>$rowD[1]</b></td></tr>";
echo "<tr><td align=center><b>$rowD[2],$rowD[6] $rowD[3]</b></td></tr>";
echo "<tr><td align=center>&nbsp;</td></tr>";
echo "<tr><td align=center><b>PAYROLL SHEDULE</b></td></tr>";
echo "<tr><td align=center><b>" . date('F j, Y, g:i a') . "</b></td></tr></table>";
echo '<br><br>';
for ($page = 1; $page <= $maxPage; $page++) {

    printTable($empData, $payData, $offset, $maxPage, $rowsperpage);
    echo '<br><br><br>';
    $offset = $rowsperpage * $page;
    echo "<table width=100%>
     <tr><td align=left>Sign ________________________________</td><td align=left>Sign. ________________________________________</td></tr>";
    echo "<tr><td align=center>Finance Officer</td><td align=center>Administrator</td></tr>";
    echo "</table><br><br>";
}

//printTable($empData, $payData);

function printTable($empData, $payData, $offset, $maxPage, $rowsperpage) {
    global $db;

    $sql2 = 'SELECT  distinct `Pid`,emp_names FROM proll_payments limit ' . $offset . ',' . $rowsperpage;
    $result2 = $db->Execute($sql2);
    while ($row2 = $result2->FetchRow()) {
        $empData[1][] = $row2[0];
        $empData[2][] = $row2[1];
    }

    echo '<table  id="t1" width=100%><tr class="r1"><td>Names</td>';
    foreach ($empData[2] as $pid) {
        echo "<td>$pid</td>";
    }
    echo '</tr>';
    foreach ($payData as $side) { // Creating each row
        echo '<tr class="r1">';
        echo '<td class="c1">' . $side . '</td>';
        foreach ($empData[1] as $top) { // Creating each column in each row
            $sql3 = 'SELECT  p.`Pid`, p.`catID`, p.`pay_type`, p.`amount`, p.`payMonth` FROM proll_payments p
       where  p.`pay_type`="' . $side . '" and p.`pid`="' . $top . '"';
            $result3 = $db->Execute($sql3);
            $numRows = $result3->RecordCount();
            $row = $result3->FetchRow();
            $output = $row[3]; // multiple the top value times the side value

            echo "<td class='c1'>$output</td>";
        }
        echo '</tr>';
    }
    echo '<tr class="r1"><td class="c1"><b>Net Pay<b></td>';
    foreach ($empData[1] as $pid) {
        $sql = 'select pid,sum(amount) as grosspay from proll_payments where catID IN("pay","relief","benefit") and pid="' . $pid . '" and paymonth="January"';
        $result = $db->Execute($sql);
        $numRows = $result->RecordCount();
        $sumRows = $result->FetchRow();

        $sql = 'select pid,sum(amount) as deductions from proll_payments where catID IN("Deduct") and pid="' . $pid . '" and paymonth="January"';
        $result = $db->Execute($sql);
        $numRows = $result->RecordCount();
        $diffRows = $result->FetchRow();

        echo '<td class="c1"><b>' . intval($sumRows[1] - $diffRows[1]) . '</b></td>';
    }
    echo '</tr></table>';
}

