
Ext.QuickTips.init();

// turn on validation errors beside the field globally
Ext.form.Field.prototype.msgTarget = 'side';

var fm = Ext.form;
var primaryKey='PID'; //primary key is used several times throughout
// custom column plugin example
var selectedKeys;
var url = {
    local:  'getEmployee.php',  // static data file
    remote: 'getEmployee.php'
};
// configure whether filter query is encoded or not (initially)
var encode = false;

// configure whether filtering is performed locally or remotely (initially)
var local = true;

var empreader=new Ext.data.JsonStore({
    url: 'getEmployee.php',
    root: 'Employees',
    totalProperty: 'total',
    fields:['PID','FirstName','LastName','SurName','Department','JobTitle','Box','PostalCode','Phone','Town','Email','ID_No',
    'Pin_No','NHIF_No','NSSF_NO','Bank_Name','Account_No','basicpay','nssf','nhif','paye','relief','emptype','AppointDate',
    'AppointEndDate','ConfirmDate','Nationality','MaritalStatus','dob','sex','hseAllowance','union','pension',
    'sacco','housed','unionised',
    'riskAllowance','gratuity','bankID','branchID','empBranch','jobGroup']
});
/*    Here is where we create the Form
 */

var filters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'PID'
    }, {
        type: 'string',
        dataIndex: 'FirstName',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'LastName'
    }, {
        type: 'string',
        dataIndex: 'SurName'
    }, {
        type: 'string',
        dataIndex: 'Department'
    }, {
        type: 'string',
        dataIndex: 'JobTitle'
    }]
});


var addresses={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:540,
    minHeight:300,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [
    {
        xtype: 'textfield',
        id: 'Box',
        fieldLabel: 'Addresses',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'PostalCode',
        fieldLabel: 'Postal Code',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Phone',
        fieldLabel: 'Phone',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Town',
        fieldLabel: 'Town',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Email',
        fieldLabel: 'Email',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    }
    ]
};

//get employee branch
//Get the combo with the bank Code


//Get the combo with the bank Code
var bankStore=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getBanks',

    id: 'ID',//
    baseParams:{
        task: "getBanks"
    },//This
    fields:['bankID','BankName']
});
bankStore.load()

var branchStore=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getBranch',
    id: 'branchID',//
    baseParams:{
        task: "getBranch"
    },//This
    fields:['BranchID','BankBranch']
});
branchStore.load();

var bankCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'bankID',
    name:"bankID",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Bank',
    store: bankStore,
    valueField: 'bankID',
    displayField: 'BankName',
    listeners: {
        select: function(f,r,i){
            branchStore.load({
                params:{
                    bankID: bankCombo.getValue()
                }
            });
        }
    }
});


var branchCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'branchID',
    name:"branchID",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
//    fieldLabel:'Payment',
    store: branchStore,
    valueField: 'BranchID',
    displayField: 'BankBranch'
});





var empNumbers={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:500,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [
    {
        xtype: 'textfield',
        id: 'ID_No',
        fieldLabel: 'ID No',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Pin_No',
        fieldLabel: 'Pin No',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'NHIF_No',
        fieldLabel: 'NHIF No',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'NSSF_NO',
        fieldLabel: 'NSSF NO',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    }
    ,
   bankCombo,
   branchCombo,
    {
        xtype: 'textfield',
        id: 'Account_No',
        name: 'Account_No',
        fieldLabel: 'Account No',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false
    }
    ]
};

var emppTypeStore=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getEmpTypes',

    id: 'ID',//
    baseParams:{
        task: "getEmpType"
    },//This
    fields:['ID','EmpType']
});

emppTypeStore.load()

var emptypeCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'emptype',
    name:"emptype",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Employment Type',
    store: emppTypeStore,
    valueField: 'ID',
    displayField: 'EmpType'
});


var empType={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:300,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [emptypeCombo,
    new Ext.form.DateField({
        name: 'AppointDate',
        id: 'AppointDate',
        fieldLabel: 'Start Date',
        width:130
    }),
    new Ext.form.DateField({
        fieldLabel: 'End Date',
        name: 'AppointEndDate',
        id: 'AppointEndDate',
        width:130
    }),
    new Ext.form.DateField({
        id: 'ConfirmDate',
        name: 'ConfirmDate',
        fieldLabel: 'Confirmation Date',
        width:130
    }),
    {
        xtype: 'checkbox',
        id: 'Housed',
        fieldLabel: '',
        labelSeparator: '',
        boxLabel: 'Housed'
    },
    {
        xtype: 'checkbox',
        id: 'Unionised',
        fieldLabel: '',
        labelSeparator: '',
        boxLabel: 'Unionised'

    }
    ]
};

var maritalCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'MaritalStatus',
    name:'MaritalStatus',
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Marital Status',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'Single','Single'], [2, 'Married', 'Married'], [3, 'Divorced', 'Divorced'], [4, 'Widowed', 'Widowed'], [5, 'Separated', 'Separated']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText'
});

var genderCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'sex',
    name:'sex',
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local',
    fieldLabel:'Gender',
    store: new Ext.data.SimpleStore({
        fields: [
        'myID',
        'dispValue',
        'displayText'
        ],
        data: [[1, 'M','Male'], [2, 'F', 'Female']]
    }),
    valueField: 'dispValue',
    displayField: 'displayText'
});



var empPersonal={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:300,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [{
        xtype: 'textfield',
        id: 'Nationality',
        fieldLabel: 'Nationality',
        allowBlank: true,
        msgTarget:'side',
        validationEvent:false,
        value:'Kenyan'
    },
    maritalCombo,
    new Ext.form.DateField({
        id: 'dob',
        name: 'dob',
        fieldLabel: 'Date of Birth',
        width:130
    }),
   genderCombo
    ]
};

function getTax(oGrid_Event) {
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'grid-editor-mysql-php.php',
        params: {
            task: "calcTax", //pass task to do to the server script
            price: oGrid_Event.value//the updated value
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myTax = responseData.tax;
            oGrid_Event.record.set('tax',myTax);
            ds.commitChanges();
        }//end success block
    }//end request config
    ); //end request
} //end getTax


var createColModel=function(finish,start){
    var columns = [
    {
        id:'PID',
        header: "PID",
        width: 55,
        sortable: true,
        dataIndex: 'PID',
        filterable: true
    },
    {
        header: "First Name",
        width: 80,
        sortable: true,
        dataIndex: 'FirstName',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "Last name",
        width: 80,
        sortable: true,
        dataIndex: 'LastName',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "SurName",
        width: 80,
        sortable: true,
        dataIndex: 'SurName',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Branch",
        width: 80,
        sortable: true,
        dataIndex: 'empBranch',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Department",
        width: 80,
        sortable: true,
        dataIndex: 'Department',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "Role",
        width: 80,
        sortable: true,
        dataIndex: 'JobTitle',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },{
        header: "Job Group",
        width: 80,
        sortable: true,
        dataIndex: 'jobGroup',
        filter: {
            type: 'string'
        // specify disabled to disable the filter menu
        //, disabled: true
        }
    },
    {
        header: "Box",
        width: 80,
        sortable: true,
        dataIndex: 'Box'
    },
    {
        header: "PostalCode",
        width: 80,
        sortable: true,
        dataIndex: 'PostalCode'
    },
    {
        header: "Phone",
        width: 80,
        sortable: true,
        dataIndex: 'Phone'
    },
    {
        header: "Town",
        width: 80,
        sortable: true,
        dataIndex: 'Town'
    },
    {
        header: "Email",
        width: 80,
        sortable: true,
        dataIndex: 'Email'

    },
    {
        header: 'ID_No',
        dataIndex: 'ID_No',
        width: 80,
        sortable: true
    },
    {
        header: 'Pin_No',
        dataIndex: 'Pin_No',
        width: 80,
        sortable: true
    },
    {
        header: 'NHIF_No',
        dataIndex: 'NHIF_No',
        width: 80,
        sortable: true
    },
    {
        header: 'NSSF_NO',
        dataIndex: 'NSSF_NO',
        width: 80,
        sortable: true
    },
    {
        header: 'Bank_Name',
        dataIndex: 'NSSF_NO',
        width: 80,
        sortable: true
    },
    {
        header: 'NSSF_NO',
        dataIndex: 'NSSF_NO',
        width: 80,
        sortable: true
    },
    {
        header: 'Housed',
        dataIndex: 'housed',
        width: 80,
        sortable: true
    },
    {
        header: 'house Allowance',
        dataIndex: 'hseAllowance',
        width: 80,
        sortable: true
    },
    {
        header: 'Pension',
        dataIndex: 'pension',
        width: 80,
        sortable: true
    },
    {
        header: 'Unionised',
        dataIndex: 'unionised',
        width: 80,
        sortable: true
    },
    {
        header: 'Union',
        dataIndex: 'union',
        width: 80,
        sortable: true
    },
    {
        header: 'Sacco',
        dataIndex: 'sacco',
        width: 80,
        sortable: true
    },
    {
        header: 'Risk Allowance',
        dataIndex: 'riskAllowance',
        width: 80,
        sortable: true
    },
    {
        header: 'Gratuity',
        dataIndex: 'gratuity',
        width: 80,
        sortable: true
    },
    {
        header: 'bankID',
        dataIndex: 'bankID',
        width: 80,
        sortable: true
    },
    {
        header: 'branchID',
        dataIndex: 'branchID',
        width: 80,
        sortable: true
    }
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var empDepartment=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getEmpDept',

    id: 'ID',//
    baseParams:{
        task: "getEmpDept"
    },//This
    fields:['ID','DeptName']
});

//empDepartment.load()

var empDeptCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'Department',
    name:'Department',
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Department',
    store: empDepartment,
    valueField: 'ID',
    displayField: 'DeptName'
});

empDepartment.load();

var empBranchStore=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getEmpBranch',
    id: 'ID',//
    baseParams:{
        task: "getEmpBranch"
    },//This
    fields:['empBranch']
});


var empBranchCombo = new Ext.form.ComboBox({
    typeAhead: true,
    id:'empBranch',
    name:"empBranch",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Branch',
    store: empBranchStore,
    valueField: 'empBranch',
    displayField: 'empBranch'
});

empBranchStore.load()

var empTitleStore=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getEmpTitle',

    id: 'ID',//
    baseParams:{
        task: "getEmpTitle"
    },//This
    fields:['ID','JobTitle']
});

empTitleStore.load()

var empTitle = new Ext.form.ComboBox({
    typeAhead: true,
    id:'JobTitle',
    name:"JobTitle",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Job Title',
    store: empTitleStore,
    valueField: 'ID',
    displayField: 'JobTitle'
});

//--------------------------------------------------
//job group combo
//--------------------------------------------------
var empJobGroupStore=new Ext.data.JsonStore({
    url: 'comboFields.php',
    root: 'getJobGroup',

    id: 'ID',//
    baseParams:{
        task: "getJobGroup"
    },//This
    fields:['jcode','jname']
});

empJobGroupStore.load()

var empJobGroup = new Ext.form.ComboBox({
    typeAhead: true,
    id:'jobGroup',
    name:"jobGroup",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Job Group',
    store: empJobGroupStore,
    valueField: 'jcode',
    displayField: 'jname'
});

//===================================================
//end of jog group
//----------------------------------------------------

var gridForm = new Ext.FormPanel({
    id: 'employees',
    frame: true,
    labelAlign: 'left',
    renderto:'register',
    title: 'Employees data',
    bodyStyle:'padding:5px',
    url:'saveEmployee.php',
    width: 500,
    height:600,
    record: null,
    layout: 'column',    // Specifies that the items will now be arranged in columns
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            xtype: 'grid',
            id:'grid',
            store: empreader,
            colModel: createColModel(4),
            tbar: [{
                tag: 'input',
                type: 'text',
                size: '30', 
                value: '',
                style: 'background: #F0F0F9;'
            },{
                text: 'Update',
                iconCls:'remove'
            }, '->', // next fields will be aligned to the right
            {
                text: 'Refresh',
                tooltip: 'Click to Refresh the table',
                handler: refreshGrid,
                iconCls:'refresh'
            },{
                text: 'Clear Filter Data',
                handler: function () {
                    grid.filters.clearFilters();
                }
            },{
                text: 'Delete Employee',
                 handler :handleDelEmp
            },{
                text: 'Reconfigure Grid',
                handler: function () {
                    grid.reconfigure(empreader, createColModel(6));
                }
            }],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        Ext.getCmp("employees").getForm().loadRecord(rec);
                    }
                }
            }),
            //                autoExpandColumn: 'FirstName',
            height: 580,
            title:'Employees Data',
            border: true,
            listeners: {
                render: {
                    fn: function(g){
                        empreader.load({
                            params: {
                                start: 0,
                                limit: 30
                            }
                        });
                        g.getSelectionModel().selectRow(0);
                            delay: 10
                    }

                }
            // Allow rows to be rendered.
            },

            plugins: [filters],
            bbar: new Ext.PagingToolbar({
                store: empreader,
                pageSize: 30,
                plugins: [filters]

            })
           
        }
    },{
        columnWidth: 0.6,
        xtype: 'fieldset',
        title:'Employee details',
        autoHeight: true,
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: false,
        style: {
            "margin-left": "10px", // when you add custom margin in IE 6...
            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'PID',
            name: 'PID'
        },{
            xtype: 'textfield',
            fieldLabel: 'First Name',
            name: 'FirstName'
        },{
            xtype: 'textfield',
            fieldLabel: 'Last Name',
            name: 'LastName'
        },{
            xtype: 'textfield',
            fieldLabel: 'Surname',
            name: 'SurName'
        },empBranchCombo,empDeptCombo,empTitle,empJobGroup,{
            xtype: 'tabpanel',
            activeTab:0,
            height:300,
            items: [{
                title: 'Employee Addresses',
                items:[addresses]
            },{
                title: 'Employee Numbers',
                items:[empNumbers]
            },{
                title: 'Employement Details',
                items:[empType]
            },{
                title: 'Personal Info',
                items:[empPersonal]
            }//,{
                //title: 'Payments',
               // items:[empMoney]
            //}
        ]
        }],
        buttons: [{
            text: 'Save',
            iconCls:'icon-save',
            handler: function(){
                gridForm.form.submit({
                    url:'saveEmployee.php', //php
                    baseParams:{
                        task: "create"
                    },
                    waitMsg:'Saving Data...',
                    success: function (form, action) {
                        Ext.MessageBox.alert('Message', 'Saved OK');

                    },
                    failure:function(form, action) {
                        Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                    }
                })
            //                refreshGrid();
            }
        },{
            text: 'Cancel'
        },{
            text: 'Create',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Reset',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Delete',
            iconCls:'silk-user-add',
            handler :handleDelEmp
        }]//,
    
    }]

});

function handleDelEmp() {

    //returns array of selected rows ids only
//    var selectedKeys = grid.selModel.selections.keys;
    var spid=gridForm.getForm().findField('PID').value
    if(spid.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to Remove the Employee?', deleteEmp);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
} // end

function deleteEmp(btn) {
    var spid=gridForm.getForm().findField('PID').value
    if(btn=='yes')
    {
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getEmpPayments.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "deleteEmps", //pass task to do to the server script
                ID: spid,//the unique id(s)
                key: spid//pass to server same 'id' that the reader used
            },
            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);
                    Ext.MessageBox.alert('OK',' Employee diactivated successfully.');
                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                empreader.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    }//end if click 'yes' on button
} // end deleteRecord


function refreshGrid() {
    empreader.reload();//
} // end refresh

empreader.load();
    //  Create Panel view code. Ignore.
//    createCodePanel('form-grid.js', 'View code to create this Form');