
Ext.QuickTips.init();

// turn on validation errors beside the field globally
Ext.form.Field.prototype.msgTarget = 'side';

var fm = Ext.form;


var empreader=new Ext.data.JsonStore({
    url: 'getEmployee.php',
    root: 'Employees',
    fields:['PID','FirstName','LastName','SurName','Department','JobTitle','Box','PostalCode','Phone','Town','Email','ID_No',
    'Pin_No','NHIF_No','NSSF_NO','Bank_Name','Account_No','basicpay','nssf','nhif','paye','relief','emptype','AppointDate','AppointEndDate',
    'ConfirmDate','Nationality','MaritalStatus','dob','sex','hseAllowance','union','pension','sacco','housed','unionised','riskAllowance','gratuity']
});
/*    Here is where we create the Form
 */
var addresses={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    width:540,
    minHeight:400,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [
    {
        xtype: 'textfield',
        id: 'Box',
        fieldLabel: 'Addresses',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'PostalCode',
        fieldLabel: 'Postal Code',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'Phone',
        fieldLabel: 'Phone',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'Town',
        fieldLabel: 'Town',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Email',
        fieldLabel: 'Email',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
};

var empNumbers={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:500,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [
    {
        xtype: 'textfield',
        id: 'ID_No',
        fieldLabel: 'ID No',
        allowBlank: true,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'Pin_No',
        fieldLabel: 'Pin No',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'NHIF_No',
        fieldLabel: 'NHIF No',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'NSSF_NO',
        fieldLabel: 'NSSF NO',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ,
    {
        xtype: 'textfield',
        id: 'Bank_Name',
        fieldLabel: 'Bank Name/Brach',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'Account_No',
        fieldLabel: 'Bank A/C No',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
};


var empType={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:500,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [{
        xtype: 'textfield',
        id: 'emptype',
        fieldLabel: 'Employment Type',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    new Ext.form.DateField({
        name: 'AppointDate',
        id: 'AppointDate',
        fieldLabel: 'Start Date',
        width:130
    }),
    new Ext.form.DateField({
        fieldLabel: 'Load Date',
        name: 'AppointEndDate',
        id: 'AppointEndDate',
        width:130
    }),
    new Ext.form.DateField({
        id: 'ConfirmDate',
        name: 'ConfirmDate',
        fieldLabel: 'Confirmation Date',
        width:130
    }),
    {
        xtype: 'checkbox',
        id: 'Housed',
        fieldLabel: '',
        labelSeparator: '',
        boxLabel: 'Housed'
    },
    {
        xtype: 'checkbox',
        id: 'Unionised',
        fieldLabel: '',
        labelSeparator: '',
        boxLabel: 'Unionised'

    }
    ]
};


var empPersonal={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:500,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [{
        xtype: 'textfield',
        id: 'Nationality',
        fieldLabel: 'Nationality',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'MaritalStatus',
        fieldLabel: 'Marital Status',
        allowBlank: false,
        msgTarget:'side'
    },
    new Ext.form.DateField({
        id: 'dob',
        name: 'dob',
        fieldLabel: 'Date of Birth',
        width:130
    }),
    {
        xtype: 'textfield',
        id: 'sex',
        fieldLabel: 'Gender',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
};

function getTax(oGrid_Event) {
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {
        url: 'grid-editor-mysql-php.php',
        params: {
            task: "calcTax", //pass task to do to the server script
            price: oGrid_Event.value//the updated value
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },//end failure block
        success:function(response,options){
            var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
            var myTax = responseData.tax;
            oGrid_Event.record.set('tax',myTax);
            ds.commitChanges();
        }//end success block
    }//end request config
    ); //end request
}; //end getTax

var empMoney={
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    frame:true,
    minWidth:540,
    minHeight:500,
    waitMsgTarget: true,
    xtype: 'fieldset',
    items: [{
        xtype: 'textfield',
        id: 'basicpay',
        fieldLabel: 'BasicPay',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'nssf',
        fieldLabel: 'NSSF',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'nhif',
        fieldLabel: 'NHIF',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'paye',
        fieldLabel: 'PAYE',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'relief',
        fieldLabel: 'Relief',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {//'hseAllowance','union','pension','sacco'
        xtype: 'textfield',
        id: 'hseAllowance',
        fieldLabel: 'House Allowance',
        allowBlank: false,
        msgTarget:'side'
    },
    {
        xtype: 'textfield',
        id: 'union',
        fieldLabel: 'Union',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'pension',
        fieldLabel: 'Pension',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'gratuity',
        fieldLabel: 'gratuity',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'riskAllowance',
        fieldLabel: 'Risk Allowance',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    },
    {
        xtype: 'textfield',
        id: 'sacco',
        fieldLabel: 'Sacco',
        allowBlank: false,
        msgTarget:'side',
        validationEvent:false
    }
    ]
};

var gridForm = new Ext.FormPanel({
    id: 'employees',
    frame: true,
    labelAlign: 'left',
    title: 'Employees data',
    bodyStyle:'padding:5px',
    url:'saveEmployee.php',
    width: 500,
    height:600,
    record: null,
    layout: 'column',    // Specifies that the items will now be arranged in columns
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            xtype: 'grid',
            store: empreader,
            columns:[
            {
                id:'PID',
                header: "PID",
                width: 55,
                sortable: true,
                dataIndex: 'PID'
            },
            {
                header: "First Name",
                width: 80,
                sortable: true,
                dataIndex: 'FirstName'
            },
            {
                header: "Last name",
                width: 80,
                sortable: true,
                dataIndex: 'LastName'
            },
            {
                header: "SurName",
                width: 80,
                sortable: true,
                dataIndex: 'SurName'
            },
            {
                header: "Department",
                width: 80,
                sortable: true,
                dataIndex: 'Department'
            },
            {
                header: "Role",
                width: 80,
                sortable: true,
                dataIndex: 'JobTitle'
            },
            {
                header: "Box",
                width: 80,
                sortable: true,
                dataIndex: 'Box'
            },
            {
                header: "PostalCode",
                width: 80,
                sortable: true,
                dataIndex: 'PostalCode'
            },
            {
                header: "Phone",
                width: 80,
                sortable: true,
                dataIndex: 'Phone'
            },
            {
                header: "Town",
                width: 80,
                sortable: true,
                dataIndex: 'Town'
            },
            {
                header: "Email",
                width: 80,
                sortable: true,
                dataIndex: 'Email'

            },
            {
                header: 'ID_No',
                dataIndex: 'ID_No',
                width: 80,
                sortable: true
            },
            {
                header: 'Pin_No',
                dataIndex: 'Pin_No',
                width: 80,
                sortable: true
            },
            {
                header: 'NHIF_No',
                dataIndex: 'NHIF_No',
                width: 80,
                sortable: true
            },
            {
                header: 'NSSF_NO',
                dataIndex: 'NSSF_NO',
                width: 80,
                sortable: true
            },
            {
                header: 'Bank_Name',
                dataIndex: 'NSSF_NO',
                width: 80,
                sortable: true
            },
            {
                header: 'NSSF_NO',
                dataIndex: 'NSSF_NO',
                width: 80,
                sortable: true
            },
            {
                header: 'Housed',
                dataIndex: 'housed',
                width: 80,
                sortable: true
            },
            {
                header: 'house Allowance',
                dataIndex: 'hseAllowance',
                width: 80,
                sortable: true
            },
            {
                header: 'Pension',
                dataIndex: 'pension',
                width: 80,
                sortable: true
            },
            {
                header: 'Unionised',
                dataIndex: 'unionised',
                width: 80,
                sortable: true
            },
            {
                header: 'Union',
                dataIndex: 'union',
                width: 80,
                sortable: true
            },
            {
                header: 'Sacco',
                dataIndex: 'sacco',
                width: 80,
                sortable: true
            },
            {
                header: 'Risk Allowance',
                dataIndex: 'riskAllowance',
                width: 80,
                sortable: true
            },
            {
                header: 'Gratuity',
                dataIndex: 'gratuity',
                width: 80,
                sortable: true
            }
            ],
            tbar: [{
                text: 'Update',
                iconCls:'remove'
            }, '->', // next fields will be aligned to the right
            {
                text: 'Refresh',
                tooltip: 'Click to Refresh the table',
                handler: refreshGrid,
                iconCls:'refresh'
            }],
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        Ext.getCmp("employees").getForm().loadRecord(rec);
                    }
                }
            }),
            //                autoExpandColumn: 'FirstName',
            height: 350,
            title:'Employees Data',
            border: true,
            listeners: {
                render: function(g) {
                    g.getSelectionModel().selectRow(0);
                },
                delay: 10 // Allow rows to be rendered.
            }
        }
    },{
        columnWidth: 0.6,
        xtype: 'fieldset',
        title:'Employee details',
        autoHeight: true,
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: false,
        style: {
            "margin-left": "10px", // when you add custom margin in IE 6...
            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'PID',
            name: 'PID'
        },{
            xtype: 'textfield',
            fieldLabel: 'First Name',
            name: 'FirstName'
        },{
            xtype: 'textfield',
            fieldLabel: 'Last Name',
            name: 'LastName'
        },{
            xtype: 'textfield',
            fieldLabel: 'Surname',
            name: 'SurName'
        },{
            xtype: 'textfield',
            fieldLabel: 'Department',
            name: 'Department'
        }, {
            xtype: 'textfield',
            fieldLabel: 'JobTitle',
            name: 'JobTitle'
         
        },{
            xtype: 'tabpanel',
            activeTab:0,
            items: [{
                title: 'Employee Addresses',
                items:[addresses]
            },{
                title: 'Employee Numbers',
                items:[empNumbers]
            },{
                title: 'Employement Details',
                items:[empType]
            },{
                title: 'Personal Info',
                items:[empPersonal]
            },{
                title: 'Payments',
                items:[empMoney]
            }]
        }],
        buttons: [{
            text: 'Save',
            iconCls:'icon-save',
            handler: function(){
                gridForm.form.submit({
                    url:'saveEmployee.php', //php
                    baseParams:{
                        task: "create"
                    },
                    waitMsg:'Saving Data...',

                    success: function (form, action) {
                        Ext.MessageBox.alert('Message', 'Saved OK');
                       
                    },
                    failure:function(form, action) {
                        Ext.MessageBox.alert('Message', 'Save failed ');
                    }
                });
                refreshGrid();
            }
        },{
            text: 'Cancel'
        },{
            text: 'Create',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Reset',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        }],
        onUpdate : function(btn, ev) {
            if (gridForm.record == null) {
                return;
            }
            if (!gridForm.getForm().isValid()) {
                Ext.MessageBox.alert(false, "Form is invalid.");
                return false;
            }
            gridForm.getForm().updateRecord(gridForm.record);
        }
    }]

});
function refreshGrid() {
    empreader.reload();//
}; // end refresh

empreader.load();
    //  Create Panel view code. Ignore.
//    createCodePanel('form-grid.js', 'View code to create this Form');