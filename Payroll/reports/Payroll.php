<?php

/* Care2x Payroll deployment 01-01-2010
 * GNU General Public License
 * Copyright 2010 George Maina
 * georgemainake@gmail.com
 *
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
$slipmont = $_POST[slipMonth];
$sql1 = 'SELECT * FROM proll_company';
$result1 = $db->Execute($sql1);
$coRow = $result1->FetchRow();

$paymonth = $_POST['param1'];
$spid = $_POST['param3'];
$month1 = date_create($_POST['param4']);
$month2 = date_create($_POST['param5']);
$maxSlips = $_POST['param2'];
$dept=$_POST['param6'];


$sql = 'SELECT distinct p.Pid, CONCAT(p.surname ," ", p.firstname ," ", p.lastname) AS empnames,p.department,p.basicpay,c.payMonth,p.pin_no,p.id,c.payDate FROM proll_empRegister p
inner join proll_payments c on p.PID=c.Pid';

if ($paymonth <> '' && $spid == '') {
    $sql.= ' where c.payMonth="' . $paymonth . '"';
} else if ($paymonth <> '' && $spid <> '') {
    $sql.= ' where c.payMonth="' . $paymonth . '" and p.pid="' . $spid . '"';
} else if ($month1 <> '' && $month2 <> '' && $spid == '') {
    $sql.= ' where c.payDate between "' . date_format($month1, 'Y-m-d') . '" and "' . date_format($month2, 'Y-m-d') . '"';
} else if ($month1 <> '' && $month2 <> '' && $spid <> '') {
    $sql.= ' where p.pid="' . $spid . '" and c.payDate between "' . date_format($month1, 'Y-m-d') . '" and "' . date_format($month2, 'Y-m-d') . '"';
//} else if ($month1 == '' && $month2 == '' && $spid == '' && $paymonth <> '' && $dept <> '') {
    //$sql.= ' where p.department="' . $dept . '" and c.payMonth="' . $paymonth . '"';
} else {
    $prevmonth = date('F', strtotime(date('Y-m-j') . "-1 month"));
    $sql.= ' where c.payMonth="' . $prevmonth . '"';
}
$result2 = $db->Execute($sql);
$numRows = $result2->RecordCount();
//echo $sql;
echo "<table border=0 width=100%>";

$max = $maxSlips;
echo "<tr>";
$position = 1;
while ($row2 = $result2->FetchRow()) {

    $coName = strtoupper($coRow['CompanyName']);
    $pid = $row2['Pid'];
    $empnames = $row2['empnames'];
    $payMonth = $row2['payMonth'];
    $dept = $row2['department'];
    $pn_no = $row2['pin_no'];
    $id = $row2['id'];
    $payDates = date_create($row2['payDate']);
    $payDate = date_format($payDates, "Y");
    if ($position == 1) {
        echo "<tr>";
    }

    echo '<td>';
    displaySlip($coName, $pid, $empnames, $pno, $dept, $pn_no, $payMonth, $id, $payDate);
    echo '</td>';

    if ($position == $max) {
        echo "</tr> ";
        $position = 1;
    } else {
        $position++;
    }
}
//$end = "";
//if ($position != 1) {
//    for ($z = (3 - $position); $z > 0; $z--) {
//        $end .= "<td></td>";
//    }
//    $end .= "</tr>";
//}

echo '</table>';

function displaySlip($coName, $pid, $empnames, $pno, $dept, $pn_no, $payMonth, $id, $payDate) {
    global $db;
    echo "<table border=0 cellpadding=2>";
    echo '<tr><td colspan=2><b>' . $coName . '</b></td></tr>';
    echo "<tr><td>Employee   :</td><td><b>" . $pid . ' : ' . $empnames . '</b></td></tr>';
    echo '<tr><td>PAYSLIP    :</td><td><b>' . $payMonth . ' ' . $payDate . '</b></td></tr>';
    echo '<tr><td>Department :</td><td><b>' . $dept . '</b></td></tr>';
    echo '<tr><td>Pin_No     :</td><td><b>' . $pn_no . '</b></td></tr>';
    echo '<tr><td colspan=2>--------------------------------------</td></tr>';
    echo "</table>";

    echo '<table>';
//Earnings
    echo '<tr><td cospan=2><b>Earnings</b><br></td>';
    $sql = 'select a.Pid, a.emp_names,c.id,a.pay_type,a.amount,a.Notes FROM proll_payments a
            inner join proll_paytypes b on a.pay_type=b.Type
            inner join proll_paycategory c on b.CatID=c.ID
            where a.catid in("pay","Relief","benefit") and amount>0 and pid="' . $pid . '" and a.paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();

    while ($row = $result->FetchRow()) {
        echo '<tr><td align=left>' . $row[3] . '</td><td align=right>' . $row[4] . '</td></tr>';
    }

//Tax Calculation
    echo '<tr><td cospan=2><br><b>Tax Calculations</b><br></td>';
//$sql='Select';
    $sql = ' select a.Pid, a.emp_names,c.id,a.pay_type,a.amount,a.Notes FROM proll_payments a
inner join proll_paytypes b on a.pay_type=b.Type
inner join proll_paycategory c on b.CatID=c.ID
where c.id="deduct" and pid="' . $pid . '" and a.paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();

    while ($row = $result->FetchRow()) {
        if ($row[3] == 'NSSF') {
            echo '<tr><td align=left>Less N.S.S.F and Pension</td><td align=right>(' . $row[4] . ')</td></tr>';
            echo '<tr><td align=left>Taxable Pay</td><td align=right>' . intval($row2[3] - $row[4]) . '</td></tr>';
        }
//        echo '<tr><td align=left>Tax Charged</td><td align=right>' . $row[4] . '</td></tr>';
    }
    $pyesql = 'select amount from proll_payments where pid="' . $pid . '" and pay_type="paye" and paymonth="' . $payMonth . '"';
    $pyeresult = $db->Execute($pyesql);
    $pyerow = $pyeresult->FetchRow();
    echo '<tr><td align=left>Tax Charged</td><td align=right>' . $pyerow[0] . '</td></tr>';
//Deductions

    echo '<tr><td cospan=2><br><b>Deductions</b><br></td>';
    $sql = ' select a.Pid, a.emp_names,c.id,a.pay_type,a.amount,a.Notes FROM proll_payments a
inner join proll_paytypes b on a.pay_type=b.Type
inner join proll_paycategory c on b.CatID=c.ID
where a.catid="deduct" and amount>0 and pid="' . $pid . '" and a.paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();

    while ($row = $result->FetchRow()) {
        echo '<tr><td align=left>' . $row[3] . '</td><td align=right>' . $row[4] . '</td></tr>';
    }
    //Summary
    echo '<tr><td cospan=2><br><b>Summary</b><br></td>';
    $sql = 'select pid,sum(amount) as grosspay from proll_payments where catID IN("pay","relief","benefit") and pid="' . $pid . '" and paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    $sumRows = $result->FetchRow();
    echo '<tr><td align=left>Gross Pay</td><td align=right>' . $sumRows[1] . '</td></tr>';

    $sql = 'select pid,sum(amount) as deductions from proll_payments where catID IN("Deduct") and pid="' . $pid . '" and paymonth="' . $payMonth . '"';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    $diffRows = $result->FetchRow();
    echo '<tr><td align=left>Less Deductions</td><td align=right>' . $diffRows[1] . '</td></tr>';

    echo '<tr><td align=left>Net pay</td><td align=right>' . intval($sumRows[1] - $diffRows[1]) . '</td></tr>';

    echo '<tr><td cospan=2><br><br></td>';
    $sql = 'select BankID,Account_no from proll_empregister where pid="' . $pid . '"';
    $result = $db->Execute($sql);
    $AcRows = $result->FetchRow();
    echo '<tr><td align=left>PAYMENT BY:</td><td align=left>Bank</td></tr>';
    echo '<tr><td align=right>:</td><td align=left>' . $AcRows[0] . '</td></tr>';
    echo '<tr><td align=right>:</td><td align=left>' . $AcRows[1] . '</td></tr>';

    echo '</table><br><br>';
    echo '-------------------------------------------------------------------<br>';
    echo '-------------------------------------------------------------------<br><br>';
}

?>
