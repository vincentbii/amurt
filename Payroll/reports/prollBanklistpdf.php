<?php

require ('roots.php');
require ($root_path . 'include/inc_environment_global.php');
$pid = $_REQUEST['pid'];
$bankid=$_REQUEST['bankid'];
$branchID=$_REQUEST['branchID'];
$payMonth=$_REQUEST['payMonth'];
createInvoiceTitle($db,$bankid,$payMonth);

//echo '<br>'.$_REQUEST['bankid'];;
//echo '<br>'.$branchID;
function createInvoiceTitle($db,$bankid,$payMonth) {
    require ('roots.php');
//    require_once 'Zend/Pdf.php';
    require_once '../My_Pdf.php';
    $pdf = new Zend_Pdf ();
    $mpdf=new My_Pdf();
    $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);

    $pageHeight = $page->getHeight();
    $width = $page->getWidth();
    $topPos = $pageHeight - 10;
    $leftPos = 11;
    $config_type = 'main_info_%';
    $sql = "SELECT * FROM proll_company";
    $global_result = $db->Execute($sql);
    if ($global_result) {
        while ($data_result = $global_result->FetchRow()) {
            $company = $data_result ['CompanyName'];
            $address = $data_result ['Address'];
            $town = $data_result ['Town'];
            $postal = $data_result ['Postal'];
            $tel = $data_result ['Phone'];
            $email = $data_result ['email'];
            $topMessage = $data_result ['topMessage'];
            $bottomMessage = $data_result ['bottomMessage'];
        }
        $global_config_ok = 1;
    } else {
        $global_config_ok = 0;
    }

    $title = 'Bank Payment List';

    $headlineStyle = new Zend_Pdf_Style ();
    $headlineStyle->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
    $headlineStyle->setFont($font, 10);
    $page->setStyle($headlineStyle);
    $page->drawText($company, $leftPos + 200, $topPos - 36);
    $page->drawText($address, $leftPos + 200, $topPos - 50);
    $page->drawText($town . ' - ' . $postal, $leftPos + 200, $topPos - 65);
    $page->drawText($tel, $leftPos + 200, $topPos - 80);


    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
    $headlineStyle2 = new Zend_Pdf_Style ();
    $headlineStyle2->setFont($font, 13);
    $page->setStyle($headlineStyle2);
    $page->drawText($title, $leftPos + 200, $topPos - 100);

      $dataStyle = new Zend_Pdf_Style ();
    $dataStyle->setFillColor(new Zend_Pdf_Color_RGB(0.1, 0.1, 0.1));
    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
    $dataStyle->setFont($font, 7);
    $page->setStyle($dataStyle);

    $page->drawText('Date:  ' . date('d-m-Y'), $leftPos + 12, $topPos - 120);

  
    $page->drawText('To the Bank Manager:  ', $leftPos + 12, $topPos - 130);

     //$page->drawText($topMessage, $leftPos + 12, $topPos - 135);

    $mpdf->drawTextBox($page, $topMessage, $leftPos + 12, $topPos - 140,$leftPos+500);

    $headlineStyle4 = new Zend_Pdf_Style ();
    $headlineStyle4->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
    $headlineStyle4->setFont($font, 10);
    $page->setStyle($headlineStyle4);
    $page->drawText('PNO:', $leftPos + 12, $topPos - 200);
    $page->drawText('Name:      ', $leftPos + 36, $topPos - 200);
    $page->drawText('Bank:   ', $leftPos + 160, $topPos - 200);
    $page->drawText('Branch:      ', $leftPos + 300, $topPos - 200);
    $page->drawText('Account No:     ', $leftPos + 400, $topPos - 200);
    $page->drawText('Amount ', $leftPos + 500, $topPos - 200);
    $page->setLineWidth(0.9);
    $page->drawLine($leftPos + 12, $topPos - 205, $leftPos + 550, $topPos - 205, Zend_Pdf_Page::SHAPE_DRAW_STROKE);

    $sql = 'SELECT DISTINCT p.`PID`, p.`FirstName`, p.`Surname`, p.`LastName`, p.`Account_No`, b.`BankName`, e.`BankBranch` FROM proll_empregister p
            inner join proll_banks b on b.bankname=p.bankid
            inner join proll_bankbranches e on e.bankbranch=p.branchid
             WHERE p.bankid  like "'.$bankid.'%"';
    $result = $db->Execute($sql);
//    echo $sql;
    $numRows = $result->RecordCount();

    $dataStyle = new Zend_Pdf_Style ();
    $dataStyle->setFillColor(new Zend_Pdf_Color_RGB(0.1, 0.1, 0.1));
    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
    $dataStyle->setFont($font, 8);
    $page->setStyle($dataStyle);
     $currpoint = 215;
    while ($row = $result->FetchRow()) {
       
        if ($topPos < 220) {
            array_push($pdf->pages, $page);
            $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
            $resultsStyle = new Zend_Pdf_Style ();
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
            $resultsStyle->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
            $resultsStyle->setFont($font, 8);
            $page->setStyle($resultsStyle);
            $pageHeight = $page->getHeight();
            $topPos = $pageHeight -10;
            $currpoint = 15;
          
        }

        $page->drawText($row['PID'], $leftPos + 12, $topPos - $currpoint);
        $page->drawText($row['FirstName'] . ' ' . $row['Surname'] . ' ' . $row['LastName'], $leftPos + 36, $topPos - $currpoint);
        $page->drawText($row['BankName'], $leftPos + 160, $topPos - $currpoint);
        $page->drawText($row['BankBranch'], $leftPos + 300, $topPos - $currpoint);
        $page->drawText($row['Account_No'], $leftPos + 400, $topPos - $currpoint);


        $sql = 'select pid,sum(amount) as grosspay from proll_payments where catID IN("pay","relief","benefit") and pid="' . $row['PID'] . '" and paymonth="'.$payMonth.'"';
        $result2 = $db->Execute($sql);
        $sumRows = $result2->FetchRow();

        $sql = 'select pid,sum(amount) as deductions from proll_payments where catID IN("Deduct") and pid="' . $row['PID'] . '" and paymonth="'.$payMonth.'"';
        $result3 = $db->Execute($sql);
        $diffRows = $result3->FetchRow();

        $page->drawText("Ksh ".number_format(intval($sumRows[1] - $diffRows[1])), $leftPos + 500, $topPos - $currpoint);

        $page->setLineWidth(0.5);
//        $currpoint=$currpoint-15;
        $page->drawLine($leftPos + 12, $topPos - $currpoint-2, $leftPos + 550, $topPos - $currpoint-2, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            $topPos = $topPos - 20;
    }
    $topPos = $topPos - $currpoint;
   if ($topPos < 220) {
            array_push($pdf->pages, $page);
            $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
            $resultsStyle = new Zend_Pdf_Style ();
            $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
            $resultsStyle->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
            $resultsStyle->setFont($font, 8);
            $page->setStyle($resultsStyle);
            $pageHeight = $page->getHeight();
            $topPos = $pageHeight -10;
            $currpoint = 15;
          
        }
      $topPos= $topPos-$currpoint;
      $bottomMessage2='Payroll for Kikuyu Hospital';
     $mpdf->drawTextBox($page, $bottomMessage2, $leftPos + 12, $topPos - $currpoint,$leftPos+500);
     $currpoint=$currpoint+70;
     $page->drawLine($leftPos + 12, $topPos - $currpoint, $leftPos + 100, $topPos - $currpoint, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
     $page->drawLine($leftPos + 400, $topPos - $currpoint, $leftPos + 500, $topPos - $currpoint, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
     $currpoint=$currpoint+10;
     $page->drawText('Finance Manager', $leftPos + 12, $topPos - $currpoint);
     $page->drawText('Administrator', $leftPos + 400, $topPos - $currpoint);

      $topPos = $topPos - 10;
     array_push($pdf->pages, $page);
    header('Content-type: application/pdf');
    echo $pdf->render();
}

?>
