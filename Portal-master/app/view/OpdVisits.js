/*
 * File: app/view/OpdVisits.js
 *
 * This file was generated by Sencha Architect version 4.1.1.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('CarePortal.view.OpdVisits', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.opdvisits',

    requires: [
        'Ext.chart.Chart',
        'Ext.util.Point',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Numeric',
        'Ext.chart.series.Bar',
        'Ext.view.View',
        'Ext.XTemplate'
    ],

    isPortlet: true,
    cls: 'x-portlet',
    itemId: 'clinics',
    closable: true,
    collapsible: true,
    title: 'Outpatient Clinics Statistics',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            draggable: {
                moveOnDrag: false
            },
            dockedItems: [
                {
                    xtype: 'chart',
                    dock: 'top',
                    height: 313,
                    width: 400,
                    shadow: true,
                    animate: true,
                    insetPadding: 20,
                    store: 'ClinicInfoStore',
                    theme: 'Category6',
                    axes: [
                        {
                            type: 'Category',
                            fields: [
                                'Clinic'
                            ],
                            grid: false,
                            position: 'left',
                            title: 'Clinics',
                            categoryNames: [
                                'Clinics'
                            ]
                        },
                        {
                            type: 'Numeric',
                            fields: [
                                'TotalPatients'
                            ],
                            grid: true,
                            position: 'bottom',
                            title: 'Number of Patients',
                            decimals: 0,
                            maximum: 80
                        }
                    ],
                    series: [
                        {
                            type: 'bar',
                            highlight: true,
                            label: {
                                display: 'insideEnd',
                                field: 'TotalPatients',
                                color: 'white',
                                'text-anchor': 'middle'
                            },
                            tips: '{trackMouse: true,width: 140,height: 40,renderer: function(storeItem, item) {this.setTitle(" Occupied Beds");}',
                            axis: 'bottom',
                            xField: 'Clinic',
                            yField: 'TotalPatients'
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'dataview',
                    height: 179,
                    tpl: [
                        '<table class="clinics" border=0 width="100%">',
                        '    <tr><td class="clinics">Clinic</td><td class="clinics">TotalPatients</td>',
                        '        <td class="clinics">Males</td><td class="clinics">Females</td>',
                        '        <td class="clinics">Below 5</td><td class="clinics">Above 5</td></tr>',
                        '    <tpl for=".">',
                        '        <tpl><tr>',
                        '            <td class="clinics">{Clinic}</td><td class="clinics">{TotalPatients}</td>',
                        '            <td class="clinics">{Males}</td><td class="clinics">{Females}</td>',
                        '            <td class="clinics">{Below5}</td><td class="clinics">{Above5}</td></tr>',
                        '        </tpl>',
                        '    </tpl>',
                        '</table>',
                        '<div class="x-clear"></div>'
                    ],
                    itemSelector: 'div.SelectedClinicsValues',
                    store: 'ClinicInfoStore'
                }
            ]
        });

        me.callParent(arguments);
    }

});