/*
 * File: app/view/PortalPanel.js
 *
 * This file was generated by Sencha Architect version 4.1.1.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('CarePortal.view.PortalPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.portalpanel',

    requires: [
        'CarePortal.view.OpdVisits',
        'CarePortal.view.WardOccupancy',
        'CarePortal.view.Announcements',
        'Ext.panel.Panel'
    ],

    cls: 'x-portal',
    autoScroll: true,
    layout: 'column',
    bodyCls: 'x-portal-body',
    manageHeight: false,

    initComponent: function() {
        var me = this;

        me.addEvents(
            'validatedrop',
            'beforedragover',
            'dragover',
            'beforedrop',
            'drop'
        );

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    columnWidth: 0.33333,
                    cls: 'x-portal-column',
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'opdvisits'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    columnWidth: 0.33333,
                    cls: 'x-portal-column',
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'wardoccupancy',
                            frame: true,
                            bodyCls: 'x-portal-body'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    columnWidth: 0.33333,
                    cls: 'x-portal-column',
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'announcements',
                            height: 520
                        }
                    ]
                }
            ],
            listeners: {
                afterrender: {
                    fn: me.onPanelAfterRender,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onPanelAfterRender: function(component, eOpts) {
        this.dd = Ext.create('CarePortal.view.PortalDropZone', this, this.dropConfig);
    }

});