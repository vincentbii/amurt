/*
 * File: app/view/OpdVisits.js
 *
 * This file was generated by Sencha Architect version 4.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 6.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 6.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('CarePortal.view.OpdVisits', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.opdvisits',

    requires: [
        'CarePortal.view.OpdVisitsViewModel',
        'Ext.chart.CartesianChart',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Numeric',
        'Ext.chart.series.Bar',
        'Ext.view.View',
        'Ext.XTemplate'
    ],

    config: {
        isPortlet: true
    },

    viewModel: {
        type: 'opdvisits'
    },
    cls: 'x-portlet',
    draggable: {
        moveOnDrag: false
    },
    itemId: 'clinics',
    closable: true,
    collapsible: true,
    title: 'Outpatient Clinics Statistics',

    dockedItems: [
        {
            xtype: 'cartesian',
            shadow: true,
            dock: 'top',
            height: 313,
            width: 400,
            insetPadding: 20,
            store: 'ClinicInfoStore',
            theme: 'Category6',
            axes: [
                {
                    type: 'category',
                    fields: [
                        'Clinic'
                    ],
                    position: 'left',
                    title: 'Clinics'
                },
                {
                    type: 'numeric',
                    fields: [
                        'TotalPatients'
                    ],
                    grid: true,
                    maximum: 20,
                    title: 'Number of Patients',
                    position: 'bottom'
                }
            ],
            series: [
                {
                    type: 'bar',
                    highlight: true,
                    label: {
                        display: 'insideEnd',
                        field: 'TotalPatients',
                        color: 'white',
                        'text-anchor': 'middle'
                    },
                    xField: 'Clinic',
                    yField: [
                        'TotalPatients'
                    ]
                }
            ]
        }
    ],
    items: [
        {
            xtype: 'dataview',
            height: 179,
            tpl: [
                '<table class="clinics" border=0 width="100%">',
                '    <tr><td class="clinics">Clinic</td><td class="clinics">TotalPatients</td>',
                '        <td class="clinics">Males</td><td class="clinics">Females</td>',
                '        <td class="clinics">Below 5</td><td class="clinics">Above 5</td></tr>',
                '    <tpl for=".">',
                '        <tpl><tr>',
                '            <td class="clinics">{Clinic}</td><td class="clinics">{TotalPatients}</td>',
                '            <td class="clinics">{Males}</td><td class="clinics">{Females}</td>',
                '            <td class="clinics">{Below5}</td><td class="clinics">{Above5}</td></tr>',
                '        </tpl>',
                '    </tpl>',
                '</table>',
                '<div class="x-clear"></div>'
            ],
            itemSelector: 'div.SelectedClinicsValues',
            store: 'ClinicInfoStore'
        }
    ]

});