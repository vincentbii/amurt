<?php
require('./roots.php');
require('include/inc_environment_global.php');
require_once('include/care_api_classes/class_tz_insurance.php');
require_once('include/care_api_classes/class_tz_billing.php');
$insurance_obj = new Insurance_tz;
 $bill_obj = new Bill;
global $db;
$debug = false;
($debug) ? $db->debug = true : $db->debug = FALSE;  //5259



$dtNow = date('Y-m-d');
$disTime = date('H:i:s');
$sql = 'select * from care_encounter where encounter_class_nr=2 and is_discharged=0';
if($debug) echo $sql;
$result = $db->Execute($sql);



while ($row = $result->FetchRow()) {


//    if ($row['encounter_date'] < $dtNow) {
        $sql = "UPDATE `care_encounter` SET `is_discharged` = '1',`discharge_date` = '$dtNow',
            `discharge_time` = '$disTime' 
            WHERE IS_discharged = '0' and encounter_class_nr=2 and 
            pid=$row[pid] and encounter_nr=$row[encounter_nr] AND encounter_date<'$dtNow'";
        if ($db->Execute($sql)) {

            $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($row[pid]);
            $insuCompanyID = $insurance_obj->GetCompanyFromPID2($row[pid]);
            if ($IS_PATIENT_INSURED) {

                 updateIncome($row[pid],$row[encounter_nr],$bill_obj);
                 $bill_obj ->updateDebtorsTrans($row[pid],$insuCompanyID,$row[encounter_nr]);
            }

            echo "Patient $row[pid] discharged successfully <br> ";
        } else {
            echo 'Error discharging patients ' . $sql;
        }
//    }
}


function updateIncome($pid,$encNr,$bill_obj){
    global $db;
    $debug=true;

    $sql="SELECT b.pid,encounter_nr,`ip-op`,bill_date,bill_number,b.`partcode`,b.`rev_code`,
                                b.`service_type`,b.`Description`,b.`total`,b.`status`,d.`gl_sales_acct`
                                FROM care_ke_billing b left join `care_tz_drugsandservices` d on b.`partcode`=d.`partcode`
                                 WHERE b.pid=$pid and encounter_nr=$encNr
                                AND service_type NOT IN('payment','NHIF','NHIF2','NHIF3','NHIF4')";
    if($debug) echo $sql;
    $results=$db->Execute($sql);

    if($bill_obj->checkIncomeTrans('invoice')){
        while($row=$results->FetchRow()){
//                                $glCode=$bill_obj->getItemGL($rev_code);
            $bill_obj->updateIncomeTrans($row[gl_sales_acct],$row[total],$row[bill_date],$row[bill_number],'+','invoice');
        }
    }
}



?>
<!--<script>
    window.close();
</script>-->