<?php
$LDAdmitHistoPhysical='Admission History and Physical';
$LDDoctorDailyNotes='Doctor\'s daily notes';
$LDDischargeSummary="Doctors Notes";
$LDConsultNotes='Consultation notes';
$LDOpNotes='Operation notes';
$LDDailyNurseNotes='Daily ward\'s notes';
$LDOther='Other notes';
$LDChartNotes='Chart notes';
$LDPTATGetc='PT, ATG, etc. daily notes';
$LDIVDailyNotes='Treatment';
$LDAnticoagDailyNotes='HPI Notes';
$LDMaterialLOTChargeNr='Physical Examination Findings';
$LDDiagnosis='Other System Findings';
$LDTherapy='Therapy text';
$LDExtraNotes='Final Diadnosis(es)';
$LDNursingReport='Nursing report';
$LDNursingProblemReport='Nursing problem report';
$LDNursingEffectivityReport='Nursing effectivity report';
$LDInquiryToDoctor='Systemic Enquiry';
$LDDoctorDirective='Investigations and Results';
$LDProblem='Problems';
$LDDevelopment='Main System Findings';
$LDAllergy='Allergy emr';
$LDDietPlan='Impression';
$LDPrintPDFDocAllReport='PDF document of all reports';
?>
