<?php
require('./roots.php');
require('../include/inc_environment_global.php');
require_once('../include/care_api_classes/class_tz_insurance.php');
require_once('../include/care_api_classes/class_tz_billing.php');
$insurance_obj = new Insurance_tz;
 $bill_obj = new Bill;
 
global $db;
$debug = false;

     $sql="SELECT b.pid,encounter_nr,`ip-op`,bill_date,bill_number,SUM(total) AS total,debtorUpdate,id,p.`insurance_ID`
     FROM care_ke_billing b LEFT JOIN care_person p ON b.`pid`=p.`pid` WHERE service_type
    NOT IN('payment','NHIF','NHIF2','NHIF3','NHIF4') AND p.`insurance_ID`<>'-1' and debtorUpdate=0
        AND bill_date BETWEEN'2016-04-01' AND '2016-04-30'  AND `ip-op`=2 GROUP BY encounter_nr";


    if ($debug) echo $sql;
    $result = $db->Execute($sql);

    while($row = $result->FetchRow()) {

        $insuCompanyID = $insurance_obj->GetCompanyFromPID2($row[pid]);

        $sql2 = "SELECT pid,encounter_nr,`ip-op`,bill_date,bill_number,SUM(total) AS total,debtorUpdate,id
            FROM care_ke_billing WHERE pid=$row[pid] and encounter_nr=$row[encounter_nr] and debtorUpdate=0
            AND service_type IN('payment','NHIF','NHIF2','NHIF3','NHIF4') AND `ip-op`=2
            GROUP BY encounter_nr";

        $result2 = $db->Execute($sql2);
        $row2 = $result2->FetchRow();
        $payments = $row2[total];


        $transType = 2; //Invoices
        $trnsNo = $bill_obj->getTransNo($transType);

        if ($transType == 2) {
            $amount = $row[total] - $payments;
        }

        $sql4 = "insert into `care_ke_debtortrans`
                                            (`transno`,`transtype`,`accno`, `pid`,`transdate`,`bill_number`,`amount`,`lastTransDate`,
                                            `lasttransTime`,`settled`,encounter_nr,encounter_class_nr,reference)
                                            values('$trnsNo','$transType','$insuCompanyID', '$row[pid] ','" . $row[bill_date] . "',' $row[bill_number]',
                                            '$amount','" . $row[bill_date] . "','" . date('H:i:s') . "',0,'$row[encounter_nr]','" . $row['ip-op'] . "','$row[id]')";
        if ($debug)
            echo $sql4;

        if ($db->Execute($sql4)) {
            $newTransNo = ($trnsNo + 1);
            $sql3 = "update care_ke_transactionNos set transNo='$newTransNo' where typeid='$transType'";
            if ($debug)
                echo $sql3;
            $db->Execute($sql3);

            $sql5 = "update care_ke_billing set debtorUpdate=1 where pid='$row[pid] ' and encounter_nr='$row[encounter_nr]'";
            if ($debug)
                echo $sql5;
            $db->Execute($sql5);


            $sql6 = "update care_encounter set debtorUpdate=1 where encounter_nr='$row[encounter_nr]'";
            if ($debug)
                echo $sql6;
            $db->Execute($sql6);

            echo "Created transaction for $row[pid] and encounter_nr=$row[encounter_nr] <br>";

        }
    }


?>