<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 10/14/2015
 * Time: 04:23 PM
 */


error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

require_once($root_path . 'include/care_api_classes/class_tz_insurance.php');
require_once($root_path . 'include/care_api_classes/class_tz_billing.php');
$insurance_obj = new Insurance_tz;
$bill_obj = new Bill;

$limit = $_REQUEST[limit];
$start = $_REQUEST[start];

$task = ($_REQUEST['task']) ? ($_REQUEST['task']) : '';

$regDate1 = new DateTime($_POST[dateJoined]);
$dateJoined = $regDate1->format('Y/m/d');


$debtorFormStat=$_POST[formStatus];
$AllocationItems=$_REQUEST[transNos];

if (isset($_POST[debtorStatus])) {
    $suspended = 1;
} else {
    $suspended = 0;
}

$accno = ($_REQUEST['accno']) ? ($_REQUEST['accno']) : ($_POST['accno']);
$debtorParams = $_POST[params];

switch ($task) {

    case "getPatientsList":
        getPatientsList($start, $limit);
        break;

    case "saveinitial":
         saveInitialEncounter($_POST);
        break;
    case "getEncountersList":
        getEncountersList($start, $limit);
        break;

    default:
        echo "{failure:true}";
        break;
}//end switch

function getEncountersList(){
    global $db;
    $debug=false;

    $sql = "SELECT c.`pid`,c.`name_first`, c.`name_2`, c.`name_last`, c.`date_birth`, c.`sex`,e.`name_formal`, d.`encounter_date`,d.encounter_nr,d.encounter_time
    FROM care_person c
inner join care_encounter d on c.pid=d.pid inner join care_department e on e.nr=d.current_dept_nr
where e.`type`=1 and d.encounter_class_nr=2 and d.encounter_date=date(now()) AND e.`name_formal`='Hypertension' order by d.encounter_time desc";
//echo $sql;

    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "encounters":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        echo '{"PID":"'. $row[0].'","PatientName":"'. $row[1].' '. $row[2].' '. $row[3].'","Time":"'. $row[encounter_time]
            .'","Sex":"'. $row[5] .'","Clinic":"'. $row[6].'","EncounterNo":"'. $row[encounter_nr].'","DOB":"'. $row[4].'"}';
        if ($counter<>$numRows){
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getPatientsList($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT `PID`,`PatientName`,`FacilityName`,`FacilityID`,`InitialDate`,`UniqueID`,`NationalID`,
   `Address`,`Dob`,`Sex`,`MobileConsent`,`Mobile`,`InputUser` FROM `care_hha_patients`";

//    $sql.=" limit $start,$limit";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
    "total":"' . $total . '","patientslist":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        echo '{"PID":"' . $row[PID] . '","PatientName":"' . trim($row[PatientName]) . '","Dob":"' . $row[Dob]
            . '","Sex":"' . $row[Sex] . '","InitialDate":"' . $row[InitialDate] . '","UniqueID":"' . $row[UniqueID] . '","NationalID":"' . $row[NationalID]
            . '","Address":"' . $row[Address] . '","FacilityName":"' . $row[FacilityName]
            . '","FacilityID":"' . $row[FacilityID] . '","MobileConsent":"' . $row[MobileConsent] . '","Mobile":"' . $row[Mobile]  .'"}';

        $counter++;
        if ($counter <> $total) {
            echo ',';
        }
    }
    echo ']}';
}

function validateDebtorNo($debtorNo){
    global $db;
    $psql = "select accno from care_ke_debtors where accno='$debtorNo'";
//    echo $psql;
    $presult = $db->Execute($psql);
    $row = $presult->FetchRow();
    $rowCount=$presult->RecordCount();

    return $rowCount;
}

function saveInitialEncounter($encounterDetails) {
    global $db;
    $debug=false;

        unset($encounterDetails['formStatus']);

            $sql2 = "INSERT INTO `care_hha_patients`
                        (`PID`,`PatientName`,`FacilityName`,`FacilityID`,`Date`,`UniqueID`,`NationalID`,
                           `Address`,`Dob`,`Sex`,`MobileConsent`,`Mobile`,`InputUser`)
                    VALUES
                      ( '$encounterDetails[PID]','$encounterDetails[PatientName]','$encounterDetails[FacilityName]','$encounterDetails[FacilityID]'
                        ,'$encounterDetails[ScreeningDate]','$encounterDetails[PID]','$encounterDetails[NationalID]','$encounterDetails[Address]'
                        ,'$encounterDetails[Dob]','$encounterDetails[Sex]','$encounterDetails[MobileConsent]','$encounterDetails[Mobile]',
                        'Admin')";

            if($debug) echo $sql2;

            if ($db->Execute($sql2)) {
                $results = "{success: true}";
            } else {
                $results = "{success: false,errors:{'Encounter':'Could not Create new Encounter '}}";
            }



    echo $results;
}


?>
