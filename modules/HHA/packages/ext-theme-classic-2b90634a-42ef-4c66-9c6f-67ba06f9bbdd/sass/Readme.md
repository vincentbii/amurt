# ext-theme-classic-2b90634a-42ef-4c66-9c6f-67ba06f9bbdd/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-classic-2b90634a-42ef-4c66-9c6f-67ba06f9bbdd/sass/etc
    ext-theme-classic-2b90634a-42ef-4c66-9c6f-67ba06f9bbdd/sass/src
    ext-theme-classic-2b90634a-42ef-4c66-9c6f-67ba06f9bbdd/sass/var
