# ext-theme-classic-2b90634a-42ef-4c66-9c6f-67ba06f9bbdd/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-classic-2b90634a-42ef-4c66-9c6f-67ba06f9bbdd/sass/etc"`, these files
need to be used explicitly.
