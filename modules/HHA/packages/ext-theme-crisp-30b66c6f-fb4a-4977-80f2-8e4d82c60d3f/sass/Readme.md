# ext-theme-crisp-30b66c6f-fb4a-4977-80f2-8e4d82c60d3f/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-crisp-30b66c6f-fb4a-4977-80f2-8e4d82c60d3f/sass/etc
    ext-theme-crisp-30b66c6f-fb4a-4977-80f2-8e4d82c60d3f/sass/src
    ext-theme-crisp-30b66c6f-fb4a-4977-80f2-8e4d82c60d3f/sass/var
