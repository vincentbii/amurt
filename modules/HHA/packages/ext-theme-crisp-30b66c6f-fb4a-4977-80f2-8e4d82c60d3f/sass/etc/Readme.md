# ext-theme-crisp-30b66c6f-fb4a-4977-80f2-8e4d82c60d3f/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-crisp-30b66c6f-fb4a-4977-80f2-8e4d82c60d3f/sass/etc"`, these files
need to be used explicitly.
