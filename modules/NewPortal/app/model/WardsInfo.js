Ext.define('Portal.model.WardsInfo', {
    extend: 'Ext.data.Model',
    alias: 'model.wardsinfo',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'Ward',
            type: 'string'
        },
        {
            name: 'beds',
            type: 'string'
        },
        {
            name: 'occupancy',
            type: 'string'
        }
        ,
        {
            name: 'vacant',
            type: 'string'
        }
        ,
        {
            name: 'occupied',
            type: 'string'
        }
        ,
        {
            name: 'Males',
            type: 'string'
        }
        ,
        {
            name: 'females',
            type: 'string'
        }
    ]
});