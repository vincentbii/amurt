
<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');


$limit = $_REQUEST[limit];
$start = $_REQUEST[start];
$formStatus = $_REQUEST[formStatus];
$searchParam = $_REQUEST[sParam];
$groupID=$_REQUEST[groupID];

$OrderNo=$_REQUEST['OrderNo'];
$OrderStatus=$_REQUEST['OrderStatus'];
$OrderDate=$_REQUEST['OrderDate'];
$Comment=$_REQUEST['Comment'];
$grnno=$_REQUEST[grnno];
$ReceiveDate=$_REQUEST['ReceiveDate'];
$salesOrderNo=$_REQUEST['OrderNo'];


$task = ($_REQUEST['task']) ? ($_REQUEST['task']) : '';

switch ($task) {
    case "getOpdPatients":
        getOpdPatients();
        break;
//    case "getWardInfo":
//        getWardInfo();
//        break;
    default:
        echo "{failure:true}";
        break;
}//end switch
//0727588512

function getWardInfo(){


    $sql = 'SELECT c.`nr`, c.`ward_id`, c.`name` FROM care_ward c';
    $result = $db->Execute($sql);
    $numRows = $result->RecordCount();
    while($row = $result->FetchRow()){
        echo '{"Wards":[';
        getWardInfoDetails($row[0]);
        getWardInfoDetails($row[0]);
        getWardInfoDetails($row[0]);
        getWardInfoDetails($row[0]);
        getWardInfoDetails($row[0]);
        echo ']}';
    }


}

function getWardInfoDetails($wrdNO) {
    global $db;
    $db->debug = 0;
    require('./roots.php');
    require_once($root_path . 'include/care_api_classes/class_ward.php');
    $ward_obj = new Ward;

# Load date formatter
    require_once($root_path . 'include/inc_date_format_functions.php');
    require_once($root_path . 'global_conf/inc_remoteservers_conf.php');
    $ward_nr = $wrdNO;
    if ($ward_info = &$ward_obj->getWardInfo($ward_nr)) {
        $room_obj = &$ward_obj->getRoomInfo($ward_nr, $ward_info['room_nr_start'], $ward_info['room_nr_end']);
        if (is_object($room_obj)) {
            $room_ok = true;
        } else {
            $room_ok = false;
        }
        # GEt the number of beds
        $nr_beds = $ward_obj->countBeds($ward_nr);
//        echo 'number of beds '.$nr_beds.'<br>';
        # Get ward patients
        if ($is_today)
            $patients_obj = &$ward_obj->getDayWardOccupants($ward_nr);
        else
            $patients_obj= & $ward_obj->getDayWardOccupants($ward_nr, $s_date);
//
//    echo $ward_obj->getLastQuery();
//    echo $ward_obj->LastRecordCount();

        if (is_object($patients_obj)) {
            # Prepare patients data into array matrix
            while ($buf = $patients_obj->FetchRow()) {
                $patient[$buf['room_nr']][$buf['bed_nr']] = $buf;
            }
            $patients_ok = true;
            $occup = 'ja';
        } else {
            $patients_ok = false;
        }

        $ward_ok = true;

        # Create the waiting inpatients' list
        $wnr = (isset($w_waitlist) && $w_waitlist) ? 0 : $ward_nr;
        $waitlist = $ward_obj->createWaitingInpatientList($wnr);
        $waitlist_count = $ward_obj->LastRecordCount();

        # Get the doctor's on duty information
        #### Start of routine to fetch doctors on duty
# If ward exists, show the occupancy list

        if ($ward_ok) {
            if ($pyear . $pmonth . $pday < date('Ymd')) {
//                echo '<b>'.$LDAttention.'</font> '.$LDOldList.'</b>';
                # Prevent adding new patients to the list  if list is old
                $edit = FALSE;
            }

            # Start here, create the occupancy list
            # Assign the column  names
            # Initialize help flags
            $toggle = 1;
            $room_info = array();
            # Set occupied bed counter
            $occ_beds = 0;
            $lock_beds = 0;
            $males = 0;
            $females = 0;
            $cflag = $ward_info['room_nr_start'];

            # Initialize list rows container string
            $sListRows = '';

            # Loop trough the ward rooms

            for ($i = $ward_info['room_nr_start']; $i <= $ward_info['room_nr_end']; $i++) {
                if ($room_ok) {
                    $room_info = $room_obj->FetchRow();
                } else {
                    $room_info['nr_of_beds'] = 1;
                    $edit = false;
                }

                // Scan the patients object if the patient is assigned to the bed & room
                # Loop through room beds


                for ($j = 1; $j <= $room_info['nr_of_beds']; $j++) {
                    //for($j=1;$j<=$nr_beds;$j++){
                    # Reset elements


                    if ($patients_ok) {

                        if (isset($patient[$i][$j])) {
                            $bed = &$patient[$i][$j];
                            $is_patient = true;
                            # Increase occupied bed nr
                            $occ_beds++;
                        } else {
                            $is_patient = false;
                            $bed = NULL;
                        }
                    }

                    if ($is_patient) {
                        $sBuffer = '<a href="javascript:popPic(\'' . $bed['pid'] . '\')">';
                        if (strtolower($bed['sex']) == 'f') {
                            $females++;
                        } elseif (strtolower($bed['sex']) == 'm') {
                            $males++;
                        }
                    }
                }
                # set room nr change flag , toggle row color
                if ($cflag != $i) {
                    $toggle = !$toggle;
                    $cflag = $i;
                }

                # Check if bed is locked
                if (stristr($room_info['closed_beds'], $j . '/')) {
                    $bed_locked = true;
                    $lock_beds++;
                    # Consider locked bed as occupied so increase occupied bed counter
                    $occ_bed++;
                } else {
                    $bed_locked = false;
                }
            } // end of ward loop
            $sql = 'SELECT c.`nr`, c.`ward_id`, c.`name` FROM care_ward c where  c.`nr`="'.$ward_nr.'"';
            $result = $db->Execute($sql);
            $numRows = $result->RecordCount();
            $row = $result->FetchRow();
            # Final occupancy list line
            # Prepare the stations quick info data
            # Occupancy in percent
            $occ_percent = ceil(($occ_beds / $nr_beds) * 100);

            # Nr of vacant beds
            $vac_beds = $nr_beds - $occ_beds;
            echo '{"Ward":"' . $row[2] . '","beds":"' . $nr_beds . '","occupancy":"' . $occ_percent . '%","occupied":' . $occ_beds
                . ' ,"vacant":' . $vac_beds . ',"Males:"' . $males . '","females":"' . $females. '"},';


        }
    }
}


function getOpdPatients(){
    global $db;
    $debug=false;


    $sql = 'SELECT c.`pid`,c.`name_first`, c.`name_2`, c.`name_last`, c.`date_birth`, c.`sex`,e.`name_formal`, d.`encounter_date`,d.encounter_nr,d.encounter_time
    FROM care_person c
inner join care_encounter d on c.pid=d.pid inner join care_department e on e.nr=d.current_dept_nr
where e.`type`=1 and d.encounter_class_nr=2 and d.encounter_date="'.date('Y-m-d').'" order by d.encounter_time desc';

    if($debug) echo $sql;

    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "OutPatients":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        echo '{"Pid":"'. $row[0].'","FirstName":"'. $row[1].'","LastName":"'. $row[2].'","SurName":"'. $row[3].'","EncounterTime":"'. $row[encounter_time]
            .'","Gender":"'. $row[5] .'","Clinic":"'. $row[6].'","EncounterNo":"'. $row[encounter_nr].'","DOB":"'. $row[4].'"}';
        if ($counter<>$numRows){
            echo ",";
        }
        $counter++;
    }
    echo ']}';

}


?>
