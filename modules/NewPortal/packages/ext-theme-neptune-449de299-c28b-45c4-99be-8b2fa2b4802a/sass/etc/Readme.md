# ext-theme-neptune-449de299-c28b-45c4-99be-8b2fa2b4802a/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-449de299-c28b-45c4-99be-8b2fa2b4802a/sass/etc"`, these files
need to be used explicitly.
