<div id="add_parameter" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Add Parameter</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="insert_parameters">
                     		<label>Parameter Group Name</label>  
                          <input required type="text" name="groupid" id="groupid" class="form-control" />  
                          <br />
                          <label>Enter Parameter Name</label>  
                          <input required type="text" name="name" id="name" class="form-control" />  
                          <br />
                          <label>Msr. Unit</label>  
                          <input type="text" name="msr_unit" id="msr_unit" class="form-control" />  
                          <br />

                          <fieldset>
                          	<legend>Male</legend>
                          	<label>Upper Boundary</label>
                          	<input type="text" name="upper_bound_m" id="upper_bound_m" class="form-control">
                          	<br />
                          	<label>Lower Boundary</label>
                          	<input type="text" name="lower_bound_m" id="lower_bound_m" class="form-control">
                          	<br />
                          </fieldset>
                          <fieldset>
                          	<legend>Female</legend>
                          	<label>Upper Boundary</label>
                          	<input type="text" name="upper_bound_f" id="upper_bound_f" class="form-control">
                          	<br />
                          	<label>Lower Boundary</label>
                          	<input type="text" name="lower_bound_f" id="lower_bound_f" class="form-control">
                          	<br />
                          </fieldset>
                          <fieldset>
                          	<legend>Children</legend>
                          	<label>Upper Boundary</label>
                          	<input type="text" name="upper_bound_c" id="upper_bound_c" class="form-control">
                          	<br />
                          	<label>Lower Boundary</label>
                          	<input type="text" name="lower_bound_c" id="lower_bound_c" class="form-control">
                          	<br />
                          </fieldset>

                          <fieldset>
                          	<legend>Price</legend>
                          	<label>Cash Price</label>
                          	<input required type="text" name="cash_price" id="cash_price" class="form-control">
                          	<br />
                          	<label>Credit Price</label>
                          	<input required type="text" name="credit_price" id="credit_price" class="form-control">
                          	<br />
                          </fieldset>
                          <fieldset>
                          	<legend>Input</legend>
                          	<label>Input Type</label>
                          	<select required name="input_type" id="input_type" class="form-control">
                          		<option value="input_box">Input Box</option>
                          		<option value="group_field">Group of Values</option>
                          		<option value="drop_down">Drop Down</option>
                          	</select>
                          	<p>for separate values separate them by ; 
for range separate them with a dash -
</p>
                          	<br />
                          	<label>Input Value</label>
                          	<input type="text" name="input_value" id="input_value" class="form-control">
                          	<br />
                          </fieldset>
                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
 <script type="text/javascript">
 	$(document).on('click', '.add_parameter', function(){  
           var id = $(this).attr("id");  
           $.ajax({  
                url:"fetchParamGroups.php",  
                method:"POST",  
                data:{id:id},  
                dataType:"json",  
                success:function(data){  
                     $('#groupid').val(data.name);
                     $('#insert').val("Update");  
                     $('#add_parameter').modal('show');  
                }  
           });  
      });

 	$('#insert_parameters').on("submit", function(event){  
           event.preventDefault();  
           
                $.ajax({  
                     url:"insert_parameters.php",  
                     method:"POST",  
                     data:$('#insert_parameters').serialize(),  
                     beforeSend:function(){  
                          $('#insert').val("Inserting Parameter...");  
                     },  
                     success:function(data){  
                          $('#insert_parameters')[0].reset();  
                          $('#add_parameter').modal('hide');
                          $('#paramGroups').load(location.href + " #paramGroups");
                          alert(data);
                     }  
                });
      });
 </script>