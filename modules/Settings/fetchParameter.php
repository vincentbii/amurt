<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require('./roots.php');
require($root_path.'include/inc_environment_global.php');
require_once($root_path . "include/inc_img_fx.php");
require_once($root_path . 'include/inc_date_format_functions.php');


define('LANG_FILE','billing.php');
define('NO_CHAIN',1);

require_once($root_path.'include/inc_front_chain_lang.php');

 if(isset($_POST["id"]))  
 {  
      $sql = "SELECT p.id, p.group_id, p.name, p.msr_unit, p.hi_bound, p.lo_bound, p.hi_bound_f, p.lo_bound_f, p.hi_bound_y, p.lo_bound_y, d.unit_price_1, d.unit_price_2, p.field_type, p.add_type, p.item_id FROM care_tz_laboratory_param as p inner join care_tz_drugsandservices as d ON p.item_id = d.partcode WHERE p.id = '".$_POST["id"]."' and p.group_id <> '-1'";
      // $result = mysqli_query($connect, $query);  
      $result = $db->Execute($sql);
      $row = $result->FetchRow();;  
      echo json_encode($row);  
 }elseif (isset($_POST['resultID'])) {
 	$sql = "SELECT resultID, results, unit_msr, hi_bound_m, lo_bound_m, hi_bound_f, lo_bound_f, hi_bound_c, lo_bound_c, input_type, result_values, item_id FROM care_tz_laboratory_resultstypes WHERE resultID = '".$_POST["resultID"]."'";
      // $result = mysqli_query($connect, $query);  
      $result = $db->Execute($sql);
      $row = $result->FetchRow();;  
      echo json_encode($row);  
 }elseif ($_POST['item_id']) {
      $sql = "SELECT p.id, p.group_id, p.name, p.msr_unit, p.hi_bound, p.lo_bound, p.hi_bound_f, p.lo_bound_f, p.hi_bound_y, p.lo_bound_y, d.unit_price_1, d.unit_price_2, p.field_type, p.add_type, p.item_id FROM care_tz_laboratory_param as p inner join care_tz_drugsandservices as d ON p.item_id = d.partcode WHERE p.item_id = '".$_POST["item_id"]."' and p.group_id <> '-1'";
      // $result = mysqli_query($connect, $query);  
      $result = $db->Execute($sql);
      $row = $result->FetchRow();;  
      echo json_encode($row);  
 }
 ?>