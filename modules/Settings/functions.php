<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require('./roots.php');
require($root_path.'include/inc_environment_global.php');
require_once($root_path . "include/inc_img_fx.php");
require_once($root_path . 'include/inc_date_format_functions.php');


define('LANG_FILE','billing.php');
define('NO_CHAIN',1);

require_once($root_path.'include/inc_front_chain_lang.php');

function getDrugClasses(){
	global $db;
	$sql = "SELECT id, name FROM care_tz_drug_classes WHERE status = '1'";
	$result = $db->Execute($sql);
	$dclasses = array();
	$dclassesInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$dclasses[$dclassesInc] = array('id' => $row['id'], 'name' => $row['name']);
			$dclassesInc++;
		}
	}

	$json_array = json_encode($dclasses);
	return $json_array;
}

function allParameterGroups(){
	global $db;

	$sql = "SELECT * FROM care_tz_laboratory_param WHERE group_id = '-1'";
	$result = $db->Execute($sql);
	$groups = array();
	$groupsInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$groups[$groupsInc] = array('name' => $row['name'], 'id' => $row['id']);
			$groupsInc++;
		}
	}

	$json_array = json_encode($groups);
	return $json_array;
}

function testParameterResults(){
	global $db;

	$sql = "SELECT * FROM care_tz_laboratory_resultstypes";
	$result = $db->Execute($sql);
	$groups = array();
	$groupsInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$groups[$groupsInc] = array('results' => $row['results'], 'resultID' => $row['resultID'], 'unit_msr' => $row['unit_msr'], 'hi_bound_m' => $row['hi_bound_m'], 'lo_bound_m' => $row['lo_bound_m'], 'hi_bound_f' => $row['hi_bound_f'], 'lo_bound_f' => $row['lo_bound_f'], 'hi_bound_c' => $row['hi_bound_c'], 'lo_bound_c'=>$row['lo_bound_c'], 'item_id' => $row['item_id']);
			$groupsInc++;
		}
	}

	$json_array = json_encode($groups);
	return $json_array;
}

function testParameters(){
	global $db;

	$sql = "SELECT * FROM care_tz_laboratory_param WHERE group_id <> '-1'";
	$result = $db->Execute($sql);
	$groups = array();
	$groupsInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$groups[$groupsInc] = array('name' => $row['name'], 'id' => $row['id'], 'item_id' => $row['item_id'], 'group_id' => $row['group_id'], 'msr_unit' => $row['msr_unit'], 'hi_bound' => $row['hi_bound'], 'lo_bound' => $row['lo_bound'], 'hi_bound_f' => $row['hi_bound_f'], 'lo_bound_f' => $row['lo_bound_f'], 'hi_bound_y' => $row['hi_bound_y'], 'lo_bound_y'=>$row['lo_bound_y']);
			$groupsInc++;
		}
	}

	$json_array = json_encode($groups);
	return $json_array;

}

function insertService($item_id, $item_description, $unit_price_1, $unit_price_2){
	global $db;

	$sql = "INSERT into care_tz_drugsandservices (partcode, item_number, item_description, item_full_description, purchasing_class, category, unit_price_1, unit_price_2) VALUES ('".$item_id."', '".$item_id."', '".$item_description."', 'LABORATORY', 'LAB', '".$item_description."','".$unit_price_1."', '".$unit_price_2."')";

	$result->db->Execute($sql);
}

function labID(){
	global $db;
	$lsql="SELECT MAX(CAST(SUBSTR(item_id,6)AS UNSIGNED)) as labID FROM `care_tz_laboratory_param` WHERE GROUP_ID<>'-1'";

    $lresult=$db->Execute($lsql);
    $lrow=$lresult->FetchRow();
                         if($lrow<>"" && $lrow<>NULL){
                            $labID=$lrow[0]+1;
                            $labID="LTEST".$labID;
                         }else{
                             $labID="LTEST1";
                         }
     return $labID;
}

function insertParameter($labid, $name, $unit_price_1, $unit_price_2){
	$sql3="INSERT INTO care_tz_drugsandservices
                    (item_number, partcode, is_pediatric, is_adult, is_other, is_consumable, is_labtest, item_description,
                    item_full_description, 	unit_price, unit_price_1, unit_price_2, unit_price_3, purchasing_class,category)
                    VALUES
                    ('".$labID."', '".$labID."', '0', '0', '0', '0', '1',
                    '".$_POST['name']."', '".$_POST['name']."', '".$_POST['price']."', '0', '0', '0',
                    'LABORATORY','38')";
                        $db->Execute($sql3);
}

function insertTest($param_id,$modify_id,$weberp_syncd,$item_id,$groupid,$msr_unit,$upper_bound_m, $lower_bound_m, $upper_bound_f, $lower_bound_f, $upper_bound_c, $lower_bound_c, $name){

	// $sql = "INSERT into care_tz_laboratory_param (id, name, groupid, item_id, msr_unit, hi_bound, lo_bound, hi_bound_f, lo_bound_f, hi_bound_y, lo_bound_y) values ('".$param_id."', '".$name."', '".$groupid."', '".$item_id."', '".$msr_unit."', '".$upper_bound_m."', '".$lower_bound_m."', '".$upper_bound_m."')";

	$sql3="INSERT INTO care_tz_laboratory_param
                    (id, modify_time, weberp_syncd, item_id, groupid, msr_unit, hi_bound, lo_bound,
                    hi_bound_f, lo_bound_f, hi_bound_y, lo_bound_y)
                    VALUES
                    ('".$param_id."', '".$modify_id."', '".$weberp_syncd."', '".$item_id."', '".$groupid."', '".$msr_unit."', '".$upper_bound_m."','".$lower_bound_m."', '".$upper_bound_f."', '".$lower_bound_f."', '".$upper_bound_c."', '".$lower_bound_c."'
                    )";

    $db->Execute($sql3);
}

function cleanString($wild) {
    return ereg_replace("[^[:alnum:]+]","_",$wild);
}

?>