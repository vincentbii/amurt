<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require 'functions.php';

if(!empty($_POST))  
 { 
 	$modify_id = $_SESSION['sess_user_name'];
 	$param_id = "_" . cleanString(strtolower($_POST['name'])) . '__' . strtolower($_POST['groupid']);
 	$message = '';
 	$weberp_syncd='0';
 	$item_id = labID();
 	$category = '38';

     $sql2 = "INSERT into care_tz_laboratory_param (id, name, group_id, msr_unit, weberp_syncd, hi_bound, lo_bound, hi_bound_f, lo_bound_f, hi_bound_y, lo_bound_y, item_id) VALUES ('".$param_id."', '".$_POST['name']."', '".$_POST['groupid']."', '".$_POST['msr_unit']."', '0', '".$_POST['upper_bound_m']."','".$_POST['lower_bound_m']."', '".$_POST['upper_bound_f']."', '".$_POST['lower_bound_f']."', '".$_POST['upper_bound_c']."', '".$_POST['lower_bound_c']."', '".$item_id."')";
    if($db->Execute($sql2)){
    	if(!empty($_POST['input_type'])) {
    		if($_POST['input_type'] === 'drop_down') {
    			$value_type = explode(";",$_POST['input_value']);
    			$inputValues = '';
					foreach($value_type as $arrNr => $value) {
						// $tmp_array['input_value'] 	= $value;
						// $object = $object.$lab_obj->insertDataFromArray($tmp_array);
						if($inputValues == ''){
							$inputValues = $value;
						}else{
							$inputValues = $inputValues.','.$value;
						}
					}
					$sql="UPDATE care_tz_laboratory_param set add_type='".$inputValues."', field_type='drop_down'
                        WHERE id='".$param_id."'";
                    if($db->Execute($sql)){
                    	$message = 'Added Successfully!';
                    }
    		}else{
						$input_value = str_replace(",",".",$_POST['input_value']);

						$sql="UPDATE care_tz_laboratory_param set add_type='".$input_value."', field_type='".$_POST['input_type']."'
                        WHERE id='".$param_id."'";
                        $db->Execute($sql);
                        if($db->Execute($sql)){
	                    	$message = 'Added Successfully!';
	                    }
					
    		}
    	}

        $sql3="INSERT into care_tz_drugsandservices
                    (item_number, partcode, is_pediatric, is_adult, is_other, is_consumable, is_labtest, item_description,
                    item_full_description,  unit_price, unit_price_1, unit_price_2, unit_price_3, purchasing_class,category)
                    values
                    ('".labID()."', '".labID()."', '0', '0', '0', '0', '1',
                    '".$_POST['name']."', '".$_POST['name']."', '0', '".$_POST['cash_price']."', '".$_POST['credit_price']."', '0',
                    'LABORATORY','38')";
        

        if($db->Execute($sql3)){
            $message = 'Service Created Successfully!';
        }

        // $sql_service = "INSERT into care_tz_drugsandservices (partcode, item_number) VALUES ('".$item_id."', '".$item_id."')";

        // $sql_service = "INSERT into care_tz_drugsandservices (partcode, item_number, item_description, item_full_description, purchasing_class, category, unit_price_1, unit_price_2, is_labtest) VALUES ('".$item_id."', '".$item_id."', '".$_POST['name']."', 'LABORATORY', 'LAB', '".$_POST['name']."','".$_POST['cash_price']."', '".$_POST['credit_price']."', 1)";

    // $result_service->db->Execute($sql_service);
    }

 	echo $message;
 }  
 ?>