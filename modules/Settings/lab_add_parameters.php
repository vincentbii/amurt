<div id="add_parameter" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Add Parameter</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="insert_parameters">
                     		<label>Parameter Name</label>  
                          <input required type="text" name="item_id" id="item_id" class="form-control" />  
                          <br />
                          <label>Parameter Result</label>  
                          <input required type="text" name="results" id="results" class="form-control" />  
                          <br />
                          <label>Msr. Unit</label>  
                          <input type="text" name="unit_msr" id="unit_msr" class="form-control" />  
                          <br />

                          <fieldset>
                          	<legend>Male</legend>
                          	<label>Upper Boundary</label>
                          	<input type="text" name="hi_bound_m" id="hi_bound_m" class="form-control">
                          	<br />
                          	<label>Lower Boundary</label>
                          	<input type="text" name="lo_bound_m" id="lo_bound_m" class="form-control">
                          	<br />
                          </fieldset>
                          <fieldset>
                          	<legend>Female</legend>
                          	<label>Upper Boundary</label>
                          	<input type="text" name="hi_bound_f" id="hi_bound_f" class="form-control">
                          	<br />
                          	<label>Lower Boundary</label>
                          	<input type="text" name="lo_bound_f" id="lo_bound_f" class="form-control">
                          	<br />
                          </fieldset>
                          <fieldset>
                          	<legend>Children</legend>
                          	<label>Upper Boundary</label>
                          	<input type="text" name="hi_bound_c" id="hi_bound_c" class="form-control">
                          	<br />
                          	<label>Lower Boundary</label>
                          	<input type="text" name="lo_bound_c" id="lo_bound_c" class="form-control">
                          	<br />
                          </fieldset>

                          <fieldset>
                            <legend>Input</legend>
                            <label>Input Type</label>
                            <select required name="input_type" id="input_type" class="form-control">
                              <option value="INPUT_BOX">Input Box</option>
                              <option value="TEXT_AREA">TEXT AREA</option>
                              <option value="COMBO_BOX">Drop Down</option>
                              <option value="TITLE">TITLE</option>
                            </select>
                          	<p>for separate values separate them by ; 
for range separate them with a dash -
</p>
                          	<br />
                          	<label>Input Value</label>
                          	<input type="text" name="result_values" id="result_values" class="form-control">
                          	<br />
                          </fieldset>
                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
 <script type="text/javascript">
 	$(document).on('click', '.add_result', function(){  
           var item_id = $(this).attr("id");  
           $.ajax({  
                url:"fetchParamGroups.php",  
                method:"POST",  
                data:{item_id:item_id},
                dataType:"json",
                success:function(data){  
                     $('#item_id').val(data.item_id);
                     $('#insert').val("Update");  
                     $('#add_parameter').modal('show');  
                }  
           });  
      });

 	$('#insert_parameters').on("submit", function(event){  
           event.preventDefault();  
           
                $.ajax({  
                     url:"insert_parameters_results.php",  
                     method:"POST",  
                     data:$('#insert_parameters').serialize(),  
                     beforeSend:function(){  
                          $('#insert').val("Inserting Parameter...");  
                     },  
                     success:function(data){  
                          $('#insert_parameters')[0].reset();  
                          $('#add_parameter').modal('hide');
                          $('#paramGroups').load(location.href + " #paramGroups");
                          alert(data);
                     }  
                });
      });
 </script>