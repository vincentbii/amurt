<div id="edit_modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Update Parameter</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="update_form">
                          <label>Parameter Name</label>  
                          <input required type="text" name="item_id" id="item_id" class="form-control" />  
                          <br />
                          <label>Parameter Result</label>  
                          <input required type="text" name="results" id="results" class="form-control" />  
                          <br />
                          <label>Msr. Unit</label>  
                          <input type="text" name="unit_msr" id="unit_msr" class="form-control" />  
                          <br />

                          <fieldset>
                            <legend>Male</legend>
                            <label>Upper Boundary</label>
                            <input type="text" name="hi_bound_m" id="hi_bound_m" class="form-control">
                            <br />
                            <label>Lower Boundary</label>
                            <input type="text" name="lo_bound_m" id="lo_bound_m" class="form-control">
                            <br />
                          </fieldset>
                          <fieldset>
                            <legend>Female</legend>
                            <label>Upper Boundary</label>
                            <input type="text" name="hi_bound_f" id="hi_bound_f" class="form-control">
                            <br />
                            <label>Lower Boundary</label>
                            <input type="text" name="lo_bound_f" id="lo_bound_f" class="form-control">
                            <br />
                          </fieldset>
                          <fieldset>
                            <legend>Children</legend>
                            <label>Upper Boundary</label>
                            <input type="text" name="hi_bound_c" id="hi_bound_c" class="form-control">
                            <br />
                            <label>Lower Boundary</label>
                            <input type="text" name="lo_bound_c" id="lo_bound_c" class="form-control">
                            <br />
                          </fieldset>
                          <fieldset>
                            <legend>Input</legend>
                            <label>Input Type</label>
                            <select required name="input_type" id="input_type" class="form-control">
                              <option value="INPUT_BOX">Input Box</option>
                              <option value="TEXT_AREA">TEXT AREA</option>
                              <option value="COMBO_BOX">Drop Down</option>
                              <option value="TITLE">TITLE</option>
                            </select>
                            <p>for separate values separate them by ; 
for range separate them with a dash -
</p>
                            <br />
                            <label>Input Value</label>
                            <input type="text" name="result_values" id="result_values" class="form-control">
                            <br />
                          </fieldset>
                          <input type="submit" name="update" id="update" value="Update" class="btn btn-success" />  
                          <input type="hidden" name="resultID" id="resultID">
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
 <script type="text/javascript">
   $('#update_form').on("submit", function(event){  
           event.preventDefault();  
           if($('#resultID').val() == "")  
           {  
                alert("resultID is required");  
           }
           else  
           {  
                $.ajax({  
                     url:"lab_updateParameterResults.php",  
                     method:"POST",  
                     data:$('#update_form').serialize(),  
                     beforeSend:function(){  
                          $('#update').val("Updating...");  
                     },  
                     success:function(data){  
                          $('#update_form')[0].reset();  
                          $('#edit_modal').modal('hide');
                          $('#paramGroups').load(location.href + " #paramGroups");
                          alert(data)
                     }  
                });  
           }  
      });
 </script>