<div id="edit_modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Update Parameter</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="update_form">
                          <label>Parameter Group ID</label>  
                          <input required type="text" name="groupid" id="groupid" class="form-control" />  
                          <br />
                          <label>Enter Parameter Name</label>  
                          <input required type="text" name="name" id="name" class="form-control" />  
                          <br />
                          <label>Msr. Unit</label>  
                          <input type="text" name="msr_unit" id="msr_unit" class="form-control" />  
                          <br />

                          <fieldset>
                            <legend>Male</legend>
                            <label>Upper Boundary</label>
                            <input type="text" name="hi_bound" id="hi_bound" class="form-control">
                            <br />
                            <label>Lower Boundary</label>
                            <input type="text" name="lo_bound" id="lo_bound" class="form-control">
                            <br />
                          </fieldset>
                          <fieldset>
                            <legend>Female</legend>
                            <label>Upper Boundary</label>
                            <input type="text" name="hi_bound_f" id="hi_bound_f" class="form-control">
                            <br />
                            <label>Lower Boundary</label>
                            <input type="text" name="lo_bound_f" id="lo_bound_f" class="form-control">
                            <br />
                          </fieldset>
                          <fieldset>
                            <legend>Children</legend>
                            <label>Upper Boundary</label>
                            <input type="text" name="hi_bound_y" id="hi_bound_y" class="form-control">
                            <br />
                            <label>Lower Boundary</label>
                            <input type="text" name="lo_bound_y" id="lo_bound_y" class="form-control">
                            <br />
                          </fieldset>

                          <fieldset>
                            <legend>Price</legend>
                            <label>Cash Price</label>
                            <input required type="text" name="unit_price_1" id="unit_price_1" class="form-control">
                            <br />
                            <label>Credit Price</label>
                            <input required type="text" name="unit_price_2" id="unit_price_2" class="form-control">
                            <br />
                          </fieldset>
                          <fieldset>
                            <legend>Input</legend>
                            <label>Input Type</label>
                            <select required name="field_type" id="field_type" class="form-control">
                              <option value="input_box">Input Box</option>
                              <option value="limited_values">Limited Values</option>
                              <option value="group_field">Group of Values</option>
                              <option value="drop_down">Drop Down</option>
                            </select>
                            <p>for separate values separate them by ; 
for range separate them with a dash -
</p>
                            <br />
                            <label>Input Value</label>
                            <input type="text" name="add_type" id="add_type" class="form-control">
                            <br />
                          </fieldset>
                          <input type="submit" name="update" id="update" value="Update" class="btn btn-success" />  
                          <input type="hidden" name="id" id="id">
                          <input type="hidden" name="item_id" id="item_id">
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
 <script type="text/javascript">
   $('#update_form').on("submit", function(event){  
           event.preventDefault();  
           if($('#name').val() == "")  
           {  
                alert("Name is required");  
           }
           else  
           {  
                $.ajax({  
                     url:"lab_updateParameters.php",  
                     method:"POST",  
                     data:$('#update_form').serialize(),  
                     beforeSend:function(){  
                          $('#update').val("Updating...");  
                     },  
                     success:function(data){  
                          $('#update_form')[0].reset();  
                          $('#edit_modal').modal('hide');
                          $('#paramGroups').load(location.href + " #paramGroups");
                          alert(data)
                     }  
                });  
           }  
      });
 </script>