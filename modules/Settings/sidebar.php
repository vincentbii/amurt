


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Prescriptions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="drug_classes.php"><i class="fa fa-circle-o"></i> Drug Classes</a></li>
            <li><a href="all_services.php"><i class="fa fa-circle-o"></i> All Services</a></li>
          </ul>
        </li>
       
       <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Laboratory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="lab_parameter_groups.php"><i class="fa fa-circle-o"></i>Parament Groups</a></li>
            <li><a href="lab_test_parameters.php"><i class="fa fa-circle-o"></i>Test Parameters</a></li>
            <li><a href="lab_test_parameter_results.php"><i class="fa fa-circle-o"></i>Test Parameter Results</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>