# theme-triton-96ce0d41-b781-4585-be3c-882eaead6028/sass/etc

This folder contains miscellaneous SASS files. Unlike `"theme-triton-96ce0d41-b781-4585-be3c-882eaead6028/sass/etc"`, these files
need to be used explicitly.
