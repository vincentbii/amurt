<?php
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once 'roots.php';require($root_path . "modules/accounting/extIncludes.php"); ?>
<link rel="stylesheet" type="text/css" href="accounting.css">
   <?php
require($root_path . 'include/inc_environment_global.php');
require_once($root_path . 'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path . 'include/inc_init_xmlrpc.php');
require_once($root_path . 'include/care_api_classes/class_tz_billing.php');
require($root_path . 'include/care_api_classes/accounting.php');
require_once($root_path . 'include/care_api_classes/class_tz_insurance.php');

$insurance_obj = new Insurance_tz;
$bill_obj = new Bill;
require_once('myLinks_1.php');
//require_once('report_functions.php');
//    jsIncludes();
echo "<table width=100% border=0>
        <tr class='titlebar'><td colspan=2 bgcolor=#99ccff><font color='#330066'>Finalize Invoices</font></td></tr>
    <tr><td align=left valign=top>";
require 'acLinks.php';
echo '</td><td width=80% valign=top>';

if (!isset($_POST[submit])) {
    Finalize();
} else {
    $pid = $_POST['pid'];
    $finalizeMode = $_POST['finalizeMode'];
    $billNumber=$_POST['billNumber'];
    $accno = $_POST['accNo'];
//Finalize by Account
    if ($finalizeMode == 'accNo') {
        $sql1 = "SELECT distinct b.pid,b.bill_number,b.status,b.encounter_nr FROM care_ke_billing b LEFT JOIN care_encounter e
                ON b.pid=e.pid WHERE b.pid=$pid and bill_number=$billNumber AND b.`ip-op`=1
                AND e.is_discharged=1 and b.finalised=0";
        if($debug)
            echo $sql1;
        $result = $db->Execute($sql1);
        $rcount=$result->RecordCount();
            $rows = $result->FetchRow();

        $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and
                      insurance_id in (select c.id from care_person p 
                       inner join care_tz_company c on p.insurance_id=c.id 
                      where pid=$pid) and bill_number=$billNumber AND `ip-op`=1";
        if ($result = $db->Execute($sql)) {

                        $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
                        $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
                        if ($IS_PATIENT_INSURED) {
                            $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $rows[encounter_nr]);
                        }


                    echo "<div class='myMessage'>Invoice number  $bill_number finalized succefully</div>";
                    echo "<div class='myMessage2'><button id='preview'>Print Satement</button>
                            <button id='print'>Export Satement</button><button id='Preview'>Preview Invoices</button></div>";
            ?>
            <button id='preview' onclick="printStatement('<?php $accno ?>')">Print Statement</button>
            <button id='print'  onclick="exportExcel('<?php echo $accno ?>')">Export Statement</button>
            <button id='Preview' onclick="getStatement('<?php echo $accno ?>')">Preview Invoices</button></div>
            <?php
            Finalize();
        } else {
            echo "<div class='myMessage'>Error Finalizing Invoice:<br> " . $sql . "</div>";
            Finalize();
        }
    }
    //Finalize by PID
    if ($finalizeMode == 'pid') {
        finalizePID($pid, $bill_obj, $insurance_obj,$billNumber,$accno);
    }
}
echo "</td></tr></table>";


function finalizePID($pid, $bill_obj, $insurance_obj,$billNumber,$accno) {
    global $db;
    $debug = false;
    $sql1 = "SELECT distinct b.pid,b.bill_number,b.status,b.encounter_nr FROM care_ke_billing b LEFT JOIN care_encounter e 
                ON b.pid=e.pid WHERE b.pid=$pid and bill_number=$billNumber AND b.`ip-op`=1
                AND e.is_discharged=1 and b.finalised=0";
              if($debug) echo $sql1;
        $result = $db->Execute($sql1);
         $rcount=$result->RecordCount();
    if ($rcount>0) {
        $rows = $result->FetchRow();
        $bill_number = $rows[1];
        $encNr = $rows[encounter_nr];
        $stat = $rows[2];
        if ($stat <> "Finalized") {
            $sql2 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=1 
                        and service_type not in('payment','nhif','Payment Adjustment') and bill_number='$billNumber'";
                        if($debug) echo $sql2;
            $result2 = $db->Execute($sql2);
            $row2 = $result2->FetchRow();
            echo 'bill=' . $row2[0] . '<br>';

            $sql3 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=1
                and service_type in('payment','nhif','Payment Adjustment') and rev_code<>'nhif2' and bill_number='$billNumber'";
                if($debug) echo $sql3;

            $result3 = $db->Execute($sql3);
            $row3 = $result3->FetchRow();
            echo 'paid=' . $row3[0] . '<br>';

            $balance = intval($row2[0] - $row3[0]);
            echo "balance=" . $balance;


           if ($balance < 0) {
            echo "<div class='myMessage'>The patient has paid in Excess of $balance</div>";
           }else{
               if ($balance ==0) {

                    $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and pid=$pid and encounter_nr=$encNr";
                    if ($debug)
                        echo $sql;
                    $result = $db->Execute($sql);

                    echo "<div class='myMessage'>Invoice number  $bill_number finalized succefully</div>";
                    echo "<div class='myMessage2'><button id='preview'>Print Satement</button>
                            <button id='print'>Export Satement</button><button id='Preview'>Preview Invoices</button></div>";
                } else {
                   $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
                   $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
                   if ($IS_PATIENT_INSURED) {
                       $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and pid=$pid and encounter_nr=$encNr";
                       if ($debug)
                           echo $sql;
                       $result = $db->Execute($sql);

                       $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $encNr);

                       echo "<div class='myMessage'>Invoice number  $billNumber finalized succefully</div>";
                       echo "<div class='myMessage2'><button id='preview'>Print Satement</button>";

                   }else{
                       echo "<div class='myMessage'>The patient has a pending balance of Ksh $balance</div>";
                   }

                }
               }
            Finalize();
        } else {
            echo "<div class='myMessage'>Invoice number  $bill_number is already finalized</div>";
            Finalize();
        }
    }else{
         echo "<div class='myMessage'>Invoice number  $billNumber is already finalized</div>";
         Finalize();
    }
}
?>

