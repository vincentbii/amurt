<?php
require_once 'roots.php';
require($root_path . 'include/inc_environment_global.php');
require_once($root_path . 'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path . 'include/inc_init_xmlrpc.php');
require_once($root_path . 'include/care_api_classes/class_tz_billing.php');

function Departments()
{
	global $db;
	$sql = "SELECT DISTINCT purchasing_class FROM care_tz_drugsandservices WHERE purchasing_class NOT IN ('CONSULTATION')";
	$result = $db->Execute($sql);
	$Departments = array();
	$DepartmentsInc = 0;
	while ($row = $result->FetchRow()) {
		$Departments[$DepartmentsInc] = array('departments' => $row['purchasing_class']);
		$DepartmentsInc++;
	}

	return json_encode($Departments);
}
function Stock($dep){
	global $db;
	$sql = "SELECT b.partcode, b.item_description, b.unit_price, a.qty_in_store, a.quantity, (b.unit_price * (a.qty_in_store+a.quantity)) AS total_value FROM care_ke_locstock as a INNER JOIN care_tz_drugsandservices as b ON a.stockid = b.partcode WHERE a.loccode IN ('MAIN', 'DISPENS') AND b.purchasing_class NOT IN ('CONSULTATION')";
	if($dep !== null || $dep !== ''){
		$sql .= " AND b.purchasing_class = '$dep'";
	}
	$result = $db->Execute($sql);
	$stock = array();
	$stockInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$stock[$stockInc] = array('partcode' => $row['partcode'], 'item_description' => $row['item_description'], 'unit_price' => $row['unit_price'], 'qty_in_store' => $row['qty_in_store'], 'quantity' => $row['quantity'], 'total_value' => $row['total_value']);
			$stockInc++;
		}
	}
	return json_encode($stock);
}

function StockSummary(){
	global $db;
	$sql = "SELECT DISTINCT a.purchasing_class FROM care_tz_drugsandservices AS a INNER JOIN care_ke_locstock AS b ON a.partcode = b.stockid WHERE a.purchasing_class NOT IN ('CONSULTATION') AND b.loccode in ('MAIN', 'DISPENS')";
	$result = $db->Execute($sql);
	$stock = array();
	$stockInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$itemsSql = "SELECT COUNT(*) AS itemscount, SUM((b.qty_in_store + b.quantity)*a.unit_price) AS tsum FROM care_tz_drugsandservices a INNER JOIN care_ke_locstock b ON a.partcode = b.stockid WHERE a.purchasing_class = '".$row['purchasing_class']."' AND b.loccode IN ('MAIN', 'DISPENS')";
			$itemsresult = $db->Execute($itemsSql);
			$itemsRow = $itemsresult->FetchRow();
			$stock[$stockInc] = array('purchasing_class' => $row['purchasing_class'], 'tsum' => $itemsRow['tsum'], 'itemscount' => $itemsRow['itemscount']);
			$stockInc++;
		}
	}

	return json_encode($stock);
}
?>