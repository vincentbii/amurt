<?php
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once 'roots.php';require($root_path . "modules/accounting/extIncludes.php"); ?>
<link rel="stylesheet" type="text/css" href="accounting.css">
<?php
require($root_path . 'include/inc_environment_global.php');
require_once($root_path . 'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path . 'include/inc_init_xmlrpc.php');
require_once($root_path . 'include/care_api_classes/class_tz_billing.php');
require($root_path . 'include/care_api_classes/accounting.php');
require_once($root_path . 'include/care_api_classes/class_tz_insurance.php');

$insurance_obj = new Insurance_tz;
$bill_obj = new Bill;
require_once('myLinks_1.php');
//require_once('report_functions.php');
//    jsIncludes();
echo "<table width=100% border=0>
        <tr class='titlebar'><td colspan=2 bgcolor=#99ccff><font color='#330066'>Finalize Invoices</font></td></tr>
    <tr><td align=left valign=top>";
require 'acLinks.php';
echo '</td><td width=80% valign=top>';

if (!isset($_POST[submit])) {
    Finalize();
} else {
    $pid = $_POST['pid'];
    $finalizeMode = $_POST['finalizeMode'];
    $billNumber=$_POST['billNumber'];
    $accno = $_POST['accNo'];
//Finalize by Account
    if ($finalizeMode == 'accNo') {
        $sql1 = "SELECT distinct b.pid,b.bill_number,b.status,b.encounter_nr FROM care_ke_billing b LEFT JOIN care_encounter e
                ON b.pid=e.pid WHERE b.pid=$pid and bill_number=$billNumber AND b.`ip-op`=2
                AND b.finalised=0 AND b.`insurance_id` <> '-1'";
        if($debug)
            echo $sql1;
        $result = $db->Execute($sql1);
        $rcount=$result->RecordCount();
        $rows = $result->FetchRow();

        $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and
                      insurance_id in (select c.id from care_person p 
                       inner join care_tz_company c on p.insurance_id=c.id 
                      where pid=$pid) and bill_number=$billNumber AND `ip-op`=2";
        if ($result = $db->Execute($sql)) {

            $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
            $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
            if ($IS_PATIENT_INSURED) {
                $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $rows[encounter_nr]);
            }

            $sql="update care_encounter set is_discharged=1 and discharge_date='$disDate' where pid=$pid and encounter_nr='$rows[encounter_nr]'";


            echo "<div class='myMessage'>Invoice number  $bill_number finalized succefully</div>";
            echo "<div class='myMessage2'><button id='preview'>Print Satement</button>
                            <button id='print'>Export Satement</button><button id='Preview'>Preview Invoices</button></div>";
            ?>
            <button id='preview' onclick="printStatement('<?php $accno ?>')">Print Statement</button>
            <button id='print'  onclick="exportExcel('<?php echo $accno ?>')">Export Statement</button>
            <button id='Preview' onclick="getStatement('<?php echo $accno ?>')">Preview Invoices</button></div>
            <?php
            Finalize();
        } else {
            echo "<div class='myMessage'>Error Finalizing Invoice:<br> " . $sql . "</div>";
            Finalize();
        }
    }else if ($finalizeMode == 'pid') {
        finalizePID($pid, $bill_obj, $insurance_obj,$billNumber,$accno);
    }else if($finalizeMode == 'all'){
        finalizeAll($insurance_obj, $bill_obj);
    }else if($finalizeMode == 'credit'){
        finalizaCredit($insurance_obj, $bill_obj);
    }else if($finalizeMode == 'cash'){
        finalizeCash($insurance_obj, $bill_obj);
    }
}
echo "</td></tr></table>";

function finalizeCash($insurance_obj, $bill_obj){
    global $db;
    $sqlx = "SELECT distinct a.pid FROM care_ke_billing as a inner join care_person as b on a.pid = b.pid WHERE a.`ip-op`=2 AND a.finalised=0 and b.insurance_ID = '-1'";
    $resultx = $db->Execute($sqlx);
    if($resultx->RecordCount() > 0){
        while ($rowx = $resultx->FetchRow()) {
            $sql_billNumber = "SELECT a.bill_number, a.encounter_nr FROM care_ke_billing as a inner JOIN care_person as b on a.pid = b.pid WHERE a.`ip-op` = 2 and a.finalised=0 AND a.pid = '".$rowx[pid]."' and b.insurance_ID = '-1'";
            $result_bill = $db->Execute($sql_billNumber);
            if($result_bill->RecordCount() > 0){
                while ($row_bill = $result_bill->FetchRow()) {
                            $bill_number = $row_bill[bill_number];
                            $encNr = $row_bill[encounter_nr];
                            $pid = $rowx[pid];

                            $sqlf = "Update care_ke_billing set `finalised`='1' where finalised=0 and pid='".$pid."' and encounter_nr='".$encNr."'";
                            if($db->Execute($sqlf)){
                                $sqld="update care_encounter set is_discharged=1 and discharge_date='".date('Y-m-d')."' where pid='".$pid."' and encounter_nr='".$encNr."'";
                                if($db->Execute($sqld)){
                                    echo "<div class='myMessage'>Invoice number  $bill_number finalized successfully!</div>";
                                    echo "<div class='myMessage2'><button id='preview'>Print Satement</button>
                                            <button id='print'>Export Satement</button><button id='Preview'>Preview Invoices</button></div>";
                                }else{
                                    echo "<div class='myMessage'>Invoice number  $bill_number Not finalized!</div>";
                                }
                            }else{
                                echo "<div class='myMessage'>Invoice number  $bill_number, $pid, $encNr Not finalized</div>";
                            }

                }
            }
        }
    }
}

function finalizaCredit($insurance_obj, $bill_obj){
    global $db;
    $sqlx = "SELECT distinct a.pid FROM care_ke_billing as a inner join care_person as b on a.pid = b.pid WHERE a.`ip-op`=2 AND a.finalised=0 AND a.`insurance_id` <> '-1' and b.insurance_ID <> '-1'";
    $resultx = $db->Execute($sqlx);
    if($resultx->RecordCount() > 0){
        while ($rowx = $resultx->FetchRow()) {
            $sql_billNumber = "SELECT a.bill_number, a.encounter_nr FROM care_ke_billing as a inner JOIN care_person as b on a.pid = b.pid WHERE a.`ip-op` = 2 and a.finalised=0 AND a.pid = '".$rowx[pid]."' AND a.`insurance_id` <> '-1' and b.insurance_ID <> '-1'";
            $result_bill = $db->Execute($sql_billNumber);
            if($result_bill->RecordCount() > 0){
                while ($row_bill = $result_bill->FetchRow()) {
                            $bill_number = $row_bill[bill_number];
                            $encNr = $row_bill[encounter_nr];
                            $pid = $rowx[pid];

                    $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
                    $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
                    if ($IS_PATIENT_INSURED) {
                        $sql="SELECT b.pid,encounter_nr,`ip-op`,bill_date,bill_number,b.`partcode`,b.`rev_code`,
                                b.`service_type`,b.`Description`,b.`total`,b.`status`,d.`gl_sales_acct`
                                FROM care_ke_billing b left join `care_tz_drugsandservices` d on b.`partcode`=d.`partcode` inner JOIN care_person p on b.pid = p.pid
                                 WHERE b.pid='".$pid."' and encounter_nr='".$encNr."'
                                AND service_type NOT IN('payment') AND b.`insurance_id` <> '-1' AND p.insurance_ID <> '-1'";
                        if($debug) echo $sql;
                        $results=$db->Execute($sql);

                        if($bill_obj->checkIncomeTrans('invoice')){
                        while($row=$results->FetchRow()){
//                                $glCode=$bill_obj->getItemGL($rev_code);
                            $bill_obj->updateIncomeTrans($row[gl_sales_acct],$row[total],$row[bill_date],$row[bill_number],'+','invoice');
                            }
                        }

                        $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $encNr);

                        $sqlf = "UPDATE care_ke_billing set `finalised`=1 where finalised=0 and pid='".$pid."' and encounter_nr ='".$encNr."'";
                        
                        $sqld="UPDATE care_encounter set is_discharged=1,discharge_date='".date('Y-m-d')."', discharge_time='".date('H:i:s')."' where pid='".$pid."' and encounter_nr='".$encNr."'";
                        if($db->Execute($sqlf) && $db->Execute($sqld)){
                            echo "<div class='myMessage'>Invoice number  $bill_number finalized successfully</div>";
                            echo "<div class='myMessage2'><button id='preview'>Print Statement</button>";
                        }else{
                            echo "<div class='myMessage'>Invoice number  $bill_number Not finalized</div>";
                        }
                        

                    }else{
                        echo "<div class='myMessage'>The patient has a pending balance of Ksh $balance</div>";
                    }
                }
            }
        }
    }
}

function finalizeAll($insurance_obj, $bill_obj){
    global $db;
    $sql = "SELECT distinct pid FROM care_ke_billing WHERE `ip-op`=2 AND finalised=0 AND `insurance_id` <> '-1'";
    $result = $db->Execute($sql);
    if($result->RecordCount() > 0){
        while ($row = $result->FetchRow()) {
            $sql_billNumber = "SELECT bill_number, encounter_nr FROM care_ke_billing WHERE `ip-op` = 2 and finalised=0 AND pid = '".$row[pid]."' AND `insurance_id` <> '-1'";
            $result_bill = $db->Execute($sql_billNumber);
            if($result_bill->RecordCount() > 0){
                while ($row_bill = $result_bill->FetchRow()) {
                            $bill_number = $row_bill[bill_number];
                            $encNr = $row_bill[encounter_nr];
                            $pid = $row[pid];
                            $stat = $rows[2];
        if ($stat <> "Finalized") {
            $sql2 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=2 and service_type not in('payment','Payment Adjustment') and bill_number='$billNumber' AND `insurance_id` <> '-1'";
                            if($debug) echo $sql2;
                            $result2 = $db->Execute($sql2);
                            $row2 = $result2->FetchRow();
                            echo 'bill=' . $row2[0] . '<br>';

                            $sql3 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=2
                                and service_type in('payment','Payment Adjustment') and bill_number='$billNumber' AND `insurance_id` <> '-1'";
                            if($debug) echo $sql3;

                            $result3 = $db->Execute($sql3);
                            $row3 = $result3->FetchRow();
                            echo 'paid=' . $row3[0] . '<br>';

                            $balance = intval($row2[0] - $row3[0]);
                            echo "balance=" . $balance;

                            if ($balance < 0) {
                echo "<div class='myMessage'>The patient has paid in Excess of $balance</div>";
            }else{
                if ($balance ==0) {
                    $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and pid='".$pid."' and encounter_nr='".$encNr."'";
                    if ($debug)
                        echo $sql;
                    if($db->Execute($sql)){
                        $sql="update care_encounter set is_discharged=1 and discharge_date='".date('Y-m-d')."' where pid='".$pid."' and encounter_nr='".$encNr."'";
                        if($db->Execute($sql)){
                            echo "<div class='myMessage'>Invoice number  $bill_number finalized successfully!</div>";
                            echo "<div class='myMessage2'><button id='preview'>Print Satement</button>
                                    <button id='print'>Export Satement</button><button id='Preview'>Preview Invoices</button></div>";
                        }else{
                            echo "<div class='myMessage'>Invoice number  $bill_number Not finalized!</div>";
                        }
                    }else{
                        echo "<div class='myMessage'>Invoice number  $bill_number, $pid, $encNr Not finalized</div>";
                    }

                    
                } else {
                    $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
                    $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
                    if ($IS_PATIENT_INSURED) {

                        $sql="SELECT b.pid,encounter_nr,`ip-op`,bill_date,bill_number,b.`partcode`,b.`rev_code`,
                                b.`service_type`,b.`Description`,b.`total`,b.`status`,d.`gl_sales_acct`
                                FROM care_ke_billing b left join `care_tz_drugsandservices` d on b.`partcode`=d.`partcode`
                                 WHERE b.pid='".$pid."' and encounter_nr='".$encNr."'
                                AND service_type NOT IN('payment') AND b.`insurance_id` <> '-1'";
                        if($debug) echo $sql;
                        $results=$db->Execute($sql);

                        if($bill_obj->checkIncomeTrans('invoice')){
                        while($row=$results->FetchRow()){
//                                $glCode=$bill_obj->getItemGL($rev_code);
                            $bill_obj->updateIncomeTrans($row[gl_sales_acct],$row[total],$row[bill_date],$row[bill_number],'+','invoice');
                            }
                        }

                        $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $encNr);

                        $sqlf = "UPDATE care_ke_billing set `finalised`=1 where finalised=0 and pid='".$pid."' and encounter_nr ='".$encNr."'";
                        if ($debug)
                            echo $sqlf;
                        if($db->Execute($sqlf)){
                            $sqld="UPDATE care_encounter set is_discharged=1,discharge_date='".date('Y-m-d')."', discharge_time='".date('H:i:s')."' where pid='".$pid."' and encounter_nr='".$encNr."'";
                            if($db->Execute($sqld)){
                                echo "<div class='myMessage'>Invoice number  $billNumber finalized successfully</div>";
                                echo "<div class='myMessage2'><button id='preview'>Print Statement</button>";
                            }else{
                                echo "<div class='myMessage'>Invoice number  $billNumber Not finalized</div>";
                            }
                        }else{
                            echo "<div class='myMessage'>Invoice number  $billNumber Not finalized!</div>";
                        }

                        

                        

                    }else{
                        echo "<div class='myMessage'>The patient has a pending balance of Ksh $balance</div>";
                    }
        }

                            

                }
            } else {
            echo "<div class='myMessage'>Invoice number  $bill_number is already finalized</div>";
            Finalize();
        }
                }
            }
        }
    }
}


function finalizePID($pid, $bill_obj, $insurance_obj,$billNumber,$accno) {
    global $db;
    $sql1 = "SELECT distinct b.pid,b.bill_number,b.encounter_nr FROM care_ke_billing b LEFT JOIN care_encounter e 
                ON b.pid=e.pid WHERE b.pid=$pid and b.bill_number=$billNumber AND b.`ip-op`=2
                AND b.finalised=0 AND b.`insurance_id` <> '-1'";
    if($debug) echo $sql1;
    $result = $db->Execute($sql1);

    $rcount=$result->RecordCount();
    if ($rcount>0) {
        $rows = $result->FetchRow();
        $bill_number = $rows[1];
        $encNr = $rows[encounter_nr];
        $stat = $rows[2];
        if ($stat <> "Finalized") {
            $sql2 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=2 
                        and service_type not in('payment','nhif','nhif3','Payment Adjustment') and bill_number='$billNumber' AND `insurance_id` <> '-1'";
            if($debug) echo $sql2;
            $result2 = $db->Execute($sql2);
            $row2 = $result2->FetchRow();
            echo 'bill=' . $row2[0] . '<br>';

            $sql3 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=2
                and service_type in('payment','nhif','nhif3','Payment Adjustment') and rev_code<>'nhif2' and bill_number='$billNumber' AND `insurance_id` <> '-1'";
            if($debug) echo $sql3;

            $result3 = $db->Execute($sql3);
            $row3 = $result3->FetchRow();
            echo 'paid=' . $row3[0] . '<br>';

            $balance = intval($row2[0] - $row3[0]);
            echo "balance=" . $balance;


            if ($balance < 0) {
                echo "<div class='myMessage'>The patient has paid in Excess of $balance</div>";
            }else{
                if ($balance ==0) {

                    $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and pid=$pid and encounter_nr=$encNr";
                    if ($debug)
                        echo $sql;
                    $db->Execute($sql);

                    $sql="update care_encounter set is_discharged=1 and discharge_date='".date('Y-m-d')."' where pid=$pid and encounter_nr='$encNr'";
                    $db->Execute($sql);


                    echo "<div class='myMessage'>Invoice number  $bill_number finalized succefully</div>";
                    echo "<div class='myMessage2'><button id='preview'>Print Satement</button>
                            <button id='print'>Export Satement</button><button id='Preview'>Preview Invoices</button></div>";
                } else {
                    $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
                    $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
                    if ($IS_PATIENT_INSURED) {

                        $sql="SELECT b.pid,encounter_nr,`ip-op`,bill_date,bill_number,b.`partcode`,b.`rev_code`,
                                b.`service_type`,b.`Description`,b.`total`,b.`status`,d.`gl_sales_acct`
                                FROM care_ke_billing b left join `care_tz_drugsandservices` d on b.`partcode`=d.`partcode`
                                 WHERE b.pid=$pid and encounter_nr=$encNr
                                AND service_type NOT IN('payment','NHIF','NHIF2','NHIF3','NHIF4') AND b.`insurance_id` <> '-1'";
                        if($debug) echo $sql;
                        $results=$db->Execute($sql);

                        if($bill_obj->checkIncomeTrans('invoice')){
                        while($row=$results->FetchRow()){
//                                $glCode=$bill_obj->getItemGL($rev_code);
                            $bill_obj->updateIncomeTrans($row[gl_sales_acct],$row[total],$row[bill_date],$row[bill_number],'+','invoice');
                            }
                        }

                        $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $encNr);

                        $sql = "Update care_ke_billing set `finalised`='1' where finalised=0 and pid=$pid and encounter_nr=$encNr";
                        if ($debug)
                            echo $sql;
                        $db->Execute($sql);

                        $sql="update care_encounter set is_discharged=1,discharge_date='".date('Y-m-d')."', discharge_date='".date('H:i:s')."' where pid=$pid and encounter_nr='$encNr'";
                        $db->Execute($sql);

                        echo "<div class='myMessage'>Invoice number  $billNumber finalized succefully</div>";
                        echo "<div class='myMessage2'><button id='preview'>Print Satement</button>";

                    }else{
                        echo "<div class='myMessage'>The patient has a pending balance of Ksh $balance</div>";
                    }

                }
            }
            Finalize();
        } else {
            echo "<div class='myMessage'>Invoice number  $bill_number is already finalized</div>";
            Finalize();
        }
    }else{
        echo "<div class='myMessage'>Invoice number  $billNumber is already finalized</div>";
        Finalize();
    }
}
?>

