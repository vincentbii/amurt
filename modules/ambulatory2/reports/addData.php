<div id="add_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Add Service</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="insert_form">
                          <label>Invoice Number</label>  
                          <input type="number" name="bill_number" id="bill_number" class="form-control" />  
                          <br /> 
                          <label>Enter Date</label>  
                          <input type="date" name="bill_date" id="bill_date" value="<?php echo 'Y-m-d'; ?>" class="form-control" />  
                          <br /> 
                          <label>Service Type</label>  
                          <input type="text" name="service_type" id="service_type" class="form-control" />  
                          <br />  
                          <label>Price</label>  
                          <input type="number" name="price" id="price" class="form-control" />  
                          <br /> 
                          <label>Quantity</label>  
                          <input type="number" name="quantity" id="quantity" class="form-control" />  
                          <br />  
                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>

 <script type="text/javascript">
   $('#insert_form').on("submit", function(event){  
           event.preventDefault();  
           
                $.ajax({  
                     url:"insertServices.php",  
                     method:"POST",  
                     data:$('#insert_form').serialize(),  
                     beforeSend:function(){  
                          $('#insert').val("Inserting...");  
                     },  
                     success:function(data){  
                          $('#insert_form')[0].reset();  
                          $('#add_Modal').modal('hide');
                          location.reload();
                          alert(data);
                     }  
                });
      });
 </script>