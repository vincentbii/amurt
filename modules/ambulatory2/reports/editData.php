<div id="edit_modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Update Service</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="update_form">
                          <label>Invoice Data</label>  
                          <input type="date" name="bill_date" id="bill_date" class="form-control" />  
                          <br />  
                          <label>Service Type</label>  
                          <input type="text" name="service_type" id="service_type" class="form-control" />  
                          <br /> 
                          <label>Price</label>  
                          <input type="number" name="price" id="price" class="form-control" />  
                          <br />  
                          <label>Quantity</label>  
                          <input type="number" name="qty" id="qty" class="form-control" />  
                          <br />  
                          <input type="hidden" name="id" id="id" />  
                          <input type="submit" name="update" id="update" value="Update" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>

 <script type="text/javascript">
   $('#update_form').on("submit", function(event){  
           event.preventDefault();  
           if($('#bill_date').val() == "")  
           {  
                alert("Prescribe Date required");  
           }  
           else if($('#service_type').val() == '')  
           {  
                alert("Service Type is required");  
           }  
           else if($('#price').val() == '')  
           {  
                alert("Price is required");  
           }
           else if($('#qty').val() == '')  
           {  
                alert("Quantity is required");  
           }
           else  
           {  
                $.ajax({  
                     url:"updateServices.php",  
                     method:"POST",  
                     data:$('#update_form').serialize(),  
                     beforeSend:function(){  
                          $('#update').val("Updating...");  
                     },  
                     success:function(data){  
                          $('#update_form')[0].reset();  
                          $('#edit_modal').modal('hide');
                          location.reload();
                          alert(data);
                     }  
                });  
           }  
      });
 </script>