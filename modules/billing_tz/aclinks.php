
<?php require_once 'roots.php';
$title=$_REQUEST['title'];
?>
<link rel="stylesheet" type="text/css" href="../../css/themes/default/default.css">
<table>
    <tbody class="submenu">
       
        <tr>
            <td  colspan="2"  class="submenu_title">Debtor Reports:</td>
        </tr>
        <TR>
            <td align=center><img src="<?php echo $root_path ?>gui/img/common/default/documents.gif" border=0 width="16" height="17"></td>
            <TD class="submenu_item"><nobr><a href="<?php echo $root_path ?>debtors_ReportsMain.php?title=statement">Debtor Statement</a></nobr></TD>
</tr>
<tr>
    <td align=center><img src="<?php echo $root_path ?>gui/img/common/default/documents.gif" border=0 width="16" height="17"></td>
    <TD class="submenu_item"><nobr><a href="<?php echo $root_path ?>debtors_ReportsMain.php?title==invoice">debtor Invoice</a></nobr></TD>

</tr>
<tr>
    <td align=center><img src="<?php echo $root_path ?>gui/img/common/default/documents.gif" border=0 width="16" height="17"></td>
    <td class="submenu_item"><nobr><a href="<?php echo $root_path ?>debtors_ReportsMain.php?title=agedBalance">Aged Balances</a></nobr></td>

</tr>
<tr>
    <td align=center><img src="<?php echo $root_path ?>gui/img/common/default/documents.gif" border=0 width="16" height="17"></td>
    <td class="submenu_item"><nobr><a href="<?php echo $root_path ?>debtors_ReportsMain.php?title=detailedInvoice">Detailed Invoice</a></nobr></td>

</tr>
<tr>
    <td align=center><img src="<?php echo $root_path ?>gui/img/common/default/documents.gif" border=0 width="16" height="17"></td>
    <td class="submenu_item"><nobr><a href=".<?php echo $root_path ?>debtors_ReportsMain.php?title=pInvoiceDetail">Patient invoice detail</a></nobr></td>

</tr>
<tr>
    <td align=center><img src="<?php echo $root_path ?>gui/img/common/default/documents.gif" border=0 width="16" height="17"></td>
    <td class="submenu_item"><nobr><a href=".<?php echo $root_path ?>debtors_ReportsMain.php?title=pInvoiceSummary">Patient Invoice Summary</a></nobr></td>

</tr>
</tbody>
</table>


<!-- dhtmlxWindows -->
<link rel="stylesheet" type="text/css" href="<?php echo $root_path; ?>include/dhtmlxWindows/codebase/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="<?php echo $root_path; ?>include/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_skyblue.css">
<script src="<?php echo $root_path; ?>include/dhtmlxWindows/codebase/dhtmlxcommon.js"></script>
<script src="<?php echo $root_path; ?>include/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>

<!-- dhtmlxGrid -->
<link rel='STYLESHEET' type='text/css' href='<?php echo $root_path; ?>modules/cashbook/codebase/dhtmlxgrid.css'>
<script src='<?php echo $root_path; ?>modules/cashbook/codebase/dhtmlxcommon.js'></script>
<script src='<?php echo $root_path; ?>modules/cashbook/codebase/dhtmlxgrid.js'></script>
<script src='<?php echo $root_path; ?>modules/cashbook/codebase/dhtmlxgrid_form.js'></script>
<script src='<?php echo $root_path; ?>modules/cashbook/codebase/ext/dhtmlxgrid_filter.js'></script>
<script src='<?php echo $root_path; ?>modules/cashbook/codebase/ext/dhtmlxgrid_srnd.js'></script>
<script src='<?php echo $root_path; ?>modules/cashbook/codebase/dhtmlxgridcell.js'></script>
<script src="<?php echo $root_path; ?>include/dhtmlxWindows/codebase/dhtmlxcontainer.js"></script>

<script src='<?php echo $root_path; ?>include/dhtmlxConnector/codebase/connector.js'></script>



<script>
    var dhxWins, w1, grid;
    function initPsearch(){
        dhxWins = new dhtmlXWindows();

        dhxWins.setImagePath("<?php echo $root_path; ?>include/dhtmlxWindows/codebase/imgs/");

        w1 = dhxWins.createWindow("w1", 300, 100, 340, 250);
        w1.setText("Search Patient");

        grid = w1.attachGrid();
        grid.setImagePath("<?php echo $root_path; ?>modules/cashbook/codebase/imgs/");
        grid.setHeader("Patient ID,first Name,Surname,Last name");
        grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter");
        grid.setSkin("light");
        grid.setColTypes("ro,ro,ro,ro");
        grid.setInitWidths("80,80,80,80");
        grid.loadXML("pSearch_pop.php");
        grid.attachEvent("onRowSelect",doOnRowSelected3);
        grid.attachEvent("onEnter",doOnEnter3);
        //grid.attachEvent("onRightClick",doonRightClick);
        grid.init();
        grid.enableSmartRendering(true);
    }


    function doOnRowSelected3(id,ind){
        document.getElementById('pid').value=+id;
        document.getElementById('pname').value=document.getElementById("pname").value=grid.cells(id,1).getValue()+' '+grid.cells(id,2).getValue()+' '+grid.cells(id,3).getValue();

    }

    function doOnEnter3(rowId,cellInd){
        document.getElementById('pid').value=+rowId;
        closeWindow();
    }

    function closeWindow() {
        dhxWins.window("w1").close();
    }

    function getPatient(str)
    {
        xmlhttp3=GetXmlHttpObject();
        if (xmlhttp3==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="cashbookFns.php?desc3="+str;
        url=url+"&callerID=patient"
        url=url+"&sid="+Math.random();
        xmlhttp3.onreadystatechange=stateChanged3;
        xmlhttp3.open("POST",url,true);
        xmlhttp3.send(null);

    }

    function stateChanged3()
    {
        //get payment description
        if (xmlhttp3.readyState==4)//show point desc
        {
            var str=xmlhttp3.responseText;
            str3=str.split(",");
            document.getElementById('pname').value=str;//str3[0]+' '+str3[1]+' '+str3[2];

             getBillNumbers(document.getElementById('pid').value)

        }
    }

    function getBillNumbers(str){
         xmlhttp10=GetXmlHttpObject();
        if (xmlhttp10==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="../getDesc.php?pid="+str;
        url=url+"&sid="+Math.random();
        url=url+"&callerID=getBillNumbers";
        xmlhttp10.onreadystatechange=stateChangedPid;
        xmlhttp10.open("POST",url,true);
        xmlhttp10.send(null);
    }
    
    function stateChangedPid()
    {
        //get payment description
        if (xmlhttp10.readyState==4)//show point desc
        {
            var str=xmlhttp10.responseText;
    
            document.getElementById('billNumbers').innerHTML=str;

        }
    }


// function stateChanged8()
//    {
//        //get payment description
//        if (xmlhttp3.readyState==4)//show point desc
//        {
//            var str=xmlhttp3.responseText;
//            document.getElementById('bills').innerHTML=str;
//        }
//    }


                                        
    function printSlip(patientid) {
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url;
        //        patientid=document.getElementById('pid').value;
        url="chargeSlip.php?pid="+patientid;
        xmlhttp.onreadystatechange=stateChanged2;
        xmlhttp.open("GET",url,true);
        xmlhttp.send(null);
    }
                    
    function stateChanged2()
    {
        if (xmlhttp.readyState==4)
        {
            //                                    if(xmlhttp.responseText=='success'){
            alert(xmlhttp.responseText);
            //                                    }
            if(xmlhttp.responseText=='success'){
                displaySlip(<?php echo $pid ?>);
            }
        }
    }
                                      
    function displaySlip(enc_no) {
        window.open('http://localhost/receipt/slip.php?enc_no='+enc_no
        ,"Label","menubar=no,toolbar=no,width=300,height=550,location=yes,resizable=no,scrollbars=no,status=yes");
    }
                               
    function GetXmlHttpObject()
    {
        if (window.XMLHttpRequest)
        {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject)
        {
            // code for IE6, IE5
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }
</script>


