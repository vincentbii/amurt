<?php
//error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
error_reporting(E_ALL);
require('./roots.php');
require($root_path.'include/inc_environment_global.php');
/**
* CARE2X Integrated Hospital Information System Deployment 2.1 - 2004-10-02
* GNU General Public License
* Copyright 2005 Robert Meggle based on the development of Elpidio Latorilla (2002,2003,2004,2005)
* elpidio@care2x.org, meggle@merotech.de
*
* See the file "copy_notice.txt" for the licence notice
*/
require_once($root_path.'include/care_api_classes/class_encounter.php');
require_once($root_path.'include/care_api_classes/class_tz_billing.php');

require_once($root_path.'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path.'include/inc_init_xmlrpc.php');
//require_once($root_path.'include/care_api_classes/class_tz_insurance.php');
//$insurance_tz = New Insurance_tz;
$enc_obj = new Encounter;
$bill_obj = new Bill;

require_once($root_path.'include/care_api_classes/class_tz_drugsandservices.php');
//require_once($root_path.'include/care_api_classes/accounting.php');
$drg_obj = new DrugsAndServices;
//$acc_obj=new accounting();
$in_outpatient= $_REQUEST['patient'] ;

  define('LANG_FILE','billing.php');
  require($root_path.'include/inc_front_chain_lang.php');

require ('myLinks.php');

jsIncludes();

require_once 'gridfiles.php';

$bill_obj->Display_Header($LDNewInsuranceClaim,$enc_obj->ShowPID($bat_nr),'');

echo '<BODY bgcolor="#ffffff" link="#000066" alink="#cc0000" vlink="#000066" onLoad="javascript:setBallon("BallonTip","","");">';

 $bill_obj->Display_Headline($LDNewInsuranceClaim,'','','billing_create_2.php','Billing :: Create Quotation');

 if(!isset($mode)) $mode='';

echo "<table width=100% border=0>
    <tr><td align=left valign=top>";
echo '</td><td width=80%>';

if(!isset($_POST[submit])) {
    displayForm();
}else {

    echo var_dump($_POST);
    $creditNo= $_POST['crNo'];
    $insuranceID= $_POST['isuranceID'];
    $insurance= $_POST['insuName'];
    $admno= $_POST['pid'];
    $Names= $_POST['pname'];
    $admDate= $_POST['admDate'];
    $disDate= $_POST['discDate'];
    $ReleaseDate= $_POST['releaseDt'];
    $wrdDays= $_POST['wrdDays'];
    $invoiceNo= $_POST['invNo'];
    $BillAmount= $_POST['invAmount'];
    $Premium= $_POST['totalCrdit'];
    $Balance= $_POST['balance'];

    insertData($db,$creditNo,$insuranceID,$insurance,$admno,
    $Names,$admDate,$disDate,$ReleaseDate,$wrdDays,$invoiceNo,$BillAmount,
    $Premium,$Balance);

    updateBill($db,$creditNo,$insuranceID,$insurance,$admno,
        $Names,$admDate,$disDate,$ReleaseDate,$wrdDays,$invoiceNo,$BillAmount,
        $Premium,$Balance);
    //                updateDbtErp($db,$_POST['pid']);
    //                updateinsuredDbt($_POST['pid']);
    displayForm();

}
echo "</td></tr></table>";

$bill_obj->Display_Footer($LDNewInsuranceClaim,'','','billing_create_2.php','Billing :: Create Quotation');

 $bill_obj->Display_Credits(); 

?>