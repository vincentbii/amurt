
Ext.QuickTips.init();

// turn on validation errors beside the field globally
Ext.form.Field.prototype.msgTarget = 'side';

var fm = Ext.form;
var primaryKey='ID'; //primary key is used several times throughout
// custom column plugin example
var selectedKeys;
var url = {
    local:  'getDebtors.php',  // static data file
    remote: 'getDebtors.php'
};
// configure whether filter query is encoded or not (initially)
var encode = false;

// configure whether filtering is performed locally or remotely (initially)
var local = true;

var joined = new Ext.form.DateField({
    //renderTo: 'datefield',
    fieldLabel: 'Date Joined',
    labelWidth: 100, // label settings here cascade unless overridden
    frame: false,
    width: 180,
    name: 'joined',
    id:'joined'
});

var DOB = new Ext.form.DateField({
    //renderTo: 'datefield',
    fieldLabel: 'Date of Birth',
    labelWidth: 100, // label settings here cascade unless overridden
    frame: false,
    width: 180,
    name: 'DOB',
    id:'DOB'
});
            
var last_trans = new Ext.form.DateField({
    //renderTo: 'datefield2',
    fieldLabel: 'Last Trans Date',
    frame: false,
    width: 180,
    name: 'last_trans',
    id:'last_trans'
});


Ext.namespace('Ext.comboData');

Ext.comboData.depType = [
        ['1', 'Father'],
        ['2', 'Mother'],
        ['3', 'Son'],
        ['4', 'Daughter'],
        ['5', 'Brother'],
        ['6', 'Sister'],
        ['7', 'Member']
    ];

var depTypeStore = new Ext.data.ArrayStore({
        fields: ['id', 'depType'],
        data : Ext.comboData.depType // from states.js
    });
 
 var depTypeCombo = new Ext.form.ComboBox({
        id:'memberType',
        store: depTypeStore,
        displayField:'depType',
        valueField:'depType',
        fieldLabel:'Member Type',
        typeAhead: true,
        mode: 'local',
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a Type...',
        selectOnFocus:true
    });
    
    
var dTypeStore=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'debtorType',

    id: 'ID',//
    baseParams:{
        task: "debtorType"
    },//This
    fields:['dCode','dName']
});
dTypeStore.load()

var debtorTypes = new Ext.form.ComboBox({
    typeAhead: true,
    id:'dTypeid',
    name:"dTypeid",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Bank',
    store: dTypeStore,
    valueField: 'dCode',
    displayField: 'dName',
    listeners: {
        select: function(f,r,i){
            empreader.load({
                params:{
                    dCat: debtorTypes.getValue()
                }
            });
        }
    }
});

var dTypeStore2=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'debtorType',

    id: 'ID',//
    baseParams:{
        task: "debtorType"
    },//This
    fields:['dCode','dName']
});
dTypeStore2.load()

var debtorTypes2 = new Ext.form.ComboBox({
    typeAhead: true,
    id:'category',
    name:"category",
    triggerAction: 'all',
    lazyRender:true,
    mode: 'local2',
    fieldLabel:'Debtor Type',
    store: dTypeStore2,
    valueField: 'dCode',
    displayField: 'dName'
});

 //"total":"11165","Debtors":[{"accno":"C0318","debtorName":"AIC KAMANGU CHILD DEVCENTRE",
//        "address1":"PO BOX 908", "address2":"LIMURU","phone":"","joined":"1899-12-30 00:00:00", 
//        "category":"COMPANIES","os_bal":"-220157.00","last_trans":"2011-09-20 00:00:00", 
//        "un_alloc":"0.00","cr_limit":"70000.00","suspended":"0", "OP_Cover":"0.00","IP_Cover":"0.00",
//        "OP_Usage":"0.00" "IP_Usage":"0.00","OP_Exceed":"0.00","IP_Exceed":"0.00"}
var empreader=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'Debtors',
    baseParams:{
        task: "getDebtors"
    },
    totalProperty: 'total',
    fields:['accno','debtorName','address1','address2','phone','joined','category','os_bal',
    'last_trans','un_alloc','cr_limit','suspended','OP_Cover','IP_Cover','OP_Usage','IP_Usage','OP_Exceed','IP_Exceed']
});
/*    Here is where we create the Form
 */

var filters = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'accno'
    }, {
        type: 'string',
        dataIndex: 'debtorName',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'category',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'address1'
    }, {
        type: 'string',
        dataIndex: 'address2'
    }]
});

var filters2 = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: encode, // json encode the filter query
    local: local,   // defaults to false (remote filtering)
    filters: [{
        type: 'string',
        dataIndex: 'accNo'
    },{
        type: 'string',
        dataIndex: 'PID'
    }, {
        type: 'string',
        dataIndex: 'Names',
        disabled: true
    }, {
        type: 'string',
        dataIndex: 'billDate'
    }]
});

//=====================================================================================
//Create Employee Dependants Section

var addDependants= new Ext.FormPanel({
    id: 'addDependants',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:350,
    url: 'getDebtors.php',
    items: [{xtype: 'textfield',id: 'accno',  fieldLabel: 'Account NO',       allowBlank: false,msgTarget:'side'},
            {xtype: 'textfield',id: 'PID',fieldLabel: 'Patient PID',allowBlank: false,msgTarget:'side'},
            {xtype: 'textfield',id: 'memberID',fieldLabel: 'Member NO',allowBlank: false,msgTarget:'side'},
            {xtype: 'textfield',id: 'Names',fieldLabel: 'Member Names',allowBlank: false,msgTarget:'side'},
            depTypeCombo,
            DOB

        ],
    buttons: [{
        text: 'Save',
        handler: function() {
            var accno=addDependants.getForm().findField('accno').getValue();
            var pid = addDependants.getForm().findField("PID").getValue();
            var memberID=addDependants.getForm().findField('memberID').getValue();
            var names = addDependants.getForm().findField("Names").getValue();
            var memberType=addDependants.getForm().findField('memberType').getValue();
            var dob = addDependants.getForm().findField("DOB").getValue();

            Ext.Ajax.request({
                url: 'getDebtors.php',
                method: 'POST',
                params: {
                    accno:accno,
                    pid:pid,
                    memberID:memberID,
                    names:names,
                    memberType:memberType,
                    dob:dob,
                    task:'addDependants'
                },
                waitMsg:'Saving Data...',
                success: function (form, action) {
                    //                    Ext.MessageBox.alert('Message', 'Saved OK');
                    //                    refreshGrid2();
                    createDependants.hide();
                    refreshDependants();
                      
                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                }
            });

        }
    },
    {
        text: 'Close',
        handler: function() {
            createDependants.hide();
            refreshDependants();
        }
    }]
})

createDependants = new Ext.Window({
    title:'Add Dependants',
    layout:'fit',
    id: 'addEmpDependants',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:400,
    height:300,
    closable:false,
    items: [addDependants]

});



//End of Create Dependants Section
//=====================================================================================


var addMembers= new Ext.FormPanel({
    id: 'addEmpPayments',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:350,
    url: 'detDebtors.php',
    items: [{
        xtype: 'textfield',
        id: 'pid',
        fieldLabel: 'PID',
        allowBlank: false,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vpid = this.el.getValue();
            if(vpid === this.emptyText || vpid === undefined){
                vpid = '';
            }
            return vpid;
        }

    },
    {
        xtype: 'textfield',
        id: 'accno',
        fieldLabel: 'Account NO',
        allowBlank: false,
        msgTarget:'side',
        getValue : function(){
            if(!this.rendered) {
                return this.value;
            }
            var vamount = this.el.getValue();
            if(vamount === this.emptyText || vamount === undefined){
                vamount = '';
            }
            return vamount;
        }
    }],
    buttons: [{
        text: 'Save',
        handler: function() {
            var pid = addMembers.getForm().findField("pid").getValue();
            var accno=addMembers.getForm().findField('accno').getValue();
            Ext.Ajax.request({
                url: 'getDebtors.php',
                method: 'POST',
                params: {
                    pid:pid,
                    accno:accno,
                    task:'addMember'
                },
                waitMsg:'Saving Data...',
                success: function (form, action) {
                    //                    Ext.MessageBox.alert('Message', 'Saved OK');
                    //                    refreshGrid2();
                    createMembers.hide();
                    refreshGrid2();
                      
                },
                failure:function(form, action) {
                    Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                }
            });

        }
    },
    {
        text: 'Close',
        handler: function() {
            createMembers.hide();
            refreshGrid2();
        }
    }]
})

createMembers = new Ext.Window({
    title:'Add A Member',
    layout:'fit',
    id: 'addEmpPays',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:400,
    height:250,
    closable:false,
    items: [addMembers]

});



var accDetails={
    bodyStyle: 'padding:15px;background:transparent',
    frame:true,
    frame:true,
    bodyStyle:'padding:5px 5px 0',
    waitMsgTarget: true,
    width:670,
    layout:'column', // arrangve items in columns
   defaults: {      // defaults applied to items
        layout: 'form',
        border: false,
        bodyStyle: 'padding:4px'
    },
    items: [{
         xtype: 'fieldset',
         columnWidth: 0.5,
         title: 'Section 1',
         collapsible: true,
         autoHeight:true,
         items :[
                joined,
                last_trans,
                {xtype: 'textfield',id: 'cr_limit',fieldLabel: 'CR Limit',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'os_bal',fieldLabel: 'OS Bal',allowBlank: true, msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'un_alloc',fieldLabel: 'Allocated',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'checkbox', id: 'suspended', fieldLabel: '',labelSeparator: '', boxLabel: 'suspended'}
         ]
    },{
         xtype: 'fieldset',
         columnWidth: 0.5,
         title: 'Section 2',
         collapsible: true,
         autoHeight:true,
         items :[
                {xtype: 'textfield',id: 'OP_Cover',fieldLabel: 'OP Cover',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'IP_Cover',fieldLabel: 'IP Cover',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'OP_Usage',fieldLabel: 'OP Usage',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'IP_Usage',fieldLabel: 'IP Usage',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'OP_Exceed',fieldLabel: 'OP Exceed',allowBlank: true,msgTarget:'side',validationEvent:false},
                {xtype: 'textfield',id: 'IP_Exceed',fieldLabel: 'IP Exceed',allowBlank: true,msgTarget:'side',validationEvent:false},
         ]
    }]
};


var paymentsReader=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'payments',
    baseParams:{
        task: "getPayments"
    },
    totalProperty: 'total',
    fields:['payDate','refNo','payer','Total','accno']
});

var paymentsColModel=function(finish,start){
    var columns = [
            {id:'payDate', header: "payDate",width: 80,sortable: true,dataIndex: 'payDate',filterable: true},
            {id:'accno',header: "Account No",width: 80,sortable: true,dataIndex: 'accno',filterable: true},
            {id:'refNo',header: "Receipt",width: 80,sortable: true,dataIndex: 'refNo', filterable: true},
            {id:'payer', header: "Payer",width: 150,sortable: true,dataIndex: 'payer',filterable: true},
            {header: "Amount",width: 80,sortable: true,dataIndex: 'Total',filter: {type: 'string'}}
        ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var paymentsPanel=new Ext.grid.GridPanel({
    id:'payments',
    store: paymentsReader,
    colModel: paymentsColModel(7),
    tbar: [{tag: 'input',type: 'text', size: '30',value: '',style: 'background: #F0F0F9;'
    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid,
        iconCls:'refresh'
    }],
    height: 350,
    border: true,
    listeners: {
        render: {
            fn: function(g){
                paymentsReader.load({
                    params: {
                        start: 0,
                        limit: 30
                    }
                });
                g.getSelectionModel().selectRow(0);
                    delay: 10
            }

        }
    // Allow rows to be rendered.
    }
})

var membersReader=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'Members',
    baseParams:{
        task: "getMembers"
    },
    totalProperty: 'total',
    fields:['accNo','PID','Names','billDate','billNumber','Amount']
});

var membersColModel=function(finish,start){
    var columns = [
        {id:'accNo', header: "accNo", width: 55,sortable: true,dataIndex: 'accNo',filterable: true},
        {id:'PID',header: "PID",width: 80,sortable: true,dataIndex: 'PID',filterable: true},
        {id:'Names', header: "Names",width: 150,sortable: true,dataIndex: 'Names',filterable: true},
        {header: "billDate",width: 80,sortable: true,dataIndex: 'billDate',filter: {type: 'string'}},
        {header: "billNumber", width: 80,sortable: true,dataIndex: 'billNumber',filter: {type: 'string'}},
        {header: "Amount",width: 80,sortable: true,dataIndex: 'Amount',filter: {type: 'string'}}
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}
                
var membersPanel=new Ext.grid.GridPanel({
    id:'membersPanel',
    title:'Members',
    store: membersReader,
    colModel: membersColModel(7),
    closable:true,
    closeAction:'hide',
    listeners: {
        remove: function(ct, c){
            c.hide();
        }
    },
    tbar: [{
        tag: 'input',
        type: 'text',
        size: '30',
        value: '',
        style: 'background: #F0F0F9;'
    }, '->', // next fields will be aligned to the right
    {
        text: 'Add Members',
        handler :function(){
            createMembers.show();
            addMembers.getForm().reset();
            var gridrecord = debtorsGrid.getSelectionModel().getSelected();
            strAccno=gridrecord.get('accno');
            addMembers.getForm().findField("accno").setValue(strAccno);
           
        }
    },
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid2,
        iconCls:'refresh'
    }],
    height:220,
    border: true
    // 
})
//debtorName','address1','address2','phone','joined','os_bal','last_trans'

//=========================================================================================================
//bills pop up window


var cm=new Ext.grid.ColumnModel([
    {id: 'ID', header: 'ID',dataIndex: 'ID',width: 100},
    {id:'pid',header: "PID",width: 80,sortable: true,dataIndex: 'pid'},
    {id:'encounter_nr', header: "encounter_nr",width: 50,sortable: true,dataIndex: 'encounter_nr',
        editor: new fm.TextField({allowBlank: false})},
    { id:'insurance_id',header: "Insurance ID",width: 120, sortable: true,dataIndex: 'insurance_id',
        editor: new fm.TextField({allowBlank: false })},
    { id:'bill_number', header: "Bill Number",width: 80,sortable: true,dataIndex: 'bill_number',
        editor: new fm.TextField({ allowBlank: false})},
    {header: "bill Date", width: 80,sortable: true,dataIndex: 'bill_date',
        editor: new fm.TextField({allowBlank: false})},
    {header: "Bill Time",width: 70,sortable: true,dataIndex: 'bill_time'},
    {header: "IP/OP",width: 40,sortable: true,dataIndex: 'encClass',
        editor: new fm.TextField({allowBlank: false})},
    {header: "PartCode",width: 50,sortable: true,dataIndex: 'partcode'},
    {header: "Description",width: 160,sortable: true,dataIndex: 'Description'},
    { header: "Amount",width: 80, sortable: true, dataIndex: 'Total',
        editor: new fm.TextField({allowBlank: false})}
]
)

cm.defaultSortable = true;

var reader= new Ext.data.JsonReader(
{
    root: 'getBill',
    id:'ID'
},[
        {name:'pid',type:'string'},
        {name:'encounter_nr',type:'string'}, 
        {name:'insurance_id',type:'string'},
        {name:'bill_number',type:'string'}, 
        {name:'bill_date',type:'string'}, 
        {name:'bill_time',type:'string'},
        {name:'encClass',type:'string'}, 
        {name:'partcode', type:'string'}, 
        {name:'Description',type:'string'},
        {name:'Total', type:'string'},
        {name:'ID',type:'numeric'} 
    ]
)


var billsReader=new Ext.data.GroupingStore({
    proxy: new Ext.data.HttpProxy({
        url: 'getDebtors.php',
        method: 'POST'
    }),
    baseParams:{
        task: "getBill"
    },
    reader:reader, 
    sortInfo: {
        field:'pid',
        direction:'ASC'
    },
    groupField:'bill_number'
});

var billsPanel=new Ext.grid.EditorGridPanel({
    id:'billsGrid',
    store: billsReader,
    cm: cm,
    view: new Ext.grid.GroupingView({
        forceFit:true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),
    tbar: ['->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid4,
        iconCls:'refresh'
    }],
    height: 220,
    border: true
})
billsPanel.addListener('afteredit', handletPtypeEdit);

function handletPtypeEdit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;

    //start the process to update the db with cell contents
    updatePtype(editEvent);

}

function updatePtype(oGrid_Event) {

    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d H:i:s');
    } else
{
        var fieldValue = oGrid_Event.value;
    }

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {  
        waitMsg: 'Saving changes...',
        url: 'getDebtors.php',
        params: {
            task: "updateBill", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used
            keyID: oGrid_Event.record.data.ID,
            field: oGrid_Event.field,//the column name
            value: fieldValue,//the updated value
            originalValue: oGrid_Event.record.modified
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            if(oGrid_Event.record.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
                var newID = responseData.newID;
                oGrid_Event.record.set('newRecord','no');
                oGrid_Event.record.set('ID',newID);
                billsReader.commitChanges();
            } else {
                billsReader.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
} //end updateDB

var billsWindow = new Ext.Window({
    title:'Patient Bill',
    layout:'fit',
    id: 'bills',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:800,
    height:350,
    closable:true,
    closeAction:'hide',
    items: [billsPanel]

});

function updateBillNumbers(){
    //     var selectedKeys = allocPanel.selModel.selections.keys;
    var gridrecord=allocPanel.getSelectionModel().getSelected();
    var strPid=gridrecord.get('PID');
    var strBill=gridrecord.get('billNumber');
    var  strAccNo=gridrecord.get('accNo');
    var  strAmount=gridrecord.get('Amount');
    Ext.MessageBox.alert('Allocating Invoice','PID and Bill number '+strPid+' '+strBill)
    //submit to server
    if(strBill==''){
        Ext.MessageBox.alert('Empty Bill number','Please check the bill number')
    }else{
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
        {  
            waitMsg: 'Saving changes...',
            url: 'getDebtors.php',
            params: {
                task: "allocateFund", //pass task to do to the server script
                pid:strPid,
                accNo:strAccNo,
                billNumber:strBill,
                amount:strAmount
            
            },//end params
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            },
            success:function(response,options){
                Ext.MessageBox.alert('Message','Success');
                refreshGrid3();
            }//end success block
        }//end request config
        ); //end request
    }
}



//End of Bill pop up Window
//===================================================================================================

//----------------------------------------------------------------------------------
//Debtor Allocations
//======================================================================================

var allocReader=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'allocations',
    baseParams:{
        task: "getAllocations"
    },
    totalProperty: 'total',
    fields:['accNo','PID','Names','billDate','billNumber','Amount']
});

var sm2 = new Ext.grid.CheckboxSelectionModel({
    listeners: {
        // On selection change, set enabled state of the removeButton
        // which was placed into the GridPanel using the ref config
        selectionchange: function(sm) {
            if (sm.getCount()) {
                allocPanel.removeButton.enable();
            } else {
                allocPanel.removeButton.disable();
            }
        }
    }
});

var allocColModel=function(finish,start){
    var columns = [
    sm2,
    {
        id:'accNo',
        header: "accNo",
        width: 55,
        sortable: true,
        dataIndex: 'accNo',
        filterable: true
    },
    {
        id:'PID',
        header: "PID",
        width: 80,
        sortable: true,
        dataIndex: 'PID',
        filterable: true
    },
    {
        id:'Names',
        header: "Names",
        width: 150,
        sortable: true,
        dataIndex: 'Names',
        filterable: true
    },
    {
        header: "billDate",
        width: 80,
        sortable: true,
        dataIndex: 'billDate',
        filter: {
            type: 'string'
        }
    },
    {
        header: "billNumber",
        width: 80,
        sortable: true,
        dataIndex: 'billNumber',
        filter: {
            type: 'string'
        }
    },
    {
        header: "Amount",
        width: 80,
        sortable: true,
        dataIndex: 'Amount',
        filter: {
            type: 'string'
        }
    }
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var allocPanel=new Ext.grid.GridPanel({
    id:'grid2',
    store: allocReader,
    colModel: allocColModel(7),
    sm:sm2,
    tbar: [{
        tag: 'input',
        type: 'text',
        size: '30',
        value: '',
        style: 'background: #F0F0F9;'
    }, '->', // next fields will be aligned to the right
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshGrid3,
        iconCls:'refresh'
    },{
        text:'Allocate Payment',
        tooltip:'Allocate the Selected Invoice',
        iconCls:'add',

        // Place a reference in the GridPanel
        ref: '../removeButton',
        disabled: true,
        handler: allocateFunds
    },{
        text:'Allocate All Payment',
        tooltip:'Allocate to All Invoices',
        iconCls:'add',

        // Place a reference in the GridPanel
        ref: '../removeButton',
        disabled: true,
        handler: allocateFunds
    },{
        text:'Update Bill Numbers',
        tooltip:'Update Empty Bill Numbers',
        iconCls:'add',
        handler :function(){
            billsWindow.show();
            var gridrecord=allocPanel.getSelectionModel().getSelected();
            var strPid=gridrecord.get('PID');
            //                var strBill=gridrecord.get('billNumber');
            billsReader.load({
                params: {
                    pid:strPid
                        
                }
            });
        }
    }],
    height: 220,
    border: true,
    listeners: {
        render: {
            fn: function(g){
                allocReader.load({
                    params: {
                        start: 0,
                        limit: 30
                    }
                });
                g.getSelectionModel().selectRow(0);
                    delay: 10
            }

        },
        dblclick: {
            fn: function(g){
                billsWindow.show();
                var gridrecord=allocPanel.getSelectionModel().getSelected();
                var strPid=gridrecord.get('PID');
                //                var strBill=gridrecord.get('billNumber');
                billsReader.load({
                    params: {
                        pid:strPid
                        
                    }
                });
            }

        }
    // Allow rows to be rendered.
    }, 
    getSelections : function(){
        //        return [].concat(this.selections.items);
        Ext.MessageBox.alert('selected',[].concat(this.selections.items))
    }
})



function allocateFunds(oGrid_Event){
    //     var selectedKeys = allocPanel.selModel.selections.keys;
    var gridrecord=allocPanel.getSelectionModel().getSelected();
    var strPid=gridrecord.get('PID');
    var strBill=gridrecord.get('billNumber');
    var  strAccNo=gridrecord.get('accNo');
    var  strAmount=gridrecord.get('Amount');
    Ext.MessageBox.alert('Allocating Invoice','PID and Bill number '+strPid+' '+strBill)
    //submit to server
    if(strBill==''){
        Ext.MessageBox.alert('Empty Bill number','Please check the bill number')
    }else{
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
        {  
            waitMsg: 'Saving changes...',
            url: 'getDebtors.php',
            params: {
                task: "allocateFund", //pass task to do to the server script
                pid:strPid,
                accNo:strAccNo,
                billNumber:strBill,
                amount:strAmount
            
            },//end params
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            },
            success:function(response,options){
                Ext.MessageBox.alert('Message','Success');
                refreshGrid3();
            }//end success block
        }//end request config
        ); //end request
    }
}
//===========================================================================
//End of Debtor Allocations
//-----------------------------------------------------------------------------


//"total":"11165","Debtors":[{"accno":"C0318","debtorName":"AIC KAMANGU CHILD DEVCENTRE",
//        "address1":"PO BOX 908", "address2":"LIMURU","phone":"","joined":"1899-12-30 00:00:00", 
//        "category":"COMPANIES","os_bal":"-220157.00","last_trans":"2011-09-20 00:00:00", 
//        "un_alloc":"0.00","cr_limit":"70000.00","suspended":"0", "OP_Cover":"0.00","IP_Cover":"0.00",
//        "OP_Usage":"0.00" "IP_Usage":"0.00","OP_Exceed":"0.00","IP_Exceed":"0.00"}
    
var createColModel=function(finish,start){
    var columns = [
 {id:'accno',header: "accno",      width: 55, sortable: true,dataIndex: 'accno',     filterable: true},
            {header: "Debtor Name",width: 120,sortable: true,dataIndex: 'debtorName',filter: {type: 'string'}},
            {header: "address1",   width: 80,sortable:  true,dataIndex: 'address1',  filter: {type: 'string'}},
            {header: "Address",    width: 80,sortable:  true,dataIndex: 'address2',  filter: {type: 'string' }},
            {header: "Debtor Type",width: 80,sortable:  true,dataIndex: 'category',  filter: {type: 'string'}},
            {header: "OS Balance", width: 80,sortable:  true,dataIndex: 'os_bal',    filter: {type: 'string' }},
            {header: "Allocated",  width: 80,sortable:  true,dataIndex: 'un_alloc',  filter: {type: 'string' }},
            {header: "Phone",      width: 80,sortable:  true,dataIndex: 'phone',     filter: {type: 'string'}},
            {header: "joined",     width: 80,sortable:  true,dataIndex: 'joined',    filter: {type: 'string' }},
            {header: "last_trans", width: 80,sortable:  true,dataIndex: 'last_trans',filter: {type: 'string'}},
            {header: "OP Cover",   width: 80,sortable:  true,dataIndex: 'OP_Cover',  filter: {type: 'string'}},
            {header: "IP Cover",   width: 80,sortable:  true,dataIndex: 'IP_Cover',  filter: {type: 'string'}},
            {header: "OP Usage",   width: 80,sortable:  true,dataIndex: 'OP_Usage',  filter: {type: 'string'}},
            {header: "IP Usage",   width: 80,sortable:  true,dataIndex: 'IP_Usage',  filter: {type: 'string'}},
            {header: "OP Exceed",  width: 80,sortable:  true,dataIndex: 'OP_Exceed', filter: {type: 'string'}},
            {header: "IP Exceed",  width: 80,sortable:  true,dataIndex: 'IP_Exceed', filter: {type: 'string'}}
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

//========================================================================================
//NOTE:Staff dependants Panel
var dependantsReader=new Ext.data.JsonStore({
    url: 'getDebtors.php',
    root: 'dependants',
    baseParams:{
        task: "getDependants"
    },
    totalProperty: 'total',
    fields:['memberID','PID','Names','memberType', 'OP_Usage', 'IP_Usage','DOB','inputDate']
});

var dependantsColModel=function(finish,start){
    var columns = [
        {id:'memberID',header: "memberID",width: 55,sortable: true,dataIndex: 'memberID',filterable: true},
        {id:'PID',header: "PID",width: 80,sortable: true,dataIndex: 'PID',filterable: true},
        {id:'Names',header: "Names", width: 150,sortable: true,dataIndex: 'Names',filterable: true},
        {header: "memberType",width: 80,sortable: true,dataIndex: 'memberType',filter: {type: 'string'}},
        {header: "OP_Usage",width: 80,sortable: true,dataIndex: 'OP_Usage',filter: {type: 'string'}},
        {header: "IP_Usage",width: 80,sortable: true,dataIndex: 'IP_Usage',filter: {type: 'string'}},
        {header: "DOB", width: 80,sortable: true,dataIndex: 'DOB',filter: {type: 'string'}},
        {header: "inputDate",width: 80,sortable: true,dataIndex: 'inputDate',filter: {type: 'string'}}
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var staffDependants=new Ext.grid.GridPanel({
    id:'staffDependants',
    title:'Staff Dependants',
    store: dependantsReader,
    colModel: dependantsColModel(10),
    closable:false,
    closeAction:'hide',
    tbar: [{
        tag: 'input',
        type: 'text',
        size: '30',
        value: '',
        style: 'background: #F0F0F9;'
    }, '->', // next fields will be aligned to the right
    {
        text: 'Add Dependants',
        handler :function(){
            createDependants.show();
            addDependants.getForm().reset();
            var gridrecord = debtorsGrid.getSelectionModel().getSelected();
            strAccno=gridrecord.get('accno');
            addDependants.getForm().findField("accno").setValue(strAccno);
           
        }
    },
    {
        text: 'Refresh',
        tooltip: 'Click to Refresh the table',
        handler: refreshDependants,
        iconCls:'refresh'
    }],
    height:200,
    border: true//,

})

//End: of staff dependants panel
//================================================================================================

var debtorsGrid = new Ext.grid.EditorGridPanel({
    title: 'Debtors List',
    id:'debtorsGrid',
    store: empreader,
    colModel: createColModel(16),
    frame:true,
    minWidth:400,
    height:400,
    collapsible:true,
    clicksToEdit: 2,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true,
        listeners: {
            rowselect: function(sm, row, rec) {
                Ext.getCmp("debtors").getForm().loadRecord(rec);
                var gridrecord = debtorsGrid.getSelectionModel().getSelected();
                if (gridrecord == undefined){
                    alert('no good');
                }else{
                    //sends details to family information form
                    var strAccType=gridrecord.get('category');
                    
                    if(strAccType=='STAFF MEDICALS NEW'){
                        tabPanel.add('staffDependants');
                        tabPanel.doLayout();
                        tabPanel.setActiveTab('staffDependants');
                        
                        var tabToHide1 = Ext.getCmp('membersPanel');
                        tabPanel.hideTabStripItem(tabToHide1);
      
//                        tabPanel.remove('membersPanel');
                        
                            var gridrecord2 = debtorsGrid.getSelectionModel().getSelected();
                            var strAccno2=gridrecord2.get('accno');
                            dependantsReader.load({
                                params: {
                                    accno:strAccno2,
                                    start: 0,
                                    limit: 30
                                }
                            });
                    }else{
                        tabPanel.add('membersPanel');
                        tabPanel.doLayout();
                        tabPanel.setActiveTab('membersPanel'); 
                       
                        var tabToHide = Ext.getCmp('staffDependants');
                        tabPanel.hideTabStripItem(tabToHide);
                        
//                        tabPanel.remove('listings');
                         var straccno=gridrecord.get('accno');
                         membersReader.load({
                            params:{
                                accno:straccno
                            }
                         });
                    }
                //TODO:Enable allocation of funds                    
                    
                //                    paymentsReader.load({
                //                        params:{
                //                            accno:straccno
                //                        }
                //                    });
                //                    
                //                    allocReader.load({
                //                        params:{
                //                            accno:straccno
                //                        }
                //                    });
                }
            }
        }
    }),
    stripeRows: true,
    tbar: new Ext.Toolbar({
        items: [debtorTypes,{
            tag: 'input',
            type: 'text',
            size: '30', 
            value: '',
            id:'searchParam',
            style: 'background: #F0F0F9;'
        },{
            text: 'Search',
            iconCls:'remove',
            handler: function () {
                var sParam=document.getElementById('searchParam').value;
                //                   Ext.MessageBox.alert('Message', sParam);
                empreader.load({
                    params:{
                        dDesc:sParam,
                        start: 0,
                        limit: 500
                    }
                });
            }
        }, '->', // next fields will be aligned to the right
        {
            text: 'Refresh',
            tooltip: 'Click to Refresh the table',
            handler: refreshGrid,
            iconCls:'refresh'
        }]
    }),

    view: new Ext.ux.grid.BufferView({
        // custom row height
        rowHeight: 20,
        // render rows as they come into viewable area.
        scrollDelay: false
    }),
    listeners: {
        render: {
            fn: function(g){
                empreader.load({
                    params: {
                        start: 0,
                        limit: 400
                    }
                });
                g.getSelectionModel().selectRow(0);
                    delay: 10
            }

        }
    // Allow rows to be rendered.
    },
    plugins: [filters],
    bbar: new Ext.PagingToolbar({
        store: empreader,
        pageSize: 600,
        plugins: [filters]

    })

})


var tabPanel=new Ext.TabPanel({
    id:'tabpanel',
    name:'tabpanel',
    autoScroll:true,
    enableTabScroll : true,
    resizeTabs: true,
    closable:true,
    activeTab: 0,
    minHeight:400,
    listeners: {
        remove: function(ct, c){
            c.hide();
        }
    },                
    items: [{
        title: 'Debtors Details',
        items:[accDetails]
    }
    //            ,{
    //                title: 'Members Data',
    //                items:[membersPanel]
    //            }
    ]
});

var gridForm = new Ext.FormPanel({
    id: 'debtors',
    frame: true,
    labelAlign: 'left',
    title: 'Debtors data',
    bodyStyle:'padding:5px',
    url:'saveDebtor.php',
    width: 1200,
    height:600,
    record: null,
    layout: 'column',    // Specifies that the items will now be arranged in columns
    items: [{
        columnWidth: 0.40,
        layout: 'fit',
        items: {
            items:[debtorsGrid]
        }
    },{//accno,name as debtorName,address1,address2,phone,joined,category,os_bal,last_trans,un_alloc,cr_limit,suspended
        columnWidth: 0.6,
        xtype: 'fieldset',
        title:'Debtors details',
        autoHeight: true,
        bodyStyle: Ext.isIE ? 'padding:0 0 5px 15px;' : 'padding:10px 15px;',
        border: false,
        style: {
            "margin-left": "10px", // when you add custom margin in IE 6...
            "margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'accno',
            name: 'accno'
        },{
            xtype: 'textfield',
            fieldLabel: 'debtorName',
            name: 'debtorName'
        },debtorTypes2,{
            xtype: 'textfield',
            fieldLabel: 'address1',
            name: 'address1'
        },{
            xtype: 'textfield',
            fieldLabel: 'address2',
            name: 'address2'
        },{
            xtype: 'textfield',
            fieldLabel: 'phone',
            name: 'phone',
            width:250
        },tabPanel

        ],
        buttons: [{
            text: 'Save',
            iconCls:'icon-save',
            handler: function(){
                gridForm.form.submit({
                    url:'saveDebtor.php', //php
                    baseParams:{
                        task: "create"
                    },
                    waitMsg:'Saving Data...',
                    success: function (form, action) {
                        Ext.MessageBox.alert('Message', 'Saved OK');

                    },
                    failure:function(form, action) {
                        Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
                    }
                })
            //                refreshGrid();
            }
        },{
            text: 'Cancel'
        },{
            text: 'Create',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Reset',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Delete',
            iconCls:'silk-user-add',
            handler :handleDelEmp
        }]//,
    
    }]

});

function handleDelEmp() {

    //returns array of selected rows ids only
    //    var selectedKeys = grid.selModel.selections.keys;
    var spid=gridForm.getForm().findField('accno').value
    if(spid.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to Remove the Employee?', deleteEmp);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
// end
}
function deleteEmp(btn) {
    var spid=gridForm.getForm().findField('accno').value
    if(btn=='yes')
    {
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getEmpPayments.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "deleteEmps", //pass task to do to the server script
                ID: spid,//the unique id(s)
                key: spid//pass to server same 'id' that the reader used
            },
            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);
                    Ext.MessageBox.alert('OK',' Employee diactivated successfully.');
                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                empreader.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    }//end if click 'yes' on button
} // end deleteRecord


function refreshGrid() {
    empreader.reload();//
} // end refresh


function refreshGrid2() {
    membersReader.reload();//
} // end refresh

function refreshDependants() {
    dependantsReader.reload();//
} 

function refreshGrid3() {
    allocReader.reload();//
} 
function refreshGrid4() {
    billsReader.reload();//
} 

empreader.load();
    //  Create Panel view code. Ignore.
//    createCodePanel('form-grid.js', 'View code to create this Form');