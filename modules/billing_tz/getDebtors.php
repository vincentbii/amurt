<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$limit = $_POST[limit];
$start = $_POST[start];
$task = $_POST[task];
$dCat = $_POST[dCat];
$accno = $_POST[accno];
$dDesc = $_POST[dDesc];
$strName=$_POST[GLName];
$strAccno=$_POST[strAccno];
$debitNo=$_POST[debitNo];


if ($task == "getDebtors") {
    getDebtors($dCat, $dDesc, $limit, $start);
} else if ($task == "getMembers") {
    getMembers($accno, $limit, $start);
} else if ($task == "getDependants") {
    getDependants($accno, $limit, $start);
} else if ($task == "debtorType") {
    getDebtorType();
} else if ($task == "addMember") {
    addMember();
} else if ($task == "addDependants") {
    addDependants();
} else if ($task == "getPayments") {
    getPayments($accno);
} else if ($task == "getAllocations") {
    getAllocations($accno, $limit, $start);
} else if ($task == "allocateFund") {
    allocateFunds($pid, $billNumber);
}else if ($task == "getBill") {
    getBills();
}else if ($task == "updateBill") {
    updateBills();
}else if($task == "getDebit"){
     getDebits($accno,$debitNo, $limit, $start);
}else if($task == "getGLAccounts"){
     getGLAccounts($strName, $limit, $start);
}else if($task == "nextCreditNo"){
     nextCreditNo($limit, $start);
}else if($task == "getDebtorName"){
    getDebtorName($accno);
}else if($task == "createDebit"){
    createDebit();
}else if($task == "createCredit"){
    createCredit();
}

function getDebtors($dCat, $dDesc, $limit, $start) {
    global $db;
    if ($start) {
        $start1 = $start;
    } else {
        $start1 = 0;
    }

    if ($limit) {
        $limit1 = $limit;
    } else {
        $limit1 = 200;
    }
    $sql = "SELECT d.accno,d.name AS debtorName,d.address1,d.address2,d.phone,d.joined,
        k.name AS category,d.os_bal,d.last_trans,d.un_alloc,d.cr_limit,suspended,
        d.`OP_Cover`,d.`IP_Cover`,d.`OP_Usage`,d.`IP_Usage`,d.`OP_Exceed`,d.`IP_Exceed`
        FROM care_ke_debtorcat k LEFT JOIN care_ke_debtors d  ON k.`code`=d.category where d.accno=''";
    if ($dCat) {
        $sql = $sql . " and d.category ='$dCat'";
    } 
    
    if ($dDesc) {
        $sql = $sql . " and d.`name` like '$dDesc%' OR d.`accno` like '$dDesc%'";
    } 
    

    $sql = $sql . " order by d.`name` asc limit $start1,$limit1";

    $result = $db->Execute($sql);
//    $numRows = $result->RecordCount();
//echo $sql;

    $sql2 = 'select count(accno) from care_ke_debtors';
    $result2 = $db->Execute($sql2);
    $total = $result2->FetchRow();
    $total = $total[0];
    echo '{
    "total":"' . $total . '","Debtors":[';
//$counter=0;
    while ($row = $result->FetchRow()) {
        $accno = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[accno]);
        $debtorName = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[debtorName]));
        $address1 = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[address1]));
        $address2 = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[address2]));
        $phone = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[phone]));
        $category = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[category]));
        
        echo '{"accno":"' . $accno . '","debtorName":"' . $debtorName . '","address1":"' . $address1 . '",
        "address2":"' . $address2 . '","phone":"' . $phone . '","joined":"' . $row[5] . '",
        "category":"' . $category . '","os_bal":"' . $row[os_bal] . '","last_trans":"' . $row[last_trans] . '",
        "un_alloc":"' . $row[un_alloc] . '","cr_limit":"' . $row[cr_limit] . '","suspended":"' . $row[suspended] . '",
        "OP_Cover":"' . $row[OP_Cover] . '","IP_Cover":"' . $row[IP_Cover] . '","OP_Usage":"' . $row[OP_Usage] . '",
        "IP_Usage":"' . $row[OP_Exceed] . '","OP_Exceed":"' . $row[OP_Exceed] . '","IP_Exceed":"' . $row[IP_Exceed]. '"},';
 //d.`OP_Cover`,d.`IP_Cover`,d.`OP_Usage`,d.`IP_Usage`,d.`OP_Exceed`,d.`IP_Exceed`
//    if ($counter<$numRows){
//        echo ",";
//    }
//    $counter++;
    }
    echo ']}';
}

function getMembers($accno, $limit, $start) {
    global $db;
    if ($start) {
        $start1 = $start;
    } else {
        $start1 = 0;
    }

    if ($limit) {
        $limit1 = $limit;
    } else {
        $limit1 = 200;
    }

    $sql = "SELECT
    p.`pid`
    , p.`name_first`
    , p.`name_2`
    , p.`name_last`
    , b.`bill_date`
    , b.`bill_number`
    , SUM(b.`total`-(if(service_type='payment',b.total,0)))
    , p.`insurance_ID`
    , c.`accNo`
FROM
    care_person p
    INNER JOIN care_ke_billing b
        ON (p.pid = b.pid)
    INNER JOIN care_tz_company c
        ON (c.id= p.insurance_ID)";

    if ($accno) {
        $sql = $sql . " WHERE `c`.`accNo`='$accno' and b.service_type not in ('payment','NHIF')";
    } else {
        $sql = $sql . "";
    }
    $sql = $sql . " GROUP BY p.pid limit $start1,$limit1";

    $result = $db->Execute($sql);
//    $numRows = $result->RecordCount();
//echo $sql;

    $sql2 = 'select count(accno) from care_ke_debtors';
    $result2 = $db->Execute($sql2);
    $total = $result2->FetchRow();
    $total = $total[0];
    echo '{
    "total":"' . $total . '","Members":[';
//$counter=0;
    while ($row = $result->FetchRow()) {
        $pid = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[0]);
        $firstName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]);
        $lastName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]);
        $surName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[3]);
        $names = $firstName . " " . $lastName . " " . $surName;
        $bill_date = $row[4];
        $bill_Number = $row[5];
        $Amount = $row[6];
        $accNo = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[8]);
        echo '{"accNo":"' . $accNo . '","PID":"' . $pid . '","Names":"' . $names . '","billDate":"' . $bill_date . '",
        "billNumber":"' . $bill_Number . '","Amount":"' . $Amount . '"},';
    }
    echo ']}';
}

function getDependants($accno, $limit, $start) {
    global $db;
    if ($start) {
        $start1 = $start;
    } else {
        $start1 = 0;
    }

    if ($limit) {
        $limit1 = $limit;
    } else {
        $limit1 = 200;
    }

    $sql = "SELECT `memberID`,`PID`,`Names`,`memberType`, `OP_Usage`, `IP_Usage`,`DOB`,`inputDate`
                FROM `care_ke_debtormembers`";

//    $accno1=substr($accno, 2);
    if ($accno) {
        $sql = $sql . " WHERE accno='$accno'";
    } else {
        $sql = $sql . "";
    }
    
    $sql = $sql . " limit $start1,$limit1";

    $result = $db->Execute($sql);
//echo $sql;

    $sql2 = 'select count(memberID) from care_ke_debtormembers';
    $result2 = $db->Execute($sql2);
    $total = $result2->FetchRow();
    $total = $total[0];
    echo '{
    "total":"' . $total . '","dependants":[';
//$counter=0;
    while ($row = $result->FetchRow()) {
        $memberID = $row[memberID];
        $PID =  $row[PID];
        $Names = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[Names]);
        $memberType = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[memberType]);
        $OP_Usage = $row[OP_Usage];
        $IP_Usage = $row[IP_Usage];
        $DOB = $row[DOB];
        $inputDate = $row[inputDate];
        echo '{"memberID":"' . $memberID . '","PID":"' . $PID . '","Names":"' . $Names . '","memberType":"' . $memberType. '",
        "OP_Usage":"' . $OP_Usage . '","IP_Usage":"' . $IP_Usage . '","DOB":"' . $DOB. '","inputDate":"' . $inputDate. '"},';
    }
    echo ']}';
}

function getAllocations($accno, $limit, $start) {
    global $db;
    if ($start) {
        $start1 = $start;
    } else {
        $start1 = 0;
    }

    if ($limit) {
        $limit1 = $limit;
    } else {
        $limit1 = 200;
    }

    $sql = "SELECT p.`pid`,p.`name_first`,p.`name_2`,p.`name_last`,b.`bill_date`,b.`bill_number`
    , SUM(b.`total`) AS Total,p.`insurance_ID`,c.`accNo` ,b.allocated
FROM `care_person` p left JOIN `care_ke_billing` b ON (p.`pid` = b.`pid`) left join care_tz_company c 
ON (c.`id` = p.`insurance_ID`)
    WHERE c.`accNo` IS NOT NULL and b.allocated=0 or b.allocated is null ";

    if ($accno) {
        $sql = $sql . "and c.`accNo`='$accno' ";
    } else {
        $sql = $sql . "";
    }
    $sql = $sql . " GROUP BY pid limit $start1,$limit1";

    $result = $db->Execute($sql);
//    $numRows = $result->RecordCount();
//echo $sql;

    $sql2 = 'select count(accno) from care_ke_debtors';
    $result2 = $db->Execute($sql2);
    $total = $result2->FetchRow();
    $total = $total[0];
    echo '{
    "total":"' . $total . '","allocations":[';
//$counter=0;
    while ($row = $result->FetchRow()) {
        $pid = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[0]);
        $firstName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]);
        $lastName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]);
        $surName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[3]);
        $names = $firstName . " " . $lastName . " " . $surName;
        $bill_date = $row[4];
        $bill_Number = $row[5];
        $Amount = $row[6];
        $accNo = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[8]);
        echo '{"accNo":"' . $accNo . '","PID":"' . $pid . '","Names":"' . $names . '","billDate":"' . $bill_date . '",
        "billNumber":"' . $bill_Number . '","Amount":"' . $Amount . '"},';
    }
    echo ']}';
}

function getPayments($accno) {
    global $db;
    $sql = "SELECT bill_date,batch_no,pid,insurance_id,total FROM care_ke_billing 
        WHERE service_type='payment'";
    if ($accno) {
        $sql = $sql . " and `insurance_id`='$accno' ";
    } else {
        $sql = $sql . "";
    }
    $request = $db->Execute($sql);
//    echo $sql;
    
    echo '{
        "payments":[';
    while ($row = $request->FetchRow()) {
        $dCode = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[0]);
        $dName = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[3]));

        echo '{"payDate":"' . $row[0] . '","refNo":"' . $row[1] . '",
            "payer":"' . $row[2] . '","Total":"' . $row[4]. '","accno":"' . $row[3] . '"},';
    }
    echo ']}';
}


function getDebits($accno,$debitNo, $limit, $start) {
    global $db;
    $sql = "SELECT `ID`,`debitNo`, `accno`,`acc_Description`,`GL_acc`,`Gl_Description`,
  `DB_CD`,`Amount`,`inputDate`,`inputTime`,`inputUser`
FROM `care_ke_debtordb_cd` where accno='$accno' and debitNo='$debitNo'";

    $request = $db->Execute($sql);
//    echo $sql;
    
    echo '{
        "debit":[';
    while ($row = $request->FetchRow()) {
        $accDesc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[acc_Description]);
        $glDesc = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[Gl_Description]));

        echo '{"ID":"' . $row[0] . '","GL_acc":"' . $row[4] . '",
            "Gl_Description":"' . $row[5] . '","Amount":"' . $row[7].'"},';
    }
    echo ']}';
}

function getGLAccounts($strName) {
    global $db;
    $sql = "SELECT accountcode,accountname FROM chartmaster";
    
    if($strName<>''){
       $sql =$sql." where accountname like '$strName%'";
    }
    
    $request = $db->Execute($sql);
//    echo $sql;
    
    echo '{
        "glAccounts":[';
    while ($row = $request->FetchRow()) {
        $accDesc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[accountname]);

        echo '{"accountcode":"' . $row[0] . '","accountname":"' . $row[1] .'"},';
    }
    echo ']}';
}

function getDebtorType() {
    global $db;
    $sql = "SELECT `code`,`name` FROM care_ke_debtorcat";
    $request = $db->Execute($sql);

    echo '{
        "debtorType":[';
    while ($row = $request->FetchRow()) {
        $dCode = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[0]);
        $dName = trim(preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[1]));
        echo '{"dCode":"' . $dCode . '","dName":"' . $dName . '"},';
    }
    echo ']}';
}

function addMember() {
    global $db;
    $pid = $_POST[pid];
    $accno = $_POST[accno];


    $sql1 = "select ID from care_tz_company where accno='$accno'";
    $result1 = $db->Execute($sql1);
    $row = $result1->FetchRow();
//    echo $sql1 . '<br>';

  
    
    $sql3 = "update care_person set insurance_ID='$row[0]' where pid=$pid";
//    echo $sql3;
    if ($db->Execute($sql3)) {
        
        echo '{success:true}';
    } else {
        echo '{success:false,sql' . $sql3 . '}';
    }
    
    $sql2 = "update care_ke_billing set insurance_ID='$row[0]',status='billed' where pid='$pid' and status='pending'";
    if ($db->Execute($sql2)) {
      
        echo '{success:true}';
    } else {
        echo '{success:false,sql' . $sql2 . '}';
    }
}


function addDependants() {
    global $db;
    $pid = $_POST[pid];
    $accno = $_POST[accno];
    $memberID= $_POST[memberID];
    $Names= $_POST[names];
    $memberType = $_POST[memberType];
    
     $date1 = $_POST[dob];
     $date = new DateTime(date($date1));
     $DOB = $date->format("Y/m/d");

    $inputDate = date('Y-m-d');
    $inputUser = $_SESSION['sess_login_username'];

    $sql1 = "select ID from care_tz_company where accno='$accno'";
    $result1 = $db->Execute($sql1);
    $row = $result1->FetchRow();
//    echo $sql1 . '<br>';

      
//    $accno1=substr($accno,2);
    $sql="INSERT INTO `care_ke_debtormembers`
            (`accno`,`memberID`,`PID`,`Names`,`memberType`,`OP_Usage`, `IP_Usage`,
             `DOB`,`inputDate`,`inputUser`)
         VALUES ('$accno','$memberID', '$pid','$Names','$memberType','0.00','0.00',
                '$DOB','$inputDate','$inputUser');";
       if ($db->Execute($sql)) {
        echo '{success:true}';
    } else {
        echo '{success:false,sql' . $sql . '}';
    }

    
    
    $sql3 = "update care_person set insurance_ID='$row[0]' where pid=$pid";
    if ($db->Execute($sql3)) {
        echo '{success:true}';
    } else {
        echo '{success:false,sql' . $sql3 . '}';
    }
    
    $sql2 = "update care_ke_billing set insurance_ID='$row[0]',status='billed' where pid=$pid and status='pending'";
    if ($db->Execute($sql2)) {
        echo '{success:true}';
    } else {
        echo '{success:false,sql' . $sql2 . '}';
    }
}

function createDebit() {
    global $db;
    $accno = $_POST[accno];
    $accDesc= $_POST[accDesc];
    $creditNo = $_POST[creditNo];
    $glCode=$_POST[GL_acc];
    $glDesc=$_POST[GL_Desc];
    $glAmount=$_POST[Amount];
    $input_user = $_SESSION['sess_login_username'];
    
   
    $sql2 = "INSERT INTO `care_ke_debtordb_cd`
            (`debitNo`,`accno`,`acc_Description`,`GL_acc`,`Gl_Description`,`DB_CD`,`Amount`, `inputDate`,
             `inputTime`,`inputUser`)
            VALUES ('$creditNo','$accno', '$accDesc','$glCode','$glDesc','DB',
                   '$glAmount','".date('Y-m-d')."','".date('H:m:s')."','$input_user');";
    if ($db->Execute($sql2)) {
            $newID = $db->Insert_ID();
            echo "{success:true, newID:$newID}";
            
            $newNO=intval( $creditNo+1);
            $sql="update care_ke_invoice set debtorDB_CD=$newNO";
              $db->Execute($sql);
              
            $sql2="Select os_bal from care_ke_debtors where accno='$accno'";
            if($result=$db->Execute($sql2)){
                 $row=$result->FetchRow();
                 $currBal=$row[0];
                 $newBal=intval($currBal-$glAmount);
                 
                 $sql3="Update care_ke_debtors set os_bal=$newBal where accno='$accno'";
                 $db->Execute($sql3);
            }else{
                echo '{success:false,sql='.$sql2.'}';
            }
          
    } else {
        echo '{success:false,sql' . $sql2 . '}';
    }
}

function createCredit() {
    global $db;
    $accno = $_POST[accno];
    $accDesc= $_POST[accDesc];
    $creditNo = $_POST[creditNo];
    $glCode=$_POST[GL_acc];
    $glDesc=$_POST[GL_Desc];
    $glAmount=$_POST[Amount];
    $input_user = $_SESSION['sess_login_username'];
    
   
    $sql2 = "INSERT INTO `care_ke_debtordb_cd`
            (`debitNo`,`accno`,`acc_Description`,`GL_acc`,`Gl_Description`,`DB_CD`,`Amount`, `inputDate`,
             `inputTime`,`inputUser`)
            VALUES ('$creditNo','$accno', '$accDesc','$glCode','$glDesc','CD',
                   '$glAmount','".date('Y-m-d')."','".date('H:m:s')."','$input_user');";
    if ($db->Execute($sql2)) {
            $newID = $db->Insert_ID();
            echo "{success:true, newID:$newID}";
            
            $newNO=intval( $creditNo+1);
            $sql="update care_ke_invoice set debtorCD_DB=$newNO";
            $db->Execute($sql);
            
            $sql2="Select os_bal from care_ke_debtors where accno='$accno'";
            if($result=$db->Execute($sql2)){
                 $row=$result->FetchRow();
                 $currBal=$row[0];
                 $newBal=intval($currBal+$glAmount);
                 
                 $sql3="Update care_ke_debtors set os_bal=$newBal where accno='$accno'";
                 $db->Execute($sql3);
            }else{
                echo '{success:false,sql='.$sql2.'}';
            }
            
    } else {
        echo '{success:false,sql' . $sql2 . '}';
    }
}

function allocateFunds() {
    global $db;
    $pid = $_POST[pid];
    $accno = $_POST[accNo];
    $billNumber = $_POST[billNumber];
    $amount = $_POST[amount];
    $input_user = $_SESSION['sess_login_username'];
    $sql1 = "SELECT p.`pid`, p.`name_first`, p.`name_2`, p.`name_last`, b.`bill_date`
    , b.`bill_number`,b.`ip-op`,b.`total`, p.`insurance_ID`, c.`accNo`,b.allocated
    FROM `care_person` p left JOIN `care_ke_billing` b ON (p.`pid` = b.`pid`)
    left JOIN `care_tz_company` c ON (c.`id` = p.`insurance_ID`) 
    WHERE c.`accNo`='$accno' AND b.pid=$pid AND b.bill_number=$billNumber";
    $result1 = $db->Execute($sql1);

//    echo $sql1 . '<br>';

    while ($row = $result1->FetchRow()) {
        $sql = "update care_ke_billing set allocated='1' where pid='$pid' and bill_number='$billNumber'";
        if ($db->Execute($sql)) {
            echo '{success:true}';
        } else {
            echo '{success:false,sql' . $sql . '}';
        }
    }

    $sql2 = "INSERT INTO `care_ke_debtorallocations`
            (`accno`,`pid`,`bill_Number`,`allocDate`,`allocTime`,`amount`,`inputuser`)
   VALUES ('$accno','$pid','$billNumber','" . date('Y-m-d') . "','" . date("H:m:s") . "','$amount','$input_user')";
    if ($db->Execute($sql2)) {
        echo '{success:true}';
    } else {
        echo '{success:false,sql' . $sql2 . '}';
    }
}

function nextCreditNo() {
    global $db;

    $sql = "select debtorDB_CD from care_ke_invoice";

    $result = $db->Execute($sql);

    if($row = $result->FetchRow()){
        $nextNo=$row[0];
    }else{
          echo $nextNo='1001';
    }
    echo $nextNo;
}


function getDebtorName($accno) {
    global $db;

    $sql = "select d.accno,d.name from care_ke_debtors d where d.accno='$accno'";

    $result = $db->Execute($sql);

    if($row = $result->FetchRow()){
        $debtor=$row[1];
    }else{
        $debtor='nil';
    }
     echo '{"debtorName":"'.$debtor.'"}';
}


function getBills() {
    global $db;
    $pid=$_POST[pid];
   // $bill_number=$_POST[billnumber];

    $sql = "SELECT pid,encounter_nr,insurance_id,bill_number,`IP-OP`,bill_date,bill_time,partcode,
        Description,Total,ID FROM care_ke_billing where pid=$pid and allocated is null";

    $result = $db->Execute($sql);
echo $sql;

    $sql2 = 'select count(accno) from care_ke_debtors';
    $result2 = $db->Execute($sql2);
    $total = $result2->FetchRow();
    $total = $total[0];
    echo '{"getBill":[';

    while ($row = $result->FetchRow()) {
        $Desc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[8]);
        $bill_date = $row[4];
        $bill_Number = $row[5];
        $Amount = $row[6];
        echo '{"pid":"' .  $row[0] . '","encounter_nr":"' . $row[1] . '","insurance_id":"' . $row[2] .
                '","bill_number":"' . $row[3] . '",
        "bill_date":"' . $row[bill_date] . '","bill_time":"' . $row[bill_time] . '","encClass":"' . $row[`IP-OP`] . '",
         "partcode":"' . $row[partcode] . '","Description":"' . $Desc . '","Total":"' . $row[Total]
                . '","ID":"' . $row[ID].'"},';
    }
    echo ']}';
}

function updateBills() {
    /*
     * $key:   db primary key label
     * $id:    db primary key value
     * $field: column or field name that is being updated (see data.Record mapping)
     * $value: the new value of $field
    */

    global $db;
    $key = $_POST['key'];
    $id    = (integer) mysql_real_escape_string($_POST['keyID']);
    $field = $_POST['field'];
    $value = $_POST['value'];
    $newRecord = $id == 0 ? 'yes' : 'no';

    //should validate and clean data prior to posting to the database

    if ($newRecord == 'yes') {
        //INSERT INTO `stock` (`company`) VALUES ('a new company');
        $query = 'INSERT INTO proll_paytypes(`'.$field.'`) VALUES (\''.$value.'\')';
    } else {
        $query = 'UPDATE care_ke_billing SET `'.$field.'` = \''.$value.'\' WHERE `'.$key.'` = '.$id;
    }

    //save data to database
    $result=$db->Execute($query);
    $rows= $db->Affected_Rows();

    if ($rows > 0) {
        if($newRecord == 'yes') {
            $newID = $db->Insert_ID();
            echo "{success:true, newID:$newID}";
        } else {
            echo "{success:true}";
        }
    } else {
        echo "{success:false, error:$query}"; //if we want to trigger the false block we should redirect somewhere to get a 404 page
    }
}

?>
