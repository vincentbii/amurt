<script>
    cal1=new dhtmlxCalendarObject('admDate',true);
    Cal1.setSkin("dhx_black");

   
</script>

<script>
    var dhxWins, w1, grid;

    sgrid = new dhtmlXGridObject('gridbox');
    sgrid.setImagePath('../../include/dhtmlxGrid/codebase/imgs/');
    sgrid.setHeader("item_number,Description,price,qty,total");
    sgrid.setInitWidths("80,270,100,60,100");
    sgrid.setSkin("light");
    sgrid.setColTypes("ed,ed,ed,ed,ed");
    sgrid.setColSorting("str,str,str,str");
    sgrid.setColumnColor("white,white,white");
    sgrid.enableDragAndDrop(true);
    sgrid.attachEvent("onEditCell",doOnCellEdit);
    sgrid.init();
    sgrid.enableSmartRendering(true);
    myDp=new dataProcessor('');
    sgrid.submitOnlyChanged(false);

    function initPsearch(){
        dhxWins = new dhtmlXWindows();

        dhxWins.setImagePath("../../include/dhtmlxWindows/codebase/imgs/");

        w1 = dhxWins.createWindow("w1", 462, 200, 340, 250);
        w1.setText("Search Patient");

        grid = w1.attachGrid();
        grid.setImagePath("../../include/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("Patient ID,first Name,Surname,Last name,en_nr");
        grid.attachHeader("#connector_text_filter,#connector_text_filter,#connector_text_filter");
        grid.setSkin("light");
        grid.setColTypes("ro,ro,ro,ro,ro");
        grid.setInitWidths("80,80,80,80,60");
        grid.loadXML("pSearch_pop.php");
        grid.attachEvent("onRowSelect",doOnRowSelected3);
        grid.attachEvent("onEnter",doOnEnter3);
        //grid.attachEvent("onRightClick",doonRightClick);
        grid.init();
        grid.enableSmartRendering(true);
    }

    function initRsearch(){
        dhxWins = new dhtmlXWindows();

        dhxWins.setImagePath("../../include/dhtmlxWindows/codebase/imgs/");

        w1 = dhxWins.createWindow("w1", 600, 200, 400, 250);
        w1.setText("Search Patient");

        grid = w1.attachGrid();
        grid.setImagePath("../../include/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("revenue code,Description,unitprice");
        grid.attachHeader("#connector_text_filter,#connector_text_filter");
        grid.setSkin("light");
        grid.setColTypes("ro,ro,ro");
        grid.setInitWidths("60,200,80");
        grid.loadXML("RSearch_pop.php");
        grid.enableDragAndDrop(true);
        grid.attachEvent("onRowSelect",doOnRowSelected);
        grid.attachEvent("onEnter",doOnEnter);
        //grid.attachEvent("onRightClick",doonRightClick);
        grid.init();
        grid.enableSmartRendering(true);
    }

    function doOnCellEdit(stage,rowId,cellInd){

        var qty=sgrid.cells(sgrid.getSelectedId(),3).getValue();
        var amnt=sgrid.cells(sgrid.getSelectedId(),2).getValue();
        var total=amnt*qty;
        if(stage==2 && cellInd==3){
            sgrid.cells(rowId,4).setValue(total);
            document.getElementById('total').value=sumColumn(4);
        }
        return true;
    }

    function addRows(rowcount){
        var i=0;
        for(i=1; i<=rowcount; i++){
            var newId = (new Date()).valueOf();
            sgrid.addRow(newId,"",sgrid.getRowsNum())
        }
        sgrid.selectRow(mygrid.getRowIndex(newId),false,false,true);

    }

    function getBalance(str){
        xmlhttp2=GetXmlHttpObject();
        if (xmlhttp2==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="insuranceFns.php?desc6="+str;
        url=url+"&caller=getAdDsDate";
        url=url+"&sid="+Math.random();
        xmlhttp2.onreadystatechange=stateChanged2;
        xmlhttp2.open("POST",url,true);
        xmlhttp2.send(null);

    }

    function stateChanged2()
    {
        //get payment description
        if (xmlhttp2.readyState==4)//show point desc
        {
            str=xmlhttp2.responseText;
            //str2=str.split(",");

            document.getElementById('admDate').value=str;
//            document.getElementById('disDate').value=str2[1];
   //         document.getElementById('wrdDays').value=str2[2];


        }
    }


	function getDisDate(){
        xmlhttp6=GetXmlHttpObject();
        if (xmlhttp6==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="accountingFns.php?desc5="+document.getElementById('pid').value;
        url=url+"&caller=getDisDate";
        url=url+"&sid="+Math.random();
        xmlhttp6.onreadystatechange=stateChanged6;
        xmlhttp6.open("POST",url,true);
        xmlhttp6.send(null);

    }

    function stateChanged6()
    {
        //get payment description
        if (xmlhttp6.readyState==4)//show point desc
        {
           var str=xmlhttp6.responseText;
        update = new Date(str);
        theMonth = update.getMonth();
        theDate = update.getDate();
        theYear = update.getYear();
        //document.getElementById('discDate').value=theMonth + "/" + theDate + "/" + theYear ;
        document.getElementById('discDate').value=str;


        }
    }

function getInvoiceDetails(){
        xmlhttp1=GetXmlHttpObject();
        if (xmlhttp1==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="insuranceFns.php?desc4="+document.getElementById('pid').value;
        url=url+"&caller=getInvoiceno";
        url=url+"&sid="+Math.random();
        xmlhttp1.onreadystatechange=stateChanged;
        xmlhttp1.open("POST",url,true);
        xmlhttp1.send(null);

    }

     function stateChanged()
    {
        //get payment description
        if (xmlhttp1.readyState==4)//show point desc
        {
           var str=xmlhttp1.responseText;
           str3=str.split(",");
            document.getElementById('invNo').value=str3[0];
            document.getElementById('invAmount').value=str3[1];
//        alert(str);
        }
    }

    function getInsurance(){
        xmlhttp7=GetXmlHttpObject();
        if (xmlhttp7==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="insuranceFns.php?desc2="+document.getElementById('pid').value;
        url=url+"&caller=getInsurance";
        url=url+"&sid="+Math.random();
        xmlhttp7.onreadystatechange=stateChanged7;
        xmlhttp7.open("POST",url,true);
        xmlhttp7.send(null);
    }

     function stateChanged7()
    {
        //get payment description
        if (xmlhttp7.readyState==4)//show point desc
        {
           var str=xmlhttp7.responseText;
            str3=str.split(",");
            document.getElementById('isuranceID').value=str3[0];
            document.getElementById('insuName').value=str3[1];

        }
    }

//     function days_between(date1, date2) {
//
//            var ONE_DAY = 1000 * 60 * 60 * 24
//
//            var date1_ms = date1.getTime()
//            var date2_ms = date2.getTime()
//
//            var difference_ms = Math.abs(date1_ms - date2_ms)
//
//            return Math.round(difference_ms/ONE_DAY)
//
//        }

    function getDays(){
             firstdate=document.getElementById('admDate').value;
            seconddate=document.getElementById('discDate').value;
            strdays=dateDiff(seconddate,firstdate);

            document.getElementById('wrdDays').value=strdays;
    }

    function sumColumn(ind){
        var out = 0;
        for(var i=0;i<sgrid.getRowsNum();i++){
            out+= parseFloat(sgrid.cells2(i,ind).getValue())
        }
        return out;
    }

    function doOnRowSelected3(id,ind){
        names=grid.cells(id,1).getValue()+" "+grid.cells(id,3).getValue()+" "+grid.cells(id,2).getValue()
        document.getElementById('pid').value=grid.cells(id,0).getValue();
        document.getElementById('pname').value=names;
        document.getElementById('en_nr').value=grid.cells(id,4).getValue();



    }

    function doOnEnter3(rowId,cellInd){
        names=grid.cells(rowId,1).getValue()+" "+grid.cells(rowId,3).getValue()+" "+grid.cells(rowId,2).getValue()
        document.getElementById('pid').value=grid.cells(rowId,0).getValue();
        document.getElementById('pname').value=names;
        document.getElementById('en_nr').value=grid.cells(id,4).getValue();
        closeWindow();
    }
    function doOnRowSelected(id,ind){
        document.getElementById('revcode').value=grid.cells(id,0).getValue();
        document.getElementById('Description').value=grid.cells(id,1).getValue();
        document.getElementById('Amount').value=grid.cells(id,2).getValue();

    }

    function doOnEnter(rowId,cellInd){
        document.getElementById('revcode').value=grid.cells(rowId,0).getValue();
        document.getElementById('Description').value=grid.cells(rowId,1).getValue();
        document.getElementById('Amount').value=grid.cells(rowId,2).getValue();

        closeWindow();
    }
    function closeWindow() {
        dhxWins.window("w1").close();
    }

    function getPatient(str)
    {

        xmlhttp3=GetXmlHttpObject();
        if (xmlhttp3==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="insuranceFns.php?desc3="+str;
        url=url+"&caller=debitGetPt";
        url=url+"&sid="+Math.random();
        xmlhttp3.onreadystatechange=stateChanged3;
        xmlhttp3.open("POST",url,true);
        xmlhttp3.send(null);

    }

    function stateChanged3()
    {
        //get payment description
        if (xmlhttp3.readyState==4)//show point desc
        {
            var str=xmlhttp3.responseText;
            str3=str.split(" ");
            document.getElementById('pname').value=str3[0]+' '+str3[1]+' '+str3[2];//str.split(",",3);
            document.getElementById('en_nr').value=str3[3];

            getBalance(document.getElementById('pid').value);
            getDisDate();
            getInsurance();
            getInvoiceDetails();

        }
    }

    function getCodeDesc(str)
    {

        xmlhttp4=GetXmlHttpObject();
        if (xmlhttp4==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="accountingFns.php?desc4="+str;
        url=url+"&caller=debitGetRv";
        url=url+"&sid="+Math.random();
        xmlhttp4.onreadystatechange=stateChanged4;
        xmlhttp4.open("POST",url,true);
        xmlhttp4.send(null);

    }

    function stateChanged4()
    {
        //get payment description
        if (xmlhttp4.readyState==4)//show point desc
        {
            var str=xmlhttp4.responseText;
            var str2=str.search(/,/)+1
            document.getElementById('Description').value=str //.split(",",1);
            //applyFilter();

        }
    }


    function getNextCrdNo(){
        xmlhttp5=GetXmlHttpObject();
            if (xmlhttp5==null)
            {
                alert ("Browser does not support HTTP Request");
                return;
            }
            var url="insuranceFns.php?desc1=credit";
            url=url+"&sid="+Math.random();
            url=url+"&caller=creditNo";
            xmlhttp5.onreadystatechange=stateChanged5;
            xmlhttp5.open("POST",url,true);
            xmlhttp5.send(null);
     }

      function stateChanged5()
        {
         //get payment description
            if (xmlhttp5.readyState==4)
            {
                var str=xmlhttp5.responseText;
                 //str2=str.search(/,/)+1;
                document.debit.crNo.value=str;
            }
        }

        function getBillBalance(){
            var bal;
            totalCrdit=document.debit.totalCrdit.value;
            invAmount=document.debit.invAmount.value
            bal=invAmount-totalCrdit;
            invAmount=document.debit.balance.value=bal;
        }


    function GetXmlHttpObject()
    {
        if (window.XMLHttpRequest)
        {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject)
        {
            // code for IE6, IE5
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }
</script>