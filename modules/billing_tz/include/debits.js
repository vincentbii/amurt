
Ext.QuickTips.init();

// turn on validation errors beside the field globally
Ext.form.Field.prototype.msgTarget = 'side';

var fm = Ext.form;
var primaryKey='ID'; //primary key is used several times throughout
// custom column plugin example
var selectedKeys;
var url = {
    local:  'getDebtors.php',  // static data file
    remote: 'getDebtors.php'
};
// configure whether filter query is encoded or not (initially)
var encode = false;

// configure whether filtering is performed locally or remotely (initially)
var local = true;

//===============================================================================================
//Get GL Accounts Window
//-------------------------------------------------------------------------------------------------
var glColModel=function(finish,start){
    var columns = [
    {
        id:'accountcode',
        header: "accountcode",
        width: 80,
        sortable: true,
        dataIndex: 'accountcode'
    },{
        id:'accountname',
        header: "accountname",
        width: 150,
        sortable: true,
        dataIndex: 'accountname'
    }
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var glStore = new Ext.data.JsonStore({
    id: 'user',
    url: 'getDebtors.php',
    root: 'glAccounts',
    baseParams:{
        task: "getGLAccounts"
    },
    fields:['accountcode','accountname' ],
    autoSave: false
});
glStore.load();
   
var glAccountGrid= new Ext.grid.GridPanel({
    id: 'addEmpPayments',
    store:glStore,
    colModel: glColModel(2),
    frame:false,
    width:500,
    height:500,
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    tbar: new Ext.Toolbar({
        items: [{
            tag: 'input',
            type: 'text',
            size: '30', 
            value: '',
            id:'searchParam',
            style: 'background: #F0F0F9;'
        },{
            text: 'Search',
            iconCls:'remove',
            handler: function () {
                var sParam=document.getElementById('searchParam').value;
                //                   Ext.MessageBox.alert('Message', sParam);
                glStore.load({
                    params:{
                        GLName:sParam,
                        start: 0,
                        limit: 500
                    }
                });
            }
        }]
    }),
    listeners:{
        celldblclick:function(glAccountGrid,rowIndex,colIndex,e){
            var gridrecord1 = glAccountGrid.getSelectionModel().getSelected();
            strCode=gridrecord1.get('accountcode');
            strName=gridrecord1.get('accountname');
            //           Ext.MessageBox.alert('Message', 'Row Value '+strCode+','+strName);
            var sel_model = debitGrid.getSelectionModel();
            var record = sel_model.getSelected();
            record.set("GL_acc",strCode);
            record.set("Gl_Desc",strName);
        }  
    },
    buttons:[{
            text:'Close',
            handler:function(){
                getGLAccounts.hide();
            }
    }    
    ]
})

getGLAccounts = new Ext.Window({
    title:'General Ledger Accounts',
    layout:'fit',
    id: 'GLAccounts',
    bodyStyle: 'padding:15px;background:transparent',
    border: true,
    width:500,
    height:500,
    closable:true,
    closeAction:'hide',
    items: [glAccountGrid]

});


//=================================================================================================
var debitStore = new Ext.data.JsonStore({
    id: 'user',
    url: 'getDebtors.php',
    root: 'debit',
    baseParams:{
        task: "getDebit"
    },
    fields:['ID','GL_acc','Gl_Desc','Amount' ],
    autoSave: false
});
   


//accno,name as debtorName,address1,address2,phone,joined,category,os_bal,last_trans,un_alloc,cr_limit,suspended
var createColModel=function(finish,start){
    var columns = [
    {
        id:'ID',
        header: "ID",
        width: 55,
        sortable: true,
        dataIndex: 'ID'
    },{
        id:'GL_acc',
        header: "A/C No",
        width: 55,
        sortable: true,
        editor: new fm.TextField({
            allowBlank: false
        }),
        dataIndex: 'GL_acc'
    },
    {
        id:'Gl_Desc',
        header: "Description",
        width: 120,
        sortable: true,
        editor: new fm.TextField({
            allowBlank: false
        }),
        dataIndex: 'Gl_Desc'
    },
    {
        id:'Amount',
        header: "Amount",
        width: 80,
        sortable: true,
        editor: new fm.TextField({
            allowBlank: false
        }),
        dataIndex: 'Amount'
    }
    ];
    return new Ext.grid.ColumnModel({
        columns: columns.slice(start || 0, finish),
        defaults: {
            sortable: true
        }
    });
}

var debitGrid = new Ext.grid.EditorGridPanel({
    title: 'Debit List',
    id:'debitGrid',
    store: debitStore,
    colModel: createColModel(4),
    frame:true,
    width:500,
    height:500,
    collapsible:true,
    clicksToEdit: 2,
    selModel: new Ext.grid.RowSelectionModel({
        singleSelect:false
    }),
    stripeRows: true,
    tbar: new Ext.Toolbar({
        items: [ '->', // next fields will be aligned to the right
        {
            text: 'Refresh',
            tooltip: 'Click to Refresh the table',
            handler: refreshGrid,
            iconCls:'refresh'
        },{
            text:'ADD Row',
            tooltip:'Add Debits',
            iconCls:'add',
            disabled: false,
            handler : function(){
                // access the Record constructor through the grid's store
                var debit = debitGrid.getStore().recordType;
                var p = new debit({
                    ID: 0,
                    GL_acc: 0,
                    Gl_Desc: 0,
                    Amount: 0
                });
                debitGrid.stopEditing();
                debitStore.insert(0, p);
                debitGrid.startEditing(0, 0);
            }

        }]
    })
})


var gridForm = new Ext.FormPanel({
    id: 'debtors',
    frame: true,
    labelAlign: 'left',
    title: 'Debtors data',
    bodyStyle:'padding:5px',
    url:'getDebtors.php',
    width: 800,
    height:500,
    record: null,
    // Specifies that the items will now be arranged in columns
    items: [{//accno,name as debtorName,address1,address2,phone,joined,category,os_bal,last_trans,un_alloc,cr_limit,suspended
        title:'Miscellaneous Debits',
        autoHeight: true,
        border: true,
        xtype: 'fieldset',
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Credit No',
            name: 'creditNo',
            id:'creditNo',
            listeners:{
                blur:function(){
                    NextCreditNo();
                }
            }
        },{
            xtype: 'compositefield',
            fieldLabel: 'Account No',
            msgTarget : 'side',
            anchor    : '-20',
            defaults: {
                flex: 1
            },
            items: [
            {
                xtype: 'textfield',
                name : 'accno',
                id:'accno',
                width:125,
                listeners:{
                    blur:function(){
                        getDebtorName(this.value);
                    }
                }
            },
            {
                xtype : 'textfield',
                name  : 'accDesc',
                id:"accDesc",
                width:200
            }
            ]
        },{
            xtype: 'textfield',
            fieldLabel: 'Description',
            name: 'description'
        },{
            xtype: 'textfield',
            fieldLabel: 'Date',
            name: 'debitDate'
        },{
            xtype: 'panel',
            height:250,
            items: [{
                items:[debitGrid]
            }
            ]
        }],
        buttons: [{
            text: 'Save',
            iconCls:'icon-save',
            handler:handleDebit
        //            handler: function(){
        //                var gridrecord1 = debitGrid.getSelectionModel().getSelected();
        //                 strCode=gridrecord1.get('GL_acc');
        //                 strDesc=gridrecord1.get('Gl_Desc');
        //                 strAmount=gridrecord1.get('Amount');
        //                 gridForm.form.submit({
        //                    url:'getDebtors.php', //php
        //                    baseParams:{
        //                        task: "create",
        //                        glCode:strCode,
        //                        glDesc:strDesc,
        //                        amount:strAmount
        //                    },
        //                    waitMsg:'Saving Data...',
        //                    success: function (form, action) {
        //                        Ext.MessageBox.alert('Message', 'Saved OK');
        //
        //                    },
        //                    failure:function(form, action) {
        //                        Ext.MessageBox.alert('Message', 'Save failed, Check that all values are OK ');
        //                    }
        //                })
        //            //                refreshGrid();
        //            }
        },{
            text: 'GL Accounts',
            handler:function(){
                getGLAccounts.show();
            }
        },'->',{
            text: 'Cancel'
        },{
            text: 'Create',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Reset',
            iconCls:'silk-user-add',
            handler:function(){
                gridForm.getForm().reset();
            }
        },{
            text: 'Delete',
            iconCls:'silk-user-add',
            handler :handleDelEmp
        }]//,
    
    }]

});

//debitGrid.addListener('afteredit', handleDebit);

function NextCreditNo(){
    Ext.Ajax.request( 
    {  
        waitMsg: 'Saving changes...',
        url: 'getDebtors.php',
        params: {
            task: "nextCreditNo"
        },
        callback: function (options, success, response) {
            if (success) {
                var newNo = Ext.util.JSON.decode(response.responseText);
                //                    Ext.MessageBox.alert('OK','New No is '+newNo);
                document.getElementById('creditNo').value=newNo;
            } else{
                Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
            }
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            debitStore.reload();
        }
    }
    );
}

function getDebtorName(strAccno){
    var strAccno2=document.getElementById('accno').value;
    Ext.Ajax.request( 
    {  
        waitMsg: 'Saving changes...',
        url: 'getDebtors.php',
        params: {
            task: "getDebtorName",
            accno:strAccno2
        },
        callback: function (options, success, response) {
            if (success) {
                var responseData = Ext.util.JSON.decode(response.responseText);
                    
                //                    Ext.MessageBox.alert('OK','Debtor Name '+responseData.debtorName);
                document.getElementById('accDesc').value=responseData.debtorName;
            } else{
                Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
            }
        },
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            debitStore.reload();
        }
    }
    );
}

function handleDebit(editEvent) {
    //determine what column is being edited
    var gridField = editEvent.field;

    //start the process to update the db with cell contents
    updateDebits(editEvent);

}

function updateDebits(oGrid_Event) {

    if (oGrid_Event.value instanceof Date)
    {   //format the value for easy insertion into MySQL
        var fieldValue = oGrid_Event.value.format('Y-m-d');
    } else
    {
        var fieldValue = oGrid_Event.value;
    }
    var accno=gridForm.getForm().findField('accno').getValue();
    var accDesc=gridForm.getForm().findField('accDesc').getValue();
    var creditNo=gridForm.getForm().findField('creditNo').getValue();
    var description=gridForm.getForm().findField('description').getValue();
    var gridrecord1 = debitGrid.getSelectionModel().getSelected();

    //submit to server
    Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm
    {  
        waitMsg: 'Saving changes...',
        url: 'getDebtors.php',
        params: {
            task: "createDebit", //pass task to do to the server script
            key: primaryKey,//pass to server same 'id' that the reader used
            keyID: gridrecord1.data.ID,
            GL_acc:gridrecord1.data.GL_acc,
            GL_Desc:gridrecord1.data.Gl_Desc,
            Amount:gridrecord1.data.Amount,
            accno:accno,
            accDesc:accDesc,
            creditNo:creditNo,
            description:description,
//            field: oGrid_Event.field,//the column name
//            value: fieldValue,//the updated value
            
            originalValue: gridrecord1.modified
        },//end params
        failure:function(response,options){
            Ext.MessageBox.alert('Warning','Oops...');
        },
        success:function(response,options){
            if(gridrecord1.data.ID == 0){
                var responseData = Ext.util.JSON.decode(response.responseText);//passed back from server
                var newID = responseData.newID;
                oGrid_Event.record.set('newRecord','no');
                oGrid_Event.record.set('ID',newID);
                debitStore.commitChanges();
            } else {
                debitStore.commitChanges();
            }
        }//end success block
    }//end request config
    ); //end request
} //end updateDB



function handleDelEmp() {

    //returns array of selected rows ids only
    //    var selectedKeys = grid.selModel.selections.keys;
    var spid=gridForm.getForm().findField('accno').value
    if(spid.length > 0)
    {
        Ext.MessageBox.confirm('Message','Do you really want to Remove the Employee?', deleteEmp);
    }
    else
    {
        Ext.MessageBox.alert('Message','Please select at least one item to delete');
    }//end if/else block
// end
}
function deleteEmp(btn) {
    var spid=gridForm.getForm().findField('accno').value
    if(btn=='yes')
    {
        //submit to server
        Ext.Ajax.request( //alternative to Ext.form.FormPanel? or Ext.BasicForm.submit
        {   //specify options (note success/failure below that receives these same options)
            waitMsg: 'Saving changes...',
            //url where to send request (url to server side script)
            url: 'getEmpPayments.php',
            //params will be available via $_POST or $_REQUEST:
            params: {
                task: "deleteEmps", //pass task to do to the server script
                ID: spid,//the unique id(s)
                key: spid//pass to server same 'id' that the reader used
            },
            callback: function (options, success, response) {
                if (success) { //success will be true if the request succeeded
                    Ext.MessageBox.alert('OK',response.responseText);//you won't see this alert if the next one pops up fast
                    var json = Ext.util.JSON.decode(response.responseText);
                    Ext.MessageBox.alert('OK',' Employee diactivated successfully.');
                } else{
                    Ext.MessageBox.alert('Sorry, please try again. [Q304]',response.responseText);
                }
            },

            //the function to be called upon failure of the request (server script, 404, or 403 errors)
            failure:function(response,options){
                Ext.MessageBox.alert('Warning','Oops...');
            //ds.rejectChanges();//undo any changes
            },
            success:function(response,options){
                //Ext.MessageBox.alert('Success','Yeah...');
                //commit changes and remove the red triangle which
                //indicates a 'dirty' field
                debitStore.reload();
            }
        } //end Ajax request config
        );// end Ajax request initialization
    }//end if click 'yes' on button
} // end deleteRecord


function refreshGrid() {
    debitStore.reload();//
} // end refresh



debitStore.load();
    //  Create Panel view code. Ignore.
//    createCodePanel('form-grid.js', 'View code to create this Form');