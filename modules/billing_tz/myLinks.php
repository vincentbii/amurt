
<?php

function jsIncludes() {
?>
    <!-- dhtmlxWindows -->

    <link rel="stylesheet" type="text/css" href="../../include/dhtmlxWindows/codebase/dhtmlxwindows.css">
    <link rel="stylesheet" type="text/css" href="../../include/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_skyblue.css">
    <!--<script src='../../include/dhtmlxGrid/codebase/dhtmlxcommon_debug.js'></script>-->
    <script src="../../include/dhtmlxWindows/codebase/dhtmlxcommon.js"></script>
    <script src="../../include/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>

    <!-- Display menus-->
    <link rel="stylesheet" type="text/css" href="../../include/dhtmlxMenu/codebase/skins/dhtmlxmenu_dhx_skyblue.css">
    <link rel="stylesheet" type="text/css" href="../../include/dhtmlxMenu/codebase/skins/dhtmlxmenu_dhx_blue.css">
    <link rel="stylesheet" type="text/css" href="../../include/dhtmlxMenu/codebase/skins/dhtmlxmenu_glassy_blue.css">
    <script  src="../../include/dhtmlxMenu/codebase/dhtmlxcommon.js"></script>
    <script  src="../../include/dhtmlxMenu/codebase/dhtmlxmenu.js"></script>

    <!-- dhtmlxGrid -->
    <link rel='STYLESHEET' type='text/css' href='../../include/dhtmlxGrid/codebase/dhtmlxgrid.css'>

    <script src='../../include/dhtmlxGrid/codebase/dhtmlxgrid.js'></script>
    <script src='../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_form.js'></script>
    <script src='../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js'></script>
    <script src='../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js'></script>
    <script src='../../include/dhtmlxGrid/codebase/dhtmlxgridcell.js'></script>
    <script  src="../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_drag.js"></script>

    <script src="../../include/dhtmlxWindows/codebase/dhtmlxcontainer.js"></script>

    <script src='../../include/dhtmlxdataprocessor/codebase/dhtmlxdataprocessor.js'></script>
    <!--<script src='../../include/dhtmlxdataprocessor/codebase/dhtmlxdataprocessor_debug.js'></script>-->

    <!-- dhtmlxCalendar -->
    <link rel="STYLESHEET" type="text/css" href="../../include/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
    <script src='../../include/dhtmlxCalendar/codebase/dhtmlxcalendar.js'></script>
    <script src='../../include/dhtmlxCalendar/codebase/dhtmlxcommon.js'></script>
    <script>window.dhx_globalImgPath="'../../include/dhtmlxCalendar/codebase/imgs/";</script>

    <!-- dhtmlxCombo -->
<!--    <link rel="STYLESHEET" type="text/css" href="../../include/dhtmlxCombo/codebase/dhtmlxcombo.css">-->
<!--    <script>//window.dhx_globalImgPath="../../include/dhtmlxCombo/codebase/imgs/";</script>-->
<!--    <script src="../../include/dhtmlxCombo/codebase/dhtmlxcommon.js"></script>-->
<!--    <script src="../../include/dhtmlxCombo/codebase/dhtmlxcombo.js"></script>-->


    <script src='../../include/dhtmlxConnector_php/codebase/connector.js'></script>

    <link rel="stylesheet" href="../../css/themes/default/default.css" type="text/css">
    <script language="javascript" src="../../js/hilitebu.js"></script>                          <!-- New Grid Editor-->
    <script language="JavaScript" src="../../js/cross.js"></script>
    <script language="JavaScript" src="../../js/tooltips.js"></script>

    <!--=========================================================================
                            Extjs Source
    =============================================================================-->
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/resources/css/ext-all.css" />
    <!-- GC -->
    <!-- LIBS -->
    <script type="text/javascript" src="../../include/Extjs/adapter/ext/ext-base.js"></script>
    <!-- ENDLIBS -->
    <script type="text/javascript" src="../../include/Extjs/ext-all.js"></script>
    <script type="text/javascript" src="progress-bar.js"></script>
    <link rel="stylesheet" type="text/css" href="progress-bar.css" />

    <!-- Common Styles for the examples -->
    <link rel="stylesheet" type="text/css" href="examples.css" />
    <script type="text/javascript" src="examples.js"></script>
    <!--=========================================================================
                           End of Extjs Source
    =============================================================================-->
<?php
}

function ExtIncludes() {
?>
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/resources/css/ext-all.css">
    <!--        <link rel="stylesheet" type="text/css" href="xml-tree-loader.css" />-->
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/ux/gridfilters/css/GridFilters.css" />
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/ux/gridfilters/css/RangeMenu.css" />
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/ux/css/Portal.css" />

<!--    <link rel="stylesheet" type="text/css" href="../../include/Extjs/ux/css/GroupSummary.css" />-->


    <script type="text/javascript" src="../../include/Extjs/adapter/yui/ext-yui-adapter.js"></script>
    <script type="text/javascript" src="../../include/Extjs/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ext-all-debug.js"></script>

    <script type="text/javascript" src="../../include/Extjs/ux/RowExpander.js"></script>
<!--    <script type="text/javascript" src="grid-plugins.js"></script>-->
    
    <script type="text/javascript" src="../../include/Extjs/ux/XmlTreeLoader.js"></script>

    <script type="text/javascript" src="../../include/Extjs/ux/CheckColumn.js"></script>
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/ux/css/RowEditor.css" />
    <script type="text/javascript" src="../../include/Extjs/ux/RowEditor.js"></script>

    <link rel="stylesheet" type="text/css" href="../../include/Extjs/shared/examples.css" />
    <link rel="stylesheet" type="text/css" href="../../include/Extjs/shared/icons/silk.css" />
    <script type="text/javascript" src="../../include/Extjs/shared/examples.js"></script>
    <!-- Extensions - Paging Toolbar -->
    <script type="text/javascript" src="../../include/Extjs/ux/paging/pPageSize.js"></script>
    <!-- Extensions - Filtering -->
    
    <script type="text/javascript" src="../../include/Extjs/ux/menu/EditableItem.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/menu/RangeMenu.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/gridfilters/menu/ListMenu.js"></script>

<!--     <script type="text/javascript" src="../../include/Extjs/GroupSummary.js"></script>-->
    
    <script type="text/javascript" src="../../include/Extjs/ux/grid/GridFilters.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/grid/filter/Filter.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/grid/filter/StringFilter.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/grid/filter/DateFilter.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/grid/filter/ListFilter.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/grid/filter/NumericFilter.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/grid/filter/BooleanFilter.js"></script>

    <script type="text/javascript" src="../../include/Extjs/printer/Printer-all.js"></script>
    <script type="text/javascript" src="../../include/Extjs/printer/renderers/Base.js"></script>
    <script type="text/javascript" src="../../include/Extjs/export/Exporter-all.js"></script>
    <script type="text/javascript" src="../../include/Extjs/ux/BufferView.js"></script>

<?php
}

function Ext4Includes() {
?>
    <link rel="stylesheet" type="text/css" href="../../include/Ext4/resources/css/ext-all.css" />
    <script type="text/javascript" src="../../include/Ext4/shared/examples.css"></script>
    <script type="text/javascript" src="../../include/Ext4/bootstrap.js"></script>
<?php
}

