<?php

function jsIncludes() {
    ?>
<!-- dhtmlxWindows -->

<link rel="stylesheet" type="text/css" href="../../include/dhtmlxWindows/codebase/dhtmlxwindows.css">
<link rel="stylesheet" type="text/css" href="../../include/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_skyblue.css">
<!--<script src='../../include/dhtmlxGrid/codebase/dhtmlxcommon_debug.js'></script>-->
<script src="../../include/dhtmlxWindows/codebase/dhtmlxcommon.js"></script>
<script src="../../include/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>

<!-- Display menus-->
<link rel="stylesheet" type="text/css" href="../../include/dhtmlxMenu/codebase/skins/dhtmlxmenu_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="../../include/dhtmlxMenu/codebase/skins/dhtmlxmenu_dhx_blue.css">
<link rel="stylesheet" type="text/css" href="../../include/dhtmlxMenu/codebase/skins/dhtmlxmenu_glassy_blue.css">
<script  src="../../include/dhtmlxMenu/codebase/dhtmlxcommon.js"></script>
<script  src="../../include/dhtmlxMenu/codebase/dhtmlxmenu.js"></script>

<!-- dhtmlxGrid -->
<link rel='STYLESHEET' type='text/css' href='../../include/dhtmlxGrid/codebase/dhtmlxgrid.css'>

<script src='../../include/dhtmlxGrid/codebase/dhtmlxgrid.js'></script>
<script src='../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_form.js'></script>
<script src='../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js'></script>
<script src='../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js'></script>
<script src='../../include/dhtmlxGrid/codebase/dhtmlxgridcell.js'></script>
<script src="../../include/dhtmlxGrid/codebase/ext/dhtmlxgrid_drag.js"></script>

<script src="../../include/dhtmlxWindows/codebase/dhtmlxcontainer.js"></script>

<script src='../../include/dhtmlxdataprocessor/codebase/dhtmlxdataprocessor.js'></script>
<!--<script src='../../include/dhtmlxdataprocessor/codebase/dhtmlxdataprocessor_debug.js'></script>

 dhtmlxCalendar -->
<link rel="STYLESHEET" type="text/css" href="../../include/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
<script src='../../include/dhtmlxCalendar/codebase/dhtmlxcalendar.js'></script>
<script src='../../include/dhtmlxCalendar/codebase/dhtmlxcommon.js'></script>
<script>window.dhx_globalImgPath="'../../include/dhtmlxCalendar/codebase/imgs/";</script>

<!-- dhtmlxCombo -->
<link rel="STYLESHEET" type="text/css" href="../../include/dhtmlxCombo/codebase/dhtmlxcombo.css">
<script>window.dhx_globalImgPath="../../include/dhtmlxCombo/codebase/imgs/";</script>
<script src="../../include/dhtmlxCombo/codebase/dhtmlxcommon.js"></script>
<script src="../../include/dhtmlxCombo/codebase/dhtmlxcombo.js"></script>
<script src='../../include/dhtmlxConnector_php/codebase/connector.js'></script>

<script src="dateCalculations.js"></script>
<?
}


 function displayForm() {
    echo '<form name="debit" method="post" action="'. $_SERVER['PHP_SELF'] .'">';
    echo '<table width=80% border="0" cellpadding="0" cellspacing="5">';
    echo ' <tr><td>Credit No:</td>';
    echo '<td colspan=5><input type="text" name="crNo" id="crNo" value="" onclick="getNextCrdNo()"/>
        <input type="text" name="en_nr" id="en_nr"/>
                      </td></tr>';
    echo '<tr><td>Patient No:</td><td colspan=5>
                     <input type="text" size="10" name="pid" id="pid"
                        ondblclick="initPsearch()" onblur="getPatient(this.value)" onclick="getNextCrdNo()"/>';
    echo '<input type="text" name="pname" id="pname" size="40"/>
                        <input type="button" id="search" value="search"
                                onclick="getPatient(document.getElementById("pid").value)" /></td></tr>';
    echo '<tr><td>Insurance No:</td><td colspan=5>
                     <input type="text" size="10" name="isuranceID" id="isuranceID"
                       onblur="getInsurance()" onclick="getInsurance()"/>';
    echo '<input type="text" name="insuName" id="insuName" size="40"/>
                        <input type="button" id="search" value="search"
                                onclick="getPatient(document.getElementById("pid").value)" /></td></tr>';
    echo '<tr><td>Treatment Date:</td';
    echo '<td><input type="text" name="admDate" id="admDate" value="" onclick="getBalance(document.getElementById(pid).value)"/></td>';
    echo '<td></td><td></td>';
    echo '<td></td></tr>';
    echo '<tr><td>Invoice No:</td>';
    echo '<td><input type="text" name="invNo" id="invNo" /></td>';
    echo '<td>Invoice Amount:</td><td><input type="text" name="invAmount" id="invAmount" /></td><td></td></tr>';
    echo '<tr><td>Total Credit:</td>';
    echo '<td><input type="text" name="totalCrdit" id="totalCrdit" onchange="getBillBalance()"/></td>';
    echo '<td>Balance:</td><td colspan=2><input type="text" name="balance" id="balance" /></td></tr>';
    echo '<tr><td></td><td colspan=4 align=center>';
    echo '<input type="submit" name="submit" id="submit" value="save" />&nbsp&nbsp';
    echo '<input type="button" name="cancel" id="cancel" value="cancel" /></td></tr>';
    echo '</table>';
    echo '</form>';

   

}

function insertData($db,$creditNo,$insuranceID,$insurance,$admno,
        $Names,$admDate,$disDate,$ReleaseDate,$wrdDays,$invoiceNo,$BillAmount,
        $Premium,$Balance) {
    require('./roots.php');
    require_once($root_path.'include/care_api_classes/class_tz_billing.php');
    require_once($root_path.'include/care_api_classes/accounting.php');
    
    $bill_obj = new Bill;
    $acc_obj=new accounting();
    
    $debug=true;
    
    $sql="INSERT INTO care2x.care_ke_insurancecredits
	(creditNo,insuranceID,Insurance,inputDate,admno,
	NAMES,admDate,disDate,ReleaseDate,wrdDays,invoiceNo,
        BillAmount,Premium,Balance)
	VALUES
	('$creditNo','$insuranceID','$insurance','".date('Y-m-d')."','$admno',
       '$Names', '$admDate', '$disDate','$ReleaseDate','$wrdDays',
        '$invoiceNo','$BillAmount', '$Premium','$Balance')";

    $result=$db->Execute($sql);
    if($debug) echo $sql;

     if(!isset($new_bill_number)) {
        $new_bill_number=$bill_obj->checkBillEncounter($admno);
    }
    $request = $acc_obj->getEncounterDetails($admno);
    $user = $_SESSION['sess_user_name'];
    $row=$request->FetchRow();
     
    $sql3 = "INSERT INTO care_ke_billing (pid, encounter_nr,insurance_id,bill_date,`ip-op`,bill_number,
        service_type, price,`Description`,notes,
        days,input_user,status,qty,total,rev_code,service_type)
        value('".$admno."','".$row['encounter_nr']."','".$insuranceID."','".date("d-m-y")."','".$row['encounter_class_nr']
        ."','".$new_bill_number."','".$row['drug_class']."','$Balance','Insurance Credit','OP Invoice','$user','"
        ."','Insured','1',-'$Premium','OPinv','OP Invoice')";

    if ($debug) echo $sql3."<br>";
     $db->Execute($sql3);
}

function updateBill($db,$creditNo,$insuranceID,$insurance,$admno,
        $Names,$admDate,$disDate,$ReleaseDate,$wrdDays,$invoiceNo,$BillAmount,
        $Premium,$Balance){
    require('./roots.php');
    require_once($root_path.'include/care_api_classes/accounting.php');
    require_once($root_path.'include/care_api_classes/class_tz_billing.php');
    $bill_obj = new Bill;
    $acc_obj=new accounting();

    $debug=true;
     if(!isset($new_bill_number)) {
        $new_bill_number=$bill_obj->checkBillEncounter($admno);
    }
    $request = $acc_obj->getEncounterDetails($admno);
    $user = $_SESSION['sess_user_name'];
     $row=$request->FetchRow();

    $sql3 = "INSERT INTO care_ke_billing (pid, encounter_nr,insurance_id,bill_date,`ip-op`,bill_number,
        service_type, price,`Description`,notes,
        days,input_user,status,qty,total,rev_code,service_type)
        value('".$admno."','".$row['encounter_nr']."','".$insuranceID."','".date("d-m-y")."','".$row['encounter_class_nr']
        ."','".$new_bill_number."','".$row['drug_class']."','$Balance','OP Invoice','OP Invoice','$user','"
        ."','pending','1','$Balance','OPinv','OP Invoice')";

    if ($debug) echo $sql3."<br>";
     $db->Execute($sql3);

    
}

function updateDbtErp($db,$pn) {
//global $db, $root_path;
    $debug=true;;
    if ($debug) echo "<b>class_tz_billing::updateDbtErp()</b><br>";
    if ($debug) echo "encounter no: $pn <br>";
    ($debug) ? $db->debug=TRUE : $db->debug=FALSE;
    $sql='SELECT b.pid, c.unit_price AS price,c.partcode,c.item_Description AS article,a.prescribe_date,a.qty AS amount,a.bill_number
    FROM care2x.care_ke_billing a INNER JOIN care_tz_drugsandservices c
    ON a.item_number=c.partcode
    INNER JOIN care2x.care_encounter b
    ON a.pid=b.pid and b.pid="'.$pn.'" and service_type="ward procedure"';
    $result=$db->Execute($sql);
    if($weberp_obj = new_weberp()) {
    //$arr=Array();
        while($row=$result->FetchRow()) {
        //$weberp_obj = new_weberp();
            if(!$weberp_obj->transfer_bill_to_webERP_asSalesInvoice($row)) {
                echo 'success<br>';
                echo date($weberp_obj->defaultDateFormat);
            }
            else {
                echo 'failed';
            }
            destroy_weberp($weberp_obj);
        }
    }else {
        echo 'could not create object: debug level ';
    }
}

//If patient is insured transmit to weberp
function updateinsuredDbt($pn) {
    global $db, $root_path;;
    require_once($root_path.'include/care_api_classes/class_tz_insurance.php');
    $insurance_obj = new Insurance_tz;
    $sql='SELECT a.insurance_id as pid, a.price,a.partcode,a.description as article,a.prescribe_date,a.bill_number,
(a.dosage*a.times_per_day*a.days) AS amount,((a.dosage*a.times_per_day*a.days)*a.price) as total
FROM care2x.care_ke_billing a, care2x.care_encounter b
WHERE a.encounter_nr=b.encounter_nr AND a.encounter_nr="'.$pn.'" AND service_type="insured D member"';
    $result=$db->Execute($sql);
    $rows=$result->FetchRow();
    $IS_PATIENT_INSURED=$insurance_obj->is_patient_insured($pn);
    if($IS_PATIENT_INSURED) {
        $result=$db->Execute($sql);

        //$arr=Array();
        while($row=$result->FetchRow()) {
        //$weberp_obj = new_weberp();
            if(!$weberp_obj->transfer_bill_to_webERP_asSalesInvoice($row)) {
                echo 'success member transmission<br>';
                echo date($weberp_obj->defaultDateFormat);
            }
            else {
                echo 'failed member transmission';
            }
            destroy_weberp($weberp_obj);
        }
    }
}
?>

