<?php

include 'functions.php';

$endDate = date("Y-m-d");
$startDate = date("Y-m-d");

if(isset($_POST['preview'])){
  if($_POST['startDate'] <> ''){
    $startDate = $_POST['startDate'];
  }
  if($_POST['endDate']){
    $endDate = $_POST['endDate'];
  }
}
$departments = Departments($startDate, $endDate);
$departmentRecords = PatientDepartmentRecords($startDate, $endDate);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <style type="text/css">
    th { white-space: nowrap; }
  </style>
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
require 'sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            	<div class="box-body">

                <div class="row">
                  <div class="col-md-12">
                    <form name="debtors" method="POST" action="">

                      <table width="100%">
                        <tr>
                        	<td class="text-center"><label>Start Date</label></td>
                          <td>
                            
                            <div class="input-group date">
                      <input name="startDate" value="" type="date" class="form-control input-sm pull-right" id="">
                      
                      </div>
                          </td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="text-center"><label>End Date</label></td>
                          <td>
                            <div class="input-group date">
                      <input name="endDate" value="" type="date" class="form-control input-sm pull-right" id="datepicker2">
                      </div>
                          </td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td>
                            <label></label>
                            <input type="submit" name="preview" value="Preview Transactions">
                          </td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                          <table id="CashDepartmentSummary" cellspacing="1" cellpadding="0" class="table table-bordered table-hover">
                            <thead>
                            	<tr>
                            		<th>Date</th>
                                <th>Total Visits</th>
                                <?php
                                // var_dump($departments, true);
                                $departments = json_decode($departments, true);
                                foreach ($departments as $value) {
                                  echo '<th>'.depart($value[0].$value[1]).'</th>';
                                }
                                ?>
                            	</tr>
                            </thead>
                            <tbody>
                              <?php
                              $departRecords = json_decode($departmentRecords, true);
                              foreach ($departRecords as $value) {
                                echo '<tr>';
                                echo '<th>'.$value['date'].'</th>';
                                echo '<th>'.$value['total_visits'].'</th>';
                                foreach ($departments as $key => $dep) {
                                  echo '<td>'.departmentRecords($dep[0].$dep[1], $value['date']).'</td>';
                                }
                                echo '</tr>';
                              }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th>Total</th>
                                <th></th>
                                <?php
                                foreach ($departments as $key => $value) {
                                  echo '<th></th>';
                                }
                              ?>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                </div>
            	</div>
        	</div>
    	</div>
	</div>
</section>
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>

<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script type="text/javascript">
   $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script type="text/javascript">
   $('#datepicker1').datepicker({
      autoclose: true
    })
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#CashDepartmentSummary').DataTable( {
      "autoWidth": true,
      dom: 'Bfrtip',
        buttons: [
            { 
              extend: 'copyHtml5',
              title: 'Department Visits Summary',
              footer: true },
            { 
              extend: 'excelHtml5', 
              title: 'Department Visits Summary',
              footer: true,

            },
            { 
              extend: 'csvHtml5', 
              title: 'Department Visits Summary',
              footer: true },
            { 
              extend: 'pdfHtml5', 
              title: 'Department Visits Summary',
              
              footer: true }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
 
            // Update footer
            $( api.column( 1 ).footer() ).html(
                total
            );

            total2 = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
 
            // Update footer
            $( api.column( 2 ).footer() ).html(
                total2
            );

            total3 = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
 
            // Update footer
            $( api.column( 3 ).footer() ).html(
                total3
            );

            total4 = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num4 = total4.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                total4
            );

            total5 = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num5 = total5.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                total5
            );

            total6 = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num6 = total6.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                total6
            );

            total7 = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 7 ).footer() ).html(
                total7
            );

            total7 = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 8 ).footer() ).html(
                total7
            );

            total7 = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 9 ).footer() ).html(
                total7
            );

            total7 = api
                .column( 10 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 10 ).footer() ).html(
                total7
            );

            total7 = api
                .column( 11 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 11 ).footer() ).html(
                total7
            );

            total7 = api
                .column( 12 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 12 ).footer() ).html(
                total7
            );

            total7 = api
                .column( 13 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
              var num7 = total7.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 
            // Update footer
            $( api.column( 13 ).footer() ).html(
                total7
            );
        }
    } );
} );
</script>
</body>
</html>
