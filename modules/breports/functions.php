<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require('./roots.php');
require($root_path.'include/inc_environment_global.php');

define('LANG_FILE','billing.php');
define('NO_CHAIN',1);

require_once($root_path.'include/inc_front_chain_lang.php');

function TotalPatients(){
	global $db;
	$sql = "SELECT count(DISTINCT pid) as total from care_encounter";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total'];
}

function TodaysPatients(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(DISTINCT pid) as total from care_encounter where encounter_date = '".$date."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total'];
}

function FirstTimePatients(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(*)as total1 from (SELECT COUNT(pid)
				FROM care_encounter
				GROUP BY pid
				HAVING COUNT(pid) = 1) as total";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total1'];
}

function allFirstTimeVisits($startDate, $endDate){
	global $db;
	$sql = "SELECT p.pid, count(*) as count, CONCAT(p.name_first, ' ',p.name_2, ' ',p.name_last) AS name, e.encounter_date, p.phone_1_nr from care_person p inner join care_encounter e on p.pid = e.pid";
	if($startDate <> '' && $endDate <> ''){
		$sql = $sql . " WHERE encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
	}
	$sql = $sql . " group by p.pid having count(*) = 1";
    $result = $db->Execute($sql);$count = $result->RecordCount();
    $patient = array();
    $patientInc = 0;
    while ($row = $result->FetchRow()) {
		$patient[$patientInc] = array('patientName' => $row['name'], 'patientPhone' => $row['phone_1_nr'], 'patientDate' => $row['encounter_date'], 'patientPID' => $row['pid'], 'count' => $_row['count']);
		$patientInc++;
	}
	$json_array = json_encode($patient);
	return $json_array;
}

function allPatients($startDate, $endDate){
	global $db;

	$sql = "SELECT DISTINCT(e.pid), p.name_first, p.name_2, p.name_last, e.encounter_date, p.phone_1_nr from care_encounter as e inner join care_person p on e.pid = p.pid WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
	$result = $db->Execute($sql);
	$patient = array();
    $patientInc = 0;
    $allPatientCounts = $result->RecordCount();
	while ($row = $result->FetchRow()) {
		$name = $row['name_first'].' '.$row['name_2'].' '.$row['name_last'];

		$sql_count = "SELECT count(*) as total FROM care_encounter WHERE pid = '".$row['pid']."' AND encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
		$result_count = $db->Execute($sql_count);
		$row_visits = $result_count->FetchRow();

		$patient[$patientInc] = array('patientName' => $name, 'patientPhone' => $row['phone_1_nr'], 'patientDate' => $row['encounter_date'], 'patientPID' => $row['pid'], 'visits' => $row_visits[total], 'allPatients' => $allPatientCounts);
		$patientInc++;
	}
	$json_array = json_encode($patient);
	return $json_array;
}

function allRevisits($startDate, $endDate){
	global $db;
	// $weekAgo = date('Y-m-d',strtotime($startDate.' - 1 week'));
	$sql = "SELECT p.pid, count(*) as count, CONCAT(p.name_first, ' ',p.name_2, ' ',p.name_last) AS name, e.encounter_date, p.phone_1_nr from care_person p inner join care_encounter e on p.pid = e.pid";
	if($startDate <> ''){
		$sql = $sql . " WHERE encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
	}
	$sql = $sql . " group by p.pid having count(*) > 1";
    $result = $db->Execute($sql);$patient = array();$patientInc = 0;
    while ($row = $result->FetchRow()){
		$patient[$patientInc] = array('patientName' => $row['name'], 'patientPhone' => $row['phone_1_nr'], 'patientDate' => $row['encounter_date'], 'patientPID' => $row['pid'], 'count' => $row['count']);
		$patientInc++;
	}
	$json_array = json_encode($patient);
	return $json_array;
}

function PatientRevisits(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(*)as total1 from (SELECT COUNT(pid)
				FROM care_encounter
				GROUP BY pid
				HAVING COUNT(pid) > 1) as total";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total1'];
}

function PaymentMode(){
	global $db;

	$sql = "SELECT DISTINCT pay_mode FROM care_ke_receipts";
	$result = $db->Execute($sql);

	$PaymentMode = array();
	$PaymentModeInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('pay_mode' => $row['pay_mode']));
		$PaymentMode[$PaymentModeInc] = $json;
		$PaymentModeInc++;
	}

	$json_array = json_encode($PaymentMode);
	return $json_array;
}

function Cashier(){
	global $db;

	$sql = "SELECT DISTINCT username FROM care_ke_receipts";
	$result = $db->Execute($sql);
	$cashier = array();
	$cashierInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('username' => $row['username']));
		$cashier[$cashierInc] = $json;
		$cashierInc++;
	}
	$json_array = json_encode($cashier);
	return $json_array;
}

function Category(){
	global $db;

	$sql = "SELECT DISTINCT rev_desc FROM care_ke_receipts";
	$result = $db->Execute($sql);

	$Category = array();
	$CategoryInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('desc' => $row['rev_desc']));
		$Category[$CategoryInc] = $json;
		$CategoryInc++;
	}
	$json_array = json_encode($Category);
	return $json_array;
}

function Debtors(){
	global $db;
	$sql = "SELECT * FROM care_ke_debtors where suspended = 0";
	$result = $db->Execute($sql);$debtors = array();$debtors_inc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('accno' => $row['accno'], 'name' => $row['name']));
		$debtors[$debtors_inc] = $json;
		$debtors_inc++;
	}
	$json_array = json_encode($debtors);
	return $json_array;
}

function creditTransactions($debtor, $startDate, $endDate){
	global $db;
	$trans = array();
	$transInc = 0;
	// $sql = "SELECT * FROM care_ke_debtortrans";

	$sql = "SELECT t.id, d.name, t.accno, t.transno, t.pid, t.transdate, t.bill_number, t.amount, p.name_first, p.name_2, p.name_last FROM care_ke_debtortrans as t inner join care_ke_debtors as d on t.accno = d.accno inner join care_person as p on t.pid = p.pid";
	if($debtor <> '' && $debtor <> 1){
		$sql = $sql . " where t.accno = '".$debtor."'";
	}

	if($startDate <> '' && $endDate <> ''){
		$sql = $sql . " and t.transdate between '".$startDate."' and '".$endDate."'";
	}
	$result = $db->Execute($sql);
	while ($row = $result->FetchRow()) {
		$json = (array('id' => $row['id'], 'debtorname' => $row['name'], 'debtor_acc' => $row['accno'], 'transno' => $row['transno'], 'transdate' => $row['transdate'], 'pid' => $row['pid'], 'bill_number' => $row['bill_number'], 'amount' => $row['amount'], 'name_first' => $row['name_first'], 'name_2' => $row['name_2'], 'name_last' => $row['name_last']));
		$trans[$transInc] = $json;
		$transInc++;
	}
	$json_array = json_encode($trans);

	return $json_array;
}

function cashTransactions($cashier, $startDate, $endDate, $pay_mode, $category){
	global $db;

	$sql = "SELECT * FROM care_ke_receipts where DATE_FORMAT(input_date, '%Y-%m-%d') between '".$startDate."' and '".$endDate."'";
	if($cashier <> '' && $cashier <> 1){
		$sql = $sql . " and username = '".$cashier."'";
	}

	if($pay_mode <> '' && $pay_mode <> 1){
		$sql = $sql . " and pay_mode = '".$pay_mode."'";
	}

	if($category <> '' && $category <> 1){
		$sql = $sql . " and rev_desc = '".$category."'";
	}

	$result = $db->Execute($sql);

	$transaction = array();
	$transactionInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('cashier' => $row['username'], 'date' => $row['input_date'], 'payer' => $row['payer'], 'pay_mode' => $row['pay_mode'], 'category' => $row['rev_desc'], 'total' => $row['total']));

		$transaction[$transactionInc] = $json;
		$transactionInc++;
	}
	$json_array = json_encode($transaction);
	return $json_array;
}

function CashDepartmentSummary($startDate, $endDate){
	global $db;

	$sql = "SELECT DISTINCT rev_desc FROM care_ke_receipts WHERE currdate BETWEEN '".$startDate."' AND '".$endDate."'";
	$result = $db->Execute($sql);
	$Summary = array();
	$SummaryInc = 0;
	while ($row = $result->FetchRow()) {
		$sql_pay_mode = "SELECT DISTINCT pay_mode FROM care_ke_receipts";
		$result_pay_mode = $db->Execute($sql_pay_mode);
		$PayMode = array();
		$PayModeInc = 0;
		while ($row_pay_mode = $result_pay_mode->FetchRow()) {
			$sql_amount = "SELECT sum(total) as total FROM care_ke_receipts WHERE pay_mode = '".$row_pay_mode['pay_mode']."' AND currdate BETWEEN '".$startDate."' AND '".$endDate."' AND rev_desc = '".$row['rev_desc']."'";
			$result_amount = $db->Execute($sql_amount);
			$row_amount = $result_amount->FetchRow();
			$json1 = (array('mode' => $row_pay_mode['pay_mode'], 'amount' => $row_amount['total']));
			$PayMode[$PayModeInc] = $json1;
			$PayModeInc++;
		}

		$sql_total = "SELECT sum(total) as sum_total FROM care_ke_receipts WHERE currdate BETWEEN '".$startDate."' and '".$endDate."' and rev_desc = '".$row['rev_desc']."'";
		$result_total = $db->Execute($sql_total);
		$row_total = $result_total->FetchRow();



		$json = (array('dept' => $row['rev_desc'], 'mode_total' => $PayMode, 'sum_total' => $row_total['sum_total']));
		$Summary[$SummaryInc] = $json;
		$SummaryInc++;
	}

	

	$json_array = json_encode($Summary);
	return $json_array;
}

function CashCashierSummary($startDate, $endDate){
	global $db;

	$begin = new DateTime( $startDate );
	$end = new DateTime( $endDate );
	$end = $end->modify( '+1 day' ); 

	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($begin, $interval ,$end);

	$Dates = array();
	$DatesInc = 0;
	foreach($daterange as $date){
	    $date1 = $date->format("Y-m-d");
	    
	    $sql_cashier = "SELECT DISTINCT username FROM care_ke_receipts";
		$result_cashier = $db->Execute($sql_cashier);
		$sql_total = "SELECT sum(total) as total FROM care_ke_receipts WHERE currdate = '".$date1."'";
		$result_total = $db->Execute($sql_total);
		$row_total = $result_total->FetchRow();

		$cashier = array();
		$cashierInc = 0;
		while ($row_cashier = $result_cashier->FetchRow()) {

			$sql = "SELECT sum(total) AS amount FROM care_ke_receipts WHERE currdate = '".$date1."' and username = '".$row_cashier['username']."'";
			$result = $db->Execute($sql);
			$row = $result->FetchRow();

			$json = (array('username' => $row_cashier['username'], 'amount' => $row['amount']));
			$cashier[$cashierInc] = $json;
			$cashierInc++;
		}
	    $Dates[$DatesInc] = (array('date' => $date1, 'usertotal' => $cashier, 'dailyTotal' => $row_total['total']));
	    $DatesInc++;
	    
	}
	$json_array = json_encode($Dates);
	return $json_array;
}

function CreditDepartments($startDate, $endDate){
	global $db;

	$sql = "SELECT DISTINCT service_type FROM care_ke_billing where service_type NOT IN ('Payment') and bill_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$result = $db->Execute($sql);

		$department = array();
		$departmentInc = 0;
		while ($row = $result->FetchRow()) {

			$department[$departmentInc] = array('service_type' => $row['service_type']);
			$departmentInc++;
		}

		$json_array = json_encode($department);
		return $json_array;
}

function CreditDepartmentSummary($startDate, $endDate){
	global $db;

	$begin = new DateTime( $startDate );
	$end = new DateTime( $endDate );
	$end = $end->modify( '+1 day' ); 

	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($begin, $interval ,$end);

	$Dates = array();
	$DatesInc = 0;

	foreach ($daterange as $date) {
		$date1 = $date->format("Y-m-d");

		// $sql_date_total = "SELECT sum(b.total) as date_sum from care_ke_billing as b inner join care_ke_debtortrans as d on b.bill_number = d.bill_number WHERE b.bill_date = '".$date1."'";
		$sql_date_total = "SELECT sum(care_ke_billing.total) as date_sum FROM care_ke_billing inner join care_ke_debtortrans on care_ke_billing.bill_number = trim(care_ke_debtortrans.bill_number) WHERE care_ke_billing.service_type not in ('Payment', 'Payment Adjustment') and care_ke_billing.bill_date = '".$date1."'";
		$result_date_total = $db->Execute($sql_date_total);
		$row_date_total = $result_date_total->FetchRow();

		$sql = "SELECT DISTINCT service_type FROM care_ke_billing where service_type NOT IN ('Payment', 'Payment Adjustment') and bill_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$result = $db->Execute($sql);

		$CreditDepartment = array();
		$CreditDepartmentInc = 0;
		while ($row = $result->FetchRow()) {

			$sql_bill = "SELECT sum(care_ke_billing.total) as bill_total FROM care_ke_billing inner join care_ke_debtortrans on care_ke_billing.bill_number = trim(care_ke_debtortrans.bill_number) WHERE care_ke_billing.service_type not in ('Payment', 'Payment Adjustment') and care_ke_billing.service_type = '".$row['service_type']."' and care_ke_billing.bill_date = '".$date1."'";
			$result_bill = $db->Execute($sql_bill);
			$row_bill = $result_bill->FetchRow();

			$CreditDepartment[$CreditDepartmentInc] = array('service_type' => $row['service_type'], 'bill_total' => $row_bill['bill_total']);
			$CreditDepartmentInc++;
		}

		$Dates[$DatesInc] = array('date' => $date1, 'date_total' => $row_date_total['date_sum'], 'service_total' => $CreditDepartment);
		$DatesInc++;
		
	}
	$json_array = json_encode($Dates);
	return $json_array;
}

function Departments($startDate, $endDate){
	global $db;

	$sql = "SELECT DISTINCT(e.current_dept_nr) as department, d.name_formal from care_encounter as e inner join care_department as d on e.current_dept_nr = d.nr WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."' order by d.name_formal asc";
		$result = $db->Execute($sql);
		$depts = array();
	 if($result->RecordCount() > 0){
	 	
	 	while ($row = $result->FetchRow()) {
	 		array_push($depts, $row['department']);
	 	}
	 }

	$csql = "SELECT DISTINCT(e.testing_dept) as department, d.name_formal from care_test_request_generic as e inner join care_department as d on e.testing_dept = d.nr WHERE e.send_date BETWEEN '".$startDate."' and '".$endDate."' order by d.name_formal asc";
		$cresult = $db->Execute($csql);
		$tdepts = array();
	if($cresult->RecordCount() > 0){
	 	while ($row1 = $cresult->FetchRow()) {
	 		array_push($tdepts, $row1['department']);
	 	}
	 }


	 $departments = array_unique(array_merge($depts, $tdepts));

	 // $departments = $depts + $tdepts;

	 return json_encode($departments);
}

function PatientDepartments($startDate, $endDate){
	global $db;

		$sql = "SELECT DISTINCT(e.current_dept_nr), d.name_formal from care_encounter as e inner join care_department as d on e.current_dept_nr = d.nr WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
		$result = $db->Execute($sql);

		$department = array();
		$departmentInc = 0;
		while ($row = $result->FetchRow()) {
			
			
			$department[$departmentInc] = array('department' => $row['name_formal']);
			$departmentInc++;
		}
		$json_array = json_encode($department);
		return $json_array;

}

function PatientDepartmentRecords($startDate, $endDate){
	global $db;

	$begin = new DateTime( $startDate );
	$end = new DateTime( $endDate );
	$end = $end->modify( '+1 day' ); 

	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($begin, $interval ,$end);

	$Dates = array();
	$DatesInc = 0;

	foreach($daterange as $date){
	    $date1 = $date->format("Y-m-d");

	    $sql_total_visits = "SELECT * from care_encounter WHERE encounter_date = '".$date1."'";
	    $result_total_visits = $db->Execute($sql_total_visits);
	    $record_total_visits = $result_total_visits->RecordCount();
	    $requests_total = Requests($date1);
	    $total_requests = $record_total_visits + $requests_total;

	    $sql = "SELECT DISTINCT(e.current_dept_nr), d.name_formal from care_encounter as e inner join care_department as d on e.current_dept_nr = d.nr WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
			$result = $db->Execute($sql);

			$department = array();
			$departmentInc = 0;
			while ($row = $result->FetchRow()) {
				$sql_dep_count = "SELECT * FROM care_encounter WHERE encounter_date = '".$date1."' and current_dept_nr = '".$row['current_dept_nr']."'";
				$result_dep_count = $db->Execute($sql_dep_count);
				$record_dep_count = $result_dep_count->RecordCount();
				$department[$departmentInc] = array('department' => $row['name_formal'], 'recordCount' => $record_dep_count);
				$departmentInc++;
			}
	    
	    
	    $Dates[$DatesInc] = (array('date' => $date1, 'department' => $department, 'total_visits' => $total_requests));
	    $DatesInc++;
	    
	}
	$json_array = json_encode($Dates);
	return $json_array;
}

function pharmWeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);
	$weeks = array();
	$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = CashPatients($row['min'], $row['max']);
			$numPatients = NumPatients($row['min'], $row['max']);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function cccWeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);
	$weeks = array();
	$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = cccPatients($row['min'], $row['max']);
			$numPatients = cccCashPatients($row['min'], $row['max']);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function denWeeklyReport($year, $month, $dep){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);
	$weeks = array();
	$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = denCashPatients($row['min'], $row['max'], $dep);
			$numPatients = denNumPatients($row['min'], $row['max'], $dep);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function injWeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);$weeks = array();$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = injCashPatients($row['min'], $row['max']);
			$numPatients = injNumPatients($row['min'], $row['max']);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function conWeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);$weeks = array();$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = conCashPatients($row['min'], $row['max']);
			$numPatients = conNumPatients($row['min'], $row['max']);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function xrayWeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);
	$weeks = array();
	$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = xrayCashPatients($row['min'], $row['max']);
			$numPatients = xrayNumPatients($row['min'], $row['max']);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function labWeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max, sum(price) as price FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);
	$weeks = array();
	$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$cash_patients = labCashPatients($row['min'], $row['max']);
			$numPatients = labNumPatients($row['min'], $row['max']);
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min'], 'cash_total' => $cash_patients, 'patients' => $numPatients);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function NumPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'Drug_list' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function cccPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'ccc' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function xrayNumPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT b.encounter_nr FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'xray' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function denNumPatients($startDate, $endDate, $dep){
	global $db;
	$sql = "SELECT DISTINCT b.encounter_nr FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = '$dep' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function conNumPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT p.pid FROM care_ke_billing as b INNER JOIN care_person as d ON b.pid = d.pid WHERE b.service_type = 'consultation' AND b.bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function labNumPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_ke_billing as b INNER JOIN care_person as d ON b.pid = d.pid WHERE b.service_type = 'laboratory' AND b.bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function injNumPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT b.encounter_nr FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'injection' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function CashPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'Drug_list' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or d.insurance_ID = '' or d.insurance_ID = null)";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function cccCashPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'ccc' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function xrayCashPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'xray' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function labCashPatients($startDate, $endDate){
	global $db;
	// $sql = "SELECT sum(a.total) as total FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'lab_test' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";

	$sql = "SELECT sum(a.total) as total FROM care_ke_billing as a INNER JOIN care_person as d ON a.pid = d.pid WHERE a.service_type = 'laboratory' AND bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function denCashPatients($startDate, $endDate, $dep){
	global $db;
	$sql = "SELECT sum(a.total) as total FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = '$dep' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function injCashPatients($startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'injection' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function conCashPatients($startDate, $endDate){
	global $db;
	// $sql = "SELECT sum(a.total) as total FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'consultation' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
	$sql = "SELECT sum(a.total) as total FROM care_ke_billing as a INNER JOIN care_person as d ON a.pid = d.pid WHERE a.service_type = 'CONSULTATION' AND bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '');";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

// function labCashPatients($startDate, $endDate){
// 	global $db;
// 	$sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'lab_test' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND (d.insurance_ID = '-1' or '')";
// 	$result = $db->Execute($sql);
// 	$row = $result->FetchRow();
// 	return $row[total];
// }


function Creditors(){
	global $db;
	$creditSalesSQl = "SELECT a.id, a.accno, a.name FROM care_tz_company as a INNER JOIN care_ke_debtors as b ON a.accno = b.accno WHERE b.suspended = '0'";
			$creditSalesresult = $db->Execute($creditSalesSQl);
			$CreditSales = array();
			$CreditSalesInc = 0;
			if($creditSalesresult->RecordCount() > 0){
				while ($CreditSalesRow = $creditSalesresult->FetchRow()) {
					$CreditSales[$CreditSalesInc] = array('id' => $CreditSalesRow[id], 'name' => $CreditSalesRow[name], 'accno' => $CreditSalesRow[accno]);
					$CreditSalesInc++;
				}
			}
			$json_array = json_encode($CreditSales);
			return $json_array;
}

function Sales($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'Drug_list' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' ";
	if($acc <> '' || $acc <> null){
		$sql .= " AND d.insurance_ID = '".$acc."'";
	}else{
		$sql .= "";
	}
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function cccSales($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'ccc' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function labSales ($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total FROM care_ke_billing as a INNER JOIN care_person as d ON a.pid = d.pid WHERE a.service_type = 'laboratory' AND bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '$acc'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function xraySales($acc, $startDate, $endDate){
	global $db;
	// $sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'xray' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$sql = "SELECT sum(a.total) as total, a.bill_date FROM care_ke_billing as a INNER JOIN care_person as d ON a.pid = d.pid WHERE a.service_type = 'xray' AND a.bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function injSales($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'injection' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function denSales($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total, a.order_date FROM care_ke_internal_orders as a INNER JOIN care_encounter_prescription as b ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'dental' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function conSales($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT sum(a.total) as total, a.bill_date FROM care_ke_billing as a INNER JOIN care_person as d ON a.pid = d.pid WHERE a.service_type = 'consultation' AND a.bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function labPatients($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_ke_billing as a INNER JOIN care_person as d ON a.pid = d.pid WHERE a.service_type = 'laboratory' AND a.bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function Patients($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'Drug_list' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' ";
	if($acc <> '' && $acc <> null){
		$sql .= "AND d.insurance_ID = '".$acc."'";
	}else{
		$sql .= "";
	}
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}
function PatientsCCC($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'ccc' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function xrayPatients($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT d.pid FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'xray' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function injPatients($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT b.encounter_nr FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'injection' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function denPatients($acc, $startDate, $endDate){
	global $db;
	// $sql = "SELECT DISTINCT b.encounter_nr FROM care_encounter_prescription as b INNER JOIN care_ke_internal_orders as a ON a.presc_nr = b.nr INNER JOIN care_encounter as c ON b.encounter_nr = c.encounter_nr INNER JOIN care_person as d ON c.pid = d.pid WHERE b.drug_class = 'dental' AND b.status = 'serviced' AND order_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";

	$sql = "SELECT p.pid, d.status from care_encounter_prescription d inner join care_encounter e on e.encounter_nr = d.encounter_nr inner join care_person p on e.pid = p.pid where d.drug_class = 'dental' and d.status = 'serviced' and d.prescribe_date between '".$startDate."' and '".$endDate."' AND p.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function conPatients($acc, $startDate, $endDate){
	global $db;
	$sql = "SELECT DISTINCT b.pid FROM care_ke_billing as b INNER JOIN care_person as d ON b.pid = d.pid WHERE b.service_type = 'consultation' AND b.bill_date BETWEEN '".$startDate."' AND '".$endDate."' AND d.insurance_ID = '".$acc."'";
	$result = $db->Execute($sql);
	$count = $result->RecordCount();
	$row = $result->FetchRow();
	return $count;
}

function Requests($date){
	global $db;
	$sql = "SELECT count(encounter_nr) as total FROM care_test_request_generic WHERE send_date = '$date' AND status = 'done'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow($result);
	return $row[total];
}

function depart($dept){
	global $db;

	$sql = "SELECT name_formal FROM care_department WHERE nr = '$dept'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow($result);
	return $row[name_formal];
}

function departmentRecords($dept, $date){
	global $db;
	$sql = "SELECT count(*) as total FROM care_test_request_generic WHERE send_date = '$date' AND testing_dept = '$dept' AND status = 'done'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow($result);

	$sql_dep_count = "SELECT count(*) as dep_total FROM care_encounter WHERE encounter_date = '".$date."' and current_dept_nr = '".$dept."'";
	$cresult = $db->Execute($sql_dep_count);
	$crow = $cresult->FetchRow();

	$total_depts = $row['total'] + $crow['dep_total'];

	return $total_depts;
}

function FTP($startDate, $endDate){
	global $db;
	$result = $db->Execute("");
}

function RP($startDate, $endDate){

}

function AP($startDate, $endDate){

}

function Diagnostics($startDate, $endDate){
	global $db;
	
	$sql = "SELECT distinct ICD_10_code, ICD_10_description FROM care_tz_diagnosis";
	$result = $db->Execute($sql);
	$Diagnostics = array();
	$DiagnosticsInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$sql1 = "SELECT count(*) as total from (select b.name_first,b.name_2, b.name_last, a.pid, a.ICD_10_code, b.date_birth, DATEDIFF(CURRENT_DATE, STR_TO_DATE(b.date_birth, '%Y-%m-%d'))/365 AS ageInYears, date_format(a.timestamp, '%Y-%m-%d') AS dignosis_date from care_tz_diagnosis as a inner join care_person as b on a.PID = b.pid and a.ICD_10_code = '".$row['ICD_10_code']."') care_tz_diagnosis where ageInYears <= 5";
			if(($startDate <> '' || $startDate <> null) && ($endDate <> '' || $endDate <> null)){
				$sql1 .= " and dignosis_date between '$startDate' and '$endDate'";
			}else{
				$sql1 .= "";
			}
			$result1 = $db->Execute($sql1);$row1 = $result1->FetchRow();
			$sql2 = "SELECT count(*) as total from (select b.name_first,b.name_2, b.name_last, a.pid, a.ICD_10_code, b.date_birth, DATEDIFF(CURRENT_DATE, STR_TO_DATE(b.date_birth, '%Y-%m-%d'))/365 AS ageInYears, date_format(a.timestamp, '%Y-%m-%d') AS dignosis_date from care_tz_diagnosis as a inner join care_person as b on a.PID = b.pid and a.ICD_10_code = '".$row['ICD_10_code']."') care_tz_diagnosis where ageInYears > 5";
			if(($startDate <> '' || $startDate <> null) && ($endDate <> '' || $endDate <> null)){
				$sql2 .= " and dignosis_date between '$startDate' and '$endDate'";
			}else{
				$sql2 .= "";
			}
			$result2 = $db->Execute($sql2);$row2 = $result2->FetchRow();
			$Diagnostics[$DiagnosticsInc] = array('code' => $row[ICD_10_code], 'desc' => $row[ICD_10_description], '<5' => $row1[total], '>5' => $row2[total]);
			$DiagnosticsInc++;
		}
	}else{
		return false;
	}

	return json_encode($Diagnostics);
}

?>