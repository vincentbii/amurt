


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Patient Records</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="allpatients.php"><i class="fa fa-circle-o"></i> All Patients</a></li>
            <li><a href="firsttime.php"><i class="fa fa-circle-o"></i> First Time Visits</a></li>
            <li><a href="revisits.php"><i class="fa fa-circle-o"></i> Revisits</a></li>
            <li><a href="department_visits.php"><i class="fa fa-circle-o"></i> Patients per department</a></li>
          </ul>
        </li>
       
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Cash & Credit Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="creditreports.php"><i class="fa fa-circle-o"></i>Credit Reports</a></li>
            <li><a href="cashreports.php"><i class="fa fa-circle-o"></i>Cash General Reports</a></li>
            <li><a href="cash_department_summary.php"><i class="fa fa-circle-o"></i>Cash Summary Per Depertment</a></li>
            <li><a href="credit_department_summary.php"><i class="fa fa-circle-o"></i>Credit Summary Per Depertment</a></li>
            <li><a href="cash_cashier_summary.php"><i class="fa fa-circle-o"></i>Cash Summary Per Cashier</a></li>
           
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Department Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pharm_weekly_report.php"><i class="fa fa-circle-o"></i>Pharmavy Weekly Report</a></li>
           <li><a href="xray_weekly_report.php"><i class="fa fa-circle-o"></i>X-RAY Weekly Report</a></li>
           <li><a href="inj_weekly_report.php"><i class="fa fa-circle-o"></i>Injection Weekly Report</a></li>
           <li><a href="den_weekly_report.php"><i class="fa fa-circle-o"></i>Dental Weekly Report</a></li>
           <li><a href="con_weekly_report.php"><i class="fa fa-circle-o"></i>Consultation Weekly Report</a></li>
           <li><a href="lab_weekly_report.php"><i class="fa fa-circle-o"></i>Laboratory Weekly Report</a></li>
           <li><a href="ccc_weekly_report.php"><i class="fa fa-circle-o"></i>CCC Weekly Report</a></li>
           <li><a href="diagnostic_report.php"><i class="fa fa-circle-o"></i>Diagnostic Report</a></li>
          </ul>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>