<?php

include 'functions.php';
if(isset($_POST)) {
    $date = explode('-', $_POST['month']);
    $year = $date[0];
    $month = $date[1];
    echo "Year: ".$year." Month: ".$month;

    // $data = pharmWeeklyReport($year, $month);
    // var_dump($data, true);
    $data = json_decode(xrayWeeklyReport($year, $month), true);
    // $creditors = Creditors();
    // var_dump($creditors, true);
    $creditors = json_decode(Creditors(), true);
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <style type="text/css">
    th { white-space: nowrap; }
  </style>
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
require 'sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            	<div class="box-body">

                <div class="row">
                  <div class="col-md-12">
                    <form name="debtors" method="POST" action="">

                      <table width="100%">
                        <tr>
                          <td>
                            <select name="month" onchange="this.form.submit()">
                            <?php
                            printf('<option selected disabled> -- Select Month -- </option>', $value, $label);
                              for ($i = 0; $i <= 12; ++$i) {
                                $time = strtotime(sprintf('-%d months', $i));
                                $value = date('Y-m', $time);
                                $label = date('F Y', $time);
                                printf('<option value="%s">%s</option>', $value, $label);
                              }
                              ?>
                            </select>
                            </td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
                <br>
                <input name="b_print" type="button" class="ipt"   onClick="PrintElem('report_pharmacy');" value=" Print ">
                <div class="row" id="report_pharmacy">
                  <div class="col-md-12">
                          <table id="pharmacy" class="table table-bordered table-hover">
                            <thead>
                              <tr>
                                <th>
                                  PARAMETERS
                                </th>
                              <?php

                              foreach ($data as $key => $value) {
                                $key3 = $key + 2;
                                echo "<th>".$value[min].' - '.$value[max]."</th>";
                              }
                              echo "<th>Totals</th>";
                              ?>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th class="text-center" colspan="<?php echo $key3; ?>">Sales</th>
                              </tr>
                              <tr>
                                <td class="text-bold">Cash Sales</td>
                                <?php
                                $totalC = 0;
                              foreach ($data as $key => $value) {
                                if($value[cash_total] == null || $value[cash_total] === ''){
                                  $cash_total = 0;
                                }else{
                                  $cash_total = $value[cash_total];
                                }
                                $totalC = $totalC + $cash_total;
                                echo "<td>".number_format($cash_total)."</td>";
                              }
                              echo "<th>".number_format($totalC)."</th>";
                              ?>
                              </tr>
                                <?php
                                foreach ($creditors as $key => $value1) {
                                  echo "<tr>";
                                    echo "<td class='text-bold'>".$value1[name]."</td>";
                                    $salesT = 0;
                                    foreach ($data as $key => $value) {
                                      $sales = xraySales($value1[id], $value[min], $value[max]);
                                      if($sales == '' || $sales == null){
                                        $sales = 0;
                                      }
                                      $salesT = $salesT + $sales;
                                      echo "<td>".number_format($sales)."</td>";
                                    }
                                    echo "<th>".number_format($salesT)."</th>";
                                    echo "</tr>";
                                }
                                ?>
                                <tr>
                                <th class="text-center" colspan="<?php echo $key3; ?>">Patients</th>
                              </tr>
                                <tr>
                                <td class="text-bold">Cash Patients</td>
                                <?php
                                $cPTotals = 0;
                              foreach ($data as $key => $value) {
                                echo "<td>".$value[patients]."</td>";
                                $cPTotals = $cPTotals + $value[patients];
                              }
                              echo "<th>".$cPTotals."</th>";
                              ?>
                              </tr>
                                <?php
                                foreach ($creditors as $key => $value1) {
                                  echo "<tr>";
                                    echo "<td class='text-bold'>".$value1[name]."</td>";
                                    $crPTotals = 0;
                                    foreach ($data as $key => $value) {
                                      $sales = xrayPatients($value1[id], $value[min], $value[max]);
                                      echo "<td>".$sales."</td>";
                                      $crPTotals = $crPTotals + $sales;
                                    }
                                    echo "<th>".$crPTotals."</th>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>

                </div>

            	</div>
        	</div>
    	</div>
	</div>
</section>
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>

<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script type="text/javascript">
   $('#datepicker').datepicker({
      autoclose: true
    })
</script>
<script type="text/javascript">
   $('#datepicker1').datepicker({
      autoclose: true
    })
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pharmacy').DataTable( {
      dom: 'Bfrtip',
        buttons: [
            { 
              extend: 'copyHtml5',
              title: 'Credit Report',
              footer: true },
            { 
              extend: 'excelHtml5', 
              title: 'Credit Report',
              footer: true,

            },
            { 
              extend: 'csvHtml5', 
              title: 'Credit Report',
              footer: true },
            { 
              extend: 'pdfHtml5', 
              title: 'Credit Report',
              footer: true }
        ]
    } );
} );
</script>
<script type="text/javascript">
  function PrintElem(elem){
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>
<style type="text/css">
  @media print {
    .report_pharmacy {
        background-color: white;
        height: 100%;
        width: 100%;
        position: fixed;
        top: 0;
        left: 0;
        margin: 0;
        padding: 15px;
        font-size: 14px;
        line-height: 18px;
    }
}
</style>

</body>
</html>
