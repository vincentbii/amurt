/*
 * File: app/view/IouForm.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('CashBook.view.IouForm', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.field.Date',
        'Ext.form.field.TextArea',
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.grid.column.Column',
        'Ext.toolbar.Paging',
        'Ext.selection.RowModel',
        'Ext.button.Button'
    ],

    height: 555,
    width: 1014,
    layout: 'absolute',
    bodyPadding: 10,
    closable: true,
    collapsible: true,
    title: 'IOU Form',
    url: 'getCashbookFunctions.php?task=insertIOU',

    initComponent: function() {
        var me = this;

        me.initialConfig = Ext.apply({
            url: 'getCashbookFunctions.php?task=insertIOU'
        }, me.initialConfig);

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'textfield',
                    x: -7,
                    y: 5,
                    width: 370,
                    fieldLabel: 'Payee',
                    labelAlign: 'right',
                    name: 'Payee'
                },
                {
                    xtype: 'textfield',
                    x: -7,
                    y: 65,
                    width: 260,
                    fieldLabel: 'Amount Spent',
                    labelAlign: 'right',
                    name: 'AmountSpent',
                    listeners: {
                        change: {
                            fn: me.onTextfieldChange,
                            scope: me
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    x: -7,
                    y: 95,
                    width: 260,
                    fieldLabel: 'Balance',
                    labelAlign: 'right',
                    name: 'Balance'
                },
                {
                    xtype: 'textfield',
                    x: -7,
                    y: 35,
                    itemId: 'AmountGiven',
                    width: 260,
                    fieldLabel: 'Amount Given ',
                    labelAlign: 'right',
                    name: 'AmountGiven'
                },
                {
                    xtype: 'datefield',
                    x: 360,
                    y: 5,
                    width: 245,
                    fieldLabel: 'Date',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'IouDate'
                },
                {
                    xtype: 'textareafield',
                    x: -7,
                    y: 125,
                    height: 48,
                    width: 430,
                    fieldLabel: 'Towards',
                    labelAlign: 'right',
                    name: 'Towards'
                },
                {
                    xtype: 'gridpanel',
                    x: 5,
                    y: 220,
                    frame: true,
                    height: 290,
                    width: 1010,
                    title: 'Active IOUs',
                    store: 'IouStore',
                    columns: [
                        {
                            xtype: 'gridcolumn',
                            width: 62,
                            dataIndex: 'ID',
                            text: 'ID'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'IouDate',
                            text: 'IouDate'
                        },
                        {
                            xtype: 'gridcolumn',
                            width: 200,
                            dataIndex: 'Payee',
                            text: 'Payee'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'AmountGiven',
                            text: 'Amount Given'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'AmountUsed',
                            text: 'Amount Used'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Balance',
                            text: 'Balance'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Narrative',
                            text: 'Narrative'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Status',
                            text: 'Status'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'InputUser',
                            text: 'Input User'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'pagingtoolbar',
                            dock: 'bottom',
                            width: 360,
                            displayInfo: true,
                            store: 'IouStore'
                        }
                    ],
                    listeners: {
                        itemclick: {
                            fn: me.onGridpanelItemClick,
                            scope: me
                        }
                    },
                    selModel: Ext.create('Ext.selection.RowModel', {

                    })
                },
                {
                    xtype: 'button',
                    x: 130,
                    y: 185,
                    height: 30,
                    width: 100,
                    text: 'Save'
                },
                {
                    xtype: 'button',
                    x: 25,
                    y: 185,
                    height: 30,
                    itemId: 'cmdNew',
                    width: 100,
                    text: 'New',
                    listeners: {
                        click: {
                            fn: me.onCmdNewClick,
                            scope: me
                        }
                    }
                },
                {
                    xtype: 'button',
                    x: 235,
                    y: 185,
                    height: 30,
                    width: 95,
                    text: 'Cancel IOU'
                },
                {
                    xtype: 'button',
                    x: 335,
                    y: 185,
                    height: 30,
                    width: 100,
                    text: 'Print'
                },
                {
                    xtype: 'button',
                    x: 775,
                    y: 185,
                    height: 30,
                    width: 110,
                    text: 'Close'
                },
                {
                    xtype: 'textfield',
                    x: 500,
                    y: 110,
                    itemId: 'formStatus',
                    value: 'insert'
                }
            ]
        });

        me.callParent(arguments);
    },

    onTextfieldChange: function(field, newValue, oldValue, eOpts) {
        var strAmountgiven=this.down('#AountGiven').getValue();
        Ext.Msg.alert('test',strAmountgiven);
    },

    onGridpanelItemClick: function(dataview, record, item, index, e, eOpts) {
        this.loadRecord(record);
        this.down('#formStatus').setValue('Update');
    },

    onCmdNewClick: function(button, e, eOpts) {
        this.reset();
    }

});