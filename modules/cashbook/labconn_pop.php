<?php
require_once('roots.php');
require($root_path.'include/inc_environment_global.php');
require("../../include/dhtmlxConnector/codebase/grid_connector.php");
$gridConn=new GridConnector($db);
$gridConn->enable_log("temp.log",true);
$gridConn->dynamic_loading(100);
$gridConn->render_sql("select * from care_tz_drugsandservices where purchasing_class like 'drug_list' and item_status NOT IN (2,3,4)",
        "item_number", "category,purchasing_class,partcode,item_description,unit_price");

?>
