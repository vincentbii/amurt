<?php
require_once('roots.php');
require($root_path.'include/inc_environment_global.php');

require("../../include/dhtmlxConnector/codebase/grid_connector.php");
$gridConn=new GridConnector($db);
$gridConn->enable_log("temp.log",true);
$gridConn->dynamic_loading(100);
$gridConn->render_sql("SELECT distinct p.pid,p.name_first,p.name_last,p.name_2,e.`encounter_nr`,e.`encounter_class_nr`,b.`bill_number` FROM care_person p
                            LEFT JOIN care_encounter e ON p.pid=e.pid
                            LEFT JOIN care_ke_billing b ON e.`encounter_nr`=b.`encounter_nr`
                          WHERE e.encounter_date>DATE_SUB(NOW(),INTERVAL 2 DAY) or e.encounter_class_nr=1 
                          and e.is_discharged=0","pid", "pid,name_first,name_2,name_last,encounter_nr,bill_number,encounter_class_nr");

?>
