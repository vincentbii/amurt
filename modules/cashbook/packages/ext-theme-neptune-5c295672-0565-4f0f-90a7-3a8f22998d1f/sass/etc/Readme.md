# ext-theme-neptune-5c295672-0565-4f0f-90a7-3a8f22998d1f/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-5c295672-0565-4f0f-90a7-3a8f22998d1f/sass/etc"`, these files
need to be used explicitly.
