<?php
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require('roots.php');
require($root_path . 'include/inc_environment_global.php');

function getRevenueCodes($cashpoint, $shiftNo){
	global $db;

	$sql = "SELECT DISTINCT IF(rev_code = 'gl', towards, rev_code) as rev_code, rev_desc FROM care_ke_receipts 
            WHERE cash_point='$cashpoint' and shift_no='$shiftNo'";
    $result = $db->Execute($sql);
    if($result->RecordCount() > 0){
    	$rows = array();
    	$rowsInc = 0;
    	while ($row = $result->FetchRow()) {
	    	$rows[$rowsInc] = array('rev_code' => $row[rev_code], 'rev_desc' => $row['rev_desc']);
	    	$rowsInc++;
	    }
	    return json_encode($rows);
    }
}

function getPaymentMode($cashpoint, $shiftNo){
	global $db;

	$sql = "SELECT DISTINCT pay_mode FROM care_ke_receipts 
            WHERE cash_point='$cashpoint' and shift_no='$shiftNo'";
    $result = $db->Execute($sql);
    if($result->RecordCount() > 0){
    	$rows = array();
    	$rowsInc = 0;
    	while ($row = $result->FetchRow()) {
	    	$rows[$rowsInc] = array('pay_mode' => $row['pay_mode']);
	    	$rowsInc++;
	    }
	    return json_encode($rows);
    }
}

function getTotal($pay_mode, $rev_code, $cashpoint, $shiftNo){
	global $db;
	$sql = "SELECT sum(total) as total FROM care_ke_receipts WHERE pay_mode='$pay_mode' AND IF(rev_code = 'gl', towards, rev_code) ='$rev_code' AND cash_point = '$cashpoint' AND Shift_no = '$shiftNo'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}

function getTotals($pay_mode, $cashpoint, $shiftNo){
	global $db;
	$sql = "SELECT sum(total) as total FROM care_ke_receipts WHERE pay_mode='$pay_mode' AND cash_point = '$cashpoint' AND Shift_no = '$shiftNo'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row[total];
}
?>