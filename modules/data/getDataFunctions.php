
<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$limit = $_REQUEST[limit];
$start = $_REQUEST[start];
$formStatus = $_REQUEST[formStatus];
$searchParam = $_REQUEST[sParam];
$pid = $_REQUEST[pid];
$item_number = $_REQUEST[partcode];
$partcode = $_REQUEST[partcode];
$item_description = $_REQUEST[item_description];
$item_full_description = $_REQUEST[item_full_description];
$unit_price = $_REQUEST[unit_price];
$purchasing_class = $_REQUEST[purchasing_class];
$category = $_REQUEST[category];
$itemStatus = $_REQUEST[item_status];
$sellingPrice = $_REQUEST[selling_price];
$maximum = $_REQUEST[maximum];
$minimum = $_REQUEST[minimum];
$reorder = $_REQUEST[reorder];
$unitMeasure = $_REQUEST[unit_measure];
$icdParam = $_REQUEST[query];
$searchParam = $_REQUEST[query];

$encNr = $_REQUEST[encNr];
$procedureNo= $_REQUEST[procedureNo];
$procedureType= $_REQUEST[procedureType];
//$value = json_decode($_REQUEST['data']);
//echo 'The id is ' .$value[0][0];

$units = 'each';

$task = ($_REQUEST['task']) ? ($_REQUEST['task']) : $_POST['task'];

switch ($task) {
    case "getPatientSafety":
        getPatientSafety($encNr);
        break;
    case "getAnesthesiaCharts":
        getAnesthesiaCharts($encNr);
        break;
    case "CreateAnesthesiaForm":
        CreateAnesthesiaForm($_POST);
        break;
    case "createCheckList1":
        CreateCheckList1($_POST);
        break;
    case "createCheckList2":
        CreateCheckList2($_POST);
        break;
    case "getSurgicalCheckLists":
        getSurgicalCheckLists($start, $limit);
        break;
    case "createCheckList3":
        CreateCheckList3($_POST);
        break;
    case "getItemsList":
        getItemsList($searchParam, $start, $limit);
        break;
    case "getStatusList":
        getStatusList();
        break;
    case "InsertItem":
        if ($formStatus == 'insert') {
            InsertItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure);
        } else if ($formStatus == 'update') {
            updateItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure);
        }
        break;
    case "createBooking":
        if ($formStatus == 'insert') {
            createBooking($_POST);
        } else if ($formStatus == 'update') {
            updateBooking($_POST);
        }
        break;
    case "getItemsCategory":
        getItemsCategory($start, $limit);
        break;
    case "getStaff":
        getStaff($start, $limit);
        break;
    case "getPatientDetails":
        getPatientDetails($pid);
        break;
    case "getTheatreRooms":
        getTheatreRooms($start, $limit);
        break;
    case "getProcedureClass":
        getProcedureClass($start, $limit);
        break;
    case "getProcedureClassList":
        if (isset($_REQUEST['data'])) {
            writeprocedureclass();
        } else {
            getProcedureClassList();
        }
        break;
    case "getProcedures":
        getProcedures($searchParam, $start, $limit);
        break;
    case "getDiagnosisList":
        getDiagnosisList($icdParam, $start, $limit);
        break;
    case "getClassCodes":
        getClassCodes($start, $limit);
        break;
    case "getTheatreList":
        getTheatreList($start, $limit);
        break;
    case "getItemsSubCategory":
        getItemsSubCategory($start, $limit);
        break;
    case "getUnitsofMeasure":
        getUnitsofMeasure($start, $limit);
        break;
    case "getStoreLocations":
        getStoreLocations($start, $limit);
        break;
    case "getItemLocation":
        getItemLocations($start, $limit);
        break;
    case "deleteItem":
        deleteItem();
        break;
    case "addSpongeItems":
        addSpongeItems($encNr,$procedureType,$procedureNo);
        break;
    case "getSpongeItems":
        getSpongeItems($encNr,$procedureNo,$procedureType);
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch
//0727588512

function checkSponges($encNr,$procedureNo){
    global $db;
    $debug=false;
    
    $sql="Select count(id) from care_ke_anestheatresponge where encounter_nr=$encNr and ProcedureNo='$procedureNo'";
    if($debug){ echo $sql; }
    $request=$db->Execute($sql);
    $row=$request->FetchRow();
    
    if($row[0]>0){
        return true;
    }else{
        return false;
    }
}

function  getSpongeItems($encNr,$procedureNo,$procedureType){
    global $db;
    
    $debug=false;
    
   
    
    $json = $_POST['editedData'];
    $rctData = json_decode($json, TRUE);
    //echo $rctData[encounter_nr];

    if ($encNr == '') {
        $encNr = $rctData[Encounter_Nr];
    }
    
     if ($procedureType == '') {
        $procedureType = $rctData[ProcedureType];
    }
    
     if ($procedureNo == '') {
        $procedureNo = $rctData[ProcedureNo];
    }
    
     if(!checkSponges($encNr,$procedureNo)){
        addSpongeItems($encNr,$procedureType,$procedureNo);
    }
    
    if (count($rctData) > 0) {
        //$encNr = $rctData[encounter_nr];
        updateAnesthesiaSponges($rctData);
    }
    
    $sql="Select ID,Encounter_Nr,ProcedureNo,ProcedureType,Counts,Start,Additions,Close from care_ke_anestheatresponge
             where encounter_nr=$encNr and procedureNo='$procedureNo'";
    if($debug) { 
        echo $sql; 
    }
    
    $result=$db->Execute($sql);
    $total=$result->RecordCount();
    echo '{
    "total":"' . $total . '","spongecounts":[';
    $counter=0;
    while($row=$result->FetchRow()){
        echo '{"ID":"' . $row[ID] .'","Encounter_Nr":"' . $row[Encounter_Nr] .'","ProcedureNo":"' . $row[ProcedureNo].'","ProcedureType":"' . $row[ProcedureType]
                .'","Counts":"' . $row[Counts] .'","Start":"' . $row[Start] .'","Additions":"' . $row[Additions].'","Close":"' . $row[Close] .'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
     echo ']}';
}

function updateAnesthesiaSponges($anesData) {
    global $db;
    $debug = false;
    $sql = 'UPDATE care_ke_anestheatresponge SET ';
    foreach ($anesData as $key => $value) {
        $sql .="`$key`='$value' , ";
    }
    $sql = substr($sql, 0, -2) . ' WHERE ID="' . $anesData['ID'] . '"';

    if ($debug)
        echo $sql;

    if($db->Execute($sql)){
        echo "{success:true}";
    }else{
        echo "{failure:true}";
    }
}

function addSpongeItems($encNr,$procedureType,$procedureNo){
    global $db;
    $debug=false;
    
    $sql="select spongename from care_ke_anesspongetypes";
     if($debug) { echo $sql; }
     
    $request=$db->Execute($sql);
   
    while($row=$request->FetchRow()){
        $sql2="Insert into care_ke_anestheatresponge(encounter_nr,ProcedureNo,ProcedureType,Counts,start,additions,close)
                values ('$encNr','$procedureNo','$procedureType','$row[spongename]','','','')";
        if($debug) { echo $sql2; }

        $db->Execute($sql2);
    }
    
          
}


function getPatientSafety($encNr) {
    global $db;
    $debug = false;

    $sql = "select `id`,`Encounter_nr`, `Anes_Machine`,`Anes_Machine2`,`Airway_IV`,`Safety_Belt`,`Arms_90`,
                `Pressure_Points`,`Eye_Case`,`Patient_Post`,`Armboard_Restraints`,`Arms_Tucked`,`Axillary_Roll`,`Ointment`,
                `Taped`,`Patient_Post1` from `care_ke_anespatientsafety` where encounter_nr=$encNr";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{"total":"' . $total . '","patientsafety":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        echo '{"id":"' . $row[id] . '","Encounter_nr":"' . $row[Encounter_nr] . '","pat_Anes_Machine":"' . $row[Anes_Machine]
                . '","pat_Anes_Machine2":"' . $row[Anes_Machine2]. '","pat_Airway_IV":"' . $row[Airway_IV]. '","pat_Safety_Belt":"' . $row[Safety_Belt]
                . '","pat_Arms_90":"' . $row[Arms_90]. '","pat_Pressure_Points":"' . $row[Pressure_Points]. '","pat_Eye_Case":"' . $row[Eye_Case]
                . '","pat_Patient_Post":"' . $row[Patient_Post]. '","pat_Armboard_Restraints":"' . $row[Armboard_Restraints]. '","pat_Arms_Tucked":"' . $row[Arms_Tucked]
                . '","pat_Axillary_Roll":"' . $row[Axillary_Roll]. '","pat_Axillary_Roll":"' . $row[Axillary_Roll]. '","pat_Axillary_Roll":"' . $row[Axillary_Roll]
                . '","pat_Ointment":"' . $row[Ointment]. '","pat_Taped":"' . $row[Taped]. '","pat_Patient_Post1":"' . $row[Patient_Post1]. '"}';

        $counter++;

        if ($counter < $total) {
            echo ",";
        }
    }
    echo ']}';
}


function checkAnesthesiaChart($encNr) {
    global $db;

    $debug = false;

    $sql = "Select count(encounter_nr) as encNr from care_ke_anesthesia2 where encounter_nr =$encNr";
    if ($debug)
        echo $sql;
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    if ($row[0] < 1) {
        $sql2 = "select field_group,field_items from care_ke_anesthesia_chart_fields";
        if ($debug)
            echo $sql2;
        $result2 = $db->Execute($sql2);
        while ($row2 = $result2->FetchRow()) {
            $sql3 = "Insert into care_ke_anesthesia2(encounter_nr,item_description,item_group)
                    values ('$encNr','$row2[field_items]','$row2[field_group]')";
            if ($debug)
                echo $sql3;
            $db->Execute($sql3);
        }
    }
}

function updateAnesthesiaCharts($anesData) {
    global $db;
    $debug = false;
    $sql = 'UPDATE care_ke_anesthesia2 SET ';
    foreach ($anesData as $key => $value) {
        $sql .="`$key`='$value' , ";
    }
    $sql = substr($sql, 0, -2) . ' WHERE ID="' . $anesData['ID'] . '"';

    if ($debug)
        echo $sql;

    $db->Execute($sql);
}

function getAnesthesiaCharts($encNr) {
    global $db;
    $debug = false;
    $json = $_POST['editedData'];
    $rctData = json_decode($json, TRUE);
    //echo $rctData[encounter_nr];

    if ($encNr == '') {
        $encNr = $rctData[encounter_nr];
    }
    if (count($rctData) > 0) {
        $encNr = $rctData[encounter_nr];
        updateAnesthesiaCharts($rctData);
    }


    $groupItem = $_REQUEST[groupField];

    checkAnesthesiaChart($encNr);

    $psql = "SELECT `ID`,`encounter_nr`,`item_description`,`1`,`2`,`3`,`4`,
            `5`,`6`,`7`, `8`,`9`,`10`,`11`,`12`,`13`,`14`,`15`,`16`,`17`,`18`,
            `19`,`20`,`21`,`22`,`23`,`24` FROM `care_ke_anesthesia2` 
            where item_group='$groupItem'";

    if ($encNr <> '') {
        $psql = $psql . " and encounter_nr=$encNr";
    }

    if ($debug)
        echo $psql;

    $request = $db->Execute($psql);
    $total = $request->RecordCount();



    echo '{
    "total":"' . $total . '","chartsItems":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[ID] . '","encounter_nr":"' . $row[encounter_nr] . '","item_description":"' . $row[item_description]
        . '","group":"' . $row[group] . '","1":"' . $row[1] . '","2":"' . $row[2]
        . '","3":"' . $row[3] . '","4":"' . $row[4] . '","5":"' . $row[5]
        . '","6":"' . $row[6] . '","7":"' . $row[7] . '","8":"' . $row[8] . '","9":"' . $row[9]
        . '","10":"' . $row[10] . '","11":"' . $row[11] . '","12":"' . $row[12] . '"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function validateCheckList($encounterNo, $form_type) {
    global $db;
    $debug = false;

    $psql = "select encounter_nr from care_ke_surgicalchecklist where encounter_nr='$encounterNo' and form_type='$form_type'";

    if ($debug)
        echo $psql;
    if ($presult = $db->Execute($psql)) {
        $row = $presult->FetchRow();
        if ($row[0] <> "") {
            return '1';
        } else {
            return '0';
        }
    } else {
        return '0';
    }
}

function checkAnesthesiaExists($strTable, $encNr) {
    global $db;
    $debug = false;

    $sql = "Select count(encounter_nr) as enCount from $strTable where encounter_nr=$encNr";

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);
    $row = $request->FetchRow();
    if ($row[0] > 0) {
        return true;
    } else {
        return false;
    }
}

function CreateAnesthesiaForm($AnesthesiaDetails) {
    global $db;
    global $db;
    $debug = FALSE;
    $name = $_SESSION['sess_login_username'];


    $pjdate = new DateTime($AnesthesiaDetails[procedure_date]);
    $AnesthesiaDetails[procedure_date] = $pjdate->format('Y-m-d');
    $AnesthesiaDetails[input_user] = $name;
    $AnesthesiaDetails[create_time] = date('H:i:s');
    $AnesthesiaDetails[pat_Encounter_nr] = $AnesthesiaDetails[encounter_nr];
    $AnesthesiaDetails[air_Encounter_nr] = $AnesthesiaDetails[encounter_nr];
    $AnesthesiaDetails[ane_Encounter_nr] = $AnesthesiaDetails[encounter_nr];
    $AnesthesiaDetails[mon_Encounter_nr] = $AnesthesiaDetails[encounter_nr];
    $AnesthesiaDetails[rem_Encounter_nr] = $AnesthesiaDetails[encounter_nr];

    $AnesthesiaDetails[history] = "$name:Executed-Create new Checlist Sign_In";



    foreach ($AnesthesiaDetails as $key => $value) {
        $items = array("air_", "pat_", "ane_", "mon_", "rem_");
        $strField = substr($key, 0, 4);
        if (!in_array($strField, $items)) {
            $FieldNames.=$key . ', ';
            $FieldValues.='"' . $value . '", ';
        } else {
            if ($strField == "air_") {
                $FieldNames2.=substr($key, 4) . ', ';
                $FieldValues2.='"' . $value . '", ';
            } else if ($strField == "pat_") {
                $FieldNames3.=substr($key, 4) . ', ';
                $FieldValues3.='"' . $value . '", ';
            } else if ($strField == "ane_") {
                $FieldNames4.=substr($key, 4) . ', ';
                $FieldValues4.='"' . $value . '", ';
            } else if ($strField == "mon_") {
                $FieldNames5.='`' . substr($key, 4) . '`, ';
                $FieldValues5.='"' . $value . '", ';
            } else if ($strField == "rem_") {
                $FieldNames6.='`' . substr($key, 4) . '`, ';
                $FieldValues6.='"' . $value . '", ';
            }
        }
    }

    //unset($checkDetails['formStatus']);

    if (checkAnesthesiaExists('care_ke_anesthesia', $AnesthesiaDetails[encounter_nr])) {

        $sql = 'UPDATE care_ke_anesthesia SET ';
        foreach ($AnesthesiaDetails as $key => $value) {
            $items = array("air_", "pat_", "ane_", "mon_", "rem_");
            $strField = substr($key, 0, 4);
            if (!in_array($strField, $items)) {
                $sql .= $key . '="' . $value . '", ';
            }
        }
        $sql = substr($sql, 0, -2) . ' WHERE encounter_nr="' . $AnesthesiaDetails[encounter_nr] . '"';
    } else {
        $sql = 'INSERT INTO care_ke_anesthesia (' . substr($FieldNames, 0, -2) . ') ' .
                'VALUES (' . substr($FieldValues, 0, -2) . ') ';
    }
    if ($debug)
        echo $sql;

    if ($db->Execute($sql)) {
        $items = array("air_", "pat_", "ane_", "mon_", "rem_");
        $strField = substr($key, 0, 4);

        if (checkAnesthesiaExists('care_ke_anesairwaymanage', $AnesthesiaDetails[air_Encounter_nr])) {
            $sql2 = 'UPDATE care_ke_anesairwaymanage SET ';
            foreach ($AnesthesiaDetails as $key => $value) {
                if ($strField == "air_") {
                    $sql2 .= $key . '="' . $value . '", ';
                }
            }
            $sql2 = substr($sql2, 0, -2) . ' WHERE encounter_nr="' . $AnesthesiaDetails[air_Encounter_nr] . '"';
        } else {
            $sql2 = 'INSERT INTO care_ke_anesairwaymanage (' . substr($FieldNames2, 0, -2) . ') ' .
                    'VALUES (' . substr($FieldValues2, 0, -2) . ') ';
        }

        if ($debug) {
            echo $sql2;
        }
        if ($db->Execute($sql2)) {
            $results = "{success:true}";
        } else {
            $results = "{failure: true,errNo:'2'}"; // Return the error message(s)
        }

        if (checkAnesthesiaExists('care_ke_anespatientsafety', $AnesthesiaDetails[air_Encounter_nr])) {
            $sql3 = 'UPDATE care_ke_anespatientsafety SET ';
            foreach ($AnesthesiaDetails as $key => $value) {
                $items = array("air_", "pat_", "ane_", "mon_", "rem_");
                $strField = substr($key, 0, 4);
                if ($strField == "pat_") {
                    $sql3 .= substr($key, 4) . '="' . $value . '", ';
                }
            }
            $sql3 = substr($sql3, 0, -2) . ' WHERE encounter_nr="' . $AnesthesiaDetails[air_Encounter_nr] . '"';
        } else {
            $sql3 = 'INSERT INTO care_ke_anespatientsafety (' . substr($FieldNames3, 0, -2) . ') ' .
                    'VALUES (' . substr($FieldValues3, 0, -2) . ') ';
        }

        if ($debug) {
            echo $sql3;
        }

        if ($db->Execute($sql3)) {
            $results = "{success:true}";
        } else {
            $results = "{failure: true,errNo:'3'}"; // Return the error message(s)
        }

        if (checkAnesthesiaExists('care_ke_anestechnique', $AnesthesiaDetails[ane_Encounter_nr])) {
            $sql4 = 'UPDATE care_ke_anestechnique SET ';
            foreach ($AnesthesiaDetails as $key => $value) {
                $items = array("air_", "pat_", "ane_", "mon_", "rem_");
                $strField = substr($key, 0, 4);
                if ($strField == "ane_") {
                    $sql4 .= substr($key, 4) . '="' . $value . '", ';
                }
            }
            $sql4 = substr($sql4, 0, -2) . ' WHERE encounter_nr="' . $AnesthesiaDetails[air_Encounter_nr] . '"';
        } else {
            $sql4 = 'INSERT INTO care_ke_anestechnique (' . substr($FieldNames4, 0, -2) . ') ' .
                    'VALUES (' . substr($FieldValues4, 0, -2) . ') ';
        }
        if ($debug) {
            echo $sql4;
        }

        if ($db->Execute($sql4)) {
            $results = "{success:true}";
        } else {
            $results = "{failure: true,errNo:'4'}"; // Return the error message(s)
        }


        if (checkAnesthesiaExists('care_ke_anesmonitors', $AnesthesiaDetails[ane_Encounter_nr])) {
            $sql5 = 'UPDATE care_ke_anesmonitors SET ';
            foreach ($AnesthesiaDetails as $key => $value) {
                $items = array("air_", "pat_", "ane_", "mon_", "rem_");
                $strField = substr($key, 0, 4);
                if ($strField == "mon_") {
                    $sql5 .= '`' . substr($key, 4) . '`="' . $value . '", ';
                }
            }
            $sql5 = substr($sql5, 0, -2) . ' WHERE encounter_nr="' . $AnesthesiaDetails[air_Encounter_nr] . '"';
        } else {
            $sql5 = 'INSERT INTO care_ke_anesmonitors (' . substr($FieldNames5, 0, -2) . ') ' .
                    'VALUES (' . substr($FieldValues5, 0, -2) . ') ';
        }
        if ($debug) {
            echo $sql5;
        }
        if ($db->Execute($sql5)) {
            $results = "{success:true}";
        } else {
            $results = "{failure: true,errNo:'5'}"; // Return the error message(s)
        }


        if (checkAnesthesiaExists('care_ke_anesthesiaRemarks', $AnesthesiaDetails[rem_Encounter_nr])) {
            $sql6 = 'UPDATE care_ke_anesthesiaRemarks SET ';
            foreach ($AnesthesiaDetails as $key => $value) {
                $items = array("air_", "pat_", "ane_", "mon_", "rem_");
                $strField = substr($key, 0, 4);
                if ($strField == "rem_") {
                    $sql6 .= '`' . substr($key, 4) . '`="' . $value . '", ';
                }
            }
            $sql6 = substr($sql6, 0, -2) . ' WHERE encounter_nr="' . $AnesthesiaDetails[rem_Encounter_nr] . '"';
        } else {
            $sql6 = 'INSERT INTO care_ke_anesthesiaRemarks (' . substr($FieldNames6, 0, -2) . ') ' .
                    'VALUES (' . substr($FieldValues6, 0, -2) . ') ';
        }
        if ($debug) {
            echo $sql6;
        }
        if ($db->Execute($sql6)) {
            $results = "{success:true}";
        } else {
            $results = "{failure: true,errNo:'6'}"; // Return the error message(s)
        }
    } else {
        $results = "{failure: true,errNo:'0'}"; // Return the error message(s)
    }


    echo $results;
}

function CreateCheckList1($checkDetails) {
    global $db;
    global $db;
    $debug = false;
    $name = $_SESSION['sess_login_username'];

    $checkDetails[form_type] = 'SIGN_IN';
    $pjdate = new DateTime($checkDetails[procedure_date2]);
    $checkDetails[procedure_date] = $pjdate->format('Y-m-d');
    $checkDetails[input_user] = $name;
    $checkDetails[create_time] = date('H:i:s');
    $checkDetails[pid] = $checkDetails[pid2];
    $checkDetails[encounter_nr] = $checkDetails[encounter_nr];

    unset($checkDetails[procedure_date2]);
    unset($checkDetails[pid2]);
    unset($checkDetails[pname]);
//    $checkDetails[create_date] = date('Y:m:d');
    $checkDetails[history] = "$name:Executed-Create new Checlist Sign_In";

    if (validateCheckList($checkDetails[encounter_nr], $checkDetails[form_type]) == '1') {
        $results = "{Faulure:true,errNo:'SIGN_IN Checklist has already been entered'}";
    } else {
        foreach ($checkDetails as $key => $value) {
            $FieldNames.=$key . ', ';
            $FieldValues.='"' . $value . '", ';
        }

//unset($checkDetails['formStatus']);

        $sql = 'INSERT INTO care_ke_surgicalchecklist (' . substr($FieldNames, 0, -2) . ') ' .
                'VALUES (' . substr($FieldValues, 0, -2) . ') ';
        if ($debug)
            echo $sql;
        if ($db->Execute($sql)) {
            $results = "{success: true}"; // Return the error message(s)
        } else {
            $results = "{success: false,errNo:2}"; // Return the error message(s)
        }
    }

    echo $results;
}

function CreateCheckList2($checkDetails) {
    global $db;
    global $db;
    $debug = false;
    $name = $_SESSION['sess_login_username'];
// $checkDetails[notes] = htmlspecialchars($checkDetails[notes]);
//$checkDetails[history] = "$name Execute:Create new Booking;";

    $checkDetails[form_type] = 'TIME_OUT';
    $pjdate = new DateTime($checkDetails[procedure_date3]);
    $checkDetails[procedure_date] = $pjdate->format('Y-m-d');
    $checkDetails[input_user] = $name;
    $checkDetails[create_time] = date('H:i:s');
    $checkDetails[pid] = $checkDetails[pid3];
    $checkDetails[procedure_name] = $checkDetails[procedure_name3];
    $checkDetails[encounter_nr] = $checkDetails[encounter_nr2];

    unset($checkDetails[procedure_date3]);
    unset($checkDetails[pid3]);
    unset($checkDetails[pname3]);
    unset($checkDetails[procedure_name3]);
    unset($checkDetails[encounter_nr2]);
//    $checkDetails[create_date] = date('Y:m:d');
    $checkDetails[history] = "$name:Executed-Create new Checlist Time_OUT";

    if (validateCheckList($checkDetails[encounter_nr], $checkDetails[form_type]) == '1') {
        $results = "{Faulure:true,errNo:'TIME_OUT Checklist has already been entered'}";
    } else {

        foreach ($checkDetails as $key => $value) {
            $FieldNames.=$key . ', ';
            $FieldValues.='"' . $value . '", ';
        }

//unset($checkDetails['formStatus']);

        $sql = 'INSERT INTO care_ke_surgicalchecklist (' . substr($FieldNames, 0, -2) . ') ' .
                'VALUES (' . substr($FieldValues, 0, -2) . ') ';
        if ($debug)
            echo $sql;
        if ($db->Execute($sql)) {
            $results = "{success: true}"; // Return the error message(s)
        } else {
            $results = "{success: false,errNo:2}"; // Return the error message(s)
        }
    }

    echo $results;
}

function CreateCheckList3($checkDetails) {
    global $db;
    global $db;
    $debug = false;
    $name = $_SESSION['sess_login_username'];
// $checkDetails[notes] = htmlspecialchars($checkDetails[notes]);
//$checkDetails[history] = "$name Execute:Create new Booking;";

    $checkDetails[form_type] = 'SIGN_OUT';
    $pjdate = new DateTime($checkDetails[procedure_date4]);
    $checkDetails[procedure_date] = $pjdate->format('Y-m-d');
    $checkDetails[input_user] = $name;
    $checkDetails[create_time] = date('H:i:s');
    $checkDetails[pid] = $checkDetails[pid4];
    $checkDetails[procedure_name] = $checkDetails[procedure_name4];
    $checkDetails[encounter_nr] = $checkDetails[encounter_nr4];

    unset($checkDetails[procedure_date4]);
    unset($checkDetails[pid4]);
    unset($checkDetails[pname4]);
    unset($checkDetails[procedure_name4]);
    unset($checkDetails[encounter_nr4]);
//    $checkDetails[create_date] = date('Y:m:d');
    $checkDetails[history] = "$name:Executed-Create new Checlist SIGN_OUT";


    if (validateCheckList($checkDetails[encounter_nr], $checkDetails[form_type]) == '1') {
        $results = "{Faulure:true,errNo:'SIGN_OUT Checklist has already been entered'}";
    } else {

        foreach ($checkDetails as $key => $value) {
            $FieldNames.=$key . ', ';
            $FieldValues.='"' . $value . '", ';
        }

//unset($checkDetails['formStatus']);

        $sql = 'INSERT INTO care_ke_surgicalchecklist (' . substr($FieldNames, 0, -2) . ') ' .
                'VALUES (' . substr($FieldValues, 0, -2) . ') ';
        if ($debug)
            echo $sql;
        if ($db->Execute($sql)) {
            $results = "{success: true}"; // Return the error message(s)
        } else {
            $results = "{success: false,errNo:2}"; // Return the error message(s)
        }
    }

    echo $results;
}

function getStatusList() {
    global $db;
    $debug = false;

    $sql = "select value from care_config_global where type='procStatus'";
    if ($debug)
        echo $sql;

    $result = $db->Execute($sql);
    $row = $result->FetchRow();

    $str = explode(",", $row[0]);
    $total = count($str);
    $str1 = '{"procStatus":[';
    for ($i = 0; $i <= $total; $i++) {
        $str1.='{"sname":"' . $str[$i] . '"}';

        if ($i < $total)
            $str1.= ',';
    }
    $str1.= "]}";

    echo $str1;
}

function validateClass($ID) {
    global $db;
    $debug = true;

    $sql = "SELECT count(id) from care_ke_procedureclasses where ID='$ID'";

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);
    $row = $request->FetchRow();

    if ($row[0] == 1) {
        return '1';
    } else {
        return '0';
    }
}

function writeprocedureclass() {
    global $db;
    $debug = true;

    $procs = $_REQUEST['data'];
    $procData = json_decode($procs, TRUE);

    foreach ($procData as $value) {
        $ID = $value['ID'];
        $proc_class = $value['proc_class'];
        $class_value = $value['class_value'];
        $cost = $value['cost'];

        echo "returned $ID - $proc_class -  $class_value -  $cost";
        if (validateClass($ID) == '1') {
            $sql = "UPDATE care_ke_procedureclasses set proc_class='$proc_class', class_value='$class_value',cost='$cost'";
        } else {
            $sql = "INSERT INTO care_ke_procedureclasses(ID,proc_class,class_value,cost)
Values('$ID','$proc_class','$class_value','$cost')";
        }
        if ($debug)
            echo $sql . '<br>';

        if ($db->Execute($sql)) {
            echo '{success:true,errNo:"Successfully saved data"}';
        } else {
            echo '{failure:true,errNo:"Data could not be saved-' . $sql . '"}';
        }
    }
}

function getProcedureClassList() {
    global $db;
    $debug = false;

    $sql = "select ID,proc_class,class_value,cost from care_ke_procedureclasses";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","procedureClassLists":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        echo '{"proc_class":"' . $row[proc_class] . '","class_value":"' . $row[class_value] . '","cost":"' . $row[cost] . '","ID":"' . $row[ID] . '"}';

        $counter++;

        if ($counter < $total) {
            echo ",";
        }
    }
    echo ']}';
}

function getSalesItems($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT  d.partcode,d.`item_description`,d.`unit_price`,l.`quantity`,l.`loccode` FROM care_tz_drugsandservices d 
INNER JOIN care_ke_locstock l
ON d.`partcode`=l.`stockid` WHERE d.`purchasing_class` LIKE 'drug%' AND l.`loccode`='main'";

    if ($start <> '' && $limit <> '')
        $sql.=" limit $start,$limit";

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "SELECT  COUNT(d.partcode) AS partcode FROM care_tz_drugsandservices d 
INNER JOIN care_ke_locstock l
ON d.`partcode`=l.`stockid` WHERE d.`purchasing_class` LIKE 'drug%' AND l.`loccode`='main'";

    $request2 = $db->Execute($sqlTotal);

    $row = $request2->FetchRow();
    $total = $row[0];

    echo '{
"total":"' . $total . '","salesItems":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        if ($row[unit_price] <> '') {
            $price = $row[unit_price];
        } else {
            $price = 0;
        }

        if ($row[quantity] <> '') {
            $qty = $row[quantity];
        } else {
            $qty = 0;
        }
        echo '{"itemcode":"' . $row[partcode] . '","description":"' . $row[item_description] . '","qty":' . $qty
        . ',"loccode":"' . $row[loccode] . '","price":' . $price . '}';



        if ($counter < $total) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
//    echo "<br><br><br>".$counter;
}

function getItemsList($searchParam, $start, $limit) {
    global $db;
    $debug = false;
    $category = $_REQUEST[category];

    $sql = "SELECT partcode,item_description,item_full_description,`unit_measure`,unit_price,unit_price_1,selling_price,
purchasing_class, c.`item_Cat` AS category,item_status,`reorder`,`minimum`,maximum 
FROM care_tz_drugsandservices d LEFT JOIN  care_tz_itemscat c
ON d.`category`=c.`catID` WHERE purchasing_class='PROCEDURES'";

    if (isset($start) && isset($limit)) {
        $sql.=" limit $start,$limit";
    }

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(partcode) as counts from care_tz_drugsandservices where purchasing_class LIKE 'drug%'";

    $request2 = $db->Execute($sqlTotal);

    $row = $request2->FetchRow();
    $total = $row[0];

    echo '{
"total":"' . $total . '","itemsList":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"partcode":"' . $row[partcode] . '","item_description":"' . $row[item_description] . '","item_full_description":"' . $row[item_full_description]
        . '","unit_measure":"' . $row[unit_measure] . '","unit_price":"' . $row[unit_price] . '","selling_price":"' . $row[selling_price]
        . '","purchasing_class":"' . $row[purchasing_class] . '","category":"' . $row[category]
        . '","item_status":"' . $row[item_status] . '","reorder":"' . $row[reorder] . '","minimum":"' . $row[minimum] . '","maximum":"' . $row[maximum] . '"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
//    echo "<br><br><br>".$counter;
}

function getUnitsofMeasure($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select id,name from care_unit_measurement";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);


    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","unitsMeasure":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[id] . '","name":"' . $row[name] . '"}';


        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function getStaff($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select ID,staff_name from care_ke_staff";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","staff":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[ID] . '","staff_name":"' . $row[staff_name] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getTheatreRooms($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select room_no,room_name from care_ke_TheatreRooms";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","theatreRooms":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"room_no":"' . $row[room_no] . '","room_name":"' . $row[room_name] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getTheatreList($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT BookingNo,`procedure_date`,`surgeon`,`asst_surgeon`,`pid`, `encounter_nr`, `pnames`,
                `diagnosis`,`procedure_type`,`procedure_class`,`class_code`, `op_starttime`,`op_endtime`,
                `scrub_nurse`,`op_room`,`status`,`notes`,`formStatus`,`selian_pid`,`sex`,`allergies`,`date_birth`
            FROM `care_ke_procedures`";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","theatrelist":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        $notes = html_entity_decode($row[notes]);
        $notes2 = html_entity_decode($notes);
        echo '{"BookingNo":"' . $row[BookingNo] . '","pid":"' . $row[pid] . '","selian_pid":"' . $row[selian_pid] . '","procedure_date":"' . $row[procedure_date]
        . '","surgeon":"' . $row[surgeon] . '","asst_surgeon":"' . $row[asst_surgeon] . '","encounter_nr":"' . $row[encounter_nr]
        . '","pnames":"' . $row[pnames] . '","diagnosis":"' . $row[diagnosis] . '","procedure_type":"' . $row[procedure_type]
        . '","procedure_class":"' . $row[procedure_class] . '","class_code":"' . $row[class_code] . '","op_starttime":"' . $row[op_starttime]
        . '","op_endtime":"' . $row[op_endtime] . '","scrub_nurse":"' . $row[scrub_nurse] . '","op_room":"' . $row[op_room]
        . '","notes":"' . str_replace('"', "'", html_entity_decode($notes)) . '","formStatus":"' . $row[formStatus] . '","sex":"' . $row[sex]
        . '","allergies":"' . $row[allergies] . '","date_birth":"' . $row[date_birth] . '","status":"' . $row[status] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getDiagnosisList($icddesc) {
    global $db;
    $debug = false;

    $sql = "SELECT diagnosis_code,description FROM care_icd10_en";

    if ($icddesc) {
        $sql.=" where description like '%$icddesc%'";
    }

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","diagnosisList":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"diagnosis_code":"' . $row[diagnosis_code] . '","description":"' . trim($row[description]) . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getPatientDetails($pid) {
    global $db;
    $debug = false;

    $sql = "SELECT p.`pid`,e.`encounter_nr`,selian_pid,CONCAT(name_first,' ',name_last,' ',name_2) AS pnames ,date_birth,sex FROM care_person p 
LEFT JOIN care_encounter e ON p.`pid`=e.`pid`
WHERE p.pid=$pid";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","patientInfo":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        if ($row[sex] == 'f') {
            $sex = 'FEMALE';
        } else if ($row[sex] == 'm') {
            $sex = 'MALE';
        }

        echo '{"pid":"' . $row[pid] . '","encounter_nr":"' . $row[encounter_nr] . '","selian_pid":"' . $row[selian_pid] . '","pnames":"' . $row[pnames] . '","date_birth":"' . $row[date_birth]
        . '","sex":"' . $sex . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getProcedures($searchParam, $start, $limit) {
    global $db;
    $debug = false;

    $sql = "select partcode,item_description from care_tz_drugsandservices where purchasing_class='THEATRE' 
and item_description like '%$searchParam%'";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","procedureNames":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"partcode":"' . $row[partcode] . '","item_description":"' . $row[item_description] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getProcedureClass($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select DISTINCT proc_class from care_ke_procedureclasses";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","procedureClass":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[proc_class] . '","proc_class":"' . $row[proc_class] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getSurgicalCheckLists($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT `ID`,`form_type`,`pid`,`encounter_nr`,`procedure_date`,`Time`,`Procedure_name`,d.`item_description`,`identity`,`site`,`procedure_check`,`consent`,`age`,
`ID_bracelet`,`site_marked`,`bathed`,`scrubbed`, `allergy`,`allergy_name`,`blood_available`,`xray`,`HB`,`HCT`,`weight`,`pre_anaesthesia_eva`,
`solids_from`,`liquids_from`,`breastfeeding_from`,`Medication_rs`,`antibiotic_check`,`antibiotic_given`,`BP`,`HR`,
`RR`,`temp`,`O2_sat`,`other_vitals`,`PT_Voided`,`removal_extras`, `members_confirm`, `new_member`,`antibiotic_prophy_60`,
`antibiotic_reason`, `surgeon_reviews`,`anaesthesia_review`,`nursing_review`,`instruments`,`throat_pack`, `speciment_label`,
`equipment_problems`,`final_review`,`checklist_user`
FROM
`care_ke_surgicalchecklist` c  LEFT JOIN care_tz_drugsandservices d ON c.`Procedure_name`=d.`partcode`
";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","surgChecklists":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"form_type":"' . $row[form_type] . '","pid":"' . $row[pid] . '","encounter_nr":"' . $row[encounter_nr]
        . '","procedure_date":"' . $row[procedure_date] . '","procedure_ID":"' . $row[Procedure_name] . '","procedure_name":"' . $row[item_description]
        . '","identity":"' . $row[identity] . '","site":"' . $row[site] . '","checklist_user":"' . $row[checklist_user]
        . '","procedure_check":"' . $row[procedure_check] . '","consent":"' . $row[consent] . '","age":"' . $row[age]
        . '","ID_bracelet":"' . $row[ID_bracelet] . '","site_marked":"' . $row[site_marked] . '","bathed":"' . $row[bathed]
        . '","scrubbed":"' . $row[scrubbed] . '","allergy":"' . $row[allergy] . '","allergy_name":"' . $row[allergy_name]
        . '","blood_available":"' . $row[blood_available] . '","xray":"' . $row[xray] . '","HB":"' . $row[HB]
        . '","HCT":"' . $row[HCT] . '","weight":"' . $row[weight] . '","pre_anaesthesia_eva":"' . $row[pre_anaesthesia_eva]
        . '","solids_from":"' . $row[solids_from] . '","liquids_from":"' . $row[liquids_from] . '","breastfeeding_from":"' . $row[breastfeeding_from]
        . '","Medication_rs":"' . $row[Medication_rs] . '","antibiotic_check":"' . $row[antibiotic_check] . '","antibiotic_given":"' . $row[antibiotic_given]
        . '","BP":"' . $row[BP] . '","HR":"' . $row[HR] . '","RR":"' . $row[RR]
        . '","temp":"' . $row[temp] . '","O2_sat":"' . $row[O2_sat] . '","other_vitals":"' . $row[other_vitals]
        . '","PT_Voided":"' . $row[PT_Voided] . '","removal_extras":"' . $row[removal_extras] . '","members_confirm":"' . $row[members_confirm]
        . '","new_member":"' . $row[new_member] . '","antibiotic_prophy_60":"' . $row[antibiotic_prophy_60] . '","antibiotic_reason":"' . $row[antibiotic_reason]
        . '","surgeon_reviews":"' . $row[surgeon_reviews] . '","anaesthesia_review":"' . $row[anaesthesia_review] . '","nursing_review":"' . $row[nursing_review]
        . '","instruments":"' . $row[instruments] . '","throat_pack":"' . $row[throat_pack] . '","speciment_label":"' . $row[speciment_label]
        . '","equipment_problems":"' . $row[equipment_problems] . '","final_review":"' . $row[final_review] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getClassCodes($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select distinct class_value from care_ke_procedureclasses";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
"total":"' . $total . '","classCodes":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[class_value] . '","class_value":"' . $row[class_value] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getItemsCategory($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select ID,catName from care_tz_categories order by catName asc";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(ID) as ccount from care_tz_categories";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
"total":"' . $total . '","itemsCategory":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[ID] . '","catName":"' . $row[catName] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getItemsSubCategory($start, $limit) {
    global $db;
    $debug = false;
    $category = $request[query];

    $sql = "select catID,item_cat from care_tz_itemscat ";

//    if(isset($category)){
//        $sql.=" where item_cat= like '$category%'";
//    }

    $sql.=" order by item_cat asc";

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(catID) as ccount from care_tz_itemscat";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
"total":"' . $total . '","itemsSubCategory":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"catID":"' . $row[catID] . '","item_cat":"' . trim($row[item_cat]) . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getStoreLocations($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT st_id,st_name,store,mainStore FROM care_ke_stlocation";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(st_id) as ccount from  care_ke_stlocation";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
"total":"' . $total . '","storeLocations":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"st_id":"' . $row[st_id] . '","st_name":"' . $row[st_name] . '","store":"' . $row[store] . '","mainStore":"' . $row[mainStore] . '"}';


        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function getItemLocations($start, $limit) {
    global $db;
    $debug = false;
    $partcode = $_REQUEST[partcode];

    $sql = "SELECT stockid AS itemcode,loccode,quantity FROM care_ke_locstock where stockid='$partcode'";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(stockid) as ccount FROM care_ke_locstock where stockid='$partcode'";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
"total":"' . $total . '","itemLocations":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"stockid":"' . $row[itemcode] . '","loccode":"' . $row[loccode] . '","quantity":"' . $row[quantity] . '"}';

        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function checkStockid($partcode) {
    global $db;
    $psql = 'select count(item_number) as pcount from care_tz_drugsandservices where partcode="' . $partcode . '"';
    $presult = $db->Execute($psql);
    $row = $presult->FetchRow();
    return $row[0];
}

function validateBooking($encounterNo) {
    global $db;
    $debug = false;

    $psql = "select encounter_nr from care_ke_procedures where encounter_nr=$encounterNo";

    if ($debug)
        echo $psql;
    if ($presult = $db->Execute($psql)) {
        $row = $presult->FetchRow();
        if ($row[0] <> "") {
            return '1';
        } else {
            return '0';
        }
    } else {
        return '0';
    }
}

function updateBooking($bookingDetails) {
    global $db;
    $debug = false;


    $name = $_SESSION['sess_login_username'];
    $bookingDetails[notes] = htmlspecialchars($bookingDetails[notes]);
    $bookingDetails[history] = "$name Execute:Update new Booking;";

    $pjdate = new DateTime($bookingDetails[procedure_date]);
    $bookingDetails[procedure_date] = $pjdate->format('Y-m-d');



    $sql = 'UPDATE care_ke_Procedures SET ';
    foreach ($bookingDetails as $key => $value) {
        $sql .= $key . '="' . $value . '", ';
    }
    $sql = substr($sql, 0, -2) . ' WHERE BookingNo="' . $bookingDetails['BookingNo'] . '"';

    if ($debug)
        echo $sql;

    if ($db->Execute($sql)) {
        $results = '{success: true }';
    } else {
        $results = "{failure: true,errNo:'Could no Update booking record for patient:$bookingDetails[pnames], Please check your values'}"; // Return the error message(s)
    }



    echo $results;
}

function createBooking($bookingDetails) {
    global $db;
    $debug = false;

    if ($bookingDetails[encounter_nr] == '') {
        $results = "{failure:true,errNo:'No encounter has been created for the patient, <br> Records Department need to record the patients Visit in the system'}";
    } else {

        if (validateBooking($bookingDetails[encounter_nr]) == '1') {
            $results = "{failure:true,errNo:'The patient has been booked already'}";
        } else {
            $name = $_SESSION['sess_login_username'];
            $bookingDetails[notes] = htmlspecialchars($bookingDetails[notes]);
            $bookingDetails[history] = "$name Execute:Create new Booking;";

            $pjdate = new DateTime($bookingDetails[procedure_date]);
            $bookingDetails[procedure_date] = $pjdate->format('Y-m-d');


            unset($bookingDetails['formStatus']);
            unset($bookingDetails['BookingNo']);

            foreach ($bookingDetails as $key => $value) {
                $FieldNames.=$key . ', ';
                $FieldValues.='"' . $value . '", ';
            }

            $sql = 'INSERT INTO care_ke_Procedures (' . substr($FieldNames, 0, -2) . ') ' .
                    'VALUES (' . substr($FieldValues, 0, -2) . ') ';
            if ($debug)
                echo $sql;
            if ($db->Execute($sql)) {
                $results = "{success: true}"; // Return the error message(s)
            } else {
                $results = "{success: false,errNo:'2'}}"; // Return the error message(s)
            }
        }
    }
    echo $results;
}

function InsertItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure) {
    global $db;
    $debug = false;

    $stid = checkStockid($partcode);

    if ($stid > 0) {
        echo "{'failure':'true','errNo':'1','partcode':'$partcode'}";
    } else {
        $sql = 'insert into care_tz_drugsandservices(item_number, 
partcode, item_description, item_full_description, unit_price, purchasing_class,category,
reorder,minimum,maximum,unit_measure,selling_price,item_status)
values("' . $category . '", "' . $partcode . '", "' . $item_description . '", "' . $item_full_description . '", 
"' . $unit_price . '", "' . $purchasing_class . '","' . $category . '","' . $reorder . '","' . $minimum . '","' . $maximum . '","' . $unitMeasure
                . '","' . $sellingPrice . '","' . $itemStatus . '")';
        if ($debug)
            echo $sql;
        if ($db->Execute($sql)) {
            echo '{success:true}';
        } else {
            echo "{'failure':'true','partcode':'$partcode'}";
        }
    }
}

function updateItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure) {
    global $db;
    $debug = false;
    $sql = 'update care_tz_drugsandservices set item_number = "' . $partcode . '" , item_description = "' . $item_description . '" ,
item_full_description = "' . $item_full_description . '" ,unit_price = "' . $unit_price . '" ,
purchasing_class ="' . $purchasing_class . '",category="' . $category
            . '",reorder="' . $reorder . '",minimum="' . $minimum . '",maximum="' . $maximum . '",unit_measure="' . $unitMeasure
            . '",selling_price="' . $sellingPrice . '",item_status="' . $itemStatus . '" where partcode ="' . $partcode . '"';

    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
        echo '{success:true}';
    } else {
        echo "{'failure':'true','partcode':'$partcode'}";
    }
}

function transmitWeberp() {

    $billdata[stockid] = $partcode;
    $billdata[categoryid] = $catID;
    $billdata[description] = $item_description;
    $billdata[longdescription] = $item_full_description;
    $billdata[units] = $units;
    $billdata[mbflag] = 'B';
    $billdata[lastcurcostdate] = date('Y-m-d');
    $billdata[actualcost] = $unit_price;
    $billdata[lastcost] = $unit_price;

    if ($weberp_obj = new_weberp()) {
        if ($stid <> "") {
            $weberp_obj->modify_stock_item_in_webERP($billdata);
            $results = '{success: true }';
        } else {
            $weberp_obj->create_stock_item_in_webERP($billdata);
            $results = '{success: true }';
        }
    } else {

        $results = '{success: false }';
    }
}

function stockAdjust($item_number) {
    global $db;
    $item_number = $_REQUEST[item_number];
    $item_description = $_REQUEST[item_Description];
    $qty = $_REQUEST[quantity];
    $roorder = $_REQUEST[reorderlevel];
    $loccode = $_REQUEST[loccode];
    $adjDesc = $_REQUEST[comment];
    $name = $_SESSION['sess_login_username'];

    $sql = 'select quantity from care_ke_locstock where stockid="' . $item_number . '" and loccode="' . $loccode . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();

    $csql = "update care_ke_locstock set
quantity='$qty',
reorderlevel='$roorder',
comment='$adjDesc'
where stockid='$item_number' and loccode='$loccode'";

    $ksql = "insert into care_ke_adjustments
(item_number, prev_qty, new_qty, user, adjDate, adjTime, Reason)
values( '" . $item_number . "', '" . $row[0] . "', '" . $qty . "', '$name',
'" . date('Y-m-d') . "', '" . date('H:i:s') . "', '" . $adjDesc . "')";

    if ($db->Execute($ksql)) {
        $db->Execute($csql);
        $results = '{success: true }';
    } else {
// Errors. Set the json to return the fieldnames and the associated error messages
        $results = '{success: false, sql:' . $ksql . '}'; // Return the error message(s)
//        echo $sql;
    }

    echo $results;
}

function deleteItem() {
    global $db;
    $debug = false;
    $partcode = $_POST[partcode];
    $sql = "DELETE FROM care_ke_locstock where stockid='$partcode'";
    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
        $sql1 = "INSERT INTO `care2x`.`care_tz_drugsandservices_del`
(`item_number`,`partcode`, `is_pediatric`,`is_adult`, `is_other`,`is_consumable`,`is_labtest`,
`item_description`,`item_full_description`,`unit_price`,`unit_price_1`,`unit_price_2`,`unit_price_3`,
`purchasing_class`,`category`,`reorder`,`minimum`, `maximum`,`subCategory`,`unit_measure`, `selling_price`,`item_status`)
SELECT `item_number`,`partcode`,`is_pediatric`,`is_adult`,`is_other`,`is_consumable`, `is_labtest`, `item_description`,
`item_full_description`,`unit_price`,`unit_price_1`,`unit_price_2`,`unit_price_3`, `purchasing_class`, `category`,
`reorder`, `minimum`, `maximum`,`subCategory`,`unit_measure`, `selling_price`,`item_status`
FROM `care_tz_drugsandservices`  WHERE partcode='$partcode'";
        if ($debug)
            echo $sql1;
        if ($db->Execute($sql1)) {
            $query = 'DELETE FROM care_tz_drugsandservices WHERE partcode = "' . $partcode . '"';
            if ($debug)
                echo $query;
            if ($db->Execute($query)) { //returns number of rows deleted
                echo 'Item Successfully deleted from the database';
            } else {
                echo "Unable to delete item from table";
            }
        } else {
            echo "Unable to log deleted item";
        }
    } else {
        echo "unable to delete item in store locations,Consult System Admin";
    }
}
?>
