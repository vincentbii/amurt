/*
 * File: app/store/CustomerInfo.js
 *
 * This file was generated by Sencha Architect version 4.1.1.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.CustomerInfo', {
    extend: 'Ext.data.Store',
    alias: 'store.customerInfo',

    requires: [
        'MyApp.model.customerInfo',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'MyApp.model.customerInfo',
            storeId: 'CustomerInfo',
            proxy: {
                type: 'ajax',
                url: './data/getDataFunctions.php?task=getCustomerInfo',
                reader: {
                    type: 'json',
                    root: 'CustomerInfo'
                }
            }
        }, cfg)]);
    }
});