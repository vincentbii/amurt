<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require('./roots.php');
require($root_path.'include/inc_environment_global.php');
require_once($root_path . "include/inc_img_fx.php");
require_once($root_path . 'include/inc_date_format_functions.php');


define('LANG_FILE','billing.php');
define('NO_CHAIN',1);

require_once($root_path.'include/inc_front_chain_lang.php');

$accDB=$_SESSION['sess_accountingdb'];
$pharmLoc=$_SESSION['sess_pharmloc'];

if($pharmLoc<>"care2x"){
    $stockDb=$accDB.locstock;
}else{
    $stockDb="care_ke_locstock";
}
function WeeklyReport($year, $month){
	global $db;
	$sql = "SELECT MIN(prescribe_date) as min, MAX(prescribe_date) as max FROM care_encounter_prescription WHERE DATE_FORMAT(prescribe_date,'%Y') = '".$year."' AND DATE_FORMAT(prescribe_date,'%m') = '".$month."' GROUP BY WEEK(`prescribe_date`)";

	$result = $db->Execute($sql);$weeks = array();$weeksInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$weeks[$weeksInc] = array('max' => $row['max'], 'min' => $row['min']);
			$weeksInc++;
		}
	}
	$weeks = json_encode($weeks);
	return $weeks;
	mysql_close($db);
}

function DaysOfTheWeek($min, $max){
	global $db;
	$paf = allFirstTimeVisits1($min, $max, '1');
	$par = allFirstTimeVisits1($min, $max, '2');
	$sql = "SELECT DISTINCT prescribe_date FROM care_encounter_prescription WHERE prescribe_date BETWEEN '$min' and '$max' AND drug_class = 'dental'";
	$result = $db->Execute($sql);
	$Data = [];
	$DataInc = 0;
	while ($row = $result->FetchRow()) {
		$sqlExtractionf = "SELECT count(*) as extractionTotals FROM care_encounter_prescription WHERE prescribe_date = '".$row['prescribe_date']."' AND partcode IN ('DEN01', 'DEN02') AND encounter_nr IN ($paf)";
		$resultExtraction = $db->Execute($sqlExtractionf);
		$rowExtraction = $resultExtraction->FetchRow();

		$sqlExtractionr = "SELECT count(*) as extractionTotals FROM care_encounter_prescription WHERE prescribe_date = '".$row['prescribe_date']."' AND partcode IN ('DEN01', 'DEN02') AND encounter_nr IN ($par)";
		$resultExtractionr = $db->Execute($sqlExtractionr);
		$rowExtractionr = $resultExtractionr->FetchRow();

		$sqlPR = "SELECT count(*) as prTotals FROM care_encounter_prescription WHERE prescribe_date = '".$row['prescribe_date']."' AND partcode IN ('DEN091', 'DEN092', 'DEN093', 'DEN09', 'DEN08', 'DEN081', 'DEN082', 'DEN083', 'DEN07', 'DEN071', 'DEN072', 'DEN073') AND encounter_nr IN ($paf)";
		$resultPR = $db->Execute($sqlPR);
		$rowPR = $resultPR->FetchRow();

		$sqlPRR = "SELECT count(*) as prTotals FROM care_encounter_prescription WHERE prescribe_date = '".$row['prescribe_date']."' AND partcode IN ('DEN091', 'DEN092', 'DEN093', 'DEN09', 'DEN08', 'DEN081', 'DEN082', 'DEN083', 'DEN07', 'DEN071', 'DEN072', 'DEN073') AND encounter_nr IN ($par)";
		$resultPRR = $db->Execute($sqlPRR);
		$rowPRR = $resultPRR->FetchRow();

		$sqlOthers = "SELECT count(*) as OthersTotals FROM care_encounter_prescription WHERE drug_class = 'dental' AND prescribe_date = '".$row['prescribe_date']."' AND partcode NOT IN ('DEN091', 'DEN092', 'DEN093', 'DEN09', 'DEN08', 'DEN081', 'DEN082', 'DEN083', 'DEN07', 'DEN071', 'DEN072', 'DEN073','DEN01', 'DEN02') AND encounter_nr IN ($paf)";
		$resultOthers = $db->Execute($sqlOthers);
		$rowOthers = $resultOthers->FetchRow();

		$sqlOthersR = "SELECT count(*) as OthersRTotals FROM care_encounter_prescription WHERE drug_class = 'dental' AND prescribe_date = '".$row['prescribe_date']."' AND partcode NOT IN ('DEN091', 'DEN092', 'DEN093', 'DEN09', 'DEN08', 'DEN081', 'DEN082', 'DEN083', 'DEN07', 'DEN071', 'DEN072', 'DEN073','DEN01', 'DEN02') AND encounter_nr NOT IN ($par)";
		$resultOthersR = $db->Execute($sqlOthersR);
		$rowOthersR = $resultOthersR->FetchRow();
		
		$Data[$DataInc] = [
			'date' => $row['prescribe_date'],
			'extraction' => $rowExtraction['extractionTotals'],
			'extractionr' => $rowExtractionr['extractionTotals'],
			'PR' => $rowPR['prTotals'],
			'PRR' => $rowPRR['prTotals'],
			'Others' => $rowOthers['OthersTotals'],
			'OthersR' => $rowOthersR['OthersRTotals']

		];
		$DataInc++;
	}
	return json_encode($Data);
}


function TotalPatients(){
	global $db;
	$sql = "SELECT count(DISTINCT pid) as total from care_encounter";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total'];
}

function TodaysPatients(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(encounter_nr) as total from care_encounter where encounter_date = '".$date."' and current_dept_nr = '43'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total'];
}

function getPrescription($pn){
    global $db;

    $sql="SELECT p.nr, p.encounter_nr,p.article,p.partcode,p.modify_time, p.price, p.days, p.times_per_day, p.dosage, p.prescriber FROM care_encounter_prescription as p inner join care_encounter as e on p.encounter_nr = e.encounter_nr WHERE p.drug_class IN ('Dental', 'Medical-Supplies') AND p.status = 'pending' and p.encounter_nr = '".$pn."'";

    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();

    $btotal = 0;
    $prescription = array();
    $prescriptionInc = 0;
    if($numRows > 0){
    	while ($row = $result->FetchRow()) {
    		$total = $row['qty_prescried'] * $row['price'];
    	$qty_prescried = $row['dosage'] * $row['days'] * $row['times_per_day'];
    	$prescription[$prescriptionInc] = array('EncounterNo' => $row['encounter_nr'], 'Description' => $row['article'], 'DrugCode' => $row['partcode'], 'Dosage' => $row['dosage'], 'TimesPerDay' => $row['times_per_day'], 'Days' => $row['days'], 'InputDate' => $row['modify_time'], 'Nr' => $row['nr'], 'price' => $row['price'], 'qty_prescried' => $qty_prescried, 'prescriber' => $row['prescriber'], 'total' => $total);
    	$btotal = $btotal + $total;
    	$prescriptionInc++;
    }
    }

    $json_array = json_encode($prescription);
    return $json_array;


}

function getIssuePrescriptions($pn){
	global $db;

	$sql = "SELECT distinct a.nr,b.encounter_nr,b.encounter_class_nr, a.create_time as prescribe_date,a.partcode,b.pid,a.article_item_number,a.article,
            a.dosage,times_per_day,a.days,a.article,
            a.price AS amnt,(a.dosage*times_per_day*a.days) AS qty,(round(a.price*(a.dosage*times_per_day*a.days) / 50) * 50) AS total,
            l.quantity,'Issued' FROM care_encounter_prescription as a
            INNER JOIN  care_encounter as b ON a.encounter_nr=b.encounter_nr
            LEFT JOIN care_ke_locstock l on a.partcode=l.stockid
            LEFT JOIN care_ke_stlocation c on l.loccode=c.st_id
            WHERE a.encounter_nr = '".$pn."' AND a.drug_class in ('Dental', 'Medical-Supplies') and b.encounter_class_nr=2 AND a.status
            NOT IN('serviced','done','Cancelled')";

    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();

    $btotal = 0;
    $prescription = array();
    $prescriptionInc = 0;
    if($numRows > 0){
    	while ($row = $result->FetchRow()) {
$sql_issued = "SELECT issued from care_ke_internal_orders WHERE presc_nr = '".$row['nr']."'";
$result_issued = $db->Execute($sql_issued);
$row_issued = $result_issued->FetchRow();


    	$qty_prescried = $row['dosage'] * $row['days'] * $row['times_per_day'];
    	$prescription[$prescriptionInc] = array('EncounterNo' => $row['encounter_nr'], 'Description' => $row['article'], 'DrugCode' => $row['partcode'], 'Dosage' => $row['dosage'], 'TimesPerDay' => $row['times_per_day'], 'Days' => $row['days'], 'InputDate' => $row['prescribe_date'], 'Nr' => $row['nr'], 'price' => $row['amnt'], 'qty_prescried' => $row['qty'], 'prescriber' => $row['prescriber'], 'total' => $row['total'], 'issued' => $row_issued['issued'], 'qtyStore' => $row['quantity']);
    	$btotal = $btotal + $row['total'];
    	$prescriptionInc++;
    }
    }
    // article_item_number,prescribe_date,partcode,article,dosage,times_per_day,days,
    // quantity,amnt,qty,issued,total

    $json_array = json_encode($prescription);
    return $json_array;
}

function getPatient($pn){
	global $db;

	$sql = "SELECT p.selian_pid, p.name_first, p.name_2, p.name_last, p.date_birth, p.sex, p.pid, e.encounter_nr from care_person as p inner join care_encounter as e on p.pid = e.pid WHERE e.encounter_nr = '".$pn."'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	$name = $row['name_first'] . ' ' . $row['name_2'] . ' ' . $row['name_last'];
	$json = array('name' => $name, 'dob' => $row['date_birth'], 'sex' => $row['sex'], 'pid' => $row['pid'], 'fileno' => $row['selian_pid'], 'age' => $row['age'], 'enr' => $row['encounter_nr']);
	$json_array = json_encode($json);
	return $json;
}

function allVisits(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(encounter_nr) as total from care_encounter where current_dept_nr = '43'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total'];

}

function FirstTimePatients(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(*)as total1 from (SELECT COUNT(pid)
				FROM care_encounter
				GROUP BY pid
				HAVING COUNT(pid) = 1) as total";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total1'];
}

function allFirstTimeVisits1($sdate, $edate, $type)
{
	global $db;
	if($type == 1){
		$having = "count(pid) = 1";
	}elseif ($type == 2) {
		$having = "count(pid) > 1";
	}
	$sql = "SELECT encounter_nr FROM care_encounter where encounter_date between '$sdate' and '$edate' group by pid having $having";
	$result = $db->Execute($sql);
	$patients = [];
	$patientsInc = 0;

	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$patients[] = "'".$row['encounter_nr']."'";
			
		}
	}

	return implode(",", $patients);
}

function allFirstTimeVisits($startDate, $endDate, $type){
	global $db;
	
	$pa = allFirstTimeVisits1($startDate, $endDate, $type);
	$sql = "SELECT a.name_first, a.name_last, a.name_2, a.phone_1_nr, a.pid FROM care_person a inner join care_encounter b on a.pid = b.pid where b.encounter_nr IN ($pa) AND b.current_dept_nr = '43'";

    $result = $db->Execute($sql);
    $count = $result->RecordCount();

    $patient = array();
    $patientInc = 0;
    while ($row = $result->FetchRow()) {
    	$name = $row['name_first'].' '.$row['name_2'].' '.$row['name_last'];
		
		$patient[$patientInc] = array('patientName' => $name, 'patientPhone' => $row['phone_1_nr'], 'patientPID' => $row['pid']);
		$patientInc++;
	}

	$json_array = json_encode($patient);
	return $json_array;

}

function Patients(){
	$date = date("Y-m-d");
	global $db;
	$sql = "SELECT count(DISTINCT pid) as total from care_encounter where current_dept_nr = '43'";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total'];
}

function allPatients($startDate, $endDate){
	global $db;

	$sql = "SELECT p.pid, p.name_first, p.name_2, p.name_last, e.encounter_date, p.phone_1_nr, e.encounter_nr from care_encounter as e inner join care_person p on e.pid = p.pid WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."' and current_dept_nr = '43'";
	$result = $db->Execute($sql);
	$patient = array();
    $patientInc = 0;
    $allPatientCounts = $result->RecordCount();
	while ($row = $result->FetchRow()) {
		$name = $row['name_first'].' '.$row['name_2'].' '.$row['name_last'];

		$sql_count = "SELECT pid FROM care_encounter WHERE pid = '".$row['pid']."'";
		$result_count = $db->Execute($sql_count);
		$RecordCount = $result_count->RecordCount();

		$patient[$patientInc] = array('patientName' => $name, 'patientPhone' => $row['phone_1_nr'], 'patientDate' => $row['encounter_date'], 'patientPID' => $row['pid'], 'visits' => $RecordCount, 'allPatients' => $allPatientCounts, 'EncounterNo' => $row['encounter_nr']);
		$patientInc++;
	}
	$json_array = json_encode($patient);
	return $json_array;
}

function allRevisits($startDate, $endDate){
	global $db;

	$sql = "SELECT pid, name_first, name_2, name_last, encounter_date, phone_1_nr from (SELECT e.pid, e.encounter_date, p.name_first, p.name_last, p.name_2, p.phone_1_nr
				FROM care_encounter as e inner join care_person p on e.pid = p.pid
				GROUP BY e.pid
				HAVING COUNT(e.pid) > 1) as pid";

	if($startDate <> '' && $endDate <> ''){
		$sql = $sql . " WHERE encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
	}else{
		$sql = $sql . " limit 0, 100";
	}
    $result = $db->Execute($sql);

    $patient = array();
    $patientInc = 0;
    while ($row = $result->FetchRow()) {
    	$sql_count = "SELECT pid as row_count from care_encounter WHERE pid = '".$row['pid']."'";
    	$result_count = $db->Execute($sql_count);
    	$row_count = $result_count->FetchRow();
    	$counts = $result_count->RecordCount();


    	$name = $row['name_first'].' '.$row['name_2'].' '.$row['name_last'];
		
		$patient[$patientInc] = array('patientName' => $name, 'patientPhone' => $row['phone_1_nr'], 'patientDate' => $row['encounter_date'], 'patientPID' => $row['pid'], 'count' => $counts);
		$patientInc++;
	}

	$json_array = json_encode($patient);
	return $json_array;
}


function PatientRevisits(){
	$date = date("Y-m-d");
	global $db;

	$sql = "SELECT count(*)as total1 from (SELECT COUNT(pid)
				FROM care_encounter
				GROUP BY pid
				HAVING COUNT(pid) > 1) as total";
	$result = $db->Execute($sql);
	$row = $result->FetchRow();
	return $row['total1'];
}

function PaymentMode(){
	global $db;

	$sql = "SELECT DISTINCT pay_mode FROM care_ke_receipts";
	$result = $db->Execute($sql);

	$PaymentMode = array();
	$PaymentModeInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('pay_mode' => $row['pay_mode']));
		$PaymentMode[$PaymentModeInc] = $json;
		$PaymentModeInc++;
	}

	$json_array = json_encode($PaymentMode);
	return $json_array;
}

function Cashier(){
	global $db;

	$sql = "SELECT DISTINCT username FROM care_ke_receipts";
	$result = $db->Execute($sql);
	$cashier = array();
	$cashierInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('username' => $row['username']));
		$cashier[$cashierInc] = $json;
		$cashierInc++;
	}
	$json_array = json_encode($cashier);
	return $json_array;
}

function Category(){
	global $db;

	$sql = "SELECT DISTINCT rev_desc FROM care_ke_receipts";
	$result = $db->Execute($sql);

	$Category = array();
	$CategoryInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('desc' => $row['rev_desc']));
		$Category[$CategoryInc] = $json;
		$CategoryInc++;
	}
	$json_array = json_encode($Category);
	return $json_array;

}

function Debtors(){
	global $db;
	$sql = "SELECT * FROM care_ke_debtors where suspended = 0";
	$result = $db->Execute($sql);
	$debtors = array();
	$debtors_inc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('accno' => $row['accno'], 'name' => $row['name']));
		$debtors[$debtors_inc] = $json;
		$debtors_inc++;
	}
	$json_array = json_encode($debtors);
	return $json_array;
}

function creditTransactions($debtor, $startDate, $endDate){
	global $db;
	$trans = array();
	$transInc = 0;
	// $sql = "SELECT * FROM care_ke_debtortrans";

	$sql = "SELECT t.id, d.name, t.accno, t.transno, t.pid, t.transdate, t.bill_number, t.amount, p.name_first, p.name_2, p.name_last FROM care_ke_debtortrans as t inner join care_ke_debtors as d on t.accno = d.accno inner join care_person as p on t.pid = p.pid";
	if($debtor <> '' && $debtor <> 1){
		$sql = $sql . " where t.accno = '".$debtor."'";
	}

	if($startDate <> '' && $endDate <> ''){
		$sql = $sql . " and t.transdate between '".$startDate."' and '".$endDate."'";
	}
	$result = $db->Execute($sql);
	while ($row = $result->FetchRow()) {
		$json = (array('id' => $row['id'], 'debtorname' => $row['name'], 'debtor_acc' => $row['accno'], 'transno' => $row['transno'], 'transdate' => $row['transdate'], 'pid' => $row['pid'], 'bill_number' => $row['bill_number'], 'amount' => $row['amount'], 'name_first' => $row['name_first'], 'name_2' => $row['name_2'], 'name_last' => $row['name_last']));
		$trans[$transInc] = $json;
		$transInc++;
	}
	$json_array = json_encode($trans);

	return $json_array;
}

function cashTransactions($cashier, $startDate, $endDate, $pay_mode, $category){
	global $db;

	$sql = "SELECT * FROM care_ke_receipts where DATE_FORMAT(input_date, '%Y-%m-%d') between '".$startDate."' and '".$endDate."'";
	if($cashier <> '' && $cashier <> 1){
		$sql = $sql . " and username = '".$cashier."'";
	}

	if($pay_mode <> '' && $pay_mode <> 1){
		$sql = $sql . " and pay_mode = '".$pay_mode."'";
	}

	if($category <> '' && $category <> 1){
		$sql = $sql . " and rev_desc = '".$category."'";
	}

	$result = $db->Execute($sql);

	$transaction = array();
	$transactionInc = 0;
	while ($row = $result->FetchRow()) {
		$json = (array('cashier' => $row['username'], 'date' => $row['input_date'], 'payer' => $row['payer'], 'pay_mode' => $row['pay_mode'], 'category' => $row['rev_desc'], 'total' => $row['total']));

		$transaction[$transactionInc] = $json;
		$transactionInc++;
	}
	$json_array = json_encode($transaction);
	return $json_array;
}

function PendingRequests(){
	global $db;

	$sql="SELECT p.pid, p.selian_pid, name_first, name_last, pr.encounter_nr, pr.prescribe_date, 
            p.pid as batch_nr,e.encounter_class_nr FROM care_encounter_prescription pr 
                inner join care_encounter e on pr.encounter_nr = e.encounter_nr 
                and (pr.status='pending' OR pr.status='') 
                inner join care_person p on e.pid = p.pid 
                inner join care_tz_drugsandservices d on pr.article_item_number=d.item_id 
                and (pr.drug_class IN ('Dental', 'Medical-Supplies') OR d.purchasing_class ='Dental' and pr.article not like '%consult%') group by e.encounter_class_nr ,pr.prescribe_date, pr.encounter_nr, p.pid,
                p.selian_pid, name_first, name_last
                having datediff(now(),pr.prescribe_date)<7 ORDER BY pr.prescribe_date ASC";
}

function patientPendingRequests($pn){
	global $db;

	$sql="SELECT p.pid, p.selian_pid, name_first, name_last, pr.encounter_nr, pr.prescribe_date, 
            p.pid as batch_nr,e.encounter_class_nr FROM care_encounter_prescription pr 
                inner join care_encounter e on pr.encounter_nr = e.encounter_nr 
                and (pr.status='pending' OR pr.status='') 
                inner join care_person p on e.pid = p.pid 
                inner join care_tz_drugsandservices d on pr.article_item_number=d.item_id 
                and (pr.drug_class IN ('Dental', 'Medical-Supplies') OR d.purchasing_class ='Dental' and pr.article not like '%consult%') group by e.encounter_class_nr ,pr.prescribe_date, pr.encounter_nr, p.pid,
                p.selian_pid, name_first, name_last
                having datediff(now(),pr.prescribe_date)<7 ORDER BY pr.prescribe_date ASC";
}

function CashCashierSummary($startDate, $endDate){
	global $db;

	$begin = new DateTime( $startDate );
	$end = new DateTime( $endDate );
	$end = $end->modify( '+1 day' ); 

	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($begin, $interval ,$end);

	$Dates = array();
	$DatesInc = 0;
	foreach($daterange as $date){
	    $date1 = $date->format("Y-m-d");
	    
	    $sql_cashier = "SELECT DISTINCT username FROM care_ke_receipts";
		$result_cashier = $db->Execute($sql_cashier);
		$sql_total = "SELECT sum(total) as total FROM care_ke_receipts WHERE currdate = '".$date1."'";
		$result_total = $db->Execute($sql_total);
		$row_total = $result_total->FetchRow();

		$cashier = array();
		$cashierInc = 0;
		while ($row_cashier = $result_cashier->FetchRow()) {

			$sql = "SELECT sum(total) AS amount FROM care_ke_receipts WHERE currdate = '".$date1."' and username = '".$row_cashier['username']."'";
			$result = $db->Execute($sql);
			$row = $result->FetchRow();

			$json = (array('username' => $row_cashier['username'], 'amount' => $row['amount']));
			$cashier[$cashierInc] = $json;
			$cashierInc++;
		}
	    $Dates[$DatesInc] = (array('date' => $date1, 'usertotal' => $cashier, 'dailyTotal' => $row_total['total']));
	    $DatesInc++;
	    
	}
	$json_array = json_encode($Dates);
	return $json_array;
}

function CreditDepartments($startDate, $endDate){
	global $db;

	$sql = "SELECT DISTINCT service_type FROM care_ke_billing where service_type NOT IN ('Payment') and bill_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$result = $db->Execute($sql);

		$department = array();
		$departmentInc = 0;
		while ($row = $result->FetchRow()) {

			$department[$departmentInc] = array('service_type' => $row['service_type']);
			$departmentInc++;
		}

		$json_array = json_encode($department);
		return $json_array;
}

function CreditDepartmentSummary($startDate, $endDate){
	global $db;

	$begin = new DateTime( $startDate );
	$end = new DateTime( $endDate );
	$end = $end->modify( '+1 day' ); 

	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($begin, $interval ,$end);

	$Dates = array();
	$DatesInc = 0;

	foreach ($daterange as $date) {
		$date1 = $date->format("Y-m-d");

		// $sql_date_total = "SELECT sum(b.total) as date_sum from care_ke_billing as b inner join care_ke_debtortrans as d on b.bill_number = d.bill_number WHERE b.bill_date = '".$date1."'";
		$sql_date_total = "SELECT sum(care_ke_billing.total) as date_sum FROM care_ke_billing inner join care_ke_debtortrans on care_ke_billing.bill_number = trim(care_ke_debtortrans.bill_number) WHERE care_ke_billing.service_type not in ('Payment', 'Payment Adjustment') and care_ke_billing.bill_date = '".$date1."'";
		$result_date_total = $db->Execute($sql_date_total);
		$row_date_total = $result_date_total->FetchRow();

		$sql = "SELECT DISTINCT service_type FROM care_ke_billing where service_type NOT IN ('Payment', 'Payment Adjustment') and bill_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$result = $db->Execute($sql);

		$CreditDepartment = array();
		$CreditDepartmentInc = 0;
		while ($row = $result->FetchRow()) {

			$sql_bill = "SELECT sum(care_ke_billing.total) as bill_total FROM care_ke_billing inner join care_ke_debtortrans on care_ke_billing.bill_number = trim(care_ke_debtortrans.bill_number) WHERE care_ke_billing.service_type not in ('Payment', 'Payment Adjustment') and care_ke_billing.service_type = '".$row['service_type']."' and care_ke_billing.bill_date = '".$date1."'";
			$result_bill = $db->Execute($sql_bill);
			$row_bill = $result_bill->FetchRow();

			$CreditDepartment[$CreditDepartmentInc] = array('service_type' => $row['service_type'], 'bill_total' => $row_bill['bill_total']);
			$CreditDepartmentInc++;
		}

		$Dates[$DatesInc] = array('date' => $date1, 'date_total' => $row_date_total['date_sum'], 'service_total' => $CreditDepartment);
		$DatesInc++;
		
	}

	

	$json_array = json_encode($Dates);
	return $json_array;
}

function PatientDepartments($startDate, $endDate){
	global $db;

		$sql = "SELECT DISTINCT(e.current_dept_nr), d.name_formal from care_encounter as e inner join care_department as d on e.current_dept_nr = d.nr WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
		$result = $db->Execute($sql);

		$department = array();
		$departmentInc = 0;
		while ($row = $result->FetchRow()) {
			
			
			$department[$departmentInc] = array('department' => $row['name_formal']);
			$departmentInc++;
		}
		$json_array = json_encode($department);
		return $json_array;

}

function PatientDepartmentRecords($startDate, $endDate){
	global $db;

	$begin = new DateTime( $startDate );
	$end = new DateTime( $endDate );
	$end = $end->modify( '+1 day' ); 
	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($begin, $interval ,$end);
	$Dates = array();
	$DatesInc = 0;

	foreach($daterange as $date){
	    $date1 = $date->format("Y-m-d");
	    $sql_total_visits = "SELECT * from care_encounter WHERE encounter_date = '".$date1."'";
	    $result_total_visits = $db->Execute($sql_total_visits);
	    $record_total_visits = $result_total_visits->RecordCount();
	    $sql = "SELECT DISTINCT(e.current_dept_nr), d.name_formal from care_encounter as e inner join care_department as d on e.current_dept_nr = d.nr WHERE e.encounter_date BETWEEN '".$startDate."' and '".$endDate."'";
			$result = $db->Execute($sql);
			$department = array();
			$departmentInc = 0;
			while ($row = $result->FetchRow()) {
				$sql_dep_count = "SELECT * FROM care_encounter WHERE encounter_date = '".$date1."' and current_dept_nr = '".$row['current_dept_nr']."'";
				$result_dep_count = $db->Execute($sql_dep_count);
				$record_dep_count = $result_dep_count->RecordCount();
				
				$department[$departmentInc] = array('department' => $row['name_formal'], 'recordCount' => $record_dep_count);
				$departmentInc++;
			}
	    $Dates[$DatesInc] = (array('date' => $date1, 'department' => $department, 'total_visits' => $record_total_visits));
	    $DatesInc++;
	}
	$json_array = json_encode($Dates);
	return $json_array;
}

function allServices(){
	global $db;
	 $sql = "SELECT DISTINCT a.item_id, a.partcode, a.item_description, a.unit_price, b.quantity, b.loccode FROM care_tz_drugsandservices as a inner join care_ke_locstock as b on a.partcode = b.stockid WHERE b.loccode = 'DISPENS' and (a.purchasing_class IN ('DENTAL') OR a.category IN ('DEN', 'DEN2'))";

	 $result = $db->Execute($sql);
	 $DentalServices = array();
	 $DentalServicesInc = 0;
	 while ($row = $result->FetchRow()) {
	 	$DentalServices[$DentalServicesInc] = array('item_id' => $row['item_id'], 'partcode' => $row['partcode'], 'Description' => $row['item_description'], 'unit_price' => $row['unit_price'], 'qty' => $row['quantity']);

	 	$DentalServicesInc++;
	 }

	 $json_array = json_encode($DentalServices);
	 return $json_array;
}

function ServicesUsage($startDate, $endDate){
	global $db;
	$sql = "SELECT distinct partcode, article from care_encounter_prescription where status = 'serviced' AND drug_class = 'Medical-Supplies' and prescribe_date BETWEEN '".$startDate."' and '".$endDate."' order by prescribe_date desc";
	$result = $db->Execute($sql);
	$ServicesUsage = array();
	 $ServicesUsageInc = 0;
	 $t_t_total = '';
	 while ($row = $result->FetchRow()) {
	 	// UsageServiceDetials($row[partcode]);
	 	$sql_service = "SELECT sum(qtyIssued) as tqty, price, sum(price) as tprice from care_encounter_prescription where status = 'serviced' AND partcode = '".$row['partcode']."' and prescribe_date BETWEEN '".$startDate."' and '".$endDate."'";
	 	$result_service = $db->Execute($sql_service);
	 	$price = '';
	 	$t_qty = '';
	 	$t_total = '';
	 	$row_service = $result_service->FetchRow();
	 	$ServicesUsage[$ServicesUsageInc] = array('partcode' => $row['partcode'], 'article' => $row['article'], 'prescribe_date' => $row['prescribe_date'], 'price' => $row_service[price], 'qty' => $row_service['tqty'], 'total' => $row_service['tprice']);
	 	$ServicesUsageInc++;
	 }

	 $json_array = json_encode($ServicesUsage);
	 return $json_array;
}
function UsageServiceDetails($partcode){
	global $db;
	$sql = "SELECT ";
}

function CompletedPrescription($pid){
	global $db;
	$sql = "SELECT a.nr, a.encounter_nr, a.prescribe_date, a.article, a.dosage, a.days, a.times_per_day, a.prescriber FROM care2x.care_encounter_prescription as a inner join care_encounter as p ON a.encounter_nr = p.encounter_nr where a.status = 'serviced' AND ((a.partcode like '%den%') or a.drug_class in ('DENTAL', 'Medical-Supplies')) AND p.pid = '$pid' order by prescribe_date desc";
	$result = $db->Execute($sql);$History = array();$HistoryInc = 0;
	 while ($row = $result->FetchRow()) {
	 	$History[$HistoryInc] = array('nr' => $row['nr'], 'encounter_nr' => $row[encounter_nr], 'article' => $row['article'], 'prescribe_date' => $row['prescribe_date'], 'dosage' => $row['dosage'], 'days' =>$row['days'], 'times_per_day' => $row['times_per_day'], 'prescriber' => $row['prescriber']);
	 	$HistoryInc++;
	 }
	 $json_array = json_encode($History);
	 return $json_array;
}

function Description($item_code){
	global $db;
	$result = $db->Execute("SELECT item_description from care_tz_drugsandservices WHERE partcode = '$item_code'");
	$row = $result->FetchRow();return $row[item_description];
}

function Qty($item_code, $startDate, $endDate){
	global $db;
	$result = $db->Execute("SELECT sum(qty) as qty FROM care2x.care_ke_consumables_intake WHERE item_code = '$item_code' and `date` BETWEEN '$startDate' and '$endDate'");
	$row = $result->FetchRow();return $row[qty];
}

function Consumables($startDate, $endDate){
	global $db;
	$sql = "SELECT distinct item_code from care_ke_consumables_intake where department = 'DEN' and `date` between '$startDate' and '$endDate'";$result = $db->Execute($sql);$Consumables = array();$ConsumablesInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$desc = Description($row[item_code]);$qty = Qty($row[item_code], $startDate, $endDate);
			$Consumables[$ConsumablesInc] = (array('item_code' => $row[item_code], 'desc' => $desc, 'qty' => $qty));
			$ConsumablesInc++;
		}
	}
	return json_encode($Consumables);
}

function patientConsumables($startDate, $endDate, $patient){
	global $db;
	if($patient <> '' || $patient <> null){
		$query = " and pid = '$patient'";
	}else{
		$query = " limit 0, 100";
	}
	$sql = "SELECT item_code, qty from care_ke_consumables_intake where department = 'DEN' and `date` between '$startDate' and '$endDate' $query";$result = $db->Execute($sql);$Consumables = array();$ConsumablesInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$desc = Description($row[item_code]);$qty = Qty($row[item_code], $startDate, $endDate);
			$Consumables[$ConsumablesInc] = (array('item_code' => $row[item_code], 'desc' => $desc, 'qty' => $row[qty]));
			$ConsumablesInc++;
		}
	}
	return json_encode($Consumables);
}
?>

<script type="text/javascript">
	function getNextIssN(str){
		xmlhttp5 = GetXmlHttpObject();
        if (xmlhttp5 == null)
        {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "dentalVals.php?desc5=" + str;
        url = url + "&sid=" + Math.random();
        url = url + "&callerID=issNo";
        xmlhttp5.onreadystatechange = stateChanged5;
        xmlhttp5.open("POST", url, true);
        xmlhttp5.send(null);
	}

</script>