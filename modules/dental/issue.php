<?php

include 'functions.php';
require_once($root_path.'modules/pharmacy_tz/mylinks.php');
if(isset($_GET['enr'])){
  $pn = $_GET['enr'];
}else{
  $pn = '';
}

$patient = getPatient($pn);
$prescriptions = getIssuePrescriptions($pn);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php
require 'sidebar.php';
$pa = json_decode($prescriptions, true);

  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patient Details
      </h1>   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            <div class="box-header">
              <table><tr>
                <td></td>
              </tr></table>
            </div>
            	<div class="box-body">
                <table width=100% border=1 cellspacing=0 height=100%>
                  <tbody>
                      <tr>
                          <td  valign="top" align="middle" height="35">
                            <form name="order" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>"  onSubmit="return chkform(this)">
                              <table width=80% border=0>
                                <tr>
                                    <td>Type:</td>
                                    <td><input type="radio" id="issType" name="issType" value="2" onclick="getNextIssNo(this.value)" checked/>:OutPatient
                                        <input type="radio" id="issType" name="issType" value="1" onclick="getNextIssNo(this.value)" />:InPatient
                                        <input type="radio" id="issType" name="issType" value="Cash" onclick="getNextIssNo(this.value)" />:Cash Sale</td>
                                    <td></td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <td>Date</td>
                                    <td><input type="text" id="issDate" name="issDate" value="<?php echo $req_date = date("Y-m-d"); ?>" />
                                        Issue No:<input type="text" id="issNo" name="issNo" readonly value="" /></td>
                                    <td>Cash Receipt:</td>
                                    <td><input type="text" id="receiptNo" name="receiptNo" readonly value="" /></td>
                                  </tr>
                                  <tr>
                                    <td>Patient No</td>
                                    <td>
                                      <input type="text" id="pid" name="pid" value="<?php echo $patient['pid'] ?>" ondblclick="initPtsearch()" onblur="getOrders()"/>
                                        <input type="text" id="pname" name="pname" value="<?php echo $patient['name'] ?>" size="30" onclick="getOrders()" onblur="getDrugTotals(6);"/>
                                        <input type="text" value="<?php echo $pn ?>" id="enc_no" name="enc_no" readonly size="10" />
                                        <img src="gui/edit_pricesettings_files/en_downarrow.png" width="12" height="12" alt="en_downarrow" onclick="getOrders()"/>
                          <button id='history' onclick="getPrescriptionHistory()">Prescription History</button></td>
                                     <td width=120px>Receipt Amount:</td>
                                     <td>
                                      <input type="text" id="receiptAmount" name="receiptAmount" value="" />
                                    </td>
                                </tr>
                                <tr>
                                  <td>Doctor</td>
                                  <td><input type="text" id="docName" name="docName" value="<?php echo $pa['prescriber']; ?>" size="30" readonly/>
                                      <input type="text" id="numRows" name="numRows" readonly></td>
                                  <td>Age</td>
                                  <td><input type="text" id="age" name="age" value="<?php echo $pa['age'] ?>" size="20"/></td>
                              </tr>
                              <tr>
                                  <td>Store</td>
                                  <td>
                                    <input type="text" id="storeID" name="storeID" value="DENTAL" size="30"/>
                                       <?php  
                                        $input_user = $_SESSION['sess_login_username'];
                                       ?>
                                      <input type="text" id="" name="Storedesc" value="Dental Issue" size="30"/></td>
                                  <td>Payment Method:</td>
                                  <td><div id="paymode" class="pay_default"></div><input type="hidden" id="pmode" name="pmode" value="<?php  ?>"/>
                                      <div id="message" class="blinkError"></div></td>
                              </tr>
                              <tr>
                                <td colspan="4">
                                    <?php
                                    $totalCost = 0;
                if($pa ==! null){
                  ?>
                  <table width="100%" class="panel panel-body table-striped">
                    <thead>
                      <tr>
                        <th>Prescription Date</th>
                        <th>Item ID</th>
                        <th>Item Description</th>
                        <th>Dosage</th>
                        <th>Times Per Day</th>
                        <th>Days</th>
                        <th>Qty in Store</th>
                        <th>Price</th>
                        <th>Qty Prescribed</th>
                        <th>Issued</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <?php
                    
                    foreach ($pa as $key => $value) {
                      $total = $value['price'] * $value['qty_prescried'];
                      $nr = $value['Nr'];
                      ?>
                      <tr>
                        <input type="hidden" name="nr[]" value="<?php echo $value['Nr']; ?>">
                        <input readonly type="hidden" value="<?php echo $value['Description']; ?>" name="Description[]">
                        <input type="hidden" value="<?php  echo $value['InputDate'] ?>" name="InputDate[]">
                        <input type="hidden" value="<?php echo $value['DrugCode'] ?>" name="DrugCode[]">
                              <td><?php echo $value['InputDate']; ?></td>
                              <td><?php echo $value['DrugCode']; ?></td>
                              <td><?php echo $value['Description']; ?></td>
                              <td><input id="<?php echo 'dosage'.$key ?>" size="5" type="text" value="<?php echo $value['Dosage']; ?>" name="dosage[]"></td>
                              <td><input id="<?php echo 'TimesPerDay'.$key ?>" size="5" type="text" value="<?php echo $value['TimesPerDay']; ?>" name="TimesPerDay[]"></td>
                              <td><input size="5" id="<?php echo 'days'.$key ?>" type="text" value="<?php echo $value['Days']; ?>" name="days[]"></td>
                              <td><?php echo $value['quantity']; ?></td>
                              <td><input id="<?php echo 'price'.$key ?>" size="5" type="text" value="<?php echo $value['price']; ?>" name="price[]"></td>
                              <td><input size="5" readonly="readonly" id="<?php echo 'qty_prescried'.$key ?>" type="text" value="<?php echo $value['qty_prescried']; ?>" name="qty_prescried[]"></td>
                              <td><input size="5" readonly="readonly" id="<?php echo 'issued'.$key ?>" type="text" value="<?php echo $value['issued']; ?>" name="issued[]"></td>
                              <td><input size="5" readonly="readonly" id="<?php echo 'total'.$key ?>" type="text" value="<?php echo $total; ?>" name="total[]"></td>
                      </tr>
                      <?php
                      $totalCost = $totalCost + $total;
                    }
                    ?>
                  </table>
                  <?php
                }
    
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Total Cost</td>
                                <td><input type="text" id="" name="totalCost" value="<?php echo $totalCost ?>" onclick=""/></td>
                                <td colspan="2" align="right">
                                    <input type="submit" id="update" name="submit" value="Update" style="width:120px; height:30px;" />
                                    <input type="submit" id="submit" name="submit" value="Send"  style="width:120px; height:30px;" />
                                 </td>
                            </tr>
                                </table>
                            </form>
                          </td>
                      </tr>
                  </tbody>
                </table>
            	</div>
        	</div>
    	</div>
	</div>
  <?php
  require 'issuedrugs.php';
  ?>
</section>
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.map"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#firstvisits').DataTable();
	} );
</script>
<script type="text/javascript">
  $(document).ready(function()
    {
        function updatePrice()
        {
            var dosage = parseFloat($("#dosage").val());
            var TimesPerDay = parseFloat($("#TimesPerDay").val());
            var days = parseFloat($("#days").val());
            var qty_prescried = (dosage * TimesPerDay * days);
            var qty_prescried = qty_prescried.toFixed(2);
            $("#qty_prescried").val(qty_prescried);
        }
        $(document).on("change, keyup", "#dosage", updatePrice);
});

  function getStore3(str){
        xmlhttp4=GetXmlHttpObject();
        if (xmlhttp4==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        var url="dentalVals.php?desc4="+str;
        url=url+"&sid="+Math.random();
        url=url+"&callerID=issue";
        xmlhttp4.onreadystatechange=stateChanged4;
        xmlhttp4.open("POST",url,true);
        xmlhttp4.send(null);
    }
</script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</body>
</html>
