<?php
include 'functions.php';

$endDate = date("Y-m-d");
$startDate = date("Y-m-d");
$patient = '';

if(isset($_POST['preview'])){
  if($_POST['startDate'] <> ''){
    $startDate = $_POST['startDate'];
  }
  if($_POST['endDate']){
    $endDate = $_POST['endDate'];
  }

  if($_POST[patient]){
    $patient = $_POST[patient];
  }
}
$consumables = json_decode(patientConsumables($startDate, $endDate, $patient), true);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
require 'sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Consumable Usage - Dental
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
                  <div class="col-md-12">
                    <form name="debtors" method="POST" action="">

                      <table width="100%">
                        <tr>
                          <td class="text-center"><label>Start Date</label></td>
                          <td>
                            
                            <div class="input-group date">
                      <input name="startDate" value="" type="date" class="form-control input-sm pull-right" id="">
                      
                      </div>
                          </td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="text-center"><label>End Date</label></td>
                          <td>
                            
                            <div class="input-group date">
                      <input name="endDate" value="" type="date" class="form-control input-sm pull-right" id="datepicker2">
                      
                      </div>
                          </td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="text-center"><label>Patient ID</label></td>
                          <td>
                            
                            <div class="input-group date">
                      <input name="patient" value="" type="number" class="form-control input-sm pull-right">
                      
                      </div>
                          </td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td>
                            <label></label>
                            <input type="submit" name="preview" value="Previw">
                          </td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            	<div class="box-body">
                <table id="consumables" class="table table-responsive table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>PartCode</th>
                      <th>Description</th>
                      <th>Quantity</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    foreach ($consumables as $key => $value) {
                      $key = $key + 1;
                      echo "<tr>";
                      echo "<td>".$key."</td>";
                      echo "<td>".$value[item_code]."</td>";
                      echo "<td>".$value[desc]."</td>";
                      echo "<td>".$value[qty]."</td>";
                      ?>
                      <td>
                        <button type="button" name="Edit" id="<?php echo $value['item_code']; ?>" class="btn btn-info btn-xs edit_data">Edit</button>
                    <button type="button" name="Delete" id="<?php echo $value['item_code']; ?>" class="btn btn-danger btn-xs delete_data"><i class="glyphicon glyphicon-trash"></i>Delete</button>
                      </td>
                      <?php
                      echo "</tr>";
                    }
                    ?>
                  </tbody>
                </table>
            	</div>
        	</div>
    	</div>
</section>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#consumables').DataTable();
	} );
</script>
<script type="text/javascript">
  function IssuePrescriptions(issue){
    // var pn = document.getElementById('issue').value;
    // alert(issue);

    window.location = 'issue.php?pid=' + issue;
  }
</script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
</body>
</html>
<div id="edit_modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Update Service</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="update_form">
                          <label>Enter Description</label>  
                          <input type="text" name="item_description" id="item_description" class="form-control" />  
                          <br />
                          <label>Enter Quantity</label>  
                          <input type="text" name="qty" id="qty" class="form-control" />  
                          <br />  
                          <input type="hidden" name="id" id="id" />  
                          <input type="submit" name="update" id="update" value="Update" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
<script type="text/javascript">
  $(document).on('click', '.edit_data', function(){  
           var partcode = $(this).attr("id");  
           $.ajax({  
                url:"fetchConServices.php",  
                method:"POST",  
                data:{partcode:partcode},  
                dataType:"json",  
                success:function(data){  
                    $('#item_code').val(data.item_code);
                     $('#item_description').val(data.item_description);
                     $('#qty').val(data.qty);
                     $('#id').val(data.id);
                     $('#insert').val("Update");  
                     $('#edit_modal').modal('show');  
                }  
           });  
      });

  $('#update_form').on("submit", function(event){  
           event.preventDefault();  
           if($('#item_description').val() == "")  
           {  
                alert("Item Description is required");  
           }  
           else if($('#qty').val() == '')  
           {  
                alert("Unit Price is required");  
           }
           else  
           {  
                $.ajax({  
                     url:"updateConServices.php",  
                     method:"POST",  
                     data:$('#update_form').serialize(),  
                     beforeSend:function(){  
                          $('#update').val("Updating...");  
                     },  
                     success:function(data){  
                          $('#update_form')[0].reset();  
                          $('#edit_modal').modal('hide');
                          $('#consumables').load(location.href + " #consumables");
                          alert(data);
                     }  
                });  
           }  
      });

</script>