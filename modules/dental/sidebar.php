


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Patient Records</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="allpatients.php"><i class="fa fa-circle-o"></i> All Patients</a></li>
            <li><a href="firsttime.php"><i class="fa fa-circle-o"></i> First Time Visits</a></li>
            <li><a href="revisits.php"><i class="fa fa-circle-o"></i> Revisits</a></li>
            <li><a href="department_visits.php"><i class="fa fa-circle-o"></i> Patients per department</a></li>
          </ul>
        </li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Consumables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="Consumables.php"><i class="fa fa-circle-o"></i>Consumables - Summary</a></li></li>
            <li><a href="patientConsumables.php"><i class="fa fa-circle-o"></i>Patients Consumables</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Prescriptions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pendingrequests.php"><i class="fa fa-circle-o"></i>Pending Prescriptions</a></li>
            <li><a href="completedrequests.php"><i class="fa fa-circle-o"></i>Patients Presc. History</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="services.php"><i class="fa fa-circle-o"></i>Dental Services</a></li>
            <li><a href="services_usage.php"><i class="fa fa-circle-o"></i>Dental Services Usage</a></li>
          </ul>
        </li>

<li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="weeklyservices.php"><i class="fa fa-circle-o"></i>Weekly Services Usage</a></li>
          </ul>
        </li>        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>