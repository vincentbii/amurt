<?php

include 'functions.php';

$endDate = date("Y-m-d");
$startDate = date("Y-m-d");

if(isset($_POST)) {
    $date = explode('-', $_POST['month']);
    $year = $date[0];
    $month = $date[1];
    echo "Year: ".$year." Month: ".$month;
    $data = json_decode(WeeklyReport($year, $month), true);
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
require 'sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Services Usage
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Services Usage Report</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            	<div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <form name="debtors" method="POST" action="">

                      <table width="100%">
                        <tr>
                          <td>
                            <select name="month" onchange="this.form.submit()">
                            <?php
                            printf('<option selected disabled> -- Select Month -- </option>', $value, $label);
                              for ($i = 0; $i <= 12; ++$i) {
                                $time = strtotime(sprintf('-%d months', $i));
                                $value = date('Y-m', $time);
                                $label = date('F Y', $time);
                                printf('<option value="%s">%s</option>', $value, $label);
                              }
                              ?>
                            </select>
                            </td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
                <input name="b_print" type="button" class="ipt button btn-info btn-sm"   onClick="PrintElem('report_pharmacy');" value="Print ">
                <div class="row" id="report_pharmacy">
                  <div class="col-md-12">
                    <table class="table table-condensed table-striped table-bordered">
                      <thead>
                        <tr style="text-align: center;">
                          <th>Date</th>
                          <th style="text-align: center;" colspan="2">Extraction</th>
                          <th style="text-align: center;" colspan="2">ROOT CANAL</th>
                          <th style="text-align: center;" colspan="2">Others</th>
                          <th>Total</th>
                        </tr>
                        <tr>
                          <th></th>
                          <th>First Time</th>
                          <th>Revesits</th>
                          <th>First Time</th>
                          <th>Revesits</th>
                          <th>First Time</th>
                          <th>Revesits</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($data as $key => $value) {
                          echo "<tr>";
                          echo "<th style='text-align: center;' valign='center' colspan='8'>".$value['min'].'  -  ' .$value['max']."</th>";
                          echo "</tr>";
                          $min = $value['min'];$max = $value['max'];
                          $dates = json_decode(DaysOfTheWeek($min, $max), true);
                          // var_dump(DaysOfTheWeek($min, $max), true);
                          // echo $value['min'] .'  -  '. $value['max'] .'<br>';
                          $extraction = 0;
                          $extractionr = 0;
                          $pr = 0;
                          $prr = 0;
                          $Others = 0;
                          $OthersR = 0;
                          $ttotals = 0;
                          foreach ($dates as $key1 => $value) {
                            $totals = $value['extraction'] + $value['extractionr'] + $value['PR'] + $value['PRR'] + $value['Others'] + $value['OthersR'];
                            $extractionr += $value['extractionr'];
                            $extraction += $value['extraction'];
                            $pr += $value['PR'];
                            $prr += $value['PRR'];
                            $Others += $value['Others'];
                            $OthersR += $value['OthersR'];
                            $ttotals += $totals;

                            echo "<tr>";
                            echo "<td>".$value['date']."</td>";
                            echo "<td>".$value['extraction']."</td>";
                            echo "<td>".$value['extractionr']."</td>";
                            echo "<td>".$value['PR']."</td>";
                            echo "<td>".$value['PRR']."</td>";
                            echo "<td>".$value['Others']."</td>";
                            echo "<td>".$value['OthersR']."</td>";
                            echo "<th>".$totals."</th>";
                            echo "</tr>";
                          }
                          echo "<tr>";
                          echo "<th>Total</th>";
                          echo "<th>".$extraction."</th>";
                          echo "<th>".$extractionr."</th>";
                          echo "<th>".$pr."</th>";
                          echo "<th>".$prr."</th>";
                          echo "<th>".$Others."</th>";
                          echo "<th>".$OthersR."</th>";
                          echo "<th>".$ttotals."</th>";
                          echo "</tr>";


                        }
                        ?>
                      </tbody>
                    </table>
                    
                  </div>
                </div>

            	</div>
        	</div>
    	</div>
	</div>
</section>
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#ServicesUsage').DataTable({
        
      });
	} );
</script>
<script type="text/javascript">
  function PrintElem(elem){
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
</script>
<style type="text/css">
  @media print {
    .report_pharmacy {
        background-color: white;
        height: 100%;
        width: 100%;
        position: fixed;
        top: 0;
        left: 0;
        margin: 0;
        padding: 15px;
        font-size: 14px;
        line-height: 18px;
    }
}
</style>
</body>
</html>
