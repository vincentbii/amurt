<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require('./roots.php');
// require($root_path.'include/inc_environment_global.php');

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $root_path.'assets/css/bootstrap.min.css'; ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
    <div class="text-right">
      
      <form action="search1.php" method="GET">
        <a class="btn btn-primary btn-sm" href="service_new.php">Add New Service/Drug</a>
        <input type="text" name="query" />
        <input type="submit" value="Search" />
    </form>
    </div>
		<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#drug_list">Drug List</a></li>
			  <li><a data-toggle="tab" href="#services">Services</a></li>
			  <li><a data-toggle="tab" href="#phisiotherapy">Phisiotherapy</a></li>
			  <li><a data-toggle="tab" href="#dental_services">Dental Services</a></li>
			  <li><a data-toggle="tab" href="#medical_supplies">Medical Supplies</a></li>
			  <li><a data-toggle="tab" href="#labaratory_services">Larabatory Services</a></li>
			  <li><a data-toggle="tab" href="#consultation">Consultation</a></li>
			  <li><a data-toggle="tab" href="#other_services">Other Services</a></li>
		</ul>

<div class="tab-content">
  <div id="drug_list" class="tab-pane fade in active">
    <div class="text-right">
      <a class="btn btn-info btn-xs" href="drugs_export.php">Export Drugs Sample</a>
      <?php require_once 'drugs_import.php'; ?>
    </div>
    <?php require_once 'drug_list.php'; ?>
  </div>
  <div id="services" class="tab-pane fade">
    <?php require_once 'update_all_services_tab.php'; ?>
  </div>
  <div id="phisiotherapy" class="tab-pane fade">
    <?php require_once 'phisiotherapy.php' ?>
  </div>
  <div id="dental_services" class="tab-pane fade">
  	<?php require_once 'dental_services.php' ?>
  </div>
  <div id="medical_supplies" class="tab-pane fade">
  	<?php require_once 'medical_supplies.php' ?>
  </div>
  <div id="labaratory_services" class="tab-pane fade">
  	<?php require_once 'labaratory_services.php' ?>
  </div>
  <div id="consultation" class="tab-pane fade">
  	<?php require_once 'consultation.php' ?>
  </div>
  <div id="other_services" class="tab-pane fade">
  	<?php require_once 'other_services.php' ?>
  </div>
</div>





		
	</div>
</body>
</html>