<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require('./roots.php');

define('LANG_FILE','billing.php');
define('NO_CHAIN',1);

require_once($root_path.'include/inc_front_chain_lang.php');

$breakfile=$root_path.'main/spediens.php'.URL_APPEND;

# Extract the language variable
extract($TXT);
?>

<?php
$setSql = "SELECT partcode, item_description, category, unit_price, purchasing_class from care_tz_drugsandservices where purchasing_class = 'Drug_list' limit 10, 10";  
$setRec = $db->Execute($setSql);
  
  
$setData = '';  

?>
<table>
	<tr>  
        <th>PartCode</th>  
        <th width="120">Item Description</th>  
        <th>Category</th>
        <th>Unit Price</th>
        <th>Purchasing Class</th>  
    </tr> 
	<?php

while ($rec = $setRec->FetchRow()) {  
  ?><tr>
  	<td><?php echo $rec['partcode']; ?></td>
  	<td><?php echo $rec['item_description']; ?></td>
  	<td><?php echo $rec['category']; ?></td>
  	<td><?php echo $rec['unit_price']; ?></td>
  	<td><?php echo $rec['purchasing_class']; ?></td>
  </tr><?php
}  
  

  ?>
</table>
<?php
  
  
header("Content-type: application/octet-stream");  
header("Content-Disposition: attachment; filename=DrugsList.xls");  
header("Pragma: no-cache");  
header("Expires: 0");  
  
echo $setData; 
?>