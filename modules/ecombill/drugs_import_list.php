<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
/**
* eComBill 1.0.04 for Care2002 beta 1.0.04 
* (2003-04-30)
* adapted from eComBill beta 0.2 
* developed by ecomscience.com http://www.ecomscience.com 
* Dilip Bharatee
* Abrar Hazarika
* Prantar Deka
* GPL License
*/
require('./roots.php');

define('LANG_FILE','billing.php');
define('NO_CHAIN',1);

require_once($root_path.'include/inc_front_chain_lang.php');

$breakfile=$root_path.'main/spediens.php'.URL_APPEND;

# Extract the language variable
if(isset($_POST["submit_file"]))
{
 $file = $_FILES["file"]["tmp_name"];
 $file_open = fopen($file,"r");
 $partcode = '';
 while(($csv = fgetcsv($file_open, 1000, ",")) !== false)
 {
  $partcode = $csv[0];
  $description = $csv[1];
  $category = $csv[2];
  $unit_price = $csv[3];
  $purchasing_class = $csv[4];

  $update = $db->Execute("UPDATE care_tz_drugsandservices SET item_description = '".$description."', item_full_description = '".$description."', purchasing_class = '".$purchasing_class."', unit_price = '".$unit_price."' WHERE partcode = '".$partcode."'");

  if($update){
  	$sql="INSERT INTO care_tz_drugsandservices
			(item_number, partcode, item_description, purchasing_class, category, unit_price)
			VALUES
			(
			'".$partcode."', '".$partcode."','".$description."', 'Drug_list', '".$category."', '".$unit_price."'
			)";

			$db->Execute($sql);
  }
 }

 $allservices="servicesmenu.php";
//echo("<META http-equiv='refresh' content='0;url=$billmenu'>");
header('Location:'.$allservices);
exit;


}
?>	