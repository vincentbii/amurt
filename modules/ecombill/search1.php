<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require('./roots.php');
require($root_path.'include/inc_environment_global.php');

# Define to true to echo the sql query, for debugging 
define('SHOW_SQLQUERY',FALSE);

$lang_tables[]='search.php';
$lang_tables[]='billing.php';
define('LANG_FILE','aufnahme.php');
$local_user='aufnahme_user';
require_once($root_path.'include/inc_front_chain_lang.php');
require_once($root_path.'include/inc_date_format_functions.php');

?>

<head>
<?php echo setCharSet(); ?>

<title><?php echo $LDPatientName ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $root_path.'assets/css/bootstrap.min.css'; ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body topmargin=0 leftmargin=0 marginwidth=0 marginheight=0  onLoad="document.searchform.searchkey.select()" >

<?php
$query = $_GET['query']; 
    // gets value sent over search form
     
    $min_length = 3;
    // you can set minimum length of the query if you want
     
    if(strlen($query) >= $min_length){ // if query length is more or equal minimum length then
         
        $query = htmlspecialchars($query); 
        // changes characters used in html to their equivalents, for example: < to &gt;
         
        $query = mysql_real_escape_string($query);
        // makes sure nobody uses SQL injection
         
        $raw_results = mysql_query("SELECT * FROM care_tz_drugsandservices
            WHERE (`item_description` LIKE '%".$query."%') OR (`item_id` LIKE '%".$query."%') OR (`partcode` LIKE '%".$query."%')") or die(mysql_error());
             
        // * means that it selects all fields, you can also write: `id`, `title`, `text`
        // articles is the name of our table
         
        // '%$query%' is what we're looking for, % means anything, for example if $query is Hello
        // it will match "hello", "Hello man", "gogohello", if you want exact match use `title`='$query'
        // or if you want to match just full word so "gogohello" is out use '% $query %' ...OR ... '$query %' ... OR ... '% $query'
         
        if(mysql_num_rows($raw_results) > 0){ // if one or more rows are returned do following
             
            while($results = mysql_fetch_array($raw_results)){
            // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
             
                // echo "<p><h3>".$results['item_id']."</h3>".$results['item_description']."</p>";
                ?>
<div class="container">
    <table class="table-striped table-bordered" width="60%">
    <tr>
        <td width="25%"><?php echo $results['item_id']; ?></td>
        <td width="25%"><?php echo $results['partcode']; ?></td>
        <td width="25%"><?php echo $results['item_description']; ?></td>
        <td width="25%">
            <a class="btn btn-info btn-xs" href="update_services.php?id=<?php echo $results['item_id']; ?>">Edit</a>
            
                <?php
                if($results['item_status'] == 1){
                    ?><a onclick="confirmDesactiv()" class="btn btn-warning btn-xs deactivate" href="activate_services.php?status=2&id=<?php echo $results['item_id']; ?>">De-activate</a><?php
                }elseif($results['item_status'] == 2){
                    ?><a onclick="confirmDesactiv()" class="btn btn-success btn-xs activate" href="activate_services.php?status=1&id=<?php echo $results['item_id']; ?>">Activate</a><?php
                }
                ?>
                <a onclick="confirmDelete()" class="btn btn-danger btn-xs activate" href="activate_services.php?status=3&id=<?php echo $results['item_id']; ?>">Delete</a>
        </td>
    </tr>
</table>
</div>
                <?php
                // posts results gotten from database(title and text) you can also show id ($results['id'])
            }
             
        }
        else{ // if there is no matching rows do following
            echo "No results";
        }
         
    }
    else{ // if query length is less than minimum
        echo "Minimum length is ".$min_length;
    }
?>

</body>
</html>
<script type="text/javascript">
	function confirmDesactiv()
{
   if(confirm("Are you sure ?"))
     return true;
  
  return false;
}

function confirmDelete(){
    if(confirm("Delete this Item")){
        return true;
    }
    return false;
}
</script>