<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require('./roots.php');
require($root_path.'include/inc_environment_global.php');


if(isset($_POST['submit'])){
  $item_code = $_POST['item_code'];
  $item_description = $_POST['item_description'];
  if($_POST['unit_price'] !== null){
      $unit_price = $_POST['unit_price'];
  }else{
      $unit_price = 0;
  }
  
  $quantity = $_POST['quantity'];
  $purchasing_class = $_POST['purchasing_class'];
  $reorder_level = $_POST['reorder_level'];
  $category = $_POST['category'];
  $is_lab = '';
  if($purchasing_class == 'LABARATORY'){
    $is_lab = '1';
  }else{
    $is_lab = '0';
  }

  if($_POST['unit_price_1'] !== null){
    $unit_price_1 = $_POST['unit_price_1'];
  }else{
    $unit_price_1 = 0;
  }

  
  if($_POST['unit_price_2'] !== null){
    $unit_price_2 = $_POST['unit_price_2'];
  }else{
    $unit_price_2 = 0;
  }

  if($_POST['qty_in_store'] !== null){
    $qty_in_store = $_POST['qty_in_store'];
  }else{
    $qty_in_store = 0;
  }

  // $sql = "INSERT INTO care_tz_drugsandservices (item_number, partcode) values ('".$item_code."', '".$item_code."'')";
  $sql = "INSERT INTO care_tz_drugsandservices (item_number, partcode, item_description, item_full_description, unit_price, purchasing_class, category, reorderlevel, is_labtest, unit_price_1, unit_price_2, item_status) values ('".$item_code."', '".$item_code."', '".$item_description."', '".$item_description."', '".$unit_price."', '".$purchasing_class."', '".$category."', '".$reorder_level."', '".$is_lab."', '".$unit_price_1."', '".$unit_price_2."', 1)";

  $sql2 = "INSERT into care_ke_locstock (loccode, stockid, quantity, qty_in_store, reorderlevel) values ('DISPENS', '".$item_code."', '".$quantity."', '$qty_in_store', '".$reorder_level."')";

  if($db->Execute($sql) && $db->Execute($sql2)){
    $alert = 'Added Successfully!';
  }else{
    $alert = 'Item Not Added! Check the Item CODE';
  }
$allservices="servicesmenu.php";
  ?>
<script type="text/javascript">
  alert('<?php  echo $alert; ?>');
  window.location.href = 'servicesmenu.php';
</script>
  <?php

  
//echo("<META http-equiv='refresh' content='0;url=$billmenu'>");
// header('Location:'.$allservices.URL_REDIRECT_APPEND);

exit;
}

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $root_path.'assets/css/bootstrap.min.css'; ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
    <div class="row">
      <center>
        
      </center>
      <div align="" class="col-xs-12">
        <form name="new_service" action="" method="POST">
      <table width="100%" valign="center">
          <div class="form-group">
          <td>
            <label for="item_code">Item Code</label>
            <input class="input-sm" placeholder="Item Code" required="required" type="text" name="item_code">
          </td>
          <?php
            $sql = "SELECT distinct purchasing_class FROM care_tz_drugsandservices";
            $result1=$db->Execute($sql);
            ?>
            <td>
              <label for="purchasing_class">Purchasing Class</label>
            <select class="input-sm" name="purchasing_class">
              <option selected disabled>-- Select Purchasing Class --</option>
              <?php
              while ($rows = $result1->FetchRow()) {
                ?><option value="<?php echo $rows['purchasing_class']; ?>"><?php echo $rows['purchasing_class']; ?></option><?php
              }
              ?>
            </select>
            </td>


          <td>
            <label for="category">Category</label>
            <select class="input-sm" name="category">
              <option selected disabled>-- Select Drug Category --</option>
              <?php
              $csql = "SELECT id, name FROM care_tz_drug_classes where status = '1'";
              $cresult=$db->Execute($csql);
              while ($crows = $cresult->FetchRow()) {
                ?><option value="<?php echo $crows['id']; ?>"><?php echo $crows['name']; ?></option><?php
              }
              ?>
              <?php
              $sql = "SELECT distinct category FROM care_tz_drugsandservices where category is not null";
              $result1=$db->Execute($sql);
              while ($rows = $result1->FetchRow()) {
                ?><option value="<?php echo $rows['category']; ?>"><?php echo $rows['category']; ?></option><?php
              }
              ?>
            </select>
          </td>
            
          </div>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <div class="form-group">
            <td>
            <label for="item_description">Item Description</label>
            <input placeholder="Item Description" class="input-sm" required="required" type="text" name="item_description">
            </td>
            <td>
              <label for="qty_in_store">Store Quantity</label>
            <input placeholder="Quantity in Store" class="input-sm" required="required" type="number" name="qty_in_store">
            </td>

            <td>
              <label for="quantity">Department Quantity</label>
            <input placeholder="Quantity in the Department" class="input-sm" required="required" type="number" name="quantity">
            </td>
            </div>

        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <div class="form-group">
            <td>
              <label for="reorder_level">Reorder Level</label>
            <input class="input-sm" placeholder="Reorder Level" type="number" name="reorder_level">
            </td>
          
            <td>
              <label for="unit_price">Unit Price</label>
            <input class="input-sm" placeholder=" Unit Price" type="number" name="unit_price">
            </td>

            <td>
              <label for="unit_price_1">Lab Cash Price</label>
            <input class="input-sm" placeholder="Lab Cash Selling Price" type="number" name="unit_price_1">
            </td>
</tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <div class="form-group">
            <td>
              <label for="unit_price_2">Lab Credit Price</label>
            <input placeholder="Lab Credit Selling Price" class="input-sm" type="number" name="unit_price_2">
            </td>
          </div>
          </div>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3">
            <div class="form-group">
            <input class="btn btn-info btn-sm" type="submit" name="submit" value="submit">
          </div>
          </td>
        </tr>
      </table>
    </form>
      </div>
      
    </div>
		
	</div>
</body>
</html>