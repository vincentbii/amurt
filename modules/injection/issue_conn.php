<?php

require_once('roots.php');
require($root_path.'include/inc_environment_global.php');
global $db;
    $pid=$_GET[patientId];
    $prescDate=$_GET[prescDate];
    require("../../include/dhtmlxConnector/codebase/grid_connector.php");
    $gridConn=new GridConnector($db);
    $gridConn->enable_log("temp.log",true);
    $gridConn->dynamic_loading(200);


$accDB=$_SESSION['sess_accountingdb'];
$pharmLoc=$_SESSION['sess_pharmloc'];

if($pharmLoc<>"care2x"){
    $stockDb=$accDB.locstock;
}else{
    $stockDb="care_ke_locstock";
}

    $sql = "SELECT a.nr,b.encounter_nr,b.encounter_class_nr, a.create_time as prescribe_date,a.partcode,b.pid,a.article_item_number,a.article,
            a.dosage,times_per_day,a.days,a.article,
            a.price AS amnt,qty_prescribed AS qty,(round(a.price*(a.qty_prescribed - a.qtyIssued) / 50) * 50) AS total,
            l.quantity,a.qtyIssued as issued, (a.qty_prescribed - a.qtyIssued) as qtyToIssue FROM care_encounter_prescription a
            INNER JOIN  care_encounter b ON a.encounter_nr=b.encounter_nr
            LEFT JOIN care_ke_locstock l on a.partcode=l.stockid
            LEFT JOIN care_ke_stlocation c on l.loccode=c.st_id
            WHERE l.loccode = 'DISPENS' AND b.pid='".$pid."' AND a.drug_class in ('INJECTION') and b.encounter_class_nr=2 AND a.status
            NOT IN('serviced','done','Cancelled') and a.`prescribe_date`='$prescDate'";

       //echo $sql;
      //if(isset($pid))
     //  $sql.=" Where `IP-OP`='1' and pid ='".$pid."'";

    $gridConn->render_sql("$sql","nr", "article_item_number,prescribe_date,partcode,article,dosage,times_per_day,days,quantity,amnt,qty,issued,qtyToIssue,total");

?>
