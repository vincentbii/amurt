<?php

//echo var_dump($_POST);
$debug = false;

require_once($root_path . 'include/care_api_classes/class_tz_Billing.php');
require_once($root_path . 'include/care_api_classes/accounting.php');
require_once($root_path . 'include/care_api_classes/class_tz_insurance.php');
require_once($root_path . 'include/care_api_classes/class_person.php');
$insurance_obj = new Insurance_tz;
$person_obj = new Person;
$bill_obj = new Bill;
($debug) ? $db->debug = FALSE : $db->debug = FALSE;

$issDate = $_POST[issDate];
$issNo = $_POST[issNo];
$pid = $_POST[pid];
$pname = $_POST[pname];
$docId = $_POST[docID];
$docName = $_POST[docName];
$supStoreid = $_POST[storeID];
$supStoreDesc = $_POST[supStoredesc];
$period = date("Y");
$input_user = $_SESSION['sess_login_username'];
$total = $_POST[totalCost];
$enc_no = $_POST[enc_no];
$encClass = $_POST[issType];

//echo var_dump($_POST).'<br><br>';
//echo 'rows ' . $_POST[gridbox_rowsadded].'<br><br>';
//echo 'Update Value is '.$_POST[updatebill].'<br><br>';
//echo 'Submit Value is '.$_POST[submit].'<br><br>';
//echo 'The added Rows are '.$_POST[numRows].'<br><br>';

$insuranceid = $insurance_obj->Get_insuranceID_from_pid($pid);


if($_POST[submit]=='Update'){
        $added_rows=$_POST[numRows];
        $arr_rows= explode(",", $added_rows);
        foreach ($arr_rows as $i) {
          // echo "the Price is ".$_POST["gridbox_".$i."_8"].'<br><br>';

             $dose=$_POST["gridbox_".$i."_4"];
              $times=$_POST["gridbox_".$i."_5"];
               $days=$_POST["gridbox_".$i."_6"];

            $price=$_POST["gridbox_".$i."_8"];
            $partcode=$_POST["gridbox_".$i."_2"];
            $prescNo=$i;
            $total=$_POST["gridbox_".$i."_12"];
            if($_POST["gridbox_".$i."_10"]<>''){
                $qty=$_POST["gridbox_".$i."_10"];
            }else{
                $qty=$_POST["gridbox_".$i."_9"];
            }
           UpdateBillPrices($prescNo,$price,$partcode,$qty,$total,$dose,$times,$days);
        }
}else{
        $sql = "SELECT a.nr,article_item_number,a.encounter_nr,b.encounter_class_nr,a.nr, c.payment, c.payment
                FROM care_encounter_prescription a
                INNER JOIN  care_encounter b
                ON a.encounter_nr=b.encounter_nr 
                INNER JOIN care_tz_drugsandservices c ON a.partcode = c.partcode
                WHERE b.pid='$pid' AND b.encounter_class_nr='$encClass' AND a.drug_class in ('drug_list','Medical-Supplies', 'INJECTION')
                and a.status='pending'";
                $result = $db->Execute($sql);
                if ($debug)
                    echo $sql . '<br>';

                while ($row3 = $result->fetchRow()) {
                    insertData($db, $row3[0], $issNo, $issDate, $pid, $pname, $docId, $docName, $supStoreid, $supStoreDesc,
                                        $period, $total, $input_user, $row3[0],$enc_no,$insurance_obj,$bill_obj, $row3payment);
                    // if($row3[payment] <> 'CASH'){
                    //     if ($encClass == 1 || $insuranceid > 0) {
                    //     //        createPhrmQuote($enc_no);
                    //     $bill_obj->updateFinalBill3($enc_no,$row3[nr]);
                    //     }    
                    // }
                    
                }


}

function updateBillPrices($prescNo,$price,$partcode,$qty,$total,$dose,$times,$days){
    global $db;
    $debug=false;
    global $qty_prescribed;

    $category=GetCategoryOfItem($partcode);

    if($category == 6){
      $qty_prescribed = $dosage;
    }else if($category == 1){
      $qty_prescribed = 1;
    }else{
      $qty_prescribed = $dose * $times * $days;
    }

    $dsql = "UPDATE care_encounter set is_discharged = 1, discharge_date = '".date('Y-m-d')."', discharge_time = '".date('H:i:s')."' where encounter_date < '".date('Y-m-d')."'";
    $dresult = $db->Execute($dsql);

    $sql1="Update care_encounter_prescription set price='$price',dosage='$dose', times_per_day='$times',days='$days', qty_prescribed='$qty_prescribed' where nr='$prescNo'";
    if($debug) echo $sql1;
    $db->Execute($sql1);

    $sql2="Update care_ke_billing set price='$price',qty='$qty',total='$total' where batch_no='$prescNo'";
    if($debug) echo $sql2;
    $db->Execute($sql2);

    // $sql3="Update care_tz_drugsandservices set unit_price='$price' where partcode='$partcode'";
    // if($debug) echo $sql3;
    // $db->Execute($sql3);
}

$enc_no = $person_obj->CurrentEncounter($pid);

if (empty($enc_no)) {
    $enc_no = $person_obj->CurrentMaxEncounter($pid);
}

function insertData($db, $rowid, $issNo, $issDate, $pid, $pname, $docId, $docName, $supStoreid, $supStoreDesc, $period,
                    $total, $input_user, $nr,$enc_no,$encClass,$bill_obj, $row3payment)
{
    $debug = false;
    $itemNumber = $_POST["gridbox_" . $rowid . "_0"];
    $prescribeDate = $_POST["gridbox_" . $rowid . "_1"];
    $itemId = $_POST["gridbox_" . $rowid . "_2"];
    $item_Desc = $_POST["gridbox_" . $rowid . "_3"];
    $qty = $_POST["gridbox_" . $rowid . "_9"];
    $qtytoIssue = $_POST["gridbox_" . $rowid . "_11"];
    $price = $_POST["gridbox_" . $rowid . "_8"];
    $issued=$_POST["gridbox_". $rowid ."_10"];
    // $total = $_POST["gridbox_" . $rowid . "_12"];
    $total = $qtytoIssue * $price;

    // if($issued<>""){
    //     $qtyIssued=$issued;
    // }else{
    //     $qtyIssued=$qty;
    // }
    $qtyIssued = $issued + $qtytoIssue;

    $bal=$qty-$qtyIssued;
    $dsql = "UPDATE care_encounter set is_discharged = 1, discharge_date = '".date('Y-m-d')."', discharge_time = '".date('H:i:s')."' where encounter_date < '".date('Y-m-d')."'";
    $dresult = $db->Execute($dsql);

    //if ($itemNumber == 1) {
    $csql = "INSERT INTO care_ke_internal_orders
                (order_no,STATUS,order_date,order_time,order_type,store_loc,store_desc,adm_no,
                OP_no,patient_name,item_id,Item_desc,qty,price,unit_msr,unit_cost,issued,orign_qty,
                balance,period,input_user,total,presc_nr,weberpsync
                )
                VALUES
                ('$issNo', 'issued','" . $issDate. "','" . date("H:i:s") . "','cash sale','$supStoreid','$supStoreDesc',
                '','$pid','$pname','$itemId', '$item_Desc','$qty','$price','each','$total','$qtytoIssue','$qty',
                '$bal','$period','$input_user','$total','$nr','0' )";

    if ($debug)
        echo $csql . '<br>';
    if($db->Execute($csql)){
        reduceStock($db, $itemId, $supStoreid, $qtyIssued,$qty,$bal, $nr, $issNo, $qtytoIssue);
       // updateStockERP($db,$itemId,'MAIN',$issued,date("d-m-Y"),$nr);
        updateBillERP($encClass,$pid,$enc_no,$bill_obj);

    }

    if($row3payment <> 'CASH'){
                        if ($encClass == 1 || $insuranceid > 0) {
                        //        createPhrmQuote($enc_no);
                        $bill_obj->updateFinalBill3($enc_no,$nr, $qtytoIssue);
                        }    
                    }

}

function updateStockERP($db, $partcode, $supStore, $issueDate,$presc_nr)
{
    $debug=false;
    $weberp_obj = new_weberp();
//    $itemId=$_POST["gridbox_".$rowid."_0"];

    $pSdate = new DateTime($issueDate);
    $pdateS = $pSdate->format('Y-m-d');
    $servDate = $pdateS;
    if ($weberp_obj->stock_adjustment_in_webERP($partcode, $supStore, 'Main', $qty, $servDate) == 'failure') {
        // echo "failed to transmit $row[item_id] to weberp GL<br>";
    } else {
        //echo "transmitted $row[item_id] GLs successfully<br>";
    }

    $accDB=$_SESSION['sess_accountingdb'];
    $sql1 = "SELECT quantity from $accDB.locstock where stockid='$partcode' and loccode='$supStore'";
    $result2 = $db->Execute($sql1);
    if ($debug) echo $sql1 . '<br>';

    $row = $result2->FetchRow();
    $newQty = intval($row[0]) - intval($qty);

    $sql3 = 'SELECT IF(qty_balance>0,qty_balance,(a.qty_prescribed)) AS qty FROM care_encounter_prescription a
        where nr=' . $presc_nr;
    $result3 = $db->Execute($sql3);
    if ($debug)
        echo $sql3 . '<br>';
    $row3 = $result3->FetchRow();
    $oldqty = $row3[0];
    $balance = $oldqty - $qty;


    if($newQty<1){
        $sql="UPDATE care_tz_drugsandservices set item_status='2' where partcode='$partcode'";
        $db->Execute($sql);
    }else{
        $sql="UPDATE care_tz_drugsandservices set item_status='1' where partcode='$partcode'";
        $db->Execute($sql);
    }

    if ($balance > 0) {
        $stat = "pending";
    } else {
        $stat = "serviced";
    }

    $sqlp = 'UPDATE care_encounter_prescription set  status="' . $stat . '",qty_balance="' . $balance
        . '",bill_status="pending"  where
        partcode="' . $partcode . '" AND `status` = "pending" and nr="' . $presc_nr . '"';
    $db->Execute($sqlp);
    if ($debug)
        echo $sqlp . '<br>';

}

function updateBillERP($encClass,$pid,$enc_no,$bill_obj){
    global $db,$insurance_obj;
    $debug=false;

    $insuranceid = $insurance_obj->Get_insuranceID_from_pid($pid);

    if ($encClass == 1 || $insuranceid > 0) {
//        createPhrmQuote($enc_no);
//        $bill_obj->updateFinalBill($enc_no);

        $sql = "SELECT a.OP_no AS pid, a.price,a.item_id AS partcode,a.item_desc AS article,a. order_date AS prescribe_date,a.order_no AS bill_number,
                a.unit_cost AS ovamount,store_loc AS salesArea,d.`category`
                FROM care_ke_internal_orders a LEFT JOIN care_tz_drugsandservices d ON a.`item_id`=d.`partcode`
                WHERE a.Op_no='$pid' and weberpsync=0";
        $result = $db->Execute($sql);
        if ($debug)
            echo $sql;
        //$arr=Array();
        while ($row = $result->FetchRow()) {
            if ($weberp_obj = new_weberp()) {
                if (!$weberp_obj->transfer_bill_to_webERP_asSalesInvoice($row)) {
                    $sql = "UPDATE care_ke_internal_orders set weberpSync=1 where weberpSync=0 and order_no='$row[bill_number]'";
                    //echo $sql;
                    $db->Execute($sql);

                } else {
                    //echo "Failed to transmit item_ID --$row[partcode]--$row[article]  to weberp GL: Check GL Linkage<br>";
                }
                destroy_weberp($weberp_obj);
            }
        }

    }
}

function reduceStock($db, $stockid, $store, $qtyIssued,$OrigQty,$bal, $presc_nr, $order_no, $qtytoIssue)
{
    $debug =false;
//    $sql3 = 'SELECT IF(qty_balance>0,qty_balance,(a.dosage*a.times_per_day*a.days)) AS qty FROM care_encounter_prescription a
//        where nr=' . $presc_nr;
//    $result3 = $db->Execute($sql3);
//    if ($debug)
//        echo $sql3 . '<br>';
//    $row3 = $result3->FetchRow();
//    $oldqty = $row3[0];
//    $balance = $oldqty - $qty;



    $sql4 = 'UPDATE care_ke_internal_orders set orign_qty="' . $OrigQty . '",balance="' . $bal . '"
        where item_id="' . $stockid . '" and order_no="' . $order_no . '"';
    $db->Execute($sql4);
    if ($debug)
        echo $sql4 . '<br>';

    $sql1 = 'SELECT quantity from care_ke_locstock where stockid="' . $stockid . '" and loccode="' . $store . '"';
    $result2 = $db->Execute($sql1);
    if ($debug) echo $sql1 . '<br>';

    $row = $result2->FetchRow();
    $newQty = intval($row[0]) - intval($qtyIssued);


    $sql = 'UPDATE care_ke_locstock set quantity="' . $newQty . '" where stockid="' . $stockid . '" and loccode="' . $store . '"';
    $db->Execute($sql);
    if ($debug) echo $sql . '<br>';

   if ($bal > 0) {
        $stat = "pending";
   } else {
        $stat = "serviced";
   }

//    if($newQty<1){
//        $sql="Update care_tz_drugsandservices set item_status='2' where partcode='$stockid'";
//        $db->Execute($sql);
//    }

    $sqlp = 'UPDATE care_encounter_prescription set  status="' . $stat . '",qty_balance="' . $bal. '",qtyIssued="' . $qtyIssued
        . '",bill_status="pending"  where
        partcode="' . $stockid . '" AND `status` = "pending" and nr="' . $presc_nr . '"';
    $db->Execute($sqlp);
    if ($debug)
        echo $sqlp . '<br>';

}

function GetCategoryOfItem($item_number){
        global $db;
        $debug = false;
        ($debug) ? $db->debug = TRUE : $db->debug = FALSE;
        $sql = "SELECT category FROM care_tz_drugsandservices WHERE partcode = '" . $item_number . "'";
        if ($result = $db->Execute($sql)) {
            if ($result->RecordCount()) {
                $item_array = $result->GetArray();
                while (list($x, $v) = each($item_array)) {
                    $db->debug = FALSE;
                    return $v['category'];
                }
            } else {
                $db->debug = FALSE;
                return 0;
            }
        }
    }

?>



