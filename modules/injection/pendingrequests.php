<?php


$endDate = date("Y-m-d");
$startDate = date("Y-m-d");

if(isset($_POST['preview'])){
  if($_POST['startDate'] <> ''){
    $startDate = $_POST['startDate'];
  }
  if($_POST['endDate']){
    $endDate = $_POST['endDate'];
  }
}
// $patients = PendingRequests($startDate, $endDate);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
require 'sidebar.php';

$sql="SELECT p.pid, p.selian_pid, name_first, name_last, pr.encounter_nr, pr.prescribe_date, 
            p.pid as batch_nr,e.encounter_class_nr FROM care_encounter_prescription pr 
                inner join care_encounter e on pr.encounter_nr = e.encounter_nr 
                and (pr.status='pending' OR pr.status='') 
                inner join care_person p on e.pid = p.pid 
                inner join care_tz_drugsandservices d on pr.article_item_number=d.item_id 
                where (pr.drug_class = 'INJECTION' OR d.category = '9' OR d.purchasing_class ='INJECTION' and pr.article not like '%consult%') group by e.encounter_class_nr ,pr.prescribe_date, pr.encounter_nr, p.pid,
                p.selian_pid, name_first, name_last
                having datediff(now(),pr.prescribe_date)<7 ORDER BY pr.prescribe_date ASC";
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pending Requests
      </h1>    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-3">
          	<div class="box">
            <!-- /.box-header -->
            	<div class="box-body">
                <?php


if (!isset($tracker) || !$tracker)
    $tracker = 1;


$tracker = 1;
echo "<br><br>";

$send_date = "";


                if($requests=$db->Execute($sql)){
  if ($requests->RecordCount()>0){
    while ($test_request = $requests->FetchRow()) {
    if ($debug)
        echo $tracker . "<br>";
    list($buf_date, $x) = explode(" ", $test_request['prescribe_date']);
    if ($buf_date != $send_date) {
        echo "<FONT size=2 color=\"#990000\"><b>" . formatDate2Local($buf_date, $date_format) . "</b></font><br>";
        $send_date = $buf_date;
    }
    if ($debug)
        echo "Batch_nr=" . $batch_nr . " -- test_request['batch_nr']=" . $test_request['batch_nr'] . "<br>";
    if ($debug)
        echo "prescription_date=" . $prescription_date . " -- test_request['prescribe_date'=" . $test_request['prescribe_date'] . "<br>";
    if ($debug)
        echo "Patient number:" . $pn . "<br>";
    if ($batch_nr != $test_request['batch_nr'] || $prescription_date != $test_request['prescribe_date']) {
        echo "<img src=\"" . $root_path . "gui/img/common/default/pixel.gif\" border=0 width=4 height=7>
        <a onmouseover=\"showBallon('" . $test_request['name_first'] . " " . $test_request['name_last'] . 
                " encounter: " . $test_request['encounter_nr'] . " Hospital file nr: " . $test_request['selian_pid'] . 
                "',0,150,'#99ccff'); window.status='Care2x Tooltip'; return true;\"
  onmouseout=\"hideBallon(); return true;\" href=\"" . $thisfile . URL_APPEND . "&target=" . $target . 
                "&subtarget=" . $subtarget . "&batch_nr=" . $test_request['batch_nr'] . 
                "&prescription_date=" . $test_request['prescribe_date'] . "&pn=" . $test_request['encounter_nr'] . 
                "&user_origin=" . $user_origin . "&tracker=" . $tracker . "&back_path=" . $back_path . "\">";

        if ($test_request['batch_nr']) {
            //echo $test_request['selian_pid'].'/'.$test_request['name_first']." ".$test_request['name_last'];
            echo $test_request['pid'];
        }
        echo " " . $test_request['room_nr'] . "/" . $test_request['name_first'] . " " . $test_request['name_last'] . "</a><br>";
    } else {
        echo "<img " . createComIcon($root_path, 'redpfeil.gif', '0', '', TRUE) . "> <FONT size=1 color=\"red\">";
        if ($test_request['batch_nr']) {
            echo $test_request['pid'];
        }
        echo " " . $test_request['room_nr'] . "</font><br>";
        $track_item = $tracker;
    }


    $tracker++;
}
  }else{
    echo "No Request";
  }
}

?>
            	</div>
        	</div>
    	</div>
        <div class="col-xs-9">
            <div class="box">
            <!-- /.box-header -->
              <div class="box-body">
                <?php
                if(isset($_GET['pn'])){
                  $pn = $_GET['pn'];
                }
                else{
                  $pn = $pn;
                }
                
                $patientData = getPatient($pn);
                $patientPrescription = getPrescription($pn);
                $prescription = json_decode($patientPrescription, true);
                ?>
                <table width="100%" cellpadding="0" cellspacing="1">
                  <tr>
                    <td colspan="8" style="font-size: 20px; color: darkblue" class="text-bold"><label>PID:</label><?php echo $patientData['pid']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td><label>Hospital File No.</label></td>
                    <td class="text-left"><?php echo $patientData['fileno']; ?></td>
                    <td><label>Name</label></td>
                    <td class="text-left"><?php echo $patientData['name']; ?></td>
                    <td><label>Date Of Birth</label></td>
                    <td class="text-left"><?php echo $patientData['dob']; ?></td>
                    <td><label>Sex</label></td>
                    <td class="text-left"><?php echo $patientData['sex']; ?></td>
                  </tr>
                </table>
                  <?php
if($prescription !== null){
  ?>
<table class="table-condensed table-striped" width="100%" cellspacing="1" cellpadding="0">
                  <thead>
                    <tr>
                      <th>Prescription Date</th>
                      <th>Encounter Nr</th>
                      <th>Days</th>
                      <th>Description</th>
                      <th>Dosage</th>
                      <th>Times Per Day</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                       foreach ($prescription as $key => $value) {
                                      ?>
                                      <tr>
                                        <td><?php echo $value['InputDate']; ?></td>
                                        <td><?php echo $value['EncounterNo']; ?></td>
                                        <td><?php echo $value['Days']; ?></td>
                                        <td><?php echo $value['Description']; ?></td>
                                        <td><?php echo $value['Dosage']; ?></td>
                                        <td><?php echo $value['TimesPerDay']; ?></td>
                                      </tr>
                                      <?php
                                    }             
                  ?>
                  <tr>
                    <td class="text-right" colspan="6">
                      <a href="issue.php?enr=<?php echo $patientData['enr']; ?>" class="btn btn-primary btn-sm">Issue</a>
                    </td>
                  </tr>
                  </tbody>
                </table>
  <?php
}else{
  echo "<h4>No Pending Requests Available</h4>";
  
}
                  ?>
              </div>
            </div>
          </div>
        </div>
</section>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#firstvisits').DataTable();
	} );
</script>
<script type="text/javascript">
  $(document).on('click', '.edit_data', function(){  
           var employee_id = $(this).attr("id");  
           $.ajax({  
                url:"fetch.php",  
                method:"POST",  
                data:{employee_id:employee_id},  
                dataType:"json",  
                success:function(data){  
                     $('#name').val(data.name);  
                     $('#address').val(data.address);  
                     $('#gender').val(data.gender);  
                     $('#designation').val(data.designation);  
                     $('#age').val(data.age);  
                     $('#employee_id').val(data.id);  
                     $('#insert').val("Update");  
                     $('#add_data_Modal').modal('show');  
                }  
           });  
      });  

</script>
<script type="text/javascript">
  function IssuePrescriptions(issue){
    // var pn = document.getElementById('issue').value;
    // alert(issue);

    window.location = 'issue.php?pid=' + issue;
  }
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</body>
</html>
