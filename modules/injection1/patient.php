<?php

include 'functions.php';
if(isset($_GET['pid'])){
  $pid = $_GET['pid'];
}else{
  $pid = '';
}

$patient = getPatient($pid);
$prescriptions = getPrescription($pid);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php
require 'sidebar.php';


  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patient Details
      </h1>   
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            <div class="box-header">
              <table><tr>
                <td></td>
              </tr></table>
            </div>
            	<div class="box-body">
                <?php
                var_dump($prescriptions, true);
                $pa = json_decode($prescriptions, true);

                ?>
                <div class="panel panel-heading">
                  <table width="60%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>PID</th>
                      <th>File No</th>
                      <th>Date of Birth</th>
                      <th>Sex</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    <td><?php echo $patient['name']; ?></td>
                    <td><?php echo $patient['pid']; ?></td>
                    <td><?php echo $patient['fileno']; ?></td>
                    <td><?php echo $patient['dob']; ?></td>
                    <td><?php echo $patient['sex']; ?></td>
                  </tr>
                  </tbody>
                </table>
                </div>
                <?php
                if($pa ==! null){

                  ?>
                  <div class="text-right">
                    <button id="issue" onclick="IssuePrescriptions(<?php echo $patient['pid']; ?>)" class="btn-success">Issue Prescriptions</button>
                  </div>
                  <table width="100%" class="panel panel-body table-striped">
                    <thead>
                      <tr>
                        <th>Prescription Date</th>
                        <th>Encounter Nr</th>
                        <th>Days</th>
                        <th>Description</th>
                        <th>Dosage</th>
                        <th>Times Per Day</th>
                        
                      </tr>
                    </thead>
                    <?php
                    foreach ($pa as $key => $value) {
                      ?>
                      <tr>
                              <td><?php echo $value['InputDate']; ?></td>
                              <td><?php echo $value['EncounterNo']; ?></td>
                              <td><?php echo $value['Days']; ?></td>
                              <td><?php echo $value['Description']; ?></td>
                              <td><?php echo $value['Dosage']; ?></td>
                              <td><?php echo $value['TimesPerDay']; ?></td>
                              
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                  <?php
                }else{
                  ?>
  <h4 class="text-bold">No Dental Prescriptions for this patient</h4>
  <a href="">View History</a>
                  <?php
                }
                ?>
            	</div>
        	</div>
    	</div>
	</div>
</section>
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#firstvisits').DataTable();
	} );
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript">
  function IssuePrescriptions(issue){
    // var pn = document.getElementById('issue').value;
    // alert(issue);

    window.location = 'issue.php?pid=' + issue;
  }
</script>
</body>
</html>
