<?php
$sql="SELECT p.pid, p.selian_pid, name_first, name_last, pr.encounter_nr, pr.prescribe_date, 
            p.pid as batch_nr,e.encounter_class_nr FROM care_encounter_prescription pr 
                inner join care_encounter e on pr.encounter_nr = e.encounter_nr 
                and (pr.status='pending' OR pr.status='') 
                inner join care_person p on e.pid = p.pid 
                inner join care_tz_drugsandservices d on pr.article_item_number=d.item_id 
                where (d.category = '9' OR d.purchasing_class ='INJECTION' and pr.article not like '%consult%') group by e.encounter_class_nr ,pr.prescribe_date, pr.encounter_nr, p.pid,
                p.selian_pid, name_first, name_last
                having datediff(now(),pr.prescribe_date)<7 ORDER BY pr.prescribe_date ASC";
  ?>
  <div class="content">
    <div class="row">
        <div class="col-xs-3">
            <div class="box">
              <div class="box-body">
                <?php
                if (!isset($tracker) || !$tracker)
                  $tracker = 1;
                echo "<br><br>";
                $send_date = "";

                $PatientNo = '';
                if($requests=$db->Execute($sql)){
                  if ($requests->RecordCount()>0){
                    while ($test_request = $requests->FetchRow()) {
                      $PatientNo = $test_request['encounter_nr'];
                      if ($debug)
                        echo $tracker . "<br>";
                      list($buf_date, $x) = explode(" ", $test_request['prescribe_date']);
                      if ($buf_date != $send_date) {
                        echo "<FONT size=2 color=\"#990000\"><b>" . formatDate2Local($buf_date, $date_format) . "</b></font><br>";
                        $send_date = $buf_date;
                      }
                      if ($debug)
                        echo "Batch_nr=" . $batch_nr . " -- test_request['batch_nr']=" . $test_request['batch_nr'] . "<br>";
                      if ($debug)
                        echo "prescription_date=" . $prescription_date . " -- test_request['prescribe_date'=" . $test_request['prescribe_date'] . "<br>";
                      if ($debug)
                        echo "Patient number:" . $pn . "<br>";
                      if ($batch_nr != $test_request['batch_nr'] || $prescription_date != $test_request['prescribe_date']) {
                        echo "<img src=\"" . $root_path . "gui/img/common/default/pixel.gif\" border=0 width=4 height=7>
                        <a onmouseover=\"showBallon('" . $test_request['name_first'] . " " . $test_request['name_last'] . " encounter: " . $test_request['encounter_nr'] . " Hospital file nr: " . $test_request['selian_pid'] . "',0,150,'#99ccff'); window.status='Care2x Tooltip'; return true;\"onmouseout=\"hideBallon(); return true;\" href=\"" . $thisfile . URL_APPEND . "&target=" . $target . "&subtarget=" . $subtarget . "&batch_nr=" . $test_request['batch_nr'] . "&prescription_date=" . $test_request['prescribe_date'] . "&pn=" . $test_request['encounter_nr'] . "&user_origin=" . $user_origin . "&tracker=" . $tracker . "&back_path=" . $back_path . "\">";
                      if ($test_request['batch_nr']) {
            //echo $test_request['selian_pid'].'/'.$test_request['name_first']." ".$test_request['name_last'];
                        echo $test_request['pid'];
                      }
                      echo " " . $test_request['room_nr'] . "/" . $test_request['name_first'] . " " . $test_request['name_last'] . "</a><br>";
                    } else {
                      echo "<img " . createComIcon($root_path, 'redpfeil.gif', '0', '', TRUE) . "> <FONT size=1 color=\"red\">";
                      if ($test_request['batch_nr']) {
                        echo $test_request['pid'];
                      }
                      echo " " . $test_request['room_nr'] . "</font><br>";
                      $track_item = $tracker;
                    }
                    $tracker++;
                  }
                }else{
                  echo "No Pending Request";
                }
              }
              ?>
              </div>
            </div>
          </div>
          <div class="col-xs-9">
            <div class="box">
            <!-- /.box-header -->
              <div class="box-body">
                <?php
                if(isset($_GET['pn'])){
                  $pn = $_GET['pn'];
                }
                else{
                  $pn = $pn;
                }
                
                $patientData = getPatient($pn);
                $patientPrescription = getPrescription($pn);
                $prescription = json_decode($patientPrescription, true);
                ?>
                <table width="100%" cellpadding="0" cellspacing="1">
                  <tr>
                    <td colspan="8" style="font-size: 20px; color: darkblue" class="text-bold"><label>PID:</label><?php echo $patientData['pid']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td><label>Hospital File No.</label></td>
                    <td class="text-left"><?php echo $patientData['fileno']; ?></td>
                    <td><label>Name</label></td>
                    <td class="text-left"><?php echo $patientData['name']; ?></td>
                    <td><label>Date Of Birth</label></td>
                    <td class="text-left"><?php echo $patientData['dob']; ?></td>
                    <td><label>Sex</label></td>
                    <td class="text-left"><?php echo $patientData['sex']; ?></td>
                  </tr>
                </table>
                  <?php
if($prescription !== null){
  ?>
<table class="table-condensed table-striped" width="100%" cellspacing="1" cellpadding="0">
                  <thead>
                    <tr>
                      <th>Prescription Date</th>
                      <th>Encounter Nr</th>
                      <th>Days</th>
                      <th>Description</th>
                      <th>Dosage</th>
                      <th>Times Per Day</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                       foreach ($prescription as $key => $value) {
                                      ?>
                                      <tr>
                                        <td><?php echo $value['InputDate']; ?></td>
                                        <td><?php echo $value['EncounterNo']; ?></td>
                                        <td><?php echo $value['Days']; ?></td>
                                        <td><?php echo $value['Description']; ?></td>
                                        <td><?php echo $value['Dosage']; ?></td>
                                        <td><?php echo $value['TimesPerDay']; ?></td>
                                      </tr>
                                      <?php
                                    }             
                  ?>
                  <tr>
                    <td class="text-right" colspan="6">
                      <a href="issue.php?enr=<?php echo $patientData['enr']; ?>" class="btn btn-primary btn-sm">Issue</a>
                    </td>
                  </tr>
                  </tbody>
                </table>
  <?php
}else{
  echo "<h4>No Pending Requests Available</h4>";
  
}
                  ?>
              </div>
            </div>
          </div>
        </div>
      </div>
