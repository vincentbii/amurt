<?php

include 'functions.php';

$endDate = date("Y-m-d");
$startDate = date("Y-m-d");

if(isset($_POST['preview'])){
  if($_POST['startDate'] <> ''){
    $startDate = $_POST['startDate'];
  }
  if($_POST['endDate']){
    $endDate = $_POST['endDate'];
  }
}
$Services = allServices();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
require 'sidebar.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Services
      </h1>    </section>

      <div></div>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          	<div class="box">
            <!-- /.box-header -->
            	<div class="box-body">
                <div id="" class="table-responsive">
                  <table id="dentalServices" class="table table-striped table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Service ID</th>
                      <th>Service CODE</th>
                      <th>Service Description</th>
                      <th>Unit Price</th>
                      <th>Quantity</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                $Service = json_decode($Services, true);
                foreach ($Service as $key => $value) {
                  $key = $key + 1;
                  echo "<tr>";
                  echo "<td>".$key."</td>";
                  echo "<td>".$value['item_id']."</td>";
                  echo "<td>".$value['partcode']."</td>";
                  echo "<td>".$value['Description']."</td>";
                  echo "<td>".$value['unit_price']."</td>";
                  echo "<td>".$value['qty']."</td>";
                  ?><td>
                    <!-- <input class="btn btn-info btn-xs edit_data" value="Edit" type="button" name="edit" id="<?php echo $value['item_id']; ?>"> -->
                    <button type="button" name="Edit" id="<?php echo $value['partcode']; ?>" class="btn btn-info btn-xs edit_data">Edit</button>
                    <button type="button" name="Delete" id="<?php echo $value['partcode']; ?>" class="btn btn-danger btn-xs delete_data"><i class="glyphicon glyphicon-trash"></i>Delete</button>
                  </td>
                   <?php

                  echo "</tr>";
                }
                ?>
                  </tbody>
                </table>
                </div>

            	</div>
        	</div>
    	</div>
	</div>
</section>
    <!-- /.content -->
  </div>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<!-- <script src="bower_components/jquery/dist/jquery.min.js"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js "></script> -->

<script type="text/javascript">
	$(document).ready(function() {
    	$('#dentalServices').DataTable({
        "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [
        {
                text: 'Add Service',
                action: function ( e, dt, node, config ) {
                    $('#add_Modal').modal('show');
                }
            },
           { 
              extend: 'copyHtml5',
              title: 'Injection Drugs and Services',
              exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
              footer: true },
            { 
              extend: 'excelHtml5', 
              title: 'Injection Drugs and Services',
              exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
              footer: true,

            },
            { 
              extend: 'csvHtml5', 
              title: 'Injection Drugs and Services',
              exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                },
              footer: true },
            { 
              extend: 'pdfHtml5', 
              title: 'Injection Drugs and Services',
              footer: true,
              exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            }
        ]
      });
	} );
</script>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
</body>
</html>
<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Services Details</h4>  
                </div>  
                <div class="modal-body" id="ServicesDetails">  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
 <div id="edit_modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Update Service</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="update_form">
                          <label>Enter Description</label>  
                          <input type="text" name="item_description" id="item_description" class="form-control" />  
                          <br />  
                          <label>Enter Unit Price</label>  
                          <input type="number" name="unit_price" id="unit_price" class="form-control" />  
                          <br /> 
                          <label>Enter Quantity</label>  
                          <input type="text" name="quantity" id="quantity" class="form-control" />  
                          <br />  
                          <input type="hidden" name="partcode" id="partcode" />  
                          <input type="submit" name="update" id="update" value="Update" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
 <div id="add_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Add Service</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="insert_form">
                          <label>Enter Service Code</label>  
                          <input type="text" name="service_code" id="service_code" class="form-control" />  
                          <br /> 
                          <label>Enter Descripton</label>  
                          <input type="text" name="item_description" id="item_description" class="form-control" />  
                          <br />  
                          <label>Enter Unit Price</label>  
                          <input type="number" name="unit_price" id="unit_price" class="form-control" />  
                          <br /> 
                          <label>Enter Quantity</label>  
                          <input type="number" name="quantity" id="quantity" class="form-control" />  
                          <br />  
                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
<script type="text/javascript">
  $(document).on('click', '.edit_data', function(){  
           var partcode = $(this).attr("id");  
           $.ajax({  
                url:"fetchServices.php",  
                method:"POST",  
                data:{partcode:partcode},  
                dataType:"json",  
                success:function(data){  
                    $('#partcode').val(data.partcode);
                     $('#item_description').val(data.item_description);  
                     $('#unit_price').val(data.unit_price);  
                     $('#quantity').val(data.quantity);
                     $('#insert').val("Update");  
                     $('#edit_modal').modal('show');  
                }  
           });  
      });

  

  $('#insert_form').on("submit", function(event){  
           event.preventDefault();  
           
                $.ajax({  
                     url:"insertServices.php",  
                     method:"POST",  
                     data:$('#insert_form').serialize(),  
                     beforeSend:function(){  
                          $('#insert').val("Inserting...");  
                     },  
                     success:function(data){  
                          $('#insert_form')[0].reset();  
                          $('#add_Modal').modal('hide');
                          $('#dentalServices').load(location.href + " #dentalServices");
                     }  
                });
      });

  $('#update_form').on("submit", function(event){  
           event.preventDefault();  
           if($('#item_description').val() == "")  
           {  
                alert("Item Description is required");  
           }  
           else if($('#unit_price').val() == '')  
           {  
                alert("Unit Price is required");  
           }  
           else if($('#quantity').val() == '')  
           {  
                alert("Quantity is required");  
           }
           else  
           {  
                $.ajax({  
                     url:"updateServices.php",  
                     method:"POST",  
                     data:$('#update_form').serialize(),  
                     beforeSend:function(){  
                          $('#update').val("Updating...");  
                     },  
                     success:function(data){  
                          $('#update_form')[0].reset();  
                          $('#edit_modal').modal('hide');
                          $('#dentalServices').load(location.href + " #dentalServices");
                     }  
                });  
           }  
      });
</script>


<script type="text/javascript">
  $(document).on('click', '.delete_data', function(e){  
        e.preventDefault();
        var item_id = $(this).attr("id");  
        var parent = $(this).parents("tr").attr("id");
        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
               url: 'deleteService.php',
               type: 'POST',
               data: {item_id: item_id},
               error: function() {
                  alert('Something is wrong');
               },
               success: function(data) {
                    $('#dentalServices').load(location.href + " #dentalServices");
                    alert(data);  
               } 
                
            });
        }
      });

  $(document).on('click', '.delte_data', function(e){
    e.preventDefault();
        var id = $(this).parents("tr").attr("id");

        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
               url: '/deleteService.php',
               type: 'POST',
               data: {item_id: item_id},
               error: function() {
                  alert('Something is wrong');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Record removed successfully");  
               }
            });
        }
    });
</script>