<?php
require_once('roots.php');
require($root_path . 'include/inc_environment_global.php');
require_once($root_path . 'include/care_api_classes/class_tz_insurance.php');
require_once($root_path . 'include/care_api_classes/class_encounter.php');
require_once($root_path . 'include/care_api_classes/class_tz_billing.php');
$insurance_obj = new Insurance_tz;
$bill_obj = new Bill;
$enc_obj=new Encounter;

$desc = $_GET[rev];

$caller = $_REQUEST['caller'];

$pid = $_REQUEST['pid'];
$ID=$_REQUEST['ID'];
$table=$_REQUEST['table'];

$billNumber = $_REQUEST['billNumber'];

$date1 = new DateTime($_REQUEST[startDate]);
$startDate = $date1->format("Y-m-d");

$date2 = new DateTime($_REQUEST[endDate]);
$endDate = $date2->format("Y-m-d");

$claimNo=$_REQUEST['claimNo'];
$bill_number=$_POST['bill_number'];

switch ($caller) {
    case "debit":
        getPNames($pid);
        break;
    case "grid":
        getDescription($desc);
        break;
    case "getBillNumbers":
        getBillNumbers($pid);
        break;
    case "finalize":
        finalizeInvoice($db, $pid, $billNumber, $fdate);
        break;
    case "getNewReceipt":
        getNewReceipt();
        break;
    case "nhif":
        getNHIFCredits($dt1, $dt2);
        break;
    case "closeBill":
        closeBill($insurance_obj,$bill_obj);
        break;
    case "deleteClaim":
        deleteClaim($claimNo,$pid,$bill_number);
        break;
    case "getBills":
        getBills($pid);
        break;
    case "getItemsList":
        getItemsList();
        break;
    case "saveDebit":
        saveDebits($enc_obj);
        break;
    case "getEncounter":
        getEncounter($enc_obj,$pid);
        break;
    case "getBillNumbers":
        getBillNumbers($pid);
        break;
    case "getEncounterNumbers":
        getEncounterNumbers($pid);
        break;
    case "combineBills":
        combineBills($pid);
        break;
    case "deleteBillItem":
        deleteBillItem($pid);
        break;
    case "updateBillItems":
        updateBillItems($_POST);
        break;
    case "getAllBills":
        getAllBills($pid);
        break;
    case "checkFinaliseStatus":
        checkFinaliseStatus($pid,$bill_obj,$billNumber);
        break;
    case "DeleteItems":
        deleteMethod($ID,$table);
        break;
    default:
        echo "{failure:true}";
        break;
}

function deleteMethod($ID,$table){
    global $db;
    $debug=false;


    $sql="Delete from $table where ID='$ID'";
    if($debug){echo $sql; }

    if($db->Execute($sql)){
        echo '{Success:true}';
    }else{
        echo '{Failure:true}';
    }

}

function checkFinaliseStatus($pid,$bill_obj,$billNumber){
    global $db;
    $debug=false;

    $encNr=$bill_obj->getLastEncounterNo($pid);

    $sql="SELECT DISTINCT b.pid,b.encounter_nr,b.bill_number, e.finalised FROM care_encounter e LEFT JOIN care_ke_billing b
 ON e.encounter_nr=b.encounter_nr WHERE e.pid='$pid' and b.bill_number=$billNumber and e.encounter_class_nr=1";

    if($debug) echo $sql;

    if($request=$db->Execute($sql)){
        $row=$request->FetchRow();
        echo $row[finalised].','.$row[pid].','.$row[encounter_nr].','.$row[bill_number];
    }
}

function updateBillItems($strData,$pid) {
    global $db;
    $debug = false;
   $UpdateRowsCount=$_POST[selectedCount];

    $error=0;
        if($UpdateRowsCount>1){
            foreach ($strData as $key=>$value) {
                //echo "<br> Items are in group ". $key;
                $sql="UPDATE care_ke_billing SET ";
                foreach($value as $k=>$strVal){
                    //echo "<br> Items in second loop  $k and Value $strVal";
                    if($k=='ID'){
                        $id=$strVal;
                    } 
                     $sql .= $k . '="' . $strVal . '", ';
                       foreach($strVal as $x=>$xval){
                            //echo "<br> Items in third loop  $x and Value $xval";
                        }
                }
                $sql = substr($sql, 0, -2) . " WHERE ID='$id'";
                  
                  if ($debug) echo $sql;
                  
                  if($db->Execute($sql)){
                      $error=0;
                  }else{
                      $error=1;
                  }
            }
        }else{
            $sql="UPDATE CARE_KE_BILLING SET ";
            $id='';
            foreach ($strData as $key=>$value) {
                     $sql .= $key . '="' . $value . '", ';
                     if($key=='ID'){
                         $id=$value;
                     }
                     if($key=='Bill_Date'){
                         $date1 = new DateTime($value);
                         $value = $date1->format("Y-m-d");
                     }
            }
            $sql = substr($sql, 0, -2) . " WHERE ID='$id'";
              if ($debug) echo $sql;
            if($db->Execute($sql)){
                $error=0;
            }else{
                $error=1;
            }
        }
    if ($error==0) {
        $results = "{success: true,'Error':'Successfully saved Bill'}";
//        getAllBills($pid);
    } else {
        $results = "{failure: true,'Error':'Cannot update bill Error No $error'}"; // Return the error message(s)
    }
    echo $results;
}

function deleteBillItem($pid){
    global $db;
    $debug=false;
    
    $ID=$_REQUEST[ID];
    
    $sql="Delete from care_ke_billing where ID=$ID and pid='$pid'";
    if($debug) echo $sql;
    
    if($db->Execute($sql)){
        echo "{success:true,'Error':'Successfully Deleted Item'}";
    }else{
        echo "{failure:true,'Error':'Unable to Delete Item $sql'}";
    }
   
}

function combineBills($pid){
    global $db;
    $debug=false;
    
    $bill1=$_REQUEST[bill1];
    $bill2=$_REQUEST[bill2];
    $enc1=$_REQUEST[enc1];
    $enc2=$_REQUEST[enc2];
    
    $sql="update care_ke_billing set bill_number='$bill1',encounter_nr='$enc1' "
            . "where bill_number='$bill2' and pid='$pid'";
    if($debug) echo $sql;
    
    if($db->Execute($sql)){
        echo "{success:true}";
    }else{
        echo "{failure:true}";
    }
   
}

function getEncounter($enc_obj,$pid){
    $encounterJson=$enc_obj->getCurrentEncounter($pid);

    echo $encounterJson;

}


function saveDebits($enc_obj,$pid){
    global $db;
    $debug=false;

    $billDate=$_REQUEST[debitDate];

    $date1 = new DateTime($_REQUEST[debitDate]);
    $debitDate = $date1->format("Y-m-d");

    $inputUser=$_SESSION['sess_login_username'];

    $debitData=$_REQUEST[gridData];
    $data = json_decode($debitData, true);

    $encounterJson=$enc_obj->getCurrentEncounter($pid);
    $encounters=json_decode($encounterJson);

//    $names=$encounters->FirstName ." ".$encounters->LastName." ".$encounters->SurName;
    $encounterNr=$encounters->encounterNr->EncounterNr;
    $billNumber=$encounters->encounterNr->BillNumber;
    $ward=$encounters->encounterNr->Ward;


    foreach ($data as $row){
        $partcode = $row['PartCode'];
        $description = $row['Description'];
        $serviceType = $row['ServiceType'];
        $price = $row['Price'];
        $qty = $row['Qty'];
        $Total = $row['Total'];

        $sql = "INSERT care_ke_billing(encounter_nr,pid,bill_number,bill_date,service_type,item_number,
            Description,price,qty,total,`status`,`IP-OP`,prescribe_date,weberpSync,partcode,
            bill_time,ledger,insurance_id,batch_no,input_user,rev_code,debtorUpdate)
            values('" . $encounterNr . "','" . $pid . "','" . $billNumber . "','" .$debitDate
            . "','".$serviceType."','" . $partcode . "','" . $description . "','" . $price
            . "','" . $qty . "','" . $Total . "','pending','1','" . $debitDate
            . "',0,'" . $partcode . "','" . date('H:i:s') . "','"
            . "','db','$billNumber','".$inputUser. "','".$partcode."',0)";
        if ($debug) echo $sql;

        if($db->Execute($sql)){
            echo "{success:true}";
        }else{
            echo "{failure:true}";
        }
    }

}

function getItemsList(){
    global $db;
    $debug=false;

    $sql="SELECT d.`partcode`,d.`item_description`,c.`item_Cat`,d.`unit_price` FROM care_tz_drugsandservices d 
            LEFT JOIN care_tz_itemscat c ON d.`category`=c.`catID` where d.purchasing_class not in('drug_list','medical-supplies')";

    if($debug) echo $sql;

    $result=$db->Execute($sql);
    $total=$result->RecordCount();
    echo '{
    "total":"' . $total . '","items":[';
    $counter=0;
    while($row=$result->FetchRow()){
        $description=preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[item_description]);

        echo '{"PartCode":"' . $row[partcode] .'","Description":"' . $description
            .'","Category":"' . $row[item_Cat-OP].'","Price":"' . $row[unit_price].'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function getBills($pid){
//    var_dump($_POST);
    $json_data = $_POST[updatedBills]; // file_get_contents('php://input');
    $strData = json_decode($json_data);   

    if(!empty($strData)){
        updateBillItems($strData,$pid);
    }else{
        getAllBills($pid);
    }
}

function getAllBills($pid){
    global $db;
   $debug=false;

   $pid2=($pid<>"" ? $pid=$pid : $pid='1000');
    
    $sql="SELECT ID,`pid`,`encounter_nr`,`insurance_id`,`IP-OP` as IPOP,`bill_date`,`bill_time`, `bill_number`, `batch_no`,`service_type`,
          `partcode`,`Description`,`price`,`qty`,`total`,`input_user`,
          `notes` FROM `care_ke_billing` where pid='$pid2'";

    if($debug) echo $sql;

    $result=$db->Execute($sql);
    $total=$result->RecordCount();
    echo '{"bills":[';
    $counter=0;
    while($row=$result->FetchRow()){
        echo '{"ID":"' . $row[ID] .'","Pid":"' . $row[pid] .'","Encounter_Nr":"' . $row[encounter_nr] .'","IP-OP":"' . $row[IPOP].'","Bill_Date":"' . $row[bill_date]
            .'","Bill_Time":"' . $row[bill_time] .'","Bill_Number":"' . $row[bill_number] .'","Service_Type":"' . $row[service_type]
            .'","PartCode":"' . $row[partcode] .'","Description":"' . $row[Description].'","Price":"' . number_format($row[price],2).'","Qty":"' . $row[qty]
            .'","Total":"' . number_format($row[total],2).'","InputUser":"' . $row[input_user].'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function deleteClaim($claimNo,$pid,$bill_number){
    global $db;
    
    
     $sql3="DELETE FROM CARE_KE_debtortrans WHERE PID='$pid' and bill_number='$bill_number' and accno in('NHIF','NHIF2')";
    if($db->Execute($sql3)){
        $sql2="DELETE FROM CARE_KE_billing WHERE PID='$pid' and batch_no='$claimNo' and service_type='NHIF'";
        if($db->Execute($sql2)){
           $sql="DELETE FROM CARE_KE_NHIFCREDITS WHERE admno='$pid' and creditno='$claimNo'";
                if($db->Execute($sql)){
                    echo "Successfully Removed NHIF Credit for PID $pid";
                }else{
                    echo 'Failure Delete Credit:'.$sql;
                }
        }else{
            echo 'Failure Delete bill:'.$sql2;
        }
    }else{
        echo 'Failure Delete Credit:'.$sql3;
    }
}

function closeBill($insurance_obj,$bill_obj){
   // global $db;
    $pid=$_REQUEST[pid];
    $encounterNo=$_REQUEST[enc_nr];
    $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
    $bill_obj->updateDebtorsTrans($pid,$insuCompanyID,$encounterNo);
}

function getNHIFCredits($dt1, $dt2) {
    global $db;
    $debug = false;

    $sql = "SELECT b.creditNo,b.inputDate,b.admno,b.NAMES,b.admDate,b.disDate,b.wrdDays,b.nhifNo,b.nhifDebtorNo,
	b.debtorDesc, b.invAmount,b.totalCredit,b.balance,n.bill_number,b.inputUser
	FROM care_ke_nhifcredits b left join care_ke_billing n on b.creditno=n.batch_no
    WHERE n.rev_code='NHIF' ";

    if ($dt1 && $dt2) {
        $date1 = new DateTime(date($dt1));
        $dt1 = $date1->format("Y-m-d");

        $date2 = new DateTime(date($dt2));
        $dt2 = $date2->format("Y-m-d");

        $sql = $sql . "and b.inputDate between '$dt1' and '$dt2'";
    }
    $result = $db->Execute($sql);
    if ($debug)
        echo $sql;

    $out = $out . '<table width=100%><tbody>
                    <tr>
                        <td colspan=14 align=center class="pgtitle">NHIF Credits</td>
                     </tr>
                     <tr>
                        <td colspan=14 align=center id="msg" class="myMessage"></td>
                     </tr>
                     <tr>
                        <td colspan=14 align=center><hr></td>
                     </tr>
                    <tr>
                        <td>Claim No</td>
                        <td>Date</td>
                        <td>Adm No</td>
                        <td>Names</td>
                        <td>Bill No</td>
                        <td align=right>Invoice</td>
                        <td align=right>Nhif Credit</td>
                        <td align=right>Balance</td>
                        <td align=center>Description</td>
                        <td align=center>G/L Acc</td>
                        <td align=center>User</td>
                        <td>Update</td>
                        <td>Delete</td>
                        <td>Print</td>
                     </tr>
                     <tr>
                        <td colspan=14 align=center><hr></td>
                     </tr>';
    $rowbg = 'white';
    while ($row = $result->FetchRow($result)) {
        $out = $out . '<tr bgcolor=' . $rowbg . '>
                        <td>' . $row[0] . '</td>
                        <td>' . $row[1] . '</td>
                        <td>' . $row[2] . '</td>
                        <td>' . $row[3] . '</td>
                        <td>' . $row['bill_number'] . '</td>
                        <td align=right>' . number_format($row[invAmount],2) . '</td>
                        <td align=right>' . number_format($row[totalCredit],2) . '</td>
                        <td align=right>' . number_format($row[balance],2) . '</td>
                        <td align=right> NHIF CARD ' . $row[7] . '</td>
                        <td align=right></td>
                        <td>' . $row['inputUser'] . '</td>
                        <td><a href="../credits.php?claimNo="'.$row[0].'" onclick="">Edit</a></td>
                            <td><button  onclick="deleteClaim('.$row[0] .','.$row[2] .','.$row[bill_number].')">Delete</button></td>
                        <td><button onclick="invoicePdf(' . $row[2] . ',1, ' . $row['bill_number'] . ')" id="printInv">Print Invoice</button></td>
                     </tr>';
        $rowbg = 'white';
    }
    $out = $out . '<tr><td colspan=12 align=center><input type="submit" id="print" name="print" value="Print Report" />
      <input type="submit" id="export" name="export" value="Export to Excel" /></td></tr>';
    $out = $out . '</tbody></table>';

    echo $out;
}

function getItemDescription($desc) {
    global $db;
    if ($desc) {

        $sql = "select purchasing_class,item_description, unit_price from care_tz_drugsandservices WHERE partcode='$desc'";
        $result = $db->Execute($sql);
        if (!$result) {
            echo 'Could not run query: ' . mysql_error();
            exit;
        }

        $row = $result->FetchRow();

        echo $row[0] . "," . $row[1] . "," . $row[2] . "," . $rowID; // 42
        //echo "Laboratory $rowID";
    } else {
        echo "....";
    }
}

function getPNames($pid) {
    global $db;
    if ($pid) {
        $sql = "SELECT b.name_first,b.name_2,b.name_last,max(a.encounter_nr) as encounter_nr,a.encounter_class_nr,
        a.current_ward_nr,w.description,a.finalised
        from care_person b
                inner join care_encounter a on a.pid=b.pid 
        inner join care_ward w on w.nr=a.current_ward_nr WHERE b.pid='$pid' and a.encounter_class_nr=1";
        // $result = mysql_query("SELECT name,next_receipt_no FROM care_ke_cashpoints WHERE pcode='$desc2'");
        $result = $db->Execute($sql);
        if (!$result) {
            echo 'Could not run query: ' . mysql_error();
            exit;
        }
        $row = $result->FetchRow();

        $sql2 = "SELECT newdebitNo from care_ke_invoice";
        $result2 = $db->Execute($sql2);
        $row2 = $result2->FetchRow();
        if ($row2[0] <> '')
            $debitNo = $row2[0];
        else
            $debitNo = 'D10001';

        echo $row[0] . "," . $row[1] . "," . $row[2] . "," . $row[3] . "," . $row[4] .
        "," . $row[5] . "," . $row[6] . "," . $debitNo .",". $row[7]; // 42
    } else {
        echo "---";
    }
}

//getBillNumbers($pid2);
function getBillNumbers($pid) {
    global $db;

    $pid2=($pid<>"" ? $pid=$pid : $pid='1000');
    
    $sql = "select DISTINCT bill_number from care_ke_billing where pid=$pid2
    and `IP-OP`=1 order by bill_date desc";

    $result = $db->Execute($sql);
    if (!$result) {
        echo 'Could not run query: ' . $sql;
        exit;
    }
    $total=$result->RecordCount();
    echo '{
    "total":"' . $total . '","billnumbers":[';
    $counter=0;
       while($row=$result->FetchRow()){
        echo '{"BillNumbers":"' . $row[bill_number] .'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function getEncounterNumbers($pid) {
    global $db;

    $pid2=($pid<>"" ? $pid=$pid : $pid='1000');
    
    $sql = "select DISTINCT encounter_nr from care_ke_billing where pid=$pid2
    and `IP-OP`=1 order by bill_date desc";

    $result = $db->Execute($sql);
    if (!$result) {
        echo 'Could not run query: ' . $sql;
        exit;
    }
    $total=$result->RecordCount();
    echo '{
    "total":"' . $total . '","encounters":[';
    $counter=0;
       while($row=$result->FetchRow()){
        echo '{"EncounterNumbers":"' . $row[encounter_nr] .'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function finalizeInvoice($db, $pid, $billNumber, $fdate) {
    global $db, $root_path;
    require_once($root_path . 'include/care_api_classes/class_tz_insurance.php');
    require_once($root_path . 'include/care_api_classes/class_tz_billing.php');
    $insurance_obj = new Insurance_tz;
    $bill_obj = new Bill;
    $debug = false;
    if ($billNumber) {
        $sql = "SELECT distinct b.pid,b.bill_number,b.finalised,b.encounter_nr FROM care_ke_billing b LEFT JOIN care_encounter e 
                ON b.pid=e.pid WHERE b.pid='$pid' AND b.`ip-op`=1 and bill_number='$billNumber' AND e.is_discharged=1";

        if ($results = $db->Execute($sql)) {
//           echo 'success success';

            if ($debug)
                echo $sql;
            while ($rows = $results->FetchRow()) {
                $bill_number = $rows[1];
                $enc_nr=$rows[encounter_nr];
                $stat = $rows[2];
                if ($stat <> "1") {
                    $sql2 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=1 
            and service_type no in('payment','nhif') and bill_number=$billNumber";
                    $result2 = $db->Execute($sql2);
                    $row2 = $result2->FetchRow();
                    if ($debug)
                        echo $sql2;
                    $finalResults = $finalResults . 'bill=' . $row2[0] . '<br>';

                    $sql3 = "SELECT sum(total) as total FROM care_ke_billing WHERE pid = '$pid' and `IP-OP`=1 
                        and service_type in ('Payment','NHIF') and bill_number=$billNumber";
                    if ($debug)
                        echo $sql3;
                    $result2 = $db->Execute($sql3);
                    $row3 = $result2->FetchRow();
                    $finalResults = $finalResults . 'paid=' . $row3[0] . '<br>';

                    $balance = intval($row2[0] - $row3[0]);
                    $finalResults = $finalResults . "balance=" . $balance;

                    $sql4 = "select insurance_id from care_person where pid='$pid'";
                    $result4 = $db->Execute($sql4);
                    $row4 = $result4->FetchRow();

                    if ($balance == 0 || $row4[0] > 0) {

                        $sql = "Update care_ke_billing set `status`='Finalized',finalised='1' where pid='$pid' and 
                            bill_number='$bill_number'";
                        if ($debug)
                            echo $sql;
                        if ($db->Execute($sql)) {
//                            $IS_PATIENT_INSURED = $insurance_obj->is_patient_insured($pid);
//                            if ($IS_PATIENT_INSURED) {
//                                $insuCompanyID = $insurance_obj->GetCompanyFromPID2($pid);
//                                $bill_obj->updateDebtorsTrans($pid, $insuCompanyID, $enc_nr);
//                            }

                            $finalResults = $finalResults . "<div class='myMessage'>Invoice number  $bill_number finalized succefully</div>";
                            $finalResults = $finalResults . "<div> <button id='print' onclick=invoicePdf('" . $pid . "')>Print Finalized Invoice</button></div>";
                        }
                    } else {
                        $finalResults = $finalResults . "<div class='myMessage'>The patient has a pending balance of Ksh $balance</div>";
                    }
//                } else if ($balance < 0) {
//                    echo "<div class='myMessage'>The patient has paid in Excess of $balance</div>";
//                }
                } else {
                    $finalResults = $finalResults . "<div class='myMessage'>Invoice number  $bill_number is already finalized</div>";
                }
            }
        }
    } else {
        $finalResults = $finalResults . "<div class='myMessage'>Please Select Bill Numnber to Finalize $billNumber</div>";
    }



    echo $finalResults;
}

?>
