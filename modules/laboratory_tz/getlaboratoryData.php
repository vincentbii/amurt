<?php
/**
 * Created by george maina
 * Email: georgemainake@gmail.com
 * Copyright: All rights reserved on 5/15/14.
 */
/**
 * Created by PhpStorm.
 * User: George Maina
 * Email:georgemainake@gmail.com
 * Copyright: All rights reserved
 * Date: 5/15/14
 * Time: 1:03 PM
 * 
 */

    error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
    require_once('roots.php');
    require ($root_path . 'include/inc_environment_global.php');
    require_once($root_path . 'include/care_api_classes/class_weberp_c2x.php');
    require_once($root_path . 'include/inc_init_xmlrpc.php');

    $task = ($_REQUEST['task']) ? ($_REQUEST['task']) : '';

    switch($task){
        case 'insertResultsParams':
            insertResultsParams();
            break;
        case 'updateResultsParams':
            updateResultsParams();
            break;
        case 'getLabTests':
            getLabTests();
            break;
        default:
            echo "{failure:true}";
            break;
    }

    function getLabTests(){
        global $db;
        $debug=false;

        $sql="Select `item_id`,`name` from care_tz_laboratory_param WHERE group_id<>'-1'";
        if($debug) echo $sql;
        $results=$db->Execute($sql);
        $rcount=$results->RecordCount();

        echo '{"Total":"'.$rcount.'","labtests":[';
        $counter=0;
        while($row=$results->FetchRow()){
            echo '{"item_id":"'.$row[item_id].'","testName":"'.$row[name].'"}';
            $counter++;

            if($counter<>$rcount){
                echo ',';
            }
        }

        echo ']}';
    }


 function insertResultsParams(){
     global $db;
     $debug=false;
     $item_id=$_REQUEST[item_id];
     $results=$_REQUEST[results];
     $input_type=$_REQUEST[input_type];
     $unit_msr=$_REQUEST[unit_msr];
     $normal=$_REQUEST[normal];
     $ranges=$_REQUEST[ranges];
     $result_values=$_REQUEST[result_values];
     $inputUser= $_SESSION['sess_login_username'];


     $sql="INSERT INTO `care_tz_laboratory_resultstypes`
            (`item_id`,`results`,`input_type`,`unit_msr`,`normal`, `ranges`, `result_values`,`inputUser`)
           VALUES ('$item_id','$results','$input_type','$unit_msr','$normal','$ranges','$result_values','$inputUser')";

     if($debug) echo $sql;

     if($db->Execute($sql)){
         echo "{success:true}";
     }else{
         echo "{failure:true}";
     }

 }

 function updateResultsParams(){
        global $db;
        $debug=false;
        $resultID=$_REQUEST[resultID];
        $item_id=$_REQUEST[item_id];
        $results=$_REQUEST[results];
        $input_type=$_REQUEST[input_type];
        $unit_msr=$_REQUEST[unit_msr];
        $normal=$_REQUEST[normal];
        $ranges=$_REQUEST[ranges];
        $result_values=$_REQUEST[result_values];
        $inputUser= $_SESSION['sess_login_username'];


        $sql="UPDATE `care_tz_laboratory_resultstypes`
             SET `item_id`='$item_id',`results`='$results',`input_type`='$input_type',`unit_msr`='$unit_msr',
             `normal`='$normal', `ranges`='$ranges', `result_values`='$result_values',`inputUser`='$inputUser' WHERE resultID=$$resultID";

        if($debug) echo $sql;

        if($db->Execute($sql)){
            echo "{success:true}";
        }else{
            echo "{failure:true}";
        }

    }