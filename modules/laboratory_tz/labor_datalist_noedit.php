<?php

error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require('./roots.php');
require($root_path.'include/inc_environment_global.php');
//$db->debug = true;
/**
* CARE2X Integrated Hospital Information System Deployment 2.2 - 2006-07-10
* GNU General Public License
* Copyright 2002,2003,2004,2005,2006 Elpidio Latorilla
* elpidio@care2x.org, 
*
* See the file "copy_notice.txt" for the licence notice
*/
//gjergji :
//data diff for the dob
function dateDiff($dformat, $endDate, $beginDate){
	$date_parts1=explode($dformat, $beginDate);
	$date_parts2=explode($dformat, $endDate);
	$start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
	$end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
	return $end_date - $start_date;
}
//gjergji :
//utility function to print out the arrows depending on age / sex
function checkParamValue($paramValue,$pName) {
	global $root_path,$patient;
	$txt = '';
	$dobDiff = dateDiff("-", date("Y-m-d"), $patient['date_birth']);
	switch ($dobDiff) {
	case ( ($dobDiff >= 1) and ($dobDiff <= 30 ) ) :
			if($pName['hi_bound_n']&&$paramValue>$pName['hi_bound_n']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_up_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}elseif($paramValue<$pName['lo_bound_n']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_dwn_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}else{
				$txt.=htmlspecialchars($paramValue);
			}
			break;
	case ( ($dobDiff >= 31) and ($dobDiff <= 360 ) ) :
			if($pName['hi_bound_y']&&$paramValue>$pName['hi_bound_y']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_up_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}elseif($paramValue<$pName['lo_bound_y']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_dwn_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}else{
				$txt.=htmlspecialchars($paramValue);
			}
			break;
	case ( $dobDiff >= 361) and ($dobDiff <= 5040 ) :
			if($pName['hi_bound_c']&&$paramValue>$pName['hi_bound_c']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_up_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}elseif($paramValue<$pName['lo_bound_c']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_dwn_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}else{
				$txt.=htmlspecialchars($paramValue);
			}
			break;	
	case $dobDiff > 5040 :
		if($patient['sex']=='m')
			if($pName['hi_bound']&&$paramValue>$pName['hi_bound']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_up_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}elseif($paramValue<$pName['lo_bound']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_dwn_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}else{
				$txt.=htmlspecialchars($paramValue);
			}	
		elseif($patient['sex']=='f')	
			if($pName['hi_bound_f']&&$paramValue>$pName['hi_bound_f']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_up_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}elseif($paramValue<$pName['lo_bound_f']){
				$txt.='<img '.createComIcon($root_path,'arrow_red_dwn_sm.gif','0','',TRUE).'> <font color="red">'.htmlspecialchars($paramValue).'</font>';
			}else{
				$txt.=htmlspecialchars($paramValue);
			}																				
			break;
	}
	return $txt;
}

function getParameterName($strParam){
    global $db;
    
    $sql="SELECT group_id,name from care_tz_laboratory_param where item_id='$strParam'";
    $request=$db->Execute($sql);
    
    return $request;
}

function getLowerBound($strParam, $age){
	global $db;

	$lo_bound = '';
	if($age < 6){
		$lo_bound = 'lo_bound_c';
	}else{
		if($sex == 'm'){
			$lo_bound = 'lo_bound_m';
		}elseif ($sex = 'f') {
			$lo_bound = 'lo_bound_f';
		}
	}

	$sql="SELECT * from care_tz_laboratory_resultstypes where resultID='$strParam'";
    $request=$db->Execute($sql);
    $row=$request->FetchRow();

    $value_l = '';
    if($age < 6){
		$value_l = $row['lo_bound_c'];
	}else{
		if($sex == 'm'){
			$value_l = $row['lo_bound_m'];
		}elseif ($sex = 'f') {
			$value_l = $row['lo_bound_f'];
		}
	}

	return $value_l;

}

function getUnitMsr($strParam){
	global $db;

	$sql="SELECT * from care_tz_laboratory_resultstypes where resultID='$strParam'";
    $request=$db->Execute($sql);
    $row=$request->FetchRow();

    return $row['unit_msr'];
}

function getUpperBound($strParam, $age){
	global $db;

	$hi_bound = '';
	if($age < 6){
		$hi_bound = 'hi_bound_c';
	}else{
		if($sex == 'm'){
			$hi_bound = 'hi_bound_m';
		}elseif ($sex = 'f') {
			$hi_bound = 'hi_bound_f';
		}
	}

	$sql="SELECT * from care_tz_laboratory_resultstypes where resultID='$strParam'";
    $request=$db->Execute($sql);
    $row=$request->FetchRow();

    $value_h = '';
    if($age < 6){
		$value_h = $row['hi_bound_c'];
	}else{
		if($sex == 'm'){
			$value_h = $row['hi_bound_m'];
		}elseif ($sex = 'f') {
			$value_h = $row['hi_bound_f'];
		}
	}

	return $value_h;

}

function getFate($strParam, $age, $value){
	global $db;
	$hi_bound = '';
	$lo_bound = '';
	if($age < 6){
		$hi_bound = 'hi_bound_c';
		$lo_bound = 'lo_bound_c';
	}else{
		if($sex == 'm'){
			$hi_bound = 'hi_bound_m';
			$lo_bound = 'lo_bound_m';
		}elseif ($sex = 'f') {
			$hi_bound = 'hi_bound_f';
			$lo_bound = 'lo_bound_f';
		}
	}

	$sql="SELECT * from care_tz_laboratory_resultstypes where resultID='$strParam'";
    $request=$db->Execute($sql);
    $row=$request->FetchRow();


    $result = $row['results'];
    
    if($age < 6){
		$value_h = $row['hi_bound_c'];
		$value_l = $row['lo_bound_c'];
	}else{
		if($sex == 'm'){
			$value_h = $row['hi_bound_m'];
			$value_l = $row['lo_bound_m'];
		}elseif ($sex = 'f') {
			$value_h = $row['hi_bound_f'];
			$value_l = $row['lo_bound_f'];
		}
	}

	$fate = '';
	if($value = null){
		$fate = 'N/A';
	}else{
		if($value < $value_l){
    	$fate = "L";
    }elseif ($value > $value_h) {
    	$fate = "H";
    }else{
    	$fate = '--';
    }
	}



	return $fate;
}

function getTestId($strParam){
    global $db;
    
    $sql="SELECT results from care_tz_laboratory_resultstypes where resultID='$strParam'";
    $request=$db->Execute($sql);
    $row=$request->FetchRow();
    return $row[0];
}

define('LANG_FILE','lab.php');
define('NO_2LEVEL_CHK',1);
require_once($root_path.'include/inc_front_chain_lang.php');
if(!isset($user_origin)) $user_origin='';

if($user_origin=='lab'||$user_origin=='lab_mgmt'){
  	$local_user='ck_lab_user';
  	if(isset($from) && $from=='input') $breakfile=$root_path.'modules/laboratory_tz/labor_datainput.php'.URL_APPEND.'&encounter_nr='.$encounter_nr.'&job_id='.$job_id.'&parameterselect='.$parameterselect.'&allow_update='.$allow_update.'&user_origin='.$user_origin;
		else $breakfile=$root_path.'modules/laboratory_tz/labor_data_patient_such.php'.URL_APPEND;
}else{
  	$local_user='ck_pflege_user';
  	$breakfile=$root_path.'modules/nursing/nursing-station-patientdaten.php'.URL_APPEND.'&pn='.$pn.'&edit='.$edit;
	$encounter_nr=$pn;
}
if(!$_COOKIE[$local_user.$sid]) {header("Location:".$root_path."language/".$lang."/lang_".$lang."_invalid-access-warning.php"); exit;}; 

if(!$encounter_nr) header("location:".$root_path."modules/laboratory_tz/labor_data_patient_such.php?sid=$sid&lang=$lang");

$thisfile=basename(__FILE__);

///$db->debug=1;

/* Create encounter object */
require_once($root_path.'include/care_api_classes/class_lab.php');
$enc_obj= new Encounter($encounter_nr);
$lab_obj=new Lab($encounter_nr);

$cache='';

if($nostat) $ret=$root_path."modules/laboratory_tz/labor_data_patient_such.php?sid=$sid&lang=$lang&versand=1&keyword=$encounter_nr";
	else $ret=$root_path."modules/nursing/nursing-station-patientdaten.php?sid=$sid&lang=$lang&station=$station&pn=$encounter_nr";
	
# Load the date formatter */
require_once($root_path.'include/inc_date_format_functions.php');

$enc_obj->setWhereCondition("encounter_nr='$encounter_nr'");

if($encounter=&$enc_obj->getBasic4Data($encounter_nr)) {

	$patient=$encounter->FetchRow();

	$recs=&$lab_obj->getAllResults($encounter_nr);
	
	if ($rows=$lab_obj->LastRecordCount()){

		# Check if the lab result was recently modified
		$modtime=$lab_obj->getLastModifyTime();

		$lab_obj->getDBCache('chemlabs_result_'.$encounter_nr.'_'.$modtime,$cache);
		# If cache not available, get the lab results and param items
		if(empty($cache)){
			$records=array();
			$dt=array();
			while($buffer=&$recs->FetchRow()){
				# Prepare the values
				$tmp = array($buffer['paramater_name'] => $buffer['parameter_value']);
				$records[$buffer['job_id']][] = $tmp;
				$tdate[$buffer['job_id']]=&$buffer['test_date'];
				$ttime[$buffer['job_id']]=&$buffer['test_time'];		
			}
		}
	}else{
		if($nostat) header("location:".$root_path."modules/laboratory_tz/labor-nodatafound.php".URL_REDIRECT_APPEND."&user_origin=$user_origin&ln=".strtr($patient['name_last'],' ','+')."&fn=".strtr($patient['name_first'],' ','+')."&bd=".formatDate2Local($patient['date_birth'],$date_format)."&encounter_nr=$encounter_nr&nodoc=labor&job_id=$job_id&parameterselect=$parameterselect&allow_update=$allow_update&from=$from");
		 	else header("location:".$root_path."modules/nursing/nursing-station-patientdaten-nolabreport.php?sid=$sid&lang=$lang&edit=$edit&station=$station&pn=$encounter_nr&nodoc=labor&user_origin=$user_origin");
			exit;
	}

}else{
	echo "<p>".$lab_obj->getLastQuery()."sql$LDDbNoRead";exit;
}


?>

<style type="text/css" name="1">
.va12_n{font-family:verdana,arial; font-size:12; color:#000099;font-weight: bold}
.a10_b{font-family:arial; font-size:10; color:#000000}
.a10_n{font-family:arial; font-size:10; color:#000099}
.a12_b{font-family:arial; font-size:12; color:#000000}
.j{font-family:verdana; font-size:12; color:#000000}
.wardlistrow1{background-color: #EFEFEF}
.wardlistrow2{background-color: #DCDCDC}
.wardlistrow0{background-color: #dd0000}
.adm_item{background-color:cyan}
.adm-input{background-color: #DCDCDC}


</style>

<script language="javascript">
<!-- Script Begin
var toggle=true;
function selectall(){

	d=document.labdata;
	var t=d.ptk.value;
	if(t == 1){
		if(toggle==true){ 
			d.tk.checked=true;
		}
	}else{
		for(i = 0; i<t; i++){
			if(toggle==true && d.tk[i]){
				d.tk[i].checked=true; 
			}
		}
	}
	if(toggle==false){ 
		d.reset();
	}
	toggle=(!toggle);
}

function prep2submit(){
	d=document.labdata;
	var j=false;
	var t=d.ptk.value;
	var n=false;
	for(i=0; i<t; i++) {
		if(t==1) {
			n=d.tk;
			v=d.tk.value;
		}else if( d.tk[i]){ 
			n=d.tk[i];
			v=d.tk[i].value;
		}
		if(n.checked==true && d.tk[i]){
			if(j){
				d.params.value=d.params.value +"~"+v;
			}else if( d.tk[i]){ 
				d.params.value=v;	
				j=1;
			}
		 }
	}
	if(d.params.value!=''){
		d.submit();
	}else{
		alert('<?php echo $LDCheckParamFirst ?>');
	}
}

function remove(s, t) {
  /*
  **  Remove all occurrences of a token in a string
  **    s  string to be processed
  **    t  token to be removed
  **  returns new string
  */
  i = s.indexOf(t);
  r = "";
  if (i == -1) return s;
  r += s.substring(0,i) + remove(s.substring(i + t.length), t);
  return r;
}

var skipme = '';
function wichOne(nr) {
	
	if( document.getElementById(nr).checked == true ) {
		if( skipme == '' ) skipme = nr;
		else skipme += "-"+nr;
	} else if ( document.getElementById(nr).checked == false ) 
		skipme = remove(skipme,nr);
}
	
	
function openReport() {
	enc = <?php echo $encounter_nr ?>;
	userId = '<?php echo $_SESSION['sess_user_name']; ?>';
	urlholder="<?php echo $root_path ?>modules/pdfmaker/laboratory/report_all.php<?php echo URL_REDIRECT_APPEND; ?>&encounter_nr="+enc+"&skipme="+skipme+"&userId="+userId;
	window.open(urlholder,'<?php echo $LDOpenReport; ?>',"width=700,height=500,menubar=no,resizable=yes,scrollbars=yes");
}
//  Script End -->
</script>

<?php 

echo '
<table width="100%"  cellspacing="0" border="0">
<tbody class="main">
<tr> 
    <td valign="top" height="35" align="center">
        <table class="titlebar" cellspacing="0" border="0" width=100%>
            <tbody>
                <tr class="titlebar" valign="top">
                <td bgcolor="#99ccff">
                <font color="#330066">Lab Report</font>
                </td>
                <td bgcolor="#99ccff" align="right">
                <a href="javascript:gethelp("request_radio.php","6")">
                <img width="76" height="21" border="0" onmouseout="hilite(this,0)" onmouseover="hilite(this,1)" style="filter:alpha(opacity=70)" alt="" src="../../gui/img/control/blue_aqua/en/en_hilfe-r.gif">
                </a>
                <a href="'.$breakfile.'">
                <img width="76" height="21" border="0" onmouseout="hilite(this,0)" onmouseover="hilite(this,1)" style="filter:alpha(opacity=70)" alt="" src="../../gui/img/control/blue_aqua/en/en_close2.gif">
                </a>
                </td>
                </tr>
            </tbody>
       </table>
    </td>
</tr>
<tr valign=top><td>
            <table>
                <tr class="adm_item"><td>'.$LDCaseNr.'</td><td class="adm-input">'.$encounter_nr.'</td> </tr>
                <tr class="adm_item"><td>PID</td><td class="adm-input">'.$patient['pid'].'</td> </tr>
                <tr class="adm_item"><td>Hosp File No</td><td class="adm-input">'.$patient['selian_pid'].'</td> </tr>
                <tr class="adm_item"><td>'.$LDLastName.'</td><td class="adm-input">'.$patient['name_last'].'</td> </tr>
                <tr class="adm_item"><td>'.$LDName.'</td><td class="adm-input">'.$patient['name_first'].' '.$patient['name_2'].'</td> </tr>
                <tr class="adm_item"><td>'.$LDBday.'</td><td class="adm-input">'.$patient['date_birth'].'</td> </tr>
                 <tr><td colspan=2>&nbsp;</td> </tr>
            </table>
    </td>
</tr><tr valign=top><td>';
    $sql="SELECT k.encounter_nr,k.test_date,k.test_time,k.job_id,k.batch_nr FROM  care_test_findings_chemlab k 
            LEFT JOIN care_test_request_chemlabor t ON t.batch_nr=k.job_id 
            WHERE k.status='pending' AND k.encounter_nr='$encounter_nr' ORDER BY job_id ASC";

  
$request=$db->Execute($sql);
echo '<table border=0 width=100%>
         <tr class="wardlistrow0">
            <td class="va12_n"><font color="#ffffff">JobID</font></td>
            <td class="va12_n"><font color="#ffffff">Test Date</font></td>
            <td class="va12_n"><font color="#ffffff">Time</td></font>
            <td class="va12_n"><font color="#ffffff">Results</td></font>
         </tr>';
$class='wardlistrow1';
while($row=$request->FetchRow()){
    echo '<tr><td class="'.$class .'" valign=top>'.$row['job_id'].'</td>
              <td class="'.$class .'" valign=top>'.$row['test_date'].'</td>
              <td class="'.$class .'" valign=top>'.$row['test_time'].'</td>;
              <td>';
            $sql2="select paramater_name,parameter_value from care_test_findings_chemlabor_sub where job_id='$row[job_id]'";
              //echo $sql2;
            $results=$db->Execute($sql2);
            echo "<table><tr><td>Group Id</td><td>Test Name</td><td>Results</td><td>Values</td><td>Fate</td><td>Lower Bound</td><td>Upper Bound</td></tr>";
            while($row2=$results->FetchRow()){

            	$age = $patient['date_birth'];
                    $now = time();
 
					//Get the timestamp of the person's date of birth.
					$dob = strtotime($age);
					 
					//Calculate the difference between the two timestamps.
					$difference = $now - $dob;
					 
					//There are 31556926 seconds in a year.
					$age = floor($difference / 31556926);

                if(substr($row2['paramater_name'],0, 5)=='group'){
                    $strParam=explode('-',$row2['paramater_name']);
                   // echo $strParam[1];
                    $paramName=getParameterName($strParam[1]);
                    $resultTestIDs=getTestId($strParam[2]);
                    // $fateResults=getFate($strParam['2'], $age, $row2['parameter_value']);
                    $lowerBound=getLowerBound($strParam['2'], $age);
                    $upperBound=getUpperBound($strParam['2'], $age);
                    $unit_msr=getUnitMsr($strParam['2']);



                    $value = $row2['parameter_value'];

					$fateResults = '';
					if($lowerBound == null || $upperBound == null || !is_numeric($lowerBound) || !is_numeric($upperBound) || !is_numeric($value)){
						$fateResults = '--';
					}else{
						if($lowerBound > $value){
							$fateResults = 'L';
						}elseif ($upperBound < $value) {
							$fateResults = 'H';
						}else{
							$fateResults = '--';
						}
					}
					

					if($lowerBound == null){
						$lowerBound = '--';
					}

					if($upperBound == null){
						$upperBound = '--';
					}
                    
                    $strRow1=$paramName->FetchRow();   
                    echo '<tr><td class="'.$class .'">'.$strRow1['group_id'].'</td>
                               <td class="'.$class .'">'.$strRow1['name'].'</td>
                               <td class="'.$class.'">'.$resultTestIDs.'</td>
                               <td class="'.$class .'">'.$row2['parameter_value'].' '.$unit_msr.'</td>
                               <td style="font-weight: bold; color: RED;" class="<?php echo $class; ?>">'.$fateResults.'</td>
                               <td class="'.$class .'">'.$lowerBound.'</td>
                               <td class="'.$class .'">'.$upperBound.'</td>

                               ';
                }
                
                if(substr($row2['paramater_name'],0, 5)<>'group'){
                    $str=$row2['paramater_name'];
                    $pos=strrpos($str,'_');
                    $groupName= substr($str, $pos+1);

                    $sql_fate = "SELECT * FROM care_tz_laboratory_param where id = '".$row2['paramater_name']."'";
					$result_fate=$db->Execute($sql_fate);
					$rows_fate=$result_fate->FetchRow();

                    
					$value = $row2['parameter_value'];
					$boundl = '';
					$boundh = '';
					if($age < 6){
						$boundh = $rows_fate['hi_bound_y'];
						$boundl = $rows_fate['lo_bound_y'];
					}else{
						if($sex = 'm'){
							$boundh = $rows_fate['hi_bound'];
							$boundl = $rows_fate['lo_bound'];
						}elseif ($sex = 'f') {
							$boundh = $rows_fate['hi_bound_f'];
							$boundl = $rows_fate['lo_bound_f'];
						}
					}

					if($boundl == null){
						$boundl = '--';
					}

					if($boundh == null){
						$boundh = '--';
					}
					$fate5 = '';
					if($boundh == null || $boundl == null || !is_numeric($boundl) || !is_numeric($boundh) || !is_numeric($value)){
						$fate5 = '--';
					}else{
						if($boundl > $value){
						$fate5 = 'L';
						}elseif ($boundh < $value) {
							$fate5 = 'H';
						}else{
							$fate5 = '--';
						}
					}

					
					
					

					//Print it out.
					// echo $age;
    				?>
    				<tr>
    					<td class="<?php echo $class; ?>"><?php echo $groupName; ?></td>
                                <td class="<?php echo $class; ?>"><?php echo $row2['paramater_name']; ?></td>
                                    <td class="<?php echo $class; ?>"></td>
                                    <td class="<?php echo $class; ?>"><?php echo $row2['parameter_value'].' '.$rows_fate['msr_unit']; ?></td>
                                    <td style="font-weight: bold; color: RED;" class="<?php echo $class; ?>"><?php
                                    echo $fate5;
                                                                       ?></td>
                                    <td class="<?php echo $class; ?>"><?php echo $boundl; ?></td>
                                    <td class="<?php echo $class; ?>"><?php echo $boundh; ?></td>
                                    <?php
                }
                ?></tr><?php
            }
    echo '</table></td><tr>';
    $class=='wardlistrow1' ? $class='wardlistrow2' : $class='wardlistrow1';
}
echo '</table>';

	
echo '</td><tb>
    <
</td></tr></tbody>
</table>
';
?>
