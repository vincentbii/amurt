# ext-theme-neptune-d2a18091-984a-49ef-8054-4c65ca8634bd/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-d2a18091-984a-49ef-8054-4c65ca8634bd/sass/etc"`, these files
need to be used explicitly.
