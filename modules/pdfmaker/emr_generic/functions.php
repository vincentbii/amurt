<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);

$report_textsize=12;
$report_titlesize=14;
$report_auxtitlesize=10;
$report_authorsize=10;
$filter='3,99'; # notes type number which are not allowed to be displayed

require('./roots.php');
require($root_path.'include/inc_environment_global.php');

global $db,$rows,$result,$enc;

/*

Measurement Type 
1 - Blood Pressure (lower)
2 - Blood Pressure (Upper)
3 - Temparature
6 - Weight
7 - Height
8 - 
9 - Head Circumference
10 - Pulse rate
11 - Respiratory Rate
12 - HTC
13 = BMI

*/

	$bmi_sql = 'select * from care_encounter_measurement where msr_type_nr = 13 and encounter_nr = '.$enc;
	$bmi_result=$db->Execute($bmi_sql);
	$bmi_row = $bmi_result->FetchRow();
	$bmi = $bmi_row['value'];
	$bmi_notes = $bmi_row['notes'].'. ';

	$temp_sql = 'select * from care_encounter_measurement where msr_type_nr = 3 and encounter_nr = '.$enc;
	$t_result=$db->Execute($temp_sql);
	$t_row = $t_result->FetchRow();
	$temp = $t_row['value'];
	$temp_notes = $t_row['notes'].'. ';

	$bp_sql = 'select * from care_encounter_measurement where msr_type_nr = 2 and encounter_nr = '.$enc;
	$bp_result=$db->Execute($bp_sql);
	$bp_row = $bp_result->FetchRow();
	$bp1 = $bp_row['value'];
	$bp1notes = $bp_row['notes'].'. ';


	$bp1_sql = 'select * from care_encounter_measurement where msr_type_nr = 1 and encounter_nr = '.$enc;
	$bp1_result=$db->Execute($bp1_sql);
	$bp1_row = $bp1_result->FetchRow();
	$bp2 = $bp1_row['value'];
	$bp2_notes = $bp1_row['notes'].'. ';

	$bp = $bp1.'/'.$bp2;

	$pr_sql = 'select * from care_encounter_measurement where msr_type_nr = 10 and encounter_nr = '.$enc;
	$pr_result=$db->Execute($pr_sql);
	$pr_row = $pr_result->FetchRow();
	$pr = $pr_row['value'];
	$pr_notes = $pr_row['notes'].'. ';

	$rr_sql = 'select * from care_encounter_measurement where msr_type_nr = 11 and encounter_nr = '.$enc;
	$rr_result=$db->Execute($rr_sql);
	$rr_row = $rr_result->FetchRow();
	$rr = $rr_row['value'];
	$rr_notes = $rr_row['notes'].'. ';

	$bp_sql = 'select * from care_encounter_measurement where msr_type_nr = 1 and encounter_nr = '.$enc;
	$bp_result=$db->Execute($bp_sql);
	$bp_row = $bp_result->FetchRow();
	$bp = $bp_row['value'];
	$bp_notes = $bp_row['notes'].'. ';

	$w_sql = 'select * from care_encounter_measurement where msr_type_nr = 6 and encounter_nr = '.$enc;
	$w_result=$db->Execute($w_sql);
	$w_row = $w_result->FetchRow();
	$wht = $w_row['value'];
	$wht_notes = $w_row['notes'].'. ';

	$h_sql = 'select * from care_encounter_measurement where msr_type_nr = 7 and encounter_nr = '.$enc;
	$h_result=$db->Execute($h_sql);
	$h_row = $h_result->FetchRow();
	$hgt = $h_row['value'];
	$hgt_notes = $h_row['notes'].'. ';

	$nurse_notes = $bmi_notes.$temp_notes.$bp1notes.$bp2_notes.$pr_sql.$rr_notes.$wht_notes.$hgt_notes;

	$doctor_notes = '';
	$sql_notes = 'select *from care_encounter_notes where (type_nr = 3 || 1) and encounter_nr = '.$enc;
	$result_notes = $db->Execute($sql_notes);
	while ($rows_notes = $result_notes->FetchRow()) {
		$doctor_notes = $doctor_notes.$rows_notes['notes']."\n";		
	}

	$hpi_notes = '';
	$sql_hpi = 'select *from care_encounter_notes where type_nr = 10 and encounter_nr = '.$enc;
	$result_hpi = $db->Execute($sql_hpi);
	while ($rows_hpi = $result_hpi->FetchRow()) {
		$hpi_notes = $hpi_notes.$rows_hpi['notes'].'.  ';		
	}

	$systemic_notes = '';
	$sql_systemic = 'select *from care_encounter_notes where type_nr = 18 and encounter_nr = '.$enc;
	$result_systemic = $db->Execute($sql_systemic);
	while ($rows_systemic = $result_systemic->FetchRow()) {
		$systemic_notes = $systemic_notes.$rows_systemic['notes'].'.  ';		
	}

	$pef = '';
	$sql_pef = 'select *from care_encounter_notes where type_nr = 11 and encounter_nr = '.$enc;
	$result_pef = $db->Execute($sql_pef);
	while ($rows_pef = $result_pef->FetchRow()) {
		$pef = $pef.$rows_pef['notes'].'.  ';		
	}

	$main_system_findings = '';
	$sql_ms_findings = 'select *from care_encounter_notes where type_nr = 21 and encounter_nr = '.$enc;
	$result_ms_findings = $db->Execute($sql_ms_findings);
	while ($rows_ms_findings = $result_ms_findings->FetchRow()) {
		$main_system_findings = $main_system_findings.$rows_ms_findings['notes'].'.  ';		
	}

	$other_system_findings = '';
	$sql_os_findings = 'select *from care_encounter_notes where type_nr = 12 and encounter_nr = '.$enc;
	$result_os_findings = $db->Execute($sql_os_findings);
	while ($rows_os_findings = $result_os_findings->FetchRow()) {
		$other_system_findings = $other_system_findings.$rows_os_findings['notes'].'.  ';		
	}

	$impression = '';
	$sql_impression = 'select *from care_encounter_notes where type_nr = 23 and encounter_nr = '.$enc;
	$result_impression = $db->Execute($sql_impression);
	while ($rows_impression = $result_impression->FetchRow()) {
		$impression = $impression.$rows_impression['notes'].'.  ';		
	}

	$i_r = '';
	$sql_ir = 'select *from care_encounter_notes where type_nr = 19 and encounter_nr = '.$enc;
	$result_ir = $db->Execute($sql_ir);
	while ($rows_ir = $result_ir->FetchRow()) {
		$i_r = $i_r.$rows_ir['notes'].'.  ';		
	}

	$fdiagnosis = '';
	$sql_fdiagnosis = 'select *from care_encounter_notes where type_nr = 14 and encounter_nr = '.$enc;
	$result_fdiagnosis = $db->Execute($sql_fdiagnosis);
	while ($rows_fdiagnosis = $result_fdiagnosis->FetchRow()) {
		$fdiagnosis = $fdiagnosis.$rows_fdiagnosis['notes'].'.  ';		
	}

	$treatment = '';
	$sql_treatment = 'select *from care_encounter_notes where type_nr = 9 and encounter_nr = '.$enc;
	$result_treatment = $db->Execute($sql_treatment);
	while ($rows_treatment = $result_treatment->FetchRow()) {
		$treatment = $treatment.$rows_treatment['notes'].'.  ';		
	}

	$prescription = '';
	// $sql_pre = 'select care_encounter_prescription.* from care_encounter_prescription inner join care_encounter on care_encounter.encounter_nr = care_encounter_prescription.encounter_nr where status = serviced and bill_status = billed and care_encounter.pid = '.$enc;

	$prescription_article = '';
	$prescription_type = '';
	$sql_prescription = "select * from care_encounter_prescription where encounter_nr = $enc and status = 'serviced'";
	$result_prescription = $db->Execute($sql_prescription);
	while ($row_pre = $result_prescription->FetchRow()) {
		$pre = $row_pre['article'].', ';
		$prescription_article = $prescription_article.$pre;
	}

	$chem_lab = '';
	$sql_chemlab = "select * from care_tz_diagnosis where encounter_nr = $enc";
	$result_chemlab = $db->Execute($sql_chemlab);
	while ($row_chemlab = $result_chemlab->FetchRow()) {
		$chem = $row_chemlab['ICD_10_description'].', ';
		$chem_lab = $chem_lab.$chem;
	}

	$user = $_SESSION['sess_login_username'];

?>