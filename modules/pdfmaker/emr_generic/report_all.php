<?php
//PDF USING MULTIPLE PAGES
//CREATED BY: Carlos Vasquez S.
//E-MAIL: cvasquez@cvs.cl
//CVS TECNOLOGIA E INNOVACION
//SANTIAGO, CHILE
// require('./roots.php');

include './fpdf/fpdf.php';
include './functions.php';

// global $db,$rows,$result,$enc;

$patientSQL = 'SELECT p.name_first, p.name_middle, p.name_last, p.pid from care_person as p inner join care_encounter as e ON p.pid = e.pid where e.encounter_nr = '.$enc;

$result=$db->Execute($patientSQL);

$patient = $result->FetchRow();

$name = $patient['name_first'].' '.$patient['name_middle'].' '.$patient['name_last'];
//Create new pdf file
$pdf=new FPDF();

//Disable automatic page break
$pdf->SetAutoPageBreak(true);

//Add first page
$pdf->AddPage();

//set initial y axis position per page
$y_axis_initial = 25;
$pdf->Image('amurt.jpg',20,10,-300);
$pdf->SetFont('Times','B',14);
$pdf->SetY(15);
$pdf->SetX(60);
$pdf->Write(0, 'AMURT HEALTH CARE CENTRE');
$pdf->SetY(20);
$pdf->SetX(60);
$pdf->Write(0, 'P.O BOX 29049 - 00625, NAIROBI');
$pdf->SetY(25);
$pdf->SetX(60);
$pdf->Write(0, '209 MOUNTAIN VIEW ESTATE');
$pdf->SetY(30);
$pdf->SetX(60);
$pdf->Write(0, 'Tel: 0716 693188');
$pdf->SetY(35);
$pdf->SetX(60);
$pdf->Write(0, 'Email: info@amurtafrica.org');
$pdf->SetY(40);
$pdf->SetX(60);
$pdf->Write(0, 'Website: www.amurtafrica.org');

$pdf->SetY(55);
$pdf->SetX(20);
$pdf->Write(0, 'Patient: '.$patient['pid'].' - '.$name);

$pdf->SetY(62);
$pdf->SetX(20);
$pdf->Write(0, 'Date: '.date('l jS F\, Y h:i:s A'));

$pdf->SetFont('Times','BIU');
$pdf->SetY(70);
$pdf->SetX(20);
$pdf->Write(0, 'Vital Signs');
//print column titles
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial','B',12);
$pdf->SetY(78);
$pdf->SetX(20);
$pdf->Cell(15,6,'BP',1,0,'L',1);
$pdf->Cell(20,6,'T',1,0,'L',1);
$pdf->Cell(20,6,'Pulse',1,0,'R',1);
$pdf->Cell(15,6,'RR',1,0,'R',1);
$pdf->Cell(25,6,'Weight(Kg)',1,0,'R',1);
$pdf->Cell(20,6,'Hgt.(cm)',1,0,'R',1);
$pdf->Cell(20,6,'BMI',1,0,'R',1);
$pdf->Cell(20,6,'LMP',1,0,'R',1);

$pdf->SetFont('Arial','B',12);
$pdf->SetY(84);
$pdf->SetX(20);
$pdf->Cell(15,6,$bp2.'/'.$bp1,1,0,'L',1);
$pdf->Cell(20,6,$temp,1,0,'L',1);
$pdf->Cell(20,6,$pr,1,0,'R',1);
$pdf->Cell(15,6,$rr,1,0,'R',1);
$pdf->Cell(25,6,$wht,1,0,'R',1);
$pdf->Cell(20,6,$hgt,1,0,'R',1);
$pdf->Cell(20,6,$bmi,1,0,'R',1);
$pdf->Cell(20,6,'N/A',1,0,'R',1);

	$pdf->SetFont('Times','BIU', 14);
	$pdf->SetY(97);
	$pdf->SetX(20);
	$pdf->Write(0, "Nurse's Notes");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(102);
	$pdf->SetX(20);
	$pdf->Write(0, $bp1notes);
	$pdf->SetY(106);
	$pdf->SetX(20);
	$pdf->Write(0, $temp_notes);
	$pdf->SetY(110);
	$pdf->SetX(20);
	$pdf->Write(0, $pr_notes);
	$pdf->SetY(114);
	$pdf->SetX(20);
	$pdf->Write(0, $rr_notes);
	$pdf->SetY(118);
	$pdf->SetX(20);
	$pdf->Write(0, $wht_notes);
	$pdf->SetY(122);
	$pdf->SetX(20);
	$pdf->Write(0, $hgt_notes);
	$pdf->SetY(126);
	$pdf->SetX(20);
	$pdf->Write(0, $bmi_notes);


	$pdf->SetFont('Times','U', 14);
	$pdf->SetY(133);
	$pdf->SetX(20);
	$pdf->Write(0, "Doctor's Notes");

	$pdf->SetFont('Times','IU', 12);
	$pdf->SetY(138);
	$pdf->SetX(20);
	$pdf->Write(0, "Chief Complaints");

	$pdf->SetFont('Times','', 12);
	$pdf->SetY(143);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $doctor_notes, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(196);
	$pdf->SetX(20);
	$pdf->Write(0, "HPI");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(201);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $hpi_notes, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(241);
	$pdf->SetX(20);
	$pdf->Write(0, "Systemic Enquiry:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(246);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $systemic_notes, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(270);
	$pdf->SetX(20);
	$pdf->Write(0, "Physical Examination Findings:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(275);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $pef, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(310);
	$pdf->SetX(20);
	$pdf->Write(0, "Main System Findings:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(15);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $main_system_findings, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(55);
	$pdf->SetX(20);
	$pdf->Write(0, "Other System Findings:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(60);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $other_system_findings, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(80);
	$pdf->SetX(20);
	$pdf->Write(0, "Impression:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(85);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $impression, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(115);
	$pdf->SetX(20);
	$pdf->Write(0, "Investigations and Results:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(120);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $i_r, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(165);
	$pdf->SetX(20);
	$pdf->Write(0, "Final Diagnosis(es):");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(170);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $chem_lab, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','BU', 14);
	$pdf->SetY(200);
	$pdf->SetX(20);
	$pdf->Write(0, "Treatment:");
	$pdf->SetFont('Times','', 12);
	$pdf->SetY(205);
	$pdf->SetX(20);
	$pdf->MultiCell(160, 5, $prescription_article, 0, "L", 1, 5, 5, 5, 5 );

	$pdf->SetFont('Times','B', 14);
	$pdf->SetY(280);
	$pdf->SetX(20);
	$pdf->Write(0, "Doctor's Name:  $user");


$y_axis = $y_axis + $row_height;

//Select the Products you want to show in your PDF file
$result=mysql_query('select Code,Name,Price from Products ORDER BY Code',$link);

//initialize counter
$i = 0;

//Set maximum rows per page
$max = 25;

//Set Row Height
$row_height = 6;

while($row = mysql_fetch_array($result))
{
	//If the current row is the last one, create new page and print column title
	if ($i == $max)
	{
		$pdf->AddPage();

		//print column titles for the current page
		$pdf->SetY($y_axis_initial);
		$pdf->SetX(25);
		$pdf->Cell(30,6,'CODE',1,0,'L',1);
		$pdf->Cell(100,6,'NAME',1,0,'L',1);
		$pdf->Cell(30,6,'PRICE',1,0,'R',1);
		
		//Go to next row
		$y_axis = $y_axis + $row_height;
		
		//Set $i variable to 0 (first row)
		$i = 0;
	}

	$code = $row['Code'];
	$price = $row['Price'];
	$name = $row['Code'];

	$pdf->SetY($y_axis);
	$pdf->SetX(25);
	$pdf->Cell(30,6,$code,1,0,'L',1);
	$pdf->Cell(100,6,$name,1,0,'L',1);
	$pdf->Cell(30,6,$price,1,0,'R',1);

	//Go to next row
	$y_axis = $y_axis + $row_height;
	$i = $i + 1;
}


mysql_close($link);

//Send file
$pdf->Output();
?>
