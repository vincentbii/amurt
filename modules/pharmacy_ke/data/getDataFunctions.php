
<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
require_once($root_path . 'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path . 'include/inc_init_xmlrpc.php');

$weberp_obj = new_weberp();

$limit = $_REQUEST[limit];
$start = $_REQUEST[start];
$formStatus = $_REQUEST[formStatus];
$searchParam = $_REQUEST[sParam];
$searchParam2 = $_REQUEST[sParam2];

$item_number = $_REQUEST[partcode];
$partcode = $_REQUEST[partcode];
$item_description = $_REQUEST[item_description];
$item_full_description = $_REQUEST[item_full_description];
$unit_price = $_REQUEST[unit_price];
$purchasing_class = $_REQUEST[purchasing_class];
$category = $_REQUEST[category];
$itemStatus = $_REQUEST[item_status];
$sellingPrice = $_REQUEST[selling_price];
$maximum = $_REQUEST[maximum];
$minimum = $_REQUEST[minimum];
$reorder = $_REQUEST[reorder];
$unitMeasure = $_REQUEST[unit_measure];
$gl_sales_acct = $_REQUEST[gl_sales_acct];
$gl_inventory_acct = $_REQUEST[gl_inventory_acct];
$gl_costofsales_acct = $_REQUEST[gl_costofsales_acct];
//$category=GetItemCategory($_REQUEST[partcode]);

$pid=$_REQUEST[pid];
$encounterNo=$_REQUEST[encNo];

$units = 'each';

$task = ($_REQUEST['task']) ? ($_REQUEST['task']) : '';

switch ($task) {
    case 'getPatientDetails':
        getPatientDetails();
        break;
    case "deleteItemLocation":
        deleteItemLocation();
        break;
    case "saveiItems":
        SaveItems($_POST);
        break;
    case "saveSetupItems":
        if($_POST['txtActions']=='Insert'){
            saveSetupItems($_POST);
        }else if($_POST['txtActions']=='Update') {
            updateSetupItems($_POST);
        }

        break;
    case "deleteSetupItem":
        deleteSetupItems();
        break;
    case "getDispensingLocations":
        getDispensingLocations($start,$limit);
        break;
    case "getItemsList":
        getItemsList($searchParam, $searchParam2 ,$start, $limit);
        break;
    case "InsertItem":
        if ($formStatus == 'insert') {
            InsertItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $gl_sales_acct, $gl_inventory_acct, $gl_costofsales_acct, $weberp_obj);
        } else if ($formStatus == 'update') {
            updateItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $gl_sales_acct, $gl_inventory_acct, $gl_costofsales_acct, $weberp_obj);
        }
        break;
    case "getItemsCategory":
        getItemsCategory($start, $limit);
        break;
    case "getItemsSubCategory":
        getItemsSubCategory($start, $limit);
        break;
    case "getUnitsofMeasure":
        getUnitsofMeasure($start, $limit);
        break;
    case "getStoreLocations":
        getStoreLocations($start, $limit);
        break;
    case "getItemLocation":
        getItemLocations($start, $limit);
        break;
    case "getCashPoints":
        getCashPoints($start, $limit);
        break;
    case "getPaymentModes":
        getPaymentModes($start, $limit);
        break;
    case "getCurrentPos":
        getCurrentPos($start, $limit);
        break;
    case "getSalesItems":
        getSalesItems($start, $limit);
        break;
    case "SaveReceipt":
        SaveReceipt($start, $limit);
        break;
    case "deleteItem":
        deleteItem();
        break;
    case "getGLAccounts":
        getGLAccounts();
        break;
    case "getItemPrices":
        getItemPrices($partcode);
        break;
    case 'getPriceTypes':
        getPriceTypes();
        break;
    case 'getPrescriptionItems':
        getPrescriptionItems($pid,$encounterNo);
        break;
    case 'getItemDescription':
        getItemDescription($strParam);
        break;
    case 'getEncountersList':
        getEncountersList();
        break;
    case 'itemDescription';
        getItemDescription($item_description);
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch
//0727588512


function getItemDescription($strParam){
    global $db;
    $debug=false;

    $sql="Select Item_Description from care_tz_drugsandservices where purchasing_class in('drug_list','Medical-Supplies')";

    if(isset($strParam) && $strParam<>''){
        $sql.=" and item_description like '$strParam'";
    }
    $results=$db->Execute($sql);

    $total=$results->RecordCount();

    echo '{
    "total":"' . $total . '","itemDescription":[';

    $counter = 0;
    while ($row = $results->FetchRow()) {
        echo '{"Description":"' . $row[Item_Description] . '"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }

    }
    echo ']}';
}

function getPrescriptionItems($pid,$encounterNo){
    global $db;
    $debug=false;

    $sql="SELECT pr.*, e.encounter_class_nr,service.`unit_price` FROM care_encounter AS e, care_person AS p, care_encounter_prescription AS pr,
    care_tz_drugsandservices as service WHERE p.pid=$pid AND p.pid=e.pid AND e.encounter_nr=$encounterNo AND e.encounter_nr=pr.encounter_nr AND
    service.item_id=pr.article_item_number AND service.is_labtest=0 AND pr.drug_class in ('drug_list') AND pr.status='pending' ORDER BY pr.modify_time DESC";

    if($debug) echo $sql;

    $results=$db->Execute($sql);

    $total=$results->RecordCount();

    echo '{
    "total":"' . $total . '","prescriptionList":[';
    $counter = 0;
    while ($row = $results->FetchRow()) {
        $cost=$row[unit_price]*$row[dosage]*$row[times_per_day]*$row[days];

        echo '{"PrescribeDate":"' . $row[prescribe_date] . '","EncounterNo":"' . $row[encounter_nr]. '","PrescNo":"' . $row[nr]. '","Partcode":"' . $row[partcode]
            . '","Description":"' . $row[article]. '","Dosage":"' . $row[dosage]. '","TimesPerDay":"' . $row[times_per_day]
            . '","Days":"' . $row[days]. '","Cost":"' . $cost. '","Prescriber":"' . $row[prescriber]. '"}';
        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}


function getPatientDetails(){
    global $db;
    $debug=false;

    $sql="SELECT p.pid, p.selian_pid, CONCAT(p.name_first,' ',p.name_2,' ', p.name_last) AS pnames, pr.encounter_nr, pr.prescribe_date,
            p.pid AS batch_nr,e.encounter_class_nr FROM care_encounter_prescription pr
                INNER JOIN care_encounter e ON pr.encounter_nr = e.encounter_nr
                AND (pr.status='pending' OR pr.status='')
                INNER JOIN care_person p ON e.pid = p.pid
                INNER JOIN care_tz_drugsandservices d ON pr.article_item_number=d.item_id
                AND (pr.drug_class = 'drug_list' OR d.purchasing_class ='Medical-Supplies'
                OR d.purchasing_class ='Dental-Services' AND pr.article NOT LIKE '%consult%')
                GROUP BY e.encounter_class_nr ,pr.prescribe_date, pr.encounter_nr, p.pid,
                p.selian_pid, name_first, name_last ORDER BY pr.prescribe_date ASC";

    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $total=$results->RecordCount();

    echo '{
    "total":"' . $total . '","patientslist":[';
    $counter = 0;
    while ($row = $results->FetchRow()) {
        if($row[encounter_class_nr]=='1'){
            $encClass='IP';
        }else{
            $encClass='OP';
        }
        echo '{"PID":"' . $row[pid] . '","File_No":"' . $row[selian_pid]. '","PNames":"' . $row[pnames]. '","EncounterNo":"' . $row[encounter_nr]
            . '","Prescription_Date":"' . $row[prescribe_date]. '","Encounter_Class":"' . $encClass. '"}';
        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}


function getEncountersList(){
    global $db;
    $debug=false;

    $encClass=$_REQUEST[encounterClass];

    if($encClass=='Inpatients'){
        $sql="SELECT c.`pid`,CONCAT(c.`name_first`,' ', c.`name_2`,' ', c.`name_last`) AS PNames, c.`date_birth`, c.`sex`,
                d.`encounter_date`,d.encounter_nr,d.`current_ward_nr`,w.`name` as dept
            FROM care_person c
            INNER JOIN care_encounter d ON c.pid=d.pid
            LEFT JOIN care_ward w ON d.`current_ward_nr`=w.`nr`
            WHERE d.encounter_class_nr=1 ORDER BY d.current_ward_nr ASC";
    }else{
        $sql = "SELECT c.`pid`, CONCAT(c.`name_first`,' ', c.`name_2`,' ', c.`name_last`) AS PNames, c.`date_birth`, c.`sex`
                    ,e.`name_formal` as dept, d.`encounter_date`,d.encounter_nr
                FROM care_person c
                inner join care_encounter d on c.pid=d.pid inner join care_department e on e.nr=d.current_dept_nr
                where e.`type`=1 and d.encounter_class_nr=2 and d.is_discharged=0 order by c.pid asc";
    }

    if($debug) echo $sql;

    $result=$db->Execute($sql);
    $numRows=$result->RecordCount();
    echo '{
    "encounterslist":[';
    $counter=0;
    while ($row = $result->FetchRow()) {
        echo '{"Pid":"'. $row[pid].'","PNames":"'. $row[PNames].'","Department":"'. $row[dept].'","EncounterNo":"'. $row[encounter_nr].'","date_birth":"'. $row[date_birth]
            .'","sex":"'. $row[sex] .'","EncounterDate":"'. $row[encounter_date].'"}';
        if ($counter<>$numRows){
            echo ",";
        }
        $counter++;
    }
    echo ']}';
}

function getGLAccounts() {
    global $db;
    $debug = true;

    $sql = "Select accountcode,accountname from chartmaster";
    $request = $db->Execute($sql);

    $total = $request->RecordCount();

    echo '{
    "total":"' . $total . '","glaccounts":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"accountcode":"' . $row[accountcode] . '","accountname":"' . $row[accountname] . '"}';


        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function deleteItemLocation() {
    global $db;
    $debug = true;

    $stockid = $_REQUEST['stockid'];
    $loccode = $_REQUEST['loccode'];

    $sql = "DELETE FROM CARE_KE_LOCSTOCK WHERE stockid='$stockid' and loccode='$loccode'";

    if ($debug) {
        echo $sql;
    }
    if ($db->Execute($sql)) {
        echo '{success:true}';
    } else {
        echo "{'failure':'true','stockid':'$stockid'}";
    }
}

function saveItems($itemslist) {
    global $db;
    $debug = false;

    $table = $itemslist['formtype'];
    unset($itemslist['formtype']);
    $partcode = $itemslist['partcode'];

    foreach ($itemslist as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }

    $sql = 'INSERT INTO ' . $table . ' (' . substr($FieldNames, 0, -2) . ') ' .
            'VALUES (' . substr($FieldValues, 0, -2) . ') ';

    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
        transmitWeberp($itemsList, $stid = true);
        echo '{success:true}';
    } else {
        echo "{'failure':'true','partcode':'$partcode'}";
    }
}

function deleteItems($itemslist) {
    global $db;
    $debug = false;

    $table = $itemslist['formtype'];
    unset($itemslist['formtype']);

    foreach ($itemslist as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }

    $sql = 'INSERT INTO ' . $table . ' (' . substr($FieldNames, 0, -2) . ') ' .
            'VALUES (' . substr($FieldValues, 0, -2) . ') ';

    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
        echo '{success:true}';
    } else {
        echo "{'failure':'true','partcode':'$partcode'}";
    }
}


function saveSetupItems($itemslist) {
    global $db;
    $debug = true;

    $table = $itemslist['formtype'];

    if($table=='care_tz_categories') unset($itemslist['ID']);
    if($table=='care_units_measurement') unset($itemslist['ID']);

    unset($itemslist['txtActions']);
    unset($itemslist['formtype']);
    $partcode = $itemslist['partcode'];

    foreach ($itemslist as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }

    $sql = 'INSERT INTO ' . $table . ' (' . substr($FieldNames, 0, -2) . ') ' .
        'VALUES (' . substr($FieldValues, 0, -2) . ') ';

    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
//        transmitWeberp($itemsList, $stid = true);
        echo '{success:true}';
    } else {
        echo "{'failure':'true','partcode':'$partcode'}";
    }
}

function updateSetupItems($itemslist) {
    global $db;
    $debug=false;

    $table = $itemslist['formtype'];

    if($table=='care_tz_categories') $id='ID';
    if($table=='care_tz_itemscat') $id='catID';
    if($table=='care_unit_measurement') $id='ID';
    if($table=='care_tz_categories') $id='ID';
    if($table=='care_ke_stlocation') {
        $id='st_id';
      //  unset($itemslist['st_id']);
    }
    unset($itemslist['txtActions']);
    unset($itemslist['formtype']);
    //unset($itemslist['ID']);

    $sql="UPDATE $table SET ";
    foreach ($itemslist as $key => $value) {
        $sql .= $key.'="'.$value.'", ';
    }

    $sql = substr($sql,0,-2)." WHERE $id='$itemslist[$id]'";

    if($debug) echo $sql;

    if ($db->Execute($sql)) {
        $results = '{success: true }';
    } else {
        $results = "{success: false,errors:{clientNo:'Could not delete item, Please check your values'}}"; // Return the error message(s)

    }
    echo $results;
}



function deleteSetupItems() {
    global $db;
    $debug=false;

    $table = $_REQUEST['table'];
    $ID=$_REQUEST['ID'];

    if($table=='care_tz_categories') $id='ID';
    if($table=='care_tz_itemscat') $id='catID';
    if($table=='care_unit_measurement') $id='ID';
    if($table=='care_tz_categories') $id='ID';
    if($table=='care_ke_stlocation') {
        $id='st_id';
    }

    $sql="DELETE FROM  $table WHERE $id='$ID'";

    if($debug) echo $sql;

    if ($db->Execute($sql)) {
        $results = '{success: true }';
    } else {
        $results = "{failure: true,error:'Could not delete item Please check your values'}"; // Return the error message(s)

    }
    echo $results;
}


function getSalesItems($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT  d.partcode,d.`item_description`,d.`unit_price`,l.`quantity`,l.`loccode` FROM care_tz_drugsandservices d 
           INNER JOIN care_ke_locstock l
           ON d.`partcode`=l.`stockid` WHERE d.`purchasing_class` LIKE 'drug%' AND l.`loccode`='main'";

    if ($start <> '' && $limit <> '')
        $sql.=" limit $start,$limit";

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "SELECT  COUNT(d.partcode) AS partcode FROM care_tz_drugsandservices d 
           INNER JOIN care_ke_locstock l
           ON d.`partcode`=l.`stockid` WHERE d.`purchasing_class` LIKE 'drug%' AND l.`loccode`='main'";

    $request2 = $db->Execute($sqlTotal);

    $row = $request2->FetchRow();
    $total = $row[0];

    echo '{
    "total":"' . $total . '","salesItems":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {
        if ($row[unit_price] <> '') {
            $price = $row[unit_price];
        } else {
            $price = 0;
        }

        if ($row[quantity] <> '') {
            $qty = $row[quantity];
        } else {
            $qty = 0;
        }
        echo '{"itemcode":"' . $row[partcode] . '","description":"' . $row[item_description] . '","qty":' . $qty
        . ',"loccode":"' . $row[loccode] . '","price":' . $price . '}';



        if ($counter < $total) {
            echo ",";
        }
        $counter++;
    }
    echo ']}';
//    echo "<br><br><br>".$counter;
}

function getPriceTypes() {
    global $db;
    $debug = FALSE;

    $sql = "Select ID,PriceType from care_ke_priceTypes ";

    if ($debug) {
        echo $sql;
    }

    $results = $db->Execute($sql);
    $total = $results->RecordCount();

    echo '{
    "total":"' . $total . '","pricetypes":[';
    $counter = 0;
    while ($row = $results->FetchRow()) {
        echo '{"ID":"' . $row[ID] . '","PriceType":"' . $row[PriceType] . '"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function insertNewPrice($rctData) {
    global $db;
    $debug = true;

    unset($rctData['ID']);
    foreach ($rctData as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }

    $sql = 'INSERT INTO care_ke_prices (' . substr($FieldNames, 0, -2) . ') ' .
            'VALUES (' . substr($FieldValues, 0, -2) . ') ';
    if ($debug) {
        echo $sql;
    }
    if ($db->Execute($sql)) {
        echo "{success:true}";
    } else {
        echo "{failure:true}";
    }
}

function getItemPrices($partcode) {
    global $db;
    $debug = FALSE;

    $json = $_REQUEST['writePrices'];
    $rctData = json_decode($json, TRUE);

    if (count($rctData) > 0) {
        insertNewPrice($rctData);
    }


    $sql = "Select p.ID,t.PriceType,PartCode,Price from care_ke_prices p left join care_ke_priceTypes t on p.priceType=t.ID ";

    if (isset($partcode)) {
        $sql.=" where partcode='$partcode'";
    }

    if ($debug) {
        echo $sql;
    }

    $results = $db->Execute($sql);
    $total = $results->RecordCount();

    echo '{
    "total":"' . $total . '","itemPrices":[';
    $counter = 0;
    while ($row = $results->FetchRow()) {

        echo '{"ID":"' . $row[ID] . '","PriceType":"' . $row[PriceType] . '","PartCode":"' . $row[PartCode]
        . '","Price":"' . $row[Price] . '"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function getItemsList($searchParam,$searchParam2, $start, $limit) {
    global $db;
    $debug = false;
    $category = $_REQUEST[category];

    $sql = "SELECT partcode,item_description,item_full_description,`unit_measure`,unit_price,unit_price_1,selling_price,
            purchasing_class, c.`item_Cat` AS category,item_status,`reorder`,`minimum`,maximum,gl_sales_acct,gl_inventory_acct,gl_costofsales_acct
            FROM care_tz_drugsandservices d LEFT JOIN  care_tz_itemscat c
            ON d.`category`=c.`catID` WHERE partcode <> ''";

    
    if (isset($searchParam2) && $searchParam2=='partcode') {
        $sql.=" and partcode like '%$searchParam%'";
    }else if(isset($searchParam2) && $searchParam2=='descriptions'){
        $sql.=" and item_description like '%$searchParam%'";
    }else{
        $sql.=" and item_description like '%$searchParam%'";
    }

    if (isset($category)) {
        $sql.=" and category='$category'";
    }



    if (isset($start) && isset($limit)) {
        $sql.=" limit $start,$limit";
    }

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $total = $request->RecordCount();


    echo '{
    "total":"' . $total . '","itemsList":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"partcode":"' . $row[partcode] . '","item_description":"' . $row[item_description] . '","item_full_description":"' . $row[item_full_description]
        . '","unit_measure":"' . $row[unit_measure] . '","unit_price":"' . $row[unit_price] . '","selling_price":"' . $row[selling_price]
        . '","purchasing_class":"' . $row[purchasing_class] . '","category":"' . $row[category]
        . '","item_status":"' . $row[item_status] . '","reorder":"' . $row[reorder] . '","minimum":"' . $row[minimum]
        . '","maximum":"' . $row[maximum] . '","gl_sales_acct":"' . $row[gl_sales_acct] . '","gl_inventory_acct":"' . $row[gl_inventory_acct]
        . '","gl_costofsales_acct":"' . $row[gl_costofsales_acct] . '"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
//    echo "<br><br><br>".$counter;
}

function getUnitsofMeasure($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select id,`name` from care_unit_measurement order by `name` asc";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);


    $total = $request->RecordCount();

    echo '{
    "total":"' . $total . '","unitsMeasure":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[id] . '","name":"' . $row[name] . '"}';


        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function getItemsCategory($start, $limit) {
    global $db;
    $debug = false;

    $sql = "select ID,catName from care_tz_categories order by catName asc";

    if (isset($start) && isset($limit)) {
        $sql.=" limit $start,$limit";
    }

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(ID) as ccount from care_tz_categories";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
    "total":"' . $total . '","itemsCategory":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"ID":"' . $row[ID] . '","catName":"' . $row[catName] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getItemsSubCategory($start, $limit) {
    global $db;
    $debug = false;
    $category = $request[query];

    $sql = "select catID,item_cat,parentID ,c.`catName` FROM care_tz_itemscat s LEFT JOIN care_tz_categories c
            ON s.`parentID`=c.`ID`";

//    if(isset($category)){
//        $sql.=" where item_cat= like '$category%'";
//    }

    $sql.=" order by item_cat asc";

    if (isset($start) && isset($limit)) {
        $sql.=" limit $start,$limit";
    }

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(catID) as ccount from care_tz_itemscat";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
    "total":"' . $total . '","itemsSubCategory":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"catID":"' . $row[catID] . '","item_cat":"' . trim($row[item_cat]). '","parentID":"' . $row[catName] . '"}';

        $counter++;
        if ($counter <> $total) {
            echo ",";
        }
    }

    echo ']}';
}

function getStoreLocations($start, $limit) {
    global $db;
    $debug = false;

    $sql = "SELECT st_id,st_name,store,mainStore FROM care_ke_stlocation";

    if (isset($start) && isset($limit)) {
        $sql.=" limit $start,$limit";
    }

    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);

    $sqlTotal = "select count(st_id) as ccount from  care_ke_stlocation ";

    $request2 = $db->Execute($sqlTotal);

    $row2 = $request2->FetchRow();
    $total = $row2[0];

    echo '{
    "total":"' . $total . '","storeLocations":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"st_id":"' . $row[st_id] . '","st_name":"' . $row[st_name] . '","store":"' . $row[store] . '","mainStore":"' . $row[mainStore] . '"}';


        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function updateItemLocation($rctData, $partcode) {
    global $db;
    $debug = false;

    foreach ($rctData as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }

    $sql = "update care_ke_locstock set
	quantity='$rctData[quantity]'
        where stockid='$rctData[stockid]' and loccode='$rctData[loccode]'";

    if ($debug) {
        echo $sql;
    }
    if ($db->Execute($sql)) {
        echo "{success:true}";
    } else {
        echo "{failure:true}";
    }
}

function insertNewItemLocation($rctData) {
    global $db;
    $debug = true;

    foreach ($rctData as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }

    $sql = 'INSERT INTO care_ke_locstock (' . substr($FieldNames, 0, -2) . ') ' .
            'VALUES (' . substr($FieldValues, 0, -2) . ') ';
    if ($debug) {
        echo $sql;
    }
    if ($db->Execute($sql)) {
        echo "{success:true}";
    } else {
        echo "{failure:true}";
    }
}

function verifyItemLocation($partcode, $loccode) {
    global $db;
    $debug = false;

    $sql = "Select * from care_ke_locstock where stockid='$partcode' and loccode='$loccode'";
    if ($debug)
        echo $sql;

    $results = $db->Execute($sql);
    $rcount = $results->RecordCount();
    if ($rcount > 0) {
        return true;
    } else {
        return false;
    }
}

function getItemLocations($start, $limit) {
    global $db;
    $debug = false;
    $partcode = $_REQUEST[partcode];

    $json = $_REQUEST['editData'];
    $rctData = json_decode(file_get_contents("php://input"), true);

    $loccode = $rctData[stockid];

   // echo count($rctData)." records was updated";
    
    if (count($rctData) > 0) {
        
        if (verifyItemLocation($rctData[stockid], $rctData[loccode])) {
           // echo 'item found '. verifyItemLocation($rctData[stockid], $rctData[loccode]);
            updateItemLocation($rctData, $partcode);
        } else {
           // echo 'item found '. verifyItemLocation($rctData[stockid], $rctData[loccode]);
            insertNewItemLocation($rctData);
        }
        getItemLocations2();
    } else {
        getItemLocations2();
    }
}

    function getItemLocations2() {
        global $db;
        $debug = false;
        $partcode = $_REQUEST[partcode];
        $sql = "SELECT stockid AS itemcode,loccode,quantity FROM care_ke_locstock where stockid='$partcode'";
        if ($debug)
            echo $sql;

        $request = $db->Execute($sql);

        $sqlTotal = "select count(stockid) as ccount FROM care_ke_locstock where stockid='$partcode'";

        $request2 = $db->Execute($sqlTotal);

        $row2 = $request2->FetchRow();
        $total = $row2[0];

        echo '{
    "total":"' . $total . '","itemLocations":[';
        $counter = 0;
        while ($row = $request->FetchRow()) {

            echo '{"stockid":"' . $row[itemcode] . '","loccode":"' . $row[loccode] . '","quantity":"' . $row[quantity] . '"}';

            if ($counter <> $total) {
                echo ",";
            }
            $counter++;
        }

        echo ']}';
    }



function getDispensingLocations($start, $limit) {
    global $db;
    $debug = false;
    $partcode = $_REQUEST[partcode];

    $sql = "SELECT st_id,st_name,store,mainstore FROM `care_ke_stlocation` WHERE store=1 AND Dispensing=1";
    if ($debug)
        echo $sql;

    $request = $db->Execute($sql);
    $total = $request->RecordCount();

    echo '{
    "total":"' . $total . '","dispenseLocations":[';
    $counter = 0;
    while ($row = $request->FetchRow()) {

        echo '{"st_id":"' . $row[st_id] . '","st_name":"' . $row[st_name] . '"}';

        if ($counter <> $total) {
            echo ",";
        }
        $counter++;
    }

    echo ']}';
}

function GetItemCategory($partcode) {
    global $db;
    $sql = "SELECT category FROM care_tz_drugsandservices where partcode='$partcode';";
//   echo $sql;
    $result = $db->Execute($sql);
    $myrow = $result->FetchRow();
    return $myrow[0];
}

function checkStockid($partcode) {
    global $db;
    $debug=true;
    
    $psql = 'select count(item_number) as pcount from care_tz_drugsandservices where partcode="' . $partcode . '"';
    if($debug) echo $psql;
    $presult = $db->Execute($psql);
    $row = $presult->FetchRow();
    return $row[0];
}



function InsertItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $gl_sales_acct, $gl_inventory_acct, $gl_costofsales_acct, $weberp_obj) {
    global $db;
    $debug = false;

    $stid = checkStockid($partcode);

    if ($stid > 0) {
        echo "{'failure':'true','errNo':'1','partcode':'$partcode'}";
    } else {
        $sql = 'insert into care_tz_drugsandservices(item_number, 
                partcode, item_description, item_full_description, unit_price, purchasing_class,category,
                reorder,minimum,maximum,unit_measure,selling_price,item_status, gl_sales_acct,gl_inventory_acct, gl_costofsales_acct)
                values("' . $category . '", "' . $partcode . '", "' . $item_description . '", "' . $item_full_description . '", 
                "' . $unit_price . '", "' . $purchasing_class . '","' . $category . '","' . $reorder . '","' . $minimum . '","' . $maximum . '","' . $unitMeasure
                . '","' . $sellingPrice . '","' . $itemStatus . '","' . $gl_sales_acct . '","' . $gl_inventory_acct . '","' . $gl_costofsales_acct . '")';
        if ($debug)
            echo $sql;
        if ($db->Execute($sql)) {
            if ($weberp_obj = new_weberp()) {
                transmitWeberp($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $stid = true);
            } else {

                $results = '{success: false }';
            }
        } else {
            echo "{'failure':'true','partcode':'$partcode'}";
        }
    }
}

function updateItem($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $gl_sales_acct, $gl_inventory_acct, $gl_costofsales_acct, $weberp_obj) {
    global $db;
    $debug = false;

   // $stid = checkStockid($partcode);

    $sql = 'update care_tz_drugsandservices set item_number = "' . $partcode . '" , item_description = "' . $item_description . '" ,
	item_full_description = "' . $item_full_description . '" ,unit_price = "' . $unit_price . '" ,
	purchasing_class ="' . $purchasing_class . '",category="' . $category
            . '",reorder="' . $reorder . '",minimum="' . $minimum . '",maximum="' . $maximum . '",unit_measure="' . $unitMeasure
            . '",selling_price="' . $sellingPrice . '",item_status="' . $itemStatus
            . '",gl_sales_acct="' . $gl_sales_acct . '",gl_inventory_acct="' . $gl_inventory_acct . '",gl_costofsales_acct="' . $gl_costofsales_acct
            . '" where partcode ="' . $partcode . '"';

    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
        if ($weberp_obj = new_weberp()) {
            transmitWeberp($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $stid, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $stid = false);
        } else {

            $results = '{failure: true }';
        }
    } else {
        echo "{'failure':'true','partcode':'$partcode'}";
    }
}

function transmitWeberp($partcode, $item_description, $item_full_description, $unit_price, $purchasing_class, $category, $itemStatus, $sellingPrice, $maximum, $minimum, $reorder, $unitMeasure, $stid) {

    //$itemCat=GetItemCategory($partcode);

    $itemdata[stockid] = $partcode;
    $itemdata[categoryid] = $category;
    $itemdata[description] = $item_description;
    $itemdata[longdescription] = $item_full_description;
//    $itemdata[units] = $unitMeasure;
//    $itemdata[mbflag] = 'B';
//    $itemdata[lastcurcostdate] = date('Y-m-d');
    $itemdata[actualcost] = $unit_price;
    $itemdata[lastcost] = $unit_price;

    if ($weberp_obj = new_weberp()) {
        if ($stid) {
            $weberp_obj->create_stock_item_in_webERP($itemdata);
            $results = '{success: true }';
        } else {
            $weberp_obj->modify_stock_item_in_webERP($itemdata);
            $results = '{success: true }';
        }
    } else {

        $results = '{failure: true }';
    }
    echo $results;
}

function stockAdjust($item_number) {
    global $db;
    $item_number = $_REQUEST[item_number];
    $item_description = $_REQUEST[item_Description];
    $qty = $_REQUEST[quantity];
    $roorder = $_REQUEST[reorderlevel];
    $loccode = $_REQUEST[loccode];
    $adjDesc = $_REQUEST[comment];


    $sql = 'select quantity from care_ke_locstock where stockid="' . $item_number . '" and loccode="' . $loccode . '"';
    $result = $db->Execute($sql);
    $row = $result->FetchRow();

    $csql = "update care_ke_locstock set
	quantity='$qty',
        reorderlevel='$roorder',
        comment='$adjDesc'
        where stockid='$item_number' and loccode='$loccode'";

    $ksql = "insert into care_ke_adjustments
	(item_number, prev_qty, new_qty, user, adjDate, adjTime, Reason)
	values( '" . $item_number . "', '" . $row[0] . "', '" . $qty . "', 'admin',
            '" . date('Y-m-d') . "', '" . date('H:i:s') . "', '" . $adjDesc . "')";

    if ($db->Execute($ksql)) {
        $db->Execute($csql);
        $results = '{success: true }';
    } else {
        // Errors. Set the json to return the fieldnames and the associated error messages
        $results = '{success: false, sql:' . $ksql . '}'; // Return the error message(s)
//        echo $sql;
    }

    echo $results;
}

function deleteItem() {
    global $db;
    $debug = false;
    $partcode = $_POST[partcode];
    $sql = "DELETE FROM care_ke_locstock where stockid='$partcode'";
    if ($debug)
        echo $sql;
    if ($db->Execute($sql)) {
        $sql1 = "INSERT INTO `care_tz_drugsandservices_del`
            (`item_number`,`partcode`, `is_pediatric`,`is_adult`, `is_other`,`is_consumable`,`is_labtest`,
             `item_description`,`item_full_description`,`unit_price`,`unit_price_1`,`unit_price_2`,`unit_price_3`,
             `purchasing_class`,`category`,`reorder`,`minimum`, `maximum`,`subCategory`,`unit_measure`, `selling_price`,`item_status`)
	SELECT `item_number`,`partcode`,`is_pediatric`,`is_adult`,`is_other`,`is_consumable`, `is_labtest`, `item_description`,
	  `item_full_description`,`unit_price`,`unit_price_1`,`unit_price_2`,`unit_price_3`, `purchasing_class`, `category`,
	  `reorder`, `minimum`, `maximum`,`subCategory`,`unit_measure`, `selling_price`,`item_status`
	FROM `care_tz_drugsandservices`  WHERE partcode='$partcode'";
        if ($debug)
            echo $sql1;
        if ($db->Execute($sql1)) {
            $query = 'DELETE FROM care_tz_drugsandservices WHERE partcode = "' . $partcode . '"';
            if ($debug)
                echo $query;
            if ($db->Execute($query)) { //returns number of rows deleted
                echo 'Item Successfully deleted from the database';
            } else {
                echo "Unable to delete item from table, Please Contact System Admin";
            }
        } else {
            echo "Unable to log deleted item, Please Contact System Admin";
        }
    } else {
        echo "unable to delete item in store locations,Consult System Admin";
    }
}

?>
