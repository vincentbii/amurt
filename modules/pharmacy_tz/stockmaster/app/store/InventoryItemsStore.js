/*
 * File: app/store/InventoryItemsStore.js
 *
 * This file was generated by Sencha Architect version 4.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Inventory.store.InventoryItemsStore', {
    extend: 'Ext.data.Store',
    alias: 'store.inventoryitemsstore',

    requires: [
        'Inventory.model.InventoryItems',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: true,
            model: 'Inventory.model.InventoryItems',
            remoteSort: true,
            storeId: 'InventoryItemsStore',
            buffered: true,
            leadingBufferZone: 100,
            pageSize: 500,
            purgePageCount: 10,
            proxy: {
                type: 'ajax',
                url: 'data/getDataFunctions.php?task=getItemsList',
                reader: {
                    type: 'json',
                    idProperty: 'partcode',
                    root: 'itemsList'
                },
                writer: {
                    type: 'json',
                    encode: true,
                    root: 'editData'
                }
            }
        }, cfg)]);
    }
});