<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 5/11/2015
 * Time: 1:52 PM
 */


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
require_once 'spreadsheet/Excel/Writer.php';
// Lets define some custom colors codes
define('CUSTOM_DARK_BLUE', 20);
define('CUSTOM_BLUE', 21);
define('CUSTOM_LIGHT_BLUE', 22);
define('CUSTOM_YELLOW', 23);
define('CUSTOM_GREEN', 24);

$workbook = new Spreadsheet_Excel_Writer();

$worksheet = $workbook->addWorksheet('Stock Items');

$workbook->setCustomColor(CUSTOM_DARK_BLUE, 31, 73, 125);
$workbook->setCustomColor(CUSTOM_BLUE, 0, 112, 192);
$workbook->setCustomColor(CUSTOM_LIGHT_BLUE, 184, 204, 228);
$workbook->setCustomColor(CUSTOM_YELLOW, 255, 192, 0);
$workbook->setCustomColor(CUSTOM_GREEN, 0, 176, 80);

//$worksheet->hideScreenGridlines();
//custom styles
$formatHeader = &$workbook->addFormat();
$formatHeader = &$workbook->addFormat(
    array('Size' => 16,
        'VAlign' => 'vcenter',
        'HAlign' => 'center',
        'Bold' => 1,
        'Color' => 'white',
        'FgColor' => CUSTOM_DARK_BLUE));

$formatReportHeader =
        &$workbook->addFormat(
    array('Size' => 9,
        'VAlign' => 'bottom',
        'HAlign' => 'center',
        'Bold' => 1,
        'FgColor' => CUSTOM_LIGHT_BLUE,
        'TextWrap' => true));

$formatData =
        &$workbook->addFormat(
    array(
        'Size' => 8,
        'HAlign' => 'left',
        'VAlign' => 'vcenter'));

$formatPNames =
        &$workbook->addFormat(
    array(
        'Size' => 9,
        'Bold' => 1,
        'HAlign' => 'left',
        'VAlign' => 'vcenter'));
$formatTotals =
        &$workbook->addFormat(
    array(
        'Size' => 10,
        'Bold' => 1,
        'HAlign' => 'left',
        'VAlign' => 'vcenter'));

$worksheet->setRow(0, 11, $formatHeader);
$worksheet->setRow(1, 46, $formatHeader);
$worksheet->setRow(2, 11, $formatHeader);

$worksheet->setColumn(0, 0, 12); // User Id, shrink it to 7
$worksheet->setColumn(1, 1, 15); // Name, set the width to 12
$worksheet->setColumn(4, 4, 30); // Email, set the width to 15
$worksheet->setColumn(6, 6, 30); // Email, set the width to 15

$inputUser=$_REQUEST['inputUser'];
$worksheet->write(1, 1, 'Stock Items', $formatHeader);

$indexRow = 4;
$indexCol = 0; // Start @ column 0




//Order No Prescription No Order Date OP No Patient Name Stock Code Description Unit Cost
//Qty Prescribed Qty Issued Pending qty Total Amount 	Issuing Store 	Issued By
// Create the header for the data starting @ row 4
    $worksheet->write($indexRow, $indexCol++, 'PartNo', $formatReportHeader);
    $worksheet->write($indexRow, $indexCol++, 'Description', $formatReportHeader);
    $worksheet->write($indexRow, $indexCol++, 'Category', $formatReportHeader);
    $worksheet->write($indexRow, $indexCol++, 'Sub category', $formatReportHeader);
    $worksheet->write($indexRow, $indexCol++, 'Selling Price', $formatReportHeader);

    $indexRow++;   // Advance to the next row
    $indexCol = 0; // Start @ column 0


$searchParam = $_REQUEST[searchParam];
$category=$_REQUEST[category];


    $sql = "SELECT partcode,REPLACE(item_description,'\"','') AS item_description,REPLACE(item_full_description,'\"','') AS item_full_description,`unit_measure`,unit_price,unit_price_1,selling_price,
            purchasing_class, c.`item_Cat` AS category,item_status,`reorderlevel`,`minimum`,maximum,gl_sales_acct,gl_inventory_acct,gl_costofsales_acct
            FROM care_tz_drugsandservices d LEFT JOIN  care_tz_itemscat c
            ON d.`category`=c.`catID` WHERE partcode <> ''";


    if (isset($searchParam) && $searchParam<>'') {
        $sql.=" and partcode like '%$searchParam%' or item_description like '%$searchParam%'";
    }

    if (isset($category) && $category<>'' && $category<>'null') {
        $sql.=" and category='$category'";
    }

 //echo $sql;

    $request3=$db->Execute($sql);

    while ($row = $request3->FetchRow()) {
        $worksheet->write($indexRow, $indexCol++, $row['partcode'], $formatData);
        $worksheet->write($indexRow, $indexCol++, $row['item_description'], $formatData);
        $worksheet->write($indexRow, $indexCol++, $row['purchasing_class'], $formatData);
        $worksheet->write($indexRow, $indexCol++, $row['category'], $formatData);
        $worksheet->write($indexRow, $indexCol++, $row['unit_price'], $formatData);

        // Advance to the next row
        $indexCol = 0;
        $indexRow++;
    }


// Sends HTTP headers for the Excel file.
$workbook->send('StockItems' . date('Hms') . '.xls');

// Calls finalization methods.
// This method should always be the last one to be called on every workbook
$workbook->close();
?>
