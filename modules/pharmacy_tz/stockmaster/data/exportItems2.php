<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Africa/Nairobi');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../../../../../ExcelClasses/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Items List Report" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("George Maina")
    ->setLastModifiedBy("George Maina")
    ->setTitle("Items List Report")
    ->setSubject("Items List Report")
    ->setDescription("Items List ")
    ->setKeywords("Items List Report")
    ->setCategory("Items List Report");

$objPHPExcel->getActiveSheet(0)->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Items List Report');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'PARTCODE');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'ITEM DESCRIPTION' );
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'PURCHASING CLASS' );
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'CATEGORY' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'UNIT PRICE' );


$searchParam = $_REQUEST[searchParam];
$category=$_REQUEST[category];


$sql = "SELECT partcode,REPLACE(item_description,'\"','') AS item_description,REPLACE(item_full_description,'\"','') AS item_full_description,`unit_measure`,unit_price,unit_price_1,selling_price,
            purchasing_class, c.`item_Cat` AS category,item_status,`reorderlevel`,`minimum`,maximum,gl_sales_acct,gl_inventory_acct,gl_costofsales_acct
            FROM care_tz_drugsandservices d LEFT JOIN  care_tz_itemscat c
            ON d.`category`=c.`catID` WHERE partcode <> ''";


if (isset($searchParam) && $searchParam<>'') {
    $sql.=" and partcode like '%$searchParam%' or item_description like '%$searchParam%'";
}

if (isset($category) && $category<>'' && $category<>'null') {
    $sql.=" and category='$category'";
}

$result=$db->Execute($sql);
$i=3;
while($row=$result->FetchRow()){

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i",$row['partcode']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$i",$row['item_description']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C$i",$row['purchasing_class']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D$i",$row['category']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("E$i",$row['unit_price']);

    $i=$i+1;
}

$objPHPExcel->getActiveSheet()->setTitle('Items List Report');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save(str_replace('php','xlsx',$root_path."/docs/ItemsList.xlsx"));

echo "Created file : ".str_replace('php','xlsx',$root_path."docs/ItemsList.xlsx" ),EOL;

$objReader=PHPExcel_IOFactory::load($root_path."docs/ItemsList.xlsx");

?>
<script>
    window.open('../../../../docs/ItemsList.xlsx', "Items List Report",
        "menubar=no,toolbar=no,width=600,height=800,location=yes,resizable=yes,scrollbars=yes,status=yes");
</script>
