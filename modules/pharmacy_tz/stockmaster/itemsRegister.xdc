{
    "xdsVersion": "2.2.2",
    "frameworkVersion": "ext42",
    "internals": {
        "type": "Ext.window.Window",
        "reference": {
            "name": "items",
            "type": "array"
        },
        "codeClass": null,
        "userConfig": {
            "designer|snapToGrid": 10,
            "frame": true,
            "height": 505,
            "width": 855,
            "designer|userClassName": "ItemsRegister",
            "designer|userAlias": "itemsregister",
            "layout": "absolute",
            "title": "Items Register"
        },
        "cn": [
            {
                "type": "Ext.form.FieldSet",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "height": 200,
                    "width": 390,
                    "layout": "vbox",
                    "title": "Item Details"
                },
                "cn": [
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|displayName": "itemCode",
                            "fieldLabel": "Item Code",
                            "name": "itemCode"
                        }
                    },
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "width": 351,
                            "designer|displayName": "description",
                            "fieldLabel": "Description",
                            "name": "description"
                        }
                    },
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "width": 350,
                            "designer|displayName": "fullDescription",
                            "fieldLabel": "Full Desription",
                            "name": "fullDescription"
                        }
                    },
                    {
                        "type": "Ext.form.field.ComboBox",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "width": 202,
                            "designer|displayName": "Unit of Measure",
                            "fieldLabel": "Unit of Measure",
                            "name": "unitMeasure"
                        }
                    },
                    {
                        "type": "Ext.form.field.ComboBox",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "width": 295,
                            "designer|displayName": "category",
                            "fieldLabel": "Category",
                            "name": "category"
                        }
                    },
                    {
                        "type": "Ext.form.field.ComboBox",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "width": 296,
                            "designer|displayName": "subCategory",
                            "fieldLabel": "Sub Category",
                            "name": "subCategory"
                        }
                    }
                ]
            },
            {
                "type": "Ext.form.FieldSet",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "layout|x": 410,
                    "layout|y": 0,
                    "height": 200,
                    "width": 430,
                    "layout": "vbox",
                    "title": "Item Settings"
                },
                "cn": [
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|displayName": "buyingPrice",
                            "fieldLabel": "Buying Price",
                            "name": "buyingPrice"
                        }
                    },
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|displayName": "sellingPrice",
                            "fieldLabel": "Selling Price",
                            "name": "sellingPrice"
                        }
                    },
                    {
                        "type": "Ext.form.field.Checkbox",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|displayName": "itemStatus",
                            "fieldLabel": "Active",
                            "name": "itemStatus",
                            "boxLabel": ""
                        }
                    },
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "width": 239,
                            "designer|displayName": "reorderLevel",
                            "fieldLabel": "Reorder Level",
                            "name": "reorderLevel"
                        }
                    },
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|displayName": "minOrder",
                            "fieldLabel": "Minimum Order",
                            "name": "minOrder"
                        }
                    },
                    {
                        "type": "Ext.form.field.Text",
                        "reference": {
                            "name": "items",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "designer|displayName": "maxOrder",
                            "fieldLabel": "Maximum Order",
                            "name": "maxOrder"
                        }
                    }
                ]
            },
            {
                "type": "Ext.grid.Panel",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "layout|x": 0,
                    "layout|y": 220,
                    "height": 240,
                    "width": 490,
                    "title": "Stores items is located",
                    "store": "ItemLocations"
                },
                "cn": [
                    {
                        "type": "Ext.toolbar.Toolbar",
                        "reference": {
                            "name": "dockedItems",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "dock": "top"
                        },
                        "cn": [
                            {
                                "type": "Ext.button.Button",
                                "reference": {
                                    "name": "items",
                                    "type": "array"
                                },
                                "codeClass": null,
                                "userConfig": {
                                    "layout|flex": null,
                                    "text": "Add to Store"
                                }
                            },
                            {
                                "type": "Ext.button.Button",
                                "reference": {
                                    "name": "items",
                                    "type": "array"
                                },
                                "codeClass": null,
                                "userConfig": {
                                    "layout|flex": null,
                                    "text": "Remove from Store"
                                }
                            }
                        ]
                    },
                    {
                        "type": "Ext.grid.View",
                        "reference": {
                            "name": "viewConfig",
                            "type": "object"
                        },
                        "codeClass": null
                    },
                    {
                        "type": "Ext.grid.column.Column",
                        "reference": {
                            "name": "columns",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "dataIndex": "itemcode",
                            "text": "Itemcode"
                        }
                    },
                    {
                        "type": "Ext.grid.column.Column",
                        "reference": {
                            "name": "columns",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "dataIndex": "loccode",
                            "text": "Loccode"
                        }
                    },
                    {
                        "type": "Ext.grid.column.Column",
                        "reference": {
                            "name": "columns",
                            "type": "array"
                        },
                        "codeClass": null,
                        "userConfig": {
                            "dataIndex": "quantity",
                            "text": "Quantity"
                        }
                    }
                ]
            },
            {
                "type": "Ext.button.Button",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "layout|x": 530,
                    "layout|y": 230,
                    "height": 50,
                    "width": 110,
                    "text": "Save"
                }
            },
            {
                "type": "Ext.button.Button",
                "reference": {
                    "name": "items",
                    "type": "array"
                },
                "codeClass": null,
                "userConfig": {
                    "layout|x": 660,
                    "layout|y": 230,
                    "height": 50,
                    "width": 120,
                    "text": "Close"
                }
            }
        ]
    },
    "linkedNodes": {},
    "boundStores": {
        "bc8a2186-9d1f-4793-a850-a1a964e9edcb": {
            "type": "Ext.data.Store",
            "reference": {
                "name": "items",
                "type": "array"
            },
            "codeClass": null,
            "userConfig": {
                "model": "ItemLocations",
                "storeId": "ItemLocations",
                "designer|userClassName": "ItemLocations",
                "designer|userAlias": "itemlocations"
            },
            "cn": [
                {
                    "type": "Ext.data.proxy.Ajax",
                    "reference": {
                        "name": "proxy",
                        "type": "object"
                    },
                    "codeClass": null,
                    "userConfig": {
                        "url": "data/getDataFunctions.php?task=getItemLocations"
                    },
                    "cn": [
                        {
                            "type": "Ext.data.reader.Json",
                            "reference": {
                                "name": "reader",
                                "type": "object"
                            },
                            "codeClass": null,
                            "userConfig": {
                                "root": "itemLocations"
                            }
                        }
                    ]
                }
            ]
        }
    },
    "boundModels": {
        "40f03db8-60e4-48a8-9c17-aeffad5d44dd": {
            "type": "Ext.data.Model",
            "reference": {
                "name": "items",
                "type": "array"
            },
            "codeClass": null,
            "userConfig": {
                "designer|userClassName": "ItemLocations",
                "designer|userAlias": "itemlocations"
            },
            "cn": [
                {
                    "type": "Ext.data.Field",
                    "reference": {
                        "name": "fields",
                        "type": "array"
                    },
                    "codeClass": null,
                    "userConfig": {
                        "mapping": "itemcode",
                        "name": "itemcode"
                    }
                },
                {
                    "type": "Ext.data.Field",
                    "reference": {
                        "name": "fields",
                        "type": "array"
                    },
                    "codeClass": null,
                    "userConfig": {
                        "mapping": "loccode",
                        "name": "loccode"
                    }
                },
                {
                    "type": "Ext.data.Field",
                    "reference": {
                        "name": "fields",
                        "type": "array"
                    },
                    "codeClass": null,
                    "userConfig": {
                        "mapping": "quantity",
                        "name": "quantity"
                    }
                }
            ]
        }
    }
}