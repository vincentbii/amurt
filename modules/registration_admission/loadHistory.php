<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require('./roots.php');
require($root_path.'include/inc_environment_global.php');
?>
<?php

if(isset($_POST['pid'])){
	$pid = $_POST['pid'];

	$sql = "SELECT e.encounter_nr, p.prescribe_date, p.article, p.dosage, p.days, p.times_per_day, p.prescriber FROM care_encounter as e INNER JOIN care_encounter_prescription as p ON e.encounter_nr = p.encounter_nr WHERE e.pid = '".$pid."' AND drug_class NOT IN ('CONSULTATION') AND p.status = 'serviced'";
	$result = $db->Execute($sql);
	if($result->RecordCount() > 0){
		?>

<table id="customers" width="60%" cellpadding="3" cellspacing="1">
	<thead>
		<tr>
			<th>Date</th>
			<th>Encounter Nr.</th>
			<th>Description</th>
			<th>Dosage</th>
			<th>Days</th>
			<th>Times Per Day</th>
			<th>Prescribed By</th>
		</tr>
	</thead>
	<tbody>
		<?php
		while ($row = $result->FetchRow()) {
			?>


		<tr>
			<td><?php echo $row['prescribe_date']; ?></td>
			<td><?php echo $row['encounter_nr']; ?></td>
			<td><?php echo $row['article']; ?></td>
			<td><?php echo $row['dosage']; ?></td>
			<td><?php echo $row['days']; ?></td>
			<td><?php echo $row['times_per_day']; ?></td>
			<td><?php echo $row['prescriber']; ?></td>
		</tr>
	
			<?php
		}
		?>
</tbody>
</table>
		<?php
	}else{
		?>
		<script type="text/javascript">
			alert("No Prescription History Found")
		</script><?php
	}
}

?>

<style type="text/css">
	#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
</style>