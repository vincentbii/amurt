Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

Ext.onReady(function() {
    Ext.QuickTips.init();

    // sample static data for the store

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */

    Ext.define(
            'disease', {
        extend: 'Ext.data.Model',
        fields: [{
                name: 'icdcode',
                type: 'string'
            },
            {
                name: 'desc',
                type: 'string'
            },
            {
                name: 'A1',
                mapping: 'A1',
                type: 'int'
            },
            {
                name: 'D1',
                mapping: 'A1',
                type: 'int'
            },
            {
                name: 'A2',
                type: 'int'
            },
            {
                name: 'D2',
                type: 'int'
            },
            {
                name: 'A3',
                type: 'string'
            },
            {
                name: 'D3',
                type: 'string'
            },
            {
                name: 'A4',
                type: 'string'
            },
            {
                name: 'D4',
                type: 'string'
            },
            {
                name: 'A5',
                type: 'string'
            },
            {
                name: 'D5',
                type: 'string'
            },
            {
                name: 'A6',
                type: 'string'
            },
            {
                name: 'D6',
                type: 'string'
            },
            {
                name: 'A7',
                type: 'string'
            },
            {
                name: 'D7',
                type: 'string'
            },
            {
                name: 'A8',
                type: 'string'
            },
            {
                name: 'D8',
                type: 'string'
            },
            {
                name: 'A9',
                type: 'string'
            },
            {
                name: 'D9',
                type: 'string'
            },
            {
                name: 'Alive',
                type: 'string'
            },
            {
                name: 'Dead',
                type: 'string'
            },
            {
                name: 'Totals',
                type: 'string'
            }]
    });

    var mStore = new Ext.data.Store({
        model: 'disease',
        proxy: {
            type: 'ajax',
            url: 'getReports.php?task=ipmorbidity',
            reader: {
                type: 'json',
                root: 'ipmorbidity',
                param: {
                    task: 'ipmobidity'
                }
            }
        },
        autoLoad: true
    });
    //    mStore.load()

    function change(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */
    function pctChange(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '%</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }

    var datefield = new Ext.form.DateField({
//                renderTo: 'datefield',
        labelWidth: 100, // label settings here cascade unless overridden
        frame: false,
        width: 180,
        name: 'strDate1',
        id: 'strDate1'
    });

    var datefield2 = new Ext.form.DateField({
//                renderTo: 'datefield2',
        frame: false,
        width: 180,
        name: 'strDate2',
        id: 'strDate2'
    });



    // create the Grid
    var grid = Ext.create('Ext.grid.Panel', {
        store: mStore,
        columnLines: true,
        dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                items: [{
                        tooltip: 'Toggle the visibility of the summary row',
                        text: 'Toggle Summary',
                        handler: function() {
                            var view = grid.getView();
                            showSummary = !showSummary;
                            view.getFeature('group').toggleSummaryRow(showSummary);
                            view.refresh();
                        }
                    }, datefield, datefield2, {
                        tooltip: 'Preview',
                        text: 'Preview',
                        handler: function() {
                            //sends details to family information form
                            var strDate1 = datefield.getValue();//document.getElementById(strDate1).value;
                            var strDate2 = datefield2.getValue();//document.getElementById('strDate2').value;
                            mStore.load({
                                params: {
                                    date1: strDate1,
                                    date2: strDate2
                                }
                            });
                        }
                    }, {tooltip: 'Print',
                        text: 'Print'}
                    ,
                    {
                        tooltip: 'Export to Excell',
                        text: 'Export to Excel'
                    }
                ]
            }],
        columns: [{
                text: 'icdcode',
                //            flex     : 1,
                width: 65,
                sortable: false,
                dataIndex: 'icdcode'
            }, {
                text: 'Description',
                //flex: 1,
                width    : 300,
                sortable: false,
                dataIndex: 'desc'
            }, {
                text: '<1 Years',
                columns: [{
                        text: 'A',
                        width: 30,
                        //                sortable : true,
                        //                renderer : 'usMoney',
                        dataIndex: 'A1'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D1'
                    }]
            }, {
                text: '1-4 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A2'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D2'
                    }]
            }, {
                text: '5-14 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A3'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D3'
                    }]
            }, {
                text: '15-24 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A4'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D4'
                    }]
            }, {
                text: '25-34 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A5'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D5'
                    }]
            }, {
                text: '35-44 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A6'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D6'
                    }]
            }, {
                text: '45-54 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A7'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D7'
                    }]
            }, {
                text: '55-64 Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A8'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D8'
                    }]
            }, {
                text: '65+ Yrs',
                columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'A9'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'D9'
                    }]
            }, {
                text: 'TOTALS',
                columns: [{
                    text: 'ALIVE',
                    width: 50,
                    dataIndex: 'Alive'
                }, {
                    text: 'DEAD',
                    width: 50,
                    //                sortable : true,
                    //                renderer : change,
                    dataIndex: 'Dead'
                }]
        }, {
            text: 'TOTALS',
            //flex: 1,
            width    : 50,
            sortable: false,
            dataIndex: 'Totals'
        }],
        height: 600,
       // width: 1000,
        title: 'Inpatient Morbidity and Mortality Summary Sheet',
        renderTo: 'diseases',
        viewConfig: {
            stripeRows: true
        }
    });
});
