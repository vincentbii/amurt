Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*'
]);

Ext.onReady(function() {
    Ext.QuickTips.init();

    // sample static data for the store

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */

    Ext.define(
            'disease', {
        extend: 'Ext.data.Model',
        fields: [{
                name: 'icdcode',
                type: 'string'
            },
            {
                name: 'desc',
                type: 'string'
            },
            {
                name: 'FA1',
                mapping: 'FA1',
                type: 'int'
            },
            {
                name: 'FD1',
                mapping: 'FA1',
                type: 'int'
            },
            {
                name: 'FA2',
                type: 'int'
            },
            {
                name: 'FD2',
                type: 'int'
            },
            {
                name: 'FA3',
                type: 'string'
            },
            {
                name: 'FD3',
                type: 'string'
            },
            {
                name: 'FA4',
                type: 'string'
            },
            {
                name: 'FD4',
                type: 'string'
            },
            {
                name: 'FA5',
                type: 'string'
            },
            {
                name: 'FD5',
                type: 'string'
            },
            {
                name: 'MA6',
                type: 'string'
            },
            {
                name: 'MD6',
                type: 'string'
            },
            {
                name: 'MA7',
                type: 'string'
            },
            {
                name: 'MD7',
                type: 'string'
            },
            {
                name: 'MA8',
                type: 'string'
            },
            {
                name: 'MD8',
                type: 'string'
            },
            {
                name: 'MA9',
                type: 'string'
            },
            {
                name: 'MD9',
                type: 'string'
            },
            {
                name: 'MA10',
                type: 'string'
            },
            {
                name: 'MD10',
                type: 'string'
            },
            {
                name: 'Alive',
                type: 'string'
            },
            {
                name: 'Dead',
                type: 'string'
            },
            {
                name: 'Totals',
                type: 'string'
            }]
    });

    var mStore = new Ext.data.Store({
        model: 'disease',
        proxy: {
            type: 'ajax',
            url: 'getReports.php?task=ipmorbidity',
            reader: {
                type: 'json',
                root: 'ipmorbidity',
                param: {
                    task: 'ipmobidity'
                }
            }
        },
        autoLoad: true
    });
    //    mStore.load()

    function change(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */
    function pctChange(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '%</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }

    var datefield = new Ext.form.DateField({
//                renderTo: 'datefield',
        labelWidth: 100, // label settings here cascade unless overridden
        frame: false,
        width: 180,
        name: 'strDate1',
        id: 'strDate1'
    });

    var datefield2 = new Ext.form.DateField({
//                renderTo: 'datefield2',
        frame: false,
        width: 180,
        name: 'strDate2',
        id: 'strDate2'
    });



    // create the Grid
    var grid = Ext.create('Ext.grid.Panel', {
        store: mStore,
        columnLines: true,
        dockedItems: [{
                dock: 'top',
                xtype: 'toolbar',
                items: [{
                        tooltip: 'Toggle the visibility of the summary row',
                        text: 'Toggle Summary',
                        handler: function() {
                            var view = grid.getView();
                            showSummary = !showSummary;
                            view.getFeature('group').toggleSummaryRow(showSummary);
                            view.refresh();
                        }
                    }, datefield, datefield2, {
                        tooltip: 'Preview',
                        text: 'Preview',
                        handler: function() {
                            //sends details to family information form
                            var strDate1 = datefield.getValue();//document.getElementById(strDate1).value;
                            var strDate2 = datefield2.getValue();//document.getElementById('strDate2').value;
                            mStore.load({
                                params: {
                                    date1: strDate1,
                                    date2: strDate2
                                }
                            });
                        }
                    }, {tooltip: 'Print',
                        text: 'Print'}
                    ,
                    {
                        tooltip: 'Export to Excell',
                        text: 'Export to Excel'
                    }
                ]
            }],
        columns: [{
                text: 'icdcode',
                //            flex     : 1,
                width: 65,
                sortable: false,
                dataIndex: 'icdcode'
            }, {
                text: 'Description',
                //flex: 1,
                width    : 300,
                sortable: false,
                dataIndex: 'desc'
            },
            {
                text:'Female', columns:
                    [{ text: '<1 Yrs',
                            columns: [{
                                text: 'A',
                                width: 30,
                                dataIndex: 'FA1'
                            }, {
                                text: 'D',
                                width: 30,
                                //                sortable : true,
                                //                renderer : change,
                                dataIndex: 'FD1'
                            }]
                     },{
                            text: '1-4 Yrs',
                            columns: [{
                                text: 'A',
                                width: 30,
                                dataIndex: 'FA2'
                            }, {
                                text: 'D',
                                width: 30,
                                //                sortable : true,
                                //                renderer : change,
                                dataIndex: 'FD2'
                            }]
                     },{
                            text: '5-14 Yrs',
                            columns: [{
                                text: 'A',
                                width: 30,
                                dataIndex: 'FA3'
                            }, {
                                text: 'D',
                                width: 30,
                                //                sortable : true,
                                //                renderer : change,
                                dataIndex: 'FD3'
                            }]
                     },{
                            text: '15-44 Yrs',
                            columns: [{
                                text: 'A',
                                width: 30,
                                dataIndex: 'FA4'
                            }, {
                                text: 'D',
                                width: 30,
                                //                sortable : true,
                                //                renderer : change,
                                dataIndex: 'FD4'
                            }]
                    },{
                            text: '45+ Yrs',
                            columns: [{
                                text: 'A',
                                width: 30,
                                dataIndex: 'FA5'
                            }, {
                                text: 'D',
                                width: 30,
                                //                sortable : true,
                                //                renderer : change,
                                dataIndex: 'FD5'
                            }]
                    }]
                }, {
                text:'Male', columns:
                    [{ text: '<1 Yrs',
                    columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'MA6'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'MD6'
                    }]
                },{
                    text: '1-4 Yrs',
                    columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'MA7'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'MD7'
                    }]
                },{
                    text: '5-14 Yrs',
                    columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'MA8'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'MD8'
                    }]
                },{
                    text: '15-44 Yrs',
                    columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'MA9'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'MD9'
                    }]
                },{
                    text: '45+ Yrs',
                    columns: [{
                        text: 'A',
                        width: 30,
                        dataIndex: 'MA10'
                    }, {
                        text: 'D',
                        width: 30,
                        //                sortable : true,
                        //                renderer : change,
                        dataIndex: 'MD10'
                    }]
                }]
            }, {
            text: 'TOTALS',
            //flex: 1,
            width    : 50,
            sortable: false,
            dataIndex: 'Totals'
        }],
        height: 600,
       // width: 1000,
        title: 'Inpatient Morbidity and Mortality Summary Sheet',
        renderTo: 'diseases',
        viewConfig: {
            stripeRows: true
        }
    });
});
