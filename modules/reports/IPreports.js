Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.form.field.Number',
    'Ext.form.field.Date',
    'Ext.tip.QuickTipManager'
    ]);

Ext.onReady(function() {
    Ext.QuickTips.init();


    Ext.define(
        'ipReports',{
            extend:'Ext.data.Model',
            fields:[{
                name:'parent',
                type:'string'
            },{
                name:'opCode',
                type:'string'
            },
            {
                name:'Description',
                type:'string'
            },
            {
                name:'GenAdults',
                type:'float'
            },
            {
                name:'GenPaed',
                type:'float'
            },
            {
                name:'Martmoms',
                type:'float'
            },
            {
                name:'Amenity',
                type:'float'
            },
            {
                name:'Total',
                type:'float'
            }]
        });

    var vStore=new Ext.data.Store({
        model:'ipReports',
        groupField: 'parent',
        proxy:{
            type:'ajax',
            url: 'getReports.php?task=ipReports',
            reader:{
                type:'json',
                root:'ipReports',
                param:{
                    task:'ipReports'
                }
            }
        },
        autoLoad:true
    });
    //    mStore.load()

    function change(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */
    function pctChange(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '%</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }

    var showSummary=true;
    // create the Grid
    var grid = Ext.create('Ext.grid.Panel', {
        store: vStore,
        columnLines: true,
         dockedItems: [{
            dock: 'top',
            xtype: 'toolbar',
            items: [{
                tooltip: 'Toggle the visibility of the summary row',
                text: 'Toggle Summary',
                handler: function(){
                    var view = grid.getView();
                    showSummary = !showSummary;
                    view.getFeature('group').toggleSummaryRow(showSummary);
                    view.refresh();
                }
            },{
                xtype: 'combobox',
                itemId: 'reportMonth',
                width: 321,
                fieldLabel: 'Select Report Month',
                labelWidth: 120,
                displayField: 'month',
                store: [
                    [
                        'January',
                        'January'
                    ],
                    [
                        'February',
                        'February'
                    ],
                    [
                        'March',
                        'March'
                    ],
                    [
                        'April',
                        'April'
                    ],
                    [
                        'May',
                        'May'
                    ],
                    [
                        'June',
                        'June'
                    ],
                    [
                        'July',
                        'July'
                    ],
                    [
                        'August',
                        'August'
                    ],
                    [
                        'September',
                        'September'
                    ],
                    [
                        'October',
                        'October'
                    ],
                    [
                        'November',
                        'November'
                    ],
                    [
                        'December',
                        'December'
                    ]
                ],
                valueField: 'ID'
            }]
        }],
        features:[{
                id:'group',
                ftype:'groupingsummary',
                groupHeaderTpl:'{name}',
                hideGroupedHeader:false
        }],
        columns: [{
            text     : 'parent',
            width    : 65,
            sortable : false,
            dataIndex: 'parent',
            hidden:true
        },{
            text     : 'opCode',
            width    : 65,
            sortable : false,
            dataIndex: 'opCode'
        },{
            text     : 'Description',
            flex     : 1,
            sortable : false,
            tdCls: 'opdesc',
            dataIndex: 'Description',
            renderer: function(v, params) {
                params.tdCls = "opdesc";
                return v;
            },
            summaryRenderer: function(v, params, data) {
                return ((v === 0 || v > 1) ? '(' + v + ' Results)' : '(1 Results)');
            }
        },{
            text     : 'GenAdults',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            dataIndex: 'GenAdults',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        },{
            text     : 'GenPaed',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            dataIndex: 'GenPaed',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        },{
            text     : 'Martmoms',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            dataIndex: 'Martmoms',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        },{
            text     : 'Amenity',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            dataIndex: 'Amenity',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        },{
            text     : 'TOTAL',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            summaryType: 'sum',
            dataIndex: 'Total',
            field: {
                xtype: 'numberfield'
            }
        }],
        height: 600,
        width: 1000,
        title: 'Inpatient Services',
        renderTo: 'ipReports',
        viewConfig: {
            stripeRows: true
        }
    });
});
