Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.form.field.Number',
    'Ext.form.field.Date',
    'Ext.tip.QuickTipManager'
    ]);

Ext.onReady(function() {
    Ext.QuickTips.init();


    Ext.define(
        'opipRevenue',{
            extend:'Ext.data.Model',
            fields:[{
                name:'partcode',
                type:'string'
            },{
                name:'purchasing_class',
                type:'string'
            },
            {
                name:'item_Cat',
                type:'string'
            },
            {
                name:'total',
                type:'float'
            }]
        });

    var vStore=new Ext.data.Store({
        model:'opipRevenue',
        groupField: 'item_Cat',
        proxy:{
            type:'ajax',
            url: 'OPIPgetRevenue.php?task=opipRevenue',
            reader:{
                type:'json',
                root:'opipRevenue',
                param:{
                    task:'opipRevenue'
                }
            }
        },
        autoLoad:true
    });
    //    mStore.load()

    function change(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */
    function pctChange(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '%</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }

    var showSummary=true;
    // create the Grid
    var grid = Ext.create('Ext.grid.Panel', {
        store: vStore,
        columnLines: true,
         dockedItems: [{
            dock: 'top',
            xtype: 'toolbar',
            items: [{
                tooltip: 'Toggle the visibility of the summary row',
                text: 'Toggle Summary',
                handler: function(){
                    var view = grid.getView();
                    showSummary = !showSummary;
                    view.getFeature('group').toggleSummaryRow(showSummary);
                    view.refresh();
                }
            }]
        }],
    //a.partcode,b.purchasing_class,c.item_Cat,a.description,SUM(a.total) AS total
//        features:[{
//                id:'group',
//                ftype:'groupingsummary',
//                groupHeaderTpl:'{name}',
//                hideGroupedHeader:false
//        }],
        columns: [{
            text     : 'CODE',
            width    : 65,
            sortable : false,
            dataIndex: 'partcode',
            hidden:true
        },{
            text     : 'CATEGORY',
            width    : 65,
            sortable : false,
            dataIndex: 'purchasing_class'
        },{
            text     : 'ITEM',
            flex     : 1,
            sortable : false,
            dataIndex: 'item_Cat'
        },{
            text     : 'TOTAL',
            flex     : 1,
            sortable : false,
            dataIndex: 'total',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        }],
        height: 600,
        width: 1000,
        title: 'Revenue Collection',
        renderTo: 'opipVisits',
        viewConfig: {
            stripeRows: true
        }
    });
});
