Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.util.*',
    'Ext.state.*',
    'Ext.form.field.Number',
    'Ext.form.field.Date',
    'Ext.tip.QuickTipManager'
    ]);

Ext.onReady(function() {
    Ext.QuickTips.init();

    Ext.define(
        'opVisits',{
            extend:'Ext.data.Model',
            fields:[{
                name:'parent',
                type:'string'
            },{
                name:'opCode',
                type:'string'
            },
            {
                name:'Description',
                type:'string'
            },
            {
                name:'New',
                type:'float'
            },
            {
                name:'Ret',
                type:'float'
            },
            {
                name:'Total',
                type:'float'
            }]
        });

    var vStore=new Ext.data.Store({
        model:'opVisits',
        groupField: 'parent',
        proxy:{
            type:'ajax',
            url: 'getReports.php?task=opVisits',
            reader:{
                type:'json',
                root:'opVisits',
                param:{
                    task:'opVisits'
                }
            }
        },
        autoLoad:true
    });
    //    mStore.load()

    function change(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '</span>';
        }
        return val;
    }
    
            var datefield = new Ext.form.DateField({
//                renderTo: 'datefield',
                labelWidth: 100, // label settings here cascade unless overridden
                frame: false,
                width: 180,
                name: 'strDate1',
                id:'strDate1'
            });
            
            var datefield2 = new Ext.form.DateField({
//                renderTo: 'datefield2',
                frame: false,
                width: 180,
                name: 'strDate2',
                id:'strDate2'
            });

    /**
     * Custom function used for column renderer
     * @param {Object} val
     */
    function pctChange(val) {
        if (val > 0) {
            return '<span style="color:green;">' + val + '%</span>';
        } else if (val < 0) {
            return '<span style="color:red;">' + val + '%</span>';
        }
        return val;
    }

    var showSummary=true;
    // create the Grid
    var grid = Ext.create('Ext.grid.Panel', {
        store: vStore,
        columnLines: true,
         dockedItems: [{
            dock: 'top',
            xtype: 'toolbar',
            items: [{
                tooltip: 'Toggle the visibility of the summary row',
                text: 'Toggle Summary',
                handler: function(){
                    var view = grid.getView();
                    showSummary = !showSummary;
                    view.getFeature('group').toggleSummaryRow(showSummary);
                    view.refresh();
                }
            },datefield,datefield2,{
                tooltip: 'Preview',
                text: 'Preview',
                handler: function(){
                    //sends details to family information form
                    var strDate1=datefield.getValue();//document.getElementById(strDate1).value;
                    var strDate2=datefield2.getValue();//document.getElementById('strDate2').value;
                    vStore.load({
                        params:{
                            date1:strDate1,
                            date2:strDate2
                        }
                    });
                }
            }]
        }],
        features:[{
                id:'group',
                ftype:'groupingsummary',
                groupHeaderTpl:'{name}',
                hideGroupedHeader:false
        }],
        columns: [{
            text     : 'parent',
            width    : 65,
            sortable : false,
            dataIndex: 'parent',
            hidden:true
        },{
            text     : 'opCode',
            width    : 65,
            sortable : false,
            dataIndex: 'opCode'
        },{
            text     : 'Description',
            flex     : 1,
            sortable : false,
            tdCls: 'opdesc',
            dataIndex: 'Description',
            renderer: function(v, params) {
                params.tdCls = "opdesc";
                return v;
            },
            summaryRenderer: function(v, params, data) {
                return ((v === 0 || v > 1) ? '(' + v + ' Results)' : '(1 Results)');
            }
        },{
            text     : 'New',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            dataIndex: 'New',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        },{
            text     : 'RE-ATT',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            dataIndex: 'Ret',
            summaryType: 'sum',
            field: {
                xtype: 'numberfield'
            }
        },{
            text     : 'TOTAL',
            flex     : 1,
            //            width    : 150,
            sortable : false,
            summaryType: 'sum',
            dataIndex: 'Total',
            field: {
                xtype: 'numberfield'
            }
        }],
        height: 600,
        width: 1000,
        title: 'Monthly Workload',
        //renderTo: 'itemList',
        viewConfig: {
            stripeRows: true
        }
    });
    
    grid.render('opVisits');
});
