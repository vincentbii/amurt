<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../../css/themes/default/default.css">
        <?php
        require ('../pharmacy_tz/myLinks.php');
        Ext4Includes();
        ?>

        <script type="text/javascript"  src="OPVisits.js" ></script>
        
        <style type="text/css" media="screen">
		.opdesc .x-grid-cell-inner {
			padding-left: 15px;
		}
	
		.x-grid-row-summary {
			
		}
		.x-grid-row-summary .x-grid-cell-inner {
			font-weight: bold;
			font-size: 11px;
		}

        .icon-grid {
            background: url(../../include/ext4/shared/icons/fam/grid.png) no-repeat 0 -1px;
        }
	</style>
        <script type="text/javascript" src="reportFunctions.js"></script>
    </head>
    <body>
        <table width=100%  border="1" cellpadding="0" cellspacing="0">
            <tr class="titlebar"><td bgcolor="#99ccff" colspan="2">
      <font color="#330066">Out Patient Visits</font></td></tr>
            <tr>
                <td valign="top" width="18%" ><?php require_once 'acLinks.php'; ?></td>
                <td valign="top"><table border="1">
                        <thead>
                            <tr>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
<!--                            <tr>
                                <td id="datefield">Start Date:</td>
                                <td id="datefield2">End Date:</td>
                            </tr>-->
                            <tr>
                                <td colspan="2"><div id="opVisits"></div></td>
                            </tr>
                        </tbody>
                    </table>
                   </td>
            </tr>
        </table>d



    </body>
</html>

