<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="../../css/themes/default/default.css">
        <link rel="stylesheet" type="text/css" href="sample.css" />
        <?php
        require ('../pharmacy_tz/myLinks.php');
        ExtIncludes();
        ?>

        <script type="text/javascript"  src="getWardinfo.js" ></script>
        
    </head>
    <body>
        <table width=100%  border="1" cellpadding="0" cellspacing="0">
            <tr class="titlebar"><td bgcolor="#99ccff" colspan="2">
      <font color="#330066">Ward Occupancy Reports</font></td></tr>
            <tr>
                <td valign="top" width="18%" ><?php require_once 'acLinks.php'; ?></td>
                <td valign="top"> <div id="occupancy"></div></td>
            </tr>
        </table>



    </body>
</html>

