<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../css/themes/default/default.css">
    <script src="../../../../Ext-4/ext-all.js"></script>
    <link rel="stylesheet" href="../../../../Ext-4/resources/ext-theme-classic/ext-theme-classic-all.css">
    <script type="text/javascript" src="app.js"></script>
    <style type="text/css" media="screen">
        .task .x-grid-cell-inner {
            padding-left: 15px;
        }
        .x-grid-row-summary .x-grid-cell-inner {
            font-weight: bold;
        }
        .icon-grid {
            background: url(../../icons/fam/grid.png) no-repeat 0 -1px;
        }
    </style>
</head>
<body>
<table width=100%  border="1" cellpadding="0" cellspacing="0">
    <tr class="titlebar"><td bgcolor="#99ccff" colspan="2">
            <font color="#330066">OutPatient Mobidity</font></td></tr>
    <tr>
        <td valign="top" width="18%" ><?php require_once 'acLinks.php'; ?></td>
        <td valign="top"><div id="OpMobidity"></div></td>
    </tr>
</table>



</body>
</html>

