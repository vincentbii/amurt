<?php

require ('roots.php');
require ($root_path . 'include/inc_environment_global.php');

require_once 'Zend/Pdf.php';

$pdf = new Zend_Pdf ();
//require ($root_path . 'include/care_api_classes/Library_Pdf_base.php');
//$pdfBase=new Library_Pdf_Base();

$pid=$_REQUEST[pid];
$encounter_nr=$_REQUEST[encounter_nr];

createFile($db, $pid, $encounter_nr,$pdf);

function getWrappedText($string, Zend_Pdf_Style $style, $max_width) {
    $wrappedText = '';
    $lines = explode("\n", $string);
    foreach ($lines as $line) {
        $words = explode(' ', $line);
        $word_count = count($words);
        $i = 0;
        $wrappedLine = '';
        while ($i < $word_count) {
            /* if adding a new word isn't wider than $max_width,
              we add the word */
            if (widthForStringUsingFontSize($wrappedLine . ' ' . $words[$i]
                    , $style->getFont()
                    , $style->getFontSize()) < $max_width) {
                if (!empty($wrappedLine)) {
                    $wrappedLine .= ' ';
                }
                $wrappedLine .= $words[$i];
            } else {
                $wrappedText .= $wrappedLine . "\n";
                $wrappedLine = $words[$i];
            }
            $i++;
        }
        $wrappedText .= $wrappedLine . "\n";
    }
    return $wrappedText;
}

/**
 * found here, not sure of the author :
 * http://devzone.zend.com/article/2525-Zend_Pdf-tutorial#comments-2535
 */
function widthForStringUsingFontSize($string, $font, $fontSize) {
    $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
    $characters = array();
    for ($i = 0; $i < strlen($drawingString); $i++) {
        $characters[] = (ord($drawingString[$i++]) << 8 ) | ord($drawingString[$i]);
    }
    $glyphs = $font->glyphNumbersForCharacters($characters);
    $widths = $font->widthsForGlyphs($glyphs);
    $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
    return $stringWidth;
}


function getNotes($type_nr,$enc){

    global $db,$rows,$result; // $HTTP_SESSION_VARS;

    $sql="SELECT n.nr,n.notes,n.short_notes, n.encounter_nr,n.date,n.time, n.personell_nr,n.personell_name
		FROM care_encounter AS e, 
					care_encounter_notes AS n 
		WHERE e.encounter_nr=".$enc." 
			AND e.encounter_nr=n.encounter_nr 
			AND n.type_nr=".$type_nr."
		ORDER BY n.date DESC";

    if($result=$db->Execute($sql)){
        if($rows=$result->RecordCount()){
            return $result;
        }else{
            return FALSE;
        }
    }else{
        echo $sql;
        return FALSE;
    }
}

function createFile($db, $pid,$encounter_nr, $pdf) {
    require ('roots.php');
    require ("../../include/care_api_classes/class_encounter.php");
    require ("../../include/care_api_classes/class_ward.php");
    require ("../../include/care_api_classes/class_person.php");

    $encObj=new Encounter();
    $wrd=new Ward();
    $perObj=new Person();

    $encounterDetails=$encObj->getEncounterDetails($encounter_nr);
    $admissionDetails=$encObj->getAdmissionDetails($encounter_nr);
    $personDetails=$perObj->getAllInfoArray($pid);

    $filter='3,99'; # notes type number which are not allowed to be displayed

    $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);

    $pageHeight = $page->getHeight();
    $width = $page->getWidth();
    $topPos = $pageHeight - 10;
    $leftPos = 36;

    $headlineStyle = new Zend_Pdf_Style ();
    $headlineStyle->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
    $headlineStyle->setFont($font, 12);
    $page->setStyle($headlineStyle);


    $imagePath="../../icons/logo.jpg";
    $image = Zend_Pdf_Image::imageWithPath($imagePath);
    $page->drawImage($image, $leftPos+20, $topPos-70, $leftPos+500, $topPos+10);

    $page->drawText('PATIENT ADMISSION FILE', $leftPos + 200, $topPos - 90);

    $headlineStyle2 = new Zend_Pdf_Style ();
    $headlineStyle->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
    $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
    $headlineStyle2->setFont($font, 10);
    $page->setStyle($headlineStyle2);

    $page->drawText('Outpatient No:', $leftPos + 20, $topPos - 120);
    $page->drawText($personDetails['selian_pid'], $leftPos + 100, $topPos - 120);
    $page->drawText('Patient No:', $leftPos + 20, $topPos - 135);
    $page->drawText($pid, $leftPos + 100, $topPos - 135);
    $page->drawText('Name:      ', $leftPos + 20, $topPos - 150);
    $page->drawText($personDetails['name_first']." ".$personDetails['name_2']." ".$personDetails['name_last'], $leftPos + 100, $topPos - 150);
    $page->drawText('Address:   ', $leftPos + 20, $topPos - 165);
    $page->drawText($personDetails['addr_zip'], $leftPos + 100, $topPos - 165);
    $page->drawText('Town:      ', $leftPos + 20, $topPos - 180);
    $page->drawText($personDetails['citizenship'], $leftPos + 100, $topPos - 180);
    $page->drawText('Phone:     ', $leftPos + 20, $topPos - 195);
    $page->drawText($personDetails['phone_1_nr'], $leftPos + 100, $topPos - 195);

    $page->drawText('Admission Date: ', $leftPos + 330, $topPos - 120);
    $page->drawText($encounterDetails['encounter_date']." ".$encounterDetails['encounter_time'], $leftPos + 420, $topPos - 120);
    $page->drawText('Discharge Date: ', $leftPos + 330, $topPos - 135);
    $page->drawText($encounterDetails['discharge_date'], $leftPos + 420, $topPos - 135);

     if($encounterDetails['Encounter_class_nr']==1){
         $row2 = $wrd->EncounterLocationsInfo2($encounter_nr);
         $bed_nr = $row2 [6];
         $room_nr = $row2 [5];
         $ward_nr = $row2 [0];
         $ward_name = $row2 [1];
         $admDate = $row2 [7];
         $Disc_date = $row2 [8];

         $page->drawText('Ward: ', $leftPos + 330, $topPos - 120);
         $page->drawText($ward_name." ".$ward_nr, $leftPos + 420, $topPos - 120);
         $page->drawText('Room No: ', $leftPos + 330, $topPos - 135);
         $page->drawText($room_nr, $leftPos + 420, $topPos - 135);
         $page->drawText('Bed No: ', $leftPos + 330, $topPos - 135);
         $page->drawText($bed_nr, $leftPos + 420, $topPos - 135);
     }

//    $sql = "SELECT id,accno,`name` FROM care_tz_company WHERE id=(SELECT insurance_id FROM care_person WHERE pid=$pid)";
//    if ($insu_result = $db->Execute($sql)) {
//        $insu_row = $insu_result->FetchRow();
//    }
//
//    $currPos= $topPos - 135;
//    if ($insu_row[0] <> '') {
//        $page->drawText('Account No: ', $leftPos + 20, $currPos - 10);
//        $page->drawText($insu_row[1], $leftPos + 92, $currPos - 10);
//        $page->drawText('Account Name: ', $leftPos + 20, $currPos - 20);
//        $page->drawText($insu_row[2], $leftPos + 92, $currPos - 20);
//    }

    $page->setStyle($headlineStyle);

    # Get the notes types first
    $types=$encObj->getAllTypesSort('name');

//    $notes=$encObj->getEncounterNotes($recnr);

    $currpoint=230;
    while(list($x,$v)=each($types)){

        extract($v);

        #Get the report title
        if(isset($LD_var)&&!empty($LD_var)){
            $title=$LD_var;
        }else{
            $title=$name;
        }

        $data=NULL;
        $data[]=array($title);

        $page->drawText($data, $leftPos + 20, $topPos - $currpoint);

        $page->setStyle($headlineStyle2);

        if($notes=getNotes($nr,$encounter_nr)){
                # get the report

                while($report=$notes->FetchRow()){
                    $data=NULL;
                    # create the tag infos inside a table
                    $data[]=array(" Date: ".$report['date']."   Time: ".$report['time']."   By: ".$report['personell_name']);

                    $lines = explode("\n", getWrappedText(ucfirst(strtolower($report['notes'])), $headlineStyle, 500));

                    $y = $topPos - $currpoint;
                    foreach ($lines as $line) {
                        $page->drawText($line, $leftPos + 20, $y);
                        $y-=10;
                    }


                }
            }else{
                //$y=$pdf->ezText("\n",14);
            }
        }




    $topPos = $topPos - 10;
    array_push($pdf->pages, $page);
    header('Content-type: application/pdf');
    echo $pdf->render();
}

?>
