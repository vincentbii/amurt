/*
 * File: app/view/OpVisits.js
 *
 * This file was generated by Sencha Architect version 4.1.1.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('ReportsMain.view.OpVisits', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.opvisits',

    requires: [
        'Ext.grid.View',
        'Ext.grid.column.Number',
        'Ext.grid.feature.GroupingSummary',
        'Ext.XTemplate',
        'Ext.toolbar.Toolbar',
        'Ext.form.field.Date',
        'Ext.button.Button'
    ],

    height: 650,
    closable: true,
    title: 'Monthly Workload',
    columnLines: true,
    store: 'OpVisitsStore',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    summaryType: 'count',
                    hidden: true,
                    dataIndex: 'parent',
                    groupable: true,
                    text: 'Parent'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'opCode',
                    text: 'Op Code'
                },
                {
                    xtype: 'gridcolumn',
                    summaryRenderer: function(val, params, data) {
                        return ((val === 0 || val > 1) ? '(' + val + ' Results)' : '(1 Results)');
                    },
                    width: 270,
                    dataIndex: 'Description',
                    tdCls: 'opdesc',
                    text: 'Description'
                },
                {
                    xtype: 'numbercolumn',
                    summaryType: 'sum',
                    dataIndex: 'New',
                    text: 'New',
                    format: '0'
                },
                {
                    xtype: 'numbercolumn',
                    summaryType: 'sum',
                    dataIndex: 'Ret',
                    text: 'Return',
                    format: '0'
                },
                {
                    xtype: 'numbercolumn',
                    summaryType: 'sum',
                    dataIndex: 'Total',
                    text: 'Total',
                    format: '0'
                }
            ],
            features: [
                {
                    ftype: 'groupingsummary',
                    groupHeaderTpl: [
                        '{columnName}: {name}'
                    ],
                    showGroupsText: 'Return reults'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'datefield',
                            itemId: 'endDate',
                            fieldLabel: 'Start Date',
                            labelWidth: 80
                        },
                        {
                            xtype: 'datefield',
                            itemId: 'startDate',
                            fieldLabel: 'Start Date',
                            labelWidth: 80
                        },
                        {
                            xtype: 'button',
                            itemId: 'cmdPreviewVisits',
                            width: 110,
                            text: '<b>Preview</b>'
                        },
                        {
                            xtype: 'button',
                            itemId: 'cmdExportVisits',
                            width: 110,
                            text: '<b>Export</b>'
                        },
                        {
                            xtype: 'button',
                            itemId: 'cmdPrintVisits',
                            width: 100,
                            text: '<b>Print</b>'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});