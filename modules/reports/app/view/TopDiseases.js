/*
 * File: app/view/TopDiseases.js
 *
 * This file was generated by Sencha Architect version 4.1.1.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('ReportsMain.view.TopDiseases', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.topdiseases',

    requires: [
        'Ext.grid.View',
        'Ext.grid.column.Column',
        'Ext.toolbar.Paging'
    ],

    height: 650,
    width: 785,
    resizable: true,
    closable: true,
    collapsible: true,
    title: 'Top Diseases',
    columnLines: true,
    store: 'TopDiseasesStore',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'IcdCode',
                    text: 'Icd Code'
                },
                {
                    xtype: 'gridcolumn',
                    width: 331,
                    dataIndex: 'Description',
                    text: 'Description'
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'DiseaseCount',
                    text: 'Disease Count'
                }
            ],
            dockedItems: [
                {
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    width: 360,
                    displayInfo: true,
                    store: 'TopDiseasesStore'
                }
            ]
        });

        me.callParent(arguments);
    }

});