<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Africa/Nairobi');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../../../ExcelClasses/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Cashiers Shift Report" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("George Maina")
    ->setLastModifiedBy("George Maina")
    ->setTitle("Pharmacy Drugs Revenue Report")
    ->setSubject("CPharmacy Drugs Revenue Report")
    ->setDescription("CPharmacy Drugs Revenue Report contains all daily cash collections")
    ->setKeywords("Pharmacy Drugs Revenue Report")
    ->setCategory("Pharmacy Drugs Revenuet Report");

$objPHPExcel->getActiveSheet(0)->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Pharmacy Drugs Revenue Report');


$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'Group ID');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'Item ID' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'Description' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'Male' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'Female' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', 'Total' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G2', '<5 Yrs' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H2', '5-14 Yrs' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I2', '>14 Yrs' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J2', 'POSITIVE' );

$startDate = new DateTime($_REQUEST[startDate]);
$date1 = $startDate->format("Y-m-d");

$endDate = new DateTime($_REQUEST[endDate]);
$date2 = $endDate->format("Y-m-d");

$sql = "SELECT p.`group_id`,p.Item_Id,p.name AS Description FROM care_tz_laboratory_param p
            LEFT JOIN `care_test_request_chemlabor_sub` s ON p.`item_id`=s.`item_id`
            GROUP BY s.`item_id`";

if($debug) echo $sql;

$result=$db->Execute($sql);
$i=3;
while($row=$result->FetchRow()){

        $maleCounts=getLabCounts($row[Item_Id],'m',$date1,$date2,"");
        $femaleCounts=getLabCounts($row[Item_Id],'f',$date1,$date2,"");
        $total=$maleCounts+$femaleCounts;
        $below5=getLabCounts($row[Item_Id],'',$date1,$date2,"<");
        $between5and14=getLabCounts($row[Item_Id],'',$date1,$date2,"between");
        $above14=getLabCounts($row[Item_Id],'',$date1,$date2,">");
        $pos=getLabCounts($row[Item_Id],'',$date1,$date2,"POS");

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i",$row['group_id']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$i",$row['Item_Id']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C$i",$row['Description']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D$i",$maleCounts);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("E$i",$femaleCounts);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("F$i",$total);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("G$i",$below5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("H$i",$between5and14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("I$i",$above14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("J$i",$pos);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);

    $i=$i+1;
}

$objPHPExcel->getActiveSheet()->setTitle('Pharmacy drugs Revenue');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save(str_replace('php','xlsx',$root_path."/docs/PharmacyRevenue.xlsx"));

echo "Created file : ".str_replace('php','xlsx',$root_path."docs/PharmacyRevenue.xlsx" ),EOL;

$objReader=PHPExcel_IOFactory::load($root_path."docs/PharmacyRevenue.xlsx");

?>
<script>
    window.open('../../docs/PharmacyRevenue.xlsx', "Cashiers Shift Report",
        "menubar=no,toolbar=no,width=600,height=800,location=yes,resizable=yes,scrollbars=yes,status=yes");
</script>
<?php
function getLabCounts($itemID,$sex,$startDate,$endDate,$sign){
    global $db;
    $debug=false;

    $sql="SELECT COUNT(l.`item_id`) AS Male FROM `care_test_request_chemlabor_sub` l
            LEFT JOIN `care_test_request_chemlabor` s ON l.`batch_nr`=s.`batch_nr`
            LEFT JOIN care_encounter e ON l.`encounter_nr`=e.`encounter_nr`
            LEFT JOIN care_person n ON e.`pid`=n.`pid`
            LEFT JOIN `care_test_findings_chemlabor_sub` f ON s.`batch_nr` = f.`job_id`
            WHERE l.item_id='$itemID' and s.send_date between '$startDate' and '$endDate'";
    if($sex<>""){
        $sql.=" and n.`sex`='$sex'";
    }

    if($sign=="<"){
        $sql.=" and (YEAR(NOW())-YEAR(n.date_birth))<5";
    }else if($sign=="between"){
        $sql.=" and (YEAR(NOW())-YEAR(n.date_birth)) between 5 and 14";
    }else if($sign==">"){
        $sql.=" and (YEAR(NOW())-YEAR(n.date_birth))>14";
    }else if($sign=="POS"){
        $sql.=" and f.`parameter_value` = 'POS'";
    }

    $sql.=" GROUP BY l.`item_id` having count(l.item_id)>0";

    if($debug) echo $sql;

    $request=$db->Execute($sql);
    $row=$request->FetchRow();
    if($row['Male'] <> ''){
        $value = $row['Male'];
    }else{
        $value = 0;
    }

    return $value;
}


?>