<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Africa/Nairobi');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once '../../../ExcelClasses/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Cashiers Shift Report" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("George Maina")
    ->setLastModifiedBy("George Maina")
    ->setTitle("Pharmacy Drugs Revenue Report")
    ->setSubject("CPharmacy Drugs Revenue Report")
    ->setDescription("CPharmacy Drugs Revenue Report contains all daily cash collections")
    ->setKeywords("Pharmacy Drugs Revenue Report")
    ->setCategory("Pharmacy Drugs Revenuet Report");

$objPHPExcel->getActiveSheet(0)->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Pharmacy Drugs Revenue Report');


$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'PART CODE');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'DESCRIPTION' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'CATEGORY' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'UNIT PRICE' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'QUANTITIES' );
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', 'TOTAL AMOUNT' );

$date1 = $_REQUEST[startDate];
$date2 = $_REQUEST[endDate];

$sql = "SELECT b.`OP_no` as pid,p.encounter_nr,b.`patient_name`
            ,b.`item_id`,b.`Item_desc`,p.price,p.dosage,p.times_per_day,p.days,p.prescriber,b.`qty`,b.`issued` as issued,
            b.balance,(b.`issued`*b.price) as TotalCost,b.`input_user` as issuedBy,p.drug_class
            FROM care_ke_internal_orders b LEFT JOIN care_encounter_prescription p on b.presc_nr=p.nr
            WHERE p.drug_class in ('Drug_list','Medical Supplies')";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and order_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and order_date<=now()";
    }

    $sql = $sql . "GROUP BY item_id ORDER BY Total DESC";


// $sql = "SELECT b.partcode,b.Description,b.service_type,b.price,SUM(b.total) AS Total, sum(b.qty) AS drug_Count
// FROM care_ke_billing b WHERE b.service_type = 'drug_list'";

// if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
//     $date = new DateTime($date1);
//     $dt1 = $date->format("Y-m-d");

//     $date = new DateTime($date2);
//     $dt2 = $date->format("Y-m-d");

//     $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
// } else {
//     $sql = $sql . " and b.bill_date<=now()";
// }

// $sql = $sql . "GROUP BY b.partcode,b.Description,b.service_type ORDER BY Total DESC";
if($debug) echo $sql;

$result=$db->Execute($sql);
$i=3;
while($row=$result->FetchRow()){

    $sql2 = "SELECT price, issued,(issued * price) as TCost1 from care_ke_internal_orders where item_id = '".$row['item_id']."'";
        if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
            $date = new DateTime($date1);
            $dt1 = $date->format("Y-m-d");

            $date = new DateTime($date2);
            $dt2 = $date->format("Y-m-d");

            $sql2 = $sql2 . " and order_date between '$dt1' and '$dt2' ";
        } else {
            $sql2 = $sql2 . " and order_date<=now()";
        }

        $result2 = $db->Execute($sql2);
        $tcount = $result2->RecordCount();
        $TCost = '';
        $orign_qty = '';
        $Uprice = '';
        while ($row2 = $result2->FetchRow()) {
            $cost = round($row2['TCost1']/50) * 50;
            $TCost = $TCost + $cost;
            $orign_qty = $orign_qty + $row2['issued'];
            $Uprice = $row2['price'];
        }

    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i",$row['item_id']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("B$i",$row['Item_desc']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("C$i",$row['drug_class']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("D$i",$Uprice);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("E$i",$orign_qty);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue("F$i",$TCost);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

    $i=$i+1;
}

$objPHPExcel->getActiveSheet()->setTitle('Pharmacy drugs Revenue');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save(str_replace('php','xlsx',$root_path."/docs/PharmacyRevenue.xlsx"));

echo "Created file : ".str_replace('php','xlsx',$root_path."docs/PharmacyRevenue.xlsx" ),EOL;

$objReader=PHPExcel_IOFactory::load($root_path."docs/PharmacyRevenue.xlsx");

?>
<script>
    window.open('../../docs/PharmacyRevenue.xlsx', "Cashiers Shift Report",
        "menubar=no,toolbar=no,width=600,height=800,location=yes,resizable=yes,scrollbars=yes,status=yes");
</script>
