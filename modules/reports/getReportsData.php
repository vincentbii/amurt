
<?php
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$limit = $_REQUEST[limit];
$start = $_REQUEST[start];

$item_number = $_POST[item_number];
$rptType= $_REQUEST[rptType];
$drug=$_REQUEST[partcode];

$date1 = new DateTime($_REQUEST[startDate]);
$startDate = $date1->format("Y-m-d");

$date2 = new DateTime($_REQUEST[endDate]);
$endDate = $date2->format("Y-m-d");

$wardNo=$_REQUEST[wardNo];

// getRevenues();
$task = ($_REQUEST['task']) ? ($_REQUEST['task']) : $_POST['task'];
switch ($task) {
    case "getDiagnosis":
        getDiagnosis();
        break;
    case "getPersonnel":
        getPersonnel();
        break;
    case "getTopDiagnosis":
        getTopDiagnosis();
        break;
    case "getTopDiagnosis2":
        getTopDiagnosis2();
        break;
    case "getRevenues":
        getRevenues($rptType);
        break;
    case "getAdmDis":
        if ($_REQUEST['admdis'] == 'adm') {
            getAdmissions2($startDate,$endDate);
        } else {
            getDischarges2($startDate,$endDate);
        }
        break;
    case "lab":
        if ($_REQUEST['getLab'] == '1') {
            getLabActivity();
        } else if ($_REQUEST['getLab'] == '2') {
            getLabRevenue();
        } else {
            getLabPatientTests();
        }
        break;
    case "getLabActivities":
        getLabActivities();
        break;
    case "getLabRevenue":
        getLabRevenue2();
        break;
    case "xray":
        if ($_REQUEST['getXray'] == '1') {
            getXrayActivity();
        } else if ($_REQUEST['getXray'] == '2') {
            getXrayRevenue();
        } else {
            getXrayPatientTests();
        }
        break;
    case 'getXrayActivities':
        getXrayActivities();
        break;
    case "getXrayRevenue":
        getXrayRevenue2();
        break;
    case "drugs":
        if ($_REQUEST['drugsRpts'] == '1') {
            getRevenueByCat();
        } else if ($_REQUEST['drugsRpts'] == '2') {
            getRevenueByItem();
        } else {
            getPatientStatement();
        }
        break;
    case "getDrugStatement":
        getDrugStatement($drug,$startDate,$endDate,$start,$limit);
        break;
    case "patientdrugstatement":
        getPatientStatement2();
        break;
    case "pharmacyrevenue":
        getRevenueByItem2();
        break;
    case "drugsperpatient":
        getRevenueByPatients($drug);
        break;
    case "patientstatement":
        getPatientStatement();
        break;
    case "getDiseases":
        getDiseases();
        break;
    case "getLabTests":
        getLabTests($startDate,$endDate);
        break;
    case "getDiagnosisReports":
        getDiagnosisReports();
        break;
    case "getDistype":
        getDistype();
        break;
    case "getWards":
        getWards();
        break;
    case "getItemsList":
        getItemsList();
        break;
    case "getNhifClaims":
        getNhifClaims($startDate,$endDate);
        break;
    case "getClinics":
        getClinics($startDate,$endDate);
        break;
    case 'getFinalisedBills':
        getFinalisedBills($startDate,$endDate,$wardNo);
        break;
    default:
        echo "{failure:true}";
        break;

}//end switch


function getFinalisedBills($startDate,$endDate,$wardNo){
    global $db;
    $debug=false;

    $sql = "SELECT b.pid,p.`selian_pid` AS fileNo,b.encounter_nr,e.`encounter_class_nr`,CONCAT(p.`name_first`,' ',p.name_last,' ',p.`name_2`) AS `names`,b.bill_number
,e.`is_discharged`,e.`encounter_date` AS admissionDate,e.`discharge_date`,e.finalised,w.`description`,w.`nr`
FROM care_ke_billing b LEFT JOIN care_person p ON b.`pid`=p.`pid`  
LEFT JOIN care_encounter e ON b.`encounter_nr`=e.`encounter_nr`
LEFT JOIN care_ward w ON e.current_ward_nr=w.nr
WHERE e.finalised=1 AND e.`encounter_class_nr`=1 AND e.`is_discharged`=1 ";

    if($startDate<>'' && $endDate<>''){

        $sql=$sql." and e.discharge_date between '$startDate' and '$endDate'";
    }

    if($wardNo){

        $sql=$sql." and w.nr='$wardNo'";
    }

    $sql=$sql." GROUP BY encounter_nr";

    if($debug) echo $sql;
    $result=$db->Execute($sql);
    $total=$result->RecordCount();

    echo '{
    "total":"' . $total . '","finalisedBills":[';
    $counter=0;

    while($row=$result->FetchRow()){
        echo '{"pid":"' . $row[pid] .'","fileNo":"' . $row[fileNo].'","names":"' . $row[names] .'","billnumber":"' . $row[bill_number]
            .'","admissionDate":"' . $row[admissionDate].'","dischargeDate":"' . $row[discharge_date].'","ward":"' . $row[description].'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function getNhifClaims($startDate,$endDate){
    global $db;
    $debug=false;

    $sql = "SELECT b.creditNo,b.inputDate,b.admno,b.Names,b.admDate,b.disDate,b.wrdDays,b.nhifNo,b.nhifDebtorNo,
        b.debtorDesc,b.invAmount,b.totalCredit,b.balance,n.bill_number
        FROM care_ke_nhifcredits b left join care_ke_billing n on b.creditno=n.batch_no
        WHERE n.rev_code='NHIF' ";

    if($startDate && $endDate){

        $sql=$sql."and b.inputDate between '$startDate' and '$endDate'";
    }
    if($debug) echo $sql;
    $result=$db->Execute($sql);
    $total=$result->RecordCount();

    echo '{
    "total":"' . $total . '","nhifclaims":[';
    $counter=0;

    while($row=$result->FetchRow()){
        echo '{"ClaimNo":"' . $row[creditNo] .'","BillNumber":"' . $row[bill_number].'","NHIFNo":"' . $row[nhifNo] .'","PID":"' . $row[admno]
            .'","Names":"' . $row[Names].'","AdmissionDate":"' . $row[admDate].'","DischargeDate":"' . $row[disDate] .'","BedDays":"' . $row[wrdDays]
            .'","InvoiceAmount":"' . $row[invAmount] .'","TotalCredit":"' . $row[totalCredit].'","Balance":"' . $row[balance]
            .'","InputDate":"' . $row[inputDate].'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}


function getClinics($startDate,$endDate){
    global $db;
    $debug=false;

    $sql="SELECT proc_code,prec_desc, COUNT(prec_desc) AS PCOUNT, SUM(total) AS Amount FROM care_ke_receipts
            WHERE rev_desc='CONSULTATION' AND currdate BETWEEN '$startDate' AND '$endDate'	
            GROUP BY proc_code";
    if($debug) {
        echo $sql;
    }

    $result=$db->Execute($sql);
    $total=$result->RecordCount();
    echo '{
    "total":"' . $total . '","clinics":[';
    $counter=0;
    while($row=$result->FetchRow()){
        echo '{"RevenueCode":"' . $row[proc_code] .'","Description":"' . $row[prec_desc] .'","Count":"' . $row[PCOUNT].'","Amount":"' . $row[Amount].'"}';

        $counter++;

        if ($counter <> $total) {
            echo ",";
        }
    }
    echo ']}';
}

function getPersonnel() {
    global $db;
    $sql = 'Select ID,Staff_Name from care_ke_staff';
    $request = $db->execute($sql);
    echo '{"personnel":[';
    while ($row = $request->FetchRow()) {
        echo '{"ID":"' . $row[0] . '","StaffName":"' . $row[1] . '"},';
    }
    echo ']}';
}


function getWards() {
    global $db;
    $sql = 'Select nr,name from care_ward';
    $request = $db->execute($sql);
    echo '{"getwards":[';
    while ($row = $request->FetchRow()) {
        echo '{"No":"' . $row[0] . '","WardName":"' . $row[1] . '"},';
    }
    echo ']}';
}

function getDistype() {
    global $db;
    echo '{
   "getDistype":[';

    $sql1 = 'SELECT nr,`type` FROM care_type_discharge';
    $result1 = $db->Execute($sql1);
    while ($row = $result1->FetchRow()) {
        echo '{"No":"' . $row[0] . '","DisType":"' . $row[1] . '"},';
    }
    echo ']}';
}

function getLabCounts($itemID,$sex,$startDate,$endDate,$sign){
    global $db;
    $debug=false;

    $sql="SELECT COUNT(l.`item_id`) AS Male FROM `care_test_request_chemlabor_sub` l
            LEFT JOIN `care_test_request_chemlabor` s ON l.`batch_nr`=s.`batch_nr`
            LEFT JOIN care_encounter e ON l.`encounter_nr`=e.`encounter_nr`
            LEFT JOIN care_person n ON e.`pid`=n.`pid`
            LEFT JOIN `care_test_findings_chemlabor_sub` f ON s.`batch_nr` = f.`job_id`
            WHERE l.item_id='$itemID' and s.send_date between '$startDate' and '$endDate'";
    if($sex<>""){
        $sql.=" and n.`sex`='$sex'";
    }

    if($sign=="<"){
        $sql.=" and (YEAR(NOW())-YEAR(n.date_birth))<5";
    }else if($sign=="between"){
        $sql.=" and (YEAR(NOW())-YEAR(n.date_birth)) between 5 and 14";
    }else if($sign==">"){
        $sql.=" and (YEAR(NOW())-YEAR(n.date_birth))>14";
    }else if($sign=="POS"){
        $sql.=" and f.`parameter_value` = 'POS'";
    }

    $sql.=" GROUP BY l.`item_id` having count(l.item_id)>0";

    if($debug) echo $sql;

    $request=$db->Execute($sql);
    $row=$request->FetchRow();

    return $row[0];
}

function getLabTests($startDate,$endDate){
    global $db;
    $debug =false;

    $sql="SELECT p.`group_id`,p.Item_Id,p.name AS Description FROM care_tz_laboratory_param p
            LEFT JOIN `care_test_request_chemlabor_sub` s ON p.`item_id`=s.`item_id` LEFT JOIN `care_test_findings_chemlabor_sub` f ON s.batch_nr = f.job_id
            GROUP BY s.`item_id`";
    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":"'.$totalCount.'","labtests":[';

     $counter=0;
    while($row=$results->FetchRow()){

//        $startDate=#;
//        $endDate='2015-07-30';

        $maleCounts=getLabCounts($row[Item_Id],'m',$startDate,$endDate,"");
        $femaleCounts=getLabCounts($row[Item_Id],'f',$startDate,$endDate,"");
        $total=$maleCounts+$femaleCounts;
        $below5=getLabCounts($row[Item_Id],'',$startDate,$endDate,"<");
        $between5and14=getLabCounts($row[Item_Id],'',$startDate,$endDate,"between");
        $above14=getLabCounts($row[Item_Id],'',$startDate,$endDate,">");
        $pos=getLabCounts($row[Item_Id],'',$startDate,$endDate,"POS");

        echo '{"Group":"'.$row[group_id].'","ItemID":"'.$row[Item_Id].'","Description":"'.$row[Description]
              .'","Male":"'.$maleCounts.'","Female":"'.$femaleCounts.'","Total":"'.$total
              .'","Below5":"'.$below5.'","Between5And14":"'.$between5and14.'","Above14":"'.$above14.'","POS":"'.$pos.'"}';

        $counter++;
        if ($counter < $totalCount) {
            echo ",";
        }

    }



    echo "]}";

}


function getDiseases(){
    global $db;
    $debug =false;

    $sql="Select diagnosis_code,Description from care_icd10_en WHERE diagnosis_code LIKE 'O%'";
    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":"'.$totalCount.'","diseases":[';

    while($row=$results->FetchRow()){
        echo '{"IcdCode":"'.$row[0].'","Description":"'.$row[1].'"},';
    }

    echo "]}";

}


function getDiagnosisReports() {
    global $db;
    $age1 = $_REQUEST[age1];
    $age2 = $_REQUEST[age2];
    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $gender = $_REQUEST[gender];
    $icd1 = $_REQUEST[icd10];;
    $status = $_REQUEST[status];
    $visits = $_REQUEST[visits];
    $pid = $_REQUEST[pid];

    $sql = "SELECT distinct d.pid,p.selian_pid,p.name_first,p.name_last,p.name_2,p.date_birth,
        p.sex,(YEAR(NOW())-YEAR(p.date_birth)) AS age,
        d.encounter_nr,d.ICD_10_code,d.ICD_10_description,d.type,d.timestamp,d.pataintstatus FROM care_tz_diagnosis d left JOIN care_person p
ON d.PID=p.pid LEFT join care_encounter e on d.pid=e.pid";


    if ($date1) {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");
    } else {
        $dt1 = date("Y-m-d");
    }
    if ($date2) {
        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");
    } else {
        $dt2 = date("Y-m-d");
    }

    if ($dt1 <> "" && $dt2 <> "") {
        $sql = $sql . " where DATE_FORMAT(d.timestamp,'%Y-%m-%d') between '$dt1' and '$dt2' ";
    } else if ($dt1 <> '' && $dt2 == '') {
        $sql = $sql . " where DATE_FORMAT(d.timestamp,'%Y-%m-%d')= '$dt1'";
    } else {
        $sql = $sql . " where DATE_FORMAT(d.timestamp,'%Y-%m-%d')<=now()";
    }

    if (isset($gender) && $gender <> "") {
        if ($gender == 'Male') {
            $sex = 'M';
        } else if($gender == 'Female') {
            $sex = 'F';
        }
        $sql = $sql . " and sex='$sex'";
    }
    if ($icd1 <> "") {
        $sql = $sql . " and ICD_10_code ='$icd1";
    }

    if (isset($age1) && $age2 <> "") {
        $sql = $sql . " having (YEAR(NOW())-YEAR(p.date_birth)) between '$age1' and '$age2'";
    } else if ($age1 <> "" && $age2 == "") {
        $sql = $sql . " having (YEAR(NOW())-YEAR(p.date_birth))='$age1'";
    }

    if ($pid <> "") {
        $sql = $sql . " and d.pid='$pid'";
    }

    if($status<>""){
        if($status=="Dead"){
            $dStat='D';
        }else{
            $dStat='A';
        }
        $sql=$sql." and pataintstatus='$dStat'";
    }

    if($visits){
        $sql=$sql." and `type`='$status";
    }
//   echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);
    $rowCnt=$request->RecordCount();

    echo '{ "total":"' . $rowCnt . '","diagnosis":[';

    $counter=0;
        while ($row = $request->FetchRow()) {
            if ($row[pataintstatus] == 'A') {
                $stat = 'Alive';
            } else if ($row[pataintstatus] == 'D') {
                $stat = 'Dead';
            } else {
                $stat = '';
            }

            echo '{"PID":"' . $row[pid] . '","Names":"' .  $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2]  . '","Date":"' .  $row[timestamp]
                . '","Gender":"' .  $row[sex] . '","Age":"' .  $row[date_birth]. '","AdmissionNo":"' .  $row[selian_pid]. '","Visit":"' .  $row[type]
                . '","DiagnosisCode":"' .  $row[ICD_10_code]. '","Description":"' . $row[ICD_10_description]. '","PatientStatus":"' .  $stat.'"}';

            $counter++;
            if ($counter <> $rowCnt) {
                echo ",";
            }

        }

        echo "]}";

}

function getDiagnosis() {
    global $db;
    $age1 = $_REQUEST[age1];
    $age2 = $_REQUEST[age2];
    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $gender = $_REQUEST[gender];
    $icd1 = $_REQUEST[icd1];
    $icd2 = $_REQUEST[icd2];
    $task = $_REQUEST[task];
    $visits = $_REQUEST[visits];
    $pid = $_REQUEST[pid];

    $sql = "SELECT distinct d.pid,p.selian_pid,p.name_first,p.name_last,p.name_2,p.date_birth,
        p.sex,(YEAR(NOW())-YEAR(p.date_birth)) AS age,
        d.encounter_nr,d.ICD_10_code,d.ICD_10_description,d.type,d.timestamp,d.pataintstatus FROM care_tz_diagnosis d left JOIN care_person p
ON d.PID=p.pid LEFT join care_encounter e on d.pid=e.pid";



    if ($date1) {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");
    } else {
        $dt1 = "";
    }
    if ($date2) {
        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");
    } else {
        $dt2 = "";
    }

    if ($dt1 <> "" && $dt2 <> "") {
        $sql = $sql . " where DATE_FORMAT(d.timestamp,'%Y-%m-%d') between '$dt1' and '$dt2' ";
    } else if ($dt1 <> '' && $dt2 == '') {
        $sql = $sql . " where DATE_FORMAT(d.timestamp,'%Y-%m-%d')= '$dt1'";
    } else {
        $sql = $sql . " where DATE_FORMAT(d.timestamp,'%Y-%m-%d')<=now()";
    }

    if (isset($gender) && $gender <> "") {
        if ($gender == 1) {
            $sex = 'M';
        } else {
            $sex = 'F';
        }
        $sql = $sql . " and sex='$sex'";
    }
    if ($icd1 <> "" && $icd2 <> "") {
        $sql = $sql . " and ICD_10_code between '$icd1' and '$icd2'";
    } else if ($icd1 <> "" ) {
        $sql = $sql . " and ICD_10_code = '$icd1'";
    }

    if (isset($age1) && $age2 <> "") {
        $sql = $sql . " having (YEAR(NOW())-YEAR(p.date_birth)) between '$age1' and '$age2'";
    } else if ($age1 <> "" && $age2 == "") {
        $sql = $sql . " having (YEAR(NOW())-YEAR(p.date_birth))='$age1'";
    }

    if ($pid <> "") {
        $sql = $sql . " and d.pid='$pid'";
    }
//   echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="center">pid</td>
                    <td align="center">Names</td>
                    <td align="center">Date</td>
                     <td align="center">Gender</td>
                      <td align="center">Age</td>
                    <td align="center">Adm NO</td>
                    <td align="center">Status</td>


                    <td align="center">diagnosis code</td>
                    <td align="center">Description</td>
                    <td align="center"> Visit</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($row[pataintstatus] == 'A') {
            $stat = 'Alive';
        } else if ($row[pataintstatus] == 'D') {
            $stat = 'Dead';
        } else {
            $stat = '';
        }
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[pid] . '</td>
                         <td>' . $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2] . '</td>
                    <td>' . $row[timestamp] . '</td>
                    <td>' . $row[sex] . '</td>
                    <td>' . $row[age] . '</td>
                    <td>' . $row[selian_pid] . '</td>
                    <td>' . $stat . '</td>
                    <td>' . $row[ICD_10_code] . '</td>
                    <td>' . $row[ICD_10_description] . '</td>
                    <td>' . $row[type] . '</td>    
                   
             </tr>';

        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=6><br>No of Patients $rowCnt</td></tr>";
    echo '</table>';
}

function getTopDiagnosis() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT COUNT(d.pid) AS diagCount,(YEAR(NOW())-YEAR(p.date_birth)) AS age,
        d.encounter_nr,d.ICD_10_code,d.ICD_10_description,d.type,d.timestamp FROM care_tz_diagnosis d INNER JOIN care_person p
ON d.PID=p.pid ";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " where timestamp between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " where timestamp<=now()";
    }

    $sql = $sql . " GROUP BY ICD_10_code order by diagCount desc";


//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">diagnosis code</td>
                    <td align="left">Description</td>
                    <td align="left">Count</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[ICD_10_code] . '</td>
                    <td>' . $row[ICD_10_description] . '</td>
                    <td>' . $row[diagCount] . '</td>    
                   
             </tr>';

        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=3><br>No of Patients $rowCnt</td></tr>";
    echo '</table>';
}


function getTopDiagnosis2() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $adm = $_REQUEST[adm];

    $sql = "SELECT COUNT(d.pid) AS diagCount,(YEAR(NOW())-YEAR(p.date_birth)) AS age,
        d.encounter_nr,d.ICD_10_code,d.ICD_10_description,d.type,d.timestamp FROM care_tz_diagnosis d INNER JOIN care_person p
        ON d.PID=p.pid ";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " where timestamp between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " where timestamp='".date('Y-m-d')."'";
    }

//    if(isset($adm) && $adm<>'' && $adm=='OPC'){
//        $sql = $sql . " and ICD_10_code like 'OPC%";
//    }else{
//        $sql = $sql . " and ICD_10_code like 'OP%";
//    }

    $sql = $sql . " GROUP BY ICD_10_code order by diagCount desc";

    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":'.$totalCount.',"topdiseases":[';

    $counter=0;
    while($row=$results->FetchRow()){
        echo '{"IcdCode":"'.$row[ICD_10_code].'","Description":"'.trim($row[ICD_10_description]).'","DiseaseCount":"'.$row[diagCount].'"}';

        $counter++;
        if ($counter <> $totalCount) {
            echo ",";
        }

    }

    echo "]}";
}


function getAdmissions2($startDate,$endDate) {
    global $db;

    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $wards = $_REQUEST[ward];
    $disType = $_REQUEST[disType];
    $grpWards = $_REQUEST[grpWards];
    $sex = $_REQUEST[sex];

    $sql = "SELECT  e.pid,e.newAdm_No,p.name_first,p.name_last,p.name_2,p.sex,p.date_birth,e.encounter_date,e.encounter_time,
  e.current_ward_nr,w.name  AS wardname,(DATEDIFF(DATE(NOW()),e.encounter_date)) AS BedDays FROM care_person p
  INNER JOIN care_encounter e ON (p.pid = e.pid) left join care_ward w on e.current_ward_nr=w.nr";

    if ($date1) {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");
    } else {
        $dt1 = date("Y-m-d");
    }
    if ($date2) {
        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");
    } else {
        $dt2 = date("Y-m-d");
    }


    if ($dt1 <> "" && $dt2 <> "") {
        $sql = $sql . " where e.encounter_date between '$dt1' and '$dt2' ";
    } else if ($dt1 <> '' && $dt2 == '') {
        $sql = $sql . " where e.encounter_date = '$dt1'";
    } else {
        $sql = $sql . " where e.encounter_date<=now()";
    }

    if ($wards) {
        $sql = $sql . " and e.current_ward_nr=$wards";
    }

    if ($sex) {
        $sql = $sql . " and p.sex='$sex'";
    }

    if ($grpWards) {
        if ($grpWards == 'adults') {
            $wards = " in('1','2','4','5')";
        } elseif ($grpWards == 'paeds') {
            $wards = " in ('6')";
        } elseif ($grpWards == 'mat') {
            $wards = " in ('5')";
        }
        $sql = $sql . " and e.current_ward_nr $wards";
    }

    $sql = $sql . " and e.encounter_class_nr = 1 order by e.encounter_date desc";

    if($debug) echo $sql;

    $request = $db->Execute($sql);
    $rowCnt = $request->RecordCount();

    echo '{ "total":"' . $rowCnt . '","admdis":[';

    $counter=0;
    while ($row = $request->FetchRow()) {
        //e.pid,e.newAdm_No,p.name_first,p.name_last,p.name_2,p.sex,p.date_birth,e.encounter_date,e.encounter_time,
        //e.current_ward_nr,w.name
        echo '{"PID":"' . $row[pid] . '","AdmissionNo":"' . $row[newAdm_No] . '","Names":"' .  $row[name_first].' '.$row[name_last].' '.$row[name_2]
            . '","Sex":"' .  $row[sex] . '","Dob":"' .  $row[date_birth]. '","AdmissionDate":"' .  $row[encounter_date]
            . '","Ward":"' .  $row[wardname]. '","BedDays":"' .  $row[BedDays].'"}';

        if ($counter <> $rowCnt) {
            echo ",";
        }
        $counter++;
    }

    echo "]}";


}

function getAdmissions() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $wards = $_REQUEST[wards];
    $grpWards = $_REQUEST[grpWards];
    $sex = $_REQUEST[sex];

    $sql = "SELECT  e.pid,e.newAdm_No,p.name_first,p.name_last,p.name_2,p.sex,p.date_birth,e.encounter_date,e.encounter_time,
  e.current_ward_nr,w.name FROM care_person p INNER JOIN care_encounter e ON (p.pid = e.pid) left join care_ward w 
  on e.current_ward_nr=w.nr";

    if ($date1) {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");
    } else {
        $dt1 = "";
    }
    if ($date2) {
        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");
    } else {
        $dt2 = "";
    }



    if ($dt1 <> "" && $dt2 <> "") {
        $sql = $sql . " where e.encounter_date between '$dt1' and '$dt2' ";
    } else if ($dt1 <> '' && $dt2 == '') {
        $sql = $sql . " where e.encounter_date = '$dt1'";
    } else {
        $sql = $sql . " where e.encounter_date<=now()";
    }

    if ($wards) {
        $sql = $sql . " and e.current_ward_nr=$wards";
    }

    if ($sex) {
        $sql = $sql . " and p.sex='$sex'";
    }

    if ($grpWards) {
        if ($grpWards == 'adults') {
            $wards = " in('11','12','14','15','22','23','24')";
        } elseif ($grpWards == 'paeds') {
            $wards = " in ('13')";
        } elseif ($grpWards == 'mat') {
            $wards = " in ('26')";
        }
        $sql = $sql . " and e.current_ward_nr $wards";
    }

    $sql = $sql . " and e.encounter_class_nr = 1 order by e.encounter_date desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">PID</td>
                    <td align="left">Admission No</td>
                    <td align="left">Names</td>
                    <td align="left">sex</td>
                    <td align="left">DOB</td>
                    <td align="left">Admission Date</td>
                    <td align="left">Ward</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[pid] . '</td>
                    <td>' . $row[newAdm_No] . '</td>
                    <td>' . $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2] . '</td>
                    <td>' . $row[sex] . '</td>    
                    <td>' . $row[date_birth] . '</td>
                    <td>' . $row[encounter_date] . '</td>
                    <td>' . $row[name] . '</td>    
             </tr>';

        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=3><br>No of Patients $rowCnt</td></tr>";
    echo '</table>';
}


function getDischarges2($startDate,$endDate) {
    global $db;
    $debug=false;

    $ward= $_REQUEST[ward];
    $grpWards = $_REQUEST[grpWards];
    $sex = $_REQUEST[sex];
    $discType=$_REQUEST[discTypes];

    $sql = "SELECT p.pid,e.encounter_nr, CONCAT(p.name_first,' ',p.name_last,' ',p.name_2) AS NAMES,p.`date_birth`,p.`sex`,b.`bill_number`
                ,e.encounter_date,e.`encounter_time`,e.`discharge_date`,
               l.discharge_type_nr,e.current_ward_nr,w.name AS wardName,DATEDIFF(e.discharge_date,e.`encounter_date`) AS BedDays ,
               SUM(IF( b.service_type NOT IN('payment','NHIF'),total,0)) AS bill,
               SUM(IF(b.service_type IN ('payment','NHIF'),total,0)) AS payment
               FROM care_ke_billing b
                LEFT JOIN care_encounter e ON b.encounter_nr=e.`encounter_nr`
                LEFT JOIN care_person p  ON b.pid=p.pid
                LEFT JOIN care_ward w ON e.current_ward_nr=w.nr
                LEFT JOIN care_encounter_location l ON l.encounter_nr=e.encounter_nr ";


    if ($startDate <> "" && $endDate <> "") {
        $sql = $sql . " where e.discharge_date between '$startDate' and '$endDate' ";
    } else if ($startDate <> '' && $endDate == '') {
        $sql = $sql . " where e.discharge_date = '$startDate'";
    } else {
        $sql = $sql . " where e.discharge_date<=now()";
    }

    if ($ward) {
        $sql = $sql . " and w.nr=$ward";
    }

    if ($sex) {
        $sql = $sql . " and p.sex='$sex'";
    }

    if ($grpWards) {
        if ($grpWards == 'adults') {
            $wards = " in('11','12','14','15','22','23','24')";
        } elseif ($grpWards == 'paeds') {
            $wards = " in ('13')";
        } elseif ($grpWards == 'mat') {
            $wards = " in ('26')";
        }
        $sql = $sql . " and e.current_ward_nr $wards";
    }

    if ($discType) {
        $sql = $sql . " and l.`discharge_type_nr`='$discType'";
    }

    $sql = $sql . " and e.encounter_class_nr = 1 AND e.is_discharged = 1  AND l.`type_nr`=5   GROUP BY p.`pid` order by e.discharge_date desc";

    if($debug) echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    $rowCnt = $request->RecordCount();

    echo '{ "total":"' . $rowCnt . '","admdis":[';

    $counter=0;
    while ($row = $request->FetchRow()) {

        //e.pid,e.newAdm_No,p.name_first,p.name_last,p.name_2,p.sex,p.date_birth,e.encounter_date,e.encounter_time,
        //e.discharge_date,(DATEDIFF(e.discharge_date,e.encounter_date)) AS bDays,w.name as wardName

        $bal= $row[bill]-$row[payment];
        echo '{"PID":"' . $row[pid] . '","Names":"' . $row[NAMES] . '","Dob":"' .  $row[date_birth]
            . '","Sex":"' .  $row[sex] . '","InvoiceNo":"' .  $row[bill_number]. '","AdmissionDate":"' .  $row[encounter_date]. '","DischargeDate":"' .  $row[discharge_date]
            . '","Ward":"' .  $row[wardName]. '","BedDays":"' .  $row[BedDays]. '","InvoiceAmount":"' .  $row[bill]. '","AmountPaid":"' .  $row[payment]. '","Balance":"' .  $bal.'"}';

        if ($counter <> $rowCnt) {
            echo ",";
        }
        $counter++;
    }

    echo "]}";
}


function getDischarges() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $wards = $_REQUEST[wards];
    $grpWards = $_REQUEST[grpWards];
    $sex = $_REQUEST[sex];
    $discType=$_REQUEST[discTypes];

    $sql = "SELECT DISTINCT e.pid,e.newAdm_No,p.name_first,p.name_last,p.name_2,p.sex,p.date_birth,e.encounter_date,e.encounter_time,
    e.discharge_date,(DATEDIFF(e.discharge_date,e.encounter_date)) AS bDays,w.name as wardName  FROM care_encounter e
    INNER JOIN care_person p ON (e.pid = p.pid) left join care_ward w on e.current_ward_nr=w.nr";

    if ($date1) {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");
    } else {
        $dt1 = "";
    }
    if ($date2) {
        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");
    } else {
        $dt2 = "";
    }

    if ($dt1 <> "" && $dt2 <> "") {
        $sql = $sql . " where e.discharge_date between '$dt1' and '$dt2' ";
    } else if ($dt1 <> '' && $dt2 == '') {
        $sql = $sql . " where e.discharge_date = '$dt1'";
    } else {
        $sql = $sql . " where e.discharge_date<=now()";
    }

    if ($wards) {
        $sql = $sql . " and e.current_ward_nr=$wards";
    }

    if ($sex) {
        $sql = $sql . " and p.sex='$sex'";
    }

    if ($grpWards) {
        if ($grpWards == 'adults') {
            $wards = " in('11','12','14','15','22','23','24')";
        } elseif ($grpWards == 'paeds') {
            $wards = " in ('13')";
        } elseif ($grpWards == 'mat') {
            $wards = " in ('26')";
        }
        $sql = $sql . " and e.current_ward_nr $wards";
    }

    if ($discType) {
        $sql = $sql . " and l.`discharge_type_nr`='$discType'";
    }

    $sql = $sql . " and e.encounter_class_nr = 1 AND e.is_discharged = 1 order by e.discharge_date desc";

    if($debug) echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">PID</td>
                    <td align="left">Admission No</td>
                    <td align="left">Names</td>
                    <td align="left">sex</td>
                    <td align="left">DOB</td>
                    <td align="left">Admission Date</td>
                    <td align="left">Discharge Date</td>
                    <td align="left">Bed Days</td>
                    <td align="left">Ward</td>
                 </tr>';
    $bg = '';
//        $total='';
    $days=0;
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[pid] . '</td>
                    <td>' . $row[newAdm_No] . '</td>
                    <td>' . $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2] . '</td>
                    <td>' . $row[sex] . '</td>    
                    <td>' . $row[date_birth] . '</td>
                    <td>' . $row[encounter_date] . '</td>
                    <td>' . $row[discharge_date] . '</td>
                    <td>' . $row[bDays] . '</td>
                    <td>' . $row[wardName] . '</td>
             </tr>';

        $rowbg = 'white';
        $days=$days+$row[bDays];
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=3><br><b>No of Patients $rowCnt</b></td><td colspan='4'></td></td><td colspan=''><b> Total Bed Days $days</b></td></tr>";
    echo '</table>';
}

function getLabRevenue2(){
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT b.`proc_code` AS partcode, b.`rev_desc` AS service_type,b.`Prec_desc` AS Description,
                b.`amount`,SUM(b.total) AS Total,COUNT(b.`proc_code`) AS Lab_Count
            FROM care_ke_receipts b WHERE b.`rev_desc` = 'laboratory'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.currdate between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.currdate<=now()";
    }

    $sql = $sql . "  GROUP BY b.proc_code order by SUM(b.total) desc";
   // echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);
    $rowCnt=$request->RecordCount();

    $counter=0;

    echo '{"totalCount":'.$rowCnt.',"labrevenue":[';
    while ($row = $request->FetchRow()) {
        if($row[admission]=='1'){
            $adm='IP';
        }else{
            $adm='OP';
        }
        echo '{"LabCode":"' . $row[partcode] . '","Description":"' . $row[Description]
            . '","TotalTests":"' .  $row[Lab_Count]  . '","Price":"' .   number_format($row[amount],2)  . '","TotalCost":"' .  number_format($row[Total],2).'"}';

        if ($counter <> $rowCnt) {
            echo ",";
        }
        $counter++;
    }

    echo "]}";
}

function getLabRevenue() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT b.`proc_code` AS partcode, b.`rev_desc` AS service_type,b.`Prec_desc` AS Description,
SUM(b.total) AS Total,COUNT(b.`proc_code`) AS Lab_Count
FROM care_ke_receipts b WHERE b.`rev_desc` = 'laboratory'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.currdate between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.currdate<=now()";
    }

    $sql = $sql . "  GROUP BY b.proc_code,b.rev_desc, b.Prec_desc order by b.proc_code desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">Lab Code</td>
                    <td align="left">Description</td>
                    <td align="left">Total Tests</td>
                    <td align="left">Total Amount</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[partcode] . '</td>
                    <td>' . $row[Description] . '</td>
                    <td>' . $row[Lab_Count] . '</td>    
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
             </tr>';
        $lsum = $lsum + $row[Total];
        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
    echo '</table>';
}

function getLabActivity() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $age1=$_REQUEST[age1];
    $age2=$_REQUEST[age2];


    $sql = "SELECT p.pid,p.name_first,p.name_last,p.name_2,b.`IP-OP`,b.partcode,b.service_type,b.Description,
  b.total AS Total,b.qty AS Lab_Count FROM care_ke_billing b INNER JOIN care_person p ON (b.pid = p.pid)
  WHERE b.service_type = 'laboratory'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }



    $sql = $sql . " order by b.partcode desc";
    if($debug) echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">PID</td>
                    <td align="left">Names</td>
                    <td align="left">Lab Code</td>
                    <td align="left">description</td>
                    <td align="left">Total</td>
                    <td align="left">Qty</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[pid] . '</td>
                    <td>' . $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2] . '</td>
                    <td>' . $row[partcode] . '</td>    
                    <td>' . $row[Description] . '</td>    
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
                    <td>' . $row[Lab_Count] . '</td>  
             </tr>';
        $lsum = $lsum + $row[Total];
        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
    echo '</table>';
}

function getLabPatientTests() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $pid = $_REQUEST[pid];

    $sql = "SELECT p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.`IP-OP`,b.partcode,b.service_type,b.Description,
  b.total AS Total,b.qty AS Lab_Count FROM care_ke_billing b INNER JOIN care_person p ON (b.pid = p.pid)
  WHERE b.service_type = 'laboratory'";

    if($pid){
        $sql.="  and b.pid=$pid";
    }

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    $sql = $sql . " order by b.bill_date desc";

    if($debug) echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    if ($request = $db->Execute($sql)) {

        $row1 = $request->FetchRow();
        echo '<table width=100% height=14>
        <tr><td colspan=6><br></td></tr>     
        <tr><td align="left"><b>PID:</b></td> <td>' . $row1[pid] . '</td></tr>
        <tr><td align="left"><b>Names:</b></td><td>' . $row1[name_first] . ' ' . $row1[name_last] . ' ' . $row1[name_2] . '</td></tr>
        <tr bgcolor=#6699cc>
                    <td align="left">Date</td>
                    <td align="left">Admission</td>
                    <td align="left">Lab Code</td>
                    <td align="left">description</td>
                    <td align="left">Qty</td>
                    <td align="left">Total</td>
                 </tr>';
        $bg = '';
//        $total='';
        while ($row = $request->FetchRow()) {
            if ($bg == "silver")
                $bg = "white";
            else
                $bg = "silver";
            if ($row[`IP-OP`] == 1) {
                $enc_class = 'IP';
            } else {
                $enc_class = 'OP';
            }
            echo '<tr bgcolor=' . $bg . ' height=16>
                
                    <td>' . $row[bill_date] . '</td>  
                    <td>' . $enc_class . '</td>    
                    <td>' . $row[partcode] . '</td>    
                    <td>' . $row[Description] . '</td>    
                    <td>' . $row[Lab_Count] . '</td>  
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
             </tr>';
            $lsum = $lsum + $row[Total];
            $rowbg = 'white';
        }
        $rowCnt = $request->RecordCount();

        echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
        echo '</table>';
    } else {
        echo 'SQL: Failed=' . $sql;
    }
}

function getLabActivities(){
    global $db;
    $debug=false;

    $date1 =$_REQUEST[date1];
    $date2 =$_REQUEST[date2];
    $age1=$_REQUEST[age1];
    $age2=$_REQUEST[age2];
    $pid=$_REQUEST[pid];
    $requestedBy=$_REQUEST[staffName];

    $sql = "SELECT p.pid,CONCAT(p.name_first,' ',p.name_last,' ',p.name_2) AS pnames,b.`bill_date`,
              b.`bill_time`,b.`IP-OP` AS admission,b.partcode,b.service_type,b.Description,
              b.total AS Total,b.qty AS Lab_Count,b.`status`,b.`input_user`  FROM care_ke_billing b INNER JOIN care_person p ON (b.pid = p.pid)
              WHERE b.service_type = 'laboratory'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    if(isset($pid) && $pid<>''){
        $sql = $sql . " and p.pid='$pid'";
    }

    if(isset($requestedBy) && $requestedBy<>''){
        $sql=$sql . " and b.input_user='$requestedBy'";
    }

    $sql = $sql . " GROUP BY p.`pid`,b.`Description` order by b.partcode desc";

    if($debug) echo $sql;

    $request = $db->Execute($sql);
    $rowCnt=$request->RecordCount();

    $counter=0;

    echo '{"totalCount":'.$rowCnt.',"labactivities":[';
    while ($row = $request->FetchRow()) {
        if($row[admission]=='1'){
            $adm='IP';
        }else{
            $adm='OP';
        }

        $names=preg_replace('/[^A-Za-z0-9\-]/', '', $row[pnames]);
        echo '{"PID":"' . $row[pid] . '","Names":"' . $names . '","Bill_Date":"' . $row[bill_date]
            . '","Bill_Time":"' . $row[bill_time]. '","Admission":"' .  $adm. '","LabCode":"' .  $row[partcode]
            . '","Description":"' .  $row[Description]  . '","Qty":"' .  $row[Lab_Count]
            . '","Total":"' .  $row[Total]. '","RequestedBy":"' . $row[input_user].'"}';

        if ($counter <> $rowCnt) {
            echo ",";
        }
        $counter++;
    }

    echo "]}";
}

function getXrayActivities(){
    global $db;
    $debug=false;

    $date1 ='01-01-2016';//$_REQUEST[date1];
    $date2 ='31-06-2016';//$_REQUEST[date2];
    $age1= $_REQUEST[age1];
    $age2=$_REQUEST[age2];


    $sql = "SELECT p.pid,CONCAT(p.name_first,' ',p.name_last,' ',p.name_2) AS pnames,b.`bill_date`,b.`bill_time`,b.`IP-OP` as admission,
      b.partcode,b.service_type,b.Description, b.total AS Total,b.qty AS Lab_Count,b.`input_user` FROM care_ke_billing b
      INNER JOIN care_person p ON (b.pid = p.pid) WHERE b.service_type = 'XRAY'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    $sql = $sql . " order by b.partcode desc";
    if($debug) echo $sql;

    $request = $db->Execute($sql);
    $rowCnt=$request->RecordCount();

    $counter=0;

    echo '{"totalCount":'.$rowCnt.',"xrayactivities":[';
    while ($row = $request->FetchRow()) {
        if($row[admission]=='1'){
            $adm='IP';
        }else{
            $adm='OP';
        }
        echo '{"PID":"' . $row[pid] . '","Names":"' . $row[pnames] . '","SendDate":"' . $row[bill_date] . '","SendTime":"' . $row[bill_time]
            . '","Admission":"' .  $adm. '","LabCode":"' .  $row[partcode]  . '","Description":"' .  $row[Description]
            . '","Qty":"' .  $row[Lab_Count]. '","Total":"' .  $row[Total]. '","InputUser":"' .  $row[InputUser].'"}';

        if ($counter <> $rowCnt) {
            echo ",";
        }
        $counter++;
    }

    echo "]}";
}

function getXrayRevenue2(){
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];

    $sql = "SELECT b.`proc_code` AS partcode, b.`rev_desc` AS service_type,b.`Prec_desc` AS Description,
SUM(b.total) AS Total,b.`amount` AS price,COUNT(b.`proc_code`) AS xray_Count
        FROM care_ke_receipts b WHERE b.`rev_desc` = 'xray'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.currdate between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.currdate<=now()";
    }

    $sql = $sql . "  GROUP BY b.proc_code,b.rev_desc, b.Prec_desc order by b.`proc_code` desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);
    $rowCnt=$request->RecordCount();

    $counter=0;

    echo '{"totalCount":'.$rowCnt.',"xrayrevenue":[';
    while ($row = $request->FetchRow()) {
        if($row[admission]=='1'){
            $adm='IP';
        }else{
            $adm='OP';
        }
        echo '{"xraycode":"' . $row[partcode] . '","Description":"' . $row[Description]
            . '","TotalTests":"' .  $row[xray_Count]  . '","Price":"' .   number_format($row[price],2)  . '","Total":"' .  number_format($row[Total],2).'"}';

        if ($counter <> $rowCnt) {
            echo ",";
        }
        $counter++;
    }

    echo "]}";
}

function getXrayRevenue() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];

    $sql = "SELECT b.`proc_code` AS partcode, b.`rev_desc` AS service_type,b.`Prec_desc` AS Description,
SUM(b.total) AS Total,b.`amount` AS price,COUNT(b.`proc_code`) AS Lab_Count
        FROM care_ke_receipts b WHERE b.`rev_desc` = 'xray'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.currdate between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.currdate<=now()";
    }

    $sql = $sql . "  GROUP BY b.proc_code,b.rev_desc, b.Prec_desc order by b.`proc_code` desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">Xray Code</td>
                    <td align="left">Description</td>
                    <td align="left">Total Tests</td>
                    <td align="left">Price</td>
                    <td align="left">Total Amount</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[partcode] . '</td>
                    <td>' . $row[Description] . '</td>
                    <td>' . $row[Lab_Count] . '</td>    
                    <td>' . $row[price] . '</td> 
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
             </tr>';
        $lsum = $lsum + $row[Total];
        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=4><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
    echo '</table>';
}

function getXrayActivity() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT p.pid,p.name_first,p.name_last,p.name_2,b.`IP-OP`,b.partcode,b.service_type,b.Description,
  b.total AS Total,b.qty AS Lab_Count FROM care_ke_billing b INNER JOIN care_person p ON (b.pid = p.pid)
  WHERE b.service_type = 'xray'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    $sql = $sql . " order by b.partcode desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">PID</td>
                    <td align="left">Names</td>
                    <td align="left">Lab Code</td>
                    <td align="left">description</td>
                    <td align="left">Total</td>
                    <td align="left">Qty</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[pid] . '</td>
                    <td>' . $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2] . '</td>
                    <td>' . $row[partcode] . '</td>    
                    <td>' . $row[Description] . '</td>    
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
                    <td>' . $row[Lab_Count] . '</td>  
             </tr>';
        $lsum = $lsum + $row[Total];
        $rowbg = 'white';
    }
    $rowCnt = $request->RecordCount();

    echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
    echo '</table>';
}

function getXrayPatientTests() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $pid = $_REQUEST[pid];

    $sql = "SELECT p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.`IP-OP`,b.partcode,b.service_type,b.Description,
  b.total AS Total,b.qty AS Lab_Count FROM care_ke_billing b INNER JOIN care_person p ON (b.pid = p.pid)
  WHERE b.service_type = 'xray' and b.pid=$pid";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    $sql = $sql . " order by b.bill_date desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    if ($request = $db->Execute($sql)) {

        $row1 = $request->FetchRow();
        echo '<table width=100%>
        <tr><td colspan=6><br></td></tr>     
        <tr><td align="left"><b>PID:</b></td> <td>' . $row1[pid] . '</td></tr>
        <tr><td align="left"><b>Names:</b></td><td>' . $row1[name_first] . ' ' . $row1[name_last] . ' ' . $row1[name_2] . '</td></tr>
        <tr bgcolor=#6699cc>
                    <td align="left">Date</td>
                    <td align="left">Admission</td>
                    <td align="left">Lab Code</td>
                    <td align="left">description</td>
                    <td align="left">Qty</td>
                    <td align="left">Total</td>
                 </tr>';
        $bg = '';
//        $total='';
        while ($row = $request->FetchRow()) {
            if ($bg == "silver")
                $bg = "white";
            else
                $bg = "silver";
            if ($row[`IP-OP`] == 1) {
                $enc_class = 'IP';
            } else {
                $enc_class = 'OP';
            }
            echo '<tr bgcolor=' . $bg . ' height=16>
                
                    <td>' . $row[bill_date] . '</td>  
                    <td>' . $enc_class . '</td>    
                    <td>' . $row[partcode] . '</td>    
                    <td>' . $row[Description] . '</td>    
                    <td>' . $row[Lab_Count] . '</td>  
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
             </tr>';
            $lsum = $lsum + $row[Total];
            $rowbg = 'white';
        }
        $rowCnt = $request->RecordCount();

        echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
        echo '</table>';
    } else {
        echo 'SQL: Failed=' . $sql;
    }
}

function getRevenues($rptType) {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];

    $date = new DateTime($date1);
    $dt1 = $date->format("Y-m-d");

    $date = new DateTime($date2);
    $dt2 = $date->format("Y-m-d");
    
    if($rptType=='oprevenue'){
         $sql = "SELECT rev_desc,SUM(Total) AS Total FROM care_ke_receipts WHERE currdate BETWEEN '$dt1' AND  '$dt2'
                    GROUP BY rev_desc";
    }else if($rptType=='iprevenue'){
        $sql = "SELECT service_type as rev_desc,SUM(total) AS Total FROM care_ke_billing WHERE bill_date BETWEEN '$dt1' AND '$dt2'
                AND `service_type` not in ('payment','NHIF')
                    GROUP BY service_type";
//
    }else if($rptType=='opDebtorsRevenue'){
    $sql = "SELECT b.`service_type` as rev_desc,SUM(total) AS Total FROM care_ke_billing b LEFT JOIN care_person p
                ON b.`pid`=p.`pid` WHERE `IP-OP`=2 AND (b.insurance_id <> '' or null) AND bill_date BETWEEN '$dt1' AND  '$dt2' AND b.`service_type`<>'Payment' GROUP BY b.`service_type`";
}
    //echo $sql;

    $request = $db->Execute($sql);
    $total=$request->RecordCount();
        echo '{
        "total":"' . $total . '","revenueList":[';
        $counter = 0;
        while ($row = $request->FetchRow()) {
            
            echo '{"Category":"' . $row[rev_desc] . '","Amount":"' . number_format($row[Total],2).'"}';
            
            if ($counter <> $total) {
                echo ",";
            }
            $counter++;
        }
        echo ']}';

}

function getItemsList(){
    global $db;
    $debug=false;

    $sql="select partcode,item_description,purchasing_class from care_tz_drugsandservices";
    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":'.$totalCount.',"itemslist":[';

    $counter=0;
    while($row=$results->FetchRow()){
        $description=preg_replace('/[^A-Za-z0-9\-]/', '', $row[item_description]);

        echo '{"PartCode":"'.$row[partcode].'","Description":"'.$description.'","Category":"'.$row[purchasing_class].'"}';

        $counter++;
        if ($counter <> $totalCount) {
            echo ",";
        }
    }

    echo "]}";

}

function getDrugStatement($drug,$startDate,$endDate,$start,$limit) {
    global $db;
    $debug=false;

    $searchParam=$_REQUEST[searchParam];

    $sql = "SELECT CONCAT(b.`order_date`,' ',b.`order_time`) AS OrderDate,b.`OP_no` as pid,p.encounter_nr,b.`patient_name`
            ,b.`item_id`,b.`Item_desc`,p.price,p.dosage,p.times_per_day,p.days,p.prescriber,b.`qty`,b.`orign_qty` as issued,
            b.balance,(b.`orign_qty`*b.price) as TotalCost,b.`input_user` as issuedBy
            FROM care_ke_internal_orders b LEFT JOIN care_encounter_prescription p on b.presc_nr=p.nr";
    if($startDate !== '' && $endDate !== ''){
        $sql = $sql." WHERE  b.order_date between '".$startDate."' and '".$endDate."'";
    }

    if($searchParam){
        $sql=$sql." AND b.`item_id`='$searchParam'";
    }else{
        $sql=$sql." AND b.`item_id`='$drug'";
    }

    $sql=$sql." ORDER BY b.`order_date` DESC limit $start,$limit";

    if($debug) echo $sql;


    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":'.$totalCount.',"drugstatement":[';

    $counter=0;
    while($row=$results->FetchRow()){
        echo '{"OrderDate":"'.$row[OrderDate].'","Pid":"'.$row[pid].'","EncounterNo":"'.$row[encounter_nr]
            .'","PatientName":"'.$row[patient_name].'","Price":"'.number_format($row[price],2).'","Dosage":"'.$row[dosage].'","Dosage":"'.$row[dosage]
            .'","TimesPerDay":"'.$row[times_per_day].'","Days":"'.$row[days].'","Dosage":"'.$row[dosage].'","TotalQty":"'.$row[qty]
            .'","Issued":"'.$row[issued].'","Balance":"'.$row[balance].'","TotalCost":"'.$row[TotalCost]
            .'","Prescriber":"'.$row[prescriber].'","Issuedby":"'.$row[issuedBy].'"}';

        $counter++;
        if ($counter <> $totalCount) {
            echo ",";
        }

    }

    echo "]}";
}

function getRevenueByPatients($drug) {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT CONCAT(b.`order_date`,' ',b.`order_time`) AS OrderDate,b.`OP_no` as pid,b.`patient_name`,b.`item_id`,
              b.`Item_desc`,b.`qty`,b.`orign_qty`,b.`input_user` FROM care_ke_internal_orders b
            WHERE b.`item_id`='$drug' ";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.order_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.order_date<=now()";
    }

    $sql = $sql . " ORDER BY b.Item_desc DESC";
    //echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    if ($request = $db->Execute($sql)) {

        echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">PID</td>
                    <td align="left">Patient Name</td>
                    <td align="left">OrderDate</td>
                    <td align="left">Item ID</td>
                    <td align="left">Description</td>
                    <td align="left">Qty Prescribed</td>
                    <td align="left">Qty Issued</td>
                    <td align="left">User</td>
                 </tr>';
        $bg = '';
//        $total='';
        while ($row = $request->FetchRow()) {
            if ($bg == "silver")
                $bg = "white";
            else
                $bg = "silver";
            echo "<tr bgcolor= $bg height=16>
                    <td><a href='#' onclick='getPatientStatement('".$row[pid]."')'>$row[pid]</a></td>
                    <td><a href='#' onclick='getPatientStatement('$row[pid]')'>$row[patient_name]</a></td>
                    <td> $row[OrderDate]</td>
                    <td>$row[item_id]</td>
                    <td align=left> $row[Item_desc]</td>
                    <td align=center>$row[qty] </td>
                    <td align=center>$row[orign_qty]</td>
                    <td align=left>$row[input_user]</td>
             </tr>";
            $lsum = $lsum + $row[orign_qty];
            $rowbg = 'white';
        }
        $rowCnt = $request->RecordCount();

        echo "<tr><td colspan=3><b>Total Patients issued were $rowCnt</b></td><td align=right><b>Total Quantity Issued " . number_format($lsum, 2) . "</b></td></tr>";
        echo '</table>';
    }else {
        echo 'SQl:Error=' . $sql;
    }
}

function getRevenueByItem() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT b.`proc_code` AS partcode,b.`Prec_desc` AS Description,b.`rev_desc` AS service_type,
b.`amount` AS price,SUM(b.`total`) AS Total, SUM(b.`proc_qty`) AS drug_Count
FROM care_ke_receipts b WHERE b.`rev_desc` = 'drug_list' ";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.currdate between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.currdate<=now()";
    }

    $sql = $sql . "GROUP BY b.proc_code,b.Prec_desc,b.rev_desc ORDER BY b.`total` DESC";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    if ($request = $db->Execute($sql)) {

        echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">Item No</td>
                    <td align="left">Description</td>
                    <td align="left">service_type</td>
                    <td align="left">Total drugs</td>
                    <td align="left">Unit Price</td>
                    <td align="left">Total Amount</td>
                 </tr>';
        $bg = '';
//        $total='';
        while ($row = $request->FetchRow()) {
            if ($bg == "silver")
                $bg = "white";
            else
                $bg = "silver";
            
            $partcode=$row[partcode];
            echo "<tr bgcolor= $bg height=16>
                    <td><a href='#' onclick='getDrugPatients(\"$partcode\")'>$partcode</a></td>
                    <td><a href='#' onclick='getDrugPatients(\"$partcode\")'>$row[Description]</a></td>
                    <td>$row[service_type]</td>
                    <td>$row[drug_Count]</td> 
                    <td align=right>".number_format($row[price], 2)."</td>   
                    <td align=right>".number_format($row[Total], 2)."</td>  
             </tr>";
            $lsum = $lsum + $row[Total];
            $rowbg = 'white';
        }
        $rowCnt = $request->RecordCount();

        echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
        echo '</table>';
    }else {
        echo 'SQl:Error=' . $sql;
    }
}


function getRevenueByItem2() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];


    $sql = "SELECT CONCAT(b.`order_date`,' ',b.`order_time`) AS OrderDate,b.`OP_no` as pid,p.encounter_nr,b.`patient_name`
            ,b.`item_id`,b.`Item_desc`,p.price,p.dosage,p.times_per_day,p.days,p.prescriber,b.`qty`,b.`issued` as issued,
            b.balance,(b.`issued`*b.price) as TotalCost,b.`input_user` as issuedBy,p.drug_class
            FROM care_ke_internal_orders b LEFT JOIN care_encounter_prescription p on b.presc_nr=p.nr
            WHERE p.drug_class in ('Drug_list','Medical Supplies')";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and order_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and order_date<=now()";
    }

    $sql = $sql . "GROUP BY item_id ORDER BY Total DESC";
    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":'.$totalCount.',"pharmacyrevenue":[';

    $counter=0;
    while($row=$results->FetchRow()){


        $sql2 = "SELECT price, issued,(issued * price) as TCost1 from care_ke_internal_orders where item_id = '".$row['item_id']."'";
        if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
            $date = new DateTime($date1);
            $dt1 = $date->format("Y-m-d");

            $date = new DateTime($date2);
            $dt2 = $date->format("Y-m-d");

            $sql2 = $sql2 . " and order_date between '$dt1' and '$dt2' ";
        } else {
            $sql2 = $sql2 . " and order_date<=now()";
        }

        $result2 = $db->Execute($sql2);
        $tcount = $result2->RecordCount();
        $TCost = '';
        $orign_qty = '';
        $Uprice = '';
        while ($row2 = $result2->FetchRow()) {
            $cost = round($row2['TCost1']/50) * 50;
            $TCost = $TCost + $cost;
            $orign_qty = $orign_qty + $row2['issued'];
            $Uprice = $row2['price'];
        }




        echo '{"ItemNo":"'.$row[item_id].'","Description":"'.trim($row[Item_desc]).'","Category":"'.$row[drug_class]
            .'","UnitPrice":"'.$Uprice.'","Quantities":"'.$orign_qty.'","TotalAmount":"'.$TCost.'"}';

        $counter++;
        if ($counter <> $totalCount) {
            echo ",";
        }

    }

    echo "]}";
}


function getRevenueByCat() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];




    $sql = "SELECT b.partcode,b.Description,b.service_type,b.price,SUM(b.total) AS Total, sum(b.qty) AS drug_Count
FROM care_ke_billing b  WHERE b.service_type = 'drug_list'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    $sql = $sql . "GROUP BY b.partcode ORDER BY Total DESC";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    if ($request = $db->Execute($sql)) {

        echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="left">Item No</td>
                    <td align="left">Description</td>
                    <td align="left">service_type</td>
                    <td align="left">Total drugs</td>
                    <td align="left">Unit Price</td>
                    <td align="left">Total Amount</td>
                 </tr>';
        $bg = '';
//        $total='';
        while ($row = $request->FetchRow()) {
            if ($bg == "silver")
                $bg = "white";
            else
                $bg = "silver";
            echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[partcode] . '</td>
                    <td>' . $row[Description] . '</td>
                    <td>' . $row[service_type] . '</td>
                    <td>' . $row[drug_Count] . '</td> 
                    <td align=right>' . number_format($row[price], 2) . '</td>   
                    <td align=right>' . number_format($row[Total], 2) . '</td>  
             </tr>';
            $lsum = $lsum + $row[Total];
            $rowbg = 'white';
        }
        $rowCnt = $request->RecordCount();

        echo "<tr><td colspan=3><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
        echo '</table>';
    }else {
        echo 'SQl:Error=' . $sql;
    }
}


function getRevenueByCat2() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];

    $sql = "SELECT b.partcode,b.Description,b.service_type,b.price,SUM(b.total) AS Total, sum(b.qty) AS drug_Count
            FROM care_ke_billing b  WHERE b.service_type = 'drug_list'";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.bill_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.bill_date<=now()";
    }

    $sql = $sql . "GROUP BY b.partcode ORDER BY Total DESC";
    if($debug) echo $sql;

    $results=$db->Execute($sql);
    $totalCount=$results->RecordCount();

    echo '{"totalCount":'.$totalCount.',"pharmcatrevenue":[';

    $counter=0;
    while($row=$results->FetchRow()){
        echo '{"ItemNo":"'.$row[partcode].'","Description":"'.trim($row[Description]).'","Category":"'.$row[service_type]
            .'","UnitPrice":"'.$row[price].'","Quantities":"'.$row[drug_Count].'","TotalAmount":"'.$row[Total].'"}';

        $counter++;
        if ($counter <> $totalCount) {
            echo ",";
        }

    }

    echo "]}";
}

function exportExcel() {
    global $db;
    $accno = $_REQUEST[acc1];


    $sql = "select p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,sum(b.total) as total 
            from care_person p left join care_ke_billing b
            on p.pid=b.pid where b.`IP-OP`=2 and p.insurance_ID='$accno' group by p.pid order by bill_date asc";

    $request = $db->Execute($sql);

    echo '<table width=100% height=14><tr bgcolor=#6699cc>
                    <td align="center">pid</td>
                    <td align="center">Names</td>
                    <td align="center">Bill Date</td>
                    <td align="center">Bill Number</td>
                    <td align="center">Total</td>
                    <td align="center">Running Total</td>
                 </tr>';
    $bg = '';
//        $total='';
    while ($row = $request->FetchRow()) {
        if ($bg == "silver")
            $bg = "white";
        else
            $bg = "silver";
        $total = intval($row[total] + $total);
        echo '<tr bgcolor=' . $bg . ' height=16>
                    <td>' . $row[pid] . '</td>
                    <td>' . $row[name_first] . ' ' . $row[name_last] . ' ' . $row[name_2] . '</td>
                    <td>' . $row[bill_date] . '</td>
                    <td>' . $row[bill_number] . '</td>
                    <td>' . number_format($row[total], 2) . '</td>
                    <td>' . number_format($total, 2) . '</td>    
                   
             </tr>';

        $rowbg = 'white';
    }
    echo '</table>';
}

function getPatientStatement() {
    global $db;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $pid = $_REQUEST[pid];

    echo $date1 ." ".$date2;

    $sql = "SELECT DISTINCT CONCAT(b.`order_date`,' ',b.`order_time`) AS OrderDate,b.`OP_no` AS pid,e.encounter_class_nr,b.`patient_name`,b.`item_id`,
        b.`Item_desc`,b.`qty`,b.unit_cost,b.`orign_qty`,b.`total`,b.`input_user` FROM care_ke_internal_orders b
        left join care_encounter e on b.`OP_no`=e.pid
        WHERE pid='$pid' ";

      if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.order_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.order_date<=now()";
    }

    $sql = $sql . " order by b.order_date desc";
//    echo $sql;
    //p.pid,p.name_first,p.name_last,p.name_2,b.bill_date,b.bill_number,b.total

    if ($request = $db->Execute($sql)) {

        $row1 = $request->FetchRow();
        echo '<table width=100% height=14>
        <tr><td colspan=6><br></td></tr>     
        <tr><td align="left"><b>PID:</b></td> <td>' . $row1[pid] . '</td></tr>
        <tr><td align="left"><b>Names:</b></td><td>' . $row1[patient_name] . '</td></tr>
        <tr bgcolor=#6699cc>
                    <td align="left">Date</td>
                    <td align="left">Admission</td>
                    <td align="left">Item Number</td>
                    <td align="left">Description</td>
                    <td align="left">Qty Issued</td>
                    <td align="left">Price</td>
                    <td align="left">Total</td>
                    <td align="left">running</td>
                 </tr>';
        $bg = '';
//        $total='';
        $lsum = 0;
        while ($row = $request->FetchRow()) {
            if ($bg == "silver")
                $bg = "white";
            else
                $bg = "silver";
            if ($row[`encounter_class_nr`] == 1) {
                $enc_class = 'IP';
            } else {
                $enc_class = 'OP';
            }
            $lsum = intval($lsum + $row[total]);
            echo '<tr bgcolor=' . $bg . ' height=16>
                
                    <td>' . $row[OrderDate] . '</td>
                    <td>' . $enc_class . '</td>    
                    <td>' . $row[item_id] . '</td>
                    <td>' . $row[Item_desc] . '</td>
                    <td>' . $row[orign_qty] . '</td>
                     <td>' . $row[unit_cost] . '</td>
                    <td align=right>' . number_format($row[total], 2) . '</td>
                     <td align=right>' . number_format($lsum, 2) . '</td>
             </tr>';

            $rowbg = 'white';
        }
        $rowCnt = $request->RecordCount();

        echo "<tr><td colspan=6><br>No of Tests $rowCnt</td><td align=right><b>Total Amount " . number_format($lsum, 2) . "<b></td></tr>";
        echo '</table>';
    } else {
        echo 'SQL: Failed=' . $sql;
    }
}

function getPatientStatement2() {
    global $db;
    $debug=false;

    $date1 = $_REQUEST[date1];
    $date2 = $_REQUEST[date2];
    $pid = $_REQUEST[pid];

   // echo $date1 ." ".$date2;

    $sql = "SELECT b.`order_date`,b.`order_time`,b.`OP_no` AS pid,e.encounter_class_nr,b.`patient_name`,b.`item_id`,
        b.`item_desc`,b.`qty`,b.price,b.`issued`,b.`total`,b.`input_user` FROM care_ke_internal_orders b
        LEFT JOIN `care_encounter_prescription` p ON b.`presc_nr`=p.`nr`
        LEFT JOIN care_encounter e ON p.`encounter_nr`=e.`encounter_nr` AND b.`order_date`=e.`encounter_date`
        WHERE  b.`OP_no`='$pid' ";

    if (isset($date1) && isset($date2) && $date1 <> "" && $date1 <> "") {
        $date = new DateTime($date1);
        $dt1 = $date->format("Y-m-d");

        $date = new DateTime($date2);
        $dt2 = $date->format("Y-m-d");

        $sql = $sql . " and b.order_date between '$dt1' and '$dt2' ";
    } else {
        $sql = $sql . " and b.order_date<=now()";
    }

    $sql = $sql . " order by b.order_date desc";
   if($debug) echo $sql;


    if ($request = $db->Execute($sql)) {
        $rowCnt=$request->RecordCount();

        echo '{ "total":"' . $rowCnt . '","drugstatement":[';

        $counter=0;
        $runningTotal=0;
        while ($row = $request->FetchRow()) {

            // $rtotal = round($row[total] / 50) * 50;
            $rtotal = round($row['total']/50) * 50;
            $rprice = round($row['price']/50) * 50;
            if($row[encounter_class_nr]=2){
                $encounter='OP';
            }else{
                $encounter='IP';
            }
            $runningTotal=$runningTotal+ $rtotal;
            echo '{"PID":"' . $row[pid] . '","PrescriptionDate":"' . $row[order_date] . '","PrescriptionTime":"' .  $row[order_time]
                . '","Admission":"' .  $encounter . '","PartCode":"' .  $row[item_id]. '","Description":"' .  $row[item_desc]
                . '","QtyIssued":"' .  $row[issued]. '","Price":"' .  $rprice . '","Total":"' .  $rtotal
                . '","RunningTotal":"' . $runningTotal.'"}';

            if ($counter <> $rowCnt) {
                echo ",";
            }
            
            $counter++;
        }

        echo "]}";

    } else {
        echo 'SQL: Failed=' . $sql;
    }
}



?>
