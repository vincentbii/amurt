
<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$limit = $_POST[limit];
$start = $_POST[start];
$item_number = $_POST[item_number];

$dt1=new DateTime(date($_REQUEST[date1]));
$date1=$dt1->format('Y-m-d');

$dt2=new DateTime(date($_REQUEST[date2]));
$date2=$dt2->format('Y-m-d');

//getIPMorbidity(20, 1);
$task = ($_POST['task']) ? ($_POST['task']) : $_REQUEST['task'];
switch ($task) {
    case "mobidity":
        getMorbidity($limit, $start);
        break;
    case "ipmorbidity":
        getIPMorbidity($limit, $start);
        break;
    case "ipReports":
        getIPreports();
        break;
    case "opVisits":
        getOPvisits($date1,$date2);
        break;
    case "create":
        createItem($item_number);
        break;
    case "adjustStock":
        stockAdjust($item_number);
        break;
    case "deleteItem":
        deleteItem();
        break;
    default:
        echo "{failure:true}";
        break;
}//end switch

function getMorbidity($limit, $start) {
    global $db;
    echo '{
   "mobidity":[';
    for ($i = 1; $i <= 31; $i++) {
//    $sql1 = 'select ICD_10_code as rCode,ICD_10_description as Disease,day(`timestamp`) as rDay,
//        count(icd_10_code) as rCount from care_tz_diagnosis where day(`timestamp`)="'.$i.'" group by ICD_10_code ';
        $sql1 = 'select convert(b.diagnosis_code,DECIMAL) as rCode,b.description as Disease
        from care_icd10_en b left join care_tz_diagnosis c
            on b.diagnosis_code=c.ICD_10_code where b.type="OP"  group by b.diagnosis_code order by rCode asc';
        $result1 = $db->Execute($sql1);

        while ($row = $result1->FetchRow()) {

            $sql2 = 'select  c.ICD_10_CODE,count(c.ICD_10_code) as rCount,c.ICD_10_description as Disease from care_tz_diagnosis c
        where day(c.timestamp)= "' . $i . '" and c.ICD_10_CODE="' . $row[0] . '" order by c.ICD_10_CODE asc';
            $result2 = $db->Execute($sql2);
            $row2 = $result2->FetchRow();
            $counter = 0;
//       echo $sql2;
            echo '{"rCode":"' . $row[0] . '","Disease":"' . $row[1] . '",
       "rDay":"' . $i . '","rCount":"' . $row2[1] . '"},';

            if ($counter < $numRows) {
                echo ",";
            }
            $counter++;
        }
    }
    echo ']}';
}

function doCount($icdcode, $min, $max, $sign) {
    global $db;
    if ($sign == "<") {
        $sql = 'select count(b.ICD_10_code) as rcount from care_tz_diagnosis b inner join care_icd10_en k
        on k.diagnosis_code=b.ICD_10_code
            inner join care_person c on c.pid=b.PID  where k.type<>"OP" and (year(now())-year(c.date_birth))<1 and b.ICD_10_code="' . $icdcode . '"';
    } elseif ($sign == 'Between') {
        $sql = 'select count(b.ICD_10_code) as rcount from care_tz_diagnosis b inner join care_icd10_en k
        on k.diagnosis_code=b.ICD_10_code
            inner join care_person c on c.pid=b.PID  where k.type<>"OP" and (year(now())-year(c.date_birth))
            BETWEEN "' . $min . '" and "' . $max . '" and b.ICD_10_code="' . $icdcode . '"';
        
    } else {
        $sql = 'select count(b.ICD_10_code) as rcount from care_tz_diagnosis b inner join care_icd10_en k
        on k.diagnosis_code=b.ICD_10_code
            inner join care_person c on c.pid=b.PID  where k.type<>"OP" and
            (year(now())-year(c.date_birth))>65 and b.ICD_10_code="' . $icdcode . '"';
    }
//    echo $sql;
    $result = $db->Execute($sql);
    $row = $result->FetchRow();
    return $row[0];
}

function getIPMorbidity($limit,$start) {
    global $db;
    $sql = 'select k.diagnosis_code,k.description from care_tz_diagnosis b inner join care_icd10_en k
            on k.diagnosis_code=b.ICD_10_code where k.type<>"OP" group by k.diagnosis_code';
    $result1 = $db->Execute($sql);
    
    $data1='';
    $count=0;
    while ($row1 = $result1->FetchRow()) {
        $data1[1][] = $row1[0];
        $data1[2][] = $row1[1];
        $count=$count+1;
    }
    
    
        $sql1 = "select b.PID,k.ICD_10_code,(year(now())-year(b.date_birth)) as age,k.ICD_10_description as descs,k.pataintStatus
            from care_tz_diagnosis k inner join
            care_person b on k.pid=b.pid  inner join care_icd10_en e on e.diagnosis_code=k.ICD_10_code
            where e.`type`<>'OP' order by k.ICD_10_code asc";
//        echo $sql1;
        $result = $db->Execute($sql1);
        $count2=0;
         $data='';
        while ($row = $result->FetchRow()) {
            $data[1][] = $row[1]; //ICD-code
            $data[2][] = $row[2]; //age
            $data[3][] = $row[4]; //patient status
            $data[4][] = $row[3];//ICD description
            $count2=$count2+1;
        }
     
         echo '{
            "Total":'.$count.',"ipmorbidity":[';
//                $A1sum=0;
//                $D1sum=0;$A2sum=0;$D2sum=0;$A3sum=0;$D3sum=0;
//                $A4sum=0;$D4sum=0;$A5sum=0;$D5sum=0;$A6sum=0;$D6sum=0;
//                $A7sum=0;$D7sum=0;$A8sum=0;$D8sum=0;
//                $A9sum=0;
//                $D9sum=0;
//               

         for($i=0;$i<$count;$i++){
             echo '{"icdcode":"' . $data1[1][$i]. '","desc":"' . trim($data1[4][$i]). '",';
             $counter=0;
             $A1sum=0;
               
//             for($i=0;$i<$j;$i++){
//                if ($data[2][$i] < 1 && $data[3][$i]='A' ) {$A1sum = getIpmobidityCount(1,1,'A',$data1[1][$i],'<');}else{$A1sum=0;} 
//                if ($data[2][$i] < 1 && $data[3][$i]='D' ) {$D1sum = getIpmobidityCount(1,1,'D',$data1[1][$i],'<');}else{$D1sum=0;}
////                if ($data[2][$i] >= 1 && $data[2][$i] <= 4 && $data[3][$i]='A' ) {$A2sum = $A2sum+1;}else{$A2sum=0;} 
////                if ($data[2][$i] >= 1 && $data[2][$i] <= 4 && $data[3][$i]='D' ) {$D2sum = $D2sum+1;}else{$D2sum=0;}
//                
//                if ($data[2][$i] >= 1 && $data[2][$i] <= 4 && $data[3][$i]='A' ) {$A2sum =getIpmobidityCount(1,4,'A',$data1[1][$i],'between');}else{$A2sum=0;} 
//                if ($data[2][$i] >= 1 && $data[2][$i] <= 4 && $data[3][$i]='D' ) {$D2sum = getIpmobidityCount(1,4,'D',$data1[1][$i],'between');}else{$D2sum=0;}
                if ($data[2][$i] >= 5 && $data[2][$i] <= 14 && $data[3][$i]='A' ) {$A3sum = getIpmobidityCount(5,14,'A',$data1[1][$i],'between');}else{$A3sum=0;} 
                if ($data[2][$i] >= 5 && $data[2][$i] <= 14 && $data[3][$i]='D' ) {$D3sum = getIpmobidityCount(5,14,'D',$data1[1][$i],'between');}else{$D3sum=0;}
                if ($data[2][$i] >= 15 && $data[2][$i] <= 24 && $data[3][$i]='A' ) {$A4sum =getIpmobidityCount(15,24,'A',$data1[1][$i],'between');}else{$A4sum=0;}
                if ($data[2][$i] >= 15 && $data[2][$i] <= 24 && $data[3][$i]='D' ) {$D4sum = getIpmobidityCount(15,24,'D',$data1[1][$i],'between');}else{$D4sum=0;}
                if ($data[2][$i] >= 25 && $data[2][$i] <= 34 && $data[3][$i]='A' ) {$A5sum = getIpmobidityCount(25,34,'A',$data1[1][$i],'between');}else{$A5sum=0;}
                if ($data[2][$i] >= 25 && $data[2][$i] <= 34 && $data[3][$i]='D' ) {$D5sum = getIpmobidityCount(25,34,'D',$data1[1][$i],'between');}else{$D5sum=0;}
                if ($data[2][$i] >= 35 && $data[2][$i] <= 44 && $data[3][$i]='A' ) {$A6sum = getIpmobidityCount(35,44,'A',$data1[1][$i],'between');}else{$A6sum=0;}
                if ($data[2][$i] >= 35 && $data[2][$i] <= 44 && $data[3][$i]='D' ) {$D6sum = getIpmobidityCount(35,44,'D',$data1[1][$i],'between');}else{$D6sum=0;}
                if ($data[2][$i] >= 45 && $data[2][$i] <= 54 && $data[3][$i]='A' ) {$A7sum = getIpmobidityCount(45,54,'A',$data1[1][$i],'between');}else{$A7sum=0;} 
                if ($data[2][$i] >= 45 && $data[2][$i] <= 54 && $data[3][$i]='D' ) {$D7sum = getIpmobidityCount(45,54,'D',$data1[1][$i],'between');}else{$D7sum=0;} 
                if ($data[2][$i] >= 55 && $data[2][$i] <= 64 && $data[3][$i]='A' ) {$A8sum = getIpmobidityCount(55,64,'A',$data1[1][$i],'between');}else{$A8sum=0;} 
                if ($data[2][$i] >= 55 && $data[2][$i] <= 64 && $data[3][$i]='D' ) {$D8sum = getIpmobidityCount(55,64,'A',$data1[1][$i],'between');}else{$D8sum=0;} 
                if ($data[2][$i] > 65 && $data[3][$i]='A' ) {$A9sum = getIpmobidityCount(65,65,'A',$data1[1][$i],'>');}else{$A9sum=0;} 
                if ($data[2][$i] > 65 && $data[3][$i]='D' ) {$D9sum = getIpmobidityCount(65,65,'D',$data1[1][$i],'>');}else{$D9sum=0;}  
                

//             }
              echo '"A1":"' . $A1sum . '","D1":"' . $D1sum . '","A2":"' . $A2sum . '","D2":"' . $D2sum . '",
                    "A3":"' . $A3sum . '","D3":"' . $D3sum . '","A4":"' . $A4sum . '","D4":"' . $D4sum . '",
                    "A5":"' . $A5sum . '","D5":"' . $D5sum . '","A6":"' . $A6sum . '","D6":"' . $D6sum . '",
                    "A7":"' . $A7sum . '","D7":"' . $D7sum . '","A8":"' . $A8sum . '","D8":"' . $D8sum . '",
                    "A9":"' . $A9sum . '","D9":"' . $D9sum .'"}';
             
              
             if($j<$count){
                 echo ',';
             }
//             $counter++;
            
              
             
    }
        

    echo ']}';
}

function getIpmobidityCount($age1,$age2,$patientstatus,$icdCode,$sign){
    global $db;
    $debug=false;
    $sql="select count(b.PID) as pcount from care_tz_diagnosis k inner join
            care_person b on k.pid=b.pid  inner join care_icd10_en e on e.diagnosis_code=k.ICD_10_code
            where e.`type`<>'OP' AND k.ICD_10_code='$icdCode' 
            and k.pataintStatus='$patientstatus'";
if($sign=='between'){
    $sql=$sql."and (year(now())-year(b.date_birth)) between $age1 and $age2";
}else if ($sign=='>'){
     $sql=$sql."and (year(now())-year(b.date_birth)) > $age1";
}else if($sign=='<') {
     $sql=$sql."and (year(now())-year(b.date_birth)) < $age1";
}       

    
    if($debug) echo $sql;
    
    $result=$db->Execute($sql);
    $row=$result->FetchRow();
    
    return $row[0];
}

function getOPvalues($age, $sex, $clinic, $sign, $encDate1,$encDate2,$sign2) {
    global $db;
    $debug = false;
  if($sign2=="="){
        
         $sql="select count(e.encounter_nr) as encCounter,e.pid from care_encounter e 
            left join care_person p on e.pid=p.pid
            where e.current_dept_nr='$clinic' and encounter_date between '$encDate1' and '$encDate2'";
         
            if($age){
                    $sql=$sql." AND (YEAR(NOW())-YEAR(p.date_birth))$sign $age";
            }
            
            
            if($sex){
                    $sql=$sql." and p.sex='$sex'";
            }

            $sql=$sql." and e.encounter_class_nr=2 group by pid";
            if($debug) 
                echo $sql;
            $result = $db->Execute($sql);
//            $row=$result->FetchRow();
            $counter=0;
              while($row=$result->FetchRow()){
               $sql3="select count(e.pid) as prevPid from care_encounter e 
                        where encounter_date<'$encDate2'
                        and e.pid=$row[1]";
               if($debug) echo $sql3;
                    $result3 = $db->Execute($sql3);
                   $row3=$result3->FetchRow();
                      if($row3[0]==1){
                        $counter=$counter+1;
                    }
            }
             $pcount=$counter;
  }else{
      $sql="select e.pid,count(e.encounter_nr) as encCounter,e.pid from care_encounter e  
            left join care_person p on e.pid=p.pid
            where e.current_dept_nr='$clinic' and encounter_date between '$encDate1' and '$encDate2'";
      
            if($age){
                    $sql=$sql." AND (YEAR(NOW())-YEAR(p.date_birth))$sign $age";
            }
            if($sex){
                    $sql=$sql." and p.sex='$sex'";
            }

            $sql=$sql." and e.encounter_class_nr=2 group by pid";
             if($debug) 
                 echo $sql;
            $result = $db->Execute($sql);
//            $row=$result->FetchRow();
            $counter=0;
            while($row=$result->FetchRow()){
               $sql3="select e.pid from care_encounter e 
                        where encounter_date<'$encDate2'
                        and e.pid=$row[0]";
               if($debug) echo $sql3;
                    $result3 = $db->Execute($sql3);
                   $row3=$result3->FetchRow();
                      if($row3[0]>1){
                        $counter=$counter+1;
                     }
            }
             $pcount=$counter;
  }

        return $pcount;
    
}

function getOPvisits($date1,$date2) {
    global $db;

    $sql = 'SELECT parent,opCode,description FROM care_ke_opvisitsvars where parent like "A%"';
    $result = $db->Execute($sql);
    $data='';
    while ($row = $result->FetchRow()) {
        $data[1][] = $row[1];
        $data[2][] = $row[2];
    }

    $sql = 'SELECT parent,opCode,description FROM care_ke_opvisitsvars where parent like "A%"';
    $result = $db->Execute($sql);
    echo '{"opVisits":[';
    $data[][]='';
    while ($row = $result->FetchRow()) {
        if ($data[1][1] == $row[1]) {
            $newVar = getOPvalues("5", "m", "40", ">",  $date1,$date2,'='); //Over 5-Male
        } elseif ($data[1][2] == $row[1]) {
            $newVar = getOPvalues("5", "f", "40", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("5", "f", "40", ">",  $date1,$date2,">");
        } elseif ($data[1][3] == $row[1]) {
            $newVar = getOPvalues("5", "m", "40", "<=",  $date1,$date2,'=');
            $retVar = getOPvalues("5", "m", "40", "<=",  $date1,$date2,">");
        } elseif ($data[1][4] == $row[1]) {
            $newVar = getOPvalues("5", "f", "40", "<=",  $date1,$date2,'=');
            $retVar = getOPvalues("5", "f", "40", "<=",  $date1,$date2,">");
        } elseif ($data[1][5] == $row[1]) {
            $newVar = getOPvalues("0", "", "40", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "40", ">",  $date1,$date2,">");
        } elseif ($data[1][6] == $row[1]) {
            $newVar = getOPvalues("0", "", "52", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "52", ">",  $date1,$date2,">");
        } elseif ($data[1][7] == $row[1]) {
            $newVar = getOPvalues("0", "", "6", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "6", ">",  $date1,$date2,">");
        }elseif ($data[1][8] == $row[1]) {
            $newVar = getOPvalues("0", "", "7", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "7", ">",  $date1,$date2,">");
        } elseif ($data[1][9] == $row[1]) {
            $newVar = getOPvalues("0", "", "47", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "47", ">",  $date1,$date2,">");
        } elseif ($data[1][10] == $row[1]) {
            $newVar = getOPvalues("0", "", "53", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "53", ">",  $date1,$date2,">");
        } elseif ($data[1][11] == $row[1]) {
            $newVar = getOPvalues("0", "", "54", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "54", ">",  $date1,$date2,">");
        } elseif ($data[1][12] == $row[1]) {
            $newVar = getOPvalues("0", "", "44", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "44", ">",  $date1,$date2,">");
        }elseif ($data[1][13] == $row[1]) {
            $newVar = getOPvalues("0", "", "57", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "57", ">",  $date1,$date2,">");
        } elseif ($data[1][14] == $row[1]) {
            $newVar = getOPvalues("0", "", "56", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "56", ">",  $date1,$date2,">");
        } elseif ($data[1][15] == $row[1]) {
            $newVar = getOPvalues("0", "", "62", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "62", ">",  $date1,$date2,">");
        } elseif ($data[1][16] == $row[1]) {
            $newVar = getOPvalues("0", "", "48", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "48", ">",  $date1,$date2,">");
        } elseif ($data[1][17] == $row[1]) {
            $newVar = getOPvalues("0", "", "55", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "55", ">",  $date1,$date2,">");
        } elseif ($data[1][18] == $row[1]) {
            $newVar = getOPvalues("0", "", "49", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "49", ">",  $date1,$date2,">");
        } elseif ($data[1][19] == $row[1]) {
            $newVar = getOPvalues("0", "", "43", ">",  $date1,$date2,'=');
            $retVar = getOPvalues("0", "", "43", ">",  $date1,$date2,">");
        }  else{
            $newVar = 0;
            $retVar = 0;
        }
     
        echo '{"parent":"' . $row[0] . '","opCode":"' . $row[1] . '",
                "Description":"' . $row[2] . '","New":"' . $newVar . '","Ret":"' . $retVar . '","Total":"' . intval($newVar + $retVar) . '"},';
    }

    echo ']}';
}

function getIPvalues($age,$sign, $sign2, $service,$ward) {
    global $db;
    $debug = false;

    $sql = 'SELECT COUNT(a.encounter_nr) AS pcount FROM care_encounter a 
            LEFT JOIN care_person b ON a.pid=b.pid
            LEFT JOIN care_encounter_location c ON a.encounter_nr=c.encounter_nr
            LEFT JOIN care_type_discharge d ON c.discharge_type_nr=d.nr
            WHERE a.encounter_class_nr=1 AND a.is_discharged=1 
            AND (YEAR(NOW())-YEAR(b.date_birth))' . $sign . '"' . $age . '" AND 
            c.discharge_type_nr'.$sign2.'"'.$service.'" AND a.current_ward_nr in ('.$ward.')';

            if($debug) echo $sql;
    $result = $db->Execute($sql);
    if ($debug)
        echo $sql;
    if ($row = $result->FetchRow()) {
        return $row[0];
    } else {
        return false;
    }
}

function getIPreports() {
    global $db;

    $sql = 'SELECT parent,opCode,description FROM care_ke_opvisitsvars where parent in("B1","B2")';
    $result = $db->Execute($sql);
    while ($row = $result->FetchRow()) {
        $data[1][] = $row[1];
        $data[2][] = $row[2];
    }


    $sql = 'SELECT parent,opCode,description FROM care_ke_opvisitsvars where parent in("B1","B2")';
    $result = $db->Execute($sql);
    echo '{"ipReports":[';
    while ($row = $result->FetchRow()) {
        for($i=1;$i<14;$i++){
            if ($data[1][$i] == $row[1]) {
//                getIPvalues($age,$sign, $sign2, $service)
                $newVar = getIPvalues("5", ">", "=", "1","1,2,3,4,6");
                $retVar = getIPvalues("5", "<", "=", "1","1,2,3,4,6");
                $matVar = getIPvalues("5", ">", "=", "1","5");
                $amVar =  getIPvalues("5", ">", "=", "1","1,2,3,4,6");
            } else{
                $newVar = 0;
                $retVar = 0;
            }
        }
          $fDesc = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $row[2]);
          
        echo '{"parent":"' . $row[0] . '","opCode":"' . $row[1] . '",
                "Description":"' . $fDesc . '","GenAdults":"' . $newVar . '","GenPaed":"' . $retVar . '",
                    "Martmoms":"' . $matVar. '","Amenity":"' . $amVar . '","Total":"' . intval($newVar + $retVar +$matVar+ $amVar). '"},';
    }

    echo ']}';
}

?>

