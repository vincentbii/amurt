            Ext.BLANK_IMAGE_URL="../../include/Extjs/resources/images/default/s.gif";
            Ext.onReady(function(){

var getMWard = new Ext.data.JsonStore({
    storeId: 'getWard',
    url: 'getWardinfo.php', //url to data object (server side script)
    method: 'POST'
    ,
    baseParams:{
        ward:'Male'
    },
    //    url: 'getPayments.php',p.`Pid`, p.`emp_names`,p.`pay_type`, p.`amount`,e.bank_name,e.account_no
    root: 'Wards',
    fields:[
    {
        name: 'Ward',
        type: 'string'
    },
    {
        name: 'beds',
        type: 'string'
    },
    {
        name: 'occupancy',
        type: 'string'
    }
    ,
    {
        name:'vacant',
        type: 'string'
    }
    ,
    {
        name:'occupied',
        type: 'string'
    }
    ,
    {
        name:'Males',
        type: 'string'
    }
    ,
    {
        name:'females',
        type: 'string'
    }
    ]
});


var tpl2 = new Ext.XTemplate(
    '<table class="select_template" border=0 width="100%">',
    '<tr class="selectbeds"><td>Ward</td><td>Beds</td><td>occupancy</td><td>occupied</td><td>vacant</td><td>Males</td><td>females</td></tr>',
    '<tpl for=".">',
    '<tpl><tr class="select_beds">',
    '<td class="select_ward">{Ward}</td><td>{beds}</td><td>{occupancy}</td><td>{occupied}</td><td>{vacant}</td><td>{Males}</td><td>{females}</td></tr>',
    '</tpl>',
    '</tpl>',
    '</table>',
    '<div class="x-clear"></div>'
    );


var wPanel = new Ext.Panel({
    width: 465,
    region:'center',
    margins: '0 5 5 5',
    items: new Ext.DataView({
        store: getMWard,
        tpl: tpl2,
        autoHeight:true,
        multiSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.select_beds'
    })
//        renderTo: document.body
});
  

var wChart= new Ext.Panel({
    height: 300,
//    renderTo: document.body,
    region:'north',
    layout:'fit',
    margins:'5 5 0',
    split:true,
    minHeight:100,
    maxHeight:300,

    items: {
        xtype: 'columnchart',
        store: getMWard,
        yField: 'beds',
        url: '../../include/Extjs/resources/charts.swf',
        xField: 'Ward',
        xAxis: new Ext.chart.CategoryAxis({
            title: 'Wards'
        }),
        yAxis: new Ext.chart.NumericAxis({
            displayName:'Occupied Beds',
            labelRenderer:Ext.util.Format.numberRenderer('0,0')
        }),
        extraStyle: {
            xAxis: {
                labelRotation: -90
            }
        },
        series: [{
            type: 'column',
            displayName: 'No of Beds',
            yField: 'beds',
            labelRotation: -90,
            style: {
                image:'../../include/Extjs/chart/bar.gif',
                mode: 'stretch',
                color:0x99BBE8
            }
        },{
            type:'line',
            displayName: 'Occupied Beds',
            yField: 'occupied',
            style: {
                color: 0x15428B
            }
        }]
    }
});

var wardsChart = new Ext.Panel({

    layout: 'border',
    layoutConfig: {
        columns: 1
    },
    renderTo:'occupancy',
    width:600,
    height: 700,
    items: [wChart,wPanel],
    tbar:[{
        text: 'print form',
        handler :function(){
            Ext.ux.Printer.print(wardsChart);
        }

    }]
});


getMWard.load();

 });



//wardsPanel.getTabEl('wardsPanel_1').style.display = 'none';
// Ext.getCmp('wardsPanel_1').hide();

//var wPanel = new Ext.Panel({
//    title: 'Basic Template',
//    width: 300,
//    html: '<p><i>Apply the template to see results here</i></p>',
//    items: new Ext.DataView({
//        store: getWard,
//        tpl: tpl,
//        autoHeight:true,
//        multiSelect: true,
//        overClass:'x-view-over',
//        itemSelector:'div.select_template',
//        collectData : function(records, startIndex){
//            var r = [],
//            i = 0,
//            len = records.length;
//            for(; i < len; i++){
//                r[r.length] = this.prepareData(records[i].data, startIndex + i, records[i]);
//                wardsPanel.add({
//                    title:records[i].get('ward_name'),
//                    iconCls: 'tabs',
//                    closable:true,
//                    html: r[r.length]
//                });
//
//
//            } //            return r;
//        }
//    })

//        renderTo: document.body
//});
