/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 Ext.onReady(function(){
    var mStore=new Ext.data.JsonStore({
        url: 'getReports.php',
        root: 'mobidity',
        id: 'mobidity',//
        baseParams:{
            task: "mobidity"
        },//This
        fields:['mobidity',{name:'rCode',type:'int'},{name:'rDay',type:'int'},{name:'rCount', type:'int'}]
    });

    mStore.load()

    var pivotGrid=new Ext.grid.PivotGrid({
        title:'Daily OP morbidity',
//        id:'pivotGrid',
        width:1200,
        height:600,
        renderTo:'diseases',
        store:mStore,
        aggregator:'sum',
        measure:'rCount',
        leftAxis:[{
            width:20,
            dataIndex:'rCode',
            direction:'ASC'
        },{
            width:180,
            dataIndex:'Disease'
        }],
        topAxis:[{
            dataIndex:'rDay'
        }],
      tbar:[
          {
              text:'Print table',
              iconCls:'print',
              handler:function(){
                  Ext.ux.Printer.print(pivotGrid)
              }
          },{xtype: 'exportbutton',
            store: mStore}
      ]

    });
 });

