/*!
 * Ext JS Library 3.3.0
 * Copyright(c) 2006-2010 Ext JS, Inc.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */


pharmReports = function() {


    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.BLANK_IMAGE_URL="../../include/Extjs/resources/images/default/s.gif";
    return {
        init: function() {
            Ext.QuickTips.init();


            /* Datepicker */
            var datefield = new Ext.form.DateField({
                renderTo: 'datefield',
                labelWidth: 100, // label settings here cascade unless overridden
                frame: false,
                width: 180,
                name: 'strDate1',
                id:'strDate1',
                listeners: {
                    change : function( datepicker ) {
                       Ext.Msg.alert('Test',this.getValue())
                    }
                }
            });

            var datefield2 = new Ext.form.DateField({
                renderTo: 'datefield2',
                frame: false,
                width: 180,
                name: 'strDate2',
                id:'strDate2'
            });

            Ext.define("Post", {
                extend: 'Ext.data.Model',
                proxy: {
                    type: 'jsonp',
                    url : 'getReportsData.php?task=getLabTests',
                    reader: {
                        type: 'json',
                        root: 'laboratory',
                        totalProperty: 'totalCount'
                    }
                },

                fields: [
                    {name: 'ItemId', mapping: 'ItemId'},
                    {name: 'Description', mapping: 'Description'}
                ]
            });

            ds = new Ext.data.Store, ({
                pageSize: 10,
                model: 'Post',
                autoLoad:true
            });


            var LabTests= new Ext.form.ComboBox({
                //fieldLabel: 'Laboratory Tests',
                store: ds,
                queryMode: 'local',
                typeAhead: true,
                minChars: 2,
                displayField: 'Description',
                valueField: 'ItemId',
                renderTo:'LabTests'
            });

            //Ext.Msg.alert('Test','Test Tes t');
        }
    };

}();
Ext.onReady(pharmReports.init, pharmReports);