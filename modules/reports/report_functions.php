<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function getDebtorCat() {
    global $db;
    $sql = 'Select `code`,`name` from care_ke_debtorcat order by `code` asc';
    $request = $db->execute($sql);
    return $request;
}

function getDept() {
    global $db;
    $sql = 'Select st_id,st_name from care_ke_stlocation order by st_name asc';
    $request = $db->execute($sql);
    return $request;
}

function getWards() {
    global $db;
    $sql = 'Select nr,name from care_ward';
    $request = $db->execute($sql);
    return $request;
}
function getDiagnosis() {
    global $db;
    ?>
    <table class="tbl1" border=0 width="80%">

        <tr>
            <td>From Date:</td>
            <td id="datefield"></td>
            <td>To ICD No: </td>
            <td><input type="text" id="icd1" name="icd1" value="" ondblclick="getICD10()"></td>
        </tr>
        <tr>
            <td>To Date:</td>
            <td id="datefield2"></td>
            <td>To ICD No: </td>
            <td><input type="text" id="icd2" name="icd2" value=""></td>
        </tr>
        <tr>
            <td>From Age:</td>
            <td><input type="text" id="age1" name="age1" value=""></td>
            <td>Gender: </td>
            <td><select id="gender">
                    <option></option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select></td>
        </tr>  
        <tr>
            <td>To Age:</td>
            <td><input type="text" id="age2" name="age2" value=""></td>
            <td>Visit: </td>
            <td><select id="visits">
                    <option></option>
                    <option value="1">First Visit</option>
                    <option value="2">Revisit</option>
                </select>
            </td>
        </tr><tr>
            <td>PID:</td>
            <td><input type="text" id="pid" name="pid" value=""></td>
            <td></td>
            <td></td>
        </tr>
    </tr>
    <td><button id="preview" onclick="getDiagnosis()">Preview Report</button></td>
    <td><button id="export" onclick="exportDiagnosisExcel()">Export to Excel</button></td> 
    <td><button id="preview" onclick="printStatement()">Print Report</button></td>
    <td></td>
    </tr>
    </table>
    <?php
}


function getTopDiagnosis() {
    global $db;
    ?>
    <table class="tbl1" border=0 width="80%">

        <tr>
            <td>From Date:</td>
            <td id="datefield"></td>
            <td>To ICD No: </td>
            <td><input type="text" id="icd1" name="icd1" value="" ondblclick="getICD10()"></td>
        </tr>
        <tr>
            <td>To Date:</td>
            <td id="datefield2"></td>
            <td>To ICD No: </td>
            <td><input type="text" id="icd2" name="icd2" value=""></td>
        </tr>
       
    <td><button id="preview" onclick="getTopDiagnosis()">Preview Report</button></td>
    <td><button id="preview" onclick="exportDiagnosisExcel()">Export to Excel</button></td> 
    <td><button id="preview" onclick="printStatement(document.getElementById('acc1').value)">Print Report</button></td>
    <td></td>
    </tr>
    </table>
    <?php
}

function getAdmDis() {
    global $db;
    ?>
    <table class="tbl1" border=0 width="80%">

        <tr>
            <td>From Date:</td>
            <td id="datefield"></td>
            <td>Adm|Dis: </td>
            <td><select id='admdis'>
                    <option></option>
                    <option value='adm'>Admissions</option>
                    <option value='dis'>Discharges</option>
                </select></td>
        </tr>
        <tr>
            <td>To Date:</td>
            <td id="datefield2"></td>
            <td>Wards</td>
            <td><?php
            $result=getWards();
            echo '<select id="wards"><option></option>';
            
            while($row=$result->FetchRow()){
                echo "<option value='$row[0]'>$row[1]</option>";
            }
            echo "</select>";
            ?></td>
        </tr>
        <tr>
            <td>Gender:</td>
            <td>
                <select id="sex">
                    <option value=''></option>
                    <option value='M'>Male</option>
                    <option value='F'>Female</option>
                </select>
            </td>
            <td>Group Wards</td>
            <td><select id="grpWards">
                    <option value=''></option>
                    <option value='adults'>General Adults</option>
                    <option value='paeds'>General Paediatrics</option>
                    <option value='mat'>Maternity Mothers</option>

                </select></td>
        </tr>
        <tr>
            <td></td>
            <td> </td>
            <td>Discharge Types</td>
            <td><select id="dischargeTypes">
                    <option value=''></option>
                    <?php
                    $sql="SELECT nr,`name` from care_type_discharge";
                    $result=$db->Execute($sql);
                    while($row=$result->FetchRow()){
                        echo "<option value='$row[nr]'>$row[name]</option>";
                    }
                    ?>

                </select></td>
        </tr>
       
    <td><button id="preview" onclick="getAdmDis()">Preview Report</button></td>
    <td><button id="preview" onclick="exportAdmDis(document.getElementById('admdis').value)">Export to Excel</button></td> 
    <td><button id="preview" onclick="printStatement(document.getElementById('acc1').value)">Print Report</button></td>
    <td></td>
    </tr>
    </table>
    <?php
}


function getLab() {
    global $db;
    ?>
    <table class="tbl1" border=0 width="80%">

        <tr>
            <td>From Date:</td>
            <td id="datefield"></td>
            <td>Reports </td>
            <td><select id='lab'>
                    <option></option>
                    <option value='1'>Daily Activity</option>
                    <option value='2'>Lab Revenue</option>
                    <option value='3'>Patient Tests</option>
                    <option></option>
                </select></td>
        </tr>
        <tr>
            <td>To Date:</td>
            <td id="datefield2"></td>
            <td>PID:</td>
            <td><input type="text" id="pid" name="pid" value=""></td>
        </tr>
        <tr>
            <td>Age Between:</td>
            <td><input type="text" id="age1" name="age1" value=""  size="5"></td>
            <td>Filter By LabTest:</td>
            <td id="LabTests"></td>
        </tr>
        <tr>
            <td>Age2</td>
            <td><input type="text" id="age2" name="age2" value="" size="5"></td>
            <td>Results:</td>
            <td><input type="text" id="results" name="results" value=""  size="50"></td>
        </tr>
        <td><button id="preview" onclick="getLab()">Preview Report</button></td>
        <td><button id="preview" onclick="exportExcel(document.getElementById('acc1').value)">Export to Excel</button></td>
        <td><button id="preview" onclick="printStatement(document.getElementById('acc1').value)">Print Report</button></td>
        <td></td>
        </tr>
    </table>
<?php
}


function getXray() {
    global $db;
    ?>
    <table class="tbl1" border=0 width="80%">

        <tr>
            <td>From Date:</td>
            <td id="datefield"></td>
            <td>Reports </td>
            <td><select id='xray'>
                    <option></option>
                    <option value='1'>Daily Activity</option>
                    <option value='2'>Xray Revenue</option>
                    <option value='3'>Patient Tests</option>
                </select></td>
        </tr>
        <tr>
            <td>From Date:</td>
            <td id="datefield2"></td>
            <td>Pid</td>
            <td><input type="text" id="pid" name="pid" value=""></td>
        </tr>
       
    <td><button id="preview" onclick="getXray()">Preview Report</button></td>
    <td><button id="preview" onclick="exportExcel(document.getElementById('acc1').value)">Export to Excel</button></td> 
    <td><button id="preview" onclick="printStatement(document.getElementById('acc1').value)">Print Report</button></td>
    <td></td>
    </tr>
    </table>
    <?php
}

function getDrugs() {
    global $db;
    ?>
    <table class="tbl1" border=0 width="80%">

        <tr>
            <td>From Date:</td>
            <td id="datefield"></td>
            <td>Reports </td>
            <td><select id='drugsRpts'>
                    <option></option>
                    <option value='1'>Pharmacy Revenue by Category</option>
                    <option value='2'>Pharmacy Revenue by Top selling</option>
                    <option value='3'>Patient Drug Statement</option>
                </select></td>
        </tr>
        <tr>
            <td>To Date:</td>
            <td id="datefield2"></td>
            <td>PID:</td>
            <td><input type="text" id="pid" name="pid" value=""></td>
        </tr>
       
    <td><button id="preview" onclick="getDrugs()">Preview Report</button></td>
    <td><button id="preview" onclick="exportDrugsToExcel()">Export to Excel</button></td> 
    <td><button id="preview" onclick="printStatement()">Print Report</button></td>
    <td></td>
    </tr>
    </table>
    <?php
}
?>


<script>

function getPatientStatement(str){
    xmlhttp=GetXmlHttpObject();
    if (xmlhttp==null)
    {
        alert ("Browser does not support HTTP Request");
        return;
    }

    var date1= document.getElementById('strDate1').value
    var date2= document.getElementById('strDate2').value

    var url="getReportsData.php?pid="+str;
    url=url+"&sid="+Math.random();
    url=url+"&date1="+date1;
    url=url+"&date2="+date2;
    url=url+"&task=patientstatement";
    xmlhttp.onreadystatechange=getPatientStatement;
    xmlhttp.open("POST",url,true);
    xmlhttp.send(null);
}

function getPatientStatement()
{
    //get payment description
    if (xmlhttp.readyState==4)
    {
        //            var str=xmlhttp.responseText;
        //str2=str.search(/,/)+1;
        document.getElementById("myContent").innerHTML=xmlhttp.responseText;
    }
}

    function getDrugPatients(str){
       xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
        
        var url="getReportsData.php?partcode="+str;
        url=url+"&sid="+Math.random();
        url=url+"&date1="+date1;
        url=url+"&date2="+date2;
        url=url+"&task=drugsperpatient";
        xmlhttp.onreadystatechange=getPatientsList;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
  }
  
  function getPatientsList()
    {
        //get payment description
        if (xmlhttp.readyState==4)
        {
            //            var str=xmlhttp.responseText;
            //str2=str.search(/,/)+1;
            document.getElementById("myContent").innerHTML=xmlhttp.responseText;
        }
    }
    
    function getICD10(){
         dhxWins = new dhtmlXWindows();
        //dhxWins.enableAutoViewport(false);
        //dhxWins.attachViewportTo("winVP");
        dhxWins.setImagePath("../../include/dhtmlxWindows/codebase/imgs/");

        w1 = dhxWins.createWindow("w1", 400, 150, 450, 450);
        w1.setText("Dignosis List");

        grid = w1.attachGrid();
        grid.setImagePath("../../include/dhtmlxGrid/codebase/imgs/");
        grid.setHeader("diagnosis_code ,Description");
        grid.attachHeader("#connector_text_filter,#connector_text_filter");
        grid.setSkin("light");
        grid.setColTypes("ro,ro");
        grid.setInitWidths("80,200");
        grid.loadXML("icd_pop.php");
         grid.attachEvent("onRowSelect",doOnRowSelected);
//        grid.attachEvent("onEnter",doOnEnter2);
        grid.enableDragAndDrop(true);

        //grid.attachEvent("onRightClick",doonRightClick);
        grid.init();
        grid.enableSmartRendering(true);
    }
    
     function doOnRowSelected(id,ind){

        var icdCode=grid.cells(id,0).getValue();
      document.getElementById('icd1').value=icdCode
      document.getElementById('icd2').value=icdCode

    }
    
    function getLevels(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        
        var url="getReportsData.php?catID="+str;
        url=url+"&sid="+Math.random();
        url=url+"&task=getLevels";
        xmlhttp.onreadystatechange=stateChanged;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }

    function stateChanged()
    {
        //get payment description
        if (xmlhttp.readyState==4)
        {
            //            var str=xmlhttp.responseText;
            //str2=str.search(/,/)+1;
            document.getElementById("myContent").innerHTML=xmlhttp.responseText;
        }
    }
    
    
    function getOrders(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
        strLoc=document.getElementById('st_id').value;
        strdt1=document.getElementById('strDate1').value;
        strdt2=document.getElementById('strDate2').value;

        
        var url="getReportsData.php?ordStatus="+str;
        url=url+"&ordLoc="+strLoc;
        url=url+"&orddt1="+strdt1+"&orddt2="+strdt2;
        url=url+"&sid="+Math.random();
        url=url+"&task=getOrders";
        xmlhttp.onreadystatechange=stateChanged2;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }

    function stateChanged2()
    {
        //get payment description
        if (xmlhttp.readyState==4)
        {
            //            var str=xmlhttp.responseText;
            //str2=str.search(/,/)+1;
            document.getElementById("myContent").innerHTML=xmlhttp.responseText;
        }
    }
    
    function getDiagnosis(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
       var icd1=  document.getElementById('icd1').value
       var icd2=  document.getElementById('icd2').value
       var age1=  document.getElementById('age1').value
       var age2=  document.getElementById('age2').value
       var visits=document.getElementById('visits').value
       var gender=document.getElementById('gender').value
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
       var pid=   document.getElementById("pid").value;
        var url="getReportsData.php?task=getDiagnosis";
        url=url+"&sid="+Math.random();
        url=url+"&icd1="+icd1;
         url=url+"&icd2="+icd2;
          url=url+"&age1="+age1;
           url=url+"&age2="+age2;
            url=url+"&visits="+visits;
             url=url+"&gender="+gender;
              url=url+"&date1="+date1;
              url=url+"&date2="+date2;
              url=url+"&pid="+pid;
        xmlhttp.onreadystatechange=stateChanged3;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }

    function stateChanged3()
    {
        if (xmlhttp.readyState==4)
        {
            document.getElementById("myContent").innerHTML=xmlhttp.responseText;
        }
    }
    
    function getTopDiagnosis(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
      
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
        var url="getReportsData.php?task=getTopDiagnosis";
        url=url+"&sid="+Math.random();
        url=url+"&date1="+date1;
        url=url+"&date2="+date2;
        xmlhttp.onreadystatechange=stateChanged4;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }

    function stateChanged4()
    {
        if (xmlhttp.readyState==4)
        {
            document.getElementById("myContent").innerHTML=xmlhttp.responseText;
        }
    }
    
    
     function getAdmDis(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
      
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
       var strAdmDis= document.getElementById('admdis').value
       var strwards= document.getElementById('wards').value
       var grpWards= document.getElementById('grpWards').value
       var sex= document.getElementById('sex').value
       var discTypes= document.getElementById('dischargeTypes').value

        var url="getReportsData.php?task=getAdmDis";
        url=url+"&sid="+Math.random();
        url=url+"&date1="+date1;
        url=url+"&date2="+date2;
        url=url+"&admdis="+strAdmDis;
        url=url+"&wards="+strwards;
        url=url+"&grpWards="+grpWards
        url=url+"&sex="+sex
        url=url+"&discTypes="+discTypes
        xmlhttp.onreadystatechange=stateChanged4;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }
    
    function getLab(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
      
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
       var strLab= document.getElementById('lab').value
       var pid= document.getElementById('pid').value
       
        var url="getReportsData.php?task=lab";
        url=url+"&sid="+Math.random();
        url=url+"&date1="+date1;
        url=url+"&date2="+date2;
        url=url+"&getLab="+strLab;
        url=url+"&pid="+pid;
        xmlhttp.onreadystatechange=stateChanged4;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }


function getXray(str){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
      
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
       var strXray= document.getElementById('xray').value
       var pid= document.getElementById('pid').value
        var url="getReportsData.php?task=xray";
        url=url+"&sid="+Math.random();
        url=url+"&date1="+date1;
        url=url+"&date2="+date2;
        url=url+"&getXray="+strXray;
          url=url+"&pid="+pid;
        xmlhttp.onreadystatechange=stateChanged4;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }

    function getDrugs(){
        xmlhttp=GetXmlHttpObject();
        if (xmlhttp==null)
        {
            alert ("Browser does not support HTTP Request");
            return;
        }
      
       var date1= document.getElementById('strDate1').value
       var date2= document.getElementById('strDate2').value
       var drugsRpts= document.getElementById('drugsRpts').value
       var pid= document.getElementById('pid').value
        var url="getReportsData.php?task=drugs";
        url=url+"&sid="+Math.random();
        url=url+"&date1="+date1;
        url=url+"&date2="+date2;
        url=url+"&drugsRpts="+drugsRpts;
          url=url+"&pid="+pid;
        xmlhttp.onreadystatechange=stateChanged4;
        xmlhttp.open("POST",url,true);
        xmlhttp.send(null);
    }

    function stateChanged4()
    {
        if (xmlhttp.readyState==4)
        {
            document.getElementById("myContent").innerHTML=xmlhttp.responseText;
        }
    }
    
    function exportDiagnosisExcel(){
       var age2 = document.getElementById("age2").value;
       var date1 = document.getElementById("strDate1").value;
       var date2 = document.getElementById("strDate2").value;
       var gender = document.getElementById("gender").value;
       var icd1 =document.getElementById("icd1").value;
       var icd2 = document.getElementById("icd2").value;
//       var task = document.getElementById("task").value;
       var visits = document.getElementById("visits").value;

        window.open('exportDiagnosis.php?rptType=diagnosis\n\
        &age2='+age2+'&date1='+date1+'&date2='+date2+'&gender='+gender+'&icd1='+icd1+'&icd2='+icd2+'&visits='+visits
        
        ,"Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");

    }

     function exportAdmDis(str){
       var date1 = document.getElementById("strDate1").value;
       var date2 = document.getElementById("strDate2").value;
       
       if(str=='dis'){
                window.open('exportDischarges.php?rptType=discharge&date1='+date1+'&date2='+date2,
                "Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");
       }else if(str=='adm'){
                window.open('exportAdmissions.php?rptType=admission&date1='+date1+'&date2='+date2,
                "Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");
       }

    }

    
    function printStatement(str){
       var strDate1= document.getElementById('strDate1').value;
       var strDate2= document.getElementById('strDate2').value;
       var drugsRpts= document.getElementById('drugsRpts').value;
       var pid= document.getElementById('pid').value;

            window.open('drugsStatement_pdf.php?pid='+pid+'&rptType='+drugsRpts+'&date1='+strDate1+'&date2='+strDate2
            ,"Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");

    }
    
        
    function exportDrugsToExcel(){
        
       var strDate1= document.getElementById('strDate1').value;
       var strDate2= document.getElementById('strDate2').value;
       var drugsRpts= document.getElementById('drugsRpts').value;
       var pid= document.getElementById('pid').value;

        if(drugsRpts==1){
            window.open('exportDrugsRevenue.php?rptType='+drugsRpts+'&rptDesc=Drugs Revenue&date1='+strDate1+'&date2='+strDate2
            ,"Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");
        }else if(drugsRpts==2){
            window.open('exportDrugsRevenue2.php?rptType='+drugsRpts+'&rptDesc=Drugs Revenue&date1='+strDate1+'&date2='+strDate2
            ,"Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");
        }else if(drugsRpts==3){
            window.open('exportDrugStatement.php?rptType='+drugsRpts+'&rptDesc=Drugs Revenue&date1='+strDate1+'&date2='+strDate2+'&pid='+pid
            ,"Reports","menubar=yes,toolbar=yes,width=500,height=550,location=yes,resizable=no,scrollbars=yes,status=yes");
        }else{
            alert('Select report type');
        }
    }
    
    
    function GetXmlHttpObject()
    {
        if (window.XMLHttpRequest)
        {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            return new XMLHttpRequest();
        }
        if (window.ActiveXObject)
        {
            // code for IE6, IE5
            return new ActiveXObject("Microsoft.XMLHTTP");
        }
        return null;
    }


</script>