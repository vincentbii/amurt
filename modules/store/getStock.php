<?php
require('./root.php');
header('Access-Control-Allow-Origin: *');
$sql = "SELECT d.item_id, d.partcode, d.item_description, l.quantity, l.qty_in_store, d.purchasing_class, d.item_status, l.reorderlevel FROM care_tz_drugsandservices as d inner join care_ke_locstock as l on d.partcode = l.stockid where d.purchasing_class in ('Medical-Supplies', 'Drug_list')";
$result1 = $db->Execute($sql);

$outp = "";
while($rs = $result1->FetchRow()) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"id":"'  . $rs["item_id"] . '",';
    $outp .= '"partcode":"'   . $rs["partcode"] . '",';
    $outp .= '"drug_class":"'. $rs["purchasing_class"]     . '",';
    $outp .= '"item_description":"'. $rs["item_description"]     . '",';
    $outp .= '"qty":"'. $rs["qty_in_store"]     . '",';
    $outp .= '"buying_price":"'. $rs["unit_price"]     . '",';
    $outp .= '"reorder_level":"'. $rs["reorderlevel"]     . '"}';
}
$outp ='{"records":['.$outp.']}';
$db->close();

echo($outp);



?>