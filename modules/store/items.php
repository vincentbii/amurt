<?php
require('./root.php');

$sql = "SELECT d.item_id, d.partcode, d.item_description AS des, l.quantity, l.qty_in_store, d.purchasing_class, d.item_status, l.reorderlevel FROM care_tz_drugsandservices as d inner join care_ke_locstock as l on d.partcode = l.stockid where d.purchasing_class in ('Drug_list') and l.loccode in ('DISPENS') limit 0, 480";
$result1 = $db->Execute($sql);

$outp = "";
while($rs = $result1->FetchRow()) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"item_id":"'  . $rs["item_id"] . '",';
    $outp .= '"partcode":"'   . $rs["partcode"] . '",';
    $outp .= '"item_description":"'. trim($rs["des"])     . '",';
    $outp .= '"qty_store":"'. $rs["qty_in_store"]     . '",';
    $outp .= '"qty_dep":"'. $rs["quantity"]     . '",';
    $outp .= '"purchasing_class":"'. $rs["purchasing_class"] . '",';
    $outp .= '"status":"'. $rs["item_status"]     . '",';
    $outp .= '"reorder_level":"'. $rs["reorder_level"]     . '"}';
}
$outp ='{"items_data":['.$outp.']}';
$db->close();

echo($outp);



?>