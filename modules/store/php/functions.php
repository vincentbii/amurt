<?php
require('./root.php');
require($root_path.'include/inc_environment_global.php');
require_once($root_path . "include/inc_img_fx.php");
require_once($root_path . 'include/inc_date_format_functions.php');

function getStock()
{
	global $db;
	$sql = "SELECT * FROM care_ke_store";
	$result = $db->Execute($sql);
	$data = array();
	$dataInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$data[] = $row;
		}
	}
	return json_encode($data);
}

function getItems(){
	global $db;
	$sql = "SELECT * FROM care_tz_drugsandservices";
	$result = $db->Execute($sql);
	$data = array();
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$data['items_data'][] = $row;
		}
	}
	return $data;
}

function getDrugClasses(){
	global $db;
	$sql = "SELECT id, name FROM care_tz_drug_classes WHERE status = '1'";
	$result = $db->Execute($sql);
	$dclasses = array();
	$dclassesInc = 0;
	if($result->RecordCount() > 0){
		while ($row = $result->FetchRow()) {
			$dclasses[$dclassesInc] = array('id' => $row['id'], 'name' => $row['name']);
			$dclassesInc++;
		}
	}

	$json_array = json_encode($dclasses);
	return $json_array;
}



?>