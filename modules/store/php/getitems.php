<?php
require('../root.php');
header('Access-Control-Allow-Origin: *');

$sql = "SELECT d.item_id, d.partcode, d.item_description, l.quantity, l.qty_in_store, d.purchasing_class, d.item_status, l.reorderlevel FROM care_tz_drugsandservices as d inner join care_ke_locstock as l on d.partcode = l.stockid where d.purchasing_class in ('Medical-Supplies', 'Drug_list')";
$result1 = $db->Execute($sql);

$cashier = array();
	$cashierInc = 0;
	while ($row = $result1->FetchRow()) {
		$json = (array('partcode' => $row['partcode'], 'item_description' => $row['item_description'], 'qty_dep' => $row['quantity'], 'qty_store' => $row['qty_in_store'], 'reorder_level' => $row['reorderlevel'], 'status' => $row['item_status']));
		$cashier[$cashierInc] = $json;
		$cashierInc++;
	}
	$json_array = json_encode($cashier);
	$db->close();

	return $json_array;



?>