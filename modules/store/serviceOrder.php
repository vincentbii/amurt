<?php
//echo var_dump($_POST);
require_once('roots.php');
require_once($root_path.'include/care_api_classes/class_weberp_c2x.php');
require_once($root_path.'include/inc_init_xmlrpc.php');
$debug=false;

($debug) ? $db->debug=FALSE : $db->debug=FALSE;

$req_no=$_POST[ordIrnNo];
$issueNo=$_POST[issiuNo];
$store_loc=$_POST[storeID];
$store_desc=$_POST[storeDesc];
$sup_storeId=$_POST[supStoreid];
$supStoreDesc=$_POST[supStoredesc];
$period='2009';
$req_date=$_POST[ordDate];
$req_time=date("H:i:s");
$issue_date=date("Y-m-d");
$issue_time=date("H:i:s");

$input_user = $_SESSION['sess_login_username'];

//echo var_dump($_POST);
//echo 'rows '.$_POST[gridbox_rowsadded];



function InsertData($db,$rowid,$req_no,$issueNo,$status,$req_date,$req_time,$store_loc,
        $store_desc,$period,$input_user,$sup_storeId,$supStoreDesc,$issue_date,$issue_time) {
    $debug=false;

    $itemId=$_POST["gridbox_".$rowid."_0"];
    $item_Desc=$_POST["gridbox_".$rowid."_1"];
    $unitQty=$_POST["gridbox_".$rowid."_2"];
    $qty=$_POST["gridbox_".$rowid."_3"];
    $qty_issued=$_POST["gridbox_".$rowid."_5"];
    $bal=$_POST["gridbox_".$rowid."_6"];
    $totalUnits=$_POST["gridbox_".$rowid."_8"];

    if($bal>0){
        $balance=intval($bal-$qty_issued);
    }else{
        $balance=intval($qty-$qty_issued);
    }

    
//    $Price=$_POST["gridbox_".$rowid."_3"];

   $csql="INSERT INTO care_ke_internalserv
              ( req_no,issueno,STATUS,req_date,req_time,store_loc,Store_desc,sup_storeId,sup_storeDesc,
                item_id,Item_desc,qty,qty_issued,period,input_user,balance,issue_date,issue_time)
	VALUES
                ('$req_no',
                '$issueNo',
                'serviced',
                '$req_date',
                '$req_time',
                '$store_loc',
                '$store_desc',
                '$sup_storeId',
                '$supStoreDesc',
                '$itemId',
                '$item_Desc',
                '$qty',
                '$qty_issued',
                '$period',
                '$input_user',
                '$balance',
                '$issue_date',
                '$issue_time'
                )";
    
    $result2=$db->Execute($csql);
    if($debug) echo $csql.'<br>';
   
    $sql1='select quantity from care_ke_locstock where stockid="'.$itemId.'" and loccode="'.$store_loc.'"';
    $result=$db->Execute($sql1);
     if($debug) echo $sql1;
    $row=$result->fetchRow();
    $newQty=intval($row[0])+intval($qty_issued);

     if(checkStoreType($sup_storeId)==1){
         $sql2="Update care_ke_locstock set quantity=quantity+$totalUnits where stockid='$itemId' and loccode='$store_loc'";
        $db->Execute($sql2);
        if($debug) echo $sql2;
    }

    if($balance==0){
       $status='Serviced';
    }else if($balance>0){
        $status='Pending';
    }
    
     $sql3="update care_ke_internalreq set status='$status',qty_issued='$qty_issued',balance='$balance',
        issue_date='".date('Y-m-d')."',issue_time='".date('H:i:s')."' where req_no='$req_no' and item_id='$itemId'";
        $db->Execute($sql3);
        if($debug) echo $sql3;
}


function checkStoreType($loccode){
    global $db;
    $debug=false;
    $sql="select mainstore from care_ke_stlocation where st_id='$loccode'";
    if($debug) echo $sql;

    $request=$db->Execute($sql);
    $row=$request->FetchRow();
    return $row[0];
}

$sql3='SELECT count(id) as cnt FROM care_ke_internalreq where req_no="'.$req_no.'"';
$result2=$db->Execute($sql3);
$row=$result2->FetchRow();

if($debug) echo $sql3;

    for($i=1;$i<=$row[0];$i++) {
//        echo '<br>added row '.$arr_rows[$i].'<br>';
        InsertData($db,$i,$req_no,$issueNo,$status,$req_date,$req_time,$store_loc,
         $store_desc,$period,$input_user,$sup_storeId,$supStoreDesc,$issue_date,$issue_time); 
         
    }

$weberp_obj = new_weberp();
    
    $sql="SELECT s.*,d.unit_qty FROM `care_ke_internalserv` s LEFT JOIN `care_tz_drugsandservices` d ON s.item_id=d.partcode
            WHERE req_no=$req_no AND `status`='Serviced'";
    if($debug) echo $sql;

    $result=$db->Execute($sql);
    while($row=$result->FetchRow()){

        $pSdate=new DateTime( $row[req_date]);
        $pdateS=$pSdate->format('Y-m-d');
        $servDate= $pdateS;

        //echo ' Store is '.$sup_storeId;

        // if($sup_storeId=='MAIN'){
        //     if($weberp_obj->stock_adjustment_in_webERP($row[item_id], $row[sup_storeId],$row[store_loc], $row[qty_issued], $servDate)=='failure') {
        //         //echo "failed to transmit $row[item_id] to weberp GL<br>";
        //     }else {
        //         // echo "transmitted $row[item_id] GLs successfully<br>";
        //     }
        // }else{
            reduceStock($db, $row[item_id], $row[sup_storeId],  $row[qty_issued], $req_no);
        // }
    }


    function reduceStock($db, $stockid, $store, $qty, $req_no) {
      global $db;
      $debug=FALSE;
        $sql1 = 'select qty_in_store from care_ke_locstock where stockid="' . $stockid . '" and loccode="DISPENS"';
        $result2 = $db->Execute($sql1);
        if ($debug) echo $sql1 . '<br>';
        
        $row = $result2->FetchRow();
        $newQty = intval($row[0]) - intval($qty);
    
    
        $sql = 'update care_ke_locstock set qty_in_store="' . $newQty . '" where stockid="' . $stockid . '" and loccode="DISPENS"';
        $db->Execute($sql);
        if ($debug) echo $sql . '<br>';
    
        if ($balance > 0) {
            $stat = "pending";
        } else {
            $stat = "serviced";
        }

}





