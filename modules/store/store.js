var app = angular.module('myApp', ["ngRoute", "ui.bootstrap",]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "items.htm",
    })
    .when("/reqdetails", {
    	templateUrl : "reqdetails.htm",
    	controller	: "reqdetailsCtrl"
    })
    .when("/requisitions", {
        templateUrl : "requisitions.htm",
        controller : "reqCtrl"
    })
    .when("/items", {
        templateUrl : "items.htm",
        controller : "itemsCtrl"
    })
    .when("/formstock", {
        templateUrl : "form.htm",
        controller : "formCtrl"
    })
    .when("/stockform", {
        templateUrl : "form1.html",
        controller : "stockFormCtrl"
    });
});

app.controller("reqCtrl", function ($scope, $http) {
    $scope.msg = "I love London";
    $scope.ItemDetailsIf = false;
    $http.get('getRequisitions.php').then(function (response){
    	$scope.requisitions = response.data.requisitions;
    	$scope.viewby = 5;
	      $scope.totalItems = $scope.requisitions.length;
	      $scope.currentPage = 1;
	      $scope.itemsPerPage = $scope.viewby;
	      $scope.maxSize = 5; //Number of pager buttons to show
	      $scope.setPage = function (pageNo) {
	        $scope.currentPage = pageNo;
	      };

	      $scope.pageChanged = function() {
	        console.log('Page changed to: ' + $scope.currentPage);
	      };

	    $scope.setItemsPerPage = function(num) {
	      $scope.itemsPerPage = num;
	      $scope.currentPage = 1; //reset to first page
	    }
    });

    $scope.Issue = function($req_no){
    	$scope.item_reqno = '';
    	$http({
			method: 'POST',
			url   : 'requ_details.php',
			data  : $req_no,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function(response){
			if(data.errors){

			}else{
				
				$scope.redetails = response.data.reqdetails;
				$scope.ItemDetailsIf = true;
			}
		});
    }
    
    $scope.Save = function(){
    	// alert(JSON.stringify($scope.redetails));
    	// $scope.itemsList = JSON.stringify($scope.items);
    	$http({
			method: 'POST',
			url   : 'postRequisitions.php',
			data  : $scope.redetails,
		}).then(function(data){
			if(data.errors){
				
			}else{
				$scope.msg = data;
				alert("Updated Successfully");
				$scope.reload();
			}
		});
    }

    $scope.Deactivate = function($req_no){
    	$scope.msg = 'button Deactivate Clicked';
    }
});

app.controller("formCtrl", function ($scope, $http) {
	$http.get('getItems.php').then(function (response){
		$scope.msg = "";
	    $scope.data = response.data.items_data;

		  $scope.setQuery = function(query) {
		    $scope.query = query.item_description;
		    $scope.partcode = query.partcode;
		    $scope.unit_price = query.unit_price;
		    $scope.purchasing_class = query.purchasing_class;
		    $scope.focus = false;
		  };		
	});
$scope.list = [];
	$scope.AddItems = function() {
		$scope.rowarray = [];
	        if ($scope.query) {
	        	$scope.rowarray.push($scope.partcode);
	        	$scope.rowarray.push($scope.quantity);
	        	$scope.rowarray.push($scope.purchasing_class);
	          $scope.list.push(this.rowarray);
	          $scope.query = '';
	        }
	      };
$scope.submit = function(){
	$http({
		method: 'POST',
		url   : 'postStock.php',
		data  : $scope.list,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function(data){
		if(data.errors){

		}else{
			$scope.msg = data;
		}
	});
}
});

app.filter('search', function() {
  return search;
});

function search(arr, query) {
  if (!query) {
    return arr;
  }

  var results = [];
  query = query.toLowerCase();

  angular.forEach(arr, function(item) {
    if (item.toLowerCase().indexOf(query) !== -1) {
      results.push(item);
    }
  });

  return results;
};

app.controller("stockFormCtrl", function ($scope) {

	$scope.msg = "";
});

app.filter('search', function() {
  return search;
});

function search(arr, query) {
  if (!query) {
    return arr;
  }

  var results = [];
  query = query.toLowerCase();

  angular.forEach(arr, function(item) {
    if (item.toLowerCase().indexOf(query) !== -1) {
      results.push(item);
    }
  });

  return results;
};

app.controller("itemsCtrl", function ($scope, $http) {
    $scope.valuess = "";
    $http.get('items.php').then(function (response){

    	
    	$scope.ItemDetailsIf = false;
    	$scope.items = response.data.items_data;
    	$scope.viewby = 10;
	      $scope.totalItems = $scope.items.length;
	      $scope.currentPage = 1;
	      $scope.itemsPerPage = $scope.viewby;
	      $scope.maxSize = 5; //Number of pager buttons to show
	      $scope.setPage = function (pageNo) {
	        $scope.currentPage = pageNo;
	      };

	      $scope.pageChanged = function() {
	        console.log('Page changed to: ' + $scope.currentPage);
	      };

	    $scope.setItemsPerPage = function(num) {
	      $scope.itemsPerPage = num;
	      $scope.currentPage = 1; //reset to first page
	    }
    });

    $scope.ItemDetails = function($item){
    	$scope.itemidDetail = $item.partcode;
    	$scope.iteddeDetails = $item.item_description;
    	$scope.qtystore = $item.qty_store;
    	$scope.qtydep = $item.qty_dep;
    	$scope.ItemDetailsIf = true;
    }
    $scope.qtydepd = '';
    $scope.Submit = function(){
    	alert($scope.qtydep);
		}
});

app.controller('customersCtrl', function($scope, $http) {
    $http.get("getStock.php")
    .then(function (response) {
      $scope.names = response.data.records;
      $scope.viewby = 10;
      $scope.totalItems = $scope.names.length;
      $scope.currentPage = 1;
      $scope.itemsPerPage = $scope.viewby;
      $scope.maxSize = 5; //Number of pager buttons to show
      $scope.DeleteItem = function() {
        
      };

      $scope.EditItem = function(){

      }

      $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
      };

      $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
      };

    $scope.setItemsPerPage = function(num) {
      $scope.itemsPerPage = num;
      $scope.currentPage = 1; //reset to first page
    }
  });
});