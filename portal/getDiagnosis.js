

var getDiagnosis = new Ext.data.JsonStore({
    storeId: 'getDiag',
    url: 'getDiagnosis.php', //url to data object (server side script)
    method: 'POST'
    ,
    baseParams:{
        disease:'dis_code'
    },
    //    url: 'getPayments.php',p.`Pid`, p.`emp_names`,p.`pay_type`, p.`amount`,e.bank_name,e.account_no
    //PID:'. $row[0].',ICD_10_code:"'. $row[4].',ICD_10_description:"'. $row[5].',timestamp
    root: 'disease_code',
    fields:[
    {
        name: 'ICDcode',
        type: 'string'
    },
 
    {
        name: 'less5',
        type: 'string'
    }
    ,
    {
        name:'above5',
        type: 'string'
    },
    {
        name:'Female',
        type: 'string'
    },
    {
        name:'male',
        type: 'string'
    },
    {
        name:'Total',
        type: 'string'
    }
    ]
});
//{ "diagnosis":[
//{code:"S05.0",description:"Corneal abrasion or njury of conjunctiva ",<5:"0",>5:"1",Male:"1",Female:"0",Total:"1"},


var tpl3 = new Ext.XTemplate(
    '<table border="0" width="100%">',
    '<tr class="selectdiags"><td>ICD code</td><td class="dis_right">Female</td><td class="dis_right">Male</td><td>>5</td><td><5</td><td class="dis_right">Total</td></tr>',
    '<tpl for=".">',
    '<tpl><tr class="select_diags">',
    '<td>{ICDcode}</td><td class="dis_right">{Female}</td><td class="dis_right">{male}</td><td class="dis_right">{less5}</td><td class="dis_right">{above5}</td><td class="dis_right">{Total}</td></tr>',
    '</tpl>',
    '</tpl>',
    '</table>',
    '<div class="x-clear"></div>'
    );


var diagPanel = new Ext.Panel({
    width: 440,
    region:'center',
    margins: '0 5 5 5',
//    height:300,
//    html:' Ext.example.shortBogusMarkup',
    items: new Ext.DataView({
        store: getDiagnosis,
        tpl: tpl3,
        autoHeight:true,
        multiSelect: true,
        overClass:'x-view-over',
        itemSelector:'div.select_diags'
    })
});


var diagsReport = new Ext.Panel({

    layout: 'border',
    layoutConfig: {
        columns: 1
    },
    width:450,
    height: 300,
    items: [diagPanel]
});
getDiagnosis.load();





