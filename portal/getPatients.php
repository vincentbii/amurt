
<?php
error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$month=$_POST[strMonth];

$sql = 'SELECT c.`pid`,c.`name_first`, c.`name_2`, c.`name_last`, c.`date_birth`, c.`sex`,e.`name_formal`, d.`encounter_date`,d.encounter_nr,d.encounter_time
    FROM care_person c
inner join care_encounter d on c.pid=d.pid inner join care_department e on e.nr=d.current_dept_nr
where e.`type`=1 and d.encounter_class_nr=2 and d.encounter_date=date(now()) order by d.encounter_time desc';
//echo $sql;

$result=$db->Execute($sql);
$numRows=$result->RecordCount();
echo '{
    "Opatients":[';
$counter=0;
while ($row = $result->FetchRow()) {
    echo '{"pid":"'. $row[0].'","name_first":"'. $row[1].'","name_2":"'. $row[2].'","name_last":"'. $row[3].'","Time":"'. $row[encounter_time]
    .'","sex":"'. $row[5] .'","name_formal":"'. $row[6].'","enc_nr":"'. $row[encounter_nr].'","date_birth":"'. $row[4].'"}';
    if ($counter<>$numRows){
        echo ",";
    }
    $counter++;
}
echo ']}';

?>