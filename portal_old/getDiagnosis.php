
<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require_once('roots.php');
require ($root_path . 'include/inc_environment_global.php');

$month = $_POST[strMonth];

echo '{"disease_code":[';


$sql = "SELECT year(now())-year(d.`date_birth`) as age,d.`sex`,c.`ICD_10_code`, c.`ICD_10_description` FROM care_tz_diagnosis c
inner join care_person d on c.pid=d.pid";

if ($rs_ptr = $db->Execute($sql))
    $res_array = $rs_ptr->GetArray();

while (list ($i, $v) = each($res_array)) {
echo '{';
    $icd_10_code = $v['ICD_10_code'];
    $icd_10_description = $v['ICD_10_description'];
    echo 'ICDcode:"' . $icd_10_code . '",';
//    echo 'description:"' . $icd_10_description . '",';

//    /**
//     * Amount by age
//     */
    
//
//
//
    $sql = "SELECT count(d.`date_birth`)as total_under_age FROM care_tz_diagnosis c
inner join care_person d on c.pid=d.pid  WHERE d.`date_birth`<=(year(now())-year(d.`date_birth`)) and c.ICD_10_code='" . $icd_10_code . "'";
    $rs_ptr = $db->Execute($sql);
    $row = $rs_ptr->FetchRow();
    $total_under_age = $row['total_under_age'];

    $total_over_age = $total - $total_under_age;

    echo 'less5:"'.$total_under_age.'",';
    echo 'above5:"'.$total_over_age.'",';


//    /**
//     * Amount by sex
//     */
    $sql = "SELECT count(year(now())-year(d.`date_birth`)) as total_female  FROM care_tz_diagnosis c
inner join care_person d on c.pid=d.pid WHERE d.sex='f' AND c.ICD_10_code='" . $icd_10_code . "'";
    $rs_ptr = $db->Execute($sql);
    $row = $rs_ptr->FetchRow();
    $total_female = $row['total_female'];
   echo 'Female:"' . $total_female . '",';

    $sql = "SELECT count(year(now())-year(d.`date_birth`))  as total_male FROM care_tz_diagnosis c
inner join care_person d on c.pid=d.pid WHERE d.sex='m' AND c.ICD_10_code='" . $icd_10_code . "'";
    $rs_ptr = $db->Execute($sql);
    $row = $rs_ptr->FetchRow();
    $total_male = $row['total_male'];
//
    echo 'male:"' . $total_male . '",';

    $sql = "SELECT count(year(now())-year(d.`date_birth`)) as total FROM care_tz_diagnosis c
inner join care_person d on c.pid=d.pid WHERE c.ICD_10_code='" . $icd_10_code . "'";
    $rs_ptr = $db->Execute($sql);
    $row = $rs_ptr->FetchRow();
    $total = $row['total'];

    echo 'Total:"' . $total . '"';
    echo '},';
}
echo ']}';
?>