<?php

error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require('./roots.php');
require($root_path . 'include/inc_environment_global.php');

# Set default values if not available from url
//if (!isset($station)||empty($station)) {
//    $station=$_SESSION['sess_nursing_station'];
//} # Default station must be set here !!
if (!isset($pday) || empty($pday))
    $pday = date('d');
if (!isset($pmonth) || empty($pmonth))
    $pmonth = date('m');
if (!isset($pyear) || empty($pyear))
    $pyear = date('Y');
$s_date = $pyear . '-' . $pmonth . '-' . $pday;
if ($s_date == date('Y-m-d'))
    $is_today = true;
else
    $is_today=false;

if (!isset($mode))
    $mode = '';

function getWardInfo($wrdNO) {
    global $db;
    $db->debug = 0;
    require('./roots.php');
    require_once($root_path . 'include/care_api_classes/class_ward.php');
    $ward_obj = new Ward;

# Load date formatter
    require_once($root_path . 'include/inc_date_format_functions.php');
    require_once($root_path . 'global_conf/inc_remoteservers_conf.php');
    $ward_nr = $wrdNO;
//    echo 'Ward No is '.$ward_nr;
    if ($ward_info = $ward_obj->getWardInfo($ward_nr)) {
        $room_obj = &$ward_obj->getRoomInfo($ward_nr, $ward_info['room_nr_start'], $ward_info['room_nr_end']);
        if (is_object($room_obj)) {
            $room_ok = true;
        } else {
            $room_ok = false;
        }
        # GEt the number of beds
        $nr_beds = $ward_obj->countBeds($ward_nr);
//        echo 'number of beds '.$nr_beds.'<br>';
        # Get ward patients
        if ($is_today)
            $patients_obj = &$ward_obj->getDayWardOccupants($ward_nr);
        else
            $patients_obj= & $ward_obj->getDayWardOccupants($ward_nr, $s_date);
//
//    echo $ward_obj->getLastQuery();
//    echo $ward_obj->LastRecordCount();

        if (is_object($patients_obj)) {
            # Prepare patients data into array matrix
            while ($buf = $patients_obj->FetchRow()) {
                $patient[$buf['room_nr']][$buf['bed_nr']] = $buf;
            }
            $patients_ok = true;
            $occup = 'ja';
        } else {
            $patients_ok = false;
        }

        $ward_ok = true;

        # Create the waiting inpatients' list
        $wnr = (isset($w_waitlist) && $w_waitlist) ? 0 : $ward_nr;
        $waitlist = $ward_obj->createWaitingInpatientList($wnr);
        $waitlist_count = $ward_obj->LastRecordCount();

        # Get the doctor's on duty information
        #### Start of routine to fetch doctors on duty
# If ward exists, show the occupancy list

        if ($ward_ok) {
            if ($pyear . $pmonth . $pday < date('Ymd')) {
//                echo '<b>'.$LDAttention.'</font> '.$LDOldList.'</b>';
                # Prevent adding new patients to the list  if list is old
                $edit = FALSE;
            }

            # Start here, create the occupancy list
            # Assign the column  names
            # Initialize help flags
            $toggle = 1;
            $room_info = array();
            # Set occupied bed counter
            $occ_beds = 0;
            $lock_beds = 0;
            $males = 0;
            $females = 0;
            $cflag = $ward_info['room_nr_start'];

            # Initialize list rows container string
            $sListRows = '';

            # Loop trough the ward rooms

            for ($i = $ward_info['room_nr_start']; $i <= $ward_info['room_nr_end']; $i++) {
                if ($room_ok) {
                    $room_info = $room_obj->FetchRow();
                } else {
                    $room_info['nr_of_beds'] = 1;
                    $edit = false;
                }

                // Scan the patients object if the patient is assigned to the bed & room
                # Loop through room beds


                for ($j = 1; $j <= $room_info['nr_of_beds']; $j++) {
                    //for($j=1;$j<=$nr_beds;$j++){
                    # Reset elements


                    if ($patients_ok) {

                        if (isset($patient[$i][$j])) {
                            $bed = &$patient[$i][$j];
                            $is_patient = true;
                            # Increase occupied bed nr
                            $occ_beds++;
                        } else {
                            $is_patient = false;
                            $bed = NULL;
                        }
                    }

                    if ($is_patient) {
                        $sBuffer = '<a href="javascript:popPic(\'' . $bed['pid'] . '\')">';
                        if (strtolower($bed['sex']) == 'f') {
                            $females++;
                        } elseif (strtolower($bed['sex']) == 'm') {
                            $males++;
                        }
                    }
                }
                # set room nr change flag , toggle row color
                if ($cflag != $i) {
                    $toggle = !$toggle;
                    $cflag = $i;
                }

                # Check if bed is locked
                if (stristr($room_info['closed_beds'], $j . '/')) {
                    $bed_locked = true;
                    $lock_beds++;
                    # Consider locked bed as occupied so increase occupied bed counter
                    $occ_bed++;
                } else {
                    $bed_locked = false;
                }
            } // end of ward loop
            $sql = 'SELECT c.`nr`, c.`ward_id`, c.`name` FROM care_ward c where  c.`nr`="'.$ward_nr.'"';
           // echo $sql;
            $result = $db->Execute($sql);
            $numRows = $result->RecordCount();
            $row = $result->FetchRow();
            # Final occupancy list line
            # Prepare the stations quick info data
            # Occupancy in percent
            $occ_percent = ceil(($occ_beds / $nr_beds) * 100);

            # Nr of vacant beds
            $vac_beds = $nr_beds - $occ_beds;
            echo '{Ward:"' . $row[2] . '",';
            echo 'beds:"' . $nr_beds . '",';
            echo 'occupancy:"' . $occ_percent . '%",';
            echo 'occupied:' . $occ_beds . ',';
            echo 'vacant:' . $vac_beds . ',';
//            echo 'Locked:' . $lock_beds . ',';
            echo 'Males:"' . $males . '",';
            echo 'females:"' . $females. '"},';

            //$buf1='<img '.createComIcon($root_path,'powdot.gif','0','absmiddle').'>';
            # Create waiting list block
//
//            if($waitlist_count) {
//                while($waitpatient=$waitlist->FetchRow()) {
//
//                    $buf2='';
//                    //if($waitpatient['current_ward_nr']!=$ward_nr) $buf2='<nobr>'.$waitpatient['ward_id'].'::';
//                    if($waitpatient['current_ward_nr']!=$ward_nr) $buf2=createComIcon($root_path,'red_dot.gif','0','',TRUE);
////            else  $buf2=createComIcon($root_path,'green_dot.gif','0','',TRUE);
//                    echo '<nobr><img '.$buf2.'><a href="javascript:assignWaiting(\''.$waitpatient['encounter_nr'].'\',\''.$waitpatient['ward_id'].'\')">';
//                    echo '&nbsp;'.$waitpatient['name_last'].', '.$waitpatient['name_first'].' '.formatDate2Local($waitpatient['date_birth'],$date_format).'</nobr></a><br>';
//                }
//            }else {
//                echo '&nbsp;';
//            }
//            if($edit) {
//                $wlist_url=$thisfile.URL_APPEND.'&ward_nr='.$ward_nr.'&edit='.$edit.'&station='.$station;
//                if($w_waitlist) {
//                    echo '[<a href="'.$wlist_url.'&w_waitlist=0">'.$LDShowWardOnly.'</a>]';
//                }else {
//                    echo' [<a href="'.$wlist_url.'&w_waitlist=1">'.$LDShowAll.'</a>]'; 
//                }
//            }
        }
    }
}


echo '{"Wards":[';
getWardInfo(30);
//echo '<br><br>';
getWardInfo(31);
//echo '<br><br>';
getWardInfo(32);
//echo '<br><br>';
getWardInfo(33);
//echo '<br><br>';
getWardInfo(34);
getWardInfo(35);
getWardInfo(36);
getWardInfo(37);
//echo '<br><br>';
echo ']}';
?>
