<?php
error_reporting(E_COMPILE_ERROR | E_ERROR | E_CORE_ERROR);
require('roots.php');
require($root_path . 'include/inc_environment_global.php');

$lang_tables=array('person.php','actions.php');
define('LANG_FILE','stdpass.php');
define('NO_2LEVEL_CHK',1);
require_once($root_path.'include/inc_front_chain_lang.php');

require_once($root_path.'global_conf/areas_allow.php');
$allowedarea=&$allow_area['admit'];
$append=URL_REDIRECT_APPEND; 

$lang_tables[]='ambulatory.php';
$lang_tables[] = 'prompt.php';
$lang_tables[] = 'departments.php';
define('LANG_FILE', 'nursing.php');
$local_user = 'ck_pflege_user';
require_once($root_path . 'include/inc_front_chain_lang.php');

$fileforward='amb_clinic_patients.php'.$append.'&origin=pass&target=list&dept_nr='.$dept_nr; 
$lognote=$LDAppointments.'ok';

$thisfile=basename($_SERVER['PHP_SELF']);
//$edit=true;
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Custom Layouts and Containers - Portal Example</title>

        <!-- ** CSS ** -->
        <!-- base library -->
        <link rel="stylesheet" type="text/css" href="../include/Extjs/resources/css/ext-all.css" />

        <!-- overrides to base library -->
        <link rel="stylesheet" type="text/css" href="../include/Extjs/ux/css/Portal.css" />

        <!-- page specific -->
        <link rel="stylesheet" type="text/css" href="sample.css" />
        <style type="text/css">

        </style>

        <!-- ** Javascript ** -->
        <!-- ExtJS library: base/adapter -->
        <script type="text/javascript" src="../include/Extjs/adapter/ext/ext-base.js"></script>

        <!-- ExtJS library: all widgets -->
        <script type="text/javascript" src="../include/Extjs/ext-all.js"></script>

        <!-- overrides to base library -->

        <!-- extensions -->
        <script type="text/javascript" src="../include/Extjs/ux/Portal.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/PortalColumn.js"></script>
        <script type="text/javascript" src="../include/Extjs/ux/Portlet.js"></script>
        <script>
    function getinfo(pn){

        urlholder='../modules/nursing/nursing-station-patientdaten.php?sid=<?php echo URL_REDIRECT_APPEND ?>&ntid=false&lang=en';
        urlholder=urlholder+'&pn=' + pn ;
        urlholder=urlholder+ "&pday=$pday&pmonth=$pmonth&pyear=$pyear&edit=1&station=$station";
        patientwin=window.open(urlholder,pn,"width=500,height=400,menubar=no,resizable=yes,scrollbars=yes");

        patientwin.moveTo(0,0)
        patientwin.resizeTo(screen.availWidth,screen.availHeight);
    
    /* else echo '
                  window.location.href=\'nursing-station-pass.php'.URL_APPEND.'&rt=pflege&edit=1&station='.$station.'\''; */

    }        
    </script>
        <!-- page specific -->
        <script type="text/javascript" src="sample-grid.js"></script>
        <!--script type="text/javascript" src="../include/Extjs/shared/examples.js"></script-->
        <script type="text/javascript" src="pie-chart.js"></script>
        <script type="text/javascript" src="portal.js"></script>
        <script type="text/javascript" src="getPatient.js"></script>
        <script type="text/javascript" src="getWardinfo.js"></script>
<!--        <script type="text/javascript" src="getDiagnosis.js"> </script>-->

        <style type="text/css">
            #container {
                padding:10px;
            }
            #container .x-panel {
                margin:10px;
            }
            #container .x-panel-ml {
                padding-left:1px;
            }
            #container .x-panel-mr {
                padding-right:1px;
            }
            #container .x-panel-bl {
                padding-left:2px;
            }

            #container .x-panel-br {
                padding-right:2px;
            }
            #container .x-panel-body {

            }
            #container .x-panel-mc {
                padding-top:0;
            }
            #container .x-panel-bc .x-panel-footer {
                padding-bottom:2px;
            }
            #container .x-panel-nofooter .x-panel-bc {
                height:2px;
            }
            #container .x-toolbar {
                border:1px solid #99BBE8;
                border-width: 0 0 1px 0;
            }
            .chart {
                background-image: url(chart.gif) !important;
            }
            .select_beds{
                font-family: serif,sans-serif;font-size: small;
                padding-left: 10px;text-align: center;

            }
            .selectbeds{
                text-align: center;font-weight: bold;color: #8B3232;
                font-size: small;
                font-size: 9px;

            }
            .select_ward{
                color:#000066;text-align: left;
                border: solid 1px #99BBE8;
                font-size:9px;
            }

            .selectdiags{
                text-align: left;font-weight: bold;color: #8B3232;
                font-size: small;
                
            }
            .select_diagnosis{
                text-align: center;font-weight: bold;color: #8B3232;
                font-size: small;
            }

            .dis_right{
                text-align: center;
            }
            

        </style>

    </head>
    <body>
        <div id="container">

        </div>

    </body>
</html>