
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Care 2x - AMURT</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="">
  <?php
        include ('../include/inc_init_main.php');
        $db = mysql_connect($dbhost,$dbusername,$dbpassword) or die ("No connection: " . mysql_error());
        mysql_select_db($dbname,$db) or die ("Wrong database: " . mysql_error());
    ?>

  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="">
    <div class="collapse navbar-collapse" id="navbarResponsive">
      
      <ul class="navbar-nav ml-auto">      </ul>
    </div>
  </nav>
  <div class="">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
      </ol>
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <?php
              $sql = "SELECT * FROM CARE_ENCOUNTER";
              $result = mysql_query($sql) or die ("SQL Error: " . mysql_error());
              $num_rows = mysql_num_rows($result);
              ?>
              <div class="card-body-icon">
                <i class="fa fa-fw fa-comments"></i>
              </div>
              <div class="mr-5">Total Patients<p><?php echo number_format($num_rows); ?></p></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left"></span>
              <span class="float-right">
                <i class="fa"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <?php
              $today = date('Y-m-d');
              $sql = "SELECT * FROM CARE_ENCOUNTER where encounter_date = '".$today."'";
              $result = mysql_query($sql) or die ("SQL Error: " . mysql_error());
              $num_rows = mysql_num_rows($result);
              $rows = mysql_fetch_assoc($result);
              ?>
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
              <div class="mr-5">Today's Patients
              <p><?php
                echo number_format($num_rows);
                ?></p></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left"></span>
              <span class="float-right">
                <i class="fa"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <?php
              $today = date('Y-m-d');
              $sql = "SELECT * FROM CARE_ENCOUNTER where current_dept_nr = 43";
              $result = mysql_query($sql) or die ("SQL Error: " . mysql_error());
              $num_rows = mysql_num_rows($result);
              $rows = mysql_fetch_assoc($result);
              ?>
              <div class="card-body-icon">
                <i class="fa fa-fw fa-shopping-cart"></i>
              </div>
              <div class="mr-5">Dental Clinic Patients
                <p><?php
                echo number_format($num_rows);
                ?></p></p>
                </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php $root_path.'/sbadmin/dental_patients_pass.php' ?>">
              <span class="float-left"></span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-support"></i>
              </div>
              <div class="mr-5"></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#">
              <span class="float-left">View Details</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
      </div>
      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
          <!-- <i class="fa fa-area-chart"></i> Area Chart Example</div> -->
        <div class="card-body">
          <!-- <canvas id="myAreaChart" width="100%" height="30"></canvas> -->
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>
