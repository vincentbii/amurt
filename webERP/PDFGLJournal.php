<?php

/* $Id$*/

/* $Revision: 1.5 $ */
$PageSecurity = 2;
include('includes/session.inc');

if (isset($_POST['JournalNo'])) {
	$JournalNo=$_POST['JournalNo'];
} else if (isset($_GET['JournalNo'])) {
	$JournalNo=$_GET['JournalNo'];
} else {
	$JournalNo='';
}

$totals=0;

function convert_number($number) {
    if (($number < 0) || ($number > 999999999)) {
        throw new Exception("Number is out of range");
    }

    $Gn = floor($number / 1000000);  /* Millions (giga) */
    $number -= $Gn * 1000000;
    $kn = floor($number / 1000);     /* Thousands (kilo) */
    $number -= $kn * 1000;
    $Hn = floor($number / 100);      /* Hundreds (hecto) */
    $number -= $Hn * 100;
    $Dn = floor($number / 10);       /* Tens (deca) */
    $n = $number % 10;               /* Ones */

    $res = "";

    if ($Gn) {
        $res .= convert_number($Gn) . " Million";
    }

    if ($kn) {
        $res .= (empty($res) ? "" : " ") .
            convert_number($kn) . " Thousand";
    }

    if ($Hn) {
        $res .= (empty($res) ? "" : " ") .
            convert_number($Hn) . " Hundred";
    }

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",
        "Nineteen");
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
        "Seventy", "Eigthy", "Ninety");

    if ($Dn || $n) {
        if (!empty($res)) {
            $res .= " and ";
        }

        if ($Dn < 2) {
            $res .= $ones[$Dn * 10 + $n];
        } else {
            $res .= $tens[$Dn];

            if ($n) {
                $res .= "-" . $ones[$n];
            }
        }
    }

    if (empty($res)) {
        $res = "zero";
    }

    return $res;
}



if ($JournalNo=='Preview') {
	$FormDesign = simplexml_load_file(sys_get_temp_dir().'/Journal.xml');
} else {
	$FormDesign = simplexml_load_file($PathPrefix.'companies/'.$_SESSION['DatabaseName'].'/FormDesigns/Journal.xml');
}

// Set the paper size/orintation
$PaperSize = $FormDesign->PaperSize;
$PageNumber=1;
$line_height=$FormDesign->LineHeight;
include('includes/PDFStarter.php');
$pdf->addInfo('Title', _('General Ledger Journal') );

if ($JournalNo=='Preview') {
	$LineCount = 2; // UldisN
} else {
	$sql="SELECT gltrans.typeno,
				gltrans.trandate,
				gltrans.account,
				chartmaster.accountname,
				gltrans.narrative,
				IF(gltrans.amount>0,gltrans.amount,'') AS db,
				IF(gltrans.amount<0,gltrans.amount,'') AS cr,
				gltrans.tag,
				tags.tagdescription,
				gltrans.jobref
			FROM gltrans
			INNER JOIN chartmaster
				ON gltrans.account=chartmaster.accountcode
			LEFT JOIN tags
				ON gltrans.tag=tags.tagref
			WHERE gltrans.type='0'
				AND gltrans.typeno='" . $JournalNo . "'";
	$result=DB_query($sql, $db);
	$LineCount = DB_num_rows($result); // UldisN
	$myrow=DB_fetch_array($result);
	$JournalDate=$myrow['trandate'];
	DB_data_seek($result, 0);
//    $amtWords=0;
	include('includes/PDFGLJournalHeader.inc');
}
$counter=1;
$YPos=$FormDesign->Data->y;
$dbTotal=0;
$cdTotal=0;
while ($counter<=$LineCount) {
	if ($JournalNo=='Preview') {
		$AccountCode=str_pad('',10,'x');
		$Date='1/1/1900';
		$Description=str_pad('',30,'x');
		$Narrative=str_pad('',30,'x');
		$Amount='XXXX.XX';
		$Tag=str_pad('',25,'x');
		$JobRef=str_pad('',25,'x');
	} else {
		$myrow=DB_fetch_array($result);
		if ($myrow['tag']==0) {
			$myrow['tagdescription']='None';
		}
		$AccountCode = $myrow['account'];
		$Description = ucfirst(strtolower($myrow['accountname'])) ;
		$Date = $myrow['trandate'];
		$Narrative = ucfirst(strtolower($myrow['narrative']));
		$db = $myrow['db'];
        $cr = $myrow['cr'];
		$Tag = $myrow['tag'].' - '.$myrow['tagdescription'];
		$JobRef = $myrow['jobref'];
	}

        if($db<>''){
            $db1=$myrow['db'];
            $db=locale_number_format($db,$_SESSION['CompanyRecord']['decimalplaces']);
        }else{
            $db1=0;
            $db='';
        }

        if($cr<>''){
            $cr1=$myrow['cr'];
            $cr= locale_number_format(abs($cr),$_SESSION['CompanyRecord']['decimalplaces']);
        }else{
            $cr1=0;
            $cr='';
        }




	$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column1->x,$Page_Height-$YPos,$FormDesign->Data->Column1->Length,$FormDesign->Data->Column1->FontSize, $AccountCode);
	$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column2->x,$Page_Height-$YPos,$FormDesign->Data->Column2->Length,$FormDesign->Data->Column2->FontSize, $Description);
	$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column3->x,$Page_Height-$YPos,$FormDesign->Data->Column3->Length,$FormDesign->Data->Column3->FontSize, $Narrative);
	$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column4->x,$Page_Height-$YPos,$FormDesign->Data->Column4->Length,$FormDesign->Data->Column4->FontSize, $db,'right');
    $LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column5->x,$Page_Height-$YPos,$FormDesign->Data->Column5->Length,$FormDesign->Data->Column5->FontSize, $cr,'right');

//	$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column5->x,$Page_Height-$YPos,$FormDesign->Data->Column5->Length,$FormDesign->Data->Column5->FontSize, $Tag);
	$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column6->x,$Page_Height-$YPos,$FormDesign->Data->Column6->Length,$FormDesign->Data->Column6->FontSize, $JobRef, 'left');
	$YPos += $line_height;
	$counter++;
	if ($YPos >= $FormDesign->LineAboveFooter->starty){
		/* We reached the end of the page so finsih off the page and start a newy */
		$PageNumber++;
		$YPos=$FormDesign->Data->y;
		include ('includes/PDFGrnHeader.inc');
	} //end if need a new page headed up

    $dbTotal=intval($dbTotal)+intval($db1);
    $cdTotal=intval($cdTotal)+intval(abs($cr1));
}
$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column1->x,$Page_Height-615,$FormDesign->Totals->Column1->Length,$FormDesign->Totals->Column1->FontSize, 'TOTAL', 'left');
$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column2->x,$Page_Height-615,$FormDesign->Totals->Column2->Length,$FormDesign->Totals->Column2->FontSize, locale_number_format($dbTotal,$_SESSION['CompanyRecord']['decimalplaces']), 'left');
$amtWords =$amtWords. strtoupper(convert_number($dbTotal)) . ' ONLY';
$LeftOvers = $pdf->addText($FormDesign->AmountWords->x,$Page_Height-$FormDesign->AmountWords->y,$FormDesign->AmountWords->FontSize,'AMOUNT IN WORDS: '. $amtWords);

$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column3->x,$Page_Height-615,$FormDesign->Totals->Column3->Length,$FormDesign->Totals->Column3->FontSize, locale_number_format($cdTotal,$_SESSION['CompanyRecord']['decimalplaces']), 'left');

$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column5->x,$Page_Height-710,$FormDesign->Totals->Column5->Length,$FormDesign->Totals->Column5->FontSize, "CHECKED BY .................................................................................................... DATE ..............................................", 'left');
$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column6->x,$Page_Height-750,$FormDesign->Totals->Column6->Length,$FormDesign->Totals->Column6->FontSize, "VERIFIED BY .................................................................................................... DATE ..............................................", 'left');
$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column7->x,$Page_Height-790,$FormDesign->Totals->Column7->Length,$FormDesign->Totals->Column7->FontSize, "APPROVED BY ................................................................................................... DATE ..............................................", 'left');

$preparedDate=date('Y-m-d');

$LeftOvers = $pdf->addTextWrap($FormDesign->Totals->Column4->x,$Page_Height-670,$FormDesign->Totals->Column4->Length, $FormDesign->Totals->Column4->FontSize, "PREPARED BY :".strtoupper($_SESSION['UserID']) ."                                                                                    DATE: ".$preparedDate , 'left');



if ($LineCount == 0) {   //UldisN
	$title = _('Printing Error');
	include('includes/header.inc');
	prnMsg(_('There were no Journals to print'),'warn');
	echo '<br /><a href="'.$rootpath.'/index.php">'. _('Back to the menu').'</a>';
	include('includes/footer.inc');
	exit;
} else {
    $pdf->OutputD($_SESSION['DatabaseName'] . '_Journal_' . date('Y-m-d').'.pdf');//UldisN
    $pdf->__destruct(); //UldisN
}
?>