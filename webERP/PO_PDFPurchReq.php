<?php
/* $Id: PO_PDFPurchReq.php 4479 2011-01-30 05:16:03Z daintree $*/
$PageSecurity = 2;
include('includes/session.inc');
include('includes/SQL_CommonFunctions.inc');
include('includes/DefinePOClass.php');

if(!isset($_GET['OrderNo']) && !isset($_POST['OrderNo'])){
	$title = _('Select a Purchase Order');
	include('includes/header.inc');
	echo '<div class="centre"><br /><br /><br />';
	prnMsg( _('Select a Purchase Order Number to Print before calling this page') , 'error');
	echo '<br />
				<br />
				<br />
				<table class="table_index">
					<tr><td class="menu_group_item">
						<li><a href="'. $rootpath . '/PO_SelectOSPurchOrder.php?'.SID .'">' . _('Outstanding Purchase Orders') . '</a></li>
						<li><a href="'. $rootpath . '/PO_SelectPurchOrder.php?'. SID .'">' . _('Purchase Order Inquiry') . '</a></li>
						</td>
					</tr></table>
				</div>
				<br />
				<br />
				<br />';
	include('includes/footer.inc');
	exit();

	echo '<div class="centre"><br /><br /><br />' . _('This page must be called with a purchase order number to print');
	echo '<br /><a href="'. $rootpath . '/index.php?' . SID . '">' . _('Back to the menu') . '</a></div>';
	exit;
}
if (isset($_GET['OrderNo'])){
	$OrderNo = $_GET['OrderNo'];
} elseif (isset($_POST['OrderNo'])){
	$OrderNo = $_POST['OrderNo'];
}
$title = _('Print Purchase Order Number').' '. $OrderNo;

if (isset($_POST['PrintOrEmail']) ){
	if ($_POST['PrintOrEmail'] =='Email' AND ! IsEmailAddress($_POST['EmailTo'])){
		include('includes/header.inc');
		prnMsg( _('The email address entered does not appear to be valid. No emails have been sent.'),'warn');
		include('includes/footer.inc');
		exit;
	}
}
$ViewingOnly = 0;
if (isset($_GET['ViewingOnly']) AND $_GET['ViewingOnly']!='') {
	$ViewingOnly = $_GET['ViewingOnly'];
} elseif (isset($_POST['ViewingOnly']) AND $_POST['ViewingOnly']!='') {
	$ViewingOnly = $_POST['ViewingOnly'];
}
/* If we are previewing the order then we dont want to email it */
if ($OrderNo == 'Preview') { //OrderNo is set to 'Preview' when just looking at the format of the printed order
	$_POST['PrintOrEmail']='Print';
	/*These are required to kid the system - I hate this */
	$_POST['ShowAmounts']='Yes';
	$OrderStatus = _('Printed');
	$MakePDFThenDisplayIt = True;
}
if (isset($_POST['DoIt'])  AND ($_POST['PrintOrEmail']=='Print' OR $ViewingOnly==1) ){
	$MakePDFThenDisplayIt = True;
} elseif (isset($_POST['DoIt']) AND $_POST['PrintOrEmail']=='Email'){
	$MakePDFThenEmailIt = True;
}
if (isset($OrderNo) AND $OrderNo != '' AND $OrderNo > 0 AND $OrderNo != 'Preview'){
	/*retrieve the order details from the database to print */
	$ErrMsg = _('There was a problem retrieving the purchase order header details for Order Number'). ' ' . $OrderNo .
			' ' . _('from the database');
	$sql = "SELECT	purchorders.supplierno,
									suppliers.suppname,
									suppliers.address1,
									suppliers.address2,
									suppliers.address3,
									suppliers.address4,
									purchorders.comments,
									purchorders.orddate,
									purchorders.rate,
									purchorders.dateprinted,
									purchorders.deladd1,
									purchorders.deladd2,
									purchorders.deladd3,
									purchorders.deladd4,
									purchorders.deladd5,
									purchorders.deladd6,
									purchorders.allowprint,
									purchorders.requisitionno,
									purchorders.initiator,
									purchorders.paymentterms,
									suppliers.currcode,
									purchorders.status,
									purchorders.stat_comment
								FROM purchorders INNER JOIN suppliers
									ON purchorders.supplierno = suppliers.supplierid
								WHERE purchorders.orderno='" . $OrderNo ."'";
	$result=DB_query($sql,$db, $ErrMsg);
	if (DB_num_rows($result)==0){ /*There is no order header returned */
		$title = _('Print Purchase Order Error');
		include('includes/header.inc');
		echo '<div class="centre"><br /><br /><br />';
		prnMsg( _('Unable to Locate Purchase Order Number') . ' : ' . $OrderNo . ' ', 'error');
		echo '<br />
					<br />
					<br />
					<table class="table_index">
						<tr><td class="menu_group_item">
						<li><a href="'. $rootpath . '/PO_SelectOSPurchOrder.php?'.SID .'">' . _('Outstanding Purchase Orders') . '</a></li>
						<li><a href="'. $rootpath . '/PO_SelectPurchOrder.php?'. SID .'">' . _('Purchase Order Inquiry') . '</a></li>
						</td>
						</tr>
					</table>
					</div><br /><br /><br />';
		include('includes/footer.inc');
		exit();
	} elseif (DB_num_rows($result)==1){ /*There is only one order header returned  (as it should be!)*/

		$POHeader = DB_fetch_array($result);
		
		if ($POHeader['status'] != 'Authorised' AND $POHeader['status'] != 'Printed') {
			include('includes/header.inc');
			prnMsg( _('Purchase orders can only be printed once they have been authorised') . '. ' . _('This order is currently at a status of') . ' ' . _($OrderStatus),'warn');
			include('includes/footer.inc');
			exit;
		}
		
//		if ($ViewingOnly==0) {
//			if ($POHeader['allowprint']==0){
//				$title = _('Requisition Order Already Printed');
//				include('includes/header.inc');
//				echo '<p>';
//				prnMsg( _('Requisition Order Number').' ' . $OrderNo . ' '. _('has previously been printed') . '. ' . _('It was printed on'). ' ' .
//				ConvertSQLDate($POHeader['dateprinted']) . '<br />'.
//					_('To re-print the order it must be modified to allow a reprint'). '<br />'.
//					_('This check is to ensure that duplicate Requisition orders are not Printed'), 'warn');
//				echo '<br /><table class="table_index">
//					<tr><td class="menu_group_item">
// 					<li><a href="' . $rootpath . '/PO_PDFPurchReq.php?' . SID . 'OrderNo=' . $OrderNo . '&ViewingOnly=1">'.
//				_('Print This Order as a Copy'). '</a>
// 				<li><a href="' . $rootpath . '/PO_Header.php?' . SID . 'ModifyOrderNumber=' . $OrderNo . '">'.
//				_('Modify the order to allow a real reprint'). '</a>' .
//				'<li><a href="'. $rootpath .'/PO_SelectPurchOrder.php?' . SID . '">'.
//				_('Select another order'). '</a>'.
//				'<li><a href="' . $rootpath . '/index.php?' . SID . '">'. _('Back to the menu').'</a>';
//				include('includes/footer.inc');
//				exit;
//			}//AllowedToPrint
//		}//not ViewingOnly
	}// 1 valid record
}//if there is a valid order number
else if ($OrderNo=='Preview') {// We are previewing the order
/* Fill the order header details with dummy data */
	$POHeader['supplierno']=str_pad('',10,'x');
	$POHeader['suppname']=str_pad('',40,'x');
	$POHeader['address1']=str_pad('',40,'x');
	$POHeader['address2']=str_pad('',40,'x');
	$POHeader['address3']=str_pad('',40,'x');
	$POHeader['address4']=str_pad('',30,'x');
	$POHeader['comments']=str_pad('',50,'x');
	$POHeader['orddate']='1900-01-01';
	$POHeader['rate']='0.0000';
	$POHeader['dateprinted']='1900-01-01';
	$POHeader['deladd1']=str_pad('',40,'x');
	$POHeader['deladd2']=str_pad('',40,'x');
	$POHeader['deladd3']=str_pad('',40,'x');
	$POHeader['deladd4']=str_pad('',40,'x');
	$POHeader['deladd5']=str_pad('',20,'x');
	$POHeader['deladd6']=str_pad('',15,'x');
	$POHeader['allowprint']=1;
	$POHeader['requisitionno']=str_pad('',15,'x');
	$POHeader['initiator']=str_pad('',50,'x');
	$POHeader['paymentterms']=str_pad('',15,'x');
	$POHeader['currcode']='XXX';
} // end of If we are previewing the order
/* Load the relevant xml file */
if (isset($MakePDFThenDisplayIt) or isset($MakePDFThenEmailIt)) {
	if ($OrderNo=='Preview') {
		$FormDesign = simplexml_load_file(sys_get_temp_dir().'/requisitionOrder.xml');
	} else {
		$FormDesign = simplexml_load_file($PathPrefix.'companies/'.$_SESSION['DatabaseName'].'/FormDesigns/requisitionOrder.xml');
	}
// Set the paper size/orintation
	$PaperSize = $FormDesign->PaperSize;
	include('includes/PDFStarter.php');
	$pdf->addInfo('Title', _('Requision Order') );
	$pdf->addInfo('Subject', _('Requisition Order Number' ) . ' ' . $OrderNo);
	$line_height = $FormDesign->LineHeight;
	$PageNumber = 1;
	/* Then there's an order to print and its not been printed already (or its been flagged for reprinting)
	Now ... Has it got any line items */
	if ($OrderNo !='Preview') { // It is a real order
		$ErrMsg = _('There was a problem retrieving the line details for order number') . ' ' . $OrderNo . ' ' .
			_('from the database');
		$sql = "SELECT itemcode,
									deliverydate,
									itemdescription,
									unitprice,
									suppliersunit,
									quantityord,
									decimalplaces,
									conversionfactor,
									suppliers_partno
							FROM purchorderdetails LEFT JOIN stockmaster
								ON purchorderdetails.itemcode=stockmaster.stockid
							WHERE orderno ='" . $OrderNo ."'";
		$result=DB_query($sql,$db);
	}
	if ($OrderNo=='Preview' or DB_num_rows($result)>0){
		/*Yes there are line items to start the ball rolling with a page header */
		include('includes/PO_PDFRequsitionPageHeader.inc');
		$YPos=$Page_Height - $FormDesign->Data->y;
		$OrderTotal = 0;
		while ((isset($OrderNo) and $OrderNo=='Preview') or (isset($result) and $POLine=DB_fetch_array($result))) {
			/* If we are previewing the order then fill the
			 * order line with dummy data */
			if ($OrderNo=='Preview') {
				$POLine['itemcode']=str_pad('',10,'x');
				$POLine['deliverydate']='1900-01-01';
				$POLine['itemdescription']=str_pad('',50,'x');
				$POLine['unitprice']=9999.99;
				$POLine['units']=str_pad('',4,'x');
				$POLine['quantityord']=999.99;
				$POLine['decimalplaces']=2;
			}
			$DisplayQty = number_format($POLine['quantityord']/$POLine['conversionfactor'],$POLine['decimalplaces']);
			if ($_POST['ShowAmounts']=='Yes'){
				$DisplayPrice = number_format($POLine['unitprice']*$POLine['conversionfactor'],2);
			} else {
				$DisplayPrice = '----';
			}
			$DisplayDelDate = ConvertSQLDate($POLine['deliverydate'],2);
			if ($_POST['ShowAmounts']=='Yes'){
				$DisplayLineTotal = $POLine['unitprice']*$POLine['quantityord'];
			} else {
				$DisplayLineTotal = '----';
			}
			$Desc=$POLine['itemdescription'];
			
			$OrderTotal += ($POLine['unitprice']*$POLine['quantityord']);
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column1->x,$YPos,$FormDesign->Data->Column1->Length,$FormDesign->Data->Column1->FontSize,$POLine['itemcode'], 'left');
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column2->x,$YPos,$FormDesign->Data->Column2->Length,$FormDesign->Data->Column2->FontSize,$Desc, 'left');
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column3->x,$YPos,$FormDesign->Data->Column3->Length,$FormDesign->Data->Column3->FontSize,$DisplayQty, 'left');
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column4->x,$YPos,$FormDesign->Data->Column4->Length,$FormDesign->Data->Column4->FontSize,$POLine['suppliersunit'], 'left');
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column5->x,$YPos,$FormDesign->Data->Column5->Length,$FormDesign->Data->Column5->FontSize,$DisplayDelDate, 'left');
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column6->x,$YPos,$FormDesign->Data->Column6->Length,$FormDesign->Data->Column6->FontSize,$DisplayPrice, 'right');
			$LeftOvers = $pdf->addTextWrap($FormDesign->Data->Column7->x,$YPos,$FormDesign->Data->Column7->Length,$FormDesign->Data->Column7->FontSize,$DisplayLineTotal, 'right');
			if (strlen($LeftOvers)>1){
				$LeftOvers = $pdf->addTextWrap($Left_Margin+1+94,$YPos-$line_height,270,$FontSize,$LeftOvers, 'left');
				$YPos-=$line_height;
			}
			if ($YPos-$line_height <= $Bottom_Margin){
				/* We reached the end of the page so finsih off the page and start a newy */
				$PageNumber++;
				$YPos=$Page_Height - 40; //$FormDesign->Data->y;
				include ('includes/PO_PDFOrderTitleHeader.inc');
			} //end if need a new page headed up
			/*increment a line down for the next line item */
			$YPos -= $line_height;
			/* If we are previewing we want to stop showing order
			 * lines after the first one */
			if ($OrderNo=='Preview') {
//				unlink(sys_get_temp_dir().'/PurchaseOrder.xml');
				unset($OrderNo);
			}


		} //end while there are line items to print out
        $currPoint=$YPos-10;

        if ($_POST['ShowAmounts']=='Yes'){
            $DisplayOrderTotal = $OrderTotal;
        } else {
            $DisplayOrderTotal = '----';
        }
        $currPoint=$YPos-30;
        $pdf->addText($FormDesign->OrderTotalCaption->x,$currPoint, $FormDesign->OrderTotalCaption->FontSize, _('Order Total - excl tax'). ' ' . $POHeader['currcode']);
        $pdf->addText($FormDesign->OrderTotal->x,$currPoint,$FormDesign->OrderTotal->FontSize,$DisplayOrderTotal);

        $currPoint=$YPos-45;
        $pdf->addText($FormDesign->preparedByCaption->x,$currPoint, $FormDesign->preparedByCaption->FontSize, _('Prepared By'));
        $pdf->addText($FormDesign->preparedLine1->x,$currPoint, $FormDesign->preparedLine1->FontSize, _('______________________'));
        $pdf->addText($FormDesign->checkedByCaption->x,$currPoint, $FormDesign->checkedByCaption->FontSize, _('Checked By'));
        $pdf->addText($FormDesign->checkedLine1->x,$currPoint, $FormDesign->checkedLine1->FontSize, _('______________________'));
        $currPoint=$YPos-85;
        $pdf->addText($FormDesign->approvedByCaption->x,$currPoint, $FormDesign->approvedByCaption->FontSize, _('Approved By'));
        $pdf->addText($FormDesign->checkedLine2->x,$currPoint, $FormDesign->checkedLine2->FontSize, _('______________________'));


        if ($YPos-$line_height <= $Bottom_Margin){ // need to ensure space for totals
				$PageNumber++;
				include ('includes/PO_PDFOrderTitleHeader.inc');
		} //end if need a new page headed up


        } /*end if there are order details to show on the order*/
	//} /* end of check to see that there was an order selected to print */
	
	$Success = 1; //assume the best and email goes - has to be set to 1 to allow update status
	if ($MakePDFThenDisplayIt){
		$pdf->OutputD($_SESSION['DatabaseName'] . '_Requisition_' . $OrderNo . '_' . date('Y-m-d') . '.pdf');//UldisN
		$pdf->__destruct(); //UldisN
	} else { /* must be MakingPDF to email it */
		
		$PdfFileName = $_SESSION['DatabaseName'] . '_Requisition_' . $OrderNo . '_' .date('Y-m-d') . '.pdf';
		$pdf->Output($_SESSION['reports_dir'] . '/' . $PdfFileName,'F');
		$pdf->__destruct(); 
		include('includes/htmlMimeMail.php');
		$mail = new htmlMimeMail();
		$attachment = $mail->getFile($_SESSION['reports_dir'] . '/' . $PdfFileName);
		$mail->setText( _('Please find herewith our Requisition number').' ' . $OrderNo);
		$mail->setSubject( _('Requisition Order Number').' ' . $OrderNo);
		$mail->addAttachment($attachment, $PdfFileName, 'application/pdf');
		$mail->setFrom($_SESSION['CompanyRecord']['coyname'] . "<" . $_SESSION['CompanyRecord']['email'] .">");
		$Success = $mail->send(array($_POST['EmailTo']));
		if ($Success==1){
			echo '<p />';
			prnMsg( _('Requisition Order'). ' ' . $OrderNo.' ' . _('has been emailed to') .' ' . $_POST['EmailTo'] . ' ' . _('as directed'), 'success');
		} else { //email failed
			echo '<p />';
			prnMsg( _('Emailing Requisition order'). ' ' . $OrderNo.' ' . _('to') .' ' . $_POST['EmailTo'] . ' ' . _('failed'), 'error');
		}
	}
	if ($ViewingOnly==0 AND $Success==1) {
		$sql = "UPDATE purchorders	SET	requisitionStatus = 'Completed' WHERE purchorders.orderno = '" .  $OrderNo."'";
		$result = DB_query($sql,$db);
	}
} /* There was enough info to either print or email the purchase order */
 else { /*the user has just gone into the page need to ask the question whether to print the order or email it to the supplier */
	include ('includes/header.inc');
	echo '<form action="' . $_SERVER['PHP_SELF'] . '?' . SID . '" method=post>';
	echo '<input type="hidden" name="FormID" value="' . $_SESSION['FormID'] . '" />';
	if ($ViewingOnly==1){
		echo '<input type=hidden name="ViewingOnly" VALUE=1>';
	}
	echo '<br /><br />';
	echo '<input type=hidden name="OrderNo" VALUE="'. $OrderNo. '">';
	echo '<table><tr><td>'. _('Print or Email the Order'). '</td><td>
		<select name="PrintOrEmail">';
	if (!isset($_POST['PrintOrEmail'])){
		$_POST['PrintOrEmail'] = 'Print';
	}
	if ($_POST['PrintOrEmail']=='Print'){
		echo '<option selected VALUE="Print">'. _('Print');
		echo '<option VALUE="Email">' . _('Email');
	} else {
		echo '<option VALUE="Print">'. _('Print');
		echo '<option selected VALUE="Email">'. _('Email');
	}
	echo '</select></td></tr>';
	echo '<tr><td>'. _('Show Amounts on the Order'). '</td><td>
		<select name="ShowAmounts">';
	if (!isset($_POST['ShowAmounts'])){
		$_POST['ShowAmounts'] = 'Yes';
	}
	if ($_POST['ShowAmounts']=='Yes'){
		echo '<option selected VALUE="Yes">'. _('Yes');
		echo '<option VALUE="No">' . _('No');
	} else {
		echo '<option VALUE="Yes">'. _('Yes');
		echo '<option selected VALUE="No">'. _('No');
	}
	echo '</select></td></tr>';
	if ($_POST['PrintOrEmail']=='Email'){
		$ErrMsg = _('There was a problem retrieving the contact details for the supplier');
		$SQL = "SELECT suppliercontacts.contact,
										suppliercontacts.email
							FROM suppliercontacts INNER JOIN purchorders
							ON suppliercontacts.supplierid=purchorders.supplierno
							WHERE purchorders.orderno='".$OrderNo."'";
		$ContactsResult=DB_query($SQL,$db, $ErrMsg);
		if (DB_num_rows($ContactsResult)>0){
			echo '<tr><td>'. _('Email to') .':</td><td><select name="EmailTo">';
			while ($ContactDetails = DB_fetch_array($ContactsResult)){
				if (strlen($ContactDetails['email'])>2 AND strpos($ContactDetails['email'],'@')>0){
					if ($_POST['EmailTo']==$ContactDetails['email']){
						echo '<option selected VALUE="' . $ContactDetails['email'] . '">' . $ContactDetails['Contact'] . ' - ' . $ContactDetails['email'] . '</option>';
					} else {
						echo '<option VALUE="' . $ContactDetails['email'] . '">' . $ContactDetails['contact'] . ' - ' . $ContactDetails['email'] . '</option>';
					}
				}
			}
			echo '</select></td></tr></table>';
		} else {
			echo '</table><br />';
			prnMsg ( _('There are no contacts defined for the supplier of this order') . '. ' .
				_('You must first set up supplier contacts before emailing an order'), 'error');
			echo '<br />';
		}
	} else {
		echo '</table>';
	}
	echo '<br /><div class="centre"><input type=submit name="DoIt" VALUE="' . _('OK') . '"></div>';
	echo '</form>';
	include('includes/footer.inc');
}
?>