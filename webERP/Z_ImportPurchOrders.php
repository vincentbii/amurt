<?php
/* $Id: Z_ImportPartCodes.php 5296 2012-04-29 15:28:19Z vvs2012 $*/


include('includes/session.inc');
$title = _('Import Purchase Order Items');
include('includes/header.inc');

if(isset($_POST['ProcessOrder'])){
    function getPurchDataDetails($stockid){
        global $db;
        $sql="SELECT stockid,conversionfactor FROM purchdata WHERE stockid='$stockid'";
        $results = DB_query($sql,$db);
        $row=DB_fetch_array($results);

        return $row;
    }

    $sql = "SELECT itemcode,itemdescription,unitprice,quantityord FROM purchordertemp";

    $results = DB_query($sql,$db);

    while ($row=DB_fetch_array($results)){
        $purchDetails=getPurchDataDetails($row[stockid]);

        if($purchDetails["conversionfactor"]==''){
            $purchDetails["conversionfactor"]=1;
        }

        if($row['unitprice']<>''){
            $unitprice=$row['unitprice']/$purchDetails["conversionfactor"];
        }else{
            $unitprice=$purchDetails['unitprice']/$purchDetails["conversionfactor"];
        }

        $quantityord=$row['quantityord']*$purchDetails["conversionfactor"];
        $conversionfactor=$purchDetails["conversionfactor"];
        $orderDate=date('Y-m-d');
        $orderNo=$_POST['orderNo'];

        $sql = "INSERT INTO purchorderdetails(orderno,itemcode,deliverydate,itemdescription,glcode,unitprice,quantityord,
                  suppliersunit,suppliers_partno,conversionfactor)
                SELECT '$orderNo',itemcode,'$orderDate',itemdescription,'1320',$unitprice,$quantityord,'each'
                ,itemcode,$conversionfactor FROM purchordertemp where itemcode='$row[itemcode]'";

        if(DB_query($sql,$db)){
            echo " Success: inserted stockid $purchDetails[stockid]<br>";
        }else{
            echo "Failure ".$sql.'<br>';
        }

    }
}


if (isset($_POST['update'])) {

    $is_new_post = true;

    if (isset($_SESSION["myform_key"]) && isset($_POST["myform_key"])) {
        if($_POST["myform_key"] == $_SESSION["myform_key"] ){
            $is_new_post = false;
            prnMsg( _('The order has already been uploaded<br />'));
        }
    }
    if($is_new_post){
        $_SESSION["myform_key"] = $_POST["myform_key"];

        $fp = fopen($_FILES['ImportFile']['tmp_name'], "r");
        $buffer = fgets($fp);
        $FieldNames = explode(',', $buffer);
        $SuccessStyle='style="color:green; font-weight:bold"';
        $FailureStyle='style="color:red; font-weight:bold"';


        echo '<table>
            <tr><td colspan="6" align="center"><br />
            <form id="process" enctype="multipart/form-data" method="post" action="' . htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '?' .SID .'">
            <input type="hidden" name="FormID" value="' . $_SESSION['FormID'] . '" />
            <input type="submit" name="ProcessOrder"  id="ProcessOrder" value="Process the Order" />
            <input type="text" id="orderNo" name="orderNo" value=""> <br /></form></td></tr>
			<tr>
				<th>'. _('Part Code') .'</th>
				<th>'. _('Item Description') . '</th>
				<th>'. _('Qty Ordered') .'</th>
				<th>'. _('Unit Price') .'</th>
				<th>'. _('Total') .'</th>
				<th>'. _('Status') .'</th>
			</tr>';
        $successes=0;
        $failures=0;
        while (!feof ($fp)) {
            $buffer = fgets($fp);
            $FieldValues = explode(',', $buffer);
            if ($FieldValues[0]!='') {
                //echo "Total fieldNames are ".sizeof($FieldNames) ." and size of fieldvalues is ".sizeof($FieldValues)."<br>";
                for ($i=0; $i<sizeof($FieldValues); $i++) {
                    $ItemDetails[$FieldNames[$i]]=$FieldValues[$i];
                }

                $description=trim($ItemDetails[Description]," ");

                $sql = "INSERT INTO purchordertemp(itemcode,itemdescription,unitprice,quantityord,total)
                    VALUES('$ItemDetails[StockCode]','$description','$ItemDetails[Price]','$ItemDetails[Quantity]','$ItemDetails[total]')";
                $result = DB_query($sql,$db);
//
//
                if ($result) {

                    echo '<tr '.$SuccessStyle.'><td>'.$ItemDetails['StockCode'].'</td>
			                            <td>'.$description.'</td>
			                            <td align=right>'.$ItemDetails['Quantity'].'</td>
			                            <td align=right>'.$ItemDetails['Price'].'</td>
			                            <td align=right>'.$ItemDetails['Quantity']*$ItemDetails['Price'].'</td>
			                            <td>'.'Success'.'</td></tr>';
                    $successes++;
                } else {
                    echo '<tr '.$FailureStyle.'><td>'.$ItemDetails['stockcode'].'</td><td>'.'Failure'.'</td><td></td></tr>';
                    $failures++;
                }
            }
            unset($ItemDetails);
        }
        echo '<tr><td>'.$successes._(' records successfully imported') .'</td></tr>';
        echo '<tr><td>'.$failures._(' records failed to import') .'</td></tr>';
        echo '</table>';
        fclose ($fp);
    }

} else {
	$sql = "select * from locations";
	$result = DB_query($sql,$db);
	if (DB_num_rows($result)==0) {
		prnMsg( _('No Order Number has been Set. At least one Order Number should be set up first'), "error");
	} else {
		prnMsg( _('Select a csv file containing the details of the Order that you wish to import into webERP. '). '<br />' .
			 _('The first line must contain the field names that you wish to import. ').'<br />' .
			 _('The field Names should look like this: StockCode,Description,Price,Qty'));
		echo '<form id="ItemForm" enctype="multipart/form-data" method="post" action="' . htmlspecialchars($_SERVER['PHP_SELF'],ENT_QUOTES,'UTF-8') . '?' .SID .'">';
        echo '<div class="centre">';
		echo '<input type="hidden" name="FormID" value="' . $_SESSION['FormID'] . '" />';
		echo '<table><tr><td>'._('File to import').'</td>'.
			'<td><input type="file" id="ImportFile" name="ImportFile" />';
		echo "<input type='hidden' name='myform_key' value='". md5(date('Y:m:d H:i:s'))."'/></td></tr></table>";
		echo '<div class="centre"><input type="submit" name="update" value="Import Order" /></div>';
		echo '</div>
              </form>';

	}
}

include('includes/footer.inc');

?>