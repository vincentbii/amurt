<?php

/* $Id: Z_RePostGLFromPeriod.php 5296 2012-04-29 15:28:19Z vvs2012 $*/

include ('includes/session.inc');
$title = _('Recalculation of GL Balances in Chart Details Table');
include('includes/header.inc');
  /*OK do the updates */

    /* Make the posted flag on all GL entries including and after the period selected = 0 */
    $sql = "UPDATE gltrans SET posted=0 WHERE periodno >='" . $_POST['FromPeriod'] . "'";
    $UpdGLTransPostedFlag = DB_query($sql,$db);

    /* Now make all the actuals 0 for all periods including and after the period from */
    $sql = "UPDATE chartdetails SET actual =0 WHERE period >= '" . $_POST['FromPeriod'] . "'";
    $UpdActualChartDetails = DB_query($sql,$db);

    $ChartDetailBFwdResult = DB_query("SELECT accountcode, bfwd FROM chartdetails WHERE period='" . $_POST['FromPeriod'] . "'",$db);
    while ($ChartRow=DB_fetch_array($ChartDetailBFwdResult)){
        $sql = "UPDATE chartdetails SET bfwd ='" . $ChartRow['bfwd'] . "' WHERE period > '" . $_POST['FromPeriod'] . "' AND accountcode='" . $ChartRow['accountcode'] . "'";
        $UpdActualChartDetails = DB_query($sql,$db);
    }

?>