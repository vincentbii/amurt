<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function InsertSupplierPayment($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
   
     $TransNo = GetNextTransNo(22, $db);
	$Transtype = 22;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
        $PaymentDetails['Currency']='Ksh';
        $PaymentDetails['Discount']=0;
        $PaymentDetails['creditorsact']=2081;
        $CreditorTotal = (($PaymentDetails['Discount'] + $PaymentDetails['ovamount'])/$PaymentDetails['ExRate'])/$PaymentDetails['FunctionalExRate'];
//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
   
    if (isset($PaymentDetails['supplierID'])){
	 $Errors = VerifySupplierNoExists($PaymentDetails['supplierID'],  sizeof($Errors), $Errors, $db);
    }
//    $Errors = VerifySupplierNoExists($PaymentDetails['supplierID'], $i, $Errors, $db);
//    $Errors = VerifyTransNO($PaymentDetails['transno'], 22, sizeof($Errors), $Errors, $db);
//    $Errors = VerifyTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);

//    if (isset($PaymentDetails['glCode'])) {
//        $Errors = VerifyAccountCode($PaymentDetails['glCode'], $i, $Errors, $db);
//    }
//    if (isset($PaymentDetails['glAccount'])) {
//        $Errors = VerifyAccountName($PaymentDetails['glAccount'], $i, $Errors);
//    }
//
//    if (isset($PaymentDetails['chequeNo'])) {
//        $Errors = VerifyChequeNo($PaymentDetails['chequeNo'], sizeof($Errors), $Errors);
//    }
//
//    if (isset($PaymentDetails['ovamount'])) {
//        $Errors = VerifyOVAmount($PaymentDetails['ovamount'], sizeof($Errors), $Errors);
//    }

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
            $SQL = "INSERT INTO supptrans (transno,
                                    type,
                                    supplierno,
                                    trandate,
                                    inputdate,
                                    suppreference,
                                    rate,
                                    ovamount,
                                    transtext,
                                    chequeNo,
                                    voucherNo) ";
            $SQL = $SQL . "values ('" . $TransNo . "',
                            22,
                            '" . $PaymentDetails['supplierID'] . "',
                            '" .  $PaymentDetails['trandate'] . "',
                            '" . date('Y-m-d H-i-s') . "',
                            '" . $PaymentDetails['paymentType'] . "',
                            '1',
                            '" . (-$PaymentDetails['ovamount']) . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['Voucher_No'] . "')";
//                        echo $SQL;

            $result = DB_Query($SQL, $db); 
             /*Update the supplier master with the date and amount of the last payment made */
            $SQL = "UPDATE suppliers 
                            SET	lastpaiddate = '" .$PaymentDetails['trandate'] . "',
                                    lastpaid='" . $PaymentDetails['ovamount'] ."'
                            WHERE suppliers.supplierid='" . $PaymentDetails['supplierID'] . "'";

              $result = DB_Query($SQL, $db); 
                    $SQL="INSERT INTO gltrans ( type,
                                        typeno,
                                        trandate,
                                        periodno,
                                        account,
                                        narrative,
                                        amount,
                                        chequeNo,
                                        voucherNo,Payee) ";
                    $SQL=$SQL . "VALUES (22,
                                        '" . $TransNo . "',
                                        '" . $PaymentDetails['trandate'] . "',
                                        '" . $PeriodNo . "',
                                        '" . $PaymentDetails['creditorsact'] . "',
                                        '" . $PaymentDetails['Narrative'] . "',
                                        '" . $CreditorTotal . "',
                                        '" . $PaymentDetails['chequeNo'] . "',
                                        '" . $PaymentDetails['Voucher_No'] . "',
                                        '" . $PaymentDetails['reference'] . "')";
                    $result = DB_Query($SQL, $db); 
////                    echo $SQL;
//
//				/* Bank account entry first */
				$SQL = "INSERT INTO gltrans ( type,
                                            typeno,
                                            trandate,
                                            periodno,
                                            account,
                                            narrative,
                                            amount,
                                            chequeNo,
                                            voucherNo,Payee) ";
					$SQL = $SQL . "VALUES ('" . $Transtype . "',
                                            '" . $TransNo . "',
                                            '" . $PaymentDetails['trandate'] . "',
                                            '" . $PeriodNo . "',
                                            '" . $PaymentDetails['glCode'] . "',
                                            '" . $PaymentDetails['Narrative'] . "',
                                            '" . -$PaymentDetails['ovamount'] . "',
                                            '" . $PaymentDetails['chequeNo'] . "',
                                            '" . $PaymentDetails['Voucher_No'] . "',
                                            '" . $PaymentDetails['reference'] . "')";

				  $result = DB_Query($SQL, $db); 
                                  
       //		/*now enter the BankTrans entry */
		if ($Transtype==22 && $PaymentDetails['glCode']<>'3107') {
			$SQL="INSERT INTO banktrans (transno,
                                                    type,
                                                    bankact,
                                                    ref,
                                                    exrate,
                                                    functionalexrate,
                                                    transdate,
                                                    banktranstype,
                                                    amount,
                                                    currcode) ";
                    $SQL= $SQL . "VALUES ('" . $TransNo . "',
                                            '" . $Transtype . "',
                                            '" . $PaymentDetails['glCode'] . "',
                                            '" . $PaymentDetails['Narrative'] . "',
                                            '" . $PaymentDetails['ExRate'] . "',
                                            '" . $PaymentDetails['FunctionalExRate'] . "',
                                            '" . $PaymentDetails['trandate'] . "',
                                            '" . $PaymentDetails['paymentType'] . "',
                                            '" . -$PaymentDetails['ovamount'] . "',
                                            '" . $PaymentDetails['Currency'] . "'
                                    )";

			  $result = DB_Query($SQL, $db); 
		} 
//		
      
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
            //  Return invoice number too
            $Errors[] = 1;// $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}

//398372346
//e28a7g

function InsertPCPayment($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransNo( 1, $db);
	    $Transtype = 1;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
//        $PaymentDetails['Currency']='Ksh';
//        $PaymentDetails['Discount']=0;
//        $PaymentDetails['creditorsact']=2081;
        $TrfToBankExRate = $PaymentDetails['FunctionalExRate'];
        $CreditorTotal = (($PaymentDetails['Discount'] + $PaymentDetails['ovamount'])/$PaymentDetails['ExRate'])/$PaymentDetails['FunctionalExRate'];
//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
    if (isset($PaymentDetails['ledgerCode'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['ledgerCode'], sizeof($Errors), $Errors, $db);
    }

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
            $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,
                            tag,
                            voucherNo,
                            Payee) ";
                    $SQL= $SQL . "VALUES (1,
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['ledgerCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '" . $PaymentDetails['chequeNo'] ."',0,
                            '" . $PaymentDetails['VoucherNo'] . "',
                            '" . $PaymentDetails['reference'] . "')";
                    
                   $result = DB_Query($SQL, $db); 
//                   echo $SQL;
//                   
     if ($PaymentDetails['ovamount'] !=0){
                /* Bank account entry first */
                $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,
                            voucherNo,
                            Payee) ";
                        $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['glCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']/$PaymentDetails['ExRate']/$PaymentDetails['FunctionalExRate']) . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['VoucherNo'] . "',
                            '" . $PaymentDetails['reference'] . "')";

                $result = DB_Query($SQL, $db); 
	}  

        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}

function InsertGLPayment($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
    $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//
    $TransNo = GetNextTransNo( 1, $db);
    $Transtype = 1;
    $PaymentDetails['ExRate']=1;
    $PaymentDetails['FunctionalExRate']=1;
    $PaymentDetails['Currency']='Ksh';
    $PaymentDetails['Discount']=0;
    $PaymentDetails['creditorsact']=2081;
    $TrfToBankExRate = $PaymentDetails['FunctionalExRate'];
    $CreditorTotal = (($PaymentDetails['Discount'] + $PaymentDetails['ovamount'])/$PaymentDetails['ExRate'])/$PaymentDetails['FunctionalExRate'];
//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//
    if (isset($PaymentDetails['ledgerCode'])){
        $Errors = VerifyAccountCodeExists($PaymentDetails['ledgerCode'], sizeof($Errors), $Errors, $db);
    }

    $FieldNames = '';
    $FieldValues = '';

    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
        /* Create a SuppTrans entry for the supplier payment */
        $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,tag,
                            voucherNo,Payee) ";
        $SQL= $SQL . "VALUES (1,
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['ledgerCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '" . $PaymentDetails['chequeNo'] ."',0,
                            '" . $PaymentDetails['VoucherNo'] . "',
                            '" . $PaymentDetails['reference'] . "')";

        $result = DB_Query($SQL, $db);
//                   echo $SQL;
//
        $ReceiptTransNo = GetNextTransNo( 1, $db);
//        $SQL= "INSERT INTO banktrans (transno,
//                type,
//                bankact,
//                ref,
//                exrate,
//                functionalexrate,
//                transdate,
//                banktranstype,
//                amount,
//                currcode)
//            VALUES ('" . $ReceiptTransNo . "',
//                1,
//                '" . $PaymentDetails['glCode'] . "',
//                '" . _('Act Transfer From ') . $PaymentDetails['glCode'] . ' - ' . $PaymentDetails['Narrative'] . "',
//                '" . (($PaymentDetails['ExRate'] * $PaymentDetails['FunctionalExRate'])/$TrfToBankExRate). "',
//                '" . $TrfToBankExRate . "',
//                '" . $PaymentDetails['trandate'] . "',
//                '" . $PaymentDetails['paymentType'] . "',
//                '" . $PaymentDetails['ovamount'] . "',
//                '" . $PaymentDetails['Currency'] . "'
//            )";
//        $result = DB_Query($SQL, $db);

        if ($PaymentDetails['ovamount'] !=0){
            /* Bank account entry first */
            $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,
                            voucherNo,
                            Payee) ";
            $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['glCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']/$PaymentDetails['ExRate']/$PaymentDetails['FunctionalExRate']) . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['VoucherNo'] . "',
                            '" . $PaymentDetails['reference'] . "')";

            $result = DB_Query($SQL, $db);
        }

//        $SQL="INSERT INTO banktrans (transno,
//                    type,
//                    bankact,
//                    ref,
//                    exrate,
//                    functionalexrate,
//                    transdate,
//                    banktranstype,
//                    amount,
//                    currcode) ";
//        $SQL= $SQL . "VALUES ('" . $TransNo . "',
//                        '" . $Transtype . "',
//                        '" . $PaymentDetails['glCode'] . "',
//                        '" . $PaymentDetails['Narrative'] . "',
//                        '" . $PaymentDetails['ExRate'] . "',
//                        '" . $PaymentDetails['FunctionalExRate'] . "',
//                        '" . $PaymentDetails['trandate'] . "',
//                        '" . $PaymentDetails['paymentType'] . "',
//                        '" . -$PaymentDetails['ovamount'] . "',
//                        '" . $PaymentDetails['Currency'] . "'
//                )";
//
//        $result = DB_Query($SQL, $db);

        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}

function InsertDebtorPayment($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransNo( 1, $db);
	$Transtype = 1;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
        $PaymentDetails['Currency']='Ksh';
        $PaymentDetails['Discount']=0;
        $PaymentDetails['debtorsacc']=3109;
        $TrfToBankExRate = $PaymentDetails['FunctionalExRate'];
        $CreditorTotal = (($PaymentDetails['Discount'] + $PaymentDetails['ovamount'])/$PaymentDetails['ExRate'])/$PaymentDetails['FunctionalExRate'];
//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
    if (isset($PaymentDetails['glCode'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['glCode'], $i, $Errors, $db);
    }


    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
            $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeno,tag,
                            voucherNo,
                            Payee) ";
                    $SQL= $SQL . "VALUES (1,
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['debtorsacc'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '". $PaymentDetails['chequeNo'] ."',0,
                            '". $PaymentDetails['Voucher_No'] ."',
                            '". $PaymentDetails['reference'] ."')";
                    
                   $result = DB_Query($SQL, $db); 
//                   echo $SQL;
//                   
       $ReceiptTransNo = GetNextTransNo( 2, $db);
    $SQL= "INSERT INTO banktrans (transno,
                type,
                bankact,
                ref,
                exrate,
                functionalexrate,
                transdate,
                banktranstype,
                amount,
                currcode)
            VALUES ('" . $ReceiptTransNo . "',
                1,
                '" . $PaymentDetails['glCode'] . "',
                '" . _('Act Transfer From ') . $PaymentDetails['glCode'] . ' - ' . $PaymentDetails['Narrative'] . "',
                '" . (($PaymentDetails['ExRate'] * $PaymentDetails['FunctionalExRate'])/$TrfToBankExRate). "',
                '" . $TrfToBankExRate . "',
                '" . $PaymentDetails['trandate'] . "',
                '" . $PaymentDetails['paymentType'] . "',
                '" . $PaymentDetails['ovamount'] . "',
                '" . $PaymentDetails['Currency'] . "'
            )";
             $result = DB_Query($SQL, $db); 

       if ($PaymentDetails['ovamount'] !=0){
                /* Bank account entry first */
                $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeno,
                            voucherNo,
                            Payee) ";
                        $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['glCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']/$PaymentDetails['ExRate']/$PaymentDetails['FunctionalExRate']) . "',
                            '". $PaymentDetails['chequeNo'] ."',
                            '". $PaymentDetails['Voucher_No'] ."',
                            '". $PaymentDetails['reference'] ."')";

                $result = DB_Query($SQL, $db); 
	}  
        
        $SQL="INSERT INTO banktrans (transno,
                    type,
                    bankact,
                    ref,
                    exrate,
                    functionalexrate,
                    transdate,
                    banktranstype,
                    amount,
                    currcode) ";
                $SQL= $SQL . "VALUES ('" . $TransNo . "',
                        '" . $Transtype . "',
                        '" . $PaymentDetails['glCode'] . "',
                        '" . $PaymentDetails['Narrative'] . "',
                        '" . $PaymentDetails['ExRate'] . "',
                        '" . $PaymentDetails['FunctionalExRate'] . "',
                        '" . $PaymentDetails['trandate'] . "',
                        '" . $PaymentDetails['paymentType'] . "',
                        '" . -$PaymentDetails['ovamount'] . "',
                        '" . $PaymentDetails['Currency'] . "'
                )";

                $result = DB_Query($SQL, $db); 
                     
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}

function InsertIPPayment($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransNo( 1, $db);
	$Transtype = 1;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
        $PaymentDetails['Currency']='Ksh';
        $PaymentDetails['Discount']=0;
        $PaymentDetails['debtorsacc']=3109;
        $TrfToBankExRate = $PaymentDetails['FunctionalExRate'];
        $CreditorTotal = (($PaymentDetails['Discount'] + $PaymentDetails['ovamount'])/$PaymentDetails['ExRate'])/$PaymentDetails['FunctionalExRate'];
//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
    if (isset($PaymentDetails['glCode'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['glCode'], $i, $Errors, $db);
    }


    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
            $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeno,
                            voucherNo,Payee,tag)";
                    $SQL= $SQL . "VALUES (1,
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['debtorsacc'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['Voucher_No'] . "',
                            '". $PaymentDetails['reference'] ."',0)";
                    
                   $result = DB_Query($SQL, $db); 
//                   echo $SQL;
//                   
       $ReceiptTransNo = GetNextTransNo( 2, $db);
    $SQL= "INSERT INTO banktrans (transno,
                type,
                bankact,
                ref,
                exrate,
                functionalexrate,
                transdate,
                banktranstype,
                amount,
                currcode)
            VALUES ('" . $ReceiptTransNo . "',
                2,
                '" . $PaymentDetails['glCode'] . "',
                '" . _('Act Transfer From ') . $PaymentDetails['glCode'] . ' - ' . $PaymentDetails['Narrative'] . "',
                '" . (($PaymentDetails['ExRate'] * $PaymentDetails['FunctionalExRate'])/$TrfToBankExRate). "',
                '" . $TrfToBankExRate . "',
                '" . $PaymentDetails['trandate'] . "',
                '" . $PaymentDetails['paymentType'] . "',
                '" . $PaymentDetails['ovamount'] . "',
                '" . $PaymentDetails['Currency'] ."')";
             $result = DB_Query($SQL, $db); 

       if ($PaymentDetails['ovamount'] !=0){
                /* Bank account entry first */
                $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeno,
                            voucherNo,Payee) ";
                        $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['glCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']/$PaymentDetails['ExRate']/$PaymentDetails['FunctionalExRate']) . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '". $PaymentDetails['Voucher_No'] ."',
                            '". $PaymentDetails['reference'] ."')";

                $result = DB_Query($SQL, $db); 
	}  
        
        $SQL="INSERT INTO banktrans (transno,
                    type,
                    bankact,
                    ref,
                    exrate,
                    functionalexrate,
                    transdate,
                    banktranstype,
                    amount,
                    currcode) ";
                $SQL= $SQL . "VALUES ('" . $TransNo . "',
                        '" . $Transtype . "',
                        '" . $PaymentDetails['glCode'] . "',
                        '" . $PaymentDetails['Narrative'] . "',
                        '" . $PaymentDetails['ExRate'] . "',
                        '" . $PaymentDetails['FunctionalExRate'] . "',
                        '" . $PaymentDetails['trandate'] . "',
                        '" . $PaymentDetails['paymentType'] . "',
                        '" . -$PaymentDetails['ovamount'] . "',
                        '" . $PaymentDetails['Currency'] . "'
                )";

                $result = DB_Query($SQL, $db); 
                     
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}
?>
