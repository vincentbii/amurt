<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function InsertGLPayroll($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
   
     $TransNo = GetNextTransNo(51, $db);
     $Transtype = 51;

    if (isset($PaymentDetails['glCode'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['glCode'], $i, $Errors, $db);
    }
    
    $Errors = VerifyOVAmount($PaymentDetails['ovamount'], sizeof($Errors), $Errors);
//    }

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
                    $SQL="INSERT INTO gltrans ( type,
                                        typeno,
                                        trandate,
                                        periodno,
                                        account,
                                        narrative,
                                        amount) ";
                    $SQL=$SQL . "VALUES (1,
                                        '" . $TransNo . "',
                                        '" . $PaymentDetails['trandate'] . "',
                                        '" . $PeriodNo . "',
                                        '" . $PaymentDetails['glCode'] . "',
                                        '" . $PaymentDetails['Narrative'] . "',
                                        '" . $PaymentDetails['ovamount'] . "')";
                    $result = DB_Query($SQL, $db); 
////                    echo $SQL;

		
//		
      
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
            //  Return invoice number too
            $Errors[] = 1;// $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}

function InsertGLPayrollCredit($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
   
     $TransNo = GetNextTransNo(52, $db);
     $Transtype = 52;

    if (isset($PaymentDetails['glCode'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['glCode'], $i, $Errors, $db);
    }
    
    $Errors = VerifyOVAmount($PaymentDetails['ovamount'], sizeof($Errors), $Errors);
//    }

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
                    $SQL="INSERT INTO gltrans ( type,
                                        typeno,
                                        trandate,
                                        periodno,
                                        account,
                                        narrative,
                                        amount) ";
                    $SQL=$SQL . "VALUES (52,
                                        '" . $TransNo . "',
                                        '" . $PaymentDetails['trandate'] . "',
                                        '" . $PeriodNo . "',
                                        '" . $PaymentDetails['glCode'] . "',
                                        '" . $PaymentDetails['Narrative'] . "',
                                        '" . $PaymentDetails['ovamount'] . "')";
                    $result = DB_Query($SQL, $db); 
                   echo $SQL;

        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}

?>
