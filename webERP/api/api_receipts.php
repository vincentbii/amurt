<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* Retrieves the default debtors code for webERP */


function InsertGLReceipt($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransNo( 2, $db);
	$Transtype = 1;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
        $PaymentDetails['Currency']='Ksh';
        $PaymentDetails['Discount']=0;
        $PaymentDetails['debitsact']=3115; //undeposited fund

//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
    if (isset($PaymentDetails['gl_acc'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['creditGlCode'], sizeof($Errors), $Errors, $db);
    }

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
            $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,tag,
                            voucherNo,Payee) ";
                    $SQL= $SQL . "VALUES (2,
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['gl_acc'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '" . $PaymentDetails['chequeNo'] ."',0,
                            '" . $PaymentDetails['reference'] . "',
                            '" . $PaymentDetails['payee'] . "')";
                    
                   $result = DB_Query($SQL, $db); 
//                   echo $SQL;
//                   
       if ($PaymentDetails['ovamount'] !=0){
                /* Bank account entry first */
                $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,
                            voucherNo,
                            Payee) ";
                        $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['creditGlCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']) . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['reference'] . "',
                            '" . $PaymentDetails['payee'] . "')";

                $result = DB_Query($SQL, $db); 
	}  
        
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}


function InsertDBReceipt($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransNo( 2, $db);
	$Transtype = 1;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
        $PaymentDetails['Currency']='Ksh';
        $PaymentDetails['Discount']=0;
        $PaymentDetails['creditGlCode']=1116;

//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
    if (isset($PaymentDetails['gl_acc'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['gl_acc'], sizeof($Errors), $Errors, $db);
    }

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
            /* Create a SuppTrans entry for the supplier payment */
            $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,tag,
                            voucherNo,Payee) ";
                    $SQL= $SQL . "VALUES (2,
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['gl_acc'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '" . $PaymentDetails['chequeNo'] ."',0,
                            '" . $PaymentDetails['reference'] . "',
                            '" . $PaymentDetails['payee'] . "')";
                    
                   $result = DB_Query($SQL, $db); 
//                   echo $SQL;
//                   
       if ($PaymentDetails['ovamount'] !=0){
                /* Bank account entry first */
                $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,
                            voucherNo,
                            Payee) ";
                        $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $TransNo . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['creditGlCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']) . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['reference'] . "',
                            '" . $PaymentDetails['payee'] . "')";

                $result = DB_Query($SQL, $db); 
	}  
        
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}


function InsertIPReceipt($PaymentDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($PaymentDetails as $key => $value) {
        $PaymentDetails[$key] = DB_escape_string($value);
    }
     $PeriodNo = GetPeriod($PaymentDetails['trandate'],$db);
    $PaymentDetails['trandate']=ConvertToSQLDate($PaymentDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransNo( 12, $db);
	    $Transtype = 12;
        $PaymentDetails['ExRate']=1;
        $PaymentDetails['FunctionalExRate']=1;
        $PaymentDetails['Currency']='Ksh';
        $PaymentDetails['Discount']=0;
//        $creditGlCode=1115;

        $debtorTransno=GetNextTransactionNo(12, $db);
        $debtortype = 12;
        $debtorTrandate=date('Y-m-d H:i:s');
        $debtorNo=$PaymentDetails['creditGlCode'];

//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
    if (isset($PaymentDetails['gl_acc'])){
	 $Errors = VerifyAccountCodeExists($PaymentDetails['gl_acc'], sizeof($Errors), $Errors, $db);
    }

    unset($PaymentDetails['creditGlCode']);
    $PaymentDetails['creditGlCode']=3109;

    $FieldNames = '';
    $FieldValues = '';
   
    foreach ($PaymentDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }


    if (sizeof($Errors) == 0) {

        $sql2 = 'INSERT INTO debtortrans ( `transno`,  `type`,  `debtorno`,  `branchcode`,  `trandate`,  `inputdate`,  `prd`, `reference`,  `tpe`,
                           `order_`,  `rate`,  `ovamount`,  `invtext`,  `salesarea`,  `salesType`,  `partcode`)
                VALUES ("'.$debtorTransno.'","'.$debtortype.'","'.$debtorNo.'","'.$debtorNo.'","'.$debtorTrandate.'","'.$debtorTrandate
            .'","'.$PeriodNo.'","BILL PAYMENT","CA","'.$PaymentDetails['reference'].'","1","'.$PaymentDetails['ovamount']
            .'","Payment for INPATIENT-- INPATIENT BILL","DISPENS","rct","IP")';
        //echo $sql;
        $result = DB_Query($sql2, $db);

        $sql = 'UPDATE systypes set typeno='.GetNextTransactionNo(12, $db).' where typeid=12';
        $result = DB_Query($sql, $db);

        /* Create a IP TRANS entry for the supplier payment */
            $SQL = "INSERT INTO gltrans (type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,tag,
                            voucherNo,Payee) ";
                    $SQL= $SQL . "VALUES (12,
                            '" . $debtorTransno . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['gl_acc'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . $PaymentDetails['ovamount'] . "',
                            '" . $PaymentDetails['chequeNo'] ."',0,
                            '" . $PaymentDetails['reference'] . "',
                            '" . $PaymentDetails['payee'] . "')";
                    
                   $result = DB_Query($SQL, $db); 
//                   echo $SQL;
//                   
       if ($PaymentDetails['ovamount'] !=0){
                /* Bank account entry first */
                $SQL = "INSERT INTO gltrans ( type,
                            typeno,
                            trandate,
                            periodno,
                            account,
                            narrative,
                            amount,
                            chequeNo,
                            voucherNo,
                            Payee) ";
                        $SQL = $SQL . "VALUES ('" . $Transtype . "',
                            '" . $debtorTransno . "',
                            '" . $PaymentDetails['trandate'] . "',
                            '" . $PeriodNo . "',
                            '" . $PaymentDetails['creditGlCode'] . "',
                            '" . $PaymentDetails['Narrative'] . "',
                            '" . (-$PaymentDetails['ovamount']) . "',
                            '" . $PaymentDetails['chequeNo'] . "',
                            '" . $PaymentDetails['reference'] . "',
                            '" . $PaymentDetails['payee'] . "')";

                $result = DB_Query($SQL, $db); 
	}  
        
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $debtorTransno;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}


function InsertIPSalesInvoice($InvoiceDetails, $user, $password) {
    $Errors = array();
    $db = db($user, $password);
    if (gettype($db) == 'integer') {
        $Errors[0] = NoAuthorisation;
        return $Errors;
    }
    foreach ($InvoiceDetails as $key => $value) {
        $InvoiceDetails[$key] = DB_escape_string($value);
    }
    $PeriodNo = GetPeriod($InvoiceDetails['trandate'],$db);
    $InvoiceDetails['trandate']=ConvertToSQLDate($InvoiceDetails['trandate']);
//    $PeriodNo = GetPeriodFromTransactionDate($PaymentDetails['trandate'], sizeof($Errors), $Errors, $db);
//   
        $TransNo = GetNextTransactionNo(10, $db);
	$Transtype = 10;
        $InvoiceDetails['ExRate']=1;
        $InvoiceDetails['FunctionalExRate']=1;
        $InvoiceDetails['Currency']='Ksh';
        $InvoiceDetails['Discount']=0;
        $InvoiceDetails['creditGlCode']=3109;

//    $PaymentDetails['transno'] = GetNextTransactionNo(22, $db);
//    $PaymentDetails['type'] = 22;
//   
        $Errors=VerifyDebtorExists($InvoiceDetails['debtorno'], sizeof($Errors), $Errors, $db);
//    if (isset($InvoiceDetails['gl_acc'])){
//	 $Errors = VerifyAccountCodeExists($InvoiceDetails['gl_acc'], sizeof($Errors), $Errors, $db);
//    }
        $stockCat='IP';
        $SalesArea= $InvoiceDetails['salesarea'];
        
        $FieldNames = '';
        $FieldValues = '';
   
    foreach ($InvoiceDetails as $key => $value) {
        $FieldNames.=$key . ', ';
        $FieldValues.='"' . $value . '", ';
    }
    if (sizeof($Errors) == 0) {
          $SalesGLCode=GetSalesGLCode($SalesArea, $stockCat, $db);
          $DebtorsGLCode=GetDebtorsGLCode($db);

        $sql='insert into gltrans (`type`,`typeno`,`chequeno`,`trandate`,`periodno`,
            `account`,`narrative`,`amount`,`posted`,`jobref`,tag) 
            Values(10,'.GetNextTransactionNo(10, $db).
                ',0,"'.$InvoiceDetails['trandate'].'",'.$PeriodNo.', '.$DebtorsGLCode.
                ',"'.'Invoice for -'.$InvoiceDetails['debtorno'].' Total - '.$InvoiceDetails['ovamount'].
                '", '.$InvoiceDetails['ovamount'].', 0,"'.$InvoiceDetails['jobref'].'",1)';
        $result = DB_Query($sql, $db);
////        echo $sql;
//
        $sql='insert into gltrans (`type`,`typeno`,`chequeno`,`trandate`,`periodno`,
            `account`,`narrative`,`amount`,`posted`,`jobref`,tag) Values(10,'.GetNextTransactionNo(10, $db).
                ',0,"'.$InvoiceDetails['trandate'].'",'.$PeriodNo.', '.$SalesGLCode.
                ',"'.'Invoice for -'.$InvoiceDetails['debtorno'].' Total - '.$InvoiceDetails['ovamount'].
                '", '.-intval($InvoiceDetails['ovamount']).',0,"'.$InvoiceDetails['jobref'].'",1)';
        $result = DB_Query($sql, $db);

            $sql = 'UPDATE systypes set typeno='.GetNextTransactionNo(10, $db).' where typeid=10';
        $result = DB_Query($sql, $db); 
        
        if (DB_error_no($db) != 0) {
            $Errors[0] = DatabaseUpdateFailed;
        } else {
            $Errors[0] = 0;
////            //  Return invoice number too
            $Errors[] = $TransNo;//$PaymentDetails['transno'];
        }
        return $Errors;
    } else {
        return $Errors;
    }
}


?>
