<?php
echo '<html><head>Test Tims webERP API</head><body>';

//the xmlrpc class can output some funny warnings so make sure notices are turned off
error_reporting (E_ALL & ~E_NOTICE);

//you need to include the phpxmlrpc class - see link above - copy the whole directory structure of the class over to your client application
include ("../xmlrpc/lib/xmlrpc.inc");
include '../xmlrpc/lib/xmlrpcs.inc';

//if your webERP install is on a server at http://www.yourdomain.com/webERP
$ServerURL = "http://localhost/webERP/api/api_xml-rpc.php";
$DebugLevel = 2; //Set to 0,1, or 2 with 2 being the highest level of debug info

$Parameters = array();

/*The trap for me was that each parameter needs to be run through xmlrpcval()  - to create the necessary xml required for the rpc call
if one of the parameters required is an array then it needs to be processing into xml for the rpc call through php_xmlrpc_encode()*/

$Parameters['StockID'] = xmlrpcval('ACET01'); //the stockid of the item we wish to know the balance for
//assuming the demo username and password will work !
$Parameters['Username'] = xmlrpcval('admin');
$Parameters['Password'] = xmlrpcval('355ewxx');

$msg = new xmlrpcmsg("weberp.xmlrpc_GetStockBalance", $Parameters);

$client = new xmlrpc_client($ServerURL);
$client->setDebug($DebugLevel);
$response = $client->send($msg);
$answer = php_xmlrpc_decode($response->value());
if ($answer[0]!=0){ //then the API returned some errors need to figure out what went wrong

//need to figure out how to return all the error descriptions associated with the codes

} else { //all went well the returned data is in $answer[1]
//answer will be an array of the locations and quantity on hand for DVD_TOPGUN so we need to run through the array to print out
for ($i=0; $i<sizeof($answer[1]);$i++) {
  echo '<br>' . $answer[1][$i]['loccode'] . ' has ' . $answer[1][$i]['quantity'] . ' on hand';
}
}
echo '</body></html>';
?>